Application for Golf Swing Prescription
======================================

This the official web application for Golf Swing Prescription. Here I am using cakephp framework for handle the logical
portion or programming section. And also twitter bootstrap for handle the user interfacing of this project. I will
to use some other tools later, and I will to put that tools here.

[![CakePHP](http://cakephp.org/img/cake-logo.png)](http://www.cakephp.org)

Application Security Salt and Security Seed
==========================================

```php
/**
* A random string used in security hashing methods.
*/
Configure::write('Security.salt', 'DYhG93b0qygnhytujjJfIxfs2guVoUubWwjytryhtdhtrhtvniR2G0FgaC9mi');

/**
* A random numeric string (digits only) used to encrypt/decrypt strings.
*/
Configure::write('Security.cipherSeed', '76859309657453542496715643513541354549683645');

```