-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 17, 2015 at 06:17 AM
-- Server version: 5.5.46-0ubuntu0.14.04.2
-- PHP Version: 5.5.9-1ubuntu4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `golf-swing-prescription`
--

-- --------------------------------------------------------

--
-- Table structure for table `appointments`
--

CREATE TABLE IF NOT EXISTS `appointments` (
  `id` int(11) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `uuid` varchar(100) NOT NULL,
  `appointment_by` int(11) unsigned zerofill DEFAULT NULL,
  `instructor_id` int(11) unsigned zerofill NOT NULL,
  `package_id` int(11) unsigned zerofill DEFAULT NULL,
  `lesson_no` int(3) DEFAULT NULL,
  `lesson_sum` int(3) DEFAULT NULL,
  `start` datetime NOT NULL,
  `end` datetime NOT NULL,
  `note` varchar(255) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '1' COMMENT 'status: 1 = pending, 2 = confirm, 3 = cancelled, 4 = completed, 5 = expired',
  `operated_by` int(11) unsigned zerofill DEFAULT NULL,
  `is_reschedule` int(1) NOT NULL DEFAULT '0',
  `is_reassigned` int(1) NOT NULL DEFAULT '0' COMMENT 'is_reassigned: 0 = No, 1 =Yes',
  `is_already_rescheduled` int(1) NOT NULL DEFAULT '0',
  `is_benchmarked` int(1) NOT NULL DEFAULT '0' COMMENT 'is_benchmarked : 0 = no, 1 = yes',
  `is_converted` int(1) NOT NULL DEFAULT '0' COMMENT '0 = no, 1 = yes',
  `lesson_type` int(1) NOT NULL COMMENT 'If lesson_type = 1 means lesson of package was paid, and if 2 then it was for free package',
  `group_lesson_id` int(11) unsigned zerofill DEFAULT NULL,
  `pay_at_facility` tinyint(1) NOT NULL DEFAULT '1',
  `just_booked` int(1) DEFAULT NULL COMMENT 'just_booked <1: yes>',
  `next_lesson_start` datetime DEFAULT NULL,
  `next_lesson_end` datetime DEFAULT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `appointment_by` (`appointment_by`),
  KEY `instructor_id` (`instructor_id`),
  KEY `package_id` (`package_id`),
  KEY `operated_by` (`operated_by`),
  KEY `group_lesson_id` (`group_lesson_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

--
-- Table structure for table `attachments`
--

CREATE TABLE IF NOT EXISTS `attachments` (
  `id` int(11) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `uuid` varchar(100) NOT NULL,
  `package_id` int(11) unsigned zerofill DEFAULT NULL,
  `attachment` varchar(100) NOT NULL,
  `attachment_dir` int(11) NOT NULL,
  `type` varchar(50) NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `lesson_id` (`package_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `benchmarks`
--

CREATE TABLE IF NOT EXISTS `benchmarks` (
  `id` int(11) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `uuid` varchar(100) NOT NULL,
  `appointment_id` int(11) unsigned zerofill NOT NULL,
  `package_id` int(11) unsigned zerofill NOT NULL,
  `lesson_no` int(2) NOT NULL,
  `created_by` int(11) unsigned zerofill NOT NULL,
  `created_for` int(11) unsigned zerofill DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `posture_knee` int(2) DEFAULT NULL,
  `posture_knee_note` text,
  `posture_back` int(2) DEFAULT NULL,
  `posture_back_note` text,
  `stance_foot_width` int(2) DEFAULT NULL,
  `stance_foot_width_note` text,
  `stance_foot_direction` int(2) DEFAULT NULL,
  `stance_foot_direction_note` text,
  `arm_position_right` int(2) DEFAULT NULL,
  `arm_position_right_note` text,
  `arm_position_left` int(2) DEFAULT NULL,
  `arm_position_left_note` text,
  `grip_right_hand` int(2) DEFAULT NULL,
  `grip_right_hand_note` text,
  `grip_left_hand` int(2) DEFAULT NULL,
  `grip_left_hand_note` text,
  `alignment_feet` int(2) DEFAULT NULL,
  `alignment_feet_note` text,
  `alignment_knee` int(2) DEFAULT NULL,
  `alignment_knee_note` text,
  `alignment_hip` int(2) DEFAULT NULL,
  `alignment_hip_note` text,
  `alignment_shoulder` int(2) DEFAULT NULL,
  `alignment_shoulder_note` text,
  `alignment_arm` int(2) DEFAULT NULL,
  `alignment_arm_note` text,
  `alignment_club_face` int(2) DEFAULT NULL,
  `alignment_club_face_note` text,
  `ball_position_distance_away` int(2) DEFAULT NULL,
  `ball_position_distance_away_note` text,
  `ball_position_forward_and_back` int(2) DEFAULT NULL,
  `ball_position_forward_and_back_note` text,
  `lower_halfway_back_pear` int(2) DEFAULT NULL,
  `lower_halfway_back_pear_note` text,
  `lower_halfway_back_knee_flex` int(2) DEFAULT NULL,
  `lower_halfway_back_knee_flex_note` text,
  `lower_halfway_back_hip_sway` int(2) DEFAULT NULL,
  `lower_halfway_back_hip_sway_note` text,
  `lower_halfway_back_hip_pivot` int(2) DEFAULT NULL,
  `lower_halfway_back_hip_pivot_note` text,
  `lower_swing_pear` int(2) DEFAULT NULL,
  `lower_swing_pear_note` text,
  `lower_swing_knee_flex` int(2) DEFAULT NULL,
  `lower_swing_knee_flex_note` text,
  `lower_swing_hip_sway` int(2) DEFAULT NULL,
  `lower_swing_hip_sway_note` text,
  `lower_swing_hip_pivot` int(2) DEFAULT NULL,
  `lower_swing_hip_pivot_note` text,
  `lower_at_top_pear` int(2) DEFAULT NULL,
  `lower_at_top_pear_note` text,
  `lower_at_top_knee_flex` int(2) DEFAULT NULL,
  `lower_at_top_knee_flex_note` text,
  `lower_at_top_hip_sway` int(2) DEFAULT NULL,
  `lower_at_top_hip_sway_note` text,
  `lower_at_top_hip_pivot` int(2) DEFAULT NULL,
  `lower_at_top_hip_pivot_note` text,
  `lower_downswing_pear` int(2) DEFAULT NULL,
  `lower_downswing_pear_note` text,
  `lower_downswing_knee_flex` int(2) DEFAULT NULL,
  `lower_downswing_knee_flex_note` text,
  `lower_downswing_hip_sway` int(2) DEFAULT NULL,
  `lower_downswing_hip_sway_note` text,
  `lower_downswing_hip_pivot` int(2) DEFAULT NULL,
  `lower_downswing_hip_pivot_note` text,
  `lower_impact_pear` int(2) DEFAULT NULL,
  `lower_impact_pear_note` text,
  `lower_impact_knee_flex` int(2) DEFAULT NULL,
  `lower_impact_knee_flex_note` text,
  `lower_impact_hip_sway` int(2) DEFAULT NULL,
  `lower_impact_hip_sway_note` text,
  `lower_impact_hip_pivot` int(2) DEFAULT NULL,
  `lower_impact_hip_pivot_note` text,
  `lower_through_pear` int(2) DEFAULT NULL,
  `lower_through_pear_note` text,
  `lower_through_knee_flex` int(2) DEFAULT NULL,
  `lower_through_knee_flex_note` text,
  `lower_through_hip_sway` int(2) DEFAULT NULL,
  `lower_through_hip_sway_note` text,
  `lower_through_hip_pivot` int(2) DEFAULT NULL,
  `lower_through_hip_pivot_note` text,
  `lower_finish_pear` int(2) DEFAULT NULL,
  `lower_finish_pear_note` text,
  `lower_finish_knee_flex` int(2) DEFAULT NULL,
  `lower_finish_knee_flex_note` text,
  `lower_finish_hip_sway` int(2) DEFAULT NULL,
  `lower_finish_hip_sway_note` text,
  `lower_finish_hip_pivot` int(2) DEFAULT NULL,
  `lower_finish_hip_pivot_note` text,
  `upper_halfway_back_rotation` int(2) DEFAULT NULL,
  `upper_halfway_back_rotation_note` text,
  `upper_halfway_back_spine` int(2) DEFAULT NULL,
  `upper_halfway_back_spine_note` text,
  `upper_halfway_back_head` int(2) DEFAULT NULL,
  `upper_halfway_back_head_note` text,
  `upper_swing_rotation` int(2) DEFAULT NULL,
  `upper_swing_rotation_note` text,
  `upper_swing_spine` int(2) DEFAULT NULL,
  `upper_swing_spine_note` text,
  `upper_swing_head` int(2) DEFAULT NULL,
  `upper_swing_head_note` text,
  `upper_at_top_rotation` int(2) DEFAULT NULL,
  `upper_at_top_rotation_note` text,
  `upper_at_top_spine` int(2) DEFAULT NULL,
  `upper_at_top_spine_note` text,
  `upper_at_top_head` int(2) DEFAULT NULL,
  `upper_at_top_head_note` text,
  `upper_downswing_rotation` int(2) DEFAULT NULL,
  `upper_downswing_rotation_note` text,
  `upper_downswing_spine` int(2) DEFAULT NULL,
  `upper_downswing_spine_note` text,
  `upper_downswing_head` int(2) DEFAULT NULL,
  `upper_downswing_head_note` text,
  `upper_impact_rotation` int(2) DEFAULT NULL,
  `upper_impact_rotation_note` text,
  `upper_impact_spine` int(2) DEFAULT NULL,
  `upper_impact_spine_note` text,
  `upper_impact_head` int(2) DEFAULT NULL,
  `upper_impact_head_note` text,
  `upper_through_rotation` int(2) DEFAULT NULL,
  `upper_through_rotation_note` text,
  `upper_through_spine` int(2) DEFAULT NULL,
  `upper_through_spine_note` text,
  `upper_through_head` int(2) DEFAULT NULL,
  `upper_through_head_note` text,
  `upper_finish_rotation` int(2) DEFAULT NULL,
  `upper_finish_rotation_note` text,
  `upper_finish_spine` int(2) DEFAULT NULL,
  `upper_finish_spine_note` text,
  `upper_finish_head` int(2) DEFAULT NULL,
  `upper_finish_head_note` text,
  `upper_halfway_back_triangle` int(2) DEFAULT NULL,
  `upper_halfway_back_triangle_note` text,
  `upper_halfway_back_arms` int(2) DEFAULT NULL,
  `upper_halfway_back_arms_note` text,
  `upper_swing_triangle` int(2) DEFAULT NULL,
  `upper_swing_triangle_note` text,
  `upper_swing_arms` int(2) DEFAULT NULL,
  `upper_swing_arms_note` text,
  `upper_at_top_triangle` int(2) DEFAULT NULL,
  `upper_at_top_triangle_note` text,
  `upper_at_top_arms` int(2) DEFAULT NULL,
  `upper_at_top_arms_note` text,
  `upper_downswing_triangle` int(2) DEFAULT NULL,
  `upper_downswing_triangle_note` text,
  `upper_downswing_arms` int(2) DEFAULT NULL,
  `upper_downswing_arms_note` text,
  `upper_impact_triangle` int(2) DEFAULT NULL,
  `upper_impact_triangle_note` text,
  `upper_impact_arms` int(2) DEFAULT NULL,
  `upper_impact_arms_note` text,
  `upper_through_triangle` int(2) DEFAULT NULL,
  `upper_through_triangle_note` text,
  `upper_through_arms` int(2) DEFAULT NULL,
  `upper_through_arms_note` text,
  `upper_finish_triangle` int(2) DEFAULT NULL,
  `upper_finish_triangle_note` text,
  `upper_finish_arms` int(2) DEFAULT NULL,
  `upper_finish_arms_note` text,
  `upper_halfway_back_club_shaft` int(2) DEFAULT NULL,
  `upper_halfway_back_club_shaft_note` text,
  `upper_swing_club_shaft` int(2) DEFAULT NULL,
  `upper_swing_club_shaft_note` text,
  `upper_at_top_club_shaft` int(2) DEFAULT NULL,
  `upper_at_top_club_shaft_note` text,
  `upper_downswing_club_shaft` int(2) DEFAULT NULL,
  `upper_downswing_club_shaft_note` text,
  `upper_impact_club_shaft` int(2) DEFAULT NULL,
  `upper_impact_club_shaft_note` text,
  `upper_through_club_shaft` int(2) DEFAULT NULL,
  `upper_through_club_shaft_note` text,
  `upper_finish_club_shaft` int(2) DEFAULT NULL,
  `upper_finish_club_shaft_note` text,
  `upper_halfway_back_club_face` int(2) DEFAULT NULL,
  `upper_halfway_back_club_face_note` text,
  `upper_swing_club_face` int(2) DEFAULT NULL,
  `upper_swing_club_face_note` text,
  `upper_at_top_club_face` int(2) DEFAULT NULL,
  `upper_at_top_club_face_note` text,
  `upper_downswing_club_face` int(2) DEFAULT NULL,
  `upper_downswing_club_face_note` text,
  `upper_impact_club_face` int(2) DEFAULT NULL,
  `upper_impact_club_face_note` text,
  `upper_through_club_face` int(2) DEFAULT NULL,
  `upper_through_club_face_note` text,
  `upper_finish_club_face` int(2) DEFAULT NULL,
  `upper_finish_club_face_note` text,
  `wrist_cock_swing` int(2) DEFAULT NULL,
  `wrist_cock_swing_note` text,
  `wrist_cock_at_top` int(2) DEFAULT NULL,
  `wrist_cock_at_top_note` text,
  `sequence_downswing_upper_body` int(2) DEFAULT NULL,
  `sequence_downswing_upper_body_note` text,
  `sequence_downswing_lower_body` int(2) DEFAULT NULL,
  `sequence_downswing_lower_body_note` text,
  `club_position_impact_hand_rotation` int(2) DEFAULT NULL,
  `club_position_impact_hand_rotation_note` text,
  `club_position_impact_hand_location` int(2) DEFAULT NULL,
  `club_position_impact_hand_location_note` text,
  `club_position_impact_tempo_backswing` int(2) DEFAULT NULL,
  `club_position_impact_tempo_backswing_note` text,
  `club_position_impact_tempo_downswing` int(2) DEFAULT NULL,
  `club_position_impact_tempo_downswing_note` text,
  `club_position_impact_lag` int(2) DEFAULT NULL,
  `club_position_impact_lag_note` text,
  `scoring_putting` int(2) DEFAULT NULL,
  `scoring_putting_note` text,
  `scoring_chipping` int(2) DEFAULT NULL,
  `scoring_chipping_note` text,
  `scoring_pitching` int(2) DEFAULT NULL,
  `scoring_pitching_note` text,
  `scoring_distance_wedge` int(2) DEFAULT NULL,
  `scoring_distance_wedge_note` text,
  `scoring_sand_play` int(2) DEFAULT NULL,
  `scoring_sand_play_note` text,
  `course_management_paying_style` int(2) DEFAULT NULL,
  `course_management_paying_style_note` text,
  `course_management_mind_set` int(2) DEFAULT NULL,
  `course_management_mind_set_note` text,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `lesson_id` (`package_id`),
  KEY `created_by` (`created_by`),
  KEY `created_for` (`created_for`),
  KEY `package_id` (`package_id`),
  KEY `created_for_2` (`created_for`),
  KEY `created_by_2` (`created_by`),
  KEY `appointment_id` (`appointment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `billings`
--

CREATE TABLE IF NOT EXISTS `billings` (
  `id` int(11) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `uuid` varchar(40) DEFAULT NULL,
  `is_for_create_account` tinyint(1) NOT NULL DEFAULT '0',
  `recurring_billing` int(1) NOT NULL DEFAULT '0' COMMENT 'recurring billing: 0 = No, 1 = Yes',
  `user_id` int(11) unsigned zerofill NOT NULL,
  `card_id` int(6) DEFAULT NULL,
  `subtotal` decimal(8,2) NOT NULL,
  `tax` decimal(4,2) NOT NULL,
  `amount` decimal(8,2) DEFAULT NULL,
  `payment_final_status` int(1) DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  `payment_response_code` int(2) DEFAULT NULL,
  `payment_response_reason_code` int(2) DEFAULT NULL,
  `payment_response_reason_subcode` int(2) DEFAULT NULL,
  `payment_response_reason_text` varchar(50) DEFAULT NULL,
  `payment_avs_response` varchar(25) DEFAULT NULL,
  `payment_authorization_code` varchar(25) DEFAULT NULL,
  `payment_invoice_no` varchar(25) DEFAULT NULL,
  `payment_transaction_id` varchar(20) DEFAULT NULL,
  `payment_transaction_type` varchar(25) DEFAULT NULL,
  `payment_account_number` varchar(20) DEFAULT NULL,
  `payment_card_type` int(40) DEFAULT NULL,
  `payment_response_text_full` int(80) DEFAULT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `card_id` (`card_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=150 ;

-- --------------------------------------------------------

--
-- Table structure for table `billings_packages`
--

CREATE TABLE IF NOT EXISTS `billings_packages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `billing_id` int(11) unsigned zerofill NOT NULL,
  `package_id` int(11) unsigned zerofill NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `billing_id` (`billing_id`),
  KEY `package_id` (`package_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

--
-- Table structure for table `billings_periods`
--

CREATE TABLE IF NOT EXISTS `billings_periods` (
  `id` int(11) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned zerofill NOT NULL,
  `amount` decimal(4,2) NOT NULL,
  `payment_date` datetime NOT NULL,
  `notifying_date` datetime NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `get_notified` int(1) NOT NULL DEFAULT '0' COMMENT 'get_notified: 0 = No, 1 = Yes',
  `payment_status` int(1) NOT NULL DEFAULT '0' COMMENT 'payment_status: 0 =Due, 1 = Payment Failure, 2 = Expired Date, 8 = in queue to payment, 9 = Paid',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=18 ;

-- --------------------------------------------------------

--
-- Table structure for table `block_times`
--

CREATE TABLE IF NOT EXISTS `block_times` (
  `id` int(11) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned zerofill NOT NULL,
  `start` datetime NOT NULL,
  `end` datetime NOT NULL,
  `title` varchar(128) DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL,
  `status` int(1) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `cards`
--

CREATE TABLE IF NOT EXISTS `cards` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned zerofill NOT NULL COMMENT 'Card Owner',
  `uuid` varchar(60) NOT NULL,
  `name` varchar(60) DEFAULT NULL COMMENT 'Name for saving the card',
  `identifier` varchar(20) DEFAULT 'xxxxxxxxxxx0000' COMMENT 'last 4 digits of the card',
  `authorise_dot_net_customer_profile_id` int(11) DEFAULT NULL COMMENT 'AuthorizeNet customer ID for this card',
  `authorise_dot_net_payment_profile_id` int(11) DEFAULT NULL COMMENT 'Authorize Net Payment ID for this card',
  `authorise_dot_net_shipping_id` int(11) DEFAULT NULL COMMENT 'AuthorizeNet Shipping ID for this card',
  `access_to_use_instructor` int(1) NOT NULL DEFAULT '0' COMMENT 'access_to_use_instructor: 0 = No, 1 = Yes',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `customer_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=21 ;

-- --------------------------------------------------------

--
-- Table structure for table `configs`
--

CREATE TABLE IF NOT EXISTS `configs` (
  `id` int(11) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned zerofill NOT NULL,
  `key` varchar(100) NOT NULL,
  `value` varchar(100) NOT NULL,
  `created` double NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE IF NOT EXISTS `contacts` (
  `id` int(11) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned zerofill NOT NULL,
  `first_name` varchar(25) DEFAULT NULL,
  `last_name` varchar(25) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `phone` varchar(16) DEFAULT NULL,
  `account_type` int(1) DEFAULT NULL COMMENT '1 = Student, 2 = Customer, 3 = Lead/Prospect',
  `status` int(1) NOT NULL DEFAULT '1' COMMENT '1 = New, 9 = deleted',
  `member_type` int(1) NOT NULL DEFAULT '1' COMMENT '1 = Non Member',
  `source` varchar(100) DEFAULT NULL,
  `source_id` int(2) unsigned zerofill DEFAULT NULL,
  `is_custom_source` int(1) NOT NULL DEFAULT '0' COMMENT '1:Yes, 0: No, 2 = Imported',
  `follow_up_date` varchar(20) NOT NULL,
  `rating` int(1) NOT NULL DEFAULT '0',
  `comments` text,
  `modified` datetime NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `source_id` (`source_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Table structure for table `count_lessons`
--

CREATE TABLE IF NOT EXISTS `count_lessons` (
  `id` int(11) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned zerofill NOT NULL COMMENT 'Whom will purchased packages',
  `purchased_lesson_count` int(5) NOT NULL,
  `appointed_lesson_count` int(5) NOT NULL,
  `lesson_left` int(5) NOT NULL,
  `modified` datetime NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Table structure for table `emails_events`
--

CREATE TABLE IF NOT EXISTS `emails_events` (
  `id` int(2) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `field` varchar(50) NOT NULL,
  `value` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'default value 1',
  `created_by` int(11) unsigned zerofill DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `enable_for_student` tinyint(1) NOT NULL DEFAULT '1',
  `is_mandatory` tinyint(1) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `created_by` (`created_by`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

-- --------------------------------------------------------

--
-- Table structure for table `emails_events_settings`
--

CREATE TABLE IF NOT EXISTS `emails_events_settings` (
  `id` int(11) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `emails_event_id` int(2) unsigned zerofill NOT NULL,
  `user_id` int(11) unsigned zerofill NOT NULL,
  `value` tinyint(1) NOT NULL DEFAULT '1',
  `enable_for_student` tinyint(1) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `emails_event` (`emails_event_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=248 ;

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE IF NOT EXISTS `events` (
  `id` int(11) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `uuid` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_by` int(11) unsigned zerofill NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `location` text COLLATE utf8_unicode_ci,
  `contact` text COLLATE utf8_unicode_ci,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `start` datetime DEFAULT NULL,
  `end` datetime DEFAULT NULL,
  `is_appointed` int(1) NOT NULL DEFAULT '1' COMMENT 'is_appointed: 1 = no, 2 = yes',
  PRIMARY KEY (`id`),
  KEY `idx_start` (`start`),
  KEY `idx_end` (`end`),
  KEY `created_by` (`created_by`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

-- --------------------------------------------------------

--
-- Table structure for table `group_lessons`
--

CREATE TABLE IF NOT EXISTS `group_lessons` (
  `id` int(11) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned zerofill NOT NULL COMMENT 'Who  is creating lesson group',
  `title` varchar(120) NOT NULL,
  `description` varchar(200) DEFAULT NULL,
  `price` varchar(5) DEFAULT NULL,
  `start` datetime NOT NULL,
  `end` datetime NOT NULL,
  `student_limit` int(3) DEFAULT NULL COMMENT 'if student_limit equal null then it means unlimited',
  `filled_up_student` int(3) NOT NULL DEFAULT '0',
  `status` int(1) NOT NULL DEFAULT '1',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `group_lessons_students`
--

CREATE TABLE IF NOT EXISTS `group_lessons_students` (
  `id` int(11) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `group_lesson_id` int(11) unsigned zerofill NOT NULL,
  `user_id` int(11) unsigned zerofill NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1' COMMENT 'Status: 1 = active, 2 = inactive',
  `payment_status` int(1) NOT NULL DEFAULT '2' COMMENT 'payment_status: 1 = Paid, 2 = Unpaid',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `group_lesson_id` (`group_lesson_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `notes`
--

CREATE TABLE IF NOT EXISTS `notes` (
  `id` int(11) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned zerofill DEFAULT NULL,
  `contact_id` int(11) unsigned zerofill DEFAULT NULL,
  `created_by` int(11) unsigned zerofill NOT NULL,
  `note` text,
  `note_date` datetime DEFAULT NULL,
  `rating` int(2) DEFAULT '0',
  `status` int(1) NOT NULL DEFAULT '1',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `contact_id` (`contact_id`),
  KEY `created_by` (`created_by`),
  KEY `created_by_2` (`created_by`),
  KEY `contact_id_2` (`contact_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

--
-- Table structure for table `packages`
--

CREATE TABLE IF NOT EXISTS `packages` (
  `id` int(11) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `uuid` varchar(100) NOT NULL,
  `created_by` int(11) unsigned zerofill DEFAULT NULL,
  `name` varchar(100) NOT NULL,
  `lesson` varchar(5) DEFAULT NULL,
  `description` text NOT NULL,
  `price` decimal(8,2) DEFAULT NULL,
  `tax_percentage` varchar(5) NOT NULL DEFAULT '0',
  `tax` decimal(4,2) DEFAULT NULL,
  `total` decimal(8,2) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `expiration_days` int(4) DEFAULT NULL,
  `is_default` tinyint(4) NOT NULL,
  `pay_at_facility` int(1) NOT NULL DEFAULT '0' COMMENT 'pay_at_facility: < 0 = No, 1 = Yes>',
  `modified` datetime NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `created_by` (`created_by`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=31 ;

-- --------------------------------------------------------

--
-- Table structure for table `packages_orders`
--

CREATE TABLE IF NOT EXISTS `packages_orders` (
  `id` int(11) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned zerofill NOT NULL,
  `package_id` int(11) unsigned zerofill NOT NULL,
  `order` int(3) NOT NULL,
  `creatred` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `package_id` (`package_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=90 ;

-- --------------------------------------------------------

--
-- Table structure for table `packages_users`
--

CREATE TABLE IF NOT EXISTS `packages_users` (
  `id` int(11) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `instructor_id` int(11) unsigned zerofill DEFAULT NULL,
  `created_for` int(11) unsigned zerofill NOT NULL,
  `package_id` int(11) unsigned zerofill NOT NULL,
  `total_lesson` int(3) DEFAULT NULL,
  `count_appointment` int(3) NOT NULL DEFAULT '0',
  `count_benchmark` int(3) NOT NULL,
  `available` int(2) NOT NULL,
  `lesson_exceed` int(3) DEFAULT NULL,
  `payment_status` int(1) NOT NULL DEFAULT '0' COMMENT '0: unpaid, 1 = active, 2: partial paid',
  `expiration_date` datetime NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1' COMMENT 'status: 1 = running, 2 = completedf',
  `modified` datetime NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `created_for` (`created_for`),
  KEY `package_id` (`package_id`),
  KEY `instructor_id` (`instructor_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

--
-- Table structure for table `profiles`
--

CREATE TABLE IF NOT EXISTS `profiles` (
  `id` int(11) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned zerofill NOT NULL,
  `instructor_id` int(11) unsigned zerofill DEFAULT NULL,
  `first_name` varchar(25) DEFAULT NULL,
  `last_name` varchar(25) DEFAULT NULL,
  `profile_pic_url` varchar(120) DEFAULT NULL,
  `phone` varchar(16) DEFAULT NULL,
  `fax` varchar(16) DEFAULT NULL,
  `primary_email` varchar(50) DEFAULT NULL,
  `secondary_email` varchar(50) DEFAULT NULL,
  `city` varchar(25) DEFAULT NULL,
  `state` varchar(25) DEFAULT NULL,
  `postal_code` int(5) DEFAULT NULL,
  `street_1` varchar(80) DEFAULT NULL,
  `street_2` varchar(80) DEFAULT NULL,
  `billing_street_1` varchar(80) DEFAULT NULL,
  `billing_street_2` varchar(80) DEFAULT NULL,
  `billing_city` varchar(25) DEFAULT NULL,
  `billing_state` varchar(25) DEFAULT NULL,
  `billing_postal_code` varchar(5) DEFAULT NULL,
  `billing_country` varchar(25) DEFAULT NULL,
  `birthday` varchar(20) DEFAULT NULL,
  `source` varchar(100) NOT NULL,
  `source_id` int(2) unsigned zerofill DEFAULT NULL,
  `is_custom_source` int(1) NOT NULL DEFAULT '0' COMMENT '1: Yes, 0: No',
  `biography` text,
  `history_and_goal` varchar(255) DEFAULT NULL,
  `best_day_and_time` varchar(255) DEFAULT NULL,
  `frequently_come` int(3) DEFAULT NULL,
  `handicap_starting` int(3) DEFAULT NULL,
  `handicap_current` int(3) DEFAULT NULL,
  `handicap_change` int(3) DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`),
  KEY `id` (`id`),
  KEY `user_id_2` (`user_id`),
  KEY `instructor_id` (`instructor_id`),
  KEY `source_id` (`source_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=40 ;

-- --------------------------------------------------------

--
-- Table structure for table `range_waitings`
--

CREATE TABLE IF NOT EXISTS `range_waitings` (
  `id` int(11) NOT NULL,
  `user_id` int(11) unsigned zerofill NOT NULL,
  `start_time` datetime NOT NULL,
  `end_time` datetime NOT NULL,
  `status` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '1 =Active ; 2 = Inactive',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sources`
--

CREATE TABLE IF NOT EXISTS `sources` (
  `id` int(2) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  `slug` varchar(80) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1' COMMENT '1: Active, 2: No',
  `created_by` int(11) DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

-- --------------------------------------------------------

--
-- Table structure for table `tcs_users`
--

CREATE TABLE IF NOT EXISTS `tcs_users` (
  `id` int(11) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `tc_id` int(11) unsigned zerofill NOT NULL,
  `user_id` int(11) unsigned zerofill NOT NULL,
  `status` int(1) unsigned zerofill NOT NULL DEFAULT '1' COMMENT 'Status: 1 = Active, 2 = Inactive',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `tc_id` (`tc_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `uuid` varchar(100) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(120) NOT NULL COMMENT 'Password: 123456 <=> a7656f312a1df3a34fd2c847b743f3e187eca061',
  `status` int(1) NOT NULL DEFAULT '1' COMMENT 'Status: 1 = Active, 2: Inactive, 3: Blocked, 4: Banned, 9 = Deleted',
  `is_slipping_away` tinyint(1) NOT NULL DEFAULT '0',
  `role` int(1) NOT NULL COMMENT 'Role: 1 = Admin, 2 = Student, 3 = Instructor, 4 = Training Center',
  `rating` int(1) DEFAULT '0',
  `email_verification` int(1) NOT NULL DEFAULT '2' COMMENT 'Verification: 1: Completed, 2: Pending, 3: Expired',
  `reset_val_for_password` varchar(50) DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `modified` datetime NOT NULL,
  `created` datetime NOT NULL,
  `packages_users_count` int(11) NOT NULL,
  `facebook_user_id` bigint(20) unsigned NOT NULL,
  `twitter_user_id` bigint(20) DEFAULT NULL,
  `social_pic` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=41 ;

-- --------------------------------------------------------

--
-- Table structure for table `waitings`
--

CREATE TABLE IF NOT EXISTS `waitings` (
  `id` int(11) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `uuid` varchar(80) NOT NULL,
  `appointment_id` int(11) unsigned zerofill NOT NULL,
  `instructor_id` int(11) unsigned zerofill NOT NULL,
  `appointment_by` int(11) unsigned zerofill NOT NULL,
  `package_id` int(11) unsigned zerofill DEFAULT NULL,
  `lesson_no` int(3) DEFAULT NULL,
  `start` datetime NOT NULL,
  `end` datetime NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1' COMMENT 'status: 1 = waiting, 2 = awaiting for confirmation, 3 = convert into appointment, 4 = expired, 5 = cancelled, 6 = invalid',
  `is_send_notification` int(1) NOT NULL DEFAULT '0',
  `notification_code` varchar(80) DEFAULT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `appointment_id` (`appointment_id`),
  KEY `instructor_id` (`instructor_id`),
  KEY `appointment_by` (`appointment_by`),
  KEY `package_id` (`package_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `appointments`
--
ALTER TABLE `appointments`
  ADD CONSTRAINT `appointments_ibfk_1` FOREIGN KEY (`appointment_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `appointments_ibfk_2` FOREIGN KEY (`instructor_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `appointments_ibfk_3` FOREIGN KEY (`package_id`) REFERENCES `packages` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `appointments_ibfk_4` FOREIGN KEY (`operated_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `appointments_ibfk_5` FOREIGN KEY (`group_lesson_id`) REFERENCES `group_lessons` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `benchmarks`
--
ALTER TABLE `benchmarks`
  ADD CONSTRAINT `benchmarks_ibfk_1` FOREIGN KEY (`package_id`) REFERENCES `packages` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `benchmarks_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `benchmarks_ibfk_3` FOREIGN KEY (`created_for`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `benchmarks_ibfk_4` FOREIGN KEY (`appointment_id`) REFERENCES `appointments` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `billings`
--
ALTER TABLE `billings`
  ADD CONSTRAINT `billings_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `billings_ibfk_2` FOREIGN KEY (`card_id`) REFERENCES `cards` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `billings_packages`
--
ALTER TABLE `billings_packages`
  ADD CONSTRAINT `billings_packages_ibfk_1` FOREIGN KEY (`billing_id`) REFERENCES `billings` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `billings_packages_ibfk_2` FOREIGN KEY (`package_id`) REFERENCES `packages` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `block_times`
--
ALTER TABLE `block_times`
  ADD CONSTRAINT `block_times_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `cards`
--
ALTER TABLE `cards`
  ADD CONSTRAINT `cards_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `configs`
--
ALTER TABLE `configs`
  ADD CONSTRAINT `configs_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `contacts`
--
ALTER TABLE `contacts`
  ADD CONSTRAINT `contacts_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `contacts_ibfk_2` FOREIGN KEY (`source_id`) REFERENCES `sources` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `count_lessons`
--
ALTER TABLE `count_lessons`
  ADD CONSTRAINT `count_lessons_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `emails_events`
--
ALTER TABLE `emails_events`
  ADD CONSTRAINT `emails_events_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `emails_events_settings`
--
ALTER TABLE `emails_events_settings`
  ADD CONSTRAINT `emails_events_settings_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `emails_events_settings_ibfk_3` FOREIGN KEY (`emails_event_id`) REFERENCES `emails_events` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `events`
--
ALTER TABLE `events`
  ADD CONSTRAINT `events_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `group_lessons`
--
ALTER TABLE `group_lessons`
  ADD CONSTRAINT `group_lessons_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `group_lessons_students`
--
ALTER TABLE `group_lessons_students`
  ADD CONSTRAINT `group_lessons_students_ibfk_1` FOREIGN KEY (`group_lesson_id`) REFERENCES `group_lessons` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `group_lessons_students_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `notes`
--
ALTER TABLE `notes`
  ADD CONSTRAINT `notes_ibfk_1` FOREIGN KEY (`contact_id`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `notes_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `notes_ibfk_3` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `packages`
--
ALTER TABLE `packages`
  ADD CONSTRAINT `packages_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `packages_orders`
--
ALTER TABLE `packages_orders`
  ADD CONSTRAINT `packages_orders_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `packages_orders_ibfk_2` FOREIGN KEY (`package_id`) REFERENCES `packages` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `packages_users`
--
ALTER TABLE `packages_users`
  ADD CONSTRAINT `packages_users_ibfk_1` FOREIGN KEY (`created_for`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `packages_users_ibfk_4` FOREIGN KEY (`package_id`) REFERENCES `packages` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `packages_users_ibfk_5` FOREIGN KEY (`instructor_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `profiles`
--
ALTER TABLE `profiles`
  ADD CONSTRAINT `profiles_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `profiles_ibfk_2` FOREIGN KEY (`instructor_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `profiles_ibfk_3` FOREIGN KEY (`source_id`) REFERENCES `sources` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `range_waitings`
--
ALTER TABLE `range_waitings`
  ADD CONSTRAINT `range_waitings_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `tcs_users`
--
ALTER TABLE `tcs_users`
  ADD CONSTRAINT `tcs_users_ibfk_1` FOREIGN KEY (`tc_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `tcs_users_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `waitings`
--
ALTER TABLE `waitings`
  ADD CONSTRAINT `waitings_ibfk_1` FOREIGN KEY (`appointment_id`) REFERENCES `appointments` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `waitings_ibfk_2` FOREIGN KEY (`instructor_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `waitings_ibfk_3` FOREIGN KEY (`appointment_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `waitings_ibfk_4` FOREIGN KEY (`package_id`) REFERENCES `packages` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
