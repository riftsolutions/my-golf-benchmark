-- Rana (DEC 19, 2015) --
ALTER TABLE  `appointments` ADD  `title` VARCHAR( 128 ) NULL AFTER  `lesson_sum` ;

-- Emon (DEC 18 , 2015) --
CREATE TABLE `waiting_queues` (
  `id` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `appointment_id` int(11) unsigned zerofill NOT NULL,
  `waiting_id` int(11) unsigned zerofill NOT NULL,
  `user_id` int(11) unsigned zerofill NOT NULL,
  `queue_time` datetime NOT NULL,
  `response_code` varchar(32) NOT NULL,
  `validity_period` datetime NOT NULL,
  `status` tinyint(1) NOT NULL COMMENT '1 = Active; 2 = Inactive',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  FOREIGN KEY (`appointment_id`) REFERENCES `appointments` (`id`),
  FOREIGN KEY (`waiting_id`) REFERENCES `waitings` (`id`),
  FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
);

ALTER TABLE `packages`
CHANGE `lesson` `lesson` smallint NULL AFTER `name`;
ALTER TABLE `packages`
CHANGE `lesson` `lesson` smallint(6) NOT NULL DEFAULT '0' AFTER `name`;

-- Rana (DEC 20, 2015) --
ALTER TABLE  `group_lessons_students` DROP FOREIGN KEY  `group_lessons_students_ibfk_1` ;

ALTER TABLE  `group_lessons_students` ADD CONSTRAINT  `group_lessons_students_ibfk_1` FOREIGN KEY (  `group_lesson_id` ) REFERENCES  `golf-swing-prescription`.`group_lessons` (
`id`
) ON DELETE CASCADE ON UPDATE CASCADE ;

-- Emon (DEC 21 , 2015) --
--
-- Table structure for table `waiting_queues`
--

CREATE TABLE IF NOT EXISTS `waiting_queues` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `appointment_id` int(11) unsigned zerofill NOT NULL,
  `waiting_id` int(11) unsigned zerofill NOT NULL,
  `user_id` int(11) unsigned zerofill NOT NULL,
  `queue_time` datetime NOT NULL,
  `response_code` varchar(32) NOT NULL,
  `validity_period` datetime NOT NULL,
  `status` int(3) NOT NULL DEFAULT '0' COMMENT '0 = Default; 1 = Got Email; 2 = Not respond; 3 = Not Available',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `appointment_id` (`appointment_id`),
  KEY `waiting_id` (`waiting_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `waiting_queues`
--
ALTER TABLE `waiting_queues`
  ADD CONSTRAINT `waiting_queues_ibfk_1` FOREIGN KEY (`appointment_id`) REFERENCES `appointments` (`id`),
  ADD CONSTRAINT `waiting_queues_ibfk_2` FOREIGN KEY (`waiting_id`) REFERENCES `waitings` (`id`),
  ADD CONSTRAINT `waiting_queues_ibfk_3` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--

ALTER TABLE `waiting_queues`
CHANGE `status` `status` int(1) NOT NULL DEFAULT '0' COMMENT '0 = Default; 1 = Got Email; 2 = Not respond; 3 = Not Available' AFTER `validity_period`;

ALTER TABLE `waiting_queues` CHANGE `id` `id` INT( 11 ) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT ;

-- Emon (DEC 23 , 2015) --


ALTER TABLE `waiting_queues` CHANGE `status` `status` INT( 1 ) NOT NULL DEFAULT '0' COMMENT '0 = Default; 1 = Got Email; 2 = Not respond; 3 = Not Available; 4=Confirmed';