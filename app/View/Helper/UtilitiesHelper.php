<?php

class UtilitiesHelper extends Helper
{
    /**
     * @var string
     *
     * Name of the UtilitiesHelper
     */
    public $name = 'Utilities';



    /**
     * @return array
     *
     * Getting the hours for the lesson.
     */
    public function getHours()
    {
        $now = '07:00';
        $counter = 0;
        $min = 0;
        $hours = array();
        while($counter < 96)
        {
            $hour = date('H:i A', strtotime('+'.$min.' min', strtotime($now)));
            $hours[$hour] = $hour;
            $min = $min + 15;
            $counter++;
        }

        return $hours;
    }

    /**
     * @param $default
     * @return string
     *
     */
    public function getFromHours($default)
    {
        $hours = $this->getHours();
        $options = '<option value="" selected="selected">'.ucwords($default).'</option>';
        foreach($hours as $hour => $key){
            $options .= '<option value="'.$hour.'">'.$hour.'</option>';
        }

        return $options;
    }

    /**
     * @param $status
     *
     * Manage the lesson status
     */
    public function manageLessonStatus($status)
    {
        switch ($status){
            case 1:{
                echo '<label class="label label-gray">new</label>';
                       break;
            }
            case 2:{
                echo '<label class="label label-orange">waiting</label>';
                break;
            }
            case 3:{
                echo '<label class="label label-light-green">upcoming</label>';
                break;
            }
            case 4:{
                echo '<label class="label label-light-red">invalid</label>';
                break;
            }
            case 5:{
                echo '<label class="label label-danger">expired</label>';
                break;
            }
            case 6:{
                echo '<label class="label label-success">completed</label>';
                break;
            }
            default:
                echo '<label class="label label-default">N/A</label>';
        }
    }

    /**
     * @param $status
     *
     * Getting the lesson status button according to the lesson status.
     */
    public function lessonStatusButton($status)
    {
        switch ($status){
            case 1:{
                echo '<button class="pull-right status-button status-gray">Current Status: New List</button>';
                break;
            }
            case 2:{
                echo '<button class="pull-right status-button status-orange">Current Status: Waiting List</button>';
                break;
            }
            case 3:{
                echo '<button class="pull-right status-button status-info">Current Status: Upcoming List</button>';
                break;
            }
            case 4:{
                echo '<button class="pull-right status-button status-red">Current Status: Invalid List</button>';
                break;
            }
            case 5:{
                echo '<button class="pull-right status-button status-danger">Current Status: Expired List</button>';
                break;
            }
            case 6:{
                echo '<button class="pull-right status-button status-green">Current Status: Completed List</button>';
                break;
            }
            default:
                echo '<button class="pull-right status-button status-gray">Current Status: N/A</button>';
        }
    }

    /**
     * @param $status
     *
     * Getting the lesson status button according to the lesson status.
     */
    public function packageStatusButton($status)
    {
        switch ($status){
            case 0:{
                echo '<button class="status-button status-gray">Payment Status: Unpaid</button>';
                break;
            }
            case 1:{
                echo '<button class="status-button status-green">Payment Status: Paid</button>';
                break;
            }
            case 2:{
                echo '<button class="tatus-button status-orange">Payment Status: Partial Paid</button>';
                break;
            }
            default:
                echo '<button class="status-button status-gray">Payment Status: N/A</button>';
        }
    }

    public function appointmentStatusButton($status)
    {
        switch ($status){
            case 1:{
                echo '<button class="status-button status-gray">Appointment Status: Pending</button>';
                break;
            }
            case 2:{
                echo '<button class="status-button status-success">Appointment Status: Confirm/Upcoming</button>';
                break;
            }
            case 3:{
                echo '<button class="status-button status-danger">Appointment Status: Cancelled</button>';
                break;
            }
            case 4:{
                echo '<button class="status-button status-info">Appointment Status: Complete</button>';
                break;
            }
            case 5:{
                echo '<button class="status-button status-orange">Appointment Status: Expired</button>';
                break;
            }
            default:
                echo '<button class="status-button status-gray">Payment Status: N/A</button>';
        }
    }

    /**
     * @param $status
     */
    public function managePackageStatus($status)
    {
        switch ($status){
            case 0:{
                echo '<label class="label label-gray">unpaid</label>';
                break;
            }
            case 1:{
                echo '<label class="label label-success">paid</label>';
                break;
            }
            case 2:{
                echo '<label class="label label-orange">partially paid</label>';
                break;
            }
            default:
                echo '<label class="label label-default">N/A</label>';
        }
    }

    /**
     * @param $status
     * @return string
     *
     * Getting user status.
     */
    public function getUserStatus($status)
    {
        switch ($status){
            case 1:{
                return '<label class="label label-light-green">active</label>';
                break;
            }
            case 2:{
                return '<label class="label label-gray">inactive</label>';
                break;
            }
            case 3:{
                return '<label class="label label-orange">blocked</label>';
                break;
            }
            case 4:{
                return '<label class="label label-danger">banned</label>';
                break;
            }
            case 9:{
                return '<label class="label label-danger">removed</label>';
                break;
            }
            default:
                return '<label class="label label-default">N/A</label>';
        }
    }

    /**
     * @param $events
     * @param $day
     * @return array
     *
     * Arranging previous schedules time slot.
     */
    public function setPreviousEvents($events, $day)
    {
        $previousEvents = array();
        foreach($events as $event)
        {
            $start = $event['Event']['start'];
            $end = $event['Event']['end'];
            $currentDay = strtolower(date('D', strtotime($start)));

            if($currentDay == $day){
                $previousEvents[] = array(
                    'start' => date('h:i A', strtotime($start)),
                    'end' => date('h:i A', strtotime($end)),
                );
            }
        }

        $removeDuplicatedEvent = array_map("unserialize", array_unique(array_map("serialize", $previousEvents)));
        if(sizeof($previousEvents) > sizeof($removeDuplicatedEvent))
        {
             $removeDuplicatedEvent['repeated'] = true;
        }

        return $removeDuplicatedEvent;
    }

    /**
     * @return array
     */
    public function getStateList()
    {
        $stateList = array(
            'Alabama' => 'Alabama',
            'Alaska' => 'Alaska',
            'Arizona' => 'Arizona',
            'Arkansas' => 'Arkansas',
            'California' => 'California',
            'Colorado' => 'Colorado',
            'Connecticut' => 'Connecticut',
            'Delaware' => 'Delaware',
            'District Of Columbia' => 'District Of Columbia',
            'Florida' => 'Florida',
            'Georgia' => 'Georgia',
            'Hawaii' => 'Hawaii',
            'Idaho' => 'Idaho',
            'Illinois' => 'Illinois',
            'Indiana' => 'Indiana',
            'Iowa' => 'Iowa',
            'Kansas' => 'Kansas',
            'Kentucky' => 'Kentucky',
            'Louisiana' => 'Louisiana',
            'Maine' => 'Maine',
            'Maryland' => 'Maryland',
            'Massachusetts' => 'Massachusetts',
            'Michigan' => 'Michigan',
            'Minnesota' => 'Minnesota',
            'Mississippi' => 'Mississippi',
            'Missouri' => 'Missouri',
            'Montana' => 'Montana',
            'Nebraska' => 'Nebraska',
            'Nevada' => 'Nevada',
            'New Hampshire' => 'New Hampshire',
            'New Jersey' => 'New Jersey',
            'New Mexico' => 'New Mexico',
            'New York' => 'New York',
            'North Carolina' => 'North Carolina',
            'North Dakota' => 'North Dakota',
            'Ohio' => 'Ohio',
            'Oklahoma' => 'Oklahoma',
            'Oregon' => 'Oregon',
            'PALAU' => 'PALAU',
            'Pennsylvania' => 'Pennsylvania',
            'PUERTO RICO' => 'PUERTO RICO',
            'Rhode Island' => 'Rhode Island',
            'South Carolina' => 'South Carolina',
            'South Dakota' => 'South Dakota',
            'Tennessee' => 'Tennessee',
            'Texas' => 'Texas',
            'Utah' => 'Utah',
            'Vermont' => 'Vermont',
            'Virginia' => 'Virginia',
            'Washington' => 'Washington',
            'West Virginia' => 'West Virginia',
            'Wisconsin' => 'Wisconsin',
            'Wyoming' => 'Wyoming'
        );

        return $stateList;
    }

    /**
     * @return array
     */
    public function getSources()
    {
        $sourceList = array(
            'Search Engine' => 'Search Engine',
            'Social Media' => 'Social Media',
            'Email Marketing' => 'Email Marketing',
            'Mobile/Phone Call' => 'Mobile/Phone Call',
        );

        return $sourceList;
    }

    public function isBillingProfileComplete($billingProfile){
        if($billingProfile['billing_street_1'] && $billingProfile['billing_street_2'] && $billingProfile['billing_city'] && $billingProfile['billing_state'] && $billingProfile['billing_postal_code'] && $billingProfile['billing_country']){
            return true;
        }
        return false;
    }

    public function applicationBreadcrumb()
    {
        $request = $this->request->params;
        return $this->getBreadcrumb($request['controller'], $request['action'], $request['prefix'], $request['pass']);
    }

    protected function getBreadcrumb($controller, $action, $prefix, $pass)
    {
        $baseUrl = Router::url('/', true);


        if ($controller == 'dashboards') {
            $homeLink = '<a class="btn btn-default" href="' . $baseUrl . $prefix . '"> <i class="glyphicon glyphicon-home"></i> My Dashboard</a>';
        }
        else{
            $homeLink = '<a class="btn btn-default" href="' . $baseUrl . $prefix . '"> <i class="glyphicon glyphicon-home"></i> Home</a>';
        }


        if ($controller != 'dashboards') {
            $controllerName = Inflector::singularize(Inflector::humanize($controller));
            if($controllerName == 'Package'){
                $controllerName = 'Item';
            }
            $controllerLink = '<a class="btn btn-default"  href="' . $baseUrl . $prefix . '/'.$controller.'"> '.$controllerName.' </a>';
        }
        else {
            $controllerLink = null;
        }
        $actionsWithPrefix = explode(' ', str_replace('_', ' ', $action));
        $methodName = strtolower($actionsWithPrefix[1]);
        $actionName = ucfirst($actionsWithPrefix[1]);
        if($methodName != 'index'){
            if(is_array($pass)){
                $link = '/'.$methodName;
                foreach($pass as $key => $value)
                {
                    $link .= '/'.$value;
                }
            }
            else{
                $link = null;
            }

            $actionLink = '<a class="btn btn-default" href="' . $baseUrl . $prefix . '/'.$controller.''.$link.'"> '.$actionName.' </a>';
        }
        else{
            $actionLink = null;
        }

        $finalLink = array(
            'home' => $homeLink,
            'controller' => $controllerLink,
            'action' => $actionLink
        );
        return $finalLink;
    }

    /**
     *
     *
     */
    public function getColorForLesson()
    {
        $colors = array(
            array(
                'code' => '#d9534f',
                'title' => "No lessons left",
                'desc' => "Red indicates there is no more lessons left",
            ),
            array(
                'code' => 'darkorange',
                'title' => "1 Lessons Left",
                'desc' => "Orange indicates there is ONE more lesson left",
            ),
            array(
                'code' => '#9acd32',
                'title' => "more than 1 lessons left",
                'desc' => "Green indicates there is more than ONE lesson left",
            )
        );

        return $colors;
    }


    /**
     *
     *
     */
    public function getColorForSchedule()
    {
        $colors = array(
            array(
                'code' => '#d9534f',
                'title' => "No Lessons scheduled",
                'desc' => "Red indicates there are no lessons scheduled",
            ),
            array(
                'code' => '#FFCC00',
                'title' => "Lessons scheduled less than current series",
                'desc' => "Yellow indicates there are lessons scheduled less than current series",
            ),
            array(
                'code' => '#9acd32',
                'title' => "Lessons scheduled equaling series",
                'desc' => "Green indicates lessons scheduled equaling the series",
            ),
            array(
                'code' => '#b17630',
                'title' => "Lessons scheduled beyond series",
                'desc' => "Brown indicates lessons scheduled beyond series",
            )
        );

        return $colors;
    }
}