<?php
class DataTableHelper extends Helper{

    public $name = 'DataTable';

    public function getColumn($columnList, $excludeList = array())
    {
        $tableName = array_keys($columnList);
        $keys = array_keys($columnList[$tableName[0]]);


        if(is_array($excludeList)){
            $keys = array_diff($keys, $excludeList);
        }

        foreach($keys as $value){
            $this->colunmName .= '{mData:"'.$tableName[0].'.'.$value.'"},';
            $this->colunmName .= "\n\t\t";
        }
        $this->colunmName = rtrim($this->colunmName, ',');
        $this->colunmName .= '{mData:"'.$tableName[0].'.Actions"}';

        return $this->colunmName;
    }


    public function makeVisible($columnList, $visibleList, $controller, $action, $prefix = 'admin')
    {

        $tableName = array_keys($columnList);
        $keys = array_keys($columnList[$tableName[0]]);

        $visibleColumn = null;
        $visible = 'false';

        $counter = 0;
        while($counter < sizeof($keys)){

            if(in_array($keys[$counter], $visibleList)){
                $visible = 'true';
            }

            $visibleColumn .= '
            {
            "targets": [ '.$counter.' ],
            "visible": '.$visible.',
            "mRender": function(data, type, full)
                {
                    return "<a href=/'.$prefix.'/'.$controller.'/'.$action.'/" + full.'.$tableName[0].'.id + ">" + data + "</a>";
                }
            },';

            $visibleColumn .= "\n";
            $counter ++;
            $visible = 'false';
            $buttons = 'btn-theme';
        }

        $visibleColumn .= '
            {
                "targets": [ '.$counter.' ],
                "visible": '.true.',
                "mRender": function(data, type, full)
                {
                    return "<a class='.$buttons.' href=/'.$prefix.'/'.$controller.'/'.$action.'/" + full.'.$tableName[0].'.id + ">Details</a>";
                }
            },';
        $visibleColumn .= "\n";



        return $visibleColumn;
    }

    public function getTableColumn($columnList)
    {
        $tableColumn = null;

        $tableName = array_keys($columnList);
        $keys = array_keys($columnList[$tableName[0]]);

        $counter = 0;
        while($counter < sizeof($keys)){
            $name = ucwords(str_replace('_', ' ', $keys[$counter]));
            $tableColumn .= '<th>'.$name.'</th>';
            $tableColumn .= "\n";
            $counter ++;
        }

        $tableColumn .= '<th>Action</th>';
        $tableColumn .= "\n";
//var_dump($tableColumn);
        return $tableColumn;
    }

    public function showCriteria($columnList)
    {
        $tableName = array_keys($columnList);
        $keys = array_keys($columnList[$tableName[0]]);

        $counter = 0;
        $criteria = null;
        while($counter < sizeof($keys)){
            $name = ucwords(str_replace('_', ' ', $keys[$counter]));
            $criteria .= '<a class="d-btn" href="javascript:void(0);" onclick="fnShowHide('.$counter.');">'.$name.'</a> ';
            $counter ++;
        }

        return $criteria;
    }

} 