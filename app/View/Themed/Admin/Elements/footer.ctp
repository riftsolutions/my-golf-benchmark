<!----- FOOTER AREA ----->
<footer>
    <div class="footer-text">
        &copy; All Rights Reserved  •  Golf Swing Prescription
    </div>
</footer>
<!----- /FOOTER AREA ------>

<!----- Java Script Area ----->
<?php echo $this->Html->script(array('jquery.min', 'bootstrap.min', 'jquery.dataTables.min', 'jquery.dataTables.columnFilter', 'datatables', 'bootstrap-datepicker', 'bootstrap-slider', 'custom', 'underscore-min', 'calendar', 'app' ));?>
<!----- /Java Script Area ----->
<!----- Graph/Chart ----->
<script src="assets/js/jquery.knob.js"></script>
<script src="assets/js/jquery.flot.js"></script>
<script src="assets/js/jquery.flot.stack.js"></script>
<script src="assets/js/jquery.flot.resize.js"></script>
<script src="assets/js/raphael-min.js"></script>
<script src="assets/js/morris.min.js"></script>
<script src="assets/js/graph.js"></script>
<!----- /Graph/Chart ----->
<script>
    /* ------------ SINGLE BOX ------*/
    $(document).ready(function() {
        $('#uiSlider').slider().on('slide', function() {
            var value = $(this).data('slider').getValue();
            if(value > 100){
                $('#uiSliderValue').val(200 - value);
                var colorValue = (200 - value);
            }
            else{
                $('#uiSliderValue').val(value);
                var colorValue = value;
            }
            myColor = getTheColor( colorValue );
            $( '#uiSliderValue' ).css( 'background-color', myColor );
        });
        $('#uiSliderValue').val(value);
        myColor = getTheColor( value );
        $( '#uiSliderValue' ).css( 'background-color', myColor );
    });
    /* ------------ SINGLE BOX ------*/

    /* ------------ SINGLE BOX ------*/
    $(document).ready(function() {
        $('#uiSlider2').slider().on('slide', function() {
            var value = $(this).data('slider').getValue();
            if(value > 100){
                $('#uiSliderValue2').val(200 - value);
                var colorValue = (200 - value);
            }
            else{
                $('#uiSliderValue2').val(value);
                var colorValue = value;
            }
            myColor = getTheColor( colorValue );
            $( '#uiSliderValue2' ).css( 'background-color', myColor );
        });
        $('#uiSliderValue2').val(value);
        myColor = getTheColor( value );
        $( '#uiSliderValue2' ).css( 'background-color', myColor );
    });
    /* ------------ SINGLE BOX ------*/

    /* ------------ SINGLE BOX ------*/
    $(document).ready(function() {
        $('#uiSlider3').slider().on('slide', function() {
            var value = $(this).data('slider').getValue();
            if(value > 100){
                $('#uiSliderValue3').val(200 - value);
                var colorValue = (200 - value);
            }
            else{
                $('#uiSliderValue3').val(value);
                var colorValue = value;
            }
            myColor = getTheColor( colorValue );
            $( '#uiSliderValue3' ).css( 'background-color', myColor );
        });
        $('#uiSliderValue3').val(value);
        myColor = getTheColor( value );
        $( '#uiSliderValue3' ).css( 'background-color', myColor );
    });
    /* ------------ SINGLE BOX ------*/

    /* ------------ SINGLE BOX ------*/
    $(document).ready(function() {
        $('#uiSlider4').slider().on('slide', function() {
            var value = $(this).data('slider').getValue();
            if(value > 100){
                $('#uiSliderValue4').val(200 - value);
                var colorValue = (200 - value);
            }
            else{
                $('#uiSliderValue4').val(value);
                var colorValue = value;
            }
            myColor = getTheColor( colorValue );
            $( '#uiSliderValue4' ).css( 'background-color', myColor );
        });
        $('#uiSliderValue4').val(value);
        myColor = getTheColor( value );
        $( '#uiSliderValue4' ).css( 'background-color', myColor );
    });
    /* ------------ SINGLE BOX ------*/

    /* ------------ SINGLE BOX ------*/
    $(document).ready(function() {
        $('#uiSlider5').slider().on('slide', function() {
            var value = $(this).data('slider').getValue();
            if(value > 100){
                $('#uiSliderValue5').val(200 - value);
                var colorValue = (200 - value);
            }
            else{
                $('#uiSliderValue5').val(value);
                var colorValue = value;
            }
            myColor = getTheColor( colorValue );
            $( '#uiSliderValue5' ).css( 'background-color', myColor );
        });
        $('#uiSliderValue5').val(value);
        myColor = getTheColor( value );
        $( '#uiSliderValue5' ).css( 'background-color', myColor );
    });
    /* ------------ SINGLE BOX ------*/

    function getTheColor( colorVal ) {
        var theColor = "";
        if ( colorVal < 50 ) {
            myRed = 255;
            myGreen = parseInt( ( ( colorVal * 2 ) * 255 ) / 100 );
        }
        else 	{
            myRed = parseInt( ( ( 100 - colorVal ) * 2 ) * 255 / 100 );
            myGreen = 255;
        }
        theColor = "rgb(" + myRed + "," + myGreen + ",0)";
        return( theColor );
    }
</script>
</body>
</html>