<div class="col-xs-6 col-sm-2 sidebar-offcanvas" id="sidebar" role="navigation">
    <div class="row">
        <div class="sidebar">
            <ul id="menu" class="nav nav-list">
                <li>
                    <?php echo $this->Html->link('<i class="fa fa-home"></i> Dashboard', array('controller' => 'dashboards', 'action' => 'index'), array('escape' => false))?>
                </li>
                <li>
                    <?php echo $this->Html->link('<i class="fa fa-cogs"></i> Appointments', array('controller' => 'appointments', 'action' => 'index'), array('escape' => false))?>
                </li>
                <li>
                    <a href="#">
                        <i class="fa fa-user"></i>
                        My Account
                        <span class="glyphicon arrow"></span>
                    </a>
                    <ul class="nav nav-list submenu">
                        <li>
                            <?php echo $this->Html->link(
                                'Profile</a>',
                                array('controller' => 'profiles', 'action' => 'index'),
                                array('escape' => false)
                            ); ?>
                        </li>
                        <li>
                            <?php echo $this->Html->link(
                                'Update Profile</a>',
                                array('controller' => 'profiles', 'action' => 'update'),
                                array('escape' => false)
                            ); ?>
                        </li>
                        <li>
                            <?php echo $this->Html->link(
                                'Change Profile Picture</a>',
                                array('controller' => 'profiles', 'action' => 'change_photo'),
                                array('escape' => false)
                            ); ?>
                        </li>
                        <li>
                            <?php echo $this->Html->link(
                                'Account Settins</a>',
                                array('controller' => 'profiles', 'action' => 'settings'),
                                array('escape' => false)
                            ); ?>
                        </li>
                        <li>
                            <?php echo $this->Html->link(
                                'Change Password</a>',
                                array('controller' => 'profiles', 'action' => 'change_password'),
                                array('escape' => false)
                            ); ?>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#">
                        <i class="fa fa-book"></i>
                        Contact
                        <span class="glyphicon arrow"></span>
                    </a>
                    <ul class="nav nav-list submenu">
                        <li>
                            <?php echo $this->Html->link('Contact List', array('controller' => 'contacts', 'action' => 'index'), array('escape' => false))?>
                        <li>
                            <?php echo $this->Html->link('Contact Add', array('controller' => 'contacts', 'action' => 'add'), array('escape' => false))?>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#">
                        <i class="fa fa-mortar-board"></i>
                        Student
                        <span class="glyphicon arrow"></span>
                    </a>
                    <ul class="nav nav-list submenu">
                        <li><a href="student_list.html">Student List</a></li>
                        <li><a href="add_student.html">Add Student</a></li>
                        <li><a href="edit_student.html">Edit Student</a></li>
                        <li><a href="student_details.html">Student Details</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#">
                        <i class="fa fa-male"></i>
                        Instructor
                        <span class="glyphicon arrow"></span>
                    </a>
                    <ul class="nav nav-list submenu">
                        <li><a href="instructor_list.html">Instructor List</a></li>
                        <li><a href="add_instructor.html">Add Instructor</a></li>
                        <li><a href="edit_instructor.html">Edit Instructor</a></li>
                        <li><a href="instructor_details.html">Instructor Details</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#">
                        <i class="fa fa-flask"></i>
                        Lesson
                        <span class="glyphicon arrow"></span>
                    </a>
                    <ul class="nav nav-list submenu">
                        <li>
                            <?php echo $this->Html->link(
                                'Lesson List',
                                array('controller' => 'lessons', 'action' => 'list'),
                                array('escape' => false)
                            ); ?>
                        </li>
                        <li>
                            <?php echo $this->Html->link(
                                'Lesson Create',
                                array('controller' => 'lessons', 'action' => 'create'),
                                array('escape' => false)
                            ); ?>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#">
                        <i class="fa fa-cubes"></i>
                        Package
                        <span class="glyphicon arrow"></span>
                    </a>
                    <ul class="nav nav-list submenu">
                        <li><a href="package_list.html">Package List</a></li>
                        <li><a href="add_package.html">Add Package</a></li>
                        <li><a href="edit_package.html">Edit Package</a></li>
                        <li><a href="package_details.html">Package Details</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#">
                        <i class="fa fa-file-pdf-o"></i>
                        PDF
                        <span class="glyphicon arrow"></span>
                    </a>
                    <ul class="nav nav-list submenu">
                        <li><a href="pdf_list.html">PDF List</a></li>
                        <li><a href="add_pdf.html">Add PDF</a></li>
                        <li><a href="edit_pdf.html">Edit PDF</a></li>
                        <li><a href="pdf_details">PDF Details</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#">
                        <i class="fa fa-history"></i>
                        Payment History
                        <span class="glyphicon arrow"></span>
                    </a>
                    <ul class="nav nav-list submenu">
                        <li><a href="payment.html">Payment</a></li>
                        <li><a href="payment_history.html">History</a></li>
                        <li><a href="payment_details.html">Payment Details</a></li>
                        <li><a href="invoice.html">Invoice</a></li>
                    </ul>
                </li>
                <li>
                    <a href="Calendar.html">
                        <i class="fa fa-calendar"></i>
                        Calendar
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class="fa fa-group"></i>
                        Referral
                        <span class="glyphicon arrow"></span>
                    </a>
                    <ul class="nav nav-list submenu">
                        <li><a href="referral_list.html">Referral List</a></li>
                        <li><a href="add_referral.html">Add Referral</a></li>
                        <li><a href="edit_referral.html">Edit Referral</a></li>
                        <li><a href="referral_details.html">Referral Details</a></li>
                    </ul>
                </li>

                <li>
                    <a href="#">
                        <i class="fa fa-signal"></i>
                        Utilities
                        <span class="glyphicon arrow"></span>
                    </a>
                    <ul class="nav nav-list submenu">
                        <!--<li><a href="churn_rate.html">Churn Rate</a></li>-->
                        <li><a href="appointments.html">Appointments</a></li>
                    </ul>
                </li>
                <li><a></a></li>
            </ul>
        </div>
    </div>
</div>