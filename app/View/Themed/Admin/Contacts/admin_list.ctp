<?php
/**
 * @var $this View
 */
?>
    <h2 class="page-title">List of Contact</h2>
    <hr class="shadow-line"/>
    <div class="dropdown margin-bottom-5 pull-left">
        <button class="btn btn-info dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown">
            Quick Navigation
            <span class="caret"></span>
        </button>
        <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
            <li role="presentation">
                <?php echo $this->Html->link('All (' .$allContact. ')', array('controller' => 'contacts', 'action' => 'list'));?>
            </li>
            <li role="presentation" class="divider"></li>
            <li role="presentation">
                <?php echo $this->Html->link('Student (' .$studentContact. ')', array('controller' => 'contacts', 'action' => 'list/student'));?>
            </li>
            <li role="presentation" class="divider"></li>
            <li role="presentation">
                <?php echo $this->Html->link('Customer (' .$customerContact. ')', array('controller' => 'contacts', 'action' => 'list/customer'));?>
            </li>
            <li role="presentation" class="divider"></li>
            <li role="presentation">
                <?php echo $this->Html->link('Lead (' .$leadContact. ')', array('controller' => 'contacts', 'action' => 'list/lead'));?>
            </li>
        </ul>
    </div>
    <div class="pull-right">
        <?php echo $this->Html->link('Import Contact', array('controller' => 'contacts', 'action' => 'import'), array('class' => 'btn btn-theme'))?>

        <?php echo $this->Html->link('Export Contact', array('controller' => 'contacts', 'action' => 'export'), array('class' => 'btn btn-theme'))?>
    </div>

    <div class="clearfix"></div>
<?php  if($contactList):?>
    <div class="table-responsive">
        <table class="table data-table table-bordered sort-table" colspecing=0>
            <thead>
            <tr>
                <th style="width: 15%;">
                    <?php echo $this->Paginator->sort('first_name', 'Name'); ?>
                </th>
                <th style="width: 15%;">
                    <?php echo $this->Paginator->sort('account_type'); ?>
                </th>
                <th style="width: 15%;">
                    <?php echo $this->Paginator->sort('email'); ?>
                </th>
                <th style="width: 15%;">
                    <?php echo $this->Paginator->sort('phone'); ?>
                </th>
                <th style="width: 10%;">
                    <?php echo $this->Paginator->sort('status'); ?>
                </th>
                <th style="width: 10%;">
                    <?php echo $this->Paginator->sort('member_type'); ?>
                </th>
                <th style="width: 15%;">
                    <?php echo $this->Paginator->sort('member_created', 'Member Since'); ?>
                </th>
                <th style="width: 5%;">Action</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($contactList as $contact):?>
                <tr>
                    <td>
                        <?php echo $this->Html->link($contact['Contact']['first_name']. ' '. $contact['Contact']['first_name'], array('controller' => 'contacts', 'action' => 'edit', $contact['Contact']['id']));?>
                    </td>
                    <td>
                        <?php
                        if($contact['Contact']['account_type'] == 1){
                            echo 'Student';
                        }
                        elseif($contact['Contact']['account_type'] == 2){
                            echo 'Customer';
                        }
                        elseif($contact['Contact']['account_type'] == 3){
                            echo 'Lead/Prospect';
                        }
                        else{
                            echo 'N/A';
                        }
                        ?>
                    </td>

                    <td><?php echo $contact['Contact']['email']; ?></td>
                    <td><?php echo $contact['Contact']['phone']?></td>
                    <td>
                        <?php if ($contact['Contact']['status'] == 1): ?>
                            <span class="status"><i class="active fa fa-check"></i></span>
                            <small>(Active)</small>
                        <?php elseif ($contact['Contact']['status'] == 2): ?>
                            <span class="status"><i class="active fa fa-ban danger"></i></span>
                            <small>(Not Active)</small>
                        <?php endif; ?>
                    </td>
                    <td>
                        <?php if ($contact['Contact']['member_type'] == 1): ?>
                            <label class="label label-info">new member</label>
                        <?php elseif ($contact['Contact']['member_type'] == 2): ?>
                            <label class="label label-success">already member</label>
                        <?php elseif($contact['Contact']['member_type'] == 3): ?>
                            <label class="label label-danger">not member</label>
                        <?php endif; ?>
                    </td>
                    <td>
                        <time><?php echo $this->Time->format('M d, Y', $contact['Contact']['created']); ?></time>
                    </td>
                    <td>
                        <div class="icons-area">
                            <?php echo $this->Html->link('<i class="fa fa-pencil"></i>', array('controller' => 'contacts', 'action' => 'edit', $contact['Contact']['id']), array('escape' => false, 'class' => 'icon primary'));?>

                            <?php
                            echo $this->Form->postLink(
                                '<i class="glyphicon glyphicon-remove"></i>',
                                array(
                                    'controller' => 'contacts',
                                    'action' => 'delete',
                                    $contact['Contact']['id']
                                ),
                                array(
                                    'class' => 'icon danger',
                                    'escape' => false
                                ),
                                __('Are you sure you want to delete this contract?')
                            );
                            ?>
                        </div>
                    </td>
                </tr>
            <?php endforeach;?>
            </tbody>
        </table>
        <?php echo $this->element('pagination');?>
    </div>
<?php else:?>
    <h4 class="not-found">Sorry, contact not found</h4>
<?php endif;?>