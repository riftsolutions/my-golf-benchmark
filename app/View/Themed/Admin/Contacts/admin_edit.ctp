<?php
/**
 * @var $this View
 */
?>
<h2 class="page-title">Edit Contact</h2>
<hr class="shadow-line"/>
<div class="row">
    <div class="col-md-8 col-md-offset-2 col-sm-12">
        <div class="box box-padding-adding radius-5 margin-top-25 margin-bottom-25">
            <?php echo $this->Form->create(
                'Contact',
                array('controller' => 'contacts', 'action' => 'edit/'.$contactDetails['Contact']['id'], 'class' => 'form-horizontal form-custom')
            ); ?>
            <div class="form-group">
                <label class="col-sm-4 control-label">First Name</label>

                <div class="col-sm-8">
                    <?php
                    echo $this->Form->input(
                        'Contact.first_name',
                        array(
                            'type' => 'text',
                            'class' => 'form-control',
                            'value' => $contactDetails['Contact']['first_name'],
                            'label' => false,
                            'required' => false,
                        )
                    );
                    ?>
                    <?php echo $this->Form->error('Contact.first_name'); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4 control-label">Last Name</label>

                <div class="col-sm-8">
                    <?php
                    echo $this->Form->input(
                        'Contact.last_name',
                        array(
                            'type' => 'text',
                            'class' => 'form-control',
                            'value' => $contactDetails['Contact']['last_name'],
                            'label' => false,
                            'required' => false,
                        )
                    );
                    ?>
                    <?php echo $this->Form->error('Contact.last_name'); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4 control-label">Email</label>

                <div class="col-sm-8">
                    <?php
                    echo $this->Form->input(
                        'Contact.email',
                        array(
                            'type' => 'text',
                            'class' => 'form-control',
                            'value' => $contactDetails['Contact']['email'],
                            'label' => false,
                            'required' => false,
                        )
                    );
                    ?>
                    <?php echo $this->Form->error('Contact.email'); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4 control-label">Phone</label>

                <div class="col-sm-8">
                    <?php
                    echo $this->Form->input(
                        'Contact.phone',
                        array(
                            'type' => 'text',
                            'class' => 'form-control',
                            'value' => $contactDetails['Contact']['phone'],
                            'label' => false,
                            'required' => false,
                        )
                    );
                    ?>
                    <?php echo $this->Form->error('Contact.phone'); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4 control-label">Account Type</label>

                <div class="col-sm-8">
                    <div class="user-role">
                        <div class="radio">
                            <label>
                                <input type="radio" name="data[Contact][account_type]" value="1" <?php if($contactDetails['Contact']['account_type'] == 1){echo 'checked';}?>>
                                Student
                            </label>
                        </div>
                        <div class="radio disabled">
                            <label>
                                <input type="radio" name="data[Contact][account_type]" value="2" <?php if($contactDetails['Contact']['account_type'] == 2){echo 'checked';}?>>
                                Customer
                            </label>
                        </div>
                        <div class="radio disabled">
                            <label>
                                <input type="radio" name="data[Contact][account_type]" value="3" <?php if($contactDetails['Contact']['account_type'] == 3){echo 'checked';}?>>
                                Lead/Prospect
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4 control-label">Note</label>

                <div class="col-sm-8">
                    <?php
                    echo $this->Form->input(
                        'Contact.note',
                        array(
                            'type' => 'textarea',
                            'class' => 'form-control',
                            'value' => $contactDetails['Contact']['note'],
                            'label' => false,
                            'required' => false,
                        )
                    );
                    ?>
                    <?php echo $this->Form->error('Contact.phone'); ?>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12">
                    <button class="btn btn-theme pull-right">Edit Contact</button>
                </div>
            </div>
            <?php echo $this->Form->end(); ?>
        </div>
    </div>
</div>