<?php
/**
 * @var $this View
 */
?>
<h2 class="page-title">Import Contact</h2>
<hr class="shadow-line"/>
<div class="row">
    <div class="col-md-8 col-md-offset-2 col-sm-12">
        <div class="box box-padding-adding radius-5 margin-top-25 margin-bottom-25">
            <?php echo $this->Form->create(
                'Contact',
                array('controller' => 'contacts', 'action' => 'import', 'class' => 'form-horizontal form-custom', 'type' => 'file')
            ); ?>

            <div class="form-group">
                <label class="col-sm-4 control-label">Account Type</label>

                <div class="col-sm-8">
                    <div class="user-role">
                        <div class="radio">
                            <label>
                                <input type="radio" name="data[Contact][account_type]" value="1" checked>
                                Student
                            </label>
                        </div>
                        <div class="radio disabled">
                            <label>
                                <input type="radio" name="data[Contact][account_type]" value="2">
                                Customer
                            </label>
                        </div>
                        <div class="radio disabled">
                            <label>
                                <input type="radio" name="data[Contact][account_type]" value="3">
                                Lead/Prospect
                            </label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-4 control-label">CSV/Excle File</label>

                <div class="col-sm-8">
                    <?php
                    echo $this->Form->inout(
                        'file',
                        array(
                            'type' => 'file',
                            'class' => 'form-control',
                            'placeholder' => 'First Name',
                            'label' => false,
                            'error' => false,
                            'required' => false,
                        )
                    );
                    ?>
                    <?php echo $this->Form->error('file'); ?>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12">
                    <button class="btn btn-theme pull-right">Add New Contact</button>
                </div>
            </div>
            <?php echo $this->Form->end(); ?>
        </div>
    </div>
</div>