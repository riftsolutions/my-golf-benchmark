<?php
/**
 * @var $this View
 */
?>
<h2 class="page-title">List of Lesson</h2>
<hr class="shadow-line"/>

<div class="table-responsive">
<?php if ($lessonList): ?>
    <table class="table data-table table-bordered" colspecing=0>
    <thead>
    <tr>
        <th>Title</th>
        <th style="width:7%;">Duration</th>
        <th style="width: 8%;">Price</th>
        <th style="width: 10%;">Action</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach($lessonList as $lesson):?>
    <tr>
        <td>
            <?php echo $this->Html->link($this->Text->excerpt($lesson['Lesson']['title'], 'method', 30, '...'), array('controller' => 'lessons', 'action' => 'view', $lesson['Lesson']['uuid']), array('class' => 'green'));?>
        </td>
        <td><?php echo $lesson['Lesson']['duration'];?> Hours</td>
        <td>
            $<?php echo $lesson['Lesson']['price']?>
        </td>
        <td>
            <div class="icons-area">
                <?php echo $this->Html->link('<i class="fa fa-pencil"></i>', array('controller' => 'lessons', 'action' => 'edit', $lesson['Lesson']['uuid']), array('escape' => false, 'class' => 'icon primary'));?>

                <?php echo $this->Html->link('<i class="fa fa-check"></i>', array('controller' => 'lessons', 'action' => 'view', $lesson['Lesson']['uuid']), array('escape' => false, 'class' => 'icon info'));?>

                <?php
                echo $this->Form->postLink(
                    '<i class="glyphicon glyphicon-remove"></i>',
                    array(
                        'controller' => 'lessons',
                        'action' => 'delete',
                        $lesson['Lesson']['uuid']
                    ),
                    array(
                        'class' => 'icon danger',
                        'escape' => false
                    ),
                    __('Are you sure you want to delete this lesson')
                );
                ?>
            </div>
        </td>
    </tr>
    <?php endforeach;?>
    </tbody>
    </table>
    <?php echo $this->element('pagination');?>
    <?php else: ?>
    <h2 class="not-found">Sorry, lesson not found</h2>
    <hr class="shadow-line"/>
<?php endif; ?>
</div>
<div class="clearfix"></div>