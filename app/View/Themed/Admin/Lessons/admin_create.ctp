<?php
/**
 * @var $this View
 */
?>
<h2 class="page-title">Create New Lesson</h2>
<hr class="shadow-line"/>
<div class="row">
    <div class="col-md-8 col-md-offset-2 col-sm-12">
        <div class="box box-padding-adding radius-5 margin-top-25 margin-bottom-25">
            <?php echo $this->Form->create(
                'Lesson',
                array(
                    'controller' => 'lessons',
                    'action' => 'create',
                    'type' => 'file',
                    'method' => 'post',
                    'class' => 'form-horizontal form-custom'
                )
            ); ?>

            <div class="form-group">
                <label class="col-sm-4 control-label">Title</label>

                <div class="col-sm-8">
                    <?php
                    echo $this->Form->input(
                        'Lesson.title',
                        array(
                            'type' => 'text',
                            'class' => 'form-control',
                            'label' => false,
                            'div' => false,
                            'required' => false,
                            'placeholder' => 'Title of Lesson'
                        )
                    )
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4 control-label">Description</label>

                <div class="col-sm-8">
                    <?php
                    echo $this->Form->input(
                        'Lesson.description',
                        array(
                            'type' => 'textarea',
                            'class' => 'form-control',
                            'label' => false,
                            'div' => false,
                            'required' => false,
                            'rows' => 6,
                            'placeholder' => 'Lesson description...'
                        )
                    )
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4 control-label">Lesson Price</label>

                <div class="col-sm-6">
                    <div class="input-group">
                        <span class="input-group-addon">$</span>
                        <?php
                        echo $this->Form->input(
                            'Lesson.price',
                            array(
                                'type' => 'text',
                                'class' => 'form-control',
                                'label' => false,
                                'div' => false,
                                'required' => false,
                                'placeholder' => 'Price',
                                'error' => false
                            )
                        )
                        ?>
                        <span class="input-group-addon">.00</i></span>
                    </div>
                    <?php echo $this->Form->error('Lesson.price'); ?>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-4 control-label">Duration</label>

                <div class="col-sm-4">
                    <?php
                    echo $this->Form->input(
                        'Lesson.hours',
                        array(
                            'type' => 'text',
                            'class' => 'form-control',
                            'label' => false,
                            'div' => false,
                            'required' => false,
                            'placeholder' => 'Hours'
                        )
                    )
                    ?>
                </div>
                <div class="col-sm-4">
                    <?php
                    echo $this->Form->input(
                        'Lesson.mins',
                        array(
                            'type' => 'text',
                            'class' => 'form-control',
                            'label' => false,
                            'div' => false,
                            'required' => false,
                            'placeholder' => 'Minutes'
                        )
                    )
                    ?>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-4 control-label">Add Attachment</label>

                <div class="col-sm-8">
                    <table class="addAttachment" style="width: 100%;">
                        <tbody>
                        <tr>
                            <td colspan="2" style="height: 55px;">
                                <input class="form-control" name="data[Attachment][][attachment]" id="phone_number"
                                       type="file">
                            </td>
                        </tr>
                        </tbody>
                        <tfoot>
                        <tr>
                            <td>

                            </td>
                        </tr>
                        </tfoot>
                    </table>

                    <button type="button" class="pull-left btn btn-xs btn-default" id="addAttachment">
                        <i class="fa fa-paperclip"></i> Add More Attachment <i class="fa fa-plus"></i>
                    </button>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12">
                    <button class="btn btn-theme pull-right">Create Lesson</button>
                </div>
            </div>
            <?php echo $this->Form->end(); ?>
        </div>
    </div>
</div>

<script>
    /*---- ADD Attachment -----*/
    $(document).ready(function () {
        var counter = 1;
        $("#addAttachment").on("click", function () {
            $("#addAllCatBrands").slideUp();
            counter++;
            var newRow = $("<tr>");
            var cols = "";
            cols += '<td>' +
                '<input class="form-control" name="data[Attachment][][attachment]" id="phone_number" type="file">' +
                '</td>';
            cols += '<td style="text-align: right; height: 55px;"><a class="deleteRow danger"><i class="glyphicon glyphicon-remove"></i></td>';
            newRow.append(cols);
            $("table.addAttachment").append(newRow);
        });
        $("table.addAttachment").on("click", "a.deleteRow", function (event) {
            $(this).closest("tr").remove();
            counter--;
            if (counter < 2) {
                $("#addAllCatBrands").slideDown();
            }
        });
    });
    /*---- ADD Attachment -----*/

    $(document).ready(function () {
        $('#datepicker').datepicker()
    });
</script>