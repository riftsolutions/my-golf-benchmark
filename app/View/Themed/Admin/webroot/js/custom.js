/****** Dynamically Set The height of sidebar ******************/
$(document).ready(function() {
    var contentAreaHeight = $("#content-area").innerHeight();
    var windowsHeight = window.innerHeight;
    if(windowsHeight > 786 ){
        sidebarHeight = windowsHeight;
    }
    else{
        sidebarHeight = contentAreaHeight;
    }
    $(".sidebar").css("height", sidebarHeight + "px");
});
/****** Dynamically Set The height of sidebar ******************/



$(document).ready(function() {
    $('[data-toggle=offcanvas]').click(function() {
        $('.row-offcanvas').toggleClass('active');
    });
});





$(function () {
    $('#menu').metisMenu({
        toggle: false // disable the auto collapse. Default: true.
    });
});

;(function ($, window, document, undefined) {

    var pluginName = "metisMenu",
        defaults = {
            toggle: true
        };

    function Plugin(element, options) {
        this.element = element;
        this.settings = $.extend({}, defaults, options);
        this._defaults = defaults;
        this._name = pluginName;
        this.init();
    }

    Plugin.prototype = {
        init: function () {

            var $this = $(this.element),
                $toggle = this.settings.toggle;

            $this.find('li.active').has('ul').children('ul').addClass('collapse in');
            $this.find('li').not('.active').has('ul').children('ul').addClass('collapse');

            $this.find('li').has('ul').children('a').on('click', function (e) {
                e.preventDefault();

                $(this).parent('li').toggleClass('active').children('ul').collapse('toggle');

                if ($toggle) {
                    $(this).parent('li').siblings().removeClass('active').children('ul.in').collapse('hide');
                }
            });
        }
    };

    $.fn[ pluginName ] = function (options) {
        return this.each(function () {
            if (!$.data(this, "plugin_" + pluginName)) {
                $.data(this, "plugin_" + pluginName, new Plugin(this, options));
            }
        });
    };

})(jQuery, window, document);


/*------- Script for the dataTable --------*/

/*--- Contact --*/
$(document).ready(function() {
    $('#datatable').dataTable({
        "sPaginationType": "bs_two_button"
    });
    $("#datatable").dataTable().columnFilter({
        aoColumns: [
            { type: "text"},
            { type: "select", values: [ 'Student', 'Istructure', 'Lead']},
            { type: "text" },
            { type: "text"},
            { type: "select", values: [ 'Active', 'Not Active']},
            { type: "select", values: [ 'new member', 'already member', 'not member']}
        ]

    });
    $('#datatable').each(function(){
        var datatable = $(this);
        // SEARCH - Add the placeholder for Search and Turn this into in-line form control
        var search_input = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input');
        search_input.attr('placeholder', 'Search');
        search_input.addClass('form-control input-sm');
        // LENGTH - Inline-Form control
        var length_sel = datatable.closest('.dataTables_wrapper').find('div[id$=_length] select');
        length_sel.addClass('form-control input-sm');
        datatable.bind('page', function(e){
            window.console && console.log('pagination event:', e) //this event must be fired whenever you paginate
        });
    });
});
/*-- /Contact ---*/

/*--- Lesson Report ---*/
$(document).ready(function() {
    $('#lessonReport').dataTable({
        "sPaginationType": "bs_two_button"
    });
    $("#lessonReport").dataTable().columnFilter({
        aoColumns: [
            { type: "text"},
            { type: "text" },
            { type: "text" },
            { type: "text" },
            { type: "text"},
            { type: "text" },
            { type: "select", values: [ 'Open', 'Closed']}
        ]

    });
    $('#lessonReport').each(function(){
        var datatable = $(this);
        // SEARCH - Add the placeholder for Search and Turn this into in-line form control
        var search_input = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input');
        search_input.attr('placeholder', 'Search');
        search_input.addClass('form-control input-sm');
        // LENGTH - Inline-Form control
        var length_sel = datatable.closest('.dataTables_wrapper').find('div[id$=_length] select');
        length_sel.addClass('form-control input-sm');
        datatable.bind('page', function(e){
            window.console && console.log('pagination event:', e) //this event must be fired whenever you paginate
        });
    });
});
/*--- /Lesson Report ---*/

/*--- Referral Report ---*/
$(document).ready(function() {
    $('#referralList').dataTable({
        "sPaginationType": "bs_two_button"
    });
    $("#referralList").dataTable().columnFilter({
        aoColumns: [
            { type: "text"},
            { type: "text" },
            null,
            { type: "text" },
            { type: "select", values: ['Valid', 'Invalid', 'Expired']},
            { type: "text" }
        ]
    });
    $('#referralList').each(function(){
        var datatable = $(this);
        // SEARCH - Add the placeholder for Search and Turn this into in-line form control
        var search_input = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input');
        search_input.attr('placeholder', 'Search');
        search_input.addClass('form-control input-sm');
        // LENGTH - Inline-Form control
        var length_sel = datatable.closest('.dataTables_wrapper').find('div[id$=_length] select');
        length_sel.addClass('form-control input-sm');
        datatable.bind('page', function(e){
            window.console && console.log('pagination event:', e) //this event must be fired whenever you paginate
        });
    });
});
/*--- /Referral Report ---*/

/*--- Student List --*/
$(document).ready(function() {
    $('#student-list').dataTable({
        "sPaginationType": "bs_two_button"
    });
    $("#student-list").dataTable().columnFilter({
        aoColumns: [
            { type: "text"},
            { type: "text" },
            { type: "text" },
            { type: "select", values: [ 'Active', 'Slipping Away', 'Inactive']},
            null,
            null
        ]

    });
    $('#student-list').each(function(){
        var datatable = $(this);
        // SEARCH - Add the placeholder for Search and Turn this into in-line form control
        var search_input = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input');
        search_input.attr('placeholder', 'Search');
        search_input.addClass('form-control input-sm');
        // LENGTH - Inline-Form control
        var length_sel = datatable.closest('.dataTables_wrapper').find('div[id$=_length] select');
        length_sel.addClass('form-control input-sm');
        datatable.bind('page', function(e){
            window.console && console.log('pagination event:', e) //this event must be fired whenever you paginate
        });
    });
});
/*-- /Student List ---*/

/*--- Instructor List --*/
$(document).ready(function() {
    $('#Instructor-list').dataTable({
        "sPaginationType": "bs_two_button"
    });
    $("#Instructor-list").dataTable().columnFilter({
        aoColumns: [
            { type: "text"},
            { type: "text" },
            { type: "text" },
            { type: "select", values: [ 'Active', 'Inactive']},
            null,
            null,
            null,
            null
        ]

    });
    $('#Instructor-list').each(function(){
        var datatable = $(this);
        // SEARCH - Add the placeholder for Search and Turn this into in-line form control
        var search_input = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input');
        search_input.attr('placeholder', 'Search');
        search_input.addClass('form-control input-sm');
        // LENGTH - Inline-Form control
        var length_sel = datatable.closest('.dataTables_wrapper').find('div[id$=_length] select');
        length_sel.addClass('form-control input-sm');
        datatable.bind('page', function(e){
            window.console && console.log('pagination event:', e) //this event must be fired whenever you paginate
        });
    });
});
/*-- /Instructor List ---*/

/*--- Lesson List --*/
$(document).ready(function() {
    $('#lesson-list').dataTable({
        "sPaginationType": "bs_two_button"
    });
    $("#lesson-list").dataTable().columnFilter({
        aoColumns: [
            { type: "text"},
            { type: "text" },
            { type: "text" },
            { type: "select", values: [ 'Active', 'Blocked']},
            { type: "text" },
            null
        ]

    });
    $('#lesson-list').each(function(){
        var datatable = $(this);
        // SEARCH - Add the placeholder for Search and Turn this into in-line form control
        var search_input = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input');
        search_input.attr('placeholder', 'Search');
        search_input.addClass('form-control input-sm');
        // LENGTH - Inline-Form control
        var length_sel = datatable.closest('.dataTables_wrapper').find('div[id$=_length] select');
        length_sel.addClass('form-control input-sm');
        datatable.bind('page', function(e){
            window.console && console.log('pagination event:', e) //this event must be fired whenever you paginate
        });
    });
});
/*-- /Lesson List ---*/

/*--- Upcoming Lesson List --*/
$(document).ready(function() {
    $('#upcomingLesson').dataTable({
        "sPaginationType": "bs_two_button"
    });
    $("#upcomingLesson").dataTable().columnFilter({
        aoColumns: [
            { type: "text"},
            { type: "text" },
            { type: "text" },
            null,
            { type: "text" },
            null
        ]

    });
    $('#upcomingLesson').each(function(){
        var datatable = $(this);
        // SEARCH - Add the placeholder for Search and Turn this into in-line form control
        var search_input = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input');
        search_input.attr('placeholder', 'Search');
        search_input.addClass('form-control input-sm');
        // LENGTH - Inline-Form control
        var length_sel = datatable.closest('.dataTables_wrapper').find('div[id$=_length] select');
        length_sel.addClass('form-control input-sm');
        datatable.bind('page', function(e){
            window.console && console.log('pagination event:', e) //this event must be fired whenever you paginate
        });
    });
});
/*-- /Upcoming Lesson List ---*/

/*--- Booked Lesson List --*/
$(document).ready(function() {
    $('#bookedLesson').dataTable({
        "sPaginationType": "bs_two_button"
    });
    $("#bookedLesson").dataTable().columnFilter({
        aoColumns: [
            { type: "text"},
            { type: "text" },
            { type: "text" },
            null,
            { type: "text" },
            null
        ]

    });
    $('#bookedLesson').each(function(){
        var datatable = $(this);
        // SEARCH - Add the placeholder for Search and Turn this into in-line form control
        var search_input = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input');
        search_input.attr('placeholder', 'Search');
        search_input.addClass('form-control input-sm');
        // LENGTH - Inline-Form control
        var length_sel = datatable.closest('.dataTables_wrapper').find('div[id$=_length] select');
        length_sel.addClass('form-control input-sm');
        datatable.bind('page', function(e){
            window.console && console.log('pagination event:', e) //this event must be fired whenever you paginate
        });
    });
});
/*-- /Booked Lesson List ---*/

/*--- Taken Lesson List --*/
$(document).ready(function() {
    $('#takenLesson').dataTable({
        "sPaginationType": "bs_two_button"
    });
    $("#takenLesson").dataTable().columnFilter({
        aoColumns: [
            { type: "text"},
            { type: "text" },
            { type: "text" },
            null,
            { type: "text" },
            null
        ]

    });
    $('#takenLesson').each(function(){
        var datatable = $(this);
        // SEARCH - Add the placeholder for Search and Turn this into in-line form control
        var search_input = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input');
        search_input.attr('placeholder', 'Search');
        search_input.addClass('form-control input-sm');
        // LENGTH - Inline-Form control
        var length_sel = datatable.closest('.dataTables_wrapper').find('div[id$=_length] select');
        length_sel.addClass('form-control input-sm');
        datatable.bind('page', function(e){
            window.console && console.log('pagination event:', e) //this event must be fired whenever you paginate
        });
    });
});
/*-- /Taken Lesson List ---*/

/*--- PDF List --*/
$(document).ready(function() {
    $('#pdf-list').dataTable({
        "sPaginationType": "bs_two_button"
    });
    $("#pdf-list").dataTable().columnFilter({
        aoColumns: [
            { type: "text"},
            { type: "text" },
            { type: "text" },
            { type: "text" },
            null
        ]

    });
    $('#pdf-list').each(function(){
        var datatable = $(this);
        // SEARCH - Add the placeholder for Search and Turn this into in-line form control
        var search_input = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input');
        search_input.attr('placeholder', 'Search');
        search_input.addClass('form-control input-sm');
        // LENGTH - Inline-Form control
        var length_sel = datatable.closest('.dataTables_wrapper').find('div[id$=_length] select');
        length_sel.addClass('form-control input-sm');
        datatable.bind('page', function(e){
            window.console && console.log('pagination event:', e) //this event must be fired whenever you paginate
        });
    });
});
/*-- /PDF List ---*/

/*--- Purchase PDF List --*/
$(document).ready(function() {
    $('#purchasedPDF').dataTable({
        "sPaginationType": "bs_two_button"
    });
    $("#purchasedPDF").dataTable().columnFilter({
        aoColumns: [
            { type: "text"},
            { type: "text" },
            { type: "text" },
            { type: "text" },
            null
        ]

    });
    $('#purchasedPDF').each(function(){
        var datatable = $(this);
        // SEARCH - Add the placeholder for Search and Turn this into in-line form control
        var search_input = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input');
        search_input.attr('placeholder', 'Search');
        search_input.addClass('form-control input-sm');
        // LENGTH - Inline-Form control
        var length_sel = datatable.closest('.dataTables_wrapper').find('div[id$=_length] select');
        length_sel.addClass('form-control input-sm');
        datatable.bind('page', function(e){
            window.console && console.log('pagination event:', e) //this event must be fired whenever you paginate
        });
    });
});
/*-- /Purchase PDF List ---*/

/*--- Purchase Package List --*/
$(document).ready(function() {
    $('#purchasedPackage').dataTable({
        "sPaginationType": "bs_two_button"
    });
    $("#purchasedPackage").dataTable().columnFilter({
        aoColumns: [
            { type: "text"},
            { type: "text" },
            { type: "text" },
            null
        ]

    });
    $('#purchasedPackage').each(function(){
        var datatable = $(this);
        // SEARCH - Add the placeholder for Search and Turn this into in-line form control
        var search_input = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input');
        search_input.attr('placeholder', 'Search');
        search_input.addClass('form-control input-sm');
        // LENGTH - Inline-Form control
        var length_sel = datatable.closest('.dataTables_wrapper').find('div[id$=_length] select');
        length_sel.addClass('form-control input-sm');
        datatable.bind('page', function(e){
            window.console && console.log('pagination event:', e) //this event must be fired whenever you paginate
        });
    });
});
/*-- /Purchase Package List ---*/

/*--- Benchmark List --*/
$(document).ready(function() {
    $('#benchmark-list').dataTable({
        "sPaginationType": "bs_two_button"
    });
    $("#benchmark-list").dataTable().columnFilter({
        aoColumns: [
            { type: "text"},
            { type: "text" },
            { type: "select", values: [ 'failed', 'passed']},
            { type: "text" },
            { type: "date"},
            null
        ]

    });
    $('#benchmark-list').each(function(){
        var datatable = $(this);
        // SEARCH - Add the placeholder for Search and Turn this into in-line form control
        var search_input = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input');
        search_input.attr('placeholder', 'Search');
        search_input.addClass('form-control input-sm');
        // LENGTH - Inline-Form control
        var length_sel = datatable.closest('.dataTables_wrapper').find('div[id$=_length] select');
        length_sel.addClass('form-control input-sm');
        datatable.bind('page', function(e){
            window.console && console.log('pagination event:', e) //this event must be fired whenever you paginate
        });
    });
});
/*-- /Benchmark List ---*/

/*--- Payment History --*/
$(document).ready(function() {
    $('#payment-history').dataTable({
        "sPaginationType": "bs_two_button"
    });
    $("#payment-history").dataTable().columnFilter({
        aoColumns: [
            { type: "text"},
            { type: "text" },
            { type: "text" },
            { type: "text" },
            { type: "text" },
            { type: "text" },
            { type: "select", values: [ 'received', 'processing', 'pending', 'cancelled']},
            null
        ]

    });
    $('#payment-history').each(function(){
        var datatable = $(this);
        // SEARCH - Add the placeholder for Search and Turn this into in-line form control
        var search_input = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input');
        search_input.attr('placeholder', 'Search');
        search_input.addClass('form-control input-sm');
        // LENGTH - Inline-Form control
        var length_sel = datatable.closest('.dataTables_wrapper').find('div[id$=_length] select');
        length_sel.addClass('form-control input-sm');
        datatable.bind('page', function(e){
            window.console && console.log('pagination event:', e) //this event must be fired whenever you paginate
        });
    });
});
/*-- /Payment History ---*/


/*------- Script for the dataTable --------*/

/*------ Bootstrap date picker --------*/
$(document).ready(function(){
    $('#datepicker').datepicker()
});
/*------ Bootstrap date picker --------*/

$(window).bind("load", function() {
    var footer = $("footer");
    var pos = footer.position();
    var height = $(window).height();
    height = height - pos.top;
    height = height - footer.height();
    if (height > 0) {
        footer.css({'margin-top' : height+'px'});
    }
});


/*---- ADD PACKAGE -----*/
$(document).ready(function () {
    var counter = 1;
    $("#customMenu").on("click", function () {
    $("#addAllCatBrands").slideUp();
    counter++;
    var newRow = $("<tr>");
    var cols = "";
    cols += '<td>' +
    '<select name="lessonName[]" class="form-control">' +
    '<option>How to play Golf</option>' +
    '<option>What is Golf</option>' +
    '<option>How can I start playing golf</option>' +
    '<option>How to play Golf</option>' +
    '<option>What is Golf</option>' +
    '<option>How can I start playing golf</option>' +
    '</select>' +
    '</td>';
    cols += '<td style="text-align: right; height: 55px;"><a class="deleteRow danger"><i class="glyphicon glyphicon-remove"></i></td>';
    newRow.append(cols);
    $("table.customMenu").append(newRow);
    });
$("table.customMenu").on("click", "a.deleteRow", function (event) {
    $(this).closest("tr").remove();
    counter--;
    if (counter < 2) {
    $("#addAllCatBrands").slideDown();
    }
});
});
/*---- ADD PACKAGE -----*/

