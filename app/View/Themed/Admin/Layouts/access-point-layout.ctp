<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="<?php echo Router::url('/', true);?>theme/Admin/img/favicon.png">

    <title><?php echo $title_for_layout;?></title>

    <!-- Bootstrap core CSS -->

    <?php echo $this->Html->css(array('bootstrap.min', 'login-signup.css', 'custom.css', 'font-awesome', 'bootstrap-social'));?>
    <!-- Custom styles for this template -->

    <link href='http://fonts.googleapis.com/css?family=Ubuntu:400,500,700,300italic,400italic,500italic,700italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]>
    <?php echo $this->Html->script(array('ie8-responsive-file-warning'));?></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <?php echo $this->Html->script(array('html5shiv', 'respond.min'));?>
    <![endif]-->
    
    <?php echo $this->Html->script(array('jquery.min', 'bootstrap.min'));?>
</head>
<body class="login-bg">

<div class="new-box">
    <div class="login-wrapper">
        <div class="flash-area"><?php echo $this->Session->flash(); ?></div>
        <?php echo $content_for_layout; ?>
    </div>
</div>

<?php echo $this->Js->writeBuffer(); ?>
</body>
</html>