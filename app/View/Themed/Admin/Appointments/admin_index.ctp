<?php
/**
 * @var $this View
 */
?>
<h2 class="page-title">Appointment</h2>
<hr class="shadow-line"/>
<div class="box box-padding-normal">
<h2 class="heading-sm u-p">The Golf Swing Prescription Benchmark</h2>
<hr class="shadow-line">
<p class="italic-md">
    N.B: Our golf lessons in Orange County are conducted by PGA Certified golf instructors using the latest equipment
    and best techniques. Give us a call Our golf lessons in Orange County are conducted by PGA Certified golf
    instructors using the latest equipment and best techniques. Give us a call
</p>

<div class="row margin-top-25">
<div class="col-lg-12">
<ul id="myTab" class="nav nav-tabs">
    <li class="active"><a href="#history-admin-goals" data-toggle="tab">History and Goals</a></li>
    <li><a href="#summary" data-toggle="tab">Profile Summary</a></li>
    <li><a href="#training-lesson" data-toggle="tab">Training Lesson</a></li>
</ul>
<div id="myTabContent" class="tab-content margin-top-15">
<div class="tab-pane fade in active" id="history-admin-goals">
    <div class="row">
        <div class="col-lg-6">
            <fieldset>
                <legend>Personal Information</legend>
                <form class="form-horizontal form-custom" role="form">
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Picture</label>

                        <div class="col-sm-6">
                            <a href="profile.html">
                                <?php echo $this->Html->image('profile.jpg', array('class' => 'profile-pic-small profile-pic-md img-thumbnail'));?>
                            </a>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Start Date</label>

                        <div class="col-sm-4">
                            <input id="datepicker" type="text" class="form-control" placeholder="02-25-2014">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Student Name</label>

                        <div class="col-sm-6">
                            <input type="text" class="form-control" placeholder="John Doe">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Student Phone</label>

                        <div class="col-sm-6">
                            <input type="text" class="form-control" placeholder="111 222 3333">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Student Email</label>

                        <div class="col-sm-6">
                            <input type="text" class="form-control" placeholder="john.doe@gmail.com">
                        </div>
                    </div>
                </form>
            </fieldset>
        </div>
        <div class="col-lg-6">
            <fieldset>
                <legend>History and Goals</legend>
                <form class="form-horizontal form-custom" role="form">
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Golf History and Goal</label>

                        <div class="col-sm-8">
                            <textarea class="form-control" placeholder="N.B: Our golf lessons in Orange County are conducted by PGA Certified golf instructors" rows="5"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">What day and time best for you <span class="italic-sm">(in a week)</span></label>

                        <div class="col-sm-8">
                            <textarea class="form-control" placeholder="N.B: Our golf lessons in Orange County are conducted by PGA Certified golf instructors" rows="5"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">How frequently can you come it.</label>

                        <div class="col-sm-2">
                            <input type="text" class="form-control" placeholder="10">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">How frequently can you come it.</label>

                        <div class="col-sm-2">
                            <input type="text" class="form-control" placeholder="10">
                        </div>
                    </div>
                </form>
            </fieldset>
        </div>
    </div>
</div>
<div class="tab-pane fade" id="summary">
    <fieldset>
        <legend>Summary</legend>
        <ul class="form-list">
            <li>
                <input name="totalCcore" class="input-list input-green" value="88">
                <strong>% Total Score</strong>

                <p class="italic-sm">
                    <small>ur golf lessons in Orange County are conducted by PGA</small>
                </p>
            </li>
        </ul>
        <h2 class="heading-sm u-p">Fundamental Score</h2>
        <hr class="shadow-line">
        <div class="row">
            <div class="col-lg-4 col-sm-4">
                <ul class="form-list">
                    <li>
                        <input name="" class="input-list input-gray" value="40">
                        % Setup
                    </li>
                    <li>
                        <input name="" class="input-list input-info" value="50">
                        % Lower body stability
                    </li>
                    <li>
                        <input name="" class="input-list input-gray" value="70">
                        % Setup
                    </li>
                </ul>
            </div>
            <div class="col-lg-4 col-sm-4">
                <ul class="form-list">
                    <li>
                        <input name="" class="input-list input-gray" value="60">
                        % Setup
                    </li>
                    <li>
                        <input name="" class="input-list input-gray" value="20">
                        % Setup
                    </li>
                    <li>
                        <input name="" class="input-list input-gray" value="50">
                        % Lower body stability
                    </li>
                </ul>
            </div>
            <div class="col-lg-4 col-sm-4">
                <ul class="form-list">
                    <li>
                        <input name="" class="input-list input-gray" value="70">
                        % Setup
                    </li>
                    <li>
                        <input name="" class="input-list input-gray" value="60">
                        % Setup
                    </li>
                    <li>
                        <input name="" class="input-list input-gray" value="20">
                        % Setup
                    </li>
                </ul>
            </div>
        </div>
    </fieldset>
</div>
<div class="tab-pane fade" id="training-lesson">
    <fieldset>
        <legend>Training Lesson</legend>
        <ul class="form-list">
            <li>
                <input name="totalCcore" class="input-list input-green" value="88">
                <strong>% Total Score</strong>

                <p class="italic-sm">
                    <small>ur golf lessons in Orange County are conducted by PGA</small>
                </p>
            </li>
        </ul>
        <h2 class="heading-sm u-p">Fundamental Score</h2>
        <hr class="shadow-line">
        <div class="row">
            <div class="col-lg-4 col-sm-4">
                <ul class="form-list">
                    <li>
                        <input name="" class="input-list input-gray" value="40">
                        % Setup
                    </li>
                    <li>
                        <input name="" class="input-list input-info" value="50">
                        % Lower body stability
                    </li>
                    <li>
                        <input name="" class="input-list input-gray" value="70">
                        % Setup
                    </li>
                </ul>
            </div>
            <div class="col-lg-4 col-sm-4">
                <ul class="form-list">
                    <li>
                        <input name="" class="input-list input-gray" value="60">
                        % Setup
                    </li>
                    <li>
                        <input name="" class="input-list input-gray" value="20">
                        % Setup
                    </li>
                    <li>
                        <input name="" class="input-list input-gray" value="50">
                        % Lower body stability
                    </li>
                </ul>
            </div>
            <div class="col-lg-4 col-sm-4">
                <ul class="form-list">
                    <li>
                        <input name="" class="input-list input-gray" value="70">
                        % Setup
                    </li>
                    <li>
                        <input name="" class="input-list input-gray" value="60">
                        % Setup
                    </li>
                    <li>
                        <input name="" class="input-list input-gray" value="20">
                        % Setup
                    </li>
                </ul>
            </div>
        </div>
    </fieldset>
</div>
</div>
</div>
</div>
<div class="clearfix"></div>

<!---- Widget One ----->
<div class="widget black-widget">
<div class="widget-header u-p">
    <i class="fa fa-bar-chart-o"></i> Lower Body Stability
</div>
<div class="widget-body padding-15">
<h2 class="heading-sm u-p">Halfway Back: </h2>
<hr class="shadow-line">
<div class="container-fluid">
    <div class="row">
        <!--Single Box -->
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">REAR</h4>
            <div class="scoring-body">
                <input id="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                <input name="Name" type="text" id="uiSliderValue" class="textbox" />
                <div class="scoring-single-img">
                    <?php echo $this->Html->image('banner.jpg', array('class' => 'scoring-img'));?>
                </div>
                <textarea class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <!--/Single Box -->
        <!--Single Box -->
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">REAR</h4>
            <div class="scoring-body">
                <input id="uiSlider2" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                <input name="Name" type="text" id="uiSliderValue2" class="textbox" />
                <div class="scoring-single-img">
                    <?php echo $this->Html->image('banner.jpg', array('class' => 'scoring-img'));?>
                </div>
                <textarea class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <!--/Single Box -->
        <!--Single Box -->
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">KNEE FLEX</h4>
            <div class="scoring-body">
                <input id="uiSlider3" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                <input name="Name" type="text" id="uiSliderValue3" class="textbox" />
                <div class="scoring-single-img">
                    <?php echo $this->Html->image('banner.jpg', array('class' => 'scoring-img'));?>
                </div>
                <textarea class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <!--/Single Box -->
        <!--Single Box -->
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">HIP SWAY</h4>
            <div class="scoring-body">
                <input id="uiSlider4" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                <input name="Name" type="text" id="uiSliderValue4" class="textbox" />
                <div class="scoring-single-img">
                    <?php echo $this->Html->image('banner.jpg', array('class' => 'scoring-img'));?>
                </div>
                <textarea class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <!--/Single Box -->
        <!--Single Box -->
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">HIP PIVOT</h4>
            <div class="scoring-body">
                <input id="uiSlider5" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                <input name="Name" type="text" id="uiSliderValue5" class="textbox" />
                <div class="scoring-single-img">
                    <?php echo $this->Html->image('banner.jpg', array('class' => 'scoring-img'));?>
                </div>
                <textarea class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <!--/Single Box -->
    </div>
</div>
<div class="margin-top-25"></div>
<h2 class="heading-sm u-p">¾ Swing: </h2>
<hr class="shadow-line">
<div class="container-fluid">
    <div class="row">
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">Rear</h4>

            <div class="scoring-body">
                <div class="slider slider-horizontal">
                    <div class="slider-track">
                        <div class="slider-selection" style="left: 0%; width: 50%;"></div>
                        <div class="slider-handle triangle" style="left: 50%;"></div>
                        <div class="slider-handle triangle hide" style="left: 0%;"></div>
                    </div>
                    <div class="tooltip top" style="top: -30px; left: 116.5px;">
                        <div class="tooltip-arrow"></div>
                        <div class="tooltip-inner">100</div>
                    </div>
                    <input id="mySlider5" data-slider-handle="triangle" data-slider-min="0" data-slider-value="100" data-slider-max="200" type="text"></div>
                <br>
                <span id="mySliderValue5" class="textbox">100</span>

                <div class="scoring-single-img">
                    <?php echo $this->Html->image('banner.jpg', array('class' => 'scoring-img'));?>
                </div>
                <textarea class="form-control" rows="5" placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">Knee Flex</h4>

            <div class="scoring-body">
                <div class="slider slider-horizontal">
                    <div class="slider-track">
                        <div class="slider-selection" style="left: 0%; width: 52%;"></div>
                        <div class="slider-handle triangle" style="left: 52%;"></div>
                        <div class="slider-handle triangle hide" style="left: 0%;"></div>
                    </div>
                    <div class="tooltip top" style="top: -30px; left: 125.4px;">
                        <div class="tooltip-arrow"></div>
                        <div class="tooltip-inner">86</div>
                    </div>
                    <input id="mySlider6" data-slider-handle="triangle" data-slider-min="0" data-slider-value="100" data-slider-max="200" type="text"></div>
                <br>
                <span id="mySliderValue6" class="textbox">96</span>

                <div class="scoring-single-img">
                    <?php echo $this->Html->image('banner.jpg', array('class' => 'scoring-img'));?>
                </div>
                <textarea class="form-control" rows="5" placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p"> Hip Sway</h4>

            <div class="scoring-body">
                <div class="slider slider-horizontal">
                    <div class="slider-track">
                        <div class="slider-selection" style="left: 0%; width: 50%;"></div>
                        <div class="slider-handle triangle" style="left: 50%;"></div>
                        <div class="slider-handle triangle hide" style="left: 0%;"></div>
                    </div>
                    <div class="tooltip top" style="top: -30px; left: 116.5px;">
                        <div class="tooltip-arrow"></div>
                        <div class="tooltip-inner">100</div>
                    </div>
                    <input id="mySlider7" data-slider-handle="triangle" data-slider-min="0" data-slider-value="100" data-slider-max="200" type="text"></div>
                <br>
                <span id="mySliderValue7" class="textbox">100</span>

                <div class="scoring-single-img">
                    <?php echo $this->Html->image('banner.jpg', array('class' => 'scoring-img'));?>
                </div>
                <textarea class="form-control" rows="5" placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p"> Hip Pivot </h4>

            <div class="scoring-body">
                <div class="slider slider-horizontal">
                    <div class="slider-track">
                        <div class="slider-selection" style="left: 0%; width: 50%;"></div>
                        <div class="slider-handle triangle" style="left: 50%;"></div>
                        <div class="slider-handle triangle hide" style="left: 0%;"></div>
                    </div>
                    <div class="tooltip top" style="top: -30px; left: 116.5px;">
                        <div class="tooltip-arrow"></div>
                        <div class="tooltip-inner">100</div>
                    </div>
                    <input id="mySlider8" data-slider-handle="triangle" data-slider-min="0" data-slider-value="100" data-slider-max="200" type="text"></div>
                <br>
                <span id="mySliderValue8" class="textbox">100</span>

                <div class="scoring-single-img">
                    <?php echo $this->Html->image('banner.jpg', array('class' => 'scoring-img'));?>
                </div>
                <textarea class="form-control" rows="5" placeholder="Add Notes Here"></textarea>
            </div>
        </div>
    </div>
</div>
<div class="margin-top-25"></div>
<h2 class="heading-sm u-p">At Top: </h2>
<hr class="shadow-line">
<div class="container-fluid">
    <div class="row">
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">Rear</h4>

            <div class="scoring-body">
                <div class="slider slider-horizontal">
                    <div class="slider-track">
                        <div class="slider-selection" style="left: 0%; width: 50%;"></div>
                        <div class="slider-handle triangle" style="left: 50%;"></div>
                        <div class="slider-handle triangle hide" style="left: 0%;"></div>
                    </div>
                    <div class="tooltip top" style="top: -30px; left: 116.5px;">
                        <div class="tooltip-arrow"></div>
                        <div class="tooltip-inner">100</div>
                    </div>
                    <input id="mySlider5" data-slider-handle="triangle" data-slider-min="0" data-slider-value="100" data-slider-max="200" type="text"></div>
                <br>
                <span id="mySliderValue5" class="textbox">100</span>

                <div class="scoring-single-img">
                    <?php echo $this->Html->image('banner.jpg', array('class' => 'scoring-img'));?>
                </div>
                <textarea class="form-control" rows="5" placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">Knee Flex</h4>

            <div class="scoring-body">
                <div class="slider slider-horizontal">
                    <div class="slider-track">
                        <div class="slider-selection" style="left: 0%; width: 52%;"></div>
                        <div class="slider-handle triangle" style="left: 52%;"></div>
                        <div class="slider-handle triangle hide" style="left: 0%;"></div>
                    </div>
                    <div class="tooltip top" style="top: -30px; left: 125.4px;">
                        <div class="tooltip-arrow"></div>
                        <div class="tooltip-inner">86</div>
                    </div>
                    <input id="mySlider6" data-slider-handle="triangle" data-slider-min="0" data-slider-value="100" data-slider-max="200" type="text"></div>
                <br>
                <span id="mySliderValue6" class="textbox">96</span>

                <div class="scoring-single-img">
                    <?php echo $this->Html->image('banner.jpg', array('class' => 'scoring-img'));?>
                </div>
                <textarea class="form-control" rows="5" placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p"> Hip Sway</h4>

            <div class="scoring-body">
                <div class="slider slider-horizontal">
                    <div class="slider-track">
                        <div class="slider-selection" style="left: 0%; width: 50%;"></div>
                        <div class="slider-handle triangle" style="left: 50%;"></div>
                        <div class="slider-handle triangle hide" style="left: 0%;"></div>
                    </div>
                    <div class="tooltip top" style="top: -30px; left: 116.5px;">
                        <div class="tooltip-arrow"></div>
                        <div class="tooltip-inner">100</div>
                    </div>
                    <input id="mySlider7" data-slider-handle="triangle" data-slider-min="0" data-slider-value="100" data-slider-max="200" type="text"></div>
                <br>
                <span id="mySliderValue7" class="textbox">100</span>

                <div class="scoring-single-img">
                    <?php echo $this->Html->image('banner.jpg', array('class' => 'scoring-img'));?>
                </div>
                <textarea class="form-control" rows="5" placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p"> Hip Pivot </h4>

            <div class="scoring-body">
                <div class="slider slider-horizontal">
                    <div class="slider-track">
                        <div class="slider-selection" style="left: 0%; width: 50%;"></div>
                        <div class="slider-handle triangle" style="left: 50%;"></div>
                        <div class="slider-handle triangle hide" style="left: 0%;"></div>
                    </div>
                    <div class="tooltip top" style="top: -30px; left: 116.5px;">
                        <div class="tooltip-arrow"></div>
                        <div class="tooltip-inner">100</div>
                    </div>
                    <input id="mySlider8" data-slider-handle="triangle" data-slider-min="0" data-slider-value="100" data-slider-max="200" type="text"></div>
                <br>
                <span id="mySliderValue8" class="textbox">100</span>

                <div class="scoring-single-img">
                    <?php echo $this->Html->image('banner.jpg', array('class' => 'scoring-img'));?>
                </div>
                <textarea class="form-control" rows="5" placeholder="Add Notes Here"></textarea>
            </div>
        </div>
    </div>
</div>
<div class="margin-top-25"></div>
<h2 class="heading-sm u-p">Downswing: </h2>
<hr class="shadow-line">
<div class="container-fluid">
    <div class="row">
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">Rear</h4>

            <div class="scoring-body">
                <div class="slider slider-horizontal">
                    <div class="slider-track">
                        <div class="slider-selection" style="left: 0%; width: 50%;"></div>
                        <div class="slider-handle triangle" style="left: 50%;"></div>
                        <div class="slider-handle triangle hide" style="left: 0%;"></div>
                    </div>
                    <div class="tooltip top" style="top: -30px; left: 116.5px;">
                        <div class="tooltip-arrow"></div>
                        <div class="tooltip-inner">100</div>
                    </div>
                    <input id="mySlider5" data-slider-handle="triangle" data-slider-min="0" data-slider-value="100" data-slider-max="200" type="text"></div>
                <br>
                <span id="mySliderValue5" class="textbox">100</span>

                <div class="scoring-single-img">
                    <?php echo $this->Html->image('banner.jpg', array('class' => 'scoring-img'));?>
                </div>
                <textarea class="form-control" rows="5" placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">Knee Flex</h4>

            <div class="scoring-body">
                <div class="slider slider-horizontal">
                    <div class="slider-track">
                        <div class="slider-selection" style="left: 0%; width: 52%;"></div>
                        <div class="slider-handle triangle" style="left: 52%;"></div>
                        <div class="slider-handle triangle hide" style="left: 0%;"></div>
                    </div>
                    <div class="tooltip top" style="top: -30px; left: 125.4px;">
                        <div class="tooltip-arrow"></div>
                        <div class="tooltip-inner">86</div>
                    </div>
                    <input id="mySlider6" data-slider-handle="triangle" data-slider-min="0" data-slider-value="100" data-slider-max="200" type="text"></div>
                <br>
                <span id="mySliderValue6" class="textbox">96</span>

                <div class="scoring-single-img">
                    <?php echo $this->Html->image('banner.jpg', array('class' => 'scoring-img'));?>
                </div>
                <textarea class="form-control" rows="5" placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p"> Hip Sway</h4>

            <div class="scoring-body">
                <div class="slider slider-horizontal">
                    <div class="slider-track">
                        <div class="slider-selection" style="left: 0%; width: 50%;"></div>
                        <div class="slider-handle triangle" style="left: 50%;"></div>
                        <div class="slider-handle triangle hide" style="left: 0%;"></div>
                    </div>
                    <div class="tooltip top" style="top: -30px; left: 116.5px;">
                        <div class="tooltip-arrow"></div>
                        <div class="tooltip-inner">100</div>
                    </div>
                    <input id="mySlider7" data-slider-handle="triangle" data-slider-min="0" data-slider-value="100" data-slider-max="200" type="text"></div>
                <br>
                <span id="mySliderValue7" class="textbox">100</span>

                <div class="scoring-single-img">
                    <?php echo $this->Html->image('banner.jpg', array('class' => 'scoring-img'));?>
                </div>
                <textarea class="form-control" rows="5" placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p"> Hip Pivot </h4>

            <div class="scoring-body">
                <div class="slider slider-horizontal">
                    <div class="slider-track">
                        <div class="slider-selection" style="left: 0%; width: 50%;"></div>
                        <div class="slider-handle triangle" style="left: 50%;"></div>
                        <div class="slider-handle triangle hide" style="left: 0%;"></div>
                    </div>
                    <div class="tooltip top" style="top: -30px; left: 116.5px;">
                        <div class="tooltip-arrow"></div>
                        <div class="tooltip-inner">100</div>
                    </div>
                    <input id="mySlider8" data-slider-handle="triangle" data-slider-min="0" data-slider-value="100" data-slider-max="200" type="text"></div>
                <br>
                <span id="mySliderValue8" class="textbox">100</span>

                <div class="scoring-single-img">
                    <?php echo $this->Html->image('banner.jpg', array('class' => 'scoring-img'));?>
                </div>
                <textarea class="form-control" rows="5" placeholder="Add Notes Here"></textarea>
            </div>
        </div>
    </div>
</div>
<div class="margin-top-25"></div>
<h2 class="heading-sm u-p">Impact: </h2>
<hr class="shadow-line">
<div class="container-fluid">
    <div class="row">
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">Rear</h4>

            <div class="scoring-body">
                <div class="slider slider-horizontal">
                    <div class="slider-track">
                        <div class="slider-selection" style="left: 0%; width: 50%;"></div>
                        <div class="slider-handle triangle" style="left: 50%;"></div>
                        <div class="slider-handle triangle hide" style="left: 0%;"></div>
                    </div>
                    <div class="tooltip top" style="top: -30px; left: 116.5px;">
                        <div class="tooltip-arrow"></div>
                        <div class="tooltip-inner">100</div>
                    </div>
                    <input id="mySlider5" data-slider-handle="triangle" data-slider-min="0" data-slider-value="100" data-slider-max="200" type="text"></div>
                <br>
                <span id="mySliderValue5" class="textbox">100</span>

                <div class="scoring-single-img">
                    <?php echo $this->Html->image('banner.jpg', array('class' => 'scoring-img'));?>
                </div>
                <textarea class="form-control" rows="5" placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">Knee Flex</h4>

            <div class="scoring-body">
                <div class="slider slider-horizontal">
                    <div class="slider-track">
                        <div class="slider-selection" style="left: 0%; width: 52%;"></div>
                        <div class="slider-handle triangle" style="left: 52%;"></div>
                        <div class="slider-handle triangle hide" style="left: 0%;"></div>
                    </div>
                    <div class="tooltip top" style="top: -30px; left: 125.4px;">
                        <div class="tooltip-arrow"></div>
                        <div class="tooltip-inner">86</div>
                    </div>
                    <input id="mySlider6" data-slider-handle="triangle" data-slider-min="0" data-slider-value="100" data-slider-max="200" type="text"></div>
                <br>
                <span id="mySliderValue6" class="textbox">96</span>

                <div class="scoring-single-img">
                    <?php echo $this->Html->image('banner.jpg', array('class' => 'scoring-img'));?>
                </div>
                <textarea class="form-control" rows="5" placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p"> Hip Sway</h4>

            <div class="scoring-body">
                <div class="slider slider-horizontal">
                    <div class="slider-track">
                        <div class="slider-selection" style="left: 0%; width: 50%;"></div>
                        <div class="slider-handle triangle" style="left: 50%;"></div>
                        <div class="slider-handle triangle hide" style="left: 0%;"></div>
                    </div>
                    <div class="tooltip top" style="top: -30px; left: 116.5px;">
                        <div class="tooltip-arrow"></div>
                        <div class="tooltip-inner">100</div>
                    </div>
                    <input id="mySlider7" data-slider-handle="triangle" data-slider-min="0" data-slider-value="100" data-slider-max="200" type="text"></div>
                <br>
                <span id="mySliderValue7" class="textbox">100</span>

                <div class="scoring-single-img">
                    <?php echo $this->Html->image('banner.jpg', array('class' => 'scoring-img'));?>
                </div>
                <textarea class="form-control" rows="5" placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p"> Hip Pivot </h4>

            <div class="scoring-body">
                <div class="slider slider-horizontal">
                    <div class="slider-track">
                        <div class="slider-selection" style="left: 0%; width: 50%;"></div>
                        <div class="slider-handle triangle" style="left: 50%;"></div>
                        <div class="slider-handle triangle hide" style="left: 0%;"></div>
                    </div>
                    <div class="tooltip top" style="top: -30px; left: 116.5px;">
                        <div class="tooltip-arrow"></div>
                        <div class="tooltip-inner">100</div>
                    </div>
                    <input id="mySlider8" data-slider-handle="triangle" data-slider-min="0" data-slider-value="100" data-slider-max="200" type="text"></div>
                <br>
                <span id="mySliderValue8" class="textbox">100</span>

                <div class="scoring-single-img">
                    <?php echo $this->Html->image('banner.jpg', array('class' => 'scoring-img'));?>
                </div>
                <textarea class="form-control" rows="5" placeholder="Add Notes Here"></textarea>
            </div>
        </div>
    </div>
</div>
<div class="margin-top-25"></div>
<h2 class="heading-sm u-p">Follow Through: </h2>
<hr class="shadow-line">
<div class="container-fluid">
    <div class="row">
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">Rear</h4>

            <div class="scoring-body">
                <div class="slider slider-horizontal">
                    <div class="slider-track">
                        <div class="slider-selection" style="left: 0%; width: 50%;"></div>
                        <div class="slider-handle triangle" style="left: 50%;"></div>
                        <div class="slider-handle triangle hide" style="left: 0%;"></div>
                    </div>
                    <div class="tooltip top" style="top: -30px; left: 116.5px;">
                        <div class="tooltip-arrow"></div>
                        <div class="tooltip-inner">100</div>
                    </div>
                    <input id="mySlider5" data-slider-handle="triangle" data-slider-min="0" data-slider-value="100" data-slider-max="200" type="text"></div>
                <br>
                <span id="mySliderValue5" class="textbox">100</span>

                <div class="scoring-single-img">
                    <?php echo $this->Html->image('banner.jpg', array('class' => 'scoring-img'));?>
                </div>
                <textarea class="form-control" rows="5" placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">Knee Flex</h4>

            <div class="scoring-body">
                <div class="slider slider-horizontal">
                    <div class="slider-track">
                        <div class="slider-selection" style="left: 0%; width: 52%;"></div>
                        <div class="slider-handle triangle" style="left: 52%;"></div>
                        <div class="slider-handle triangle hide" style="left: 0%;"></div>
                    </div>
                    <div class="tooltip top" style="top: -30px; left: 125.4px;">
                        <div class="tooltip-arrow"></div>
                        <div class="tooltip-inner">86</div>
                    </div>
                    <input id="mySlider6" data-slider-handle="triangle" data-slider-min="0" data-slider-value="100" data-slider-max="200" type="text"></div>
                <br>
                <span id="mySliderValue6" class="textbox">96</span>

                <div class="scoring-single-img">
                    <?php echo $this->Html->image('banner.jpg', array('class' => 'scoring-img'));?>
                </div>
                <textarea class="form-control" rows="5" placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p"> Hip Sway</h4>

            <div class="scoring-body">
                <div class="slider slider-horizontal">
                    <div class="slider-track">
                        <div class="slider-selection" style="left: 0%; width: 50%;"></div>
                        <div class="slider-handle triangle" style="left: 50%;"></div>
                        <div class="slider-handle triangle hide" style="left: 0%;"></div>
                    </div>
                    <div class="tooltip top" style="top: -30px; left: 116.5px;">
                        <div class="tooltip-arrow"></div>
                        <div class="tooltip-inner">100</div>
                    </div>
                    <input id="mySlider7" data-slider-handle="triangle" data-slider-min="0" data-slider-value="100" data-slider-max="200" type="text"></div>
                <br>
                <span id="mySliderValue7" class="textbox">100</span>

                <div class="scoring-single-img">
                    <?php echo $this->Html->image('banner.jpg', array('class' => 'scoring-img'));?>
                </div>
                <textarea class="form-control" rows="5" placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p"> Hip Pivot </h4>

            <div class="scoring-body">
                <div class="slider slider-horizontal">
                    <div class="slider-track">
                        <div class="slider-selection" style="left: 0%; width: 50%;"></div>
                        <div class="slider-handle triangle" style="left: 50%;"></div>
                        <div class="slider-handle triangle hide" style="left: 0%;"></div>
                    </div>
                    <div class="tooltip top" style="top: -30px; left: 116.5px;">
                        <div class="tooltip-arrow"></div>
                        <div class="tooltip-inner">100</div>
                    </div>
                    <input id="mySlider8" data-slider-handle="triangle" data-slider-min="0" data-slider-value="100" data-slider-max="200" type="text"></div>
                <br>
                <span id="mySliderValue8" class="textbox">100</span>

                <div class="scoring-single-img">
                    <?php echo $this->Html->image('banner.jpg', array('class' => 'scoring-img'));?>
                </div>
                <textarea class="form-control" rows="5" placeholder="Add Notes Here"></textarea>
            </div>
        </div>
    </div>
</div>
<div class="margin-top-25"></div>
<h2 class="heading-sm u-p">Finish: </h2>
<hr class="shadow-line">
<div class="container-fluid">
    <div class="row">
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">Rear</h4>

            <div class="scoring-body">
                <div class="slider slider-horizontal">
                    <div class="slider-track">
                        <div class="slider-selection" style="left: 0%; width: 50%;"></div>
                        <div class="slider-handle triangle" style="left: 50%;"></div>
                        <div class="slider-handle triangle hide" style="left: 0%;"></div>
                    </div>
                    <div class="tooltip top" style="top: -30px; left: 116.5px;">
                        <div class="tooltip-arrow"></div>
                        <div class="tooltip-inner">100</div>
                    </div>
                    <input id="mySlider5" data-slider-handle="triangle" data-slider-min="0" data-slider-value="100" data-slider-max="200" type="text"></div>
                <br>
                <span id="mySliderValue5" class="textbox">100</span>

                <div class="scoring-single-img">
                    <?php echo $this->Html->image('banner.jpg', array('class' => 'scoring-img'));?>
                </div>
                <textarea class="form-control" rows="5" placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">Knee Flex</h4>

            <div class="scoring-body">
                <div class="slider slider-horizontal">
                    <div class="slider-track">
                        <div class="slider-selection" style="left: 0%; width: 52%;"></div>
                        <div class="slider-handle triangle" style="left: 52%;"></div>
                        <div class="slider-handle triangle hide" style="left: 0%;"></div>
                    </div>
                    <div class="tooltip top" style="top: -30px; left: 125.4px;">
                        <div class="tooltip-arrow"></div>
                        <div class="tooltip-inner">86</div>
                    </div>
                    <input id="mySlider6" data-slider-handle="triangle" data-slider-min="0" data-slider-value="100" data-slider-max="200" type="text"></div>
                <br>
                <span id="mySliderValue6" class="textbox">96</span>

                <div class="scoring-single-img">
                    <?php echo $this->Html->image('banner.jpg', array('class' => 'scoring-img'));?>
                </div>
                <textarea class="form-control" rows="5" placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p"> Hip Sway</h4>

            <div class="scoring-body">
                <div class="slider slider-horizontal">
                    <div class="slider-track">
                        <div class="slider-selection" style="left: 0%; width: 50%;"></div>
                        <div class="slider-handle triangle" style="left: 50%;"></div>
                        <div class="slider-handle triangle hide" style="left: 0%;"></div>
                    </div>
                    <div class="tooltip top" style="top: -30px; left: 116.5px;">
                        <div class="tooltip-arrow"></div>
                        <div class="tooltip-inner">100</div>
                    </div>
                    <input id="mySlider7" data-slider-handle="triangle" data-slider-min="0" data-slider-value="100" data-slider-max="200" type="text"></div>
                <br>
                <span id="mySliderValue7" class="textbox">100</span>

                <div class="scoring-single-img">
                    <?php echo $this->Html->image('banner.jpg', array('class' => 'scoring-img'));?>
                </div>
                <textarea class="form-control" rows="5" placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p"> Hip Pivot </h4>

            <div class="scoring-body">
                <div class="slider slider-horizontal">
                    <div class="slider-track">
                        <div class="slider-selection" style="left: 0%; width: 50%;"></div>
                        <div class="slider-handle triangle" style="left: 50%;"></div>
                        <div class="slider-handle triangle hide" style="left: 0%;"></div>
                    </div>
                    <div class="tooltip top" style="top: -30px; left: 116.5px;">
                        <div class="tooltip-arrow"></div>
                        <div class="tooltip-inner">100</div>
                    </div>
                    <input id="mySlider8" data-slider-handle="triangle" data-slider-min="0" data-slider-value="100" data-slider-max="200" type="text"></div>
                <br>
                <span id="mySliderValue8" class="textbox">100</span>

                <div class="scoring-single-img">
                    <?php echo $this->Html->image('banner.jpg', array('class' => 'scoring-img'));?>
                </div>
                <textarea class="form-control" rows="5" placeholder="Add Notes Here"></textarea>
            </div>
        </div>
    </div>
</div>
</div>
</div>
<!---- /Widget One ----->
<!---- Widget Two ----->
<div class="widget black-widget">
<div class="widget-header u-p">
    <i class="fa fa-bar-chart-o"></i> Upper Body Position
</div>
<div class="widget-body padding-15">
<h2 class="heading-sm u-p">Upper Body Halfway Back:</h2>
<hr class="shadow-line">
<div class="container-fluid">
    <div class="row">
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">Rotation</h4>

            <div class="scoring-body">
                <div class="slider slider-horizontal">
                    <div class="slider-track">
                        <div class="slider-selection" style="left: 0%; width: 50%;"></div>
                        <div class="slider-handle triangle" style="left: 50%;"></div>
                        <div class="slider-handle triangle hide" style="left: 0%;"></div>
                    </div>
                    <div class="tooltip top" style="top: -30px; left: 116.5px;">
                        <div class="tooltip-arrow"></div>
                        <div class="tooltip-inner">100</div>
                    </div>
                    <input id="mySlider" data-slider-handle="triangle" data-slider-min="0" data-slider-value="100" data-slider-max="200" type="text"></div>
                <br>
                <span id="mySliderValue" class="textbox">100</span>

                <div class="scoring-single-img">
                    <?php echo $this->Html->image('banner.jpg', array('class' => 'scoring-img'));?>
                </div>
                <textarea class="form-control" rows="5" placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">Spine Tilt</h4>

            <div class="scoring-body">
                <div class="slider slider-horizontal">
                    <div class="slider-track">
                        <div class="slider-selection" style="left: 0%; width: 52%;"></div>
                        <div class="slider-handle triangle" style="left: 52%;"></div>
                        <div class="slider-handle triangle hide" style="left: 0%;"></div>
                    </div>
                    <div class="tooltip top" style="top: -30px; left: 125.4px;">
                        <div class="tooltip-arrow"></div>
                        <div class="tooltip-inner">86</div>
                    </div>
                    <input id="mySlider2" data-slider-handle="triangle" data-slider-min="0" data-slider-value="100" data-slider-max="200" type="text"></div>
                <br>
                <span id="mySliderValue2" class="textbox">96</span>

                <div class="scoring-single-img">
                    <?php echo $this->Html->image('banner.jpg', array('class' => 'scoring-img'));?>
                </div>
                <textarea class="form-control" rows="5" placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">Head </h4>

            <div class="scoring-body">
                <div class="slider slider-horizontal">
                    <div class="slider-track">
                        <div class="slider-selection" style="left: 0%; width: 50%;"></div>
                        <div class="slider-handle triangle" style="left: 50%;"></div>
                        <div class="slider-handle triangle hide" style="left: 0%;"></div>
                    </div>
                    <div class="tooltip top" style="top: -30px; left: 116.5px;">
                        <div class="tooltip-arrow"></div>
                        <div class="tooltip-inner">100</div>
                    </div>
                    <input id="mySlider3" data-slider-handle="triangle" data-slider-min="0" data-slider-value="100" data-slider-max="200" type="text"></div>
                <br>
                <span id="mySliderValue3" class="textbox">100</span>

                <div class="scoring-single-img">
                    <?php echo $this->Html->image('banner.jpg', array('class' => 'scoring-img'));?>
                </div>
                <textarea class="form-control" rows="5" placeholder="Add Notes Here"></textarea>
            </div>
        </div>
    </div>
</div>
<div class="margin-top-25"></div>
<h2 class="heading-sm u-p">Upper Body ¾ Swing:</h2>
<hr class="shadow-line">
<div class="container-fluid">
    <div class="row">
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">Rotation</h4>

            <div class="scoring-body">
                <div class="slider slider-horizontal">
                    <div class="slider-track">
                        <div class="slider-selection" style="left: 0%; width: 50%;"></div>
                        <div class="slider-handle triangle" style="left: 50%;"></div>
                        <div class="slider-handle triangle hide" style="left: 0%;"></div>
                    </div>
                    <div class="tooltip top" style="top: -30px; left: 116.5px;">
                        <div class="tooltip-arrow"></div>
                        <div class="tooltip-inner">100</div>
                    </div>
                    <input id="mySlider" data-slider-handle="triangle" data-slider-min="0" data-slider-value="100" data-slider-max="200" type="text"></div>
                <br>
                <span id="mySliderValue" class="textbox">100</span>

                <div class="scoring-single-img">
                    <?php echo $this->Html->image('banner.jpg', array('class' => 'scoring-img'));?>
                </div>
                <textarea class="form-control" rows="5" placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">Spine Tilt</h4>

            <div class="scoring-body">
                <div class="slider slider-horizontal">
                    <div class="slider-track">
                        <div class="slider-selection" style="left: 0%; width: 52%;"></div>
                        <div class="slider-handle triangle" style="left: 52%;"></div>
                        <div class="slider-handle triangle hide" style="left: 0%;"></div>
                    </div>
                    <div class="tooltip top" style="top: -30px; left: 125.4px;">
                        <div class="tooltip-arrow"></div>
                        <div class="tooltip-inner">86</div>
                    </div>
                    <input id="mySlider2" data-slider-handle="triangle" data-slider-min="0" data-slider-value="100" data-slider-max="200" type="text"></div>
                <br>
                <span id="mySliderValue2" class="textbox">96</span>

                <div class="scoring-single-img">
                    <?php echo $this->Html->image('banner.jpg', array('class' => 'scoring-img'));?>
                </div>
                <textarea class="form-control" rows="5" placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">Head </h4>

            <div class="scoring-body">
                <div class="slider slider-horizontal">
                    <div class="slider-track">
                        <div class="slider-selection" style="left: 0%; width: 50%;"></div>
                        <div class="slider-handle triangle" style="left: 50%;"></div>
                        <div class="slider-handle triangle hide" style="left: 0%;"></div>
                    </div>
                    <div class="tooltip top" style="top: -30px; left: 116.5px;">
                        <div class="tooltip-arrow"></div>
                        <div class="tooltip-inner">100</div>
                    </div>
                    <input id="mySlider3" data-slider-handle="triangle" data-slider-min="0" data-slider-value="100" data-slider-max="200" type="text"></div>
                <br>
                <span id="mySliderValue3" class="textbox">100</span>

                <div class="scoring-single-img">
                    <?php echo $this->Html->image('banner.jpg', array('class' => 'scoring-img'));?>
                </div>
                <textarea class="form-control" rows="5" placeholder="Add Notes Here"></textarea>
            </div>
        </div>
    </div>
</div>
<div class="margin-top-25"></div>
<h2 class="heading-sm u-p">Upper Body At Top:</h2>
<hr class="shadow-line">
<div class="container-fluid">
    <div class="row">
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">Rotation</h4>

            <div class="scoring-body">
                <div class="slider slider-horizontal">
                    <div class="slider-track">
                        <div class="slider-selection" style="left: 0%; width: 50%;"></div>
                        <div class="slider-handle triangle" style="left: 50%;"></div>
                        <div class="slider-handle triangle hide" style="left: 0%;"></div>
                    </div>
                    <div class="tooltip top" style="top: -30px; left: 116.5px;">
                        <div class="tooltip-arrow"></div>
                        <div class="tooltip-inner">100</div>
                    </div>
                    <input id="mySlider" data-slider-handle="triangle" data-slider-min="0" data-slider-value="100" data-slider-max="200" type="text"></div>
                <br>
                <span id="mySliderValue" class="textbox">100</span>

                <div class="scoring-single-img">
                    <?php echo $this->Html->image('banner.jpg', array('class' => 'scoring-img'));?>
                </div>
                <textarea class="form-control" rows="5" placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">Spine Tilt</h4>

            <div class="scoring-body">
                <div class="slider slider-horizontal">
                    <div class="slider-track">
                        <div class="slider-selection" style="left: 0%; width: 52%;"></div>
                        <div class="slider-handle triangle" style="left: 52%;"></div>
                        <div class="slider-handle triangle hide" style="left: 0%;"></div>
                    </div>
                    <div class="tooltip top" style="top: -30px; left: 125.4px;">
                        <div class="tooltip-arrow"></div>
                        <div class="tooltip-inner">86</div>
                    </div>
                    <input id="mySlider2" data-slider-handle="triangle" data-slider-min="0" data-slider-value="100" data-slider-max="200" type="text"></div>
                <br>
                <span id="mySliderValue2" class="textbox">96</span>

                <div class="scoring-single-img">
                    <?php echo $this->Html->image('banner.jpg', array('class' => 'scoring-img'));?>
                </div>
                <textarea class="form-control" rows="5" placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">Head </h4>

            <div class="scoring-body">
                <div class="slider slider-horizontal">
                    <div class="slider-track">
                        <div class="slider-selection" style="left: 0%; width: 50%;"></div>
                        <div class="slider-handle triangle" style="left: 50%;"></div>
                        <div class="slider-handle triangle hide" style="left: 0%;"></div>
                    </div>
                    <div class="tooltip top" style="top: -30px; left: 116.5px;">
                        <div class="tooltip-arrow"></div>
                        <div class="tooltip-inner">100</div>
                    </div>
                    <input id="mySlider3" data-slider-handle="triangle" data-slider-min="0" data-slider-value="100" data-slider-max="200" type="text"></div>
                <br>
                <span id="mySliderValue3" class="textbox">100</span>

                <div class="scoring-single-img">
                    <?php echo $this->Html->image('banner.jpg', array('class' => 'scoring-img'));?>
                </div>
                <textarea class="form-control" rows="5" placeholder="Add Notes Here"></textarea>
            </div>
        </div>
    </div>
</div>
<div class="margin-top-25"></div>
<h2 class="heading-sm u-p">Upper Body At Downswing:</h2>
<hr class="shadow-line">
<div class="container-fluid">
    <div class="row">
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">Rotation</h4>

            <div class="scoring-body">
                <div class="slider slider-horizontal">
                    <div class="slider-track">
                        <div class="slider-selection" style="left: 0%; width: 50%;"></div>
                        <div class="slider-handle triangle" style="left: 50%;"></div>
                        <div class="slider-handle triangle hide" style="left: 0%;"></div>
                    </div>
                    <div class="tooltip top" style="top: -30px; left: 116.5px;">
                        <div class="tooltip-arrow"></div>
                        <div class="tooltip-inner">100</div>
                    </div>
                    <input id="mySlider" data-slider-handle="triangle" data-slider-min="0" data-slider-value="100" data-slider-max="200" type="text"></div>
                <br>
                <span id="mySliderValue" class="textbox">100</span>

                <div class="scoring-single-img">
                    <?php echo $this->Html->image('banner.jpg', array('class' => 'scoring-img'));?>
                </div>
                <textarea class="form-control" rows="5" placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">Spine Tilt</h4>

            <div class="scoring-body">
                <div class="slider slider-horizontal">
                    <div class="slider-track">
                        <div class="slider-selection" style="left: 0%; width: 52%;"></div>
                        <div class="slider-handle triangle" style="left: 52%;"></div>
                        <div class="slider-handle triangle hide" style="left: 0%;"></div>
                    </div>
                    <div class="tooltip top" style="top: -30px; left: 125.4px;">
                        <div class="tooltip-arrow"></div>
                        <div class="tooltip-inner">86</div>
                    </div>
                    <input id="mySlider2" data-slider-handle="triangle" data-slider-min="0" data-slider-value="100" data-slider-max="200" type="text"></div>
                <br>
                <span id="mySliderValue2" class="textbox">96</span>

                <div class="scoring-single-img">
                    <?php echo $this->Html->image('banner.jpg', array('class' => 'scoring-img'));?>
                </div>
                <textarea class="form-control" rows="5" placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">Head </h4>

            <div class="scoring-body">
                <div class="slider slider-horizontal">
                    <div class="slider-track">
                        <div class="slider-selection" style="left: 0%; width: 50%;"></div>
                        <div class="slider-handle triangle" style="left: 50%;"></div>
                        <div class="slider-handle triangle hide" style="left: 0%;"></div>
                    </div>
                    <div class="tooltip top" style="top: -30px; left: 116.5px;">
                        <div class="tooltip-arrow"></div>
                        <div class="tooltip-inner">100</div>
                    </div>
                    <input id="mySlider3" data-slider-handle="triangle" data-slider-min="0" data-slider-value="100" data-slider-max="200" type="text"></div>
                <br>
                <span id="mySliderValue3" class="textbox">100</span>

                <div class="scoring-single-img">
                    <?php echo $this->Html->image('banner.jpg', array('class' => 'scoring-img'));?>
                </div>
                <textarea class="form-control" rows="5" placeholder="Add Notes Here"></textarea>
            </div>
        </div>
    </div>
</div>
<div class="margin-top-25"></div>
<h2 class="heading-sm u-p">Upper Body At Impact:</h2>
<hr class="shadow-line">
<div class="container-fluid">
    <div class="row">
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">Rotation</h4>

            <div class="scoring-body">
                <div class="slider slider-horizontal">
                    <div class="slider-track">
                        <div class="slider-selection" style="left: 0%; width: 50%;"></div>
                        <div class="slider-handle triangle" style="left: 50%;"></div>
                        <div class="slider-handle triangle hide" style="left: 0%;"></div>
                    </div>
                    <div class="tooltip top" style="top: -30px; left: 116.5px;">
                        <div class="tooltip-arrow"></div>
                        <div class="tooltip-inner">100</div>
                    </div>
                    <input id="mySlider" data-slider-handle="triangle" data-slider-min="0" data-slider-value="100" data-slider-max="200" type="text"></div>
                <br>
                <span id="mySliderValue" class="textbox">100</span>

                <div class="scoring-single-img">
                    <?php echo $this->Html->image('banner.jpg', array('class' => 'scoring-img'));?>
                </div>
                <textarea class="form-control" rows="5" placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">Spine Tilt</h4>

            <div class="scoring-body">
                <div class="slider slider-horizontal">
                    <div class="slider-track">
                        <div class="slider-selection" style="left: 0%; width: 52%;"></div>
                        <div class="slider-handle triangle" style="left: 52%;"></div>
                        <div class="slider-handle triangle hide" style="left: 0%;"></div>
                    </div>
                    <div class="tooltip top" style="top: -30px; left: 125.4px;">
                        <div class="tooltip-arrow"></div>
                        <div class="tooltip-inner">86</div>
                    </div>
                    <input id="mySlider2" data-slider-handle="triangle" data-slider-min="0" data-slider-value="100" data-slider-max="200" type="text"></div>
                <br>
                <span id="mySliderValue2" class="textbox">96</span>

                <div class="scoring-single-img">
                    <?php echo $this->Html->image('banner.jpg', array('class' => 'scoring-img'));?>
                </div>
                <textarea class="form-control" rows="5" placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">Head </h4>

            <div class="scoring-body">
                <div class="slider slider-horizontal">
                    <div class="slider-track">
                        <div class="slider-selection" style="left: 0%; width: 50%;"></div>
                        <div class="slider-handle triangle" style="left: 50%;"></div>
                        <div class="slider-handle triangle hide" style="left: 0%;"></div>
                    </div>
                    <div class="tooltip top" style="top: -30px; left: 116.5px;">
                        <div class="tooltip-arrow"></div>
                        <div class="tooltip-inner">100</div>
                    </div>
                    <input id="mySlider3" data-slider-handle="triangle" data-slider-min="0" data-slider-value="100" data-slider-max="200" type="text"></div>
                <br>
                <span id="mySliderValue3" class="textbox">100</span>

                <div class="scoring-single-img">
                    <?php echo $this->Html->image('banner.jpg', array('class' => 'scoring-img'));?>
                </div>
                <textarea class="form-control" rows="5" placeholder="Add Notes Here"></textarea>
            </div>
        </div>
    </div>
</div>
<div class="margin-top-25"></div>
<h2 class="heading-sm u-p">Upper Body Follow Through:</h2>
<hr class="shadow-line">
<div class="container-fluid">
    <div class="row">
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">Rotation</h4>

            <div class="scoring-body">
                <div class="slider slider-horizontal">
                    <div class="slider-track">
                        <div class="slider-selection" style="left: 0%; width: 50%;"></div>
                        <div class="slider-handle triangle" style="left: 50%;"></div>
                        <div class="slider-handle triangle hide" style="left: 0%;"></div>
                    </div>
                    <div class="tooltip top" style="top: -30px; left: 116.5px;">
                        <div class="tooltip-arrow"></div>
                        <div class="tooltip-inner">100</div>
                    </div>
                    <input id="mySlider" data-slider-handle="triangle" data-slider-min="0" data-slider-value="100" data-slider-max="200" type="text"></div>
                <br>
                <span id="mySliderValue" class="textbox">100</span>

                <div class="scoring-single-img">
                    <?php echo $this->Html->image('banner.jpg', array('class' => 'scoring-img'));?>
                </div>
                <textarea class="form-control" rows="5" placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">Spine Tilt</h4>

            <div class="scoring-body">
                <div class="slider slider-horizontal">
                    <div class="slider-track">
                        <div class="slider-selection" style="left: 0%; width: 52%;"></div>
                        <div class="slider-handle triangle" style="left: 52%;"></div>
                        <div class="slider-handle triangle hide" style="left: 0%;"></div>
                    </div>
                    <div class="tooltip top" style="top: -30px; left: 125.4px;">
                        <div class="tooltip-arrow"></div>
                        <div class="tooltip-inner">86</div>
                    </div>
                    <input id="mySlider2" data-slider-handle="triangle" data-slider-min="0" data-slider-value="100" data-slider-max="200" type="text"></div>
                <br>
                <span id="mySliderValue2" class="textbox">96</span>

                <div class="scoring-single-img">
                    <?php echo $this->Html->image('banner.jpg', array('class' => 'scoring-img'));?>
                </div>
                <textarea class="form-control" rows="5" placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">Head </h4>

            <div class="scoring-body">
                <div class="slider slider-horizontal">
                    <div class="slider-track">
                        <div class="slider-selection" style="left: 0%; width: 50%;"></div>
                        <div class="slider-handle triangle" style="left: 50%;"></div>
                        <div class="slider-handle triangle hide" style="left: 0%;"></div>
                    </div>
                    <div class="tooltip top" style="top: -30px; left: 116.5px;">
                        <div class="tooltip-arrow"></div>
                        <div class="tooltip-inner">100</div>
                    </div>
                    <input id="mySlider3" data-slider-handle="triangle" data-slider-min="0" data-slider-value="100" data-slider-max="200" type="text"></div>
                <br>
                <span id="mySliderValue3" class="textbox">100</span>

                <div class="scoring-single-img">
                    <?php echo $this->Html->image('banner.jpg', array('class' => 'scoring-img'));?>
                </div>
                <textarea class="form-control" rows="5" placeholder="Add Notes Here"></textarea>
            </div>
        </div>
    </div>
</div>
<div class="margin-top-25"></div>
<h2 class="heading-sm u-p">Upper Body Follow Finish:</h2>
<hr class="shadow-line">
<div class="container-fluid">
    <div class="row">
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">Rotation</h4>

            <div class="scoring-body">
                <div class="slider slider-horizontal">
                    <div class="slider-track">
                        <div class="slider-selection" style="left: 0%; width: 50%;"></div>
                        <div class="slider-handle triangle" style="left: 50%;"></div>
                        <div class="slider-handle triangle hide" style="left: 0%;"></div>
                    </div>
                    <div class="tooltip top" style="top: -30px; left: 116.5px;">
                        <div class="tooltip-arrow"></div>
                        <div class="tooltip-inner">100</div>
                    </div>
                    <input id="mySlider" data-slider-handle="triangle" data-slider-min="0" data-slider-value="100" data-slider-max="200" type="text"></div>
                <br>
                <span id="mySliderValue" class="textbox">100</span>

                <div class="scoring-single-img">
                    <?php echo $this->Html->image('banner.jpg', array('class' => 'scoring-img'));?>
                </div>
                <textarea class="form-control" rows="5" placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">Spine Tilt</h4>

            <div class="scoring-body">
                <div class="slider slider-horizontal">
                    <div class="slider-track">
                        <div class="slider-selection" style="left: 0%; width: 52%;"></div>
                        <div class="slider-handle triangle" style="left: 52%;"></div>
                        <div class="slider-handle triangle hide" style="left: 0%;"></div>
                    </div>
                    <div class="tooltip top" style="top: -30px; left: 125.4px;">
                        <div class="tooltip-arrow"></div>
                        <div class="tooltip-inner">86</div>
                    </div>
                    <input id="mySlider2" data-slider-handle="triangle" data-slider-min="0" data-slider-value="100" data-slider-max="200" type="text"></div>
                <br>
                <span id="mySliderValue2" class="textbox">96</span>

                <div class="scoring-single-img">
                    <?php echo $this->Html->image('banner.jpg', array('class' => 'scoring-img'));?>
                </div>
                <textarea class="form-control" rows="5" placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">Head </h4>

            <div class="scoring-body">
                <div class="slider slider-horizontal">
                    <div class="slider-track">
                        <div class="slider-selection" style="left: 0%; width: 50%;"></div>
                        <div class="slider-handle triangle" style="left: 50%;"></div>
                        <div class="slider-handle triangle hide" style="left: 0%;"></div>
                    </div>
                    <div class="tooltip top" style="top: -30px; left: 116.5px;">
                        <div class="tooltip-arrow"></div>
                        <div class="tooltip-inner">100</div>
                    </div>
                    <input id="mySlider3" data-slider-handle="triangle" data-slider-min="0" data-slider-value="100" data-slider-max="200" type="text"></div>
                <br>
                <span id="mySliderValue3" class="textbox">100</span>

                <div class="scoring-single-img">
                    <?php echo $this->Html->image('banner.jpg', array('class' => 'scoring-img'));?>
                </div>
                <textarea class="form-control" rows="5" placeholder="Add Notes Here"></textarea>
            </div>
        </div>
    </div>
</div>
<div class="margin-top-25"></div>
<h2 class="heading-sm u-p">Connection Halfway Back:</h2>
<hr class="shadow-line">
<div class="container-fluid">
    <div class="row">
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">Triangle Shape</h4>

            <div class="scoring-body">
                <div class="slider slider-horizontal">
                    <div class="slider-track">
                        <div class="slider-selection" style="left: 0%; width: 50%;"></div>
                        <div class="slider-handle triangle" style="left: 50%;"></div>
                        <div class="slider-handle triangle hide" style="left: 0%;"></div>
                    </div>
                    <div class="tooltip top" style="top: -30px; left: 116.5px;">
                        <div class="tooltip-arrow"></div>
                        <div class="tooltip-inner">100</div>
                    </div>
                    <input id="mySlider" data-slider-handle="triangle" data-slider-min="0" data-slider-value="100" data-slider-max="200" type="text"></div>
                <br>
                <span id="mySliderValue" class="textbox">100</span>

                <div class="scoring-single-img">
                    <?php echo $this->Html->image('banner.jpg', array('class' => 'scoring-img'));?>
                </div>
                <textarea class="form-control" rows="5" placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">Arms Position
            </h4>

            <div class="scoring-body">
                <div class="slider slider-horizontal">
                    <div class="slider-track">
                        <div class="slider-selection" style="left: 0%; width: 52%;"></div>
                        <div class="slider-handle triangle" style="left: 52%;"></div>
                        <div class="slider-handle triangle hide" style="left: 0%;"></div>
                    </div>
                    <div class="tooltip top" style="top: -30px; left: 125.4px;">
                        <div class="tooltip-arrow"></div>
                        <div class="tooltip-inner">86</div>
                    </div>
                    <input id="mySlider2" data-slider-handle="triangle" data-slider-min="0" data-slider-value="100" data-slider-max="200" type="text"></div>
                <br>
                <span id="mySliderValue2" class="textbox">96</span>

                <div class="scoring-single-img">
                    <?php echo $this->Html->image('banner.jpg', array('class' => 'scoring-img'));?>
                </div>
                <textarea class="form-control" rows="5" placeholder="Add Notes Here"></textarea>
            </div>
        </div>
    </div>
</div>
<div class="margin-top-25"></div>
<h2 class="heading-sm u-p">Connection ¾ Swing:</h2>
<hr class="shadow-line">
<div class="container-fluid">
    <div class="row">
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">Triangle Shape</h4>

            <div class="scoring-body">
                <div class="slider slider-horizontal">
                    <div class="slider-track">
                        <div class="slider-selection" style="left: 0%; width: 50%;"></div>
                        <div class="slider-handle triangle" style="left: 50%;"></div>
                        <div class="slider-handle triangle hide" style="left: 0%;"></div>
                    </div>
                    <div class="tooltip top" style="top: -30px; left: 116.5px;">
                        <div class="tooltip-arrow"></div>
                        <div class="tooltip-inner">100</div>
                    </div>
                    <input id="mySlider" data-slider-handle="triangle" data-slider-min="0" data-slider-value="100" data-slider-max="200" type="text"></div>
                <br>
                <span id="mySliderValue" class="textbox">100</span>

                <div class="scoring-single-img">
                    <?php echo $this->Html->image('banner.jpg', array('class' => 'scoring-img'));?>
                </div>
                <textarea class="form-control" rows="5" placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">Arms Position
            </h4>

            <div class="scoring-body">
                <div class="slider slider-horizontal">
                    <div class="slider-track">
                        <div class="slider-selection" style="left: 0%; width: 52%;"></div>
                        <div class="slider-handle triangle" style="left: 52%;"></div>
                        <div class="slider-handle triangle hide" style="left: 0%;"></div>
                    </div>
                    <div class="tooltip top" style="top: -30px; left: 125.4px;">
                        <div class="tooltip-arrow"></div>
                        <div class="tooltip-inner">86</div>
                    </div>
                    <input id="mySlider2" data-slider-handle="triangle" data-slider-min="0" data-slider-value="100" data-slider-max="200" type="text"></div>
                <br>
                <span id="mySliderValue2" class="textbox">96</span>

                <div class="scoring-single-img">
                    <?php echo $this->Html->image('banner.jpg', array('class' => 'scoring-img'));?>
                </div>
                <textarea class="form-control" rows="5" placeholder="Add Notes Here"></textarea>
            </div>
        </div>
    </div>
</div>
<div class="margin-top-25"></div>
<h2 class="heading-sm u-p">Connection At Top:</h2>
<hr class="shadow-line">
<div class="container-fluid">
    <div class="row">
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">Triangle Shape</h4>

            <div class="scoring-body">
                <div class="slider slider-horizontal">
                    <div class="slider-track">
                        <div class="slider-selection" style="left: 0%; width: 50%;"></div>
                        <div class="slider-handle triangle" style="left: 50%;"></div>
                        <div class="slider-handle triangle hide" style="left: 0%;"></div>
                    </div>
                    <div class="tooltip top" style="top: -30px; left: 116.5px;">
                        <div class="tooltip-arrow"></div>
                        <div class="tooltip-inner">100</div>
                    </div>
                    <input id="mySlider" data-slider-handle="triangle" data-slider-min="0" data-slider-value="100" data-slider-max="200" type="text"></div>
                <br>
                <span id="mySliderValue" class="textbox">100</span>

                <div class="scoring-single-img">
                    <?php echo $this->Html->image('banner.jpg', array('class' => 'scoring-img'));?>
                </div>
                <textarea class="form-control" rows="5" placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">Arms Position
            </h4>

            <div class="scoring-body">
                <div class="slider slider-horizontal">
                    <div class="slider-track">
                        <div class="slider-selection" style="left: 0%; width: 52%;"></div>
                        <div class="slider-handle triangle" style="left: 52%;"></div>
                        <div class="slider-handle triangle hide" style="left: 0%;"></div>
                    </div>
                    <div class="tooltip top" style="top: -30px; left: 125.4px;">
                        <div class="tooltip-arrow"></div>
                        <div class="tooltip-inner">86</div>
                    </div>
                    <input id="mySlider2" data-slider-handle="triangle" data-slider-min="0" data-slider-value="100" data-slider-max="200" type="text"></div>
                <br>
                <span id="mySliderValue2" class="textbox">96</span>

                <div class="scoring-single-img">
                    <?php echo $this->Html->image('banner.jpg', array('class' => 'scoring-img'));?>
                </div>
                <textarea class="form-control" rows="5" placeholder="Add Notes Here"></textarea>
            </div>
        </div>
    </div>
</div>
<div class="margin-top-25"></div>
<h2 class="heading-sm u-p">Connection Downswing:</h2>
<hr class="shadow-line">
<div class="container-fluid">
    <div class="row">
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">Triangle Shape</h4>

            <div class="scoring-body">
                <div class="slider slider-horizontal">
                    <div class="slider-track">
                        <div class="slider-selection" style="left: 0%; width: 50%;"></div>
                        <div class="slider-handle triangle" style="left: 50%;"></div>
                        <div class="slider-handle triangle hide" style="left: 0%;"></div>
                    </div>
                    <div class="tooltip top" style="top: -30px; left: 116.5px;">
                        <div class="tooltip-arrow"></div>
                        <div class="tooltip-inner">100</div>
                    </div>
                    <input id="mySlider" data-slider-handle="triangle" data-slider-min="0" data-slider-value="100" data-slider-max="200" type="text"></div>
                <br>
                <span id="mySliderValue" class="textbox">100</span>

                <div class="scoring-single-img">
                    <?php echo $this->Html->image('banner.jpg', array('class' => 'scoring-img'));?>
                </div>
                <textarea class="form-control" rows="5" placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">Arms Position
            </h4>

            <div class="scoring-body">
                <div class="slider slider-horizontal">
                    <div class="slider-track">
                        <div class="slider-selection" style="left: 0%; width: 52%;"></div>
                        <div class="slider-handle triangle" style="left: 52%;"></div>
                        <div class="slider-handle triangle hide" style="left: 0%;"></div>
                    </div>
                    <div class="tooltip top" style="top: -30px; left: 125.4px;">
                        <div class="tooltip-arrow"></div>
                        <div class="tooltip-inner">86</div>
                    </div>
                    <input id="mySlider2" data-slider-handle="triangle" data-slider-min="0" data-slider-value="100" data-slider-max="200" type="text"></div>
                <br>
                <span id="mySliderValue2" class="textbox">96</span>

                <div class="scoring-single-img">
                    <?php echo $this->Html->image('banner.jpg', array('class' => 'scoring-img'));?>
                </div>
                <textarea class="form-control" rows="5" placeholder="Add Notes Here"></textarea>
            </div>
        </div>
    </div>
</div>
<div class="margin-top-25"></div>
<h2 class="heading-sm u-p">Connection Impact: </h2>
<hr class="shadow-line">
<div class="container-fluid">
    <div class="row">
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">Triangle Shape</h4>

            <div class="scoring-body">
                <div class="slider slider-horizontal">
                    <div class="slider-track">
                        <div class="slider-selection" style="left: 0%; width: 50%;"></div>
                        <div class="slider-handle triangle" style="left: 50%;"></div>
                        <div class="slider-handle triangle hide" style="left: 0%;"></div>
                    </div>
                    <div class="tooltip top" style="top: -30px; left: 116.5px;">
                        <div class="tooltip-arrow"></div>
                        <div class="tooltip-inner">100</div>
                    </div>
                    <input id="mySlider" data-slider-handle="triangle" data-slider-min="0" data-slider-value="100" data-slider-max="200" type="text"></div>
                <br>
                <span id="mySliderValue" class="textbox">100</span>

                <div class="scoring-single-img">
                    <?php echo $this->Html->image('banner.jpg', array('class' => 'scoring-img'));?>
                </div>
                <textarea class="form-control" rows="5" placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">Arms Position
            </h4>

            <div class="scoring-body">
                <div class="slider slider-horizontal">
                    <div class="slider-track">
                        <div class="slider-selection" style="left: 0%; width: 52%;"></div>
                        <div class="slider-handle triangle" style="left: 52%;"></div>
                        <div class="slider-handle triangle hide" style="left: 0%;"></div>
                    </div>
                    <div class="tooltip top" style="top: -30px; left: 125.4px;">
                        <div class="tooltip-arrow"></div>
                        <div class="tooltip-inner">86</div>
                    </div>
                    <input id="mySlider2" data-slider-handle="triangle" data-slider-min="0" data-slider-value="100" data-slider-max="200" type="text"></div>
                <br>
                <span id="mySliderValue2" class="textbox">96</span>

                <div class="scoring-single-img">
                    <?php echo $this->Html->image('banner.jpg', array('class' => 'scoring-img'));?>
                </div>
                <textarea class="form-control" rows="5" placeholder="Add Notes Here"></textarea>
            </div>
        </div>
    </div>
</div>
<div class="margin-top-25"></div>
<h2 class="heading-sm u-p">Connection Follow Through:  </h2>
<hr class="shadow-line">
<div class="container-fluid">
    <div class="row">
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">Triangle Shape</h4>

            <div class="scoring-body">
                <div class="slider slider-horizontal">
                    <div class="slider-track">
                        <div class="slider-selection" style="left: 0%; width: 50%;"></div>
                        <div class="slider-handle triangle" style="left: 50%;"></div>
                        <div class="slider-handle triangle hide" style="left: 0%;"></div>
                    </div>
                    <div class="tooltip top" style="top: -30px; left: 116.5px;">
                        <div class="tooltip-arrow"></div>
                        <div class="tooltip-inner">100</div>
                    </div>
                    <input id="mySlider" data-slider-handle="triangle" data-slider-min="0" data-slider-value="100" data-slider-max="200" type="text"></div>
                <br>
                <span id="mySliderValue" class="textbox">100</span>

                <div class="scoring-single-img">
                    <?php echo $this->Html->image('banner.jpg', array('class' => 'scoring-img'));?>
                </div>
                <textarea class="form-control" rows="5" placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">Arms Position
            </h4>

            <div class="scoring-body">
                <div class="slider slider-horizontal">
                    <div class="slider-track">
                        <div class="slider-selection" style="left: 0%; width: 52%;"></div>
                        <div class="slider-handle triangle" style="left: 52%;"></div>
                        <div class="slider-handle triangle hide" style="left: 0%;"></div>
                    </div>
                    <div class="tooltip top" style="top: -30px; left: 125.4px;">
                        <div class="tooltip-arrow"></div>
                        <div class="tooltip-inner">86</div>
                    </div>
                    <input id="mySlider2" data-slider-handle="triangle" data-slider-min="0" data-slider-value="100" data-slider-max="200" type="text"></div>
                <br>
                <span id="mySliderValue2" class="textbox">96</span>

                <div class="scoring-single-img">
                    <?php echo $this->Html->image('banner.jpg', array('class' => 'scoring-img'));?>
                </div>
                <textarea class="form-control" rows="5" placeholder="Add Notes Here"></textarea>
            </div>
        </div>
    </div>
</div>
<div class="margin-top-25"></div>
<h2 class="heading-sm u-p">Connection Finish:  </h2>
<hr class="shadow-line">
<div class="container-fluid">
    <div class="row">
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">Triangle Shape</h4>

            <div class="scoring-body">
                <div class="slider slider-horizontal">
                    <div class="slider-track">
                        <div class="slider-selection" style="left: 0%; width: 50%;"></div>
                        <div class="slider-handle triangle" style="left: 50%;"></div>
                        <div class="slider-handle triangle hide" style="left: 0%;"></div>
                    </div>
                    <div class="tooltip top" style="top: -30px; left: 116.5px;">
                        <div class="tooltip-arrow"></div>
                        <div class="tooltip-inner">100</div>
                    </div>
                    <input id="mySlider" data-slider-handle="triangle" data-slider-min="0" data-slider-value="100" data-slider-max="200" type="text"></div>
                <br>
                <span id="mySliderValue" class="textbox">100</span>

                <div class="scoring-single-img">
                    <?php echo $this->Html->image('banner.jpg', array('class' => 'scoring-img'));?>
                </div>
                <textarea class="form-control" rows="5" placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">Arms Position
            </h4>

            <div class="scoring-body">
                <div class="slider slider-horizontal">
                    <div class="slider-track">
                        <div class="slider-selection" style="left: 0%; width: 52%;"></div>
                        <div class="slider-handle triangle" style="left: 52%;"></div>
                        <div class="slider-handle triangle hide" style="left: 0%;"></div>
                    </div>
                    <div class="tooltip top" style="top: -30px; left: 125.4px;">
                        <div class="tooltip-arrow"></div>
                        <div class="tooltip-inner">86</div>
                    </div>
                    <input id="mySlider2" data-slider-handle="triangle" data-slider-min="0" data-slider-value="100" data-slider-max="200" type="text"></div>
                <br>
                <span id="mySliderValue2" class="textbox">96</span>

                <div class="scoring-single-img">
                    <?php echo $this->Html->image('banner.jpg', array('class' => 'scoring-img'));?>
                </div>
                <textarea class="form-control" rows="5" placeholder="Add Notes Here"></textarea>
            </div>
        </div>
    </div>
</div>
<div class="margin-top-25"></div>
<h2 class="heading-sm u-p">Swing Plane Halfway Back: </h2>
<hr class="shadow-line">
<div class="container-fluid">
    <div class="row">
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p"> Club Shaft Position</h4>

            <div class="scoring-body">
                <div class="slider slider-horizontal">
                    <div class="slider-track">
                        <div class="slider-selection" style="left: 0%; width: 50%;"></div>
                        <div class="slider-handle triangle" style="left: 50%;"></div>
                        <div class="slider-handle triangle hide" style="left: 0%;"></div>
                    </div>
                    <div class="tooltip top" style="top: -30px; left: 116.5px;">
                        <div class="tooltip-arrow"></div>
                        <div class="tooltip-inner">100</div>
                    </div>
                    <input id="mySlider" data-slider-handle="triangle" data-slider-min="0" data-slider-value="100" data-slider-max="200" type="text"></div>
                <br>
                <span id="mySliderValue" class="textbox">100</span>

                <div class="scoring-single-img">
                    <?php echo $this->Html->image('banner.jpg', array('class' => 'scoring-img'));?>
                </div>
                <textarea class="form-control" rows="5" placeholder="Add Notes Here"></textarea>
            </div>
        </div>
    </div>
</div>
<div class="margin-top-25"></div>
<h2 class="heading-sm u-p">Swing Plane ¾ Swing:  </h2>
<hr class="shadow-line">
<div class="container-fluid">
    <div class="row">
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p"> Club Shaft Position</h4>

            <div class="scoring-body">
                <div class="slider slider-horizontal">
                    <div class="slider-track">
                        <div class="slider-selection" style="left: 0%; width: 50%;"></div>
                        <div class="slider-handle triangle" style="left: 50%;"></div>
                        <div class="slider-handle triangle hide" style="left: 0%;"></div>
                    </div>
                    <div class="tooltip top" style="top: -30px; left: 116.5px;">
                        <div class="tooltip-arrow"></div>
                        <div class="tooltip-inner">100</div>
                    </div>
                    <input id="mySlider" data-slider-handle="triangle" data-slider-min="0" data-slider-value="100" data-slider-max="200" type="text"></div>
                <br>
                <span id="mySliderValue" class="textbox">100</span>

                <div class="scoring-single-img">
                    <?php echo $this->Html->image('banner.jpg', array('class' => 'scoring-img'));?>
                </div>
                <textarea class="form-control" rows="5" placeholder="Add Notes Here"></textarea>
            </div>
        </div>
    </div>
</div>
<div class="margin-top-25"></div>
<h2 class="heading-sm u-p">Swing Plane at Top:  </h2>
<hr class="shadow-line">
<div class="container-fluid">
    <div class="row">
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p"> Club Shaft Position</h4>

            <div class="scoring-body">
                <div class="slider slider-horizontal">
                    <div class="slider-track">
                        <div class="slider-selection" style="left: 0%; width: 50%;"></div>
                        <div class="slider-handle triangle" style="left: 50%;"></div>
                        <div class="slider-handle triangle hide" style="left: 0%;"></div>
                    </div>
                    <div class="tooltip top" style="top: -30px; left: 116.5px;">
                        <div class="tooltip-arrow"></div>
                        <div class="tooltip-inner">100</div>
                    </div>
                    <input id="mySlider" data-slider-handle="triangle" data-slider-min="0" data-slider-value="100" data-slider-max="200" type="text"></div>
                <br>
                <span id="mySliderValue" class="textbox">100</span>

                <div class="scoring-single-img">
                    <?php echo $this->Html->image('banner.jpg', array('class' => 'scoring-img'));?>
                </div>
                <textarea class="form-control" rows="5" placeholder="Add Notes Here"></textarea>
            </div>
        </div>
    </div>
</div>
<div class="margin-top-25"></div>
<h2 class="heading-sm u-p">Swing Plane Downswing: </h2>
<hr class="shadow-line">
<div class="container-fluid">
    <div class="row">
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p"> Club Shaft Position</h4>

            <div class="scoring-body">
                <div class="slider slider-horizontal">
                    <div class="slider-track">
                        <div class="slider-selection" style="left: 0%; width: 50%;"></div>
                        <div class="slider-handle triangle" style="left: 50%;"></div>
                        <div class="slider-handle triangle hide" style="left: 0%;"></div>
                    </div>
                    <div class="tooltip top" style="top: -30px; left: 116.5px;">
                        <div class="tooltip-arrow"></div>
                        <div class="tooltip-inner">100</div>
                    </div>
                    <input id="mySlider" data-slider-handle="triangle" data-slider-min="0" data-slider-value="100" data-slider-max="200" type="text"></div>
                <br>
                <span id="mySliderValue" class="textbox">100</span>

                <div class="scoring-single-img">
                    <?php echo $this->Html->image('banner.jpg', array('class' => 'scoring-img'));?>
                </div>
                <textarea class="form-control" rows="5" placeholder="Add Notes Here"></textarea>
            </div>
        </div>
    </div>
</div>
<div class="margin-top-25"></div>
<h2 class="heading-sm u-p">Swing Plane Impact: </h2>
<hr class="shadow-line">
<div class="container-fluid">
    <div class="row">
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p"> Club Shaft Position</h4>

            <div class="scoring-body">
                <div class="slider slider-horizontal">
                    <div class="slider-track">
                        <div class="slider-selection" style="left: 0%; width: 50%;"></div>
                        <div class="slider-handle triangle" style="left: 50%;"></div>
                        <div class="slider-handle triangle hide" style="left: 0%;"></div>
                    </div>
                    <div class="tooltip top" style="top: -30px; left: 116.5px;">
                        <div class="tooltip-arrow"></div>
                        <div class="tooltip-inner">100</div>
                    </div>
                    <input id="mySlider" data-slider-handle="triangle" data-slider-min="0" data-slider-value="100" data-slider-max="200" type="text"></div>
                <br>
                <span id="mySliderValue" class="textbox">100</span>

                <div class="scoring-single-img">
                    <?php echo $this->Html->image('banner.jpg', array('class' => 'scoring-img'));?>
                </div>
                <textarea class="form-control" rows="5" placeholder="Add Notes Here"></textarea>
            </div>
        </div>
    </div>
</div>
<div class="margin-top-25"></div>
<h2 class="heading-sm u-p">Swing Plane Impact: </h2>
<hr class="shadow-line">
<div class="container-fluid">
    <div class="row">
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p"> Club Shaft Position</h4>

            <div class="scoring-body">
                <div class="slider slider-horizontal">
                    <div class="slider-track">
                        <div class="slider-selection" style="left: 0%; width: 50%;"></div>
                        <div class="slider-handle triangle" style="left: 50%;"></div>
                        <div class="slider-handle triangle hide" style="left: 0%;"></div>
                    </div>
                    <div class="tooltip top" style="top: -30px; left: 116.5px;">
                        <div class="tooltip-arrow"></div>
                        <div class="tooltip-inner">100</div>
                    </div>
                    <input id="mySlider" data-slider-handle="triangle" data-slider-min="0" data-slider-value="100" data-slider-max="200" type="text"></div>
                <br>
                <span id="mySliderValue" class="textbox">100</span>

                <div class="scoring-single-img">
                    <?php echo $this->Html->image('banner.jpg', array('class' => 'scoring-img'));?>
                </div>
                <textarea class="form-control" rows="5" placeholder="Add Notes Here"></textarea>
            </div>
        </div>
    </div>
</div>
<div class="margin-top-25"></div>
<h2 class="heading-sm u-p">Swing Plane Impact: </h2>
<hr class="shadow-line">
<div class="container-fluid">
    <div class="row">
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p"> Club Shaft Position</h4>

            <div class="scoring-body">
                <div class="slider slider-horizontal">
                    <div class="slider-track">
                        <div class="slider-selection" style="left: 0%; width: 50%;"></div>
                        <div class="slider-handle triangle" style="left: 50%;"></div>
                        <div class="slider-handle triangle hide" style="left: 0%;"></div>
                    </div>
                    <div class="tooltip top" style="top: -30px; left: 116.5px;">
                        <div class="tooltip-arrow"></div>
                        <div class="tooltip-inner">100</div>
                    </div>
                    <input id="mySlider" data-slider-handle="triangle" data-slider-min="0" data-slider-value="100" data-slider-max="200" type="text"></div>
                <br>
                <span id="mySliderValue" class="textbox">100</span>

                <div class="scoring-single-img">
                    <?php echo $this->Html->image('banner.jpg', array('class' => 'scoring-img'));?>
                </div>
                <textarea class="form-control" rows="5" placeholder="Add Notes Here"></textarea>
            </div>
        </div>
    </div>
</div>
<div class="margin-top-25"></div>
<h2 class="heading-sm u-p">Swing Plane Follow Through: </h2>
<hr class="shadow-line">
<div class="container-fluid">
    <div class="row">
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p"> Club Shaft Position</h4>

            <div class="scoring-body">
                <div class="slider slider-horizontal">
                    <div class="slider-track">
                        <div class="slider-selection" style="left: 0%; width: 50%;"></div>
                        <div class="slider-handle triangle" style="left: 50%;"></div>
                        <div class="slider-handle triangle hide" style="left: 0%;"></div>
                    </div>
                    <div class="tooltip top" style="top: -30px; left: 116.5px;">
                        <div class="tooltip-arrow"></div>
                        <div class="tooltip-inner">100</div>
                    </div>
                    <input id="mySlider" data-slider-handle="triangle" data-slider-min="0" data-slider-value="100" data-slider-max="200" type="text"></div>
                <br>
                <span id="mySliderValue" class="textbox">100</span>

                <div class="scoring-single-img">
                    <?php echo $this->Html->image('banner.jpg', array('class' => 'scoring-img'));?>
                </div>
                <textarea class="form-control" rows="5" placeholder="Add Notes Here"></textarea>
            </div>
        </div>
    </div>
</div>
<div class="margin-top-25"></div>
<h2 class="heading-sm u-p">Swing Plane Finish: </h2>
<hr class="shadow-line">
<div class="container-fluid">
    <div class="row">
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p"> Club Shaft Position</h4>

            <div class="scoring-body">
                <div class="slider slider-horizontal">
                    <div class="slider-track">
                        <div class="slider-selection" style="left: 0%; width: 50%;"></div>
                        <div class="slider-handle triangle" style="left: 50%;"></div>
                        <div class="slider-handle triangle hide" style="left: 0%;"></div>
                    </div>
                    <div class="tooltip top" style="top: -30px; left: 116.5px;">
                        <div class="tooltip-arrow"></div>
                        <div class="tooltip-inner">100</div>
                    </div>
                    <input id="mySlider" data-slider-handle="triangle" data-slider-min="0" data-slider-value="100" data-slider-max="200" type="text"></div>
                <br>
                <span id="mySliderValue" class="textbox">100</span>

                <div class="scoring-single-img">
                    <?php echo $this->Html->image('banner.jpg', array('class' => 'scoring-img'));?>
                </div>
                <textarea class="form-control" rows="5" placeholder="Add Notes Here"></textarea>
            </div>
        </div>
    </div>
</div>
<div class="margin-top-25"></div>
<h2 class="heading-sm u-p">Club Face Halfway Back: </h2>
<hr class="shadow-line">
<div class="container-fluid">
    <div class="row">
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p"> Club Face  Position</h4>

            <div class="scoring-body">
                <div class="slider slider-horizontal">
                    <div class="slider-track">
                        <div class="slider-selection" style="left: 0%; width: 50%;"></div>
                        <div class="slider-handle triangle" style="left: 50%;"></div>
                        <div class="slider-handle triangle hide" style="left: 0%;"></div>
                    </div>
                    <div class="tooltip top" style="top: -30px; left: 116.5px;">
                        <div class="tooltip-arrow"></div>
                        <div class="tooltip-inner">100</div>
                    </div>
                    <input id="mySlider" data-slider-handle="triangle" data-slider-min="0" data-slider-value="100" data-slider-max="200" type="text"></div>
                <br>
                <span id="mySliderValue" class="textbox">100</span>

                <div class="scoring-single-img">
                    <?php echo $this->Html->image('banner.jpg', array('class' => 'scoring-img'));?>
                </div>
                <textarea class="form-control" rows="5" placeholder="Add Notes Here"></textarea>
            </div>
        </div>
    </div>
</div>
<div class="margin-top-25"></div>
<h2 class="heading-sm u-p">Club Face ¾ Swing:</h2>
<hr class="shadow-line">
<div class="container-fluid">
    <div class="row">
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p"> Club Face  Position</h4>

            <div class="scoring-body">
                <div class="slider slider-horizontal">
                    <div class="slider-track">
                        <div class="slider-selection" style="left: 0%; width: 50%;"></div>
                        <div class="slider-handle triangle" style="left: 50%;"></div>
                        <div class="slider-handle triangle hide" style="left: 0%;"></div>
                    </div>
                    <div class="tooltip top" style="top: -30px; left: 116.5px;">
                        <div class="tooltip-arrow"></div>
                        <div class="tooltip-inner">100</div>
                    </div>
                    <input id="mySlider" data-slider-handle="triangle" data-slider-min="0" data-slider-value="100" data-slider-max="200" type="text"></div>
                <br>
                <span id="mySliderValue" class="textbox">100</span>

                <div class="scoring-single-img">
                    <?php echo $this->Html->image('banner.jpg', array('class' => 'scoring-img'));?>
                </div>
                <textarea class="form-control" rows="5" placeholder="Add Notes Here"></textarea>
            </div>
        </div>
    </div>
</div>
<div class="margin-top-25"></div>
<h2 class="heading-sm u-p">Club Face at Top: </h2>
<hr class="shadow-line">
<div class="container-fluid">
    <div class="row">
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p"> Club Face  Position</h4>

            <div class="scoring-body">
                <div class="slider slider-horizontal">
                    <div class="slider-track">
                        <div class="slider-selection" style="left: 0%; width: 50%;"></div>
                        <div class="slider-handle triangle" style="left: 50%;"></div>
                        <div class="slider-handle triangle hide" style="left: 0%;"></div>
                    </div>
                    <div class="tooltip top" style="top: -30px; left: 116.5px;">
                        <div class="tooltip-arrow"></div>
                        <div class="tooltip-inner">100</div>
                    </div>
                    <input id="mySlider" data-slider-handle="triangle" data-slider-min="0" data-slider-value="100" data-slider-max="200" type="text"></div>
                <br>
                <span id="mySliderValue" class="textbox">100</span>

                <div class="scoring-single-img">
                    <?php echo $this->Html->image('banner.jpg', array('class' => 'scoring-img'));?>
                </div>
                <textarea class="form-control" rows="5" placeholder="Add Notes Here"></textarea>
            </div>
        </div>
    </div>
</div>
<div class="margin-top-25"></div>
<h2 class="heading-sm u-p">Club Face Downswing: </h2>
<hr class="shadow-line">
<div class="container-fluid">
    <div class="row">
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p"> Club Face  Position</h4>

            <div class="scoring-body">
                <div class="slider slider-horizontal">
                    <div class="slider-track">
                        <div class="slider-selection" style="left: 0%; width: 50%;"></div>
                        <div class="slider-handle triangle" style="left: 50%;"></div>
                        <div class="slider-handle triangle hide" style="left: 0%;"></div>
                    </div>
                    <div class="tooltip top" style="top: -30px; left: 116.5px;">
                        <div class="tooltip-arrow"></div>
                        <div class="tooltip-inner">100</div>
                    </div>
                    <input id="mySlider" data-slider-handle="triangle" data-slider-min="0" data-slider-value="100" data-slider-max="200" type="text"></div>
                <br>
                <span id="mySliderValue" class="textbox">100</span>

                <div class="scoring-single-img">
                    <?php echo $this->Html->image('banner.jpg', array('class' => 'scoring-img'));?>
                </div>
                <textarea class="form-control" rows="5" placeholder="Add Notes Here"></textarea>
            </div>
        </div>
    </div>
</div>
<div class="margin-top-25"></div>
<h2 class="heading-sm u-p">Club Face Impact: </h2>
<hr class="shadow-line">
<div class="container-fluid">
    <div class="row">
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p"> Club Face  Position</h4>

            <div class="scoring-body">
                <div class="slider slider-horizontal">
                    <div class="slider-track">
                        <div class="slider-selection" style="left: 0%; width: 50%;"></div>
                        <div class="slider-handle triangle" style="left: 50%;"></div>
                        <div class="slider-handle triangle hide" style="left: 0%;"></div>
                    </div>
                    <div class="tooltip top" style="top: -30px; left: 116.5px;">
                        <div class="tooltip-arrow"></div>
                        <div class="tooltip-inner">100</div>
                    </div>
                    <input id="mySlider" data-slider-handle="triangle" data-slider-min="0" data-slider-value="100" data-slider-max="200" type="text"></div>
                <br>
                <span id="mySliderValue" class="textbox">100</span>

                <div class="scoring-single-img">
                    <?php echo $this->Html->image('banner.jpg', array('class' => 'scoring-img'));?>
                </div>
                <textarea class="form-control" rows="5" placeholder="Add Notes Here"></textarea>
            </div>
        </div>
    </div>
</div>
<div class="margin-top-25"></div>
<h2 class="heading-sm u-p">Club Face Follow Through: </h2>
<hr class="shadow-line">
<div class="container-fluid">
    <div class="row">
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p"> Club Face  Position</h4>

            <div class="scoring-body">
                <div class="slider slider-horizontal">
                    <div class="slider-track">
                        <div class="slider-selection" style="left: 0%; width: 50%;"></div>
                        <div class="slider-handle triangle" style="left: 50%;"></div>
                        <div class="slider-handle triangle hide" style="left: 0%;"></div>
                    </div>
                    <div class="tooltip top" style="top: -30px; left: 116.5px;">
                        <div class="tooltip-arrow"></div>
                        <div class="tooltip-inner">100</div>
                    </div>
                    <input id="mySlider" data-slider-handle="triangle" data-slider-min="0" data-slider-value="100" data-slider-max="200" type="text"></div>
                <br>
                <span id="mySliderValue" class="textbox">100</span>

                <div class="scoring-single-img">
                    <?php echo $this->Html->image('banner.jpg', array('class' => 'scoring-img'));?>
                </div>
                <textarea class="form-control" rows="5" placeholder="Add Notes Here"></textarea>
            </div>
        </div>
    </div>
</div>
<div class="margin-top-25"></div>
<h2 class="heading-sm u-p">Club Face Follow Finish: </h2>
<hr class="shadow-line">
<div class="container-fluid">
    <div class="row">
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p"> Club Face  Position</h4>

            <div class="scoring-body">
                <div class="slider slider-horizontal">
                    <div class="slider-track">
                        <div class="slider-selection" style="left: 0%; width: 50%;"></div>
                        <div class="slider-handle triangle" style="left: 50%;"></div>
                        <div class="slider-handle triangle hide" style="left: 0%;"></div>
                    </div>
                    <div class="tooltip top" style="top: -30px; left: 116.5px;">
                        <div class="tooltip-arrow"></div>
                        <div class="tooltip-inner">100</div>
                    </div>
                    <input id="mySlider" data-slider-handle="triangle" data-slider-min="0" data-slider-value="100" data-slider-max="200" type="text"></div>
                <br>
                <span id="mySliderValue" class="textbox">100</span>

                <div class="scoring-single-img">
                    <?php echo $this->Html->image('banner.jpg', array('class' => 'scoring-img'));?>
                </div>
                <textarea class="form-control" rows="5" placeholder="Add Notes Here"></textarea>
            </div>
        </div>
    </div>
</div>
<div class="margin-top-25"></div>
<h2 class="heading-sm u-p">Wrist Cock ¾ Swing: </h2>
<hr class="shadow-line">
<div class="container-fluid">
    <div class="row">
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p"> Wrist Cock</h4>

            <div class="scoring-body">
                <div class="slider slider-horizontal">
                    <div class="slider-track">
                        <div class="slider-selection" style="left: 0%; width: 50%;"></div>
                        <div class="slider-handle triangle" style="left: 50%;"></div>
                        <div class="slider-handle triangle hide" style="left: 0%;"></div>
                    </div>
                    <div class="tooltip top" style="top: -30px; left: 116.5px;">
                        <div class="tooltip-arrow"></div>
                        <div class="tooltip-inner">100</div>
                    </div>
                    <input id="mySlider" data-slider-handle="triangle" data-slider-min="0" data-slider-value="100" data-slider-max="200" type="text"></div>
                <br>
                <span id="mySliderValue" class="textbox">100</span>

                <div class="scoring-single-img">
                    <?php echo $this->Html->image('banner.jpg', array('class' => 'scoring-img'));?>
                </div>
                <textarea class="form-control" rows="5" placeholder="Add Notes Here"></textarea>
            </div>
        </div>
    </div>
</div>
<div class="margin-top-25"></div>
<h2 class="heading-sm u-p">Wrist Cock at Top:  </h2>
<hr class="shadow-line">
<div class="container-fluid">
    <div class="row">
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p"> Wrist Cock</h4>

            <div class="scoring-body">
                <div class="slider slider-horizontal">
                    <div class="slider-track">
                        <div class="slider-selection" style="left: 0%; width: 50%;"></div>
                        <div class="slider-handle triangle" style="left: 50%;"></div>
                        <div class="slider-handle triangle hide" style="left: 0%;"></div>
                    </div>
                    <div class="tooltip top" style="top: -30px; left: 116.5px;">
                        <div class="tooltip-arrow"></div>
                        <div class="tooltip-inner">100</div>
                    </div>
                    <input id="mySlider" data-slider-handle="triangle" data-slider-min="0" data-slider-value="100" data-slider-max="200" type="text"></div>
                <br>
                <span id="mySliderValue" class="textbox">100</span>

                <div class="scoring-single-img">
                    <?php echo $this->Html->image('banner.jpg', array('class' => 'scoring-img'));?>
                </div>
                <textarea class="form-control" rows="5" placeholder="Add Notes Here"></textarea>
            </div>
        </div>
    </div>
</div>
<div class="margin-top-25"></div>
<h2 class="heading-sm u-p">Downswing Sequence:</h2>
<hr class="shadow-line">
<div class="container-fluid">
    <div class="row">
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p"> Upper Body Position</h4>

            <div class="scoring-body">
                <div class="slider slider-horizontal">
                    <div class="slider-track">
                        <div class="slider-selection" style="left: 0%; width: 50%;"></div>
                        <div class="slider-handle triangle" style="left: 50%;"></div>
                        <div class="slider-handle triangle hide" style="left: 0%;"></div>
                    </div>
                    <div class="tooltip top" style="top: -30px; left: 116.5px;">
                        <div class="tooltip-arrow"></div>
                        <div class="tooltip-inner">100</div>
                    </div>
                    <input id="mySlider" data-slider-handle="triangle" data-slider-min="0" data-slider-value="100" data-slider-max="200" type="text"></div>
                <br>
                <span id="mySliderValue" class="textbox">100</span>

                <div class="scoring-single-img">
                    <?php echo $this->Html->image('banner.jpg', array('class' => 'scoring-img'));?>
                </div>
                <textarea class="form-control" rows="5" placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p"> Lower  Body Position</h4>

            <div class="scoring-body">
                <div class="slider slider-horizontal">
                    <div class="slider-track">
                        <div class="slider-selection" style="left: 0%; width: 50%;"></div>
                        <div class="slider-handle triangle" style="left: 50%;"></div>
                        <div class="slider-handle triangle hide" style="left: 0%;"></div>
                    </div>
                    <div class="tooltip top" style="top: -30px; left: 116.5px;">
                        <div class="tooltip-arrow"></div>
                        <div class="tooltip-inner">100</div>
                    </div>
                    <input id="mySlider" data-slider-handle="triangle" data-slider-min="0" data-slider-value="100" data-slider-max="200" type="text"></div>
                <br>
                <span id="mySliderValue" class="textbox">100</span>

                <div class="scoring-single-img">
                    <?php echo $this->Html->image('banner.jpg', array('class' => 'scoring-img'));?>
                </div>
                <textarea class="form-control" rows="5" placeholder="Add Notes Here"></textarea>
            </div>
        </div>
    </div>
</div>
<div class="margin-top-25"></div>
<h2 class="heading-sm u-p">Hands/Club Position Impact: </h2>
<hr class="shadow-line">
<div class="container-fluid">
    <div class="row">
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p"> Hand Position Rotation</h4>

            <div class="scoring-body">
                <div class="slider slider-horizontal">
                    <div class="slider-track">
                        <div class="slider-selection" style="left: 0%; width: 50%;"></div>
                        <div class="slider-handle triangle" style="left: 50%;"></div>
                        <div class="slider-handle triangle hide" style="left: 0%;"></div>
                    </div>
                    <div class="tooltip top" style="top: -30px; left: 116.5px;">
                        <div class="tooltip-arrow"></div>
                        <div class="tooltip-inner">100</div>
                    </div>
                    <input id="mySlider" data-slider-handle="triangle" data-slider-min="0" data-slider-value="100" data-slider-max="200" type="text"></div>
                <br>
                <span id="mySliderValue" class="textbox">100</span>

                <div class="scoring-single-img">
                    <?php echo $this->Html->image('banner.jpg', array('class' => 'scoring-img'));?>
                </div>
                <textarea class="form-control" rows="5" placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">Hand Position Location</h4>

            <div class="scoring-body">
                <div class="slider slider-horizontal">
                    <div class="slider-track">
                        <div class="slider-selection" style="left: 0%; width: 50%;"></div>
                        <div class="slider-handle triangle" style="left: 50%;"></div>
                        <div class="slider-handle triangle hide" style="left: 0%;"></div>
                    </div>
                    <div class="tooltip top" style="top: -30px; left: 116.5px;">
                        <div class="tooltip-arrow"></div>
                        <div class="tooltip-inner">100</div>
                    </div>
                    <input id="mySlider" data-slider-handle="triangle" data-slider-min="0" data-slider-value="100" data-slider-max="200" type="text"></div>
                <br>
                <span id="mySliderValue" class="textbox">100</span>

                <div class="scoring-single-img">
                    <?php echo $this->Html->image('banner.jpg', array('class' => 'scoring-img'));?>
                </div>
                <textarea class="form-control" rows="5" placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">Tempo on Backswing</h4>

            <div class="scoring-body">
                <div class="slider slider-horizontal">
                    <div class="slider-track">
                        <div class="slider-selection" style="left: 0%; width: 50%;"></div>
                        <div class="slider-handle triangle" style="left: 50%;"></div>
                        <div class="slider-handle triangle hide" style="left: 0%;"></div>
                    </div>
                    <div class="tooltip top" style="top: -30px; left: 116.5px;">
                        <div class="tooltip-arrow"></div>
                        <div class="tooltip-inner">100</div>
                    </div>
                    <input id="mySlider" data-slider-handle="triangle" data-slider-min="0" data-slider-value="100" data-slider-max="200" type="text"></div>
                <br>
                <span id="mySliderValue" class="textbox">100</span>

                <div class="scoring-single-img">
                    <?php echo $this->Html->image('banner.jpg', array('class' => 'scoring-img'));?>
                </div>
                <textarea class="form-control" rows="5" placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">Tempo on Downswing</h4>

            <div class="scoring-body">
                <div class="slider slider-horizontal">
                    <div class="slider-track">
                        <div class="slider-selection" style="left: 0%; width: 50%;"></div>
                        <div class="slider-handle triangle" style="left: 50%;"></div>
                        <div class="slider-handle triangle hide" style="left: 0%;"></div>
                    </div>
                    <div class="tooltip top" style="top: -30px; left: 116.5px;">
                        <div class="tooltip-arrow"></div>
                        <div class="tooltip-inner">100</div>
                    </div>
                    <input id="mySlider" data-slider-handle="triangle" data-slider-min="0" data-slider-value="100" data-slider-max="200" type="text"></div>
                <br>
                <span id="mySliderValue" class="textbox">100</span>

                <div class="scoring-single-img">
                    <?php echo $this->Html->image('banner.jpg', array('class' => 'scoring-img'));?>
                </div>
                <textarea class="form-control" rows="5" placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">Lag</h4>

            <div class="scoring-body">
                <div class="slider slider-horizontal">
                    <div class="slider-track">
                        <div class="slider-selection" style="left: 0%; width: 50%;"></div>
                        <div class="slider-handle triangle" style="left: 50%;"></div>
                        <div class="slider-handle triangle hide" style="left: 0%;"></div>
                    </div>
                    <div class="tooltip top" style="top: -30px; left: 116.5px;">
                        <div class="tooltip-arrow"></div>
                        <div class="tooltip-inner">100</div>
                    </div>
                    <input id="mySlider" data-slider-handle="triangle" data-slider-min="0" data-slider-value="100" data-slider-max="200" type="text"></div>
                <br>
                <span id="mySliderValue" class="textbox">100</span>

                <div class="scoring-single-img">
                    <?php echo $this->Html->image('banner.jpg', array('class' => 'scoring-img'));?>
                </div>
                <textarea class="form-control" rows="5" placeholder="Add Notes Here"></textarea>
            </div>
        </div>
    </div>
</div>
</div>
</div>
<!---- /Widget Two ----->
<!---- Widget Three ----->
<div class="widget black-widget">
    <div class="widget-header u-p">
        <i class="fa fa-bar-chart-o"></i> Scoring Areas of Training
    </div>
    <div class="widget-body padding-15">
        <h2 class="heading-sm u-p">Scoring Areas of Training</h2>
        <hr class="shadow-line">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-3 col-sm-6 scoring-box">
                    <h4 class="scoring-title u-p">Putting</h4>

                    <div class="scoring-body">
                        <div class="slider slider-horizontal">
                            <div class="slider-track">
                                <div class="slider-selection" style="left: 0%; width: 50%;"></div>
                                <div class="slider-handle triangle" style="left: 50%;"></div>
                                <div class="slider-handle triangle hide" style="left: 0%;"></div>
                            </div>
                            <div class="tooltip top" style="top: -30px; left: 116.5px;">
                                <div class="tooltip-arrow"></div>
                                <div class="tooltip-inner">100</div>
                            </div>
                            <div class="slider slider-horizontal"><div class="slider-track"><div class="slider-selection" style="left: 0%; width: 50%;"></div><div class="slider-handle triangle" style="left: 50%;"></div><div class="slider-handle triangle hide" style="left: 0%;"></div></div><div class="tooltip top" style="top: -30px; left: 77px;"><div class="tooltip-arrow"></div><div class="tooltip-inner">100</div></div><input id="mySlider" data-slider-handle="triangle" data-slider-min="0" data-slider-value="100" data-slider-max="200" type="text"></div></div>
                        <br>
                        <span id="mySliderValue" class="textbox">100</span>

                        <textarea class="form-control" rows="5" placeholder="Add Notes Here"></textarea>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 scoring-box">
                    <h4 class="scoring-title u-p">Chipping</h4>

                    <div class="scoring-body">
                        <div class="slider slider-horizontal">
                            <div class="slider-track">
                                <div class="slider-selection" style="left: 0%; width: 52%;"></div>
                                <div class="slider-handle triangle" style="left: 52%;"></div>
                                <div class="slider-handle triangle hide" style="left: 0%;"></div>
                            </div>
                            <div class="tooltip top" style="top: -30px; left: 125.4px;">
                                <div class="tooltip-arrow"></div>
                                <div class="tooltip-inner">86</div>
                            </div>
                            <div class="slider slider-horizontal"><div class="slider-track"><div class="slider-selection" style="left: 0%; width: 50%;"></div><div class="slider-handle triangle" style="left: 50%;"></div><div class="slider-handle triangle hide" style="left: 0%;"></div></div><div class="tooltip top" style="top: -30px; left: 77px;"><div class="tooltip-arrow"></div><div class="tooltip-inner">100</div></div><input id="mySlider2" data-slider-handle="triangle" data-slider-min="0" data-slider-value="100" data-slider-max="200" type="text"></div></div>
                        <br>
                        <span id="mySliderValue2" class="textbox">100</span>

                        <textarea class="form-control" rows="5" placeholder="Add Notes Here"></textarea>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 scoring-box">
                    <h4 class="scoring-title u-p">Pitching</h4>

                    <div class="scoring-body">
                        <div class="slider slider-horizontal">
                            <div class="slider-track">
                                <div class="slider-selection" style="left: 0%; width: 50%;"></div>
                                <div class="slider-handle triangle" style="left: 50%;"></div>
                                <div class="slider-handle triangle hide" style="left: 0%;"></div>
                            </div>
                            <div class="tooltip top" style="top: -30px; left: 116.5px;">
                                <div class="tooltip-arrow"></div>
                                <div class="tooltip-inner">100</div>
                            </div>
                            <div class="slider slider-horizontal"><div class="slider-track"><div class="slider-selection" style="left: 0%; width: 50%;"></div><div class="slider-handle triangle" style="left: 50%;"></div><div class="slider-handle triangle hide" style="left: 0%;"></div></div><div class="tooltip top" style="top: -30px; left: 77px;"><div class="tooltip-arrow"></div><div class="tooltip-inner">100</div></div><input id="mySlider3" data-slider-handle="triangle" data-slider-min="0" data-slider-value="100" data-slider-max="200" type="text"></div></div>
                        <br>
                        <span id="mySliderValue3" class="textbox">100</span>

                        <textarea class="form-control" rows="5" placeholder="Add Notes Here"></textarea>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 scoring-box">
                    <h4 class="scoring-title u-p">Distance Wedge </h4>

                    <div class="scoring-body">
                        <div class="slider slider-horizontal">
                            <div class="slider-track">
                                <div class="slider-selection" style="left: 0%; width: 50%;"></div>
                                <div class="slider-handle triangle" style="left: 50%;"></div>
                                <div class="slider-handle triangle hide" style="left: 0%;"></div>
                            </div>
                            <div class="tooltip top" style="top: -30px; left: 116.5px;">
                                <div class="tooltip-arrow"></div>
                                <div class="tooltip-inner">100</div>
                            </div>
                            <div class="slider slider-horizontal"><div class="slider-track"><div class="slider-selection" style="left: 0%; width: 50%;"></div><div class="slider-handle triangle" style="left: 50%;"></div><div class="slider-handle triangle hide" style="left: 0%;"></div></div><div class="tooltip top" style="top: -30px; left: 77px;"><div class="tooltip-arrow"></div><div class="tooltip-inner">100</div></div><input id="mySlider4" data-slider-handle="triangle" data-slider-min="0" data-slider-value="100" data-slider-max="200" type="text"></div></div>
                        <br>
                        <span id="mySliderValue4" class="textbox">100</span>
                        <textarea class="form-control" rows="5" placeholder="Add Notes Here"></textarea>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 scoring-box">
                    <h4 class="scoring-title u-p">Sand Play Uneven Lies </h4>

                    <div class="scoring-body">
                        <div class="slider slider-horizontal">
                            <div class="slider-track">
                                <div class="slider-selection" style="left: 0%; width: 50%;"></div>
                                <div class="slider-handle triangle" style="left: 50%;"></div>
                                <div class="slider-handle triangle hide" style="left: 0%;"></div>
                            </div>
                            <div class="tooltip top" style="top: -30px; left: 116.5px;">
                                <div class="tooltip-arrow"></div>
                                <div class="tooltip-inner">100</div>
                            </div>
                            <input id="mySlider4" data-slider-handle="triangle" data-slider-min="0" data-slider-value="100" data-slider-max="200" type="text"></div>
                        <br>
                        <span id="mySliderValue4" class="textbox">100</span>
                        <textarea class="form-control" rows="5" placeholder="Add Notes Here"></textarea>
                    </div>
                </div>
            </div>
        </div>
        <div class="margin-top-25"></div>
        <h2 class="heading-sm u-p">Course Management Mental Game:</h2>
        <hr class="shadow-line">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-3 col-sm-6 scoring-box">
                    <h4 class="scoring-title u-p">Paying Style</h4>

                    <div class="scoring-body">
                        <div class="slider slider-horizontal">
                            <div class="slider-track">
                                <div class="slider-selection" style="left: 0%; width: 50%;"></div>
                                <div class="slider-handle triangle" style="left: 50%;"></div>
                                <div class="slider-handle triangle hide" style="left: 0%;"></div>
                            </div>
                            <div class="tooltip top" style="top: -30px; left: 116.5px;">
                                <div class="tooltip-arrow"></div>
                                <div class="tooltip-inner">100</div>
                            </div>
                            <input id="mySlider" data-slider-handle="triangle" data-slider-min="0" data-slider-value="100" data-slider-max="200" type="text"></div>
                        <br>
                        <span id="mySliderValue" class="textbox">100</span>
                        <textarea class="form-control" rows="5" placeholder="Add Notes Here"></textarea>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 scoring-box">
                    <h4 class="scoring-title u-p">Mind Set</h4>

                    <div class="scoring-body">
                        <div class="slider slider-horizontal">
                            <div class="slider-track">
                                <div class="slider-selection" style="left: 0%; width: 52%;"></div>
                                <div class="slider-handle triangle" style="left: 52%;"></div>
                                <div class="slider-handle triangle hide" style="left: 0%;"></div>
                            </div>
                            <div class="tooltip top" style="top: -30px; left: 125.4px;">
                                <div class="tooltip-arrow"></div>
                                <div class="tooltip-inner">86</div>
                            </div>
                            <input id="mySlider2" data-slider-handle="triangle" data-slider-min="0" data-slider-value="100" data-slider-max="200" type="text"></div>
                        <br>
                        <span id="mySliderValue2" class="textbox">96</span>
                        <textarea class="form-control" rows="5" placeholder="Add Notes Here"></textarea>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!---- /Widget Three ----->


<div class="clearfix"></div>
<div class="margin-top-25">
    <h2 class="heading-sm u-p">Lesson Notes</h2>
    <hr class="shadow-line">
    <ul class="nav normal-list">
        <li>
            <i class="success fa fa-check"></i> Our golf lessons in Orange County are conducted by PGA Certified
        </li>
        <li>
            <i class="success fa fa-check"></i> Our golf lessons in Orange County are conducted by PGA Certified golf
            instructors using the latest equipment and best techniques.
        </li>
        <li>
            <i class="success fa fa-check"></i> Our golf lessons in Orange County are conducted by PGA
        </li>
        <li>
            <i class="success fa fa-check"></i> Our golf lessons in Orange County are conducted by PGA Certified golf
            instructors using the latest equipment and best techniques. Give us a call
        </li>
        <li>
            <i class="success fa fa-check"></i> Our golf lessons in Orange County are conducted by PGA Certified golf
            instructors using the latest equipment and best techniques. Give us a call or fill out the form to the
        </li>
    </ul>
    <p class="note">
        N.B: Our golf lessons in Orange County are conducted by PGA Certified golf instructors using the latest
        equipment and best techniques. Give us a call Our golf lessons in Orange County are conducted by PGA Certified
        golf instructors using the latest equipment and best techniques. Give us a call
    </p>
</div>
<div class="clearfix"></div>
</div>
