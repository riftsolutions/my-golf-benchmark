<?php
/**
 * @var $this View
 */
?>
<h2 class="page-title">My Profile</h2>
<hr class="shadow-line"/>
<div class="row">
    <div class="col-md-8 col-md-offset-2 col-sm-12">
        <div class="box box-padding-normal radius-5">
            <div class="row">
                <div class="col-lg-5 col-md-6 col-sm-6">
                    <?php
                    if($userInfo['Profile']['profile_pic_url']){
                        echo $this->Html->image('profiles/'.$userInfo['Profile']['profile_pic_url'], array('class' => 'img-thumbnail profile-pic-small', 'url' => array('controller' => 'profiles', 'action' => 'index')));
                    }
                    else{
                        echo $this->Html->image('avatar.jpg', array('class' => 'img-thumbnail profile-pic-small', 'url' => array('controller' => 'profiles', 'action' => 'index')));
                    }
                    ?>
                </div>
                <div class="col-lg-7 col-md-6 col-sm-6">
                    <?php echo $this->Html->link('<i class="fa fa-pencil"></i> Update Profile', array('controller' => 'profiles', 'action' => 'update'), array('escape' => false, 'class' => 'fly-edit'));?>
                    <ul class="data-list">
                        <li>
                            <span class="name-lg green">
                                <?php echo $userInfo['Profile']['first_name'].' '.$userInfo['Profile']['last_name']; ?>
                            </span>
                        </li>
                        <li>
                            <strong>Email:</strong> <span><?php echo $userInfo['User']['username'];?></span>
                        </li>
                        <li>
                            <strong>Phone: </strong>
                            <span>
                                <?php
                                if($userInfo['Profile']['phone']){
                                    echo $userInfo['Profile']['phone'];
                                }
                                else{
                                    echo 'N/A';
                                }
                                ?>
                            </span>
                        </li>
                        <li>
                            <strong>State: </strong>
                            <?php
                            if($userInfo['Profile']['state']){
                                echo $userInfo['Profile']['state'];
                            }
                            else{
                                echo 'N/A';
                            }
                            ?>
                        </li>
                        <li>
                            <strong>City: </strong>
                            <?php
                            if($userInfo['Profile']['city']){
                                echo $userInfo['Profile']['city'];
                            }
                            else{
                                echo 'N/A';
                            }
                            ?>
                        </li>
                        <li>
                            <strong>Zip Code: </strong>
                            <?php
                            if($userInfo['Profile']['postal_code']){
                                echo $userInfo['Profile']['postal_code'];
                            }
                            else{
                                echo 'N/A';
                            }
                            ?>
                        </li>
                        <li>
                            <strong>Member Since: </strong>
                            <?php
                            echo $this->Time->format('M d, Y', $userInfo['User']['created']);
                            ?>
                        </li>
                        <li>
                            <strong>Address: </strong>
                            <?php
                            if($userInfo['Profile']['address']){
                                echo $userInfo['Profile']['address'];
                            }
                            else{
                                echo 'N/A';
                            }
                            ?>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="margin-top-15">
                <hr class="shadow-line">
                <h2 class="short-title theme-color">Biography:</h2>
                <i class="italic-md theme-color">
                    <?php
                    if($userInfo['Profile']['biography']){
                        echo $userInfo['Profile']['biography'];
                    }
                    else{
                        echo 'N/A';
                    }
                    ?>
                </i>
            </div>
        </div>
    </div>
</div>
