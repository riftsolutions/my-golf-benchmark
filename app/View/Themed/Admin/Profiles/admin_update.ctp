<?php
/**
 * @var $this View
 */
?>
<h2 class="page-title">Update Profile</h2>
<hr class="shadow-line"/>
<div class="row">
    <div class="col-md-8 col-md-offset-2 col-sm-12">
        <div class="box box-padding-adding radius-5 margin-top-25 margin-bottom-25">
            <?php echo $this->Form->create(
                'Profile',
                array('controller' => 'profiles', 'action' => 'update', 'class' => 'form-horizontal form-custom')
            ); ?>
            <div class="form-group">
                <label class="col-sm-4 control-label">First Name</label>

                <div class="col-sm-6">
                    <?php echo $this->Form->inout(
                        'Profile.first_name',
                        array(
                            'type' => 'text',
                            'class' => 'form-control',
                            'value' => $userInfo['Profile']['first_name'],
                            'required' => false
                        )
                    ); ?>
                    <?php echo $this->Form->error('Profile.first_name');?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4 control-label">Last Name</label>

                <div class="col-sm-6">
                    <?php echo $this->Form->inout(
                        'Profile.last_name',
                        array(
                            'type' => 'text',
                            'class' => 'form-control',
                            'value' => $userInfo['Profile']['last_name'],
                            'required' => false
                        )
                    ); ?>
                    <?php echo $this->Form->error('Profile.last_name');?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4 control-label">Email</label>

                <div class="col-sm-6">
                    <?php echo $this->Form->inout(
                        'User.username',
                        array(
                            'type' => 'text',
                            'class' => 'form-control',
                            'value' => $userInfo['User']['username'],
                            'disabled' => true,
                            'required' => false
                        )
                    ); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4 control-label">Phone</label>

                <div class="col-sm-6">
                    <?php echo $this->Form->inout(
                        'Profile.phone',
                        array(
                            'type' => 'text',
                            'class' => 'form-control',
                            'value' => $userInfo['Profile']['phone'],
                            'required' => false
                        )
                    ); ?>
                    <?php echo $this->Form->error('Profile.phone');?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4 control-label">City</label>

                <div class="col-sm-6">
                    <?php echo $this->Form->inout(
                        'Profile.city',
                        array(
                            'type' => 'text',
                            'class' => 'form-control',
                            'value' => $userInfo['Profile']['city'],
                            'required' => false
                        )
                    ); ?>
                    <?php echo $this->Form->error('Profile.city');?>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-4 control-label">State</label>

                <div class="col-sm-6">
                    <?php
                    $stateList = array(
                        'Alabama' => 'Alabama',
                        'Alaska' => 'Alaska',
                        'Arizona' => 'Arizona',
                        'Arkansas' => 'Arkansas',
                        'California' => 'California',
                        'Colorado' => 'Colorado',
                        'Connecticut' => 'Connecticut',
                        'Delaware' => 'Delaware',
                        'District Of Columbia' => 'District Of Columbia',
                        'Florida' => 'Florida',
                        'Georgia' => 'Georgia',
                        'Hawaii' => 'Hawaii',
                        'Idaho' => 'Idaho',
                        'Illinois' => 'Illinois',
                        'Indiana' => 'Indiana',
                        'Iowa' => 'Iowa',
                        'Kansas' => 'Kansas',
                        'Kentucky' => 'Kentucky',
                        'Louisiana' => 'Louisiana',
                        'Maine' => 'Maine',
                        'Maryland' => 'Maryland',
                        'Massachusetts' => 'Massachusetts',
                        'Michigan' => 'Michigan',
                        'Minnesota' => 'Minnesota',
                        'Mississippi' => 'Mississippi',
                        'Missouri' => 'Missouri',
                        'Montana' => 'Montana',
                        'Nebraska' => 'Nebraska',
                        'Nevada' => 'Nevada',
                        'New Hampshire' => 'New Hampshire',
                        'New Jersey' => 'New Jersey',
                        'New Mexico' => 'New Mexico',
                        'New York' => 'New York',
                        'North Carolina' => 'North Carolina',
                        'North Dakota' => 'North Dakota',
                        'Ohio' => 'Ohio',
                        'Oklahoma' => 'Oklahoma',
                        'Oregon' => 'Oregon',
                        'PALAU' => 'PALAU',
                        'Pennsylvania' => 'Pennsylvania',
                        'PUERTO RICO' => 'PUERTO RICO',
                        'Rhode Island' => 'Rhode Island',
                        'South Carolina' => 'South Carolina',
                        'South Dakota' => 'South Dakota',
                        'Tennessee' => 'Tennessee',
                        'Texas' => 'Texas',
                        'Utah' => 'Utah',
                        'Vermont' => 'Vermont',
                        'Virginia' => 'Virginia',
                        'Washington' => 'Washington',
                        'West Virginia' => 'West Virginia',
                        'Wisconsin' => 'Wisconsin',
                        'Wyoming' => 'Wyoming'
                    )
                    ?>

                    <?php
                    echo $this->Form->input(
                        'Profile.state',
                        array(
                            'type' => 'select',
                            'class' => 'form-control',
                            'label' => false,
                            'div' => false,
                            'required' => false,
                            'options' => $stateList,
                            'selected' => $userInfo['Profile']['state'],
                            'empty' => 'Select State'
                        )
                    );
                    ?>
                    <?php echo $this->Form->error('Profile.state');?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4 control-label">Zip Code</label>

                <div class="col-sm-6">
                    <?php echo $this->Form->inout(
                        'Profile.postal_code',
                        array(
                            'type' => 'text',
                            'class' => 'form-control',
                            'value' => $userInfo['Profile']['postal_code'],
                            'required' => false
                        )
                    ); ?>
                    <?php echo $this->Form->error('Profile.postal_code');?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4 control-label">Biography</label>

                <div class="col-sm-8">
                    <?php echo $this->Form->textarea(
                        'Profile.biography',
                        array(
                            'type' => 'textarea',
                            'class' => 'form-control',
                            'value' => $userInfo['Profile']['biography'],
                            'rows' => '10',
                            'cols' => 4,
                            'required' => false
                        )
                    ); ?>
                    <?php echo $this->Form->error('Profile.biography');?>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-8 col-sm-offset-4">
                    <button class="btn btn-theme">Update Profile</button>
                </div>
            </div>
            <?php echo $this->Form->end(); ?>
        </div>
    </div>
</div>