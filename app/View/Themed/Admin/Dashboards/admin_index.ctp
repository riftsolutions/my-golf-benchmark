<?php
/**
 * @var $this View
 */
?>
<h2 class="page-title">My Dashboard</h2>
<hr class="shadow-line"/>
<div class="content">
<div class="row">
    <!----- LESSON OVERVIEW ------>
    <div class="col-lg-6 col-md-6 col-sm-6">
        <div class="d-box">
            <h4 class="d-title"><i class="fa fa-flask yellowgreen"></i> Lesson Overview</h4>
            <ul class="d-list">
                <li>
                    <?php echo $this->Html->link('All Lesson', array('controller' => 'lessons', 'action' => 'list', 'all')); ?>
                    <span class="pull-right d-badge d-badge-gray"><?php echo 1;?></span>
                </li>

            </ul>
            <div class="see-more">
                <?php echo $this->html->link('See More <i class="fa fa-long-arrow-right"></i>', array('controller' => 'lessons', 'action' => 'list'), array('escape' => false))?>
            </div>

            <div class="clearfix"></div>
        </div>
    </div>
    <!----- /LESSON OVERVIEW ------>
</div>
</div>