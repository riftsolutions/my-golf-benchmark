<?php
/**
 * @var $this View
 */

?>
<div class="box">
    <div class="content-wrap">
        <div class="logo-area">
            <?php echo $this->Html->image('logo-green-small.png');?>
        </div>
        <?php echo $this->Form->create('User', array('controller' => 'users', 'action' => 'login'));?>
        <div class="row form-group">
        <?php
        echo $this->Form->error('User.username', array('error-mgs'));
        echo $this->Form->input(
            'User.username',
            array(
                'id' => 'username',
                'type' => 'text',
                'class' => 'form-control',
                'required' => false,
                'label' => false,
                'div' => false,
                'error' => false,
                'placeholder' => 'E-mail address'
            )
        );
        ?>
        </div>
        <div class="row form-group">
        <?php
        echo $this->Form->error('User.password', array('error-mgs'));
        echo $this->Form->input(
            'User.password',
            array(
                'type' => 'password',
                'class' => 'form-control',
                'required' => false,
                'label' => false,
                'div' => false,
                'error' => false,
                'placeholder' => 'Password'
            )
        );
        ?>
        </div>
        <?php echo $this->Html->link('Forgot Password?', array('controller' => 'users', 'action' => 'forgot_password'), array('class' => 'forgot'));?>
        <div class="remember">
            <input id="remember-me" type="checkbox">
            <label for="remember-me">Remember me</label>
        </div>
        <button class="btn-glow primary login">Log in</button>

        <div class="social_login">
            <a class="btn btn-block btn-social btn-facebook social_login_btn" href="<?php echo $loginUrl;?>">
                <i class="fa fa-facebook"></i> Sign in with Facebook
            </a>
            <a class="btn btn-block btn-social btn-twitter social_login_btn" href="<?php echo $twitterLoginURL;?>">
                <i class="fa fa-twitter"></i> Sign in with Twitter
            </a>
        </div>

        <div class="no-account">
            <p>Don't have an account?</p>
            <?php echo $this->Html->link('Sign up', array('controller' => 'users', 'action' => 'signup'), array('class' => 'signup'));?>
        </div>
        <?php echo $this->Form->end();?>
    </div>
</div>

