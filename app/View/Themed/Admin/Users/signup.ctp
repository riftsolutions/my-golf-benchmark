<?php
/**
 * @var $this View
 */
?>
<div class="login-wrapper">
    <div id="createAccount">
        <?php echo $this->Html->image('ajax-loader.gif');?>
        <h4>You request has been processing. Please be patient...</h4>
    </div>
    <div class="box">
        <div id="success"></div>
        <div class="content-wrap">
            <div class="logo-area">
                <?php echo $this->Html->image('logo-green-small.png');?>
            </div>
            <?php echo $this->Form->create('User', array('controller' => 'users', 'action' => 'signup'));?>
            <?php
                if (array_key_exists('plan', $this->request->query)) {
                    $plan = $this->request->query['plan'];
                    echo $this->Form->input(
                        'User.plan',
                        array(
                            'type' => 'hidden',
                            'value' => $plan,
                            'label' => false
                        )
                    );
                }
            ?>
            
            <div class="row form-group">
                <div class="text-center green text-uppercase">
                    <label>Create account</label>
                </div>
            </div>
            <div class="row form-group">
            <?php
            echo $this->Form->error('Profile.first_name', array('error-mgs'));
            echo $this->Form->input(
                'Profile.first_name',
                array(
                    'id' => 'first_name',
                    'type' => 'text',
                    'class' => 'form-control col-xs-6',
                    'style' => 'width: 48%;',
                    'required' => false,
                    'label' => false,
                    'div' => false,
                    'error' => false,
                    'placeholder' => 'First Name'
                )
            );
            ?>
            <?php
            echo $this->Form->error('Profile.last_name', array('error-mgs'));
            echo $this->Form->input(
                'Profile.last_name',
                array(
                    'id' => 'first_name',
                    'type' => 'text',
                    'class' => 'form-control col-xs-6',
                    'style' => 'width: 48%; margin-left: 4%;',
                    'required' => false,
                    'label' => false,
                    'div' => false,
                    'error' => false,
                    'placeholder' => 'Last Name'
                )
            );
            ?>
            </div>
            <div class="row form-group">
            <?php
            echo $this->Form->error('User.username', array('error-mgs'));
            echo $this->Form->input(
                'User.username',
                array(
                    'id' => 'username',
                    'type' => 'text',
                    'class' => 'form-control col-xs-12',
                    'required' => false,
                    'label' => false,
                    'div' => false,
                    'error' => false,
                    'placeholder' => 'E-mail address'
                )
            );
            ?>
            </div>
            <div class="row form-group">
            <?php
            echo $this->Form->error('User.password', array('error-mgs'));
            echo $this->Form->input(
                'User.password',
                array(
                    'type' => 'password',
                    'class' => 'form-control col-xs-6',
                    'style' => 'width: 48%;',
                    'required' => true,
                    'label' => false,
                    'div' => false,
                    'error' => false,
                    'placeholder' => 'Password'
                )
            );
            ?>
            <?php
            echo $this->Form->error('User.cPassword', array('error-mgs'));
            echo $this->Form->input(
                'User.cPassword',
                array(
                    'type' => 'password',
                    'class' => 'form-control col-xs-6',
                    'style' => 'width: 48%; margin-left: 4%;',
                    'required' => true,
                    'label' => false,
                    'div' => false,
                    'error' => false,
                    'placeholder' => 'Confirm Password'
                )
            );
            ?>
            </div>
            <div class="action">
                <button class="btn-glow primary login">Create</button>
            </div>
            <?php echo $this->Form->end();?>
        </div>
    </div>

    <div class="no-account">
        <p>Already have an account?</p>
        <?php echo $this->Html->link('Sign in', array('controller' => 'users', 'action' => 'login'));?>
    </div>

</div>
