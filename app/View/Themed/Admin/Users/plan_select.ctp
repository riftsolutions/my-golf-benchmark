<?php
/**
 * @var $this View
 */
?>
<div class="login-wrapper">
    <div id="createAccount">
        <?php echo $this->Html->image('ajax-loader.gif');?>
        <h4>You request has been processing. Please be patient...</h4>
    </div>
    <div class="box">
        <div id="success"></div>
        <div class="content-wrap">
            <div class="logo-area">
                <?php echo $this->Html->image('logo-green-small.png');?>
            </div>
            <form action="" method="POST" id="plan-select">

                <?php 
                $plans = \Stripe\Plan::all(); 
                foreach($plans->data as $key=>$plan) {
                    ?>
                    <label><input type="radio" name="data[User][plan]" value="<?php echo $plan->id;?>"><?php echo $plan->name;?></label>
                    <?php
                }
                
                ?>

                <div class="action">
                    <span id="form-error"></span>
                    <br>
                    <button class="btn-glow primary login">Continue</button>
                </div>
            </form>
        </div>
    </div>

    <div class="no-account">
        <p>Already have an account?</p>
        <?php echo $this->Html->link('Sign in', array('controller' => 'users', 'action' => 'login'));?>
    </div>

</div>