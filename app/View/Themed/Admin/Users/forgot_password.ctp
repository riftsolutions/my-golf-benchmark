<?php
/**
 * @var $this View
 */
?>
<div class="login-wrapper">
    <div class="box">
        <div class="content-wrap">
            <div class="logo-area">
                <?php echo $this->Html->image('logo-green-small.png');?>
            </div>
            <?php echo $this->Form->create('User', array('controller' => 'users', 'action' => 'forgot_password'));?>
            <?php
            echo $this->Form->error('User.username', array('error-mgs'));
            echo $this->Form->input(
                'User.username',
                array(
                    'id' => 'username',
                    'type' => 'text',
                    'class' => 'form-control',
                    'required' => false,
                    'label' => false,
                    'div' => false,
                    'error' => false,
                    'placeholder' => 'E-mail address'
                )
            );
            ?>
            <div class="action">
                <button class="btn-glow primary login">Send</button>
            </div>
            <?php echo $this->Form->end();?>
        </div>
    </div>

    <div class="no-account">
        <p>Back to login page</p>
        <?php echo $this->Html->link('Login', array('controller' => 'users', 'action' => 'login'));?>
    </div>
</div>