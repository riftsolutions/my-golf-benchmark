<?php
/**
 * @var $this View
 */
?>
<div class="login-wrapper">
    <div class="box">
        <div class="content-wrap">
            <div class="logo-area">
                <?php echo $this->Html->image('logo-green-small.png');?>
            </div>
            <?php echo $this->Form->create('User', array('controller' => 'users', 'action' => 'reset_password/'.$resetValue));?>
            <?php
            echo $this->Form->error('User.password', array('error-mgs'));
            echo $this->Form->input(
                'User.password',
                array(
                    'type' => 'password',
                    'class' => 'form-control',
                    'required' => false,
                    'label' => false,
                    'div' => false,
                    'error' => false,
                    'placeholder' => 'Password'
                )
            );
            ?>
            <?php
            echo $this->Form->error('User.cPassword', array('error-mgs'));
            echo $this->Form->input(
                'User.cPassword',
                array(
                    'type' => 'password',
                    'class' => 'form-control',
                    'required' => false,
                    'label' => false,
                    'div' => false,
                    'error' => false,
                    'placeholder' => 'Confirm Password'
                )
            );
            ?>
            <div class="action">
                <button class="btn-glow primary login">Reset</button>
            </div>
            <?php echo $this->Form->end();?>
        </div>
    </div>

    <div class="no-account">
        <p>Don't need, back me to</p>
        <?php echo $this->Html->link('Login', array('controller' => 'users', 'action' => 'login'));?>
    </div>

</div>