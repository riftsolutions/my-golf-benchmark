<?php
/**
 * @var $this View
 */
?>

<div class="login-wrapper">
    <div id="createAccount">
        <?php echo $this->Html->image('ajax-loader.gif');?>
        <h4>You request has been processing. Please be patient...</h4>
    </div>
    <div class="box">
        <div id="success"></div>
        <div class="content-wrap">
            <div class="logo-area">
                <?php echo $this->Html->image('logo-green-small.png');?>
            </div>
            <form action="" method="POST" id="payment-form">
            <?php /*echo $this->Form->create('User', array('controller' => 'users', 'action' => 'payment'));*/?>

            <div class="form-group">
                <?php echo $this->Html->image('payment2.png', array('class' => 'img-responsive'));?>
                
                <?php
                $user = $this->Session->read('userInfo');
                $plan = $user['User']['plan'];
                $stripePlan = \Stripe\Plan::retrieve($plan);
                ?>
                <p><?php echo '$'.number_format($stripePlan->amount / 100.0, 2, '.', '').' / '.$stripePlan->interval_count.' '.$stripePlan->interval; ?></p>
                <p>Your card will not be charged until your 7-day free trial is over. You can cancel at any time.</p>
            </div>

            <?php if($this->Session->read('is_social_login')):?>
                <div class="user-role">
                    <div class="radio">
                        <label>
                            <input type="radio" name="data[User][role]"  value="2" checked>
                            Create Account as Student
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="data[User][role]" value="3">
                            Create Account as Instructor
                        </label>
                    </div>
                </div>
            <?php endif;?>

            <div class="row">
                <div class="form-group col-xs-12">
                    <label for="CardCardNumber">CARD NUMBER</label>
                    <?php echo $this->Form->error('Card.cardNumber', array('error-mgs'));?>
                    <div class="input-group">
                        <?php echo $this->Form->input('Card.cardNumber', array('type' => 'tel', 'size' => '20', 'label' => false, 'error' => false, 'required' => true, 'autofocus' => true, 'class' => 'form-control', 'placeholder' => 'Card Number', 'data-stripe' => 'number'))?>
                        <span class="input-group-addon"><i class="fa fa-credit-card"></i></span>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="form-group col-xs-9">
                    <label for="CardCardNumber">EXPIRATION DATE</label>
                    <?php echo $this->Form->error('Card.ExpireYear', array('error-mgs'));?>
                    <?php echo $this->Form->error('Card.ExpireMonth', array('error-mgs'));?>
                    <div class="input-group full-width">
                        <?php
                        echo $this->Form->input('Card.ExpireMonth', array(
                                'type' => 'select',
                                'label' => false,
                                'required' => false,
                                'error' => false,
                                'empty' => 'MM',
                                'class' => 'form-control',
                                'options' => array(
                                    '01' => 'Jan (01)',
                                    '02' => 'Feb (02)',
                                    '03' => 'Mar (03)',
                                    '04' => 'Apr (04)',
                                    '05' => 'May (05)',
                                    '06' => 'Jun (06)',
                                    '07' => 'Jul (07)',
                                    '08' => 'Aug (08)',
                                    '09' => 'Sep (09)',
                                    '10' => 'Oct (10)',
                                    '11' => 'Nov (11)',
                                    '12' => 'Dec (12)',
                                ),
                                'data-stripe' => 'exp-month'
                            ))
                        ?>
                        <?php
                        echo $this->Form->input('Card.ExpireYear', array(
                                'type' => 'select',
                                'label' => false,
                                'required' => false,
                                'empty' => 'YYYY',
                                'error' => false,
                                'class' => 'form-control',
                                'options' => array(
                                    '2016' => '2016',
                                    '2017' => '2017',
                                    '2018' => '2018',
                                    '2019' => '2019',
                                    '2020' => '2020',
                                    '2021' => '2021',
                                    '2022' => '2022',
                                    '2023' => '2023',
                                    '2024' => '2024',
                                    '2025' => '2025',
                                    '2026' => '2026',
                                    '2027' => '2027',
                                    '2028' => '2028',
                                    '2029' => '2029',
                                    '2030' => '2030',
                                ),
                                'data-stripe' => 'exp-year'
                            ))
                        ?>
                    </div>
                    <div><?php echo $this->Form->error('Card.Expiry');?></div>
                </div>
                <div class="form-group col-xs-3">
                    <label for="Cardcvc">CVC</label>
                    <?php echo $this->Form->error('Card.cvc', array('error-mgs'));?>
                    <div class="input-group">
                        <?php echo $this->Form->input('Card.cvc', array('type' => 'text', 'size' => '4', 'label' => false, 'error' => false, 'required' => false, 'class' => 'form-control', 'placeholder' => 'CVC', 'data-stripe' => 'cvc'))?>
                    </div>
                </div>
            </div>


            <div class="action">
                <span id="form-error"></span>
                <br>
                <button class="btn-glow primary login">Payment and Signup</button>
            </div>
            <?php /*echo $this->Form->end();*/?>
            </form>
        </div>
    </div>

    <div class="no-account">
        <p>Already have an account?</p>
        <?php echo $this->Html->link('Sign in', array('controller' => 'users', 'action' => 'login'));?>
    </div>

</div>

<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
<script>
    // This identifies the website in the createToken call below
    Stripe.setPublishableKey('<?php echo STRIPE_PUBLIC;?>');
    
    function stripeResponseHandler(status, response) {
        var $form = $('#payment-form');

        if (response.error) {
            // Show the errors on the form
            $form.find('#form-error').text(response.error.message);
            $form.find('button').prop('disabled', false);
        } else {
            // response contains id and card, which contains additional card details
            var token = response.id;
            // Insert the token into the form so it gets submitted to the server
            $form.append($('<input type="hidden" name="data[stripe][token]" />').val(token));
            // and submit
            $form.get(0).submit();
        }
    };

    $(document).ready(function() {
        $('#payment-form').submit(function(e) {
            e.preventDefault();

            var $form = $(this);

            // Disable the submit button to prevent repeated clicks
            $form.find('#form-error').text('');
            $form.find('button').prop('disabled', true);

            Stripe.card.createToken($form, stripeResponseHandler);

            // Prevent the form from submitting with the default action
            return false;
        });
    });
</script>
