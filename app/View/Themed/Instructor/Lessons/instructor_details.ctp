<?php
/**
 * @var $this View
 */
echo $this->Html->css('kit/style-100');
?>

<h2 class="page-title">Group Lesson Details</h2>
<hr class="shadow-line"/>

<div class="clearfix"></div>
<div class="row">
    <div class="col-lg-4 col-md-3 col-sm-3">
        <div class="d-box">
            <h4 class="d-title margin-bottom-15"><i class="fa fa-info-circle yellowgreen"></i> Group Lesson Information</h4>
            <ul class="d-list">
                <li>
                    <strong>Title:</strong>
                    <span>
                        <?php echo $this->Html->link($groupLessonDetails['GroupLesson']['title'], array('controller' => 'lessons', 'action' => 'details', $groupLessonDetails['GroupLesson']['id'])) ?>
                    </span>
                </li>
                <li>
                    <strong>Description:</strong>
                    <span>
                        <?php echo $groupLessonDetails['GroupLesson']['description']; ?>
                    </span>
                </li>
                <li>
                    <strong>Time:</strong>
                    <?php echo $this->Time->format('M d, Y', $groupLessonDetails['GroupLesson']['start']);?>
                    <span class="italic-sm">( <?php echo $this->Time->format('h:m a', $groupLessonDetails['GroupLesson']['start'])?> - <?php echo $this->Time->format('h:m a', $groupLessonDetails['GroupLesson']['end'])?>)</span>
                </li>
                <li>
                    <strong>Price:</strong>
                    <span>
                        <?php echo $this->Number->currency($groupLessonDetails['GroupLesson']['price']); ?>
                    </span>
                </li>
                <li>
                    <strong>Student Limit:</strong>
                    <span>
                        <?php echo $groupLessonDetails['GroupLesson']['student_limit'];?>
                    </span>
                </li>
                <li>
                    <strong>Filled up Student:</strong>
                    <span>
                        <?php echo $groupLessonDetails['GroupLesson']['filled_up_student'];?>
                    </span>
                </li>
                <li>
                    <strong>Available:</strong>
                    <span>
                        <?php echo $groupLessonDetails['GroupLesson']['student_limit'] - $groupLessonDetails['GroupLesson']['filled_up_student'];?>
                    </span>
                </li>
                <li>
                    <strong>Status:</strong>
                    <span>
                        <?php
                        if($groupLessonDetails['GroupLesson']['status']){
                            echo '<label class="label label-success">Active</label>';
                        }
                        else{
                            echo '<label class="label label-danger">Inactive</label>';
                        }
                        ?>
                    </span>
                </li>

            </ul>
        </div>
    </div>
</div>

<?php
$isDisabled = ' ';
if(strtotime($groupLessonDetails['GroupLesson']['start']) < strtotime(date('Y-m-d H:i:s'))){
    $isDisabled = ' disabled';
}
elseif($groupLessonDetails['GroupLesson']['status'] != 1){
    $isDisabled = ' disabled';
}
?>

<div class="ui-100">
    <h2 class="page-title">Student List</h2>
    <hr class="shadow-line"/>
    <?php if($groupLessonDetails['GroupLessonsStudents']):?>
        <?php
        $counter = 0;
        foreach($groupLessonDetails['GroupLessonsStudents'] as $student):
        ?>
            <?php if($counter == 0){ echo '<div class="row">'; }?>
                <div class="col-md-2 col-sm-4">

                    <!-- Team member -->
                    <div class="team-member">
                        <!-- Image hover container -->
                        <div class="img-container">
                            <!-- Image -->
                            <?php
                            if($student['User']['Profile']['profile_pic_url']){
                                echo $this->Html->image('profiles/'.$student['User']['Profile']['profile_pic_url'], array('class' => 'group_lesson_student', 'url' => array('controller' => 'users', 'action' => 'details', $student['User']['uuid'])));
                            }
                            else{
                                echo $this->Html->image('avatar.jpg', array('class' => 'group_lesson_student', 'url' => array('controller' => 'profiles', 'action' => 'index')));
                            }
                            ?>
                            <div class="img-hover">
                                <!-- Social -->
                                <div class="brand-bg">

                                    <?php echo $this->Html->link('Student Details', array('controller' => 'users', 'action' => 'details', $student['User']['uuid']), array(' class' => 'btn btn-theme text-uppercase'));?>

                                    <?php echo $this->Html->link('Remove Student', array('controller' => 'calendars', 'action' => 'leave_group_lesson', $groupLessonDetails['GroupLesson']['id'], $student['User']['id'], 'student' => true), array(' class' => 'btn btn-theme-red text-uppercase' .$isDisabled));?>

                                    <?php if($student['payment_status'] == 2):?>
                                        <?php echo $this->Html->link('Mark as Paid', array('controller' => 'lessons', 'action' => 'payment', $student['id'], 1), array(' class' => 'btn btn-theme text-uppercase'));?>
                                    <?php elseif($student['payment_status'] == 1):?>
                                        <?php echo $this->Html->link('Mark as Unpaid', array('controller' => 'lessons', 'action' => 'payment', $student['id'], 2), array(' class' => 'btn btn-theme-red text-uppercase'));?>
                                    <?php endif;?>

                                </div>
                            </div>
                        </div>
                        <div class="team-details">
                            <!-- Name -->
                            <h4><?php echo $student['User']['Profile']['first_name']. ' '. $student['User']['Profile']['last_name']?></h4>
                            <span class="deg"><?php echo $student['User']['Profile']['phone'];?></span>
                            <!-- Para -->

                            <p><?php echo $student['User']['username'];?></p>

                            <?php if($student['payment_status'] == 2):?>
                                <label class="text-red margin-top-10">Payment Status: Unpaid</label>
                            <?php elseif($student['payment_status'] == 1):?>
                                <label class="text-green margin-top-10">Payment Status: Paid</label>
                            <?php endif;?>
                        </div>
                    </div>
                </div>
            <?php
            $counter++;
            if($counter == 6){
                $counter = 0;
                echo '</div>';
            }
            ?>
        <?php endforeach;?>
    <?php else:?>
        <h2 class="not-found">Sorry, result not found</h2>
    <?php endif;?>
</div>