<?php
/**
 * @var $this View
 */
?>
<h2 class="page-title">
    <?php if(isset($title)){
        echo $title;
    }
    else{
        echo 'Preview of Student List';
    }
    ?>
</h2>
<hr class="shadow-line"/>
<div class="margin-bottom-10">
    <h2 class="title-lg pull-left">
        Total User <?php echo sizeof($userList); ?> <i class="danger" style="font-size: 12px;">(duplicate user found <?php echo $duplicate; ?>)</i>
    </h2>
    <form method="post">
        <button class="btn btn-theme pull-right">Import All User</button>
    </form>
</div>

<div class="table-responsive">
    <?php if($userList):?>
        <table class="table data-table table-bordered sort-table" colspecing = 0>
        <thead>
        <tr>
            <th>Name</th>
            <th>Email</th>
            <th>Phone</th>
            <th>Status</th>
            <th>Last Login</th>
            <th style="width: 8%;">Lesson Left</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach($userList as $user):?>
        <tr <?php if($user['duplicate']){echo 'class="danger"';} ?>>
            <td>
                <?php echo $user['Profile']['first_name']. ' '. $user['Profile']['last_name'];?>
            </td>
            <td>
                <?php echo $user['User']['username'];?>
            </td>
            <td>
                <?php if($user['Profile']['phone']):?>
                    <?php echo $user['Profile']['phone'];?>
                <?php else:?>
                    N/A
                <?php endif;?>
            </td>
            <td>
                <?php
                echo $this->Utilities->getUserStatus($user['User']['status']);
                ?>
            </td>
            <td>
                <time>
                    <?php
                    if($user['User']['last_login']){
                        echo $this->Time->timeAgoInWords($user['User']['last_login'],
                            array('accuracy' => array('month' => 'month'),'end' => '1 year'));
                    }
                    else{
                        echo '<strong class="text-uppercase danger">Never logged in</strong>';
                    }
                    ?>
                </time>
            </td>
            <td style="text-align: center;">
                <?php
                if($user['CountLesson']['lesson_left'] || $user['CountLesson']['lesson_left'] == 0)
                {
                    echo $user['CountLesson']['lesson_left'];
                }
                else{
                    echo '<i class="glyphicon glyphicon-remove danger"></i>';
                }
                ?>
            </td>
        </tr>
        <?php endforeach;?>
        </tbody>
    </table>
    <?php else:?>
        <h2 class="not-found">Sorry, student not found</h2>
        <hr class="shadow-line"/>
    <?php endif;?>
</div>


<!-- Modal For Displaying Schedule -->
<div class="modal event-modal fade" id="takeNote" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="border-bottom: 1px solid #e5e5e5;">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                </button>
                <h4 class="sm-title">Take Note</h4>
            </div>
            <div class="modal-body">
                <!----- ADD NEW CONTACT NOTE ------>
                <div class="">
                    <?php echo $this->Form->create(
                        'Contact',
                        array('controller' => 'contacts', 'action' => 'note', 'class' => 'form-horizontal form-custom')
                    ); ?>
                    <input name="noteFor" type="hidden" value="student">
                    <div class="form-group">
                        <div class="col-sm-12">
                            <input name="data[Note][contactPerson]" class="typeahead form-control" type="text" placeholder="Type Student Name">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <?php
                            echo $this->Form->input(
                                'Note.note',
                                array(
                                    'type' => 'textarea',
                                    'class' => 'form-control',
                                    'placeholder' => 'Place your note here',
                                    'rows' => 5,
                                    'label' => false,
                                    'required' => false,
                                )
                            );
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-4">
                            <button class="btn btn-theme">Save Note</button>
                        </div>
                    </div>
                    <?php echo $this->Form->end(); ?>
                    <div class="clearfix"></div>
                </div>
                <!----- /ADD NEW CONTACT NOTE ------>
            </div>
        </div>
    </div>
</div>
<!-- /Modal For Displaying Schedule -->

<?php echo $this->Html->script('typehead');?>
<script>
    // Instantiate the Bloodhound suggestion engine
    var contacts = new Bloodhound({
        datumTokenizer: function (datum) {
            return Bloodhound.tokenizers.whitespace(datum.value);
        },
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: {
            url: '<?php echo Router::url('/', true);?>instructor/contacts/getContacts/student/%QUERY',
            filter: function (contacts) {
                return $.map(contacts, function (contact) {
                    console.log(contact);
                    return {
                        value: contact['0']['contactName']
                    };
                });
            }
        }
    });

    // Initialize the Bloodhound suggestion engine
    contacts.initialize();
    // Instantiate the Typeahead UI
    $('.typeahead').typeahead(null, {
        name: 'states',
        displayKey: 'value',
        source: contacts.ttAdapter()
    });
</script>