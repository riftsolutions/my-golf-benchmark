<?php
/**
 * @var $this View
 */
?>
<h2 class="page-title">
    <?php if(isset($title)){
        echo $title;
    }
    else{
        echo 'Students list';
    }
    ?>
</h2>
<hr class="shadow-line"/>
<div class="page-option">
    <div class="pull-left page-option-left">
        <div class="dropdown pull-left">
            <button class="btn btn-info dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown">
                Quick Navigation
                <span class="caret"></span>
            </button>
            <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                <li role="presentation">
                    <?php echo $this->Html->link('Total Student (' .$userOverview[0][0]['totalUser']. ')', array('controller' => 'users', 'action' => 'list'));?>
                </li>
                <li role="presentation" class="divider"></li>
                <li role="presentation">
                    <?php echo $this->Html->link('Slipping Away (' .$userOverview[0][0]['slippingAway']. ')', array('controller' => 'users', 'action' => 'list/slipping_away'));?>
                </li>
                <li role="presentation" class="divider"></li>
                <li role="presentation">
                    <?php echo $this->Html->link('Never Login (' .$userOverview[0][0]['neverLoggedIn']. ')', array('controller' => 'users', 'action' => 'list/never_login'));?>
                </li>
                <li role="presentation" class="divider"></li>
                <li role="presentation">
                    <?php echo $this->Html->link('Never Lesson Purchased (' .$userOverview[0][0]['neverLessonPurchased']. ')', array('controller' => 'users', 'action' => 'list/never_lesson_purchased'));?>
                </li>
                <li role="presentation" class="divider"></li>
                <li role="presentation">
                    <?php echo $this->Html->link('No Lesson Left (' .$userOverview[0][0]['noLessonLeft']. ')', array('controller' => 'users', 'action' => 'list/no_lesson_left'));?>
                </li>
                <li role="presentation" class="divider"></li>
                <li role="presentation">
                    <?php echo $this->Html->link('Lesson Left (' .$userOverview[0][0]['lessonLeft']. ')', array('controller' => 'users', 'action' => 'list/lesson_left'));?>
                </li>
                <li role="presentation" class="divider"></li>
                <li role="presentation">
                    <?php echo $this->Html->link('Never Schedule (' .$userOverview[0][0]['neverSchedule']. ')', array('controller' => 'users', 'action' => 'list/never_schedule'));?>
                </li>
                <li role="presentation" class="divider"></li>
                <li role="presentation">
                    <?php echo $this->Html->link('One Star Rating (' .$userOverview[0][0]['oneStarRatingLead']. ')', array('controller' => 'users', 'action' => 'list/rating_one'));?>
                </li>
                <li role="presentation" class="divider"></li>
                <li role="presentation">
                    <?php echo $this->Html->link('Two Star Rating (' .$userOverview[0][0]['twoStarRatingLead']. ')', array('controller' => 'users', 'action' => 'list/rating_two'));?>
                </li>
                <li role="presentation" class="divider"></li>
                <li role="presentation">
                    <?php echo $this->Html->link('Three Star Rating (' .$userOverview[0][0]['threeStarRatingLead']. ')', array('controller' => 'users', 'action' => 'list/rating_three'));?>
                </li>
                <li role="presentation" class="divider"></li>
                <li role="presentation">
                    <?php echo $this->Html->link('Four Star Rating (' .$userOverview[0][0]['fourStarRatingLead']. ')', array('controller' => 'users', 'action' => 'list/rating_four'));?>
                </li>
                <li role="presentation" class="divider"></li>
                <li role="presentation">
                    <?php echo $this->Html->link('Five Star Rating (' .$userOverview[0][0]['fiveStarRatingLead']. ')', array('controller' => 'users', 'action' => 'list/rating_five'));?>
                </li>
                <li role="presentation" class="divider"></li>
                <li role="presentation">
                    <?php echo $this->Html->link('No Rating (' .$userOverview[0][0]['noRatingLead']. ')', array('controller' => 'users', 'action' => 'list/no_rating'));?>
                </li>
            </ul>
        </div>
        <button type="button" class="btn btn-theme btn-lg margin-left-15" data-toggle="modal" data-target="#takeNote">
            Take Note
        </button>
    </div>

    <form action="<?php echo Router::url('/', true);?>instructor/users/search" method=get class="form-inline pull-right page-option-right">
        <div class="form-group">
            <input type="text" name="user" class="form-control" placeholder="Search user...">
        </div>
        <div class="form-group">
            <input type="submit" class="form-control btn btn-default" value="Search">
        </div>
        <?php echo $this->Html->link('Import User', array('controller' => 'users', 'action' => 'import'), array('class' => 'btn btn-theme'))?>
        <?php echo $this->Html->link('Export User', array('controller' => 'users', 'action' => 'export'), array('class' => 'btn btn-theme'))?>
    </form>
</div>

<div class="table-responsive">
    <?php if($userList):?>
        <table class="table data-table table-bordered sort-table" colspecing = 0>
        <thead>
        <tr>
            <th><?php echo $this->Paginator->sort('Profile.first_name', 'Name'); ?></th>
            <th><?php echo $this->Paginator->sort('User.username', 'Email/Phone'); ?></th>
            <th><?php echo $this->Paginator->sort('User.status', 'Status'); ?></th>
            <th><?php echo $this->Paginator->sort('rating', 'Rating'); ?></th>
            <th><?php echo $this->Paginator->sort('User.created', 'Notes'); ?></th>
            <th>
                Most Recent Note
            </th>
            <th>
                Last Contact
            </th>
            <th>
                Followup Date
            </th>
            <th style="width: 12%;">Lesson remaining/scheduled</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach($userList as $users):?>
        <tr>
            <td>
                <?php echo $this->Html->link($users['Profile']['first_name']. ' '. $users['Profile']['last_name'], array('controller' => 'users', 'action' => 'details', $users['User']['uuid']), array('class' => 'green'))?>
            </td>
            <td>
                <a href="mailto:<?php echo $users['User']['username'];?>"><?php echo $users['User']['username'];?></a>
                <?php
                if($users['Profile']['phone'])
                {
                    echo '<br/><a href="tel:'.$users['Profile']['phone'].'"><i class="italic-sm">(phone: '.$users['Profile']['phone'].')</i></a>';
                }
                ?>
            </td>
            <td>
                <?php
                echo $this->Utilities->getUserStatus($users['User']['status']);
                if($users['User']['is_slipping_away'] == 1){
                    echo '<label class="text-uppercase danger margin-left-5">Slipping Away<label>';
                }
                ?>
            </td>
            <td>
                <select class="usersRatings">
                    <option value="" selected="selected"><?php echo $users['User']['id'];?></option>
                    <?php
                    for ($counter = 1; $counter <= 5; $counter++) {
                        $select = '';
                        if($counter == $users['User']['rating']){
                            $select = 'selected';
                        }
                        echo '<option '.$select.' value="'.$counter.'">'.$users['User']['id'].'</option>';
                    }
                    ?>
                </select>
            </td>
            <td>
                <?php echo $this->Html->link('see note', array('controller' => 'users', 'action' => 'details/' . $users['User']['uuid'] . '#note'), array('class' => 'd-btn'))?>
            </td>
            <td>
                <?php
                if(isset($users['Note'][0]['note']))
                {
                    echo $this->Html->link($this->Text->excerpt($users['Note'][0]['note'], null, 20, ' <span class="green">...more</span>'), array('controller' => 'users', 'action' => 'details/'.$users['User']['uuid'].'#'.$users['Note'][0]['id'] ), array('escape' => false));
                }
                else{
                    echo 'N/A';
                }
                ?>
            </td>
            <td>
                <?php
                if(isset($users['Note'][0]['created']))
                {
                    echo '<time>'.$this->Time->format('M d, Y', $users['Note'][0]['created']);
                    echo ' <i class="italic-sm">('.$this->Time->format('h:i A', $users['Note'][0]['created']).') <i/></time>';
                }
                else{
                    echo ' N/A';
                }
                ?>
            </td>
            <td>
                <?php
                if(isset($users['LastFollowupDate'][0]['note_date']))
                {
                    echo '<time>'.$this->Time->format('M d, Y', $users['LastFollowupDate'][0]['note_date']);
                    echo ' <i class="italic-sm">('.$this->Time->format('h:i A', $users['LastFollowupDate'][0]['note_date']).') <i/></time>';
                }
                else{
                    echo ' N/A';
                }
                ?>
            </td>
            <td style="text-align: center;">
                <?php
                if($users['CountLesson']['lesson_left'])
                {
                    echo $users['CountLesson']['lesson_left'];
                }
                else{
                    echo 0;
                }
                echo '/';
                if($users['CountLesson']['appointed_lesson_count'])
                {
                    echo $users['CountLesson']['appointed_lesson_count'];
                }
                else{
                    echo 0;
                }
                ?>
            </td>
            <td>
                <div class="icons-area">
                    <?php echo $this->Html->link('<i class="fa fa-search-plus"></i>', array('controller' => 'users', 'action' => 'details', $users['User']['uuid']), array('escape' => false, 'class' => 'icon green'))?>
                    <?php echo $this->Html->link('<i class="fa fa-pencil"></i>', array('controller' => 'users', 'action' => 'edit', $users['User']['uuid']), array('escape' => false, 'class' => 'icon primary'))?>
                    <?php
                    echo $this->Form->postLink(
                        '<i class="glyphicon glyphicon-remove"></i>',
                        array(
                            'controller' => 'users',
                            'action' => 'changeStatus',
                            $users['User']['uuid'],
                            9
                        ),
                        array(
                            'class' => 'icon danger',
                            'escape' => false
                        ),
                        __('Are you sure you want to delete this user')
                    );
                    ?>
                </div>
            </td>
        </tr>
        <?php endforeach;?>
        </tbody>
    </table>
        <?php echo $this->element('pagination');?>
    <?php else:?>
        <h2 class="not-found">Sorry, student not found</h2>
        <hr class="shadow-line"/>
    <?php endif;?>
</div>


<!-- Modal For Displaying Schedule -->
<div class="modal event-modal fade" id="takeNote" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="border-bottom: 1px solid #e5e5e5;">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                </button>
                <h4 class="sm-title">Take Note</h4>
            </div>
            <div class="modal-body">
                <!----- ADD NEW CONTACT NOTE ------>
                <div class="">
                    <?php echo $this->Form->create(
                        'Contact',
                        array('controller' => 'contacts', 'action' => 'note', 'class' => 'form-horizontal form-custom')
                    ); ?>
                    <input name="noteFor" type="hidden" value="student">
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label>Student name:</label>
                            <input name="data[Note][contactPerson]" class="typeahead form-control" type="text" placeholder="Type student name">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-6">
                            <label>Follow up date:</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                                <input id="datepicker" name="data[Note][note_date]" type="text" class="form-control" placeholder="Follow up date">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            </div>
                            <?php echo $this->Form->error('Note.note_date');?>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-6">
                            <div class="input-group">
                                <label>Rating:</label>
                                <select class="takeNoteRating" name="data[Note][rating]">
                                    <option value="" selected="selected"></option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                </select>
                            </div>
                            <?php echo $this->Form->error('Note.note_date');?>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label>Note:</label>
                            <?php
                            echo $this->Form->input(
                                'Note.note',
                                array(
                                    'type' => 'textarea',
                                    'class' => 'form-control',
                                    'placeholder' => 'Place your note here',
                                    'rows' => 5,
                                    'label' => false,
                                    'required' => false,
                                )
                            );
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-4">
                            <button class="btn btn-theme">Save Note</button>
                        </div>
                    </div>
                    <?php echo $this->Form->end(); ?>
                    <div class="clearfix"></div>
                </div>
                <!----- /ADD NEW CONTACT NOTE ------>
            </div>
        </div>
    </div>
</div>
<!-- /Modal For Displaying Schedule -->

<?php echo $this->Html->script('typehead');?>
<script>
    // Instantiate the Bloodhound suggestion engine
    var contacts = new Bloodhound({
        datumTokenizer: function (datum) {
            return Bloodhound.tokenizers.whitespace(datum.value);
        },
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: {
            url: '<?php echo Router::url('/', true);?>instructor/contacts/getContacts/student/%QUERY',
            filter: function (contacts) {
                return $.map(contacts, function (contact) {
                    console.log(contact);
                    return {
                        value: contact['0']['contactName']
                    };
                });
            }
        }
    });

    // Initialize the Bloodhound suggestion engine
    contacts.initialize();
    // Instantiate the Typeahead UI
    $('.typeahead').typeahead(null, {
        name: 'states',
        displayKey: 'value',
        source: contacts.ttAdapter()
    });

    $(function() {
        $('.takeNoteRating').barrating('show', {
            theme: 'fontawesome-stars'
        });

        $('.usersRatings').barrating('show', {
            initialRating: 0,
            theme: 'fontawesome-stars',
            onSelect:function(value, text){

                $.ajax({
                    url: '<?php echo Router::url('/', true);?>instructor/users/set_rating',
                    type: "POST",
                    dataType: "json",
                    data:  {userID: text, rating: value},
                    success: function(response) {
                        if(response == 1){
                            manageFlashMessage('alert-success', 'Your rating has been successfully complete');
                        }
                        else{
                            manageFlashMessage('alert-danger', 'Sorry, something went wrong');
                        }
                    },
                    error: function (xhr, status, error) {
                        console.log(error);
                    }
                });

            }
        });
    });
</script>