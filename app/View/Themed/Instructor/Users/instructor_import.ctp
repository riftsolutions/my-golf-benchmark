<?php
/**
 * @var $this View
 */
?>
<h2 class="page-title">Import User</h2>
<hr class="shadow-line"/>
<div class="row">
    <div class="col-md-8 col-md-offset-2 col-sm-12">
        <div class="box box-padding-adding radius-5 margin-top-25 margin-bottom-25">


            <?php echo $this->Form->create(
                'User',
                array('controller' => 'users', 'action' => 'import', 'class' => 'form-inline form-custom', 'type' => 'file')
            ); ?>

            <div class="form-group">
                <label class="col-sm-4 control-label">CSV/Excel File</label>

                <div class="col-sm-8">
                    <?php
                    echo $this->Form->inout(
                        'file',
                        array(
                            'type' => 'file',
                            'class' => 'form-control',
                            'placeholder' => 'First Name',
                            'label' => false,
                            'error' => false,
                            'required' => false,
                        )
                    );
                    ?>
                    <?php echo $this->Form->error('file'); ?>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12">
                    <button class="btn btn-theme pull-right">Import User</button>
                </div>
            </div>
            <?php echo $this->Form->end(); ?>
        </div>
    </div>
</div>