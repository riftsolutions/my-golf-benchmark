<?php
/**
 * @var $this View
 */
?>
<h2 class="page-title">Note List</h2>
<hr class="shadow-line"/>

<div class="dropdown margin-bottom-10">
    <button type="button" class="btn btn-theme btn-lg" data-toggle="modal" data-target="#takeNote">
        Take Note
    </button>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="d-box" style="min-height: 100px;">
            <?php if ($notes): ?>
                <table class="table table-striped margin-top-25 sort-table">
                    <thead>
                        <th style="width: 12%;">Student Name</th>
                        <th style="width: 15%;">Contact</th>
                        <th>Note</th>
                        <th style="width: 15%;">Rating</th>
                        <th style="width: 15%;">Time</th>
                    </thead>
                    <tbody>
                    <?php foreach ($notes as $note): ?>
                        <tr>
                            <td>
                                <?php
                                echo $this->Html->link(
                                    $note['Profile']['first_name'] . ' ' . $note['Profile']['last_name'],
                                    array('controller' => 'users', 'action' => 'details', $note['User']['uuid']),
                                    array('class' => 'green')
                                );
                                ?>
                            </td>

                            <td>
                                <strong>
                                    <?php if ($note['Profile']['phone']): ?>
                                        <a href="tel: <?php echo $note['Profile']['phone']; ?>">
                                            <?php
                                            echo substr($note['Profile']['phone'], 0, 3) . "-" . substr(
                                                    $note['Profile']['phone'],
                                                    3,
                                                    3
                                                ) . "-" . substr($note['Profile']['phone'], 6);
                                            ?>
                                        </a>
                                        <br/>
                                    <?php endif; ?>
                                    <?php echo $this->Html->link(
                                        $note['User']['username'],
                                        array('controller' => 'users', 'action' => 'details', $note['User']['uuid']),
                                        array('class' => 'green')
                                    ); ?>
                                </strong>
                            </td>
                            <td>
                                <?php
                                echo $this->Html->link(
                                    $this->Text->excerpt(
                                        $note['Note']['note'],
                                        null,
                                        100,
                                        ' <span class="green">...more</span>'
                                    ),
                                    array(
                                        'controller' => 'users',
                                        'action' => 'details/' . $note['User']['uuid'] . '#' . $note['Note']['id']
                                    ),
                                    array('escape' => false)
                                );;?>
                            </td>
                            <td>
                                <?php
                                echo $this->Form->input(
                                    'noteRating',
                                    array(
                                        'type' => 'select',
                                        'class' => 'note-rating',
                                        'label' => false,
                                        'note-id' => $note['Note']['id'],
                                        'options' => array(
                                            1 => 1,
                                            2 => 2,
                                            3 => 3,
                                            4 => 4,
                                            5 => 5,
                                        ),
                                        'value' => $note['Note']['rating']
                                    )
                                )
                                ?>

                                <?php
                                $style = 'style="display: none;"';
                                if ($note['Note']['rating']) {
                                    $style = 'style="display: visible;"';
                                }
                                ?>

                                <br/>

                                <div style="height: 25px;">
                                    <div class="rating" <?php echo $style; ?>>
                                    <span class="star-color rating-stars">
                                        <?php
                                        $counter = 0;
                                        $rating = $note['Note']['rating'];
                                        while ($rating > $counter):
                                            ?>
                                            <i class="fa fa-star"></i>
                                            <?php
                                            $counter++;
                                        endwhile;
                                        ?>
                                    </span>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <?php echo $this->Time->format('M d, Y', $note['Profile']['created'])?>
                                <i class="italic-md">
                                     (<?php echo $this->Time->format('h:i A', $note['Profile']['created'])?>)
                                </i>
                            </td>

                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
                <?php echo $this->element('pagination'); ?>
            <?php else: ?>
                <?php echo $this->element('not-found') ?>
            <?php endif; ?>
            <div class="clearfix"></div>
        </div>
    </div>
</div>


<script>
    $(document).on('click', '.note-rating', function () {
        var noteRating = $(this);
        var noteID = $(this).attr('note-id');
        var rating = $(this).val();
        var noteInfo = {'noteID': noteID, 'rating': rating};

        var stars = '';
        var counter = 0;
        while (rating > counter) {
            stars += '<i class="fa fa-star"></i>';
            counter++;
        }

        console.log(stars);


        $.ajax({
            url: '<?php echo Router::url('/', true);?>instructor/contacts/setRating',
            type: "POST",
            dataType: "json",
            data: {'noteInfo': noteInfo},
            success: function (data) {
                if (data == 1) {
                    $(noteRating).parent().closest('tr').addClass('success');
                    $(noteRating).parent().parent().find('.show-rating').text(rating);
                    $(noteRating).parent().parent().find('.rating-stars').html(stars);
                    $(noteRating).parent().parent().find('.rating').show();

                    manageFlashMessage('alert-success', 'The Follow Up Rating has been updated')
                }
                else if (data == 0) {
                    manageFlashMessage('alert-danger', 'Sorry, something went wrong')
                }
            },
            error: function (xhr, status, error) {
                console.log(error);
            }
        });
    });
</script>


<!-- Modal For Displaying Schedule -->
<div class="modal event-modal fade" id="takeNote" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="border-bottom: 1px solid #e5e5e5;">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                </button>
                <h4 class="sm-title">Take Note</h4>
            </div>
            <div class="modal-body">
                <!----- ADD NEW CONTACT NOTE ------>
                <div class="">
                    <?php echo $this->Form->create(
                        'Contact',
                        array('controller' => 'contacts', 'action' => 'note', 'class' => 'form-horizontal form-custom')
                    ); ?>
                    <input name="noteFor" type="hidden" value="student">
                    <div class="form-group">
                        <div class="col-sm-12">
                            <input name="data[Note][contactPerson]" class="typeahead form-control" type="text" placeholder="Type Student Name">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <?php
                            echo $this->Form->input(
                                'Note.note',
                                array(
                                    'type' => 'textarea',
                                    'class' => 'form-control',
                                    'placeholder' => 'Place your note here',
                                    'rows' => 5,
                                    'label' => false,
                                    'required' => false,
                                )
                            );
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-4">
                            <button class="btn btn-theme">Save Note</button>
                        </div>
                    </div>
                    <?php echo $this->Form->end(); ?>
                    <div class="clearfix"></div>
                </div>
                <!----- /ADD NEW CONTACT NOTE ------>
            </div>
        </div>
    </div>
</div>
<!-- /Modal For Displaying Schedule -->

<?php echo $this->Html->script('typehead');?>
<script>
    // Instantiate the Bloodhound suggestion engine
    var contacts = new Bloodhound({
        datumTokenizer: function (datum) {
            return Bloodhound.tokenizers.whitespace(datum.value);
        },
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: {
            url: '<?php echo Router::url('/', true);?>instructor/contacts/getContacts/student/%QUERY',
            filter: function (contacts) {
                return $.map(contacts, function (contact) {
                    console.log(contact);
                    return {
                        value: contact['0']['contactName']
                    };
                });
            }
        }
    });

    // Initialize the Bloodhound suggestion engine
    contacts.initialize();
    // Instantiate the Typeahead UI
    $('.typeahead').typeahead(null, {
        name: 'states',
        displayKey: 'value',
        source: contacts.ttAdapter()
    });
</script>