<?php
/**
 * @var $this View
 */
?>
<h2 class="page-title">List of Student</h2>
<hr class="shadow-line"/>

<form action="<?php echo Router::url('/', true);?>instructor/users/search" method=get class="form-inline pull-right">
    <div class="form-group">
        <input type="text" name="user" class="form-control" placeholder="Search user...">
    </div>
    <div class="form-group">
        <input type="submit" class="form-control btn btn-default" value="Search">
    </div>
</form>

<div class="table-responsive">
    <?php if(isset($users)):?>
        <table class="table data-table table-bordered sort-table" colspecing = 0>
        <thead>
        <tr>
            <th><?php echo $this->Paginator->sort('name', 'Name'); ?></th>
            <th><?php echo $this->Paginator->sort('username', 'Email/Username'); ?></th>
            <th><?php echo $this->Paginator->sort('phone', 'Phone'); ?></th>
            <th><?php echo $this->Paginator->sort('status', 'Status'); ?></th>
            <th><?php echo $this->Paginator->sort('created', 'Member Since'); ?></th>
            <th><?php echo $this->Paginator->sort('last_login', 'Last Login'); ?></th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach($users as $user):?>
        <tr>
            <td>
                <?php echo $this->Html->link($user['Profile']['first_name']. ' '.$user['Profile']['last_name'], array('controller' => 'users', 'action' => 'details', $user['User']['uuid']), array('class' => 'green'))?>
            </td>
            <td>
                <?php echo $this->Html->link($user['User']['username'], array('controller' => 'users', 'action' => 'details', $user['User']['uuid']));?>
            </td>
            <td>
                <?php if($user['Profile']['phone']):?>
                    <?php
                    echo $this->Html->link($user['Profile']['phone'], array('controller' => 'users', 'action' => 'details', $user['User']['uuid']));
                    ?>
                <?php else:?>
                    N/A
                <?php endif;?>
            </td>
            <td>
                <?php echo $this->Utilities->getUserStatus($user['User']['status']);?>
            </td>
            <td>
                <time><?php echo $this->Html->link($this->Time->format('M d, Y', $user['User']['created']), array('controller' => 'users', 'action' => 'details', $user['User']['uuid']));?></time>
            </td>
            <td>
                <time>
                    <?php
                    if(!strstr('0000-00-00 00:00:00', $user['User']['last_login']) && $user['User']['last_login']){
                        echo $this->Time->timeAgoInWords($user['User']['last_login'],
                            array('accuracy' => array('month' => 'month'),'end' => '1 year'));
                    }
                    else{
                        echo '<strong class="danger">Never logged in</strong>';
                    }
                    ?>
                </time>
            </td>
            <td>
                <div class="icons-area">
                    <?php echo $this->Html->link('Details', array('controller' => 'users', 'action' => 'details', $user['User']['uuid']), array('class' => 'd-btn'))?>
                </div>
            </td>
        </tr>
        <?php endforeach;?>
        </tbody>
    </table>
        <?php echo $this->element('pagination');?>
    <?php else:?>
        <h2 class="not-found">Sorry, user not found</h2>
        <hr class="shadow-line"/>
    <?php endif;?>
</div>