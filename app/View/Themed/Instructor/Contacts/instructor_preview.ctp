<?php
/**
 * @var $this View
 */
?>
<h2 class="page-title">Preview of Lead</h2>
<hr class="shadow-line"/>

<div class="margin-bottom-10">
    <h2 class="title-lg pull-left">Total Contact: <?php echo sizeof($contacts); ?></h2>
    <form method="post">
        <button class="btn btn-theme pull-right">Import All Contact</button>
    </form>
    <div class="clearfix"></div>
</div>


<table class="table data-table table-bordered" colspecing=0>
    <thead>
    <tr>
        <th style="width: 5%;">Serial</th>
        <th style="width: 15%;">Name</th>
        <th style="width: 20%;">Email Address</th>
        <th style="width: 15%;">Phone</th>
        <th style="width: 15%;">Account Type</th>
        <th style="width: 15%;">Member Type</th>
        <th style="width: 20%;">Note</th>
        <th style="width: 15%;">Created</th>
    </tr>
    </thead>
    <tbody>
    <?php $counter = 1;?>
    <?php foreach($contacts as $contact):?>
        <tr>
            <td><?php echo $counter; $counter++;?></td>
            <td><?php echo $contact['Contact']['first_name']. ' '. $contact['Contact']['last_name']; ?></td>
            <td><?php echo $contact['Contact']['email']; ?></td>
            <td><?php echo $contact['Contact']['phone']; ?></td>
            <td>
                <?php
                if($contact['Contact']['account_type'] == 1){
                    echo 'Student';
                }
                elseif($contact['Contact']['account_type'] == 2){
                    echo 'Customer';
                }
                elseif($contact['Contact']['account_type'] == 3){
                    echo 'Lead/Prospect';
                }
                else{
                    echo 'N/A';
                }
                ?>
            </td>

            <td>
                <?php if ($contact['Contact']['member_type'] == 1): ?>
                    <label class="label label-info">new member</label>
                <?php elseif ($contact['Contact']['member_type'] == 2): ?>
                    <label class="label label-success">already member</label>
                <?php elseif($contact['Contact']['member_type'] == 3): ?>
                    <label class="label label-danger">not member</label>
                <?php endif; ?>
            </td>

            <td><?php echo $this->Text->excerpt($contact['Contact']['note'], null, 50, '...'); ?></td>
            <td><?php echo $contact['Contact']['created']; ?></td>
        </tr>
    <?php endforeach;?>
    </tbody>
</table>