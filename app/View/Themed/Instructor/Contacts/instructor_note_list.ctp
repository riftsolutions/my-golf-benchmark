<?php
/**
 * @var $this View
 */
?>
<h2 class="page-title"><?php echo $title;?></h2>
<hr class="shadow-line"/>
<div class="page-option">
    <div class="dropdown margin-bottom-10 page-option-left">
        <button class="btn btn-info dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown">
            Quick Navigation
            <span class="caret"></span>
        </button>
        <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
            <li role="presentation">
                <?php echo $this->Html->link('All notes (' .$overview['total']. ')', array('controller' => 'contacts', 'action' => 'note_list'));?>
            </li>
            <li role="presentation" class="divider"></li>
            <li role="presentation">
                <?php echo $this->Html->link('Lead\'s follow up notes (' .$overview['leadsFollowup']. ')', array('controller' => 'contacts', 'action' => 'note_list/leads'));?>
            </li>
            <li role="presentation" class="divider"></li>
            <li role="presentation">
                <?php echo $this->Html->link('User\'s follow up notes (' .$overview['usersFollowup']. ')', array('controller' => 'contacts', 'action' => 'note_list/users'));?>
            </li>
            <li role="presentation" class="divider"></li>
            <li role="presentation">
                <?php echo $this->Html->link('No rating follow up notes (' .$overview['noRatingFollowup']. ')', array('controller' => 'contacts', 'action' => 'note_list/no_rating'));?>
            </li>
            <li role="presentation" class="divider"></li>
            <li role="presentation">
                <?php echo $this->Html->link('One star rating follow up notes (' .$overview['oneStarRatingFollowup']. ')', array('controller' => 'contacts', 'action' => 'note_list/rating_one'));?>
            </li>
            <li role="presentation" class="divider"></li>
            <li role="presentation">
                <?php echo $this->Html->link('Two star rating follow up notes (' .$overview['twoStarRatingFollowup']. ')', array('controller' => 'contacts', 'action' => 'note_list/rating_two'));?>
            </li>
            <li role="presentation" class="divider"></li>
            <li role="presentation">
                <?php echo $this->Html->link('Three star rating follow up notes (' .$overview['threeStarRatingFollowup']. ')', array('controller' => 'contacts', 'action' => 'note_list/rating_three'));?>
            </li>
            <li role="presentation" class="divider"></li>
            <li role="presentation">
                <?php echo $this->Html->link('Four star rating follow up notes (' .$overview['fourStarRatingFollowup']. ')', array('controller' => 'contacts', 'action' => 'note_list/rating_four'));?>
            </li>
            <li role="presentation" class="divider"></li>
            <li role="presentation">
                <?php echo $this->Html->link('Five star rating follow up notes (' .$overview['fiveStarRatingFollowup']. ')', array('controller' => 'contacts', 'action' => 'note_list/rating_five'));?>
            </li>
            <li role="presentation" class="divider"></li>
            <li role="presentation">
                <?php echo $this->Html->link('Upcoming follow up notes (' .$overview['upComingFollowup']. ')', array('controller' => 'contacts', 'action' => 'note_list/upcoming'));?>
            </li>
            <li role="presentation" class="divider"></li>
            <li role="presentation">
                <?php echo $this->Html->link('Expired follow up notes (' .$overview['expiredFollowup']. ')', array('controller' => 'contacts', 'action' => 'note_list/expired'));?>
            </li>
            <li role="presentation" class="divider"></li>
            <li role="presentation">
                <?php echo $this->Html->link('Followed follow up notes (' .$overview['followedFollowup']. ')', array('controller' => 'contacts', 'action' => 'note_list/followed'));?>
            </li>
            <li role="presentation" class="divider"></li>
            <li role="presentation">
                <?php echo $this->Html->link('Unfollowed follow up notes (' .$overview['unfollowedFollowup']. ')', array('controller' => 'contacts', 'action' => 'note_list/unfollowed'));?>
            </li>
        </ul>

        <button type="button" class="btn btn-theme btn-lg" data-toggle="modal" data-target="#takeNote">
            Take Note
        </button>

        <button type="button" class="btn btn-theme btn-lg" data-toggle="modal" data-target="#searchNote">
            Search Note
        </button>
    </div>
</div>


<div class="row">
<div class="col-lg-12">
<div class="d-box" style="min-height: 100px;">
<?php if($notes):?>
    <table class="table table-striped margin-top-25 sort-table" >
    <thead>
    <th style="width: 10%;"><?php echo $this->Paginator->sort('Contact.first_name. Contact.last_name', 'Name'); ?></th>
    <th style="width: 8%;">
        <?php echo $this->Paginator->sort('account_type', 'Note For'); ?>
    </th>
    <th style="width: 15%;">
        <?php echo $this->Paginator->sort('email', 'Email/Phone'); ?>
    </th>

    <th><?php echo $this->Paginator->sort('Note.note', 'Note'); ?></th>
    <th style="width: 15%;"><?php echo $this->Paginator->sort('Note.rating', 'Rating'); ?></th>
    <th style="width: 5%;"><?php echo $this->Paginator->sort('Note.status', 'Status'); ?></th>
    <th style="width: 10%;"><?php echo $this->Paginator->sort('Note.note_date', 'Follow Up Date'); ?></th>
    <th style="width: 7%;">Action</th>
    </thead>
    <tbody>
    <?php foreach($notes as $note):?>
        <tr>
        <td>
            <?php
            if($note['Contact']['id']){
                echo $this->Html->link($note['Contact']['first_name'].' '. $note['Contact']['last_name'], array('controller' => 'contacts', 'action' => 'view', $note['Contact']['id']), array('class' => 'green'));
            }
            elseif($note['User']['id']){
                echo $this->Html->link($note['Profile']['first_name'].' '. $note['Profile']['last_name'], array('controller' => 'users', 'action' => 'details', $note['User']['uuid']), array('class' => 'green'));
            }
            else{
                echo 'N/A';
            }

            ;?>
        </td>
        <td>
            <?php
            if($note['Contact']['id']){
                echo '<label class="label label-info">lead</label>';
            }
            elseif($note['User']['id']){
                echo '<label class="label label-success">student</label>';
            }
            else{
                echo '<label class="label label-default">N/A</label>';
            }
            ?>
        </td>

        <td>
            <span>

                <?php if($note['Contact']['id']):?>

                    <?php echo $this->Html->link($note['Contact']['email'], array('controller' => 'contacts', 'action' => 'view', $note['Contact']['id']), array('class' => 'green')); ?> <br/>
                    <?php if($note['Contact']['phone']):?>
                        <a href="tel: <?php echo $note['Contact']['phone'];?>">
                            <?php
                            echo substr($note['Contact']['phone'], 0, 3)."-".substr($note['Contact']['phone'], 3, 3)."-".substr($note['Contact']['phone'],6);
                            ?>
                        </a>
                        <br/>
                    <?php endif;?>


                <?php elseif($note['User']['id']):?>

                    <?php echo $this->Html->link($note['User']['username'], array('controller' => 'users', 'action' => 'details', $note['User']['uuid']), array('class' => 'green')); ?><br/>
                    <?php if($note['Profile']['phone']):?>
                        <a href="tel: <?php echo $note['Profile']['phone'];?>">
                            <?php
                            echo substr($note['Profile']['phone'], 0, 3)."-".substr($note['Profile']['phone'], 3, 3)."-".substr($note['Profile']['phone'],6);
                            ?>
                        </a>
                        <br/>
                    <?php endif;?>


                <?php endif;?>
            </span>
        </td>
        <td>
            <?php
            if($note['Contact']['id']){
                echo $this->Html->link($this->Text->excerpt($note['Note']['note'], null, 100, ' <span class="green">...more</span>'), array('controller' => 'contacts', 'action' => 'view/'.$note['Contact']['id'].'#'.$note['Note']['id'] ), array('escape' => false));
            }
            if($note['User']['id']){
                echo $this->Html->link($this->Text->excerpt($note['Note']['note'], null, 100, ' <span class="green">...more</span>'), array('controller' => 'users', 'action' => 'details/'.$note['User']['uuid'].'#'.$note['Note']['id'] ), array('escape' => false));
            }

            ;?>
        </td>
        <td>

            <select class="noteRatings">
                <option value="" selected="selected"></option>
                <?php
                for ($counter = 1; $counter <= 5; $counter++) {
                    $select = '';
                    if($counter == $note['Note']['rating']){
                        $select = 'selected';
                    }
                    echo '<option '.$select.' value="'.$counter.'">'.$note['Note']['id'].'</option>';
                }
                ?>
            </select>

        </td>
        <td>
                                <?php
                                $today = (array) new DateTime("now");
                                $noteDate = $note['Note']['note_date'];
                                if($note['Note']['status'] == 2){
                                    echo '<span class="label label-success">followed</span>';
                                }
                                if($note['Note']['status'] == 3){
                                    echo '<span class="label label-danger">unfollowed</span>';
                                }
                                elseif(strtotime($today['date']) < strtotime($noteDate) && $note['Note']['status'] == 1){
                                    echo '<span class="label label-info">upcoming</span>';
                                }
                                elseif(strtotime($today['date']) > strtotime($noteDate) && $note['Note']['status'] == 1){
                                    echo '<span class="label label-warning">expired</span>';
                                }
                                ?>
                            </td>
        <td>
            <?php if($note['Note']['note_date']):?>
                <?php
                $today = (array) new DateTime("now");
                $noteDate = $note['Note']['note_date'];
                ?>

                <?php if(strtotime($today['date']) > strtotime($noteDate) && $note['Note']['status'] == 1):?>
                    <i class="italic-md danger"><?php echo $this->Time->format('d M, Y', $note['Note']['note_date']);?></i>
                <?php else:?>
                    <i class="italic-md"><?php echo $this->Time->format('d M, Y', $note['Note']['note_date']);?></i>
                <?php endif; ?>
            <?php else:?>
                <label class="label label-danger">N/A</label>
            <?php endif;?>

        </td>
        <!--<td>
                                <?php
/*                                if ($note['Note']['status'] == 3) {
                                    echo $this->Html->link(
                                        'followed',
                                        array(
                                            'controller' => 'contacts',
                                            'action' => 'followedNotes',
                                            $note['Note']['id'],
                                            2
                                        ),
                                        array(
                                            'escape' => false,
                                            'class' => 'd-btn'
                                        )
                                    );
                                }
                                if ($note['Note']['status'] == 1) {
                                    echo $this->Html->link(
                                        'followed',
                                        array(
                                            'controller' => 'contacts',
                                            'action' => 'followedNotes',
                                            $note['Note']['id'],
                                            2
                                        ),
                                        array(
                                            'escape' => false,
                                            'class' => 'd-btn margin-right-5'
                                        )
                                    );
                                    echo $this->Html->link(
                                        'unfollowed',
                                        array(
                                            'controller' => 'contacts',
                                            'action' => 'followedNotes',
                                            $note['Note']['id'],
                                            3
                                        ),
                                        array(
                                            'escape' => false,
                                            'class' => 'd-btn'
                                        )
                                    );
                                }
                                elseif ($note['Note']['status'] == 2) {
                                    echo $this->Html->link(
                                        'unfollowed',
                                        array(
                                            'controller' => 'contacts',
                                            'action' => 'followedNotes',
                                            $note['Note']['id'],
                                            3
                                        ),
                                        array(
                                            'escape' => false,
                                            'class' => 'd-btn'
                                        )
                                    );
                                }
                                */?>
                            </td>-->
        <td>
            <?php
            if ($note['Note']['status'] == 3) {
                echo $this->Html->link(
                    'followed',
                    array(
                        'controller' => 'contacts',
                        'action' => 'followedNotes',
                        $note['Note']['id'],
                        2
                    ),
                    array(
                        'escape' => false,
                        'class' => 'd-btn'
                    )
                );
            }
            elseif(strtotime($today['date']) < strtotime($noteDate) && $note['Note']['status'] == 1){
                echo $this->Html->link(
                    'followed',
                    array(
                        'controller' => 'contacts',
                        'action' => 'followedNotes',
                        $note['Note']['id'],
                        2
                    ),
                    array(
                        'escape' => false,
                        'class' => 'd-btn margin-right-5'
                    )
                );
                echo $this->Html->link(
                    'unfollowed',
                    array(
                        'controller' => 'contacts',
                        'action' => 'followedNotes',
                        $note['Note']['id'],
                        3
                    ),
                    array(
                        'escape' => false,
                        'class' => 'd-btn'
                    )
                );
            }
            elseif ($note['Note']['status'] == 2) {
                echo $this->Html->link(
                    'unfollowed',
                    array(
                        'controller' => 'contacts',
                        'action' => 'followedNotes',
                        $note['Note']['id'],
                        3
                    ),
                    array(
                        'escape' => false,
                        'class' => 'd-btn'
                    )
                );
            }
            ?>
        </td>
        </tr>
    <?php endforeach;?>
    </tbody>
    </table>
    <?php echo $this->element('pagination');?>
<?php else:?>
    <?php echo $this->element('not-found')?>
<?php endif;?>
<div class="clearfix"></div>
</div>
</div>
</div>


<script>
    /*$(document).on('click', '.note-rating', function(){
        var noteRating = $(this);
        var noteID = $(this).attr('note-id');
        var rating = $(this).val();
        var noteInfo = {'noteID': noteID, 'rating': rating};

        var stars = '';
        var counter = 0;
        while(rating > counter){
            stars += '<i class="fa fa-star"></i>';
            counter++;
        }

        console.log(stars);


        $.ajax({
            url: '<?php echo Router::url('/', true);?>instructor/contacts/setRating',
            type: "POST",
            dataType: "json",
            data: {'noteInfo': noteInfo},
            success: function (data) {
                if(data == 1){
                    $(noteRating).parent().closest('tr').addClass('success');
                    $(noteRating).parent().parent().find('.show-rating').text(rating);
                    $(noteRating).parent().parent().find('.rating-stars').html(stars);
                    $(noteRating).parent().parent().find('.rating').show();

                    manageFlashMessage('alert-success', 'The Follow Up Rating has been updated')
                }
                else if(data == 0){
                    manageFlashMessage('alert-danger', 'Sorry, something went wrong')
                }
            },
            error: function (xhr, status, error) {
                console.log(error);
            }
        });
    });*/


    $('.noteRatings').barrating('show', {
        initialRating: 0,
        theme: 'fontawesome-stars',
        onSelect:function(value, text){

            $.ajax({
                url: '<?php echo Router::url('/', true);?>instructor/contacts/setRating',
                type: "POST",
                dataType: "json",
                data:  {noteID: text, rating: value},
                success: function(response) {

                    console.log(response);

                    if(response == 1){
                        manageFlashMessage('alert-success', 'Your rating has been successfully complete');
                    }
                    else{
                        manageFlashMessage('alert-danger', 'Sorry, something went wrong');
                    }
                },
                error: function (xhr, status, error) {
                    console.log(error);
                }
            });

        }
    });
</script>



<!-- Modal For Displaying Schedule -->
<div class="modal event-modal fade" id="takeNote" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="border-bottom: 1px solid #e5e5e5;">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                </button>
                <h4 class="sm-title">Take Note</h4>
            </div>
            <div class="modal-body">
                <!----- ADD NEW CONTACT NOTE ------>
                <div class="">
                    <?php echo $this->Form->create(
                        'Contact',
                        array('controller' => 'contacts', 'action' => 'note', 'class' => 'form-horizontal form-custom')
                    ); ?>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label>Choose One</label>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="noteFor" class="noteForStudent" value="student" checked="">
                                    Student
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="noteFor" class="noteForContact" value="contact">
                                    Lead
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label>Name</label>
                            <input name="data[Note][contactPerson]" class="typeahead form-control" type="text" placeholder="Type name">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-6">
                            <label>Follow up date:</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                                <input id="datepicker" name="data[Note][note_date]" type="text" class="form-control" placeholder="Follow up date">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            </div>
                            <?php echo $this->Form->error('Note.note_date');?>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-6">
                            <div class="input-group">
                                <label>Rating:</label>
                                <select class="noteRatings" name="data[Note][rating]">
                                    <option value="" selected="selected"></option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                </select>
                            </div>
                            <?php echo $this->Form->error('Note.note_date');?>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label>Note:</label>
                            <?php
                            echo $this->Form->input(
                                'Note.note',
                                array(
                                    'type' => 'textarea',
                                    'class' => 'form-control',
                                    'placeholder' => 'Place your note here',
                                    'rows' => 5,
                                    'label' => false,
                                    'required' => false,
                                )
                            );
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-4">
                            <button class="btn btn-theme">Save Note</button>
                        </div>
                    </div>
                    <?php echo $this->Form->end(); ?>
                    <div class="clearfix"></div>
                </div>
                <!----- /ADD NEW CONTACT NOTE ------>
            </div>
        </div>
    </div>
</div>
<!-- /Modal For Displaying Schedule -->



<!-- Modal For Displaying Schedule -->
<div class="modal event-modal fade" id="searchNote" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="border-bottom: 1px solid #e5e5e5;">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                </button>
                <h4 class="sm-title">Search Note</h4>
            </div>
            <div class="modal-body">
                <!----- ADD NEW CONTACT NOTE ------>
                <div class="">
                    <?php echo $this->Form->create(
                        'Contact',
                        array('controller' => 'contacts', 'action' => 'note_list', 'type' => 'GET', 'class' => 'form-horizontal form-custom')
                    ); ?>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label>Choose One</label>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="noteFor" class="noteForStudent" value="student" checked="">
                                    Student
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="noteFor" class="noteForContact" value="contact">
                                    Lead
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label>Name</label>
                            <input name="name" class="typeahead form-control" type="text" placeholder="Type name">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-lg-12"><label>Date Range</label></div>
                        <div class="clearfix"></div>
                        <div class="col-sm-6">
                            <div class="input-group">
                                <input id="datepicker3" name="starting_date" type="text" class="form-control" placeholder="Starting">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="input-group">
                                <input id="datepicker4" name="ending_date" type="text" class="form-control" placeholder="Ending">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-6">
                            <div class="input-group">
                                <label>Rating:</label>
                                <select class="noteRatings" name="rating">
                                    <option value="" selected="selected"></option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                </select>
                            </div>
                            <?php echo $this->Form->error('Note.note_date');?>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-12">
                            <label>Note Status:</label>
                            <select name="status" class="form-control">
                                <option>Choose Status</option>
                                <option value="upcoming">Upcoming</option>
                                <option value="followed">Followed</option>
                                <option value="unfollowed">Unfollowed</option>
                                <option value="expired">Expired</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-12">
                            <label>Note:</label>
                            <?php
                            echo $this->Form->input(
                                'note',
                                array(
                                    'type' => 'text',
                                    'class' => 'form-control',
                                    'placeholder' => '%note%',
                                    'label' => false,
                                    'required' => false,
                                )
                            );
                            ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-4">
                            <button class="btn btn-theme">Search</button>
                        </div>
                    </div>
                    <?php echo $this->Form->end(); ?>
                    <div class="clearfix"></div>
                </div>
                <!----- /ADD NEW CONTACT NOTE ------>
            </div>
        </div>
    </div>
</div>
<!-- /Modal For Displaying Schedule -->

<?php echo $this->Html->script('typehead');?>
<script>
    // Instantiate the Bloodhound suggestion engine
    var contacts = new Bloodhound({
        datumTokenizer: function (datum) {
            return Bloodhound.tokenizers.whitespace(datum.value);
        },
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: {
            url: '<?php echo Router::url('/', true);?>instructor/contacts/getContacts',
            replace: function (url, query) {

                if($('.noteForStudent').is(':checked')){
                    q = url+'/student/'+query;
                }

                if($('.noteForContact').is(':checked')){
                    q = url+'/contact/'+query;
                }
                return q;
            },
            filter: function (contacts) {
                return $.map(contacts, function (contact) {
                    console.log(contact);
                    return {
                        value: contact['0']['contactName']
                    };
                });
            }
        }
    });

    // Initialize the Bloodhound suggestion engine
    contacts.initialize();
    // Instantiate the Typeahead UI
    $('.typeahead').typeahead(null, {
        name: 'states',
        displayKey: 'value',
        source: contacts.ttAdapter()
    });


    $('.noteRatings').barrating('show', {
        theme: 'fontawesome-stars',
        initialRating: null
    });
</script>



<script>
    $(document).ready(function () {
        $('#datepicker').datepicker()
    });
</script>