<?php
/**
 * @var $this View
 */
echo $this->Html->css('kit/style-217.css')
?>
<style>
    .ui-217 .ui-item {
        max-width: 100%;
        margin: 10px auto 40px auto;
        position: relative;
    }

    .bg-lblue {
        background-color: #6f8e40 !important;
    }

    .ui-217 .ui-item .ui-details {
        border-bottom: 2px solid #6f8e40;
    }

    .ui-217 .ui-item .ui-details:before, .ui-217 .ui-item .ui-details:after {
        border-top-color: #6f8e40;
    }
</style>

<h2 class="page-title">Lead Details</h2>
<hr class="shadow-line"/>
<div class="row">
    <div class="col-lg-4 col-md-4 col-sm-12">
        <div class="d-box">
            <h4 class="d-title"><i class="fa fa-info-circle yellowgreen"></i> Contact Details</h4>
            <ul class="d-list">
                <li>
                    <strong>Contact Name:</strong>
                    <?php
                    echo $this->Html->link($contact['Contact']['first_name']. ' '. $contact['Contact']['last_name'], array('controller' => 'contacts', 'action' => 'view', $contact['Contact']['id']));
                    ?>
                </li>
                <li>
                    <strong>Email:</strong>
                    <?php
                    echo $contact['Contact']['email'];
                    ?>
                </li>
                <li>
                    <strong>Phone:</strong>
                    <?php
                    echo $contact['Contact']['phone']
                    ?>
                </li>
                <li>
                    <strong>Account Type:</strong>
                    <?php
                    if($contact['Contact']['account_type'] == 1){
                        echo '<label class="label label-info">student</label>';
                    }
                    elseif($contact['Contact']['account_type'] == 2){
                        echo '<label class="label label-warning">customer</label>';
                    }
                    elseif($contact['Contact']['account_type'] == 3){
                        echo '<label class="label label-success">lead/prospect</label>';
                    }
                    else{
                        echo '<label class="label label-default">N/A</label>';
                    }
                    ?>
                </li>

                <li>
                    <strong>Follow Date:</strong>
                    <?php
                    echo $contact['Contact']['follow_up_date'];
                    ?>
                </li>

                <li>
                    <strong>Source:</strong>
                    <?php
                    echo $contact['Contact']['source'];
                    ?>
                </li>
                </li>

                <li>
                    <strong>Comment:</strong>
                    <?php
                    echo $contact['Contact']['comments'];
                    ?>
                </li>

                <li>
                    <strong>Rating:</strong>
                    <select class="singleLeadRating">
                        <option value="" selected="selected"></option>
                        <?php
                        for ($counter = 1; $counter <= 5; $counter++) {
                            $select = '';
                            if($counter == $contact['Contact']['rating']){
                                $select = 'selected';
                            }
                            echo '<option '.$select.' value="'.$counter.'">'.$contact['Contact']['id'].'</option>';
                        }
                        ?>
                    </select>
                </li>
            </ul>
        </div>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-12"></div>
    <div class="col-lg-4 col-md-4 col-sm-12">
        <div class="d-box">
            <h4 class="d-title"><i class="fa fa-file-text danger"></i> Quick Note</h4>
            <br/>
            <br/>
            <?php echo $this->Form->create(
                'Contact',
                array('controller' => 'contacts', 'action' => 'view/'.$contact['Contact']['id'], 'class' => 'form-horizontal form-custom')
            ); ?>
            <div class="form-group">
                <div class="col-sm-8">
                    <label>Follow up date:</label>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                        <input id="datepicker" name="data[Note][note_date]" type="text" class="form-control" placeholder="Follow up date">
                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                    </div>
                    <?php echo $this->Form->error('Note.note_date');?>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-6">
                    <div class="input-group">
                        <label>Rating:</label>
                        <select class="takeNoteRating" name="data[Note][rating]">
                            <option value="" selected="selected"></option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                        </select>
                    </div>
                    <?php echo $this->Form->error('Note.note_date');?>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12">
                    <label>Note:</label>
                    <?php
                    echo $this->Form->input(
                        'Note.note',
                        array(
                            'type' => 'textarea',
                            'class' => 'form-control',
                            'placeholder' => 'Place your note here',
                            'label' => false,
                            'required' => false,
                        )
                    );
                    ?>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-4">
                    <button class="btn btn-theme">Save Note</button>
                </div>
            </div>
            <?php echo $this->Form->end(); ?>
        </div>
    </div>

    <div class="col-lg-12">
        <?php if($notes):?>
            <br/>
            <br/>
            <h2 class="page-title">Note</h2>
            <hr class="shadow-line"/>
            <div class="ui-217" >
                <?php foreach($notes as $note):?>
                    <div class="ui-item clearfix" id="<?php echo $note['Note']['id'];?>">
                        <!-- Quotes -->
                        <span class="bg-lblue">&#8220;</span>
                        <!-- Details -->
                        <div class="ui-details">
                            <em>
                                <a name="<?php echo $note['Note']['id'];?>"><?php echo $note['Note']['note'];?></a>
                            </em>
                            <br/>
                            <select class="noteRatings">
                                <option value="" selected="selected"></option>
                                <?php
                                for ($counter = 1; $counter <= 5; $counter++) {
                                    $select = '';
                                    if($counter == $note['Note']['rating']){
                                        $select = 'selected';
                                    }
                                    echo '<option '.$select.' value="'.$counter.'">'.$note['Note']['id'].'</option>';
                                }
                                ?>
                            </select>
                            <p>
                                <?php
                                $today = (array) new DateTime("now");
                                $noteDate = $note['Note']['note_date'];
                                if($note['Note']['status'] == 2){
                                    echo '<i class="label label-success">followed</i>';
                                }
                                if($note['Note']['status'] == 3){
                                    echo '<i class="label label-warning">unfollowed</i>';
                                }
                                elseif(strtotime($today['date']) < strtotime($noteDate) && $note['Note']['status'] == 1){
                                    echo '<i class="label label-info">upcoming</i>';
                                }
                                elseif(strtotime($today['date']) > strtotime($noteDate) && $note['Note']['status'] == 1){
                                    echo '<i class="label label-danger">expired</i>';
                                }
                                ?>
                            </p>
                            <strong style="font-size: 10px;">
                                <em>
                                    <?php echo $this->Time->format('d M, Y', $note['Note']['created'])?>
                                </em>
                            </strong>
                            <i class="italic-sm">(<?php echo $this->Time->format('h:i A', $note['Note']['created'])?>)</i>
                        </div>
                        <?php
                        if($userInfo['Profile']['profile_pic_url']){
                            echo $this->Html->image('profiles/'.$userInfo['Profile']['profile_pic_url'], array('class' => 'img-circle pro-img-sm ui-img'));
                        }
                        else{
                            echo $this->Html->image('avatar.jpg', array('class' => 'img-circle pro-img-sm'));
                        }
                        ?>
                        <h3>
                            <?php echo $this->Html->link($userInfo['Profile']['name'], array('controller' => 'profiles', 'action' => 'index'));?>
                        </h3>
                    </div>

                <?php endforeach;?>
            </div>
        <?php else:?>
            <?php echo $this->element('not-found')?>
        <?php endif;?>
    </div>
</div>


<script>
    $(document).ready(function () {
        $('#datepicker').datepicker()

        var fullUrl = window.location.href.split('#');
        var noteID = fullUrl[1];
        $('#'+noteID).addClass('current-note');
    });

    $('.takeNoteRating').barrating('show', {
        theme: 'fontawesome-stars'
    });

    $('.singleLeadRating').barrating('show', {
        initialRating: 0,
        theme: 'fontawesome-stars',
        onSelect:function(value, text){
            $.ajax({
                url: '<?php echo Router::url('/', true);?>instructor/contacts/set_rating',
                type: "POST",
                dataType: "json",
                data:  {contactID: text, rating: value},
                success: function(response) {
                    if(response == 1){
                        manageFlashMessage('alert-success', 'Your rating has been successfully complete');
                    }
                    else{
                        manageFlashMessage('alert-danger', 'Sorry, something went wrong');
                    }
                },
                error: function (xhr, status, error) {
                    console.log(error);
                }
            });

        }
    });

    $('.noteRatings').barrating('show', {
        initialRating: 0,
        theme: 'fontawesome-stars',
        onSelect:function(value, text){

            $.ajax({
                url: '<?php echo Router::url('/', true);?>instructor/contacts/setRating',
                type: "POST",
                dataType: "json",
                data:  {noteID: text, rating: value},
                success: function(response) {

                    console.log(response);

                    if(response == 1){
                        manageFlashMessage('alert-success', 'Your rating has been successfully complete');
                    }
                    else{
                        manageFlashMessage('alert-danger', 'Sorry, something went wrong');
                    }
                },
                error: function (xhr, status, error) {
                    console.log(error);
                }
            });

        }
    });
</script>