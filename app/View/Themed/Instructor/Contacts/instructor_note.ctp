<?php
/**
 * @var $this View
 */
?>
<h2 class="page-title">Take Note</h2>
<hr class="shadow-line"/>
<div class="row">
    <div class="col-md-8 col-md-offset-2 col-sm-12">
        <div class="d-box">
            <h4 class="d-title padding-bottom-10"><i class="fa fa-file-text danger"></i> Follow Up Note</h4>
            <br/>
            <div class="">
                <?php echo $this->Form->create(
                    'Contact',
                    array('controller' => 'contacts', 'action' => 'note', 'class' => 'form-horizontal form-custom')
                ); ?>
                <div class="form-group">
                    <div class="col-sm-12">
                        <label>Choose One</label>
                        <div class="radio">
                            <label>
                                <input type="radio" name="noteFor" id="noteForStudent" value="student" checked="">
                                Student
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                                <input type="radio" name="noteFor" id="noteForContact" value="contact">
                                Lead
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-12">
                        <label>Contact Name</label>
                        <input name="data[Note][contactPerson]" class="typeahead form-control" type="text" placeholder="Type name">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-6">
                        <label>Follow up date:</label>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                            <input id="datepicker" name="data[Note][note_date]" type="text" class="form-control" placeholder="Follow up date">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                        </div>
                        <?php echo $this->Form->error('Note.note_date');?>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-6">
                        <div class="input-group">
                            <label>Rating:</label>
                            <select class="noteRatings" name="data[Note][rating]">
                                <option value="" selected="selected"></option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                            </select>
                        </div>
                        <?php echo $this->Form->error('Note.note_date');?>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-12">
                        <label>Note:</label>
                        <?php
                        echo $this->Form->input(
                            'Note.note',
                            array(
                                'type' => 'textarea',
                                'class' => 'form-control',
                                'placeholder' => 'Place your note here',
                                'rows' => 5,
                                'label' => false,
                                'required' => false,
                            )
                        );
                        ?>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-4">
                        <button class="btn btn-theme">Save Note</button>
                    </div>
                </div>
                <?php echo $this->Form->end(); ?>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>

<?php echo $this->Html->script('typehead');?>
<script>
    // Instantiate the Bloodhound suggestion engine
    var contacts = new Bloodhound({
        datumTokenizer: function (datum) {
            return Bloodhound.tokenizers.whitespace(datum.value);
        },
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: {
            url: '<?php echo Router::url('/', true);?>instructor/contacts/getContacts',
            replace: function (url, query) {

                if($('#noteForStudent').is(':checked')){
                    q = url+'/student/'+query;
                }

                if($('#noteForContact').is(':checked')){
                    q = url+'/contact/'+query;
                }
                return q;
            },
            filter: function (contacts) {
                return $.map(contacts, function (contact) {
                    console.log(contact);
                    return {
                        value: contact['0']['contactName']
                    };
                });
            }
        }
    });

    // Initialize the Bloodhound suggestion engine
    contacts.initialize();
    // Instantiate the Typeahead UI
    $('.typeahead').typeahead(null, {
        name: 'states',
        displayKey: 'value',
        source: contacts.ttAdapter()
    });


    $('.noteRatings').barrating('show', {
        theme: 'fontawesome-stars',
        initialRating: null
    });
</script>