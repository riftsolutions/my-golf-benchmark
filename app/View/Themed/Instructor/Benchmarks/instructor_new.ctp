<?php
/**
 * @var $this View
 */
?>
<h2 class="page-title">Benchmark</h2>
<hr class="shadow-line"/>

<?php echo $this->Form->create('Benchmark', array('controller' => 'benchmarks', 'action' => 'new'));?>

<div class="row">
    <div class="col-xs-6">
        <?php
        echo $this->Form->input(
            'start_date',
            array(
                'type' => 'date',
                'class' => 'form-control',
                'label' => 'Start Date',
                'required' => true,
                'style' => 'width: auto; display: inline-block;'
            )
        );
        ?>
    </div>
    <div class="col-xs-6">
        <?php
        echo $this->Form->input(
            'starting_handicap',
            array(
                'type' => 'number',
                'class' => 'form-control',
                'label' => 'Starting Handicap',
                'required' => true,
                'style' => 'width: auto;'
            )
        );
        ?>
        <?php
        echo $this->Form->input(
            'current_handicap',
            array(
                'type' => 'number',
                'class' => 'form-control',
                'label' => 'Current Handicap',
                'required' => true,
                'style' => 'width: auto;'
            )
        );
        ?>
        <label>Change in Handicap</label>
        <p id="change_handicap">0</p>
    </div>
</div>
<div class="row">
    <div class="col-xs-12">
        <?php
        echo $this->Form->input(
            'previous_injuries',
            array(
                'type' => 'textarea',
                'class' => 'form-control',
                'label' => 'Any previous injuries and / or is anything bothering you from playing currently?',
                'required' => false
            )
        );
        ?>
        <?php
        echo $this->Form->input(
            'week_count',
            array(
                'type' => 'number',
                'class' => 'form-control',
                'label' => 'How frequently can you come in? (# of weeks between sessions)',
                'required' => true,
                'style' => 'width: auto;',
                'value' => 1
            )
        );
        ?>
        <?php
        echo $this->Form->input(
            'improvement_amount',
            array(
                'class' => 'form-control',
                'label' => 'How much improvement do you desire?',
                'style' => 'width: auto;',
                'empty' => false,
                'options' => array(
                    '0' => 'Minimal',
                    '1' => 'Moderate',
                    '2' => 'Maximum'
                )
            )
        );
        ?>
    </div>
</div>

<div class="row">
    <iframe src="/theme/Instructor/3D/index.html" style="border: none; width: 100%; min-width: 900px; height: 1200px;" scrolling=no></iframe>
</div>

<div class="row">
    <div class="well">
        <h1>Recommendation:</h1>
        <h2><span id="session-count">10</span> lessons</h2>
    </div>
</div>

<button class="btn btn-theme">Submit Benchmark</button>

<?php echo $this->Form->end();?>

<script>
    $(document).ready(function() {

    });
</script>
