<?php
/**
 * @var $this View
 */
?>
    <h2 class="page-title">Lead's Trash</h2>
    <hr class="shadow-line"/>

    <div class="clearfix"></div>
<?php  if($contactList):?>
    <div class="table-responsive">
        <table class="table data-table table-bordered sort-table" colspecing=0>
            <thead>
            <tr>
                <th style="width: 15%;">
                    <?php echo $this->Paginator->sort('first_name', 'Name'); ?>
                </th>
                <th style="width: 15%;">
                    <?php echo $this->Paginator->sort('account_type'); ?>
                </th>
                <th style="width: 15%;">
                    <?php echo $this->Paginator->sort('email'); ?>
                </th>
                <th style="width: 13%;">
                    <?php echo $this->Paginator->sort('phone'); ?>
                </th>
                <th style="width: 8%;">
                    <?php echo $this->Paginator->sort('status'); ?>
                </th>
                <th style="width: 14%;">
                    <?php echo $this->Paginator->sort('member_type'); ?>
                </th>
                <th style="width: 11%;">
                    <?php echo $this->Paginator->sort('member_created', 'Member Since'); ?>
                </th>
                <th style="width: 9%;">Action</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($contactList as $contact):?>
                <tr>
                    <td>
                        <?php echo $this->Html->link($contact['Contact']['first_name']. ' '. $contact['Contact']['last_name'], array('controller' => 'contacts', 'action' => 'edit', $contact['Contact']['id']));?>
                    </td>
                    <td>
                        <?php
                        if($contact['Contact']['account_type'] == 1){
                            echo 'Student';
                        }
                        elseif($contact['Contact']['account_type'] == 2){
                            echo 'Customer';
                        }
                        elseif($contact['Contact']['account_type'] == 3){
                            echo 'Lead/Prospect';
                        }
                        else{
                            echo 'N/A';
                        }
                        ?>
                    </td>

                    <td><?php echo $contact['Contact']['email']; ?></td>
                    <td><?php echo $contact['Contact']['phone']?></td>
                    <td>
                        <?php if ($contact['Contact']['status'] == 1): ?>
                            <span class="status"><i class="active fa fa-search-plus"></i></span>
                            <small>(Active)</small>
                        <?php elseif ($contact['Contact']['status'] == 9): ?>
                            <span class="status"><i class="active fa fa-trash danger"></i></span>
                            <small>(Deleted)</small>
                        <?php endif; ?>
                    </td>
                    <td>
                        <?php if ($contact['Contact']['member_type'] == 1): ?>
                            <label class="label label-info">new member</label>
                        <?php elseif ($contact['Contact']['member_type'] == 2): ?>
                            <label class="label label-success">already member</label>
                        <?php elseif($contact['Contact']['member_type'] == 3): ?>
                            <label class="label label-danger">not member</label>
                        <?php endif; ?>
                    </td>
                    <td>
                        <time><?php echo $this->Time->format('M d, Y', $contact['Contact']['created']); ?></time>
                    </td>
                    <td>
                            <?php
                            echo $this->Form->postLink(
                                'Restore <i class="fa fa-mail-reply"></i>',
                                array(
                                    'controller' => 'contacts',
                                    'action' => 'restore',
                                    $contact['Contact']['id']
                                ),
                                array(
                                    'class' => 'icon green',
                                    'escape' => false
                                ),
                                __('Are you sure you want to restore this lead?')
                            );
                            ?>
                        </div>
                    </td>
                </tr>
            <?php endforeach;?>
            </tbody>
        </table>
        <?php echo $this->element('pagination');?>
    </div>
<?php else:?>
    <h2 class="not-found">Sorry, lead's trash list empty</h2>
<?php endif;?>