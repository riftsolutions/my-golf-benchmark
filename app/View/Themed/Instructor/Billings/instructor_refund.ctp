<?php
/**
 * @var $this View
 */
?>
<h2 class="page-title">Refund Money</h2>
<hr class="shadow-line"/>
<div class="row">
    <div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-12">

        <form class="searchTransactionForm">
            <input id="transactionID" placeholder="Write Transaction no" />
            <a type="submit" class="" id="transactionIDBtn">Show Details</a>
        </form>

        <div class="clearfix"></div>

        <div class="d-box refundBox margin-top-20" style="display: none;">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <?php echo $this->Form->create('Billing', array('controller' => 'billings', 'action' => 'refundPayment'))?>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Refund Amount</label>
                            <div class="form-group">
                                <label class="sr-only" for="exampleInputAmount">Amount (in dollars)</label>
                                <div class="input-group">
                                    <div class="input-group-addon">$</div>
                                    <input name="amount" type="text" class="form-control" id="exampleInputAmount" placeholder="Amount">
                                    <input name="transId" type="hidden" id="transId">
                                    <div class="input-group-addon">.00</div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Comments</label>
                            <textarea id="commentArea" name="comments" class="form-control" rows="5" placeholder="Comments..."></textarea>
                        </div>
                        <button type="submit" class="btn btn-theme refundBtn">Refund</button>
                        <p class="note NB"></p>
                    </form>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="d-box">
                        <h4 class="d-title"><i class="fa fa-money yellowgreen"></i> Transaction Details</h4>
                        <ul class="d-list">
                            <li> <strong>Transaction ID: </strong> <span class="transId"></span></li>
                            <li> <strong>Transaction Amount: </strong> <span class="settleAmount"></span></li>
                            <li> <strong>Transaction Date: </strong> <span class="submitTimeUTC"></span></li>
                            <li> <strong>Transaction Status: </strong> <span class="transactionStatus"></span></li>
                            <li> <strong>Card Number: </strong> <span class="payment_creditCard_cardNumber"></span></li>
                            <li> <strong>Card Type: </strong> <span class="payment_creditCard_cardType"></span></li>
                            <li> <strong>Client Name: </strong> <span class="customer_name"></span></li>
                            <li> <strong>Client Email: </strong> <span class="customer_email"></span></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php echo $this->Html->script('jquery.block.ui')?>
<script>
    $(document).ajaxStop($.unblockUI);

    $('#transactionIDBtn').click(function(){
        var transactionID = $('#transactionID').val();
        $.blockUI(
            {
                message: '<h1><img src="<?php echo Router::url('/', true);?>theme/Instructor/img/busy.gif" /> ' + 'Just a moment...</h1>'
            }
        );

        console.log(transactionID);

        $.ajax({
            url: '<?php echo Router::url('/', true);?>/instructor/billings/getTransactionDetails',
            type: "POST",
            dataType: "json",
            data: {'transactionID': transactionID},
            success: function (data) {
                if(data != 'error'){
                    var transactionStatus;
                    if(data['transactionStatus'] == 'settledSuccessfully'){
                        transactionStatus = '<label class="label label-success">Settled Successfully</label>'

                        $('.refundBtn').prop('disabled', false);
                        $('#exampleInputAmount').prop('disabled', false);
                        $('#commentArea').prop('disabled', false);
                        $('.NB').text('');

                    }
                    else if(data['transactionStatus'] == 'capturedPendingSettlement'){
                        transactionStatus = '<label class="label label-warning">Settlement Pending</label>'
                            $('.refundBtn').prop('disabled', true);
                            $('#exampleInputAmount').prop('disabled', true);
                            $('#commentArea').prop('disabled', true);
                            $('.NB').text('N.B: This is transaction is still in pending settlement, you can not refund for this transaction until settlement will success');
                    }



                    console.log(data);

                    $('.searchTransactionForm').css({"margin-top": "10px"});
                    $('.searchTransactionForm').addClass('animated slideInUp');
                    $('.searchTransactionForm input').removeClass('red-form');
                    $('.searchTransactionForm input').addClass('green-form');

                    $('.refundBox').show();
                    $('.refundBox').addClass('animated slideInUp');

                    $('#transId').val(data['transId']);
                    $('.transId').text(data['transId']);
                    $('.settleAmount').text(data['settleAmount']);
                    $('.submitTimeUTC').text(data['submitTimeUTC']);
                    $('.transactionStatus').html(transactionStatus);
                    $('.payment_creditCard_cardNumber').text(data['payment_creditCard_cardNumber']);
                    $('.payment_creditCard_cardType').text(data['payment_creditCard_cardType']);
                    $('.customer_name').text(data['shipTo_firstName']+ ' ' +data['shipTo_lastName']);
                    $('.customer_email').text(data['customer_email']);
                }
                else{
                    $('.refundBox').hide();
                    $('.searchTransactionForm input').removeClass('green-form');
                    $('.searchTransactionForm input').addClass('red-form');
                    manageFlashMessage('alert-danger', 'Sorry, invalid transaction ID');
                }

            },
            error: function (xhr, status, error) {
                $('.searchTransactionForm input').removeClass('green-form');
                $('.searchTransactionForm input').addClass('red-form');
                manageFlashMessage('alert-danger', 'Sorry, something went wrong');
            }
        });

    });
</script>