<?php
/**
 * @var $this View
 */
?>


<div id="studentID" style="display: none;">
    <?php $studentID = $this->Session->read('studentID'); echo $studentID; ?>
</div>


<h2 class="page-title">Payment</h2>
<hr class="shadow-line"/>
<div class="row">
    <div class="col-lg-8 col-md-8">
        <div class="d-box">
            <h4 class="d-title"><i class="fa fa-search-plus green"></i> Review</h4>
            <table class="table margin-top-25">
                <thead>
                <tr>
                    <th>Instructor</th>
                    <th>Title</th>
                    <th><span class="pull-right">Sub total</span></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($packages as $package):?>
                <tr>
                    <td class="padding-left-0 padding-right-0">
                        <?php echo $userInfo['Profile']['name']?>
                    </td>
                    <td class="padding-left-0 padding-right-0">
                        <?php echo $this->Html->link($package['Package']['name'], array('controller' => 'package', 'action' => 'view', $package['Package']['uuid']))?>
                    </td>
                    <td class="padding-left-0 padding-right-0">
                        <span class="pull-right"><?php echo $this->Number->currency($package['Package']['price']);?></span>
                    </td>
                </tr>
                <?php endforeach;?>
                </tbody>
            </table>
            <div class="subtoal">
                <ul class="data-list">
                    <li>
                        <strong>Sub Total:</strong>
                        <?php echo $this->Number->currency($cost['subtotal']);?>
                    </li>
                    <li>
                        <strong>Tax:</strong>
                        <?php echo $this->Number->currency($cost['tax']);?>
                    </li>
                    <li>
                        <strong>Total</strong>
                        <?php echo $this->Number->currency($cost['total']);?>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    
    <div class="col-lg-4 col-md-4">
        <div class="d-box">
        <?php if ($this->Session->read('Auth.User.stripe_account_id')) { ?>
            <h4 class="d-title padding-bottom-10">
                <i class="fa fa-money green"></i> Payment
            </h4>
            <hr class="shadow-list"/>
            <?php echo $this->Form->create('Billing', array('controller' => 'billings', 'action' => 'payment'));?>
            <div class="form-group">
                <label class="control-label">Select Student</label>
                <?php
                echo $this->Form->input(
                    'user_id',
                    array(
                        'type' => 'select',
                        'class' => 'form-control',
                        'label' => false,
                        'div' => false,
                        'required' => true,
                        'options' => $students,
                        'empty' => 'Select Student',
                        'value' => $studentID,
                        'id' => 'student-select'
                    )
                )
                ?>
            </div>
            <div class="form-group">

                <label class="control-label">Select Card</label>
                <?php
                echo $this->Form->input(
                    'card_id',
                    array(
                        'type' => 'select',
                        'class' => 'form-control',
                        'label' => false,
                        'div' => false,
                        'required' => true,
                        'empty' => 'Select Card',
                        'id' => 'card-select'
                    )
                )
                ?>
                
                <br/>
                <div class="form-group btn-area">
                    <button class="btn btn-theme">Submit Payment</button>
                </div>
            </div>
            <?php echo $this->Form->end();?>
            
        <?php } else { ?>
            <h3>Before you can charge users, you must <?php echo $this->Html->link("link a Stripe account", array('controller' => 'profiles', 'action' => 'index')); ?></h3>
        <?php } ?>
        </div>
    </div>
</div>


<script>
    
    $(document).ready(function(){
        
        var changed = function() {
            var studentID = $('#student-select').val();
            
            $('#card-select').attr('disabled', !studentID);
            $('#card-select').find('option:not(:first)').remove();
            if (studentID) {
                $.ajax({
                    url: '<?php echo Router::url('/', true);?>student/cards/getStudentCards?studentID='+studentID,
                    type: 'GET',
                    dataType: 'json',
                    success: function(data) {
                        console.log(data);
                        
                        for (var i = 0; i < data.length; i++) {
                            var card = data[i];
                            var option = $('<option>');
                            
                            option.attr('value', card.id);
                            option.html('xxxx xxxx xxxx '+card.last4);
                            
                            $('#card-select').append(option);
                        }
                    }
                });
            }
        }
        
        $('#student-select').change(changed);
        changed();
        
        $('#card-select').change(function() {
            var value = $('#card-select').val();
            
            
        })
    });

</script>