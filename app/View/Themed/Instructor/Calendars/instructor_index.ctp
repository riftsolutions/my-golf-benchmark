<?php
/**
 * @var $this View
 */
?>
<style>
    #calendar table tbody td:first-child {
        border-bottom: 3px solid #DDD !important;
        background-color: #fff;
        border-right: 3px solid #DDD !important;
    }
    .fc-slats td {
        height: 75px !important;
        border-bottom: 0;
        color: #d9edf7;
        background-color: #EEE !important;
        border: 1px solid #EEE !important;
    }

    .fc-slats .fc-minor .fc-time {
        border-right: 3px solid #DDD !important;
    }

    .fc-ltr .fc-time-grid .fc-event-container {
        margin: 0px !important;
    }

    .fc-slats .fc-minor td {
        border-style: none !important;
    }

    .fc-time-grid-event > .fc-content {
        position: relative;
        z-index: 2;
        margin: 10px !important;
    }

    .fc-time-grid-event > .fc-content {
        position: relative;
        z-index: 2;
        margin: 10px !important;
    }

    .fc-unthemed .fc-today {
        background: none !important;
    }

    .fc-time-grid-event > .fc-content {
        margin: 5px !important;
    }

    .fc-slats .fc-minor td {
        border: 0px !important;

    }

    .fc-slats .fc-minor .fc-time{
        border-bottom: 3px solid #DDD !important;
    }
</style>
<h2 class="page-title margin-top-20">My Calendar</h2>
<hr class="shadow-line"/>
<span id="userID" style="display: none;"><?php echo $userID; ?></span>
<div class="control-panel">
    <div class="element">
        <select class="form-control" id="selectInstructor">
            <option>Select Instructor</option>
            <?php foreach($instructors as $instructor):?>
                <option value="<?php echo $instructor['User']['id']; ?>">
                    <?php echo $instructor['Profile']['name']; ?>
                </option>
            <?php endforeach;?>
        </select>
    </div>
    <div class="element">
        <a class="btn btn-theme text-uppercase" data-toggle="modal" data-target="#calendarSettings">My Schedule</a>
        <!--<a class="btn btn-theme text-uppercase" onclick="createGroupLesson()">Create Group Lesson</a>
        <a class="btn btn-theme text-uppercase" data-toggle="modal" data-target="#blockTimeModal">Block Time</a>-->
        <a class="btn btn-theme text-uppercase" data-toggle="modal" data-target="#waitingListModal2"> Join Waitlist</a>

    </div>
</div>


<div class="right_button_option">
    <a class="btn right_click_btn btn-theme" onclick="clickRightOptionsBtn(1)">Set Appointment</a>
    <a class="btn right_click_btn btn-black" onclick="clickRightOptionsBtn(2)">Group Lesson</a>
    <a class="btn right_click_btn btn-danger" onclick="clickRightOptionsBtn(3)">Block Time</a>
    <a class="not_now pull-right">Not now?</a>
    <div class="clearfix"></div>
</div>

<div id="calendar"></div>

<!---- Displaying Schedule ----->
<script>
$(document).ajaxStop($.unblockUI);
var LoggedInInstructor = parseInt($('#userID').html());
var defaultSource = '<?php echo Router::url('/', true);?>calendars/getAllEventsOfInstructor/' + LoggedInInstructor;

$(document).ready(function() {
    var calendar = $('#calendar').fullCalendar({
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
        },
        ignoreTimezone: true,
        defaultView: 'agendaWeek',
        allDayDefault: false,
        allDaySlot: false,
        slotEventOverlap: true,
        slotDuration: "00:15:00",
        contentHeight: 700,
        scrollTime :  "7:00:00",
        axisFormat: 'HH:mm a',
        timeFormat: 'HH:mm A()',
        eventDataTransform: function (data) {
            return data;
        },
        select: function(start, end, allDay) {

        },
        eventRender: function(event, element) {
            if(event.type == 'event'){
                availabilityEventView(event, element);
            }
            else if(event.type == 'group_lesson'){
                groupLessonEventView(event, element);
            }
            else if(event.type == 'block_time'){
                blockTimeEventView(event, element);
            }
            else if(event.type == 'appointment'){
                appointmentEventView(event, element);
                appointmentEventPopover(event, element);
            }

            $('body').on('click', function (e) {
                if (!element.is(e.target) && element.has(e.target).length === 0 && $('.popover').has(e.target).length === 0)
                    element.popover('hide');
            });
        },
        dayClick: function(date, allDay, jsEvent, view, e) {
            if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
                $('.right_button_option').show();
                closeModalPopover();
                $('.right_button_option').css('left', '0px');
                $('.right_button_option').css('bottom', '200px');
            }
            else{

                var dateTime = $.fullCalendar.moment(date._d).format('dddd, MMMM DD YYYY');
                $('#setAppointmentDate').text(dateTime);
                $('.setAppointmentDate').val(dateTime);

                var left = 50;
                var theHeight = $('.right_button_option').height();
                var windowHeight = $( window ).height();
                var top = (windowHeight - theHeight) / 2;

                $('.right_button_option').css('left', 40 + '%');
                $('.right_button_option').css('top', 40 + '%');
                $('.right_button_option').show();
                closeModalPopover();
            }
        },
        eventClick: function (data, allDay, jsEvent, view) {

            if(data.type == 'event'){
                //setAppointment(parseInt(data.id));

                var dateTime = $.fullCalendar.moment(data.start._i).format('dddd, MMMM DD YYYY');
                $('#setAppointmentDate').text(dateTime);
                $('.setAppointmentDate').val(dateTime);

                var left = 50;
                var theHeight = $('.right_button_option').height();
                var windowHeight = $( window ).height();
                var top = (windowHeight - theHeight) / 2;

                $('.right_button_option').css('left', 40 + '%');
                $('.right_button_option').css('top', 40 + '%');
                $('.right_button_option').show();
                closeModalPopover();

            }

            if(data.type == 'appointment'){
                manageAppointmentView(data);
            }
        }
    });

    //$('#calendar').fullCalendar('addEventSource', defaultSource);
});

<!----- Displaying different instructor ----->
$(document).ready(function() {
    $(document).ajaxStart($.blockUI(
        {
            message: '<h1><img src="<?php echo Router::url('/', true);?>theme/Instructor/img/busy.gif" /> ' + 'Just a moment...</h1>'
        }
    )).ajaxStop($.unblockUI);
    $('#calendar').fullCalendar('addEventSource', defaultSource);
    var currentInstructorID = LoggedInInstructor;


    $('#selectInstructor').change(function(){
        $(document).ajaxStart($.blockUI(
            {
                message: '<h1><img src="<?php echo Router::url('/', true);?>theme/Instructor/img/busy.gif" /> ' + 'Just a moment...</h1>'
            }
        )).ajaxStop($.unblockUI);

        newInstructorID = parseInt($('#selectInstructor').val());
        if(newInstructorID != '')
        {
            $('#calendar').fullCalendar('removeEventSource', defaultSource);
            $('#userID').html(newInstructorID);
            var removeEventSources = '<?php echo Router::url('/', true);?>calendars/getAllEventsOfInstructor/' + currentInstructorID;
            var AddEventSources = '<?php echo Router::url('/', true);?>calendars/getAllEventsOfInstructor/' + newInstructorID;
            $('#calendar').fullCalendar('addEventSource', AddEventSources);
        }

        $('#calendar').fullCalendar('refetchEvents');

    });
});
<!----- /Displaying different instructor ----->



<!---- Displaying Schedule ----->

function manageAppointmentView(appointmentDetails)
{
    if(appointmentDetails.appointment_status == 3){
        $.ajax({
            url: '<?php echo Router::url('/', true);?>/calendars/getStudentForReassignAppointments',
            type: "POST",
            dataType: "json",
            data:  {studentID: appointmentDetails.student_id},
            success: function(students) {

                var reassignTo = '<br/><form action="<?php echo Router::url('/', true);?>calendars/reassign" method="post"><label>Assign To:</label><input name="appointmentID" type="hidden" value="'+appointmentDetails.id+'"/><select class="form-control student_list" name="student_id"><option>Choose Student</option>';

                $.each(students, function (key, student) {
                    reassignTo += '<option value="'+student['id']+'">'+student['name']+'</option>';
                });

                reassignTo += '</select>' +
                    '<div class="student_package_list margin-top-15"></div>' +
                    '<div class="lesson_will_deduct_for_reassign margin-top-15" style="display: none;"><label>Number of Lessons to Subtract</label><select class="form-control" name="lesson_will_deduct_for_reassign">' +
                    '<option value="1">1</option>' +
                    '<option value="2">2</option>' +
                    '<option value="3">3</option>' +
                    '<option value="4">4</option>' +
                    '<option value="5">5</option>' +
                    '</select></div>' +
                    '<button class="btn btn-theme margin-top-15">Reassign</button><br/><br/>' +
                    '</form>';
                $('.popover-content .student-box').html();
                $('.popover-content .student-box').html(reassignTo);
                $('.lesson_will_deduct_for_reassign select').val(appointmentDetails.lesson_sum);

            },
            error: function (xhr, status, error) {
                console.log(error);
            }
        });


        $(document).on('change', '.student_list', function(){
            var studentID = parseInt($(this).val());
            if(studentID != 0){

                $.getJSON('<?php echo Router::url('/', true);?>instructor/packages/usersPackages/'+studentID, function (data) {
                    $('.loadingEvent').hide();
                    if(data.package){
                        var studentPackages = '<label>Select Package for Appointment</label><select class="form-control" name="package_id">';
                        $.each(data.package, function (key, value) {
                            studentPackages += '<option value="' + key + '">' + value + '</option>';
                        });
                        studentPackages += '</select>';
                        $('.student_package_list').html();
                        $('.student_package_list').html(studentPackages);

                        /*var lessonWillDeductWhileReassign = '<label>Number of Lessons to Subtract</label><select class="form-control" name="lesson_will_deduct_for_reassign">' +
                            '<option value="1">1</option>' +
                            '<option value="2">2</option>' +
                            '<option value="3">3</option>' +
                            '<option value="4">4</option>' +
                            '<option value="5">5</option>' +
                            '</select>';*/

                        $('.lesson_will_deduct_for_reassign').show();
                        console.log(data);
                    }
                    else{
                        $('#student_package_list').html();
                    }
                });
            }
        });
    }


}



function setAppointment(eventID, appointmentID)
{
    console.log(appointmentID);

    $('.popover').popover().hide();
    $('#lesson_will_deduct').show();
    $('#lesson_left').show();
    $('#appointment_title').show();

    if(typeof(appointmentID) != "undefined"){
        $.ajax({
            url: "<?php echo Router::url('/', true);?>instructor/appointments/getAppointmentByID",
            type: "POST",
            data:  {appointmentID: appointmentID},
            dataType: "json",
            success: function (data) {

                $.getJSON('<?php echo Router::url('/', true);?>instructor/users/userUserByID/'+parseInt(data.appointment_by), function (student) {
                    $('#studentID').val(student.Profile.first_name +'  '+student.Profile.last_name +' - '+student.User.username);
                });

                fetchStudentDetailsForStudent(parseInt(data.appointment_by));

                var date = $.fullCalendar.moment(data.start).format('MM/DD/YYYY');
                var start = $.fullCalendar.moment(data.start).format('hh:mm A');
                var end = $.fullCalendar.moment(data.end).format('hh:mm A');

                $('.setAppointmentDate').val(date);
                $('#lessonWillDeduct').val(data['lesson_sum']);
                $('#appointmentStart').val(start);
                $('#appointmentEnd').val(end);
                $('#appointmentNote').val(data['note']);
                $('#appointment_title input').val(data['title']);

                console.log(data);


                $('#appointmentIDInEditAppointment').html('<input type="hidden" name="beforeAppointmentID" value="'+appointmentID+'"><input type="hidden" name="beforePackageID" value="'+data.package_id+'"><input type="hidden" name="beforeEditPayItFacility" value="'+data.pay_at_facility+'"><input type="hidden" name="beforeEditLessonWillDeduct" value="'+data['lesson_sum']+'"><input type="hidden" name="beforeEditAppointmentBy" value="'+data['appointment_by']+'">');

                $('#setAppointmentBtn').text('Update Appointment');
            },
            error: function (xhr, status, error) {}
        });
    }

    $("#setAppointmentModal" ).modal('show');
    $('.loadingEvent').show();



    if(eventID != false)
    {
        $.getJSON('<?php echo Router::url('/', true);?>calendars/getEventByID/' + parseInt(eventID), function (eventInfo) {
            $('#setAppointmentDate').text($.fullCalendar.moment(eventInfo.Event.start).format('dddd, MMMM DD YYYY'));
            $('.setAppointmentDate').val($.fullCalendar.moment(eventInfo.Event.start).format('dddd, MMMM DD YYYY'));

            $('.loadingEvent').fadeOut(500);
        });
    }

    $('.not-able').hide();
    $('#setAppointmentBtn').hide();
    $('.appointmentTimeFrame').show();
    $('#packageList').show();
    $('#appointmentNote').show();


    $.getJSON('<?php echo Router::url('/', true);?>calendars/getStudents', function (students) {
        $('.loadingEvent').hide();
        $('#studentID')
            .find('option')
            .remove()
            .end()
        ;

        var packageFetched = false;

        $('#studentID').append(
            $("<option></option>").text("Choose Student").val(0)
        );
        $.each(students, function () {
            $('#studentID').append(
                $("<option></option>").text(this.Profile.name).val(this.Profile.user_id)
            );
        });
    });
    $('.setAppointmentEventInfoArea').delay(3000).fadeIn(500);
}


function fetchStudentDetailsForStudent(studentID)
{
    $('.not-able').hide();
    $('#setAppointmentBtn').hide();
    $('.appointmentTimeFrame').hide();
    $('#packageList').hide();

    $('.loadingEvent').show().delay(2000);


    if(studentID != 0){


        $.getJSON('<?php echo Router::url('/', true);?>instructor/packages/usersPackages/'+studentID, function (data) {
            $('.loadingEvent').hide();
            if(data.package){
                var studentPackages = '<label>Select Package for Appointment</label><select class="form-control" name="packageID" id="packageID">';
                $.each(data.package, function (key, value) {
                    studentPackages += '<option value="' + key + '">' + value + '</option>';
                });
                studentPackages += '</select>';
                $('#packageList').html(studentPackages);
            }
            else{
                $('#packageList').html(null);
            }

            $('#lesson_left').html('<strong>Lessons Left: '+ data.lessonLeft +'</strong>');


            var lessonWillDeduct = '';
            $('#lesson_will_deduct').show();
            $('#lesson_left').show();
            $('#appointment_title').show();


            if(data.appointments){
                var calcelledAppointments = '<label>Reschedule for cancelled appointment</label><select class="form-control" name="appointmentID" id="appointmentID"> <option>Choose Lesson</option>';
                if(data.package != 'error'){
                    $.each(data.appointments, function (key, value) {
                        calcelledAppointments += '<option value="' + key + '">' + value + '</option>';
                    });
                }


                calcelledAppointments += '</select>';
                $('#rescheduleAppointments').html(calcelledAppointments);
                $('#rescheduleAppointments').hide();
                $('#wantToReschedule').show();
                $('#wantToReschedule').addClass('animated fadeIn');
            }
            else{
                $('#rescheduleAppointments').html(null);
                $('#wantToReschedule').hide();
            }

            if(data.package == null && data.appointments == null){
                $('.not-able').show();
                $('.not-able').html("N.B. You can not able to set appointment with this student");
                $('#setAppointmentBtn').hide();
                $('.appointmentTimeFrame').hide();
                $('#packageList').hide();
                $('.not-able').addClass('animated fadeIn');
            }
            else{
                $('.not-able').hide();
                $('#setAppointmentBtn').show();
                $('.appointmentTimeFrame').show();
                $('#packageList').show();
                $('#appointmentNote').show();
            }
        });
    }
    else{
        $('.loadingEvent').hide();
        $('.not-able').show();
        $('.not-able').html("Please choose student for set appointment");
    }
}

$(document).on('change', '#wantToReschedule', function(){
    var isChecked = $('#wantToReschedule input').is(':checked');
    if(isChecked == true){
        $('#packageList').hide();
        $('#rescheduleAppointments').show();
        $('#rescheduleAppointments').addClass('animated fadeIn');
    }
    else{
        $('#rescheduleAppointments').hide();
        $('#packageList').show();
        $('#packageList').addClass('animated fadeIn');
    }
})

<!---- Manage Availability Event View ----->
function availabilityEventView(event, element)
{
    element.css('background-color', '#fff');
    element.css('border-color', '#f8f8f8');
    element.css('color', '#333');
    element.find('.fc-time').text('');
}
<!---- /Manage Availability Event View ----->

<!---- Manage Group Lesson Event View ----->
function groupLessonEventView(event, element)
{

    element.css('background-color', '#d9edf7');
    element.css('border-color', '#bce8f1');
    element.css('color', '#31708f');

    element.find('.fc-time').text('');
    element.find('.fc-title').append('<br/> <br/> ' +
        '<strong class="fc_title_group_lesson" style="font-size: 11px; text-transform: uppercase; margin-top: 11px; color: #31708f">Group Lesson</strong>' +
        '<div class="delete_spot"><a href="<?php echo Router::url('/', true);?>calendars/delete_group_lesson/'+event.id+'"><i class="fa fa-times"></i></a></div>');

    var title = '';
    if(event.title)
    {
        title = '<li><strong>Title: </strong>'+ event.title +'</li>';
    }

    var desc = '';
    if(event.description)
    {
        desc = '<li><strong>Description: </strong>'+ event.description +'</li>';
    }


    element.popover({
        title: "Group Lesson",
        placement:'auto',
        html:true,
        trigger : 'click',
        animation : 'true',
        content: '<ul class="data-list">' +
            title +
            desc +
            '<li><strong>price:  </strong>$'+ event.price +'</li>' +
            '<li><strong>Student Limit: </strong>'+ event.student_limit +'</li>' +
            '<li><strong>Filled Up: </strong>'+ event.filled_up_student +'</li>' +
            '<li><strong>Available: </strong>'+ (event.student_limit - event.filled_up_student) +'</li>' +
            '<li><strong>Appointment Date: </strong>'+ $.fullCalendar.moment(event.start._i).format('ddd, MMMM DD, YYYY') +'</li>' +
            '<li><strong>Appointment Time: </strong>'+ $.fullCalendar.moment(event.start._i).format('h:mm a')+ ' - '+ $.fullCalendar.moment(event.end._i).format('h:mm a') +'</li>' +
            '<li><br/><a onclick="createGroupLesson('+event.id+')" class="btn btn-theme-info width-100-p text-uppercase">Edit Group lesson</a><br/><br/><a id="'+event.id+'" href="<?php echo Router::url('/', true);?>instructor/lessons/details/'+event.id+'" class="btn btn-theme width-100-p text-uppercase">View Details</a></li>' +
            '</ul>',
        container:'body'
    });
}
<!---- /Manage Group Lesson Event View ----->

<!---- Manage Block Time Event View ----->
function blockTimeEventView(event, element)
{

    element.css('color', '#a94442');
    element.css('background-color', '#EEE');
    element.css('border-color', '#DDD');

    element.find('.fc-time').text('');
    element.find('.fc-time').append('<label class="appointment-status-for-box label label-warning">Blocked Time</label><div class="delete_spot"><a href="<?php echo Router::url('/', true);?>calendars/delete_block_time/'+event.id+'"><i class="fa fa-times"></i></a></div>');


    var title = '';
    if(event.title)
    {
        title = '<li><strong>Title: </strong>'+ event.title +'</li>';
    }

    var message = '';
    if(event.message)
    {
        message = '<li><strong>Message: </strong>' + event.message + '</li>';
    }

    element.popover({
        title: "<span style='color: #a94442;'>Block Time</span>",
        placement:'auto',
        html:true,
        trigger : 'click',
        animation : 'true',
        content: '<ul class="data-list block_time_list">' +
            title +
            message +
            '<li><strong>Block Date: </strong>'+ $.fullCalendar.moment(event.start._i).format('ddd, MMMM DD, YYYY') +'</li>' +
            '<li><strong>Block Time: </strong>'+ $.fullCalendar.moment(event.start._i).format('h:mm a')+ ' - '+ $.fullCalendar.moment(event.end._i).format('h:mm a') +'</li>' +
            '</ul>',
        container:'body'
    });
}
<!---- /Manage BLock Time Event View ----->

<!---- Manage Appointment Event View ----->
function appointmentEventView(event, element)
{
    element.css('color', '#3c763d');
    element.css('background-color', '#dff0d8');
    element.css('border-color', '#d6e9c6');


    var lesson_left_circle;
    var scheduled_circle;
    var lesson_left = parseInt(event.lesson_left);
    var scheduled = parseInt(event.scheduled);
    var total = parseInt(event.total);

    if(lesson_left > 1){
        lesson_left_circle = 'sm-circle-green';
    }
    else if(lesson_left == 1){
        lesson_left_circle = 'sm-circle-orange';
    }
    else if(lesson_left < 1){
        lesson_left_circle = 'sm-circle-red';
    }

     if(scheduled < 1){
        scheduled_circle = 'sm-circle-red';
    }
    else if(scheduled == total){
        scheduled_circle = 'sm-circle-green';
    }
    else if(scheduled < total){
        scheduled_circle = 'sm-circle-yellow';
    }
    else if(scheduled > total){
        scheduled_circle = 'sm-circle-gold';
    }




    if(event.appointment_status == 1){
        var appointmentStatusForBox = '<label class="appointment-status-for-box label label-gray">appointment pending</label>';
    }
    if(event.appointment_status == 2){
        var appointmentStatusForBox = '<!--<label class="appointment-status-for-box label label-success">appointment confirm</label>-->';
    }
    if(event.appointment_status == 3){
        var appointmentStatusForBox = '<label class="appointment-status-for-box label label-danger">appointment cancelled</label>';

        element.css('color', '#a94442');
        element.css('background-color', '#f2dede');
        element.css('border-color', '#f2dede');

    }
    if(event.appointment_status == 4){
        var appointmentStatusForBox = '<label class="appointment-status-for-box label label-info">Complete</label>';
    }
    if(event.appointment_status == 5){
        var appointmentStatusForBox = '<label class="appointment-status-for-box label label-warning">Incomplete</label>';
    }




    element.find('.fc-time').append('<div class="abs_label">');




    if(event.is_pay_at_facility == false){
        element.find('.fc-time').append('<span class="sm-circle '+lesson_left_circle+'" title="Lesson Left">'+event.lesson_left+'</span>');
        element.find('.fc-time').append('<span class="sm-circle-scheduled '+scheduled_circle+'" title="Lesson Scheduled">'+event.scheduled+'</span>');

        if(event.is_paid_lesson){
            element.find('.fc-time').append('<span class="paid-lesson">$</span>');
        }
    }
    else{

        element.find('.fc-time div').append('<label class="label pay-at-facility-label">Pay At Facility</label><br/>');

        if(event.appointment_status == 2){

            element.css('color', '#8a6d3b');
            element.css('background-color', '#fcf8e3');
            element.css('border-color', '#fcf8e3');

        }
        if(event.appointment_status == 3){

            element.css('color', '#FFF');
            element.css('background-color', '#d08166');
            element.css('border-color', '#fcf8e3');
        }
    }

    console.log(event);
    if(event.is_reassigned == 1){

        element.find('.fc-time div').append('<label class="label pay-at-facility-label">Reassigned</label><br/>');

        element.css('color', '#525252');
        element.css('background-color', '#CCC');
        element.css('border-color', '#CCC');
    }

    element.find('.fc-time div').append(appointmentStatusForBox);

}


function appointmentEventPopover(event, element)
{


    var appointmentType = '<li><strong>Appointment Type: </strong><label class="status-text blue">Regular</label></li>';
    if(event.is_pay_at_facility == false){

        var lessonDeduct = '<li><strong>Lessons Deducted: </strong>'+ event.lesson_sum +'</li>';
        var lessonLeft = '<li><strong>Lessons Left: </strong>'+ event.lesson_left +'</li>';
        var lessonScheduled = '<li><strong>Lessons Scheduled: </strong>'+ event.scheduled +'</li>';
        var paymentStatus = '<li><strong>Payment Status: </strong><label class="status-text orange">UNPAID</label></li>';

        if(event.is_paid_lesson){
            var paymentStatus = '<li><strong>Payment Status: </strong><label class="status-text yellowgreen">PAID</label></li>';
        }
        var payAtFacility = '';
    }
    else{
        var lessonDeduct = '';
        var lessonLeft = '';
        var lessonScheduled = '';
        var paymentStatus = '';
        var payAtFacility = '<label class="label label-info">PAY AT FACILITY</label>';
        var appointmentType = '<li><strong>Appointment Type: </strong><label class="status-text yellowgreen">PAY AT FACILITY</label></li>';
    }

    var appointmentBtn = '';
    var editAppointment = '<a onclick="setAppointment(false, '+parseInt(event.id)+')" class="btn btn-theme-info width-100-p text-uppercase">Edit Appointment</a><br/><br/>';
    if(event.appointment_status == 2){
        if(parseInt(event.student_profile.instructor_id) == parseInt(LoggedInInstructor)){
            var appointmentBtn = '<a class="btn btn-theme-red width-100-p text-uppercase" id="'+parseInt(event.id)+'" href="<?php echo Router::url('/', true);?>calendars/appointmentActions/'+parseInt(event.id)+'/3">Cancel Appointment<a>';
        }
        var appointmentStatusLabel = '<label class="status-text yellow">appointment pending</label>';
        var appointmentStatusForBox = '<label class="appointment-status-for-box label label-gray">appointment pending</label>';
    }
    if(event.appointment_status == 2){
        var appointmentStatusLabel = '<!--<label class="status-text yellowgreen">appointment confirm</label>-->';
        var appointmentStatusForBox = '<!--<label class="appointment-status-for-box label label-success">appointment confirm</label>-->';
    }
    if(event.appointment_status == 3){
        var appointmentStatusLabel = '<label class="status-text orange">appointment cancelled</label>';
        var appointmentStatusForBox = '<label class="appointment-status-for-box label label-danger">appointment cancelled</label>';
    }
    if(event.appointment_status == 4){
        var appointmentStatusLabel = '<label class="status-text green">Complete</label>';
        var appointmentStatusForBox = '<label class="appointment-status-for-box label label-info">Complete</label>';
        editAppointment = '';
    }
    if(event.appointment_status == 5){
        var appointmentStatusLabel = '<label class="status-text red">Incomplete</label>';
        var appointmentStatusForBox = '<label class="appointment-status-for-box label label-warning">Incomplete</label>';
        editAppointment = '';
    }


    var rescheduleAppointment = '';
    if(event.isAppointmentRescheduled == 1)
    {
        var rescheduleAppointment = '<p><strong class="italic-sm danger">N.B.: This is rescheduled appointment</strong> </p>'
    }

    if(event.next_lesson_start)
    {
        var nextAppointment = $.fullCalendar.moment(event.next_lesson_start).format('MMMM DD, YYYY') + '<i style="font-size: 10px; font-style: italic; color; #999; ">('+ $.fullCalendar.moment(event.next_lesson_start).format('h:mm a')+ ' - '+ $.fullCalendar.moment(event.next_lesson_end).format('h:mm a')+')</i>';
    }
    else{
        var nextAppointment = 'N/A'
    }

    var packageName = '';
    if(event.package != "Pay at Facility")
    {
        var packageName = '<li><strong>Package: </strong>'+ event.package +'</li>';
    }

    var title = '';
    if(event.title)
    {
        title = '<li><strong>Title: </strong>'+ event.title +'</li>';
    }

    var note = '';
    if(event.note)
    {
        note = '<li><i style="font-size: 12px !important; color: #999;">Note:'+ event.note +'</i></li>';
    }

    element.popover({
        title: "Appointment Details",
        placement:'auto',
        html:true,
        trigger : 'click',
        animation : 'true',
        content: payAtFacility + '<ul class="data-list">' +
            title +
            '<li><strong>Student Name: </strong>'+ event.name +' <a href="<?php echo Router::url('/', true);?>instructor/users/details/'+event.uuid+'" class="d-btn margin-left-15">View Profile</a></li>' +
            '<li><strong>Email: </strong>'+ event.username +'</li>' +
            '<li><strong>Phone: </strong>'+ event.student_profile.phone +'</li>' +
            packageName  +
            lessonDeduct  +
            lessonLeft  +
            lessonScheduled  +
            paymentStatus  +
            '<li><strong>Appointment Date: </strong>'+ $.fullCalendar.moment(event.start._i).format('ddd, MMMM DD, YYYY') +'</li>' +
            '<li><strong>Appointment Time: </strong>'+ $.fullCalendar.moment(event.start._i).format('h:mm a')+ ' - '+ $.fullCalendar.moment(event.end._i).format('h:mm a') +'</li>' +
            '<li><strong>Next Lesson: </strong>'+ nextAppointment +'</li>' +
            '<li><strong>Current Status: </strong>'+ appointmentStatusLabel +'</li>' +
            '<li><strong>Waiting List: </strong>'+ event.count_waiting +'</li>' +
            appointmentType +
            note +
            '</ul><div class="student-box"></div>' +
            '<div>'+editAppointment+' '+appointmentBtn+'</div>',
        container:'body'
    });
}

<!---- Manage Appointment Event View ----->
</script>


<?php echo $this->Js->writeBuffer(); ?>
<script>
    /*---- ADD Attachment -----*/
    $(document).ready(function () {
        var counter = 1;
        $("#sunAdd").on("click", function () {
            $("#addAllCatBrands").slideUp();
            counter++;
            var newRow = $("<div class='newRow'>");
            var rowData = "";
            rowData += $('.sunScheduleTime').html();
            rowData += '<a class="deleteRow danger"><i class="fa fa-times"></i></a></div>';
            newRow.append(rowData);
            $("div.sunSchedule").append(newRow);
        });
        $("div.sunSchedule").on("click", "a.deleteRow", function (event) {
            $(this).closest(".newRow").remove();
            counter--;
            if (counter < 2) {
                $("#addAllCatBrands").slideDown();
            }
        });

        var counter = 1;
        $("#monAdd").on("click", function () {
            $("#addAllCatBrands").slideUp();
            counter++;
            var newRow = $("<div class='newRow'>");
            var rowData = "";
            rowData += $('.monScheduleTime').html();
            rowData += '<a class="deleteRow danger"><i class="fa fa-times"></i></a></div>';
            newRow.append(rowData);
            $("div.monSchedule").append(newRow);
        });
        $("div.monSchedule").on("click", "a.deleteRow", function (event) {
            $(this).closest(".newRow").remove();
            counter--;
            if (counter < 2) {
                $("#addAllCatBrands").slideDown();
            }
        });

        var counter = 1;
        $("#tueAdd").on("click", function () {
            $("#addAllCatBrands").slideUp();
            counter++;
            var newRow = $("<div class='newRow'>");
            var rowData = "";
            rowData += $('.tueScheduleTime').html();
            rowData += '<a class="deleteRow danger"><i class="fa fa-times"></i></a></div>';
            newRow.append(rowData);
            $("div.tueSchedule").append(newRow);
        });
        $("div.tueSchedule").on("click", "a.deleteRow", function (event) {
            $(this).closest(".newRow").remove();
            counter--;
            if (counter < 2) {
                $("#addAllCatBrands").slideDown();
            }
        });

        var counter = 1;
        $("#wedAdd").on("click", function () {
            $("#addAllCatBrands").slideUp();
            counter++;
            var newRow = $("<div class='newRow'>");
            var rowData = "";
            rowData += $('.wedScheduleTime').html();
            rowData += '<a class="deleteRow danger"><i class="fa fa-times"></i></a></div>';
            newRow.append(rowData);
            $("div.wedSchedule").append(newRow);
        });
        $("div.wedSchedule").on("click", "a.deleteRow", function (event) {
            $(this).closest(".newRow").remove();
            counter--;
            if (counter < 2) {
                $("#addAllCatBrands").slideDown();
            }
        });

        var counter = 1;
        $("#thuAdd").on("click", function () {
            $("#addAllCatBrands").slideUp();
            counter++;
            var newRow = $("<div class='newRow'>");
            var rowData = "";
            rowData += $('.thuScheduleTime').html();
            rowData += '<a class="deleteRow danger"><i class="fa fa-times"></i></a></div>';
            newRow.append(rowData);
            $("div.thuSchedule").append(newRow);
        });
        $("div.thuSchedule").on("click", "a.deleteRow", function (event) {
            $(this).closest(".newRow").remove();
            counter--;
            if (counter < 2) {
                $("#addAllCatBrands").slideDown();
            }
        });

        var counter = 1;
        $("#friAdd").on("click", function () {
            $("#addAllCatBrands").slideUp();
            counter++;
            var newRow = $("<div class='newRow'>");
            var rowData = "";
            rowData += $('.friScheduleTime').html();
            rowData += '<a class="deleteRow danger"><i class="fa fa-times"></i></a></div>';
            newRow.append(rowData);
            $("div.friSchedule").append(newRow);
        });
        $("div.friSchedule").on("click", "a.deleteRow", function (event) {
            $(this).closest(".newRow").remove();
            counter--;
            if (counter < 2) {
                $("#addAllCatBrands").slideDown();
            }
        });

        var counter = 1;
        $("#satAdd").on("click", function () {
            $("#addAllCatBrands").slideUp();
            counter++;
            var newRow = $("<div class='newRow'>");
            var rowData = "";
            rowData += $('.satScheduleTime').html();
            rowData += '<a class="deleteRow danger"><i class="fa fa-times"></i></a></div>';
            newRow.append(rowData);
            $("div.satSchedule").append(newRow);
        });
        $("div.satSchedule").on("click", "a.deleteRow", function (event) {
            $(this).closest(".newRow").remove();
            counter--;
            if (counter < 2) {
                $("#addAllCatBrands").slideDown();
            }
        });
    });
    /*---- ADD Attachment -----*/

</script>

<!-- Modal for the manage schedule -->
<div class="modal event-modal fade" id="calendarSettings" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-dialog">
<form action="<?php echo Router::url('/', true);?>calendars/available" id="setAvailabilityForm" method="post">
<div class="modal-content">
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
    <h4 class="sm-title">Manage Your Available Time</h4>
</div>
<div class="modal-body">
<table class="table set-schedule">
<thead>
<tr>
    <th>Day</th>
    <th>Schedule</th>
    <th>Repeat</th>
</tr>
</thead>
<tbody>
<tr>
    <td>Sunday</td>
    <td>
        <div class="sunSchedule" style="width: 100%;">
            <div class="sunScheduleTime">
                <?php $starting  =  $this->Utilities->getFromHours('From');?>
                <?php $ending =  $this->Utilities->getFromHours('to');?>
                <?php $hoursList =  $this->Utilities->getHours();?>

                <select class="form-control schedule-to-form" name="data[sunday][start][]">
                    <?php echo $starting;?>
                </select>
                <select class="form-control schedule-to-form" name="data[sunday][end][]">
                    <?php echo $ending;?>
                </select>
            </div>

            <!-- Sunday Previous Day Schedule -->
            <div class="clearfix"></div>
            <?php
            $isRepeated = false;
            $singleSchedule = $this->Utilities->setPreviousEvents($events, 'sun');
            if(isset($singleSchedule['repeated'])){
                $isRepeated = 'checked';
                unset($singleSchedule['repeated']);
            }
            ?>
            <?php if(sizeof($singleSchedule > 0)):?>
                <?php foreach($singleSchedule as $schedule):?>
                    <div class="newRow">
                        <select class="form-control schedule-to-form" name="data[sunday][start][]">
                            <?php foreach($hoursList as $hour):?>
                                <option <?php if($schedule['start'] == $hour ){echo 'selected';}?> value="<?php echo $hour;?>">
                                    <?php echo $hour;?>

                                </option>
                            <?php endforeach;?>

                        </select>
                        <select class="form-control schedule-to-form" name="data[sunday][end][]">
                            <?php foreach($hoursList as $hour):?>
                                <option <?php if($schedule['end'] == $hour ){echo 'selected';}?> value="<?php echo $hour;?>">
                                    <?php echo $hour;?>

                                </option>
                            <?php endforeach;?>
                        </select>
                        <a class="deleteRow danger"><i class="fa fa-times"></i></a>
                    </div>
                <?php endforeach;?>
            <?php endif;?>
            <!-- Sunday Previous Day Schedule -->
        </div>
        <p class="addScheduleRow italic-sm" id="sunAdd"> Add another schedule</p>
    </td>
    <td><input name="data[sunday][select]" <?php echo $isRepeated; ?> type="checkbox" value="1"/></td>
</tr>
<tr>
    <td>Monday</td>
    <td>
        <div class="monSchedule" style="width: 100%;">
            <div class="monScheduleTime">
                <select class="form-control schedule-to-form" name="data[monday][start][]">
                    <?php echo $starting;?>
                </select>
                <select class="form-control schedule-to-form" name="data[monday][end][]">
                    <?php echo $ending;?>
                </select>
            </div>
            <!-- Monday Previous Day Schedule -->
            <div class="clearfix"></div>
            <?php
            $isRepeated = false;
            $singleSchedule = $this->Utilities->setPreviousEvents($events, 'mon');
            if(isset($singleSchedule['repeated'])){
                $isRepeated = 'checked';
                unset($singleSchedule['repeated']);
            }
            ?>
            <?php if(sizeof($singleSchedule > 0)):?>
                <?php foreach($singleSchedule as $schedule):?>
                    <div class="newRow">
                        <select class="form-control schedule-to-form" name="data[monday][start][]">
                            <?php foreach($hoursList as $hour):?>
                                <option <?php if($schedule['start'] == $hour ){echo 'selected';}?> value="<?php echo $hour;?>">
                                    <?php echo $hour;?>

                                </option>
                            <?php endforeach;?>

                        </select>
                        <select class="form-control schedule-to-form" name="data[monday][end][]">
                            <?php foreach($hoursList as $hour):?>
                                <option <?php if($schedule['end'] == $hour ){echo 'selected';}?> value="<?php echo $hour;?>">
                                    <?php echo $hour;?>

                                </option>
                            <?php endforeach;?>
                        </select>
                        <a class="deleteRow danger"><i class="fa fa-times"></i></a>
                    </div>
                <?php endforeach;?>
            <?php endif;?>
            <!-- Monday Previous Day Schedule -->
        </div>
        <p class="addScheduleRow italic-sm" id="monAdd"> Add another schedule</p>
    </td>
    <td><input name="data[monday][select]" <?php echo $isRepeated; ?> type="checkbox" value="1"/></td>
</tr>

<tr>
    <td>Tuesday</td>
    <td>
        <div class="tueSchedule" style="width: 100%;">
            <div class="tueScheduleTime">
                <select class="form-control schedule-to-form" name="data[tuesday][start][]">
                    <?php echo $starting;?>
                </select>
                <select class="form-control schedule-to-form" name="data[tuesday][end][]">
                    <?php echo $ending;?>
                </select>
            </div>
            <!-- Tuesday Previous Day Schedule -->
            <div class="clearfix"></div>
            <?php
            $isRepeated = false;
            $singleSchedule = $this->Utilities->setPreviousEvents($events, 'tue');
            if(isset($singleSchedule['repeated'])){
                $isRepeated = 'checked';
                unset($singleSchedule['repeated']);
            }
            ?>
            <?php if(sizeof($singleSchedule > 0)):?>
                <?php foreach($singleSchedule as $schedule):?>
                    <div class="newRow">
                        <select class="form-control schedule-to-form" name="data[tuesday][start][]">
                            <?php foreach($hoursList as $hour):?>
                                <option <?php if($schedule['start'] == $hour ){echo 'selected';}?> value="<?php echo $hour;?>">
                                    <?php echo $hour;?>

                                </option>
                            <?php endforeach;?>

                        </select>
                        <select class="form-control schedule-to-form" name="data[tuesday][end][]">
                            <?php foreach($hoursList as $hour):?>
                                <option <?php if($schedule['end'] == $hour ){echo 'selected';}?> value="<?php echo $hour;?>">
                                    <?php echo $hour;?>

                                </option>
                            <?php endforeach;?>
                        </select>
                        <a class="deleteRow danger"><i class="fa fa-times"></i></a>
                    </div>
                <?php endforeach;?>
            <?php endif;?>
            <!-- Tuesday Previous Day Schedule -->
        </div>
        <p class="addScheduleRow italic-sm" id="tueAdd"> Add another schedule</p>
    </td>
    <td><input name="data[tuesday][select]" <?php echo $isRepeated; ?> type="checkbox" value="1"/></td>
</tr>

<tr>
    <td>Wednesday</td>
    <td>
        <div class="wedSchedule" style="width: 100%;">
            <div class="wedScheduleTime">
                <select class="form-control schedule-to-form" name="data[wednesday][start][]">
                    <?php echo $starting;?>
                </select>
                <select class="form-control schedule-to-form" name="data[wednesday][end][]">
                    <?php echo $ending;?>
                </select>
            </div>
            <!-- Wednesday Previous Day Schedule -->
            <div class="clearfix"></div>
            <?php
            $isRepeated = false;
            $singleSchedule = $this->Utilities->setPreviousEvents($events, 'wed');
            if(isset($singleSchedule['repeated'])){
                $isRepeated = 'checked';
                unset($singleSchedule['repeated']);
            }
            ?>
            <?php if(sizeof($singleSchedule > 0)):?>
                <?php foreach($singleSchedule as $schedule):?>
                    <div class="newRow">
                        <select class="form-control schedule-to-form" name="data[wednesday][start][]">
                            <?php foreach($hoursList as $hour):?>
                                <option <?php if($schedule['start'] == $hour ){echo 'selected';}?> value="<?php echo $hour;?>">
                                    <?php echo $hour;?>

                                </option>
                            <?php endforeach;?>

                        </select>
                        <select class="form-control schedule-to-form" name="data[wednesday][end][]">
                            <?php foreach($hoursList as $hour):?>
                                <option <?php if($schedule['end'] == $hour ){echo 'selected';}?> value="<?php echo $hour;?>">
                                    <?php echo $hour;?>

                                </option>
                            <?php endforeach;?>
                        </select>
                        <a class="deleteRow danger"><i class="fa fa-times"></i></a>
                    </div>
                <?php endforeach;?>
            <?php endif;?>
            <!-- Wednesday Previous Day Schedule -->
        </div>
        <p class="addScheduleRow italic-sm" id="wedAdd"> Add another schedule</p>
    </td>
    <td><input name="data[wednesday][select]" <?php echo $isRepeated; ?> type="checkbox" value="1"/></td>
</tr>

<tr>
    <td>Thursday</td>
    <td>
        <div class="thuSchedule" style="width: 100%;">
            <div class="thuScheduleTime">
                <select class="form-control schedule-to-form" name="data[thursday][start][]">
                    <?php echo $starting;?>
                </select>
                <select class="form-control schedule-to-form" name="data[thursday][end][]">
                    <?php echo $ending;?>
                </select>
            </div>
            <!-- Thursday Previous Day Schedule -->
            <div class="clearfix"></div>
            <?php
            $isRepeated = false;
            $singleSchedule = $this->Utilities->setPreviousEvents($events, 'thu');
            if(isset($singleSchedule['repeated'])){
                $isRepeated = 'checked';
                unset($singleSchedule['repeated']);
            }
            ?>
            <?php if(sizeof($singleSchedule > 0)):?>
                <?php foreach($singleSchedule as $schedule):?>
                    <div class="newRow">
                        <select class="form-control schedule-to-form" name="data[thursday][start][]">
                            <?php foreach($hoursList as $hour):?>
                                <option <?php if($schedule['start'] == $hour ){echo 'selected';}?> value="<?php echo $hour;?>">
                                    <?php echo $hour;?>

                                </option>
                            <?php endforeach;?>

                        </select>
                        <select class="form-control schedule-to-form" name="data[thursday][end][]">
                            <?php foreach($hoursList as $hour):?>
                                <option <?php if($schedule['end'] == $hour ){echo 'selected';}?> value="<?php echo $hour;?>">
                                    <?php echo $hour;?>

                                </option>
                            <?php endforeach;?>
                        </select>
                        <a class="deleteRow danger"><i class="fa fa-times"></i></a>
                    </div>
                <?php endforeach;?>
            <?php endif;?>
            <!-- Thursday Previous Day Schedule -->
        </div>
        <p class="addScheduleRow italic-sm" id="thuAdd"> Add another schedule</p>
    </td>
    <td><input name="data[thursday][select]" <?php echo $isRepeated; ?> type="checkbox" value="1"/></td>
</tr>

<tr>
    <td>Friday</td>
    <td>
        <div class="friSchedule" style="width: 100%;">
            <div class="friScheduleTime">
                <select class="form-control schedule-to-form" name="data[friday][start][]">
                    <?php echo $starting;?>
                </select>
                <select class="form-control schedule-to-form" name="data[friday][end][]">
                    <?php echo $ending;?>
                </select>
            </div>
            <!-- Friday Previous Day Schedule -->
            <div class="clearfix"></div>
            <?php
            $isRepeated = false;
            $singleSchedule = $this->Utilities->setPreviousEvents($events, 'fri');
            if(isset($singleSchedule['repeated'])){
                $isRepeated = 'checked';
                unset($singleSchedule['repeated']);
            }
            ?>
            <?php if(sizeof($singleSchedule > 0)):?>
                <?php foreach($singleSchedule as $schedule):?>
                    <div class="newRow">
                        <select class="form-control schedule-to-form" name="data[friday][start][]">
                            <?php foreach($hoursList as $hour):?>
                                <option <?php if($schedule['start'] == $hour ){echo 'selected';}?> value="<?php echo $hour;?>">
                                    <?php echo $hour;?>

                                </option>
                            <?php endforeach;?>

                        </select>
                        <select class="form-control schedule-to-form" name="data[friday][end][]">
                            <?php foreach($hoursList as $hour):?>
                                <option <?php if($schedule['end'] == $hour ){echo 'selected';}?> value="<?php echo $hour;?>">
                                    <?php echo $hour;?>

                                </option>
                            <?php endforeach;?>
                        </select>
                        <a class="deleteRow danger"><i class="fa fa-times"></i></a>
                    </div>
                <?php endforeach;?>
            <?php endif;?>
            <!-- Friday Previous Day Schedule -->
        </div>
        <p class="addScheduleRow italic-sm" id="friAdd"> Add another schedule</p>
    </td>
    <td><input name="data[friday][select]" <?php echo $isRepeated; ?> type="checkbox" value="1"/></td>
</tr>

<tr>
    <td>Saturday</td>
    <td>
        <div class="satSchedule" style="width: 100%;">
            <div class="satScheduleTime">
                <select class="form-control schedule-to-form" name="data[saturday][start][]">
                    <?php echo $starting;?>
                </select>
                <select class="form-control schedule-to-form" name="data[saturday][end][]">
                    <?php echo $ending;?>
                </select>
            </div>
            <!-- Saturday Previous Day Schedule -->
            <div class="clearfix"></div>
            <?php
            $isRepeated = false;
            $singleSchedule = $this->Utilities->setPreviousEvents($events, 'sat');
            if(isset($singleSchedule['repeated'])){
                $isRepeated = 'checked';
                unset($singleSchedule['repeated']);
            }
            ?>
            <?php if(sizeof($singleSchedule > 0)):?>
                <?php foreach($singleSchedule as $schedule):?>
                    <div class="newRow">
                        <select class="form-control schedule-to-form" name="data[saturday][start][]">
                            <?php foreach($hoursList as $hour):?>
                                <option <?php if($schedule['start'] == $hour ){echo 'selected';}?> value="<?php echo $hour;?>">
                                    <?php echo $hour;?>

                                </option>
                            <?php endforeach;?>

                        </select>
                        <select class="form-control schedule-to-form" name="data[saturday][end][]">
                            <?php foreach($hoursList as $hour):?>
                                <option <?php if($schedule['end'] == $hour ){echo 'selected';}?> value="<?php echo $hour;?>">
                                    <?php echo $hour;?>

                                </option>
                            <?php endforeach;?>
                        </select>
                        <a class="deleteRow danger"><i class="fa fa-times"></i></a>
                    </div>
                <?php endforeach;?>
            <?php endif;?>
            <!-- Saturday Previous Day Schedule -->
        </div>
        <p class="addScheduleRow italic-sm" id="satAdd"> Add another schedule</p>
    </td>

    <td><input name="data[saturday][select]" <?php echo $isRepeated; ?> type="checkbox" value="1"/></td>
</tr>
</tbody>
</table>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-black-radius" data-dismiss="modal">Close</button>
    <button type="button" id="setAvailability" class="btn btn-theme">Save changes</button>
</div>
</div>
</form>
</div>
</div>
<!-- Modal For range Waiting List -->
<div class="modal event-modal fade" id="waitingListModal2" tabindex="-1" role="dialog"
     aria-labelledby="setApointmentModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <form action="<?php echo Router::url('/', true); ?>calendars/setRangeWaitingList" method="post" class="form-horizontal form-custom">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                    </button>
                    <h4 class="sm-title">RESERVE TIMES ON THE WAITING LIST </h4>
                </div>
                <div class="modal-body">

                    <div class="form-group">
                        <div class="col-lg-6">
                            <label for="exampleInputEmail1">Start Date:</label>

                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                                <input id="datepicker" name="fromDate" type="text" class="form-control"
                                       placeholder="From">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <label for="exampleInputEmail1">End Date:</label>

                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                                <input id="datepicker2" name="toDate" type="text" class="form-control"
                                       placeholder="To">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-lg-12"><label>Time Range</label></div>
                        <div class="col-lg-6">
                            <div class="input-group">
                                <select name="waiting_starting_time" class="form-control" id="GroupLessonStart">
                                    <?php echo $this->Utilities->getFromHours('Starting Time');?>
                                </select>
                                <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="input-group">
                                <select name="waiting_ending_time" class="form-control" id="GroupLessonEnd">
                                    <?php echo $this->Utilities->getFromHours('Ending Time');?>
                                </select>
                                <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-12">
                            <label>Student name:</label>
                            <input name="studentID" class="typeahead form-control" type="text" placeholder="Type student name">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-4">
                            <button type="submit" class="btn btn-theme pull-left">Submit</button>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <?php echo $this->Form->end(); ?>
    </div>
</div>
<!-- Modal For range Waiting List -->

<script>
    function createGroupLesson(groupID)
    {
        $('.popover').popover().hide();

        if(typeof(groupID) != "undefined"){
            $.ajax({
                url: "<?php echo Router::url('/', true);?>instructor/lessons/getGLByID",
                type: "POST",
                data:  {groupID: groupID},
                dataType: "json",
                success: function (data) {

                    var date = $.fullCalendar.moment(data.start).format('MM/DD/YYYY');
                    var start = $.fullCalendar.moment(data.start).format('hh:mm A');
                    var end = $.fullCalendar.moment(data.end).format('hh:mm A');

                    $('#groupLessonID').html('<input name="data[GroupLesson][id]" type="hidden" class="form-control" value="'+groupID+'">');
                    $('#GroupLessonTitle').val(data.title);
                    $('#GroupLessonDesc').val(data.description);
                    $('#GroupLessonPrice').val(data.price);
                    $('#GroupLessonStudentLimit').val(data.student_limit);

                    $('.GroupLessonDate').val(date);
                    $('#GroupLessonStart').val(start);
                    $('#GroupLessonEnd').val(end);
                    $('#groupLessonSaveBtn').text('Update Group Lesson');

                },
                error: function (xhr, status, error) {}
            });
        }
        $('#createGroupLesson').modal('show');
    }
</script>

<!-- Modal for the create group lesson -->
<div class="modal event-modal fade" id="createGroupLesson" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <form action="<?php echo Router::url('/', true);?>calendars/save_group_lesson" id="setAvailabilityForm" method="post">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="sm-title">Create Group Lesson</h4>
                </div>
                <div class="modal-body">
                    <div class="eventInfoArea">
                        <div id="groupLessonID"></div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Title</label>
                            <input name="data[GroupLesson][title]" type="text" class="form-control" placeholder="Title" id="GroupLessonTitle">
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Description</label>
                            <textarea name="data[GroupLesson][description]" class="form-control" placeholder="description" rows="4" id="GroupLessonDesc"></textarea>
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Price</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-money"></i></span>
                                <input name="data[GroupLesson][price]" type="text" class="form-control" placeholder="Price" id="GroupLessonPrice">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-lg-12">
                                    <label for="exampleInputEmail1">Date:</label>
                                    <div class="input-group">
                                        <input name="data[GroupLesson][date]" type="text" class="form-control GroupLessonDate datepicker" placeholder="Date">
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-lg-6">
                                    <label for="exampleInputEmail1">Starting Time:</label>
                                    <div class="input-group">
                                        <select name="data[GroupLesson][starting_time]" class="form-control" id="GroupLessonStart">
                                            <?php echo $this->Utilities->getFromHours('Starting Time');?>
                                        </select>
                                        <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <label for="exampleInputEmail1">Ending Time:</label>
                                    <div class="input-group">
                                        <select name="data[GroupLesson][ending_time]" class="form-control" id="GroupLessonEnd">
                                            <?php echo $this->Utilities->getFromHours('Ending Time');?>
                                        </select>
                                        <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Maximum Number of Student:</label>
                            <input name="data[GroupLesson][student_limit]" type="text" class="form-control" placeholder="Maximum number of student" id="GroupLessonStudentLimit">
                        </div>

                        <div class="pull-right">
                            <button type="button" class="btn btn-black-radius" data-dismiss="modal">Close</button>
                            <button class="btn btn-theme"  style="display: inline-block;" id="groupLessonSaveBtn">Save</button>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>


<!-- Modal For Displaying Schedule -->
<div class="modal event-modal fade" id="setAppointmentModal" tabindex="-1" role="dialog" aria-labelledby="createEventLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="border-bottom: 1px solid #e5e5e5;">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                </button>
                <h4 class="sm-title">Create an Appointment</h4>
            </div>
            <div class="modal-body">
                <form action="<?php echo Router::url('/', true);?>/calendars/setAppointmentByInstructor/" method="post">
                    <div class="loadingEvent" style="display: none;">
                        <?php echo $this->Html->image('ajax-loader2.gif')?><br/>
                        Loading, please wait...
                    </div>
                    <div class="setAppointmentEventInfoArea" style="display: none;">
                        <div class="form-group">
                            <label for="exampleInputEmail1" id="setAppointmentDate"></label>
                            <input name="setAppointmentDate" type="hidden" class="setAppointmentDate"/>
                            <div id="appointmentIDInEditAppointment"></div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-lg-12">
                                    <label for="exampleInputEmail1">Want to custom date?</label>
                                    <div class="input-group">
                                        <input name="custom_date" type="text" class="form-control GroupLessonDate datepicker" placeholder="Date">
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Student name:</label>
                            <input name="studentID" id="studentID" class="form-control" type="text" placeholder="Type student name">
                        </div>

                        <div class="form-group" id="appointment_title" style="display: none;">
                            <label>Appointment Title:</label>
                            <input name="appointment_title" class="form-control" type="text" placeholder="Appointment Title">
                        </div>

                        <div class="form-group" id="packageList" style="display: block;">
                            <label>Select Package for Appointment</label>
                            <select class="form-control" name="packageID" id="packageID">
                                <option value="00000000001">Pay at Facility</option>
                            </select>
                        </div>

                        <div class="" id="lesson_left">Lessons Left: 0</div>
                        <div id="lesson_will_deduct" style="display: none;">
                            <label>Number of Lessons to Subtract</label>
                            <select class="form-control" id="lessonWillDeduct" name="lessonWillDeduct">
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                            </select>
                        </div>
                        <div class="form-group" id="rescheduleAppointments">

                        </div>
                        <div id="wantToReschedule" class="checkbox" style="display: none;">
                            <label>
                                <input type="checkbox" value="reschedule"> Want to reschedule?
                            </label>
                        </div>
                        <div class="row appointmentTimeFrame">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Appointment Start</label>
                                    <?php
                                    echo $this->Form->input('start', array(
                                            'id' => 'appointmentStart',
                                            'class' => 'form-control',
                                            'type' => 'select',
                                            'label' => false,
                                            'div' => false,
                                            'options' => $this->Utilities->getHours()
                                        ));
                                    ?>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Appointment End</label>
                                    <?php
                                    echo $this->Form->input('end', array(
                                            'id' => 'appointmentEnd',
                                            'class' => 'form-control',
                                            'type' => 'select',
                                            'label' => false,
                                            'div' => false,
                                            'options' => $this->Utilities->getHours()
                                        ));
                                    ?>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <textarea class="form-control" name="appointmentNote" id="appointmentNote" rows="4" placeholder="Appointment Note" style="display: none;"></textarea>
                        </div>

                        <br/>
                        <p style="display: none;" class="not-found not-able">N.B. You can not able to set appointment with this student</p>
                        <button class="btn btn-theme" id="setAppointmentBtn">Set Appointment</button>
                    </div>
                </form>

            </div>
            <br/>
            <div class="modal-footer"></div>
        </div>
    </div>
</div>
<!-- /Modal For Displaying Schedule -->

<!-- Modal for the create block time -->
<div class="modal event-modal fade" id="blockTimeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <form action="<?php echo Router::url('/', true);?>calendars/block_time" id="setAvailabilityForm" method="post">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="sm-title">Block Time</h4>
                </div>
                <div class="modal-body">
                    <div class="eventInfoArea">

                        <div class="form-group">
                            <label for="exampleInputEmail1">Title</label>
                            <input name="data[BlockTime][title]" type="text" class="form-control" placeholder="Title">
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Description</label>
                            <textarea name="data[BlockTime][message]" class="form-control" placeholder="description" rows="4"></textarea>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-lg-12">
                                    <label for="exampleInputEmail1">Date:</label>
                                    <div class="input-group">
                                        <input name="data[BlockTime][date]" type="text" class="form-control datepicker" placeholder="Date">
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-lg-6">
                                    <label for="exampleInputEmail1">Starting Time:</label>
                                    <div class="input-group">
                                        <select name="data[BlockTime][starting_time]" class="form-control">
                                            <?php echo $this->Utilities->getFromHours('Starting Time');?>
                                        </select>
                                        <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <label for="exampleInputEmail1">Ending Time:</label>
                                    <div class="input-group">
                                        <select name="data[BlockTime][ending_time]" class="form-control">
                                            <?php echo $this->Utilities->getFromHours('Ending Time');?>
                                        </select>
                                        <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="pull-right">
                            <button type="button" class="btn btn-black-radius" data-dismiss="modal">Close</button>
                            <button class="btn btn-theme"  style="display: inline-block;">Save</button>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- Modal for the create block time -->


<?php echo $this->Html->css(array('jq'))?>
<?php echo $this->Html->script('jquery.block.ui')?>
<?php echo $this->Html->script('typehead');?>
<!-- /Modal for the manage schedule -->

<script>
    $(document).ready(function(){
        $("#setAvailability").on("click", function () {
            $('#setAvailabilityForm').submit();
        });


    });

    $(document).ready(function(){
        $('#datepickerSlot').datepicker()
    });
</script>

<script>


    // Instantiate the Bloodhound suggestion engine
    var contacts = new Bloodhound({
        datumTokenizer: function (datum) {
            return Bloodhound.tokenizers.whitespace(datum.value);
        },
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: {
            url: '<?php echo Router::url('/', true);?>instructor/contacts/getContacts/student/%QUERY',
            filter: function (contacts) {
                return $.map(contacts, function (contact) {
                    console.log(contact);
                    return {
                        value: contact['0']['contactName']
                    };
                });
            }
        }
    });

    contacts.initialize();
    // Instantiate the Typeahead UI
    $('.typeahead').typeahead(null, {
        name: 'states',
        displayKey: 'value',
        source: contacts.ttAdapter()
    });



    /*$(window).bind("load", function() {
        var deviceWight = $(window).innerWidth();
        if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {

        }
        else{
            $("#calendar .fc-widget-content").not(":has(.fc-event-container .fc-event)").on("mousedown", function (e) {
                document.oncontextmenu = function() {return false;};
                if( e.button == 2 ) {
                    var offset = $(this).offset();
                    var left = e.pageX;
                    var top = e.pageY;
                    var theHeight = $('.right_button_option').height();
                    $('.right_button_option').show();
                    closeModalPopover();
                    $('.right_button_option').css('left', (left-310) + 'px');
                    $('.right_button_option').css('top', (top-(theHeight/2)-67) + 'px');
                }
            });
        }
    });*/

    $(".not_now").on("click", function () {
        $('.right_button_option').hide();
    });

     function clickRightOptionsBtn(value)
     {
         $('.right_button_option').hide();

         if(value == 1)
         {
            setAppointment(false);
         }
         else if(value == 2)
         {
            $("#createGroupLesson" ).modal('show');
         }
         else if(value == 3)
         {
            $("#blockTimeModal" ).modal('show');
         }
     }

    function closeModalPopover()
    {
        $('.popover').popover().hide();
    }

    var students = new Bloodhound({
        datumTokenizer: function (datum) {
            return Bloodhound.tokenizers.whitespace(datum.value);
        },
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: {
            url: '<?php echo Router::url('/', true);?>instructor/contacts/getContacts/student/%QUERY',
            filter: function (students) {
                return $.map(students, function (contact) {
                    return {
                        value: contact['0']['contactName']
                    };
                });
            }
        }
    });


    students.initialize();
    $('#studentID').typeahead(null, {
        name: 'states',
        displayKey: 'value',
        source: students.ttAdapter()
    }).on('typeahead:selected', function (obj, datum) {
            var studentInfo = datum.value.split(' - ');
            var email = studentInfo[1];

            $.ajax({
                url: "<?php echo Router::url('/', true);?>instructor/users/getUserIDByEmail",
                type: "POST",
                data:  {email: email},
                dataType: "json",
                success: function (studentID) {
                    fetchStudentDetailsForStudent(studentID);
                },
                error: function (xhr, status, error) {}
            });

        });

    $('div').on('hidden.bs.modal', function () {
        $(document).ready(function() {
            $('.form-group').find('input').val('');
            $('.form-group').find('textarea').val('');
            $('.form-group').find('select').val('');
        });
    })

    $('div').on('shown.bs.modal', function () {
        $(document).ready(function() {
            $('.right_button_option').hide();
            $('.popover').popover().hide();
        });
    })

    /*$('div').on('hidden.bs.popover', function () {
        $('.right_button_option').hide();
    })

    $('div').on('shown.bs.popover', function () {
        $('.right_button_option').hide();
    })*/
</script>

<style>
    .datepicker.dropdown-menu {
        z-index: 11111;
    }
</style>

