<?php
/**
 * @var $this View
 */
?>
<h2 class="page-title margin-top-20">My Calendar</h2>
<hr class="shadow-line"/>
<span id="userID" style="display: none;"><?php echo $userID; ?></span>
<div class="control-panel">
    <div class="element">
        <select class="form-control" id="selectInstructor">
            <option>Select Instructor</option>
            <?php foreach($instructors as $instructor):?>
                <option value="<?php echo $instructor['User']['id']; ?>">
                    <?php echo $instructor['Profile']['name']; ?>
                </option>
            <?php endforeach;?>
        </select>
    </div>
    <div class="element">
        <a class="btn btn-theme" data-toggle="modal" data-target="#calendarSettings">My Schedule</a>
        <a class="btn btn-theme" data-toggle="modal" data-target="#createGroupLesson">Create Group Lesson</a>
    </div>
</div>

<div id="calendar"></div>

<!---- Displaying Schedule ----->
<script>
$(document).ajaxStop($.unblockUI);
var LoggedInInstructor = parseInt($('#userID').html());
var defaultSource = '<?php echo Router::url('/', true);?>calendars/getEventByInstructor/' + LoggedInInstructor;
var appointmentSource = '<?php echo Router::url('/', true);?>calendars/getEventOfStudentsByInstructorID/' + LoggedInInstructor;
var groupLesson = '<?php echo Router::url('/', true);?>calendars/getGroupLesson/' + LoggedInInstructor;


$(document).ready(function() {
    var calendar = $('#calendar').fullCalendar({
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
        },
        ignoreTimezone: true,
        defaultView: 'agendaWeek',
        allDayDefault: false,
        editable: true,
        allDaySlot: true,
        slotEventOverlap: true,
        /*selectable: true,*/
        contentHeight: 'auto',
        eventDataTransform: function (data) {

            if(data.Appointment){
                var appointmentDate = $.fullCalendar.moment(data.Appointment.start).format('ddd, MMMM DD, YYYY');
                var appointmentTime = $.fullCalendar.moment(data.Appointment.start).format('h:mm a')+ ' - '+ $.fullCalendar.moment(data.Appointment.end).format('h:mm a');
                var lessonLeft = data.Appointment.lesson_left;
                var eventInfo = {
                    title: data.Profile.name,
                    start: data.Appointment.start,
                    end: data.Appointment.end,
                    appointmentID: data.Appointment.id,
                    appointmentStart: data.Appointment.start,
                    appointmentEnd: data.Appointment.end,
                    appointmentStatus: data.Appointment.status,
                    appointmentNumber: data.Appointment.lesson_no,
                    isAppointmentRescheduled: data.Appointment.is_reschedule,
                    studentProfile: data.Profile,
                    studentLesson: data.Lesson,
                    appointmentDate: appointmentDate,
                    appointmentTime: appointmentTime,
                    packageInfo: data.Package,
                    appointedUser: data.User,
                    appointmentEvent: true,
                    lessonLeft: lessonLeft
                }
            }
            else if (data.GroupLesson){
                var eventInfo = {
                    id: data.GroupLesson.id,
                    title: data.GroupLesson.title,
                    start: data.GroupLesson.start,
                    end: data.GroupLesson.end,
                    student_limit: data.GroupLesson.student_limit,
                    isGrouped: true
                }
            }
            else{
                var eventInfo = {
                    id: data.Event.id,
                    title: data.Event.title,
                    description: data.Event.description,
                    createdBy: data.Event.created_by,
                    start: data.Event.start,
                    end: data.Event.end
                }
            }

            return eventInfo;
        },
        select: function(start, end, allDay) {
            startingTime = $.fullCalendar.moment(start).format('ddd, MMMM DD, h:mm a');
            endingTime = $.fullCalendar.moment(end).format('h:mm a');
            startTime = $.fullCalendar.moment(start).format('YYYY-MM-DD HH:mm:ss');
            endTime = $.fullCalendar.moment(end).format('YYYY-MM-DD HH:mm:ss');

            var eventTime = startingTime + ' - ' + endingTime;
            $('#createEvent #apptStartTime').val(startTime);
            $('#createEvent #apptEndTime').val(endTime);
            $('#createEvent #apptAllDay').val(allDay);
            $('#createEvent #when').text(eventTime);
            $('#createEvent').modal('show');
        },
        eventRender: function(event, element) {
            if(event.appointmentEvent)
            {
                manageEventView(event, element);

                if(event.studentProfile.profile_pic_url != null){
                    var profileImg = '<?php echo Router::url('/', true);?>img/profiles/' + event.studentProfile.profile_pic_url;
                }
                else{
                    var profileImg = '<?php echo Router::url('/', true);?>theme/Instructor/img/avatar.jpg';
                }

                var appointmentBtn = '';

                console.log(event);

                if(event.appointmentStatus == 1){
                    if(parseInt(event.studentProfile.instructor_id) == parseInt(LoggedInInstructor)){
                        var appointmentBtn = '<a class="btn btn-theme-square" id="'+parseInt(event.appointmentID)+'" onclick="appointmentAction('+parseInt(event.appointmentID)+', 2)">Confirn<a> <a class="btn btn-black" id="'+parseInt(event.appointmentID)+'" onclick="appointmentAction('+parseInt(event.appointmentID)+', 3)">Cancel<a>';
                    }
                    var appointmentStatusLabel = '<label class="label label-gray">appointment pending</label>';
                }
                if(event.appointmentStatus == 2){
                    var appointmentStatusLabel = '<label class="label label-success">appointment confirm</label>';
                }
                if(event.appointmentStatus == 3){
                    var appointmentStatusLabel = '<label class="label label-danger">appointment cancelled</label>';
                }
                if(event.appointmentStatus == 4){
                    var appointmentStatusLabel = '<label class="label label-info">appointment completed</label>';
                }
                if(event.appointmentStatus == 5){
                    var appointmentStatusLabel = '<label class="label label-warning">appointment expired</label>';
                }

                var rescheduleAppointment = '';
                if(event.isAppointmentRescheduled == 1)
                {
                    var rescheduleAppointment = '<p><strong class="italic-sm danger">N.B.: This is rescheduled appointment</strong> </p>'
                }

                element.popover({
                    title: "Appointment Information",
                    placement:'auto',
                    html:true,
                    trigger : 'click',
                    animation : 'true',
                    content: '<img src="'+ profileImg +'" class="pop-profile-img"> <ul class="data-list">' +
                        '<li><strong>Name: </strong>'+ event.studentProfile.first_name+ ' '+event.studentProfile.first_name +'</li>' +
                        '<li><strong>Email: </strong>'+ event.appointedUser.username +'</li>' +
                        '<li><strong>Phone: </strong>'+ event.studentProfile.phone +'</li>' +
                        '<li><strong>City: </strong>'+ event.studentProfile.city +'</li>' +
                        '<li><strong>State: </strong>'+ event.studentProfile.state +'</li>' +
                        '<li><strong>Postal Code: </strong>'+ event.studentProfile.postal_code +'</li>' +
                        '<li><strong>Appointment Date: </strong>'+ event.appointmentDate +'</li>' +
                        '<li><strong>Appointment Time: </strong>'+ event.appointmentTime +'</li>' +
                        '<li><strong>Package Name: </strong>'+ event.packageInfo.name +'</li>' +
                        '<li><strong>Lesson Number: </strong>'+ event.appointmentNumber +'</li>' +
                        '<li><strong>Current Status: </strong>'+ appointmentStatusLabel +'</li>' +
                        '</ul>' + appointmentBtn + rescheduleAppointment,
                    container:'body'
                });
            }
            else if(event.isGrouped == true)
            {
                manageGroupEvent(event, element);

                element.popover({
                    title: "Group Lesson",
                    placement:'auto',
                    html:true,
                    trigger : 'click',
                    animation : 'true',
                    content: '<ul class="data-list">' +
                        '<li><strong>Name: </strong>'+ event.title +'</li>' +
                        '<li><strong>Appointment Start: </strong>'+ event.start._i +'</li>' +
                        '<li><strong>Appointment End: </strong>'+ event.end._i +'</li>' +
                        '<li><strong>Student Limit: </strong>'+ event.student_limit +'</li>' +
                        '</ul>',
                    container:'body'
                });
            }
            else{
                element.css('background-color', '#d9edf7');
                element.css('border-color', '#bce8f1');
                element.css('color', '#31708f');
                element.find('.fc-title').text( '' );
                element.find('.fc-time').text( '' );
            }

            $('body').on('click', function (e) {
                if (!element.is(e.target) && element.has(e.target).length === 0 && $('.popover').has(e.target).length === 0)
                    element.popover('hide');
            });
        },
        eventDrop: function(event, delta, revertFunc) {
            editEvents(event);
        },
        eventResize: function(event, delta, revertFunc) {
            editEvents(event);
        },
        eventClick: function (data, allDay, jsEvent, view) {
            if(!data.appointmentID){
                //setAppointment(parseInt(data.id));
            }
        }
        /*eventMouseover: function(event, jsEvent, view) {
         $('.fc-resizer', this).find('.hover-end').remove();
         $('.fc-resizer', this).append('<div class=\"hover-end\">' +
         '<div stop_event_click="empty" class="icons-area">' +
         '<a appointment-id=\"'+event.id+'\" class="icon customizeTimeframe"><i class="fa fa-wrench"></i></a>' +
         '<a appointment-id=\"'+event.id+'\" class="icon editTimeframe"><i class="fa fa-pencil"></i></a>' +
         '<a appointment-id=\"'+event.id+'\" class="icon deleteTimeframe"><i class="fa fa-times"></i></a>' +
         '</div>' +
         '</div>');
         },

         eventMouseout: function(event, jsEvent, view) {
         $('.fc-resizer', this).find('.hover-end').remove();
         }*/
    });
});

function setAppointment(eventID)
{
    $("#setAppointmentModal" ).modal('show');
    $('.loadingEvent').show();
    $.getJSON('<?php echo Router::url('/', true);?>calendars/getEventByID/' + parseInt(eventID), function (eventInfo) {
        $('#setAppointmentDate').text($.fullCalendar.moment(eventInfo.Event.start).format('dddd, MMMM DD YYYY'));


        $('.not-able').hide();
        $('#setAppointmentBtn').hide();
        $('.appointmentTimeFrame').hide();
        $('#packageList').hide();

        $.getJSON('<?php echo Router::url('/', true);?>calendars/getStudents', function (students) {
            $('.loadingEvent').hide();
            $('#studentID')
                .find('option')
                .remove()
                .end()
            ;

            var packageFetched = false;

            $('#studentID').append(
                $("<option></option>").text("Choose Student").val(0)
            );
            $.each(students, function () {

                /*if(packageFetched == false){
                 $.getJSON('<?php echo Router::url('/', true);?>instructor/packages/usersPackages/'+this.Profile.user_id, function (data) {

                 if(data.package){
                 var studentPackages = '<label>Select Package for Appointment</label><select class="form-control" id="packageID">';
                 $.each(data.package, function (key, value) {
                 studentPackages += '<option value="' + key + '">' + value + '</option>';
                 });
                 studentPackages += '</select>';
                 $('#packageList').html(studentPackages);
                 }
                 else{
                 $('#packageList').html(null);
                 }


                 if(data.appointments){
                 var calcelledAppointments = '<label>Reschedule for cancelled appointment</label><select class="form-control" id="appointmentID">';
                 if(data.package != 'error'){
                 $.each(data.appointments, function (key, value) {
                 calcelledAppointments += '<option value="' + key + '">' + value + '</option>';
                 });
                 }
                 calcelledAppointments += '</select>';
                 $('#rescheduleAppointments').html(calcelledAppointments);
                 $('#rescheduleAppointments').hide();
                 $('#wantToReschedule').show();
                 $('#wantToReschedule').addClass('animated fadeIn');
                 }
                 else{
                 $('#rescheduleAppointments').html(null);
                 $('#wantToReschedule').hide();
                 $('#wantToReschedule').addClass('animated fadeIn');
                 }
                 });
                 packageFetched = true;
                 }*/

                $('#studentID').append(
                    $("<option></option>").text(this.Profile.name).val(this.Profile.user_id)
                );
            });
        });

        $('.loadingEvent').fadeOut(500);
        $('.eventInfoArea').delay(3000).fadeIn(500);
    });

    $('#setAppointmentBtn').click(function(){
        var startingTime = $('#setAppointmentDate').html() + ' '+ $('#appointmentStart').val();
        var endingTime = $('#setAppointmentDate').html() + ' '+ $('#appointmentEnd').val();

        var studentID = $('#studentID').val();
        var packageID = $('#packageID').val();
        var appointmentID = $('#appointmentID').val();
        var start = $.fullCalendar.moment(startingTime).format('YYYY-MM-DD HH:mm:ss');
        var end = $.fullCalendar.moment(endingTime).format('YYYY-MM-DD HH:mm:ss');

        var isChecked = $('#wantToReschedule input').is(':checked');
        if(isChecked == true){
            packageID = null;
        }
        else{
            appointmentID = null
        }

        var appointmentInfo = {
            'studentID': studentID,
            'packageID': packageID,
            'appointmentID': appointmentID,
            'start': start,
            'end': end
        };


        var d1 = new Date(start);
        var d2 = new Date(end);
        var diff = (d2 - d1)/1000/60/60/24;

        if(diff > 0){
            if(studentID != null){
                if(appointmentID != null || packageID != null){
                    $.ajax({
                        url: '<?php echo Router::url('/', true);?>/calendars/setAppointmentByInstructor',
                        type: "POST",
                        dataType: "json",
                        data:  appointmentInfo,
                        success: function(data) {
                            if(data == 'done'){
                                manageFlashMessage('alert-success', 'Appointment has been completed');
                            }
                            else if(data == 'reschedule'){
                                manageFlashMessage('alert-success', 'Appointment has been rescheduled successfully');
                            }
                            else if(data == 'invalid-time'){
                                manageFlashMessage('alert-danger', 'Sorry, appointment time invalid');
                            }
                            else{
                                manageFlashMessage('alert-danger', 'Sorry, something went wrong');
                            }
                        }
                    });
                }
                else{
                    manageFlashMessage('alert-danger', 'You have no purchased package available for appointment')
                }
            }
            else{
                manageFlashMessage('alert-danger', 'Sorry, you have no student for appointment')
            }
        }
        else{
            manageFlashMessage('alert-danger', 'Sorry, starting time should be less then ending time')
        }



        $("#setAppointmentModal" ).modal('hide');
        $('#calendar').fullCalendar('refetchEvents');
        $('#calendar').fullCalendar('refetchEvents');
    });
}

<!---- CONFIRM/CANCEL APPOINTMENT ----->
function appointmentAction(appointmentID, action)
{
    console.log(appointmentID);
    console.log(action);

    $.ajax({
        url: "<?php echo Router::url('/', true);?>/calendars/appointmentActions/"+appointmentID+"/"+action+"/",
        type: "POST",
        dataType: "json",
        success: function (data) {
            if (data == 2) {
                $('.popover').hide();
                manageFlashMessage('alert-success', 'Appointment has been confirmed')
            }
            else if (data == 3) {
                $('.popover').hide();
                manageFlashMessage('alert-danger', 'Appointment has been cancelled')
            }
            else {
                manageFlashMessage('alert-danger', 'Sorry, something went wrong')
            }
        },
        error: function (xhr, status, error) {
            console.log(error);
        }
    });
    $('#calendar').fullCalendar('refetchEvents');
    $('#calendar').fullCalendar('refetchEvents');
}
<!---- /CONFIRM/CANCEL APPOINTMENT ----->

<!---- EDIT specific Schedule ----->
function editEvents(event)
{
    var eventInfo = {
        'id': event.id,
        'title': event.title,
        'start': event.start.format('YYYY-MM-DD HH:mm:ss'),
        'end': event.end.format('YYYY-MM-DD HH:mm:ss')
    };

    if(parseInt(event.createdBy) == LoggedInInstructor){
        $.ajax({
            url: '<?php echo Router::url('/', true);?>/calendars/editEvents',
            type: "POST",
            dataType: "json",
            data: eventInfo,
            success: function (data) {
                if (data == 'done') {
                    manageFlashMessage('alert-success', 'Schedule has been modified successfully')
                }
                else {
                    manageFlashMessage('alert-danger', 'Sorry, something went wrong')
                }
            },
            error: function (xhr, status, error) {
                console.log(error);
            }
        });
    }

    $('#calendar').fullCalendar('refetchEvents');
    $('#calendar').fullCalendar('refetchEvents');
}

function removeEvent(eventID)
{
    $.ajax({
        url: '<?php echo Router::url('/', true);?>/calendars/removeEvent/'+ parseInt(eventID),
        type: "POST",
        dataType: "json",
        success: function (data) {
            if (data == 'done') {
                manageFlashMessage('alert-success', 'Schedule has been deleted');
            }
            else {
                manageFlashMessage('alert-danger', 'Sorry, something went wrong');
            }
        },
        error: function (xhr, status, error) {
            console.log(error);
        }
    });
    $('#calendar').fullCalendar('refetchEvents');
    $('#calendar').fullCalendar('refetchEvents');
}
<!---- /EDIT specific Schedule ----->

<!----- View any Specific Schedule ----->
function viewEvent(eventID){
    $("#eventView" ).modal('show');
    $.getJSON('<?php echo Router::url('/', true);?>/calendars/getEventsByID/' + parseInt(eventID), function (data) {
        $('.loadingEvent').show();
        $('#eventTitle').html(data.Event.title);
        $('#eventInstructor').html(data.Profile.name);
        $('#eventDescription').html(data.Event.description);
        $('#eventDate').html($.fullCalendar.moment(data.Event.start).format('ddd, MMMM DD, YYYY'));
        $('#eventTime').html($.fullCalendar.moment(data.Event.start).format('h:mm a') + ' - ' + $.fullCalendar.moment(data.Event.end).format('h:mm a'));
        $('#appointmentBy').html(data.AppointedBy.first_name);

        $('.loadingEvent').fadeOut(500);
        $('.eventInfoArea').delay(3000).fadeIn(500);

        if(data.Event.is_appointed == 2){
            $('.appointmentComplete').show(500);
            $('#isAppointed').show();
            $('#isAppointed').addClass('animated fadeIn');
        }
        var userID = parseInt(data.Event.created_by);
        if($('#userID').html() == userID){
            $('.eventBtnArea').show();
            $('.eventBtnArea').addClass('animated fadeIn');
        }

        $('#eventView').on('hidden.bs.modal', function () {
            clearArea();
        })
    });
}
<!----- View any Specific Schedule ----->

<!----- Displaying different instructor ----->
$(document).ready(function() {
    $('#calendar').fullCalendar('addEventSource', defaultSource);
    $('#calendar').fullCalendar('addEventSource', appointmentSource);
    $('#calendar').fullCalendar('addEventSource', groupLesson);
    var instructorID;
    $('#selectInstructor').change(function(){
        $(document).ajaxStart($.blockUI(
            {
                message: '<h1><img src="<?php echo Router::url('/', true);?>theme/Instructor/img/busy.gif" /> ' + 'Just a moment...</h1>'
            }
        )).ajaxStop($.unblockUI);

        if(instructorID != '')
        {
            var removeEventSources = '<?php echo Router::url('/', true);?>calendars/getEventByInstructor/' + instructorID;
            var removeAppointmentSources = '<?php echo Router::url('/', true);?>calendars/getEventOfStudentsByInstructorID/' + instructorID;
            $('#calendar').fullCalendar('removeEventSource', removeEventSources);
            $('#calendar').fullCalendar('removeEventSource', removeAppointmentSources);
            instructorID = parseInt($('#selectInstructor').val());
        }
        var eventSources = '<?php echo Router::url('/', true);?>calendars/getEventByInstructor/' + instructorID
        var appointmentSourceOfStd = '<?php echo Router::url('/', true);?>calendars/getEventOfStudentsByInstructorID/' + instructorID
        $('#calendar').fullCalendar('removeEventSource', defaultSource);
        $('#calendar').fullCalendar('removeEventSource', appointmentSource);
        $('#calendar').fullCalendar('addEventSource', eventSources);
        $('#calendar').fullCalendar('addEventSource', appointmentSourceOfStd);
        $('#calendar').fullCalendar('refetchEvents');
    });
});
<!----- /Displaying different instructor ----->

<!-----  Managing the specific Schedule view ----->
function manageEventView(event, element)
{
    if(event.appointmentStatus == 1){
        var appointmentStatusLabel = '<label class="label label-gray">appointment pending</label>';
    }
    if(event.appointmentStatus == 2){
        var appointmentStatusLabel = '<label class="label label-success">appointment confirm</label>';
    }
    if(event.appointmentStatus == 3){
        var appointmentStatusLabel = '<label class="label label-danger">appointment cancelled</label>';
    }
    if(event.appointmentStatus == 4){
        var appointmentStatusLabel = '<label class="label label-info">appointment completed</label>';
    }
    if(event.appointmentStatus == 5){
        var appointmentStatusLabel = '<label class="label label-warning">appointment expired</label>';
    }

    if(event.studentProfile.profile_pic_url != null){
        var profileImg = '<?php echo Router::url('/', true);?>img/profiles/' + event.studentProfile.profile_pic_url;
    }
    else{
        var profileImg = '<?php echo Router::url('/', true);?>theme/Instructor/img/avatar.jpg';
    }

    element.find('.fc-resizer').removeClass();
    element.find('.fc-time').prepend( '<img src="'+ profileImg +'" class="vs-profile"/>' );
    element.find('.fc-time').append( '<p style="margin: 0px; padding: 0px; font-weight: bold; font-size: 10px;">'+event.studentProfile.first_name+' '+ event.studentProfile.last_name+'</p>' );
    element.find('.fc-time').append( '<span class="appointment-status-label">'+appointmentStatusLabel+'</span>');


    if(event.lessonLeft == 0)
    {
        element.css('color', '#525252');
        element.css('background-color', '#d9534f');
        element.css('border-color', '#d9534f');
    }
    else if(event.lessonLeft == 1)
    {
        element.css('color', '#525252');
        element.css('background-color', '#ffff00');
        element.css('border-color', '#ffff00');
    }
    else if(event.lessonLeft == 2){
        element.css('color', '#525252');
        element.css('background-color', 'orange');
        element.css('border-color', 'orange');
    }
    else{
        element.css('color', '#525252');
        element.css('background-color', '#AAA');
        element.css('border-color', '#AAA');
    }




    if(event.isAppointmentRescheduled == 1)
    {
        element.find('.fc-time').append(' (Rescheduled) ');
    }





}
<!----- /Managing the specific Schedule view ----->

<!-----  Managing the specific Schedule view ----->
function manageGroupEvent(event, element)
{
    element.css('color', '#525252');
    element.css('background-color', '#d9534f');
    element.css('border-color', '#d9534f');

}

<!----- /Managing the specific Schedule view ----->

<!----- Create New Schedule ----->
$(document).ready(function() {
    $('#createEventButton').click(function(){

        $('.creatingEvent').show().delay(2000);
        var eventTitle = $('#eventName').val();
        var eventDescription = $('#eventDescription').val();
        $('.creatingEvent').fadeOut(0);

        if(eventTitle != '')
        {
            var eventInfo = {
                'title': eventTitle,
                'description': eventDescription,
                'start': $('#apptStartTime').val(),
                'end': $('#apptEndTime').val(),
                'allDay': ($('#apptAllDay').val() == true)
            };

            $.ajax({
                url: '<?php echo Router::url('/', true);?>/calendars/createEvents',
                type: "POST",
                dataType: "json",
                data:  eventInfo,
                success: function(data) {
                    if(data == 'done'){
                        manageFlashMessage('alert-success', 'New schedule has been created successfully')
                    }
                    else if(data == 'empty-title'){
                        manageFlashMessage('alert-danger', 'Sorry, Title can not be empty')
                    }
                    else{
                        manageFlashMessage('alert-danger', 'Sorry, something went wrong')
                    }
                },
                error: function(xhr, status, error) {

                }
            });

            $("#createEvent").modal('hide');
            $('#calendar').fullCalendar('refetchEvents');
            $('#calendar').fullCalendar('refetchEvents');
            $('#createAppointmentForm')[0].reset();
            $('#createAppointmentForm')[1].reset();
        }
        else{
            $(".empty-title" ).empty();
            $(".empty-title" ).append( "Sorry, you entered invalid title" );
        }

        $('#createEvent').on('hidden.bs.modal', function () {
            clearArea();
        })
    });
});
<!----- /Create New Event ----->

function clearArea(){
    $('.creatingEvent').hide();
    $('.loadingEvent').hide();
    $('.eventInfoArea').hide();;
    $('.appointmentComplete').hide();
    $('.eventBtnArea').hide();
    $('#isAppointed').hide();
    $('.empty-title').empty();
}


</script>


<!-- Modal For Displaying Schedule -->
<div class="modal event-modal fade" id="setAppointmentModal" tabindex="-1" role="dialog" aria-labelledby="createEventLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="border-bottom: 1px solid #e5e5e5;">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                </button>
                <h4 class="sm-title">My Appointment</h4>
            </div>
            <div class="modal-body">
                <div class="loadingEvent" style="display: none;">
                    <?php echo $this->Html->image('ajax-loader2.gif')?><br/>
                    Loading, please wait...
                </div>

                <div class="eventInfoArea" style="display: none;">
                    <div class="form-group">
                        <label for="exampleInputEmail1" id="setAppointmentDate"></label>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Select Student</label>
                        <select class="form-control" id="studentID">
                        </select>
                    </div>
                    <div class="form-group" id="packageList">

                    </div>
                    <div class="form-group" id="rescheduleAppointments">

                    </div>
                    <div id="wantToReschedule" class="checkbox" style="display: none;">
                        <label>
                            <input type="checkbox" value="reschedule"> Want to reschedule?
                        </label>
                    </div>
                    <div class="row appointmentTimeFrame">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Appointment Start</label>
                                <?php
                                echo $this->Form->input('start', array(
                                        'id' => 'appointmentStart',
                                        'class' => 'form-control',
                                        'type' => 'select',
                                        'label' => false,
                                        'div' => false,
                                        'options' => $this->Utilities->getHours()
                                    ));
                                ?>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Appointment End</label>
                                <?php
                                echo $this->Form->input('end', array(
                                        'id' => 'appointmentEnd',
                                        'class' => 'form-control',
                                        'type' => 'select',
                                        'label' => false,
                                        'div' => false,
                                        'options' => $this->Utilities->getHours()
                                    ));
                                ?>
                            </div>
                        </div>
                    </div>
                    <br/>
                    <p style="display: none;" class="not-found not-able">N.B. You can not able to set appointment with this student</p>
                    <button class="btn btn-theme" id="setAppointmentBtn">Set Appointment</button>
                </div>
            </div>
            <br/>
            <div class="modal-footer"></div>
        </div>
    </div>
</div>
<!-- /Modal For Displaying Schedule -->


<!-- Modal For Creating Schedule -->
<div class="modal event-modal fade" id="createEvent" tabindex="-1" role="dialog" aria-labelledby="createEventLabel" aria-hidden="true">
    <div class="modal-dialog">
        <form id="createAppointmentForm" action="<?php echo Router::url('/', true);?>calendars/createEvents" method="post">
            <div class="modal-content">
                <div class="modal-header" style="border-bottom: 1px solid #e5e5e5;">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                    </button>
                    <h4 class="sm-title">Set Your Schedule</h4>
                </div>
                <div class="modal-body">
                    <div class="loadingEvent creatingEvent margin-bottom-10" style="display: none;">
                        <?php echo $this->Html->image('ajax-loader2.gif')?><br/>
                        Schedule has been creating, please be patient...
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">When:</label>
                        <span class="controls controls-row" id="when" style="margin-top:5px;"> </span>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Name of Schedule:</label>
                        <input type="text" class="form-control" name="eventName" id="eventName" data-provide="typeahead" data-items="4" data-source="[&quot;Value 1&quot;,&quot;Value 2&quot;,&quot;Value 3&quot;]">
                        <input type="hidden" id="apptStartTime"/>
                        <input type="hidden" id="apptEndTime"/>
                        <input type="hidden" id="apptAllDay" />
                        <div class="empty-title field-empty"></div>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Description:</label>
                        <textarea class="form-control" name="eventDescription" id="eventDescription" rows="5" placeholder="Write description..."> </textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <a id="createEventButton" class="btn btn-theme pull-left">Create New Event</a>
                </div>
            </div>
            <?php echo $this->Form->end();?>
    </div>
</div>
<!-- /Modal For Creating Schedule -->

<!-- Modal For View Schedule -->
<div class="modal event-modal fade" id="eventView" tabindex="-1" role="dialog" aria-labelledby="createEventLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="border-bottom: 1px solid #e5e5e5;">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                </button>
                <h4 class="page-title">Schedule Details</h4>
            </div>
            <div class="modal-body" style="min-height: 200px;">
                <div class="loadingEvent" style="display: none;">
                    <?php echo $this->Html->image('ajax-loader2.gif')?><br/>
                    Schedule is loading
                </div>

                <div class="eventInfoArea" style="display: none;">
                    <div class="appointment-status">
                        <div class="appointmentComplete" style="display: none;">
                            <span class="fa fa-search-plus appointment-available"></span>
                            <br/>
                            <strong>Appointed</strong>
                        </div>
                    </div>

                    <ul class="data-list">
                        <li>
                            <strong>Title: </strong> <span id="eventTitle"></span>
                        </li>
                        <li>
                            <strong>Description: </strong> <span id="eventDescription"></span>
                        </li>
                        <li>
                            <strong>Instructor Name: </strong> <span id="eventInstructor"></span>
                        </li>
                        <li>
                            <strong>Date: </strong> <span id="eventDate"></span>
                        </li>
                        <li>
                            <strong>Time: </strong> <span id="eventTime"></span>
                        </li>
                        <li id='isAppointed' style="display: none;">
                            <strong>Appointed By: </strong> <span id="appointmentBy"></span>
                        </li>
                    </ul>
                </div>

            </div>
            <div class="modal-footer">
                <div class="eventBtnArea" style="display: none;">
                    <a class="btn btn-theme pull-left">Edit Schedule</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Modal For View Schedule -->

<!-- Popover 2 hidden content -->

<!-- /Popover 2 hidden content -->

<?php echo $this->Js->writeBuffer(); ?>
<script>
    /*---- ADD Attachment -----*/
    $(document).ready(function () {
        var counter = 1;
        $("#sunAdd").on("click", function () {
            $("#addAllCatBrands").slideUp();
            counter++;
            var newRow = $("<div class='newRow'>");
            var rowData = "";
            rowData += $('.sunScheduleTime').html();
            rowData += '<a class="deleteRow danger"><i class="fa fa-times"></i></a></div>';
            newRow.append(rowData);
            $("div.sunSchedule").append(newRow);
        });
        $("div.sunSchedule").on("click", "a.deleteRow", function (event) {
            $(this).closest(".newRow").remove();
            counter--;
            if (counter < 2) {
                $("#addAllCatBrands").slideDown();
            }
        });

        var counter = 1;
        $("#monAdd").on("click", function () {
            $("#addAllCatBrands").slideUp();
            counter++;
            var newRow = $("<div class='newRow'>");
            var rowData = "";
            rowData += $('.monScheduleTime').html();
            rowData += '<a class="deleteRow danger"><i class="fa fa-times"></i></a></div>';
            newRow.append(rowData);
            $("div.monSchedule").append(newRow);
        });
        $("div.monSchedule").on("click", "a.deleteRow", function (event) {
            $(this).closest(".newRow").remove();
            counter--;
            if (counter < 2) {
                $("#addAllCatBrands").slideDown();
            }
        });

        var counter = 1;
        $("#tueAdd").on("click", function () {
            $("#addAllCatBrands").slideUp();
            counter++;
            var newRow = $("<div class='newRow'>");
            var rowData = "";
            rowData += $('.tueScheduleTime').html();
            rowData += '<a class="deleteRow danger"><i class="fa fa-times"></i></a></div>';
            newRow.append(rowData);
            $("div.tueSchedule").append(newRow);
        });
        $("div.tueSchedule").on("click", "a.deleteRow", function (event) {
            $(this).closest(".newRow").remove();
            counter--;
            if (counter < 2) {
                $("#addAllCatBrands").slideDown();
            }
        });

        var counter = 1;
        $("#wedAdd").on("click", function () {
            $("#addAllCatBrands").slideUp();
            counter++;
            var newRow = $("<div class='newRow'>");
            var rowData = "";
            rowData += $('.wedScheduleTime').html();
            rowData += '<a class="deleteRow danger"><i class="fa fa-times"></i></a></div>';
            newRow.append(rowData);
            $("div.wedSchedule").append(newRow);
        });
        $("div.wedSchedule").on("click", "a.deleteRow", function (event) {
            $(this).closest(".newRow").remove();
            counter--;
            if (counter < 2) {
                $("#addAllCatBrands").slideDown();
            }
        });

        var counter = 1;
        $("#thuAdd").on("click", function () {
            $("#addAllCatBrands").slideUp();
            counter++;
            var newRow = $("<div class='newRow'>");
            var rowData = "";
            rowData += $('.thuScheduleTime').html();
            rowData += '<a class="deleteRow danger"><i class="fa fa-times"></i></a></div>';
            newRow.append(rowData);
            $("div.thuSchedule").append(newRow);
        });
        $("div.thuSchedule").on("click", "a.deleteRow", function (event) {
            $(this).closest(".newRow").remove();
            counter--;
            if (counter < 2) {
                $("#addAllCatBrands").slideDown();
            }
        });

        var counter = 1;
        $("#friAdd").on("click", function () {
            $("#addAllCatBrands").slideUp();
            counter++;
            var newRow = $("<div class='newRow'>");
            var rowData = "";
            rowData += $('.friScheduleTime').html();
            rowData += '<a class="deleteRow danger"><i class="fa fa-times"></i></a></div>';
            newRow.append(rowData);
            $("div.friSchedule").append(newRow);
        });
        $("div.friSchedule").on("click", "a.deleteRow", function (event) {
            $(this).closest(".newRow").remove();
            counter--;
            if (counter < 2) {
                $("#addAllCatBrands").slideDown();
            }
        });

        var counter = 1;
        $("#satAdd").on("click", function () {
            $("#addAllCatBrands").slideUp();
            counter++;
            var newRow = $("<div class='newRow'>");
            var rowData = "";
            rowData += $('.satScheduleTime').html();
            rowData += '<a class="deleteRow danger"><i class="fa fa-times"></i></a></div>';
            newRow.append(rowData);
            $("div.satSchedule").append(newRow);
        });
        $("div.satSchedule").on("click", "a.deleteRow", function (event) {
            $(this).closest(".newRow").remove();
            counter--;
            if (counter < 2) {
                $("#addAllCatBrands").slideDown();
            }
        });
    });
    /*---- ADD Attachment -----*/
</script>

<!-- Modal for the manage schedule -->
<div class="modal event-modal fade" id="calendarSettings" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-dialog">
<form action="<?php echo Router::url('/', true);?>calendars/available" id="setAvailabilityForm" method="post">
<div class="modal-content">
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
    <h4 class="sm-title">Manage Your Available Time</h4>
</div>
<div class="modal-body">
<table class="table set-schedule">
<thead>
<tr>
    <th>Day</th>
    <th>Schedule</th>
    <th>Repeat</th>
</tr>
</thead>
<tbody>
<tr>
    <td>Sunday</td>
    <td>
        <div class="sunSchedule" style="width: 100%;">
            <div class="sunScheduleTime">
                <?php $starting  =  $this->Utilities->getFromHours('From');?>
                <?php $ending =  $this->Utilities->getFromHours('to');?>
                <?php $hoursList =  $this->Utilities->getHours();?>

                <select class="form-control schedule-to-form" name="data[sunday][start][]">
                    <?php echo $starting;?>
                </select>
                <select class="form-control schedule-to-form" name="data[sunday][end][]">
                    <?php echo $ending;?>
                </select>
            </div>

            <!-- Sunday Previous Day Schedule -->
            <div class="clearfix"></div>
            <?php
            $isRepeated = false;
            $singleSchedule = $this->Utilities->setPreviousEvents($events, 'sun');
            if(isset($singleSchedule['repeated'])){
                $isRepeated = 'checked';
                unset($singleSchedule['repeated']);
            }
            ?>
            <?php if(sizeof($singleSchedule > 0)):?>
                <?php foreach($singleSchedule as $schedule):?>
                    <div class="newRow">
                        <select class="form-control schedule-to-form" name="data[sunday][start][]">
                            <?php foreach($hoursList as $hour):?>
                                <option <?php if($schedule['start'] == $hour ){echo 'selected';}?> value="<?php echo $hour;?>">
                                    <?php echo $hour;?>

                                </option>
                            <?php endforeach;?>

                        </select>
                        <select class="form-control schedule-to-form" name="data[sunday][end][]">
                            <?php foreach($hoursList as $hour):?>
                                <option <?php if($schedule['end'] == $hour ){echo 'selected';}?> value="<?php echo $hour;?>">
                                    <?php echo $hour;?>

                                </option>
                            <?php endforeach;?>
                        </select>
                        <a class="deleteRow danger"><i class="fa fa-times"></i></a>
                    </div>
                <?php endforeach;?>
            <?php endif;?>
            <!-- Sunday Previous Day Schedule -->
        </div>
        <p class="addScheduleRow italic-sm" id="sunAdd"> Add another schedule</p>
    </td>
    <td><input name="data[sunday][select]" <?php echo $isRepeated; ?> type="checkbox" value="1"/></td>
</tr>
<tr>
    <td>Monday</td>
    <td>
        <div class="monSchedule" style="width: 100%;">
            <div class="monScheduleTime">
                <select class="form-control schedule-to-form" name="data[monday][start][]">
                    <?php echo $starting;?>
                </select>
                <select class="form-control schedule-to-form" name="data[monday][end][]">
                    <?php echo $ending;?>
                </select>
            </div>
            <!-- Monday Previous Day Schedule -->
            <div class="clearfix"></div>
            <?php
            $isRepeated = false;
            $singleSchedule = $this->Utilities->setPreviousEvents($events, 'mon');
            if(isset($singleSchedule['repeated'])){
                $isRepeated = 'checked';
                unset($singleSchedule['repeated']);
            }
            ?>
            <?php if(sizeof($singleSchedule > 0)):?>
                <?php foreach($singleSchedule as $schedule):?>
                    <div class="newRow">
                        <select class="form-control schedule-to-form" name="data[monday][start][]">
                            <?php foreach($hoursList as $hour):?>
                                <option <?php if($schedule['start'] == $hour ){echo 'selected';}?> value="<?php echo $hour;?>">
                                    <?php echo $hour;?>

                                </option>
                            <?php endforeach;?>

                        </select>
                        <select class="form-control schedule-to-form" name="data[monday][end][]">
                            <?php foreach($hoursList as $hour):?>
                                <option <?php if($schedule['end'] == $hour ){echo 'selected';}?> value="<?php echo $hour;?>">
                                    <?php echo $hour;?>

                                </option>
                            <?php endforeach;?>
                        </select>
                        <a class="deleteRow danger"><i class="fa fa-times"></i></a>
                    </div>
                <?php endforeach;?>
            <?php endif;?>
            <!-- Monday Previous Day Schedule -->
        </div>
        <p class="addScheduleRow italic-sm" id="monAdd"> Add another schedule</p>
    </td>
    <td><input name="data[monday][select]" <?php echo $isRepeated; ?> type="checkbox" value="1"/></td>
</tr>

<tr>
    <td>Tuesday</td>
    <td>
        <div class="tueSchedule" style="width: 100%;">
            <div class="tueScheduleTime">
                <select class="form-control schedule-to-form" name="data[tuesday][start][]">
                    <?php echo $starting;?>
                </select>
                <select class="form-control schedule-to-form" name="data[tuesday][end][]">
                    <?php echo $ending;?>
                </select>
            </div>
            <!-- Tuesday Previous Day Schedule -->
            <div class="clearfix"></div>
            <?php
            $isRepeated = false;
            $singleSchedule = $this->Utilities->setPreviousEvents($events, 'tue');
            if(isset($singleSchedule['repeated'])){
                $isRepeated = 'checked';
                unset($singleSchedule['repeated']);
            }
            ?>
            <?php if(sizeof($singleSchedule > 0)):?>
                <?php foreach($singleSchedule as $schedule):?>
                    <div class="newRow">
                        <select class="form-control schedule-to-form" name="data[tuesday][start][]">
                            <?php foreach($hoursList as $hour):?>
                                <option <?php if($schedule['start'] == $hour ){echo 'selected';}?> value="<?php echo $hour;?>">
                                    <?php echo $hour;?>

                                </option>
                            <?php endforeach;?>

                        </select>
                        <select class="form-control schedule-to-form" name="data[tuesday][end][]">
                            <?php foreach($hoursList as $hour):?>
                                <option <?php if($schedule['end'] == $hour ){echo 'selected';}?> value="<?php echo $hour;?>">
                                    <?php echo $hour;?>

                                </option>
                            <?php endforeach;?>
                        </select>
                        <a class="deleteRow danger"><i class="fa fa-times"></i></a>
                    </div>
                <?php endforeach;?>
            <?php endif;?>
            <!-- Tuesday Previous Day Schedule -->
        </div>
        <p class="addScheduleRow italic-sm" id="tueAdd"> Add another schedule</p>
    </td>
    <td><input name="data[tuesday][select]" <?php echo $isRepeated; ?> type="checkbox" value="1"/></td>
</tr>

<tr>
    <td>Wednesday</td>
    <td>
        <div class="wedSchedule" style="width: 100%;">
            <div class="wedScheduleTime">
                <select class="form-control schedule-to-form" name="data[wednesday][start][]">
                    <?php echo $starting;?>
                </select>
                <select class="form-control schedule-to-form" name="data[wednesday][end][]">
                    <?php echo $ending;?>
                </select>
            </div>
            <!-- Wednesday Previous Day Schedule -->
            <div class="clearfix"></div>
            <?php
            $isRepeated = false;
            $singleSchedule = $this->Utilities->setPreviousEvents($events, 'wed');
            if(isset($singleSchedule['repeated'])){
                $isRepeated = 'checked';
                unset($singleSchedule['repeated']);
            }
            ?>
            <?php if(sizeof($singleSchedule > 0)):?>
                <?php foreach($singleSchedule as $schedule):?>
                    <div class="newRow">
                        <select class="form-control schedule-to-form" name="data[wednesday][start][]">
                            <?php foreach($hoursList as $hour):?>
                                <option <?php if($schedule['start'] == $hour ){echo 'selected';}?> value="<?php echo $hour;?>">
                                    <?php echo $hour;?>

                                </option>
                            <?php endforeach;?>

                        </select>
                        <select class="form-control schedule-to-form" name="data[wednesday][end][]">
                            <?php foreach($hoursList as $hour):?>
                                <option <?php if($schedule['end'] == $hour ){echo 'selected';}?> value="<?php echo $hour;?>">
                                    <?php echo $hour;?>

                                </option>
                            <?php endforeach;?>
                        </select>
                        <a class="deleteRow danger"><i class="fa fa-times"></i></a>
                    </div>
                <?php endforeach;?>
            <?php endif;?>
            <!-- Wednesday Previous Day Schedule -->
        </div>
        <p class="addScheduleRow italic-sm" id="wedAdd"> Add another schedule</p>
    </td>
    <td><input name="data[wednesday][select]" <?php echo $isRepeated; ?> type="checkbox" value="1"/></td>
</tr>

<tr>
    <td>Thursday</td>
    <td>
        <div class="thuSchedule" style="width: 100%;">
            <div class="thuScheduleTime">
                <select class="form-control schedule-to-form" name="data[thursday][start][]">
                    <?php echo $starting;?>
                </select>
                <select class="form-control schedule-to-form" name="data[thursday][end][]">
                    <?php echo $ending;?>
                </select>
            </div>
            <!-- Thursday Previous Day Schedule -->
            <div class="clearfix"></div>
            <?php
            $isRepeated = false;
            $singleSchedule = $this->Utilities->setPreviousEvents($events, 'thu');
            if(isset($singleSchedule['repeated'])){
                $isRepeated = 'checked';
                unset($singleSchedule['repeated']);
            }
            ?>
            <?php if(sizeof($singleSchedule > 0)):?>
                <?php foreach($singleSchedule as $schedule):?>
                    <div class="newRow">
                        <select class="form-control schedule-to-form" name="data[thursday][start][]">
                            <?php foreach($hoursList as $hour):?>
                                <option <?php if($schedule['start'] == $hour ){echo 'selected';}?> value="<?php echo $hour;?>">
                                    <?php echo $hour;?>

                                </option>
                            <?php endforeach;?>

                        </select>
                        <select class="form-control schedule-to-form" name="data[thursday][end][]">
                            <?php foreach($hoursList as $hour):?>
                                <option <?php if($schedule['end'] == $hour ){echo 'selected';}?> value="<?php echo $hour;?>">
                                    <?php echo $hour;?>

                                </option>
                            <?php endforeach;?>
                        </select>
                        <a class="deleteRow danger"><i class="fa fa-times"></i></a>
                    </div>
                <?php endforeach;?>
            <?php endif;?>
            <!-- Thursday Previous Day Schedule -->
        </div>
        <p class="addScheduleRow italic-sm" id="thuAdd"> Add another schedule</p>
    </td>
    <td><input name="data[thursday][select]" <?php echo $isRepeated; ?> type="checkbox" value="1"/></td>
</tr>

<tr>
    <td>Friday</td>
    <td>
        <div class="friSchedule" style="width: 100%;">
            <div class="friScheduleTime">
                <select class="form-control schedule-to-form" name="data[friday][start][]">
                    <?php echo $starting;?>
                </select>
                <select class="form-control schedule-to-form" name="data[friday][end][]">
                    <?php echo $ending;?>
                </select>
            </div>
            <!-- Friday Previous Day Schedule -->
            <div class="clearfix"></div>
            <?php
            $isRepeated = false;
            $singleSchedule = $this->Utilities->setPreviousEvents($events, 'fri');
            if(isset($singleSchedule['repeated'])){
                $isRepeated = 'checked';
                unset($singleSchedule['repeated']);
            }
            ?>
            <?php if(sizeof($singleSchedule > 0)):?>
                <?php foreach($singleSchedule as $schedule):?>
                    <div class="newRow">
                        <select class="form-control schedule-to-form" name="data[friday][start][]">
                            <?php foreach($hoursList as $hour):?>
                                <option <?php if($schedule['start'] == $hour ){echo 'selected';}?> value="<?php echo $hour;?>">
                                    <?php echo $hour;?>

                                </option>
                            <?php endforeach;?>

                        </select>
                        <select class="form-control schedule-to-form" name="data[friday][end][]">
                            <?php foreach($hoursList as $hour):?>
                                <option <?php if($schedule['end'] == $hour ){echo 'selected';}?> value="<?php echo $hour;?>">
                                    <?php echo $hour;?>

                                </option>
                            <?php endforeach;?>
                        </select>
                        <a class="deleteRow danger"><i class="fa fa-times"></i></a>
                    </div>
                <?php endforeach;?>
            <?php endif;?>
            <!-- Friday Previous Day Schedule -->
        </div>
        <p class="addScheduleRow italic-sm" id="friAdd"> Add another schedule</p>
    </td>
    <td><input name="data[friday][select]" <?php echo $isRepeated; ?> type="checkbox" value="1"/></td>
</tr>

<tr>
    <td>Saturday</td>
    <td>
        <div class="satSchedule" style="width: 100%;">
            <div class="satScheduleTime">
                <select class="form-control schedule-to-form" name="data[saturday][start][]">
                    <?php echo $starting;?>
                </select>
                <select class="form-control schedule-to-form" name="data[saturday][end][]">
                    <?php echo $ending;?>
                </select>
            </div>
            <!-- Saturday Previous Day Schedule -->
            <div class="clearfix"></div>
            <?php
            $isRepeated = false;
            $singleSchedule = $this->Utilities->setPreviousEvents($events, 'sat');
            if(isset($singleSchedule['repeated'])){
                $isRepeated = 'checked';
                unset($singleSchedule['repeated']);
            }
            ?>
            <?php if(sizeof($singleSchedule > 0)):?>
                <?php foreach($singleSchedule as $schedule):?>
                    <div class="newRow">
                        <select class="form-control schedule-to-form" name="data[saturday][start][]">
                            <?php foreach($hoursList as $hour):?>
                                <option <?php if($schedule['start'] == $hour ){echo 'selected';}?> value="<?php echo $hour;?>">
                                    <?php echo $hour;?>

                                </option>
                            <?php endforeach;?>

                        </select>
                        <select class="form-control schedule-to-form" name="data[saturday][end][]">
                            <?php foreach($hoursList as $hour):?>
                                <option <?php if($schedule['end'] == $hour ){echo 'selected';}?> value="<?php echo $hour;?>">
                                    <?php echo $hour;?>

                                </option>
                            <?php endforeach;?>
                        </select>
                        <a class="deleteRow danger"><i class="fa fa-times"></i></a>
                    </div>
                <?php endforeach;?>
            <?php endif;?>
            <!-- Saturday Previous Day Schedule -->
        </div>
        <p class="addScheduleRow italic-sm" id="satAdd"> Add another schedule</p>
    </td>

    <td><input name="data[saturday][select]" <?php echo $isRepeated; ?> type="checkbox" value="1"/></td>
</tr>
</tbody>
</table>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-black-radius" data-dismiss="modal">Close</button>
    <button type="button" id="setAvailability" class="btn btn-theme">Save changes</button>
</div>
</div>
</form>
</div>
</div>

<!-- Modal for the create group lesson -->
<div class="modal event-modal fade" id="createGroupLesson" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <form action="<?php echo Router::url('/', true);?>calendars/save_group_lesson" id="setAvailabilityForm" method="post">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="sm-title">Create Group Lesson</h4>
                </div>
                <div class="modal-body">
                    <div class="eventInfoArea">

                        <div class="form-group">
                            <label for="exampleInputEmail1">Title</label>
                            <input name="data[GroupLesson][title]" type="text" class="form-control" placeholder="Title">
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-lg-12">
                                    <label for="exampleInputEmail1">Date:</label>
                                    <div class="input-group">
                                        <input id="datepicker" name="data[GroupLesson][date]" type="text" class="form-control" placeholder="Date">
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="row">
                                <div class="col-lg-6">
                                    <label for="exampleInputEmail1">Starting Time:</label>
                                    <div class="input-group">

                                        <select name="data[GroupLesson][starting_time]" id="appointmentStart" class="form-control">
                                            <option value="12:00 AM">12:00 AM</option>
                                            <option value="12:30 AM">12:30 AM</option>
                                            <option value="01:00 AM">01:00 AM</option>
                                            <option value="01:30 AM">01:30 AM</option>
                                            <option value="02:00 AM">02:00 AM</option>
                                            <option value="02:30 AM">02:30 AM</option>
                                            <option value="03:00 AM">03:00 AM</option>
                                            <option value="03:30 AM">03:30 AM</option>
                                            <option value="04:00 AM">04:00 AM</option>
                                            <option value="04:30 AM">04:30 AM</option>
                                            <option value="05:00 AM">05:00 AM</option>
                                            <option value="05:30 AM">05:30 AM</option>
                                            <option value="06:00 AM">06:00 AM</option>
                                            <option value="06:30 AM">06:30 AM</option>
                                            <option value="07:00 AM">07:00 AM</option>
                                            <option value="07:30 AM">07:30 AM</option>
                                            <option value="08:00 AM">08:00 AM</option>
                                            <option value="08:30 AM">08:30 AM</option>
                                            <option value="09:00 AM">9:00 AM</option>
                                            <option value="09:30 AM">09:30 AM</option>
                                            <option value="10:00 AM">10:00 AM</option>
                                            <option value="10:30 AM">10:30 AM</option>
                                            <option value="11:00 AM">11:00 AM</option>
                                            <option value="11:30 AM">11:30 AM</option>
                                            <option value="12:00 PM">12:00 PM</option>
                                            <option value="12:30 PM">12:30 PM</option>
                                            <option value="01:00 PM">01:00 PM</option>
                                            <option value="01:30 PM">01:30 PM</option>
                                            <option value="02:00 PM">02:00 PM</option>
                                            <option value="02:30 PM">02:30 PM</option>
                                            <option value="03:00 PM">03:00 PM</option>
                                            <option value="03:30 PM">03:30 PM</option>
                                            <option value="04:00 PM">04:00 PM</option>
                                            <option value="04:30 PM">04:30 PM</option>
                                            <option value="05:00 PM">05:00 PM</option>
                                            <option value="05:30 PM">05:30 PM</option>
                                            <option value="06:00 PM">06:00 PM</option>
                                            <option value="06:30 PM">06:30 PM</option>
                                            <option value="07:00 PM">07:00 PM</option>
                                            <option value="07:30 PM">07:30 PM</option>
                                            <option value="08:00 PM">08:00 PM</option>
                                            <option value="08:30 PM">08:30 PM</option>
                                            <option value="09:00 PM">09:00 PM</option>
                                            <option value="09:30 PM">09:30 PM</option>
                                            <option value="10:00 PM">10:00 PM</option>
                                            <option value="10:30 PM">10:30 PM</option>
                                            <option value="11:00 PM">11:00 PM</option>
                                            <option value="11:30 PM">11:30 PM</option>
                                        </select>
                                        <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <label for="exampleInputEmail1">Ending Time:</label>
                                    <div class="input-group">

                                        <select name="data[GroupLesson][ending_time]" id="appointmentStart" class="form-control">
                                            <option value="12:00 AM">12:00 AM</option>
                                            <option value="12:30 AM">12:30 AM</option>
                                            <option value="01:00 AM">01:00 AM</option>
                                            <option value="01:30 AM">01:30 AM</option>
                                            <option value="02:00 AM">02:00 AM</option>
                                            <option value="02:30 AM">02:30 AM</option>
                                            <option value="03:00 AM">03:00 AM</option>
                                            <option value="03:30 AM">03:30 AM</option>
                                            <option value="04:00 AM">04:00 AM</option>
                                            <option value="04:30 AM">04:30 AM</option>
                                            <option value="05:00 AM">05:00 AM</option>
                                            <option value="05:30 AM">05:30 AM</option>
                                            <option value="06:00 AM">06:00 AM</option>
                                            <option value="06:30 AM">06:30 AM</option>
                                            <option value="07:00 AM">07:00 AM</option>
                                            <option value="07:30 AM">07:30 AM</option>
                                            <option value="08:00 AM">08:00 AM</option>
                                            <option value="08:30 AM">08:30 AM</option>
                                            <option value="09:00 AM">9:00 AM</option>
                                            <option value="09:30 AM">09:30 AM</option>
                                            <option value="10:00 AM">10:00 AM</option>
                                            <option value="10:30 AM">10:30 AM</option>
                                            <option value="11:00 AM">11:00 AM</option>
                                            <option value="11:30 AM">11:30 AM</option>
                                            <option value="12:00 PM">12:00 PM</option>
                                            <option value="12:30 PM">12:30 PM</option>
                                            <option value="01:00 PM">01:00 PM</option>
                                            <option value="01:30 PM">01:30 PM</option>
                                            <option value="02:00 PM">02:00 PM</option>
                                            <option value="02:30 PM">02:30 PM</option>
                                            <option value="03:00 PM">03:00 PM</option>
                                            <option value="03:30 PM">03:30 PM</option>
                                            <option value="04:00 PM">04:00 PM</option>
                                            <option value="04:30 PM">04:30 PM</option>
                                            <option value="05:00 PM">05:00 PM</option>
                                            <option value="05:30 PM">05:30 PM</option>
                                            <option value="06:00 PM">06:00 PM</option>
                                            <option value="06:30 PM">06:30 PM</option>
                                            <option value="07:00 PM">07:00 PM</option>
                                            <option value="07:30 PM">07:30 PM</option>
                                            <option value="08:00 PM">08:00 PM</option>
                                            <option value="08:30 PM">08:30 PM</option>
                                            <option value="09:00 PM">09:00 PM</option>
                                            <option value="09:30 PM">09:30 PM</option>
                                            <option value="10:00 PM">10:00 PM</option>
                                            <option value="10:30 PM">10:30 PM</option>
                                            <option value="11:00 PM">11:00 PM</option>
                                            <option value="11:30 PM">11:30 PM</option>
                                        </select>
                                        <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Maximum Number of Student:</label>
                            <input name="data[GroupLesson][student_limit]" type="text" class="form-control" placeholder="Maximum number of student">
                        </div>

                        <div class="pull-right">
                            <button type="button" class="btn btn-black-radius" data-dismiss="modal">Close</button>
                            <button class="btn btn-theme" id="setAppointmentBtn" style="display: inline-block;">Save</button>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<?php echo $this->Html->css(array('jq'))?>
<?php echo $this->Html->script('jquery.block.ui')?>
<!-- /Modal for the manage schedule -->

<script>
    $(document).ready(function(){
        $("#setAvailability").on("click", function () {
            $('#setAvailabilityForm').submit();
        });
    });

    $(document).ready(function(){
        $('#datepickerSlot').datepicker()
    });
</script>

<style>
    .datepicker.dropdown-menu {
        z-index: 11111;
    }
</style>

<script>
    $(document).on('change', '#studentID', function(){

        $('.not-able').hide();
        $('#setAppointmentBtn').hide();
        $('.appointmentTimeFrame').hide();
        $('#packageList').hide();

        $('.loadingEvent').show().delay(2000);

        var studentID = parseInt($(this).val());
        if(studentID != 0){


            $.getJSON('<?php echo Router::url('/', true);?>instructor/packages/usersPackages/'+studentID, function (data) {
                $('.loadingEvent').hide();
                if(data.package){
                    var studentPackages = '<label>Select Package for Appointment</label><select class="form-control" id="packageID">';
                    $.each(data.package, function (key, value) {
                        studentPackages += '<option value="' + key + '">' + value + '</option>';
                    });
                    studentPackages += '</select>';
                    $('#packageList').html(studentPackages);
                }
                else{
                    $('#packageList').html(null);
                }


                if(data.appointments){
                    var calcelledAppointments = '<label>Reschedule for cancelled appointment</label><select class="form-control" id="appointmentID"> <option>Choose Lesson</option>';
                    if(data.package != 'error'){
                        $.each(data.appointments, function (key, value) {
                            calcelledAppointments += '<option value="' + key + '">' + value + '</option>';
                        });
                    }
                    calcelledAppointments += '</select>';
                    $('#rescheduleAppointments').html(calcelledAppointments);
                    $('#rescheduleAppointments').hide();
                    $('#wantToReschedule').show();
                    $('#wantToReschedule').addClass('animated fadeIn');
                }
                else{
                    $('#rescheduleAppointments').html(null);
                    $('#wantToReschedule').hide();
                }

                if(data.package == null && data.appointments == null){
                    $('.not-able').show();
                    $('.not-able').html("N.B. You can not able to set appointment with this student");
                    $('#setAppointmentBtn').hide();
                    $('.appointmentTimeFrame').hide();
                    $('#packageList').hide();
                    $('.not-able').addClass('animated fadeIn');
                }
                else{
                    $('.not-able').hide();
                    $('#setAppointmentBtn').show();
                    $('.appointmentTimeFrame').show();
                    $('#packageList').show();
                }
            });
        }
        else{
            $('.loadingEvent').hide();
            $('.not-able').show();
            $('.not-able').html("Please choose student for set appointment");
        }
    });


    $('#wantToReschedule').click(function(){
        var isChecked = $('#wantToReschedule input').is(':checked');
        if(isChecked == true){
            $('#packageList').hide();
            $('#rescheduleAppointments').show();
            $('#rescheduleAppointments').addClass('animated fadeIn');
        }
        else{
            $('#rescheduleAppointments').hide();
            $('#packageList').show();
            $('#packageList').addClass('animated fadeIn');
        }
    });


    $( ".hover-end" ).mouseover(function() {
        $(this).attr("stop_event_click", "");
    });


    $('.hover-end').mouseleave(function(){
        $(this).attr("stop_event_click", "empty");
    });


    $('.customizeTimeframe').click(function(){
        var appointmentID = $(this).attr('appointment-id');
    });


    $('.editTimeframe').click(function(){
        var appointmentID = $(this).attr('appointment-id');
    });


    $('.deleteTimeframe').click(function(){
        var appointmentID = $(this).attr('appointment-id');
        console.log(appointmentID);
    });
</script>