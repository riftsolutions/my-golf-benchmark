<?php
/**
 * @var $this View
 */
?>
<h2 class="page-title">My Dashboard</h2>
<hr class="shadow-line"/>
<div class="content">
    <div class="row">

        <div class="col-lg-6 col-md-6 col-sm-6">

            <!----- APPLICATION OVERVIEW ------>
            <div class="d-box">
                <h4 class="d-title"><i class="fa fa-flask yellowgreen"></i> Application Overview</h4>
                <ul class="d-list">
                    <li>
                        <?php echo $this->Html->link('Total leads', array('controller' => 'contacts', 'action' => 'list', 'all')); ?>
                        <span class="pull-right d-badge d-badge-danger"><?php echo $contactsOverview[0][0]['totalContact'];?></span>
                    </li>
                    <li>
                        <?php echo $this->Html->link('Total Students', array('controller' => 'users', 'action' => 'list', 'all')); ?>
                        <span class="pull-right d-badge d-badge-gray"><?php echo $countStudent;?></span>
                    </li>
                    <li>
                        <?php echo $this->Html->link('Total Running POS', array('controller' => 'packages', 'action' => 'pos', 'all')); ?>
                        <span class="pull-right d-badge d-badge-green"><?php echo $countPOS;?></span>
                    </li>
                    <li>
                        <?php echo $this->Html->link('Total Benchmarks', array('controller' => 'benchmarks', 'action' => 'list', 'all')); ?>
                        <span class="pull-right d-badge d-badge-orange"><?php echo $countBenchmark;?></span>
                    </li>
                </ul>
                <div class="see-more">
                    <?php echo $this->html->link('See More <i class="fa fa-long-arrow-right"></i>', array('controller' => 'lessons', 'action' => 'list'), array('escape' => false))?>
                </div>

                <div class="clearfix"></div>
            </div>
            <!----- /APPLICATION OVERVIEW ------>

            <!----- UPCOMING APPOINTMENT ------>
            <div class="d-box">
                <h4 class="d-title padding-bottom-10">
                    <i class="fa fa-fighter-jet danger"></i> Upcoming Appointments
                    <?php
                    if($upComingAppointments){
                        echo $this->html->link('See Full List', array('controller' => 'appointments', 'action' => 'list/upcoming'), array('escape' => false, 'class' => 'full-list pull-right'));
                    }
                    ?>
                </h4>

                <?php if($upComingAppointments):?>
                    <table class="table margin-top-25">
                        <thead>
                        <th>Appointment Time</th>
                        <th>Student Name</th>
                        <th style="text-align: right;">Action</th>
                        </thead>
                        <tbody>
                        <?php foreach($upComingAppointments as $appointment):?>
                            <tr>
                                <td>
                                    <?php echo $this->Time->format('d M, Y', $appointment['Appointment']['start'])?>
                                    <i class="italic-sm">(
                                        <?php echo $this->Time->format('h:i A', $appointment['Appointment']['start'])?>
                                        - <?php echo $this->Time->format('h:i A', $appointment['Appointment']['end'])?>
                                        )
                                    </i>
                                </td>
                                <td>
                                    <?php echo $this->Html->link($appointment[0]['studentName'], array('controller' => 'users', 'action' => 'details', $appointment['User']['uuid']), array('class' => 'green'));?>
                                </td>
                                <td style="text-align: right;">
                                    <?php echo $this->Html->link('View', array('controller' => 'appointments', 'action' => 'view', $appointment['Appointment']['id']), array('class' => 'd-btn'));?>
                                </td>
                            </tr>
                        <?php endforeach;?>

                        </tbody>
                    </table>

                    <div class="see-more">
                        <?php echo $this->html->link('See More <i class="fa fa-long-arrow-right"></i>', array('controller' => 'appointments', 'action' => 'list'), array('escape' => false))?>
                    </div>

                <?php else:?>
                    <?php echo $this->element('not-found')?>
                <?php endif;?>

                <div class="clearfix"></div>
            </div>
            <!----- /UPCOMING APPOINTMENT ------>

            <!----- FOLLOW UP NOTE ------>
            <div class="d-box">
                <h4 class="d-title padding-bottom-10">
                    <i class="fa fa-bell orange"></i> Upcoming Follow Ups
                    <?php
                    if($followUp){
                        echo $this->html->link('See Full List', array('controller' => 'contacts', 'action' => 'note_list'), array('escape' => false, 'class' => 'full-list pull-right'));
                    }
                    ?>
                </h4>
                <?php if($followUp):?>
                    <table class="table margin-top-25">
                        <thead>
                        <th style="width: 20%">Contact Name</th>
                        <th style="width: 40%;">Note</th>
                        <th style="width: 10%;">Status</th>
                        <th style="width: 15%;">Date</th>
                        </thead>
                        <tbody>
                        <?php foreach($followUp as $follow):?>
                            <tr>
                                <td>
                                    <?php
                                    if($follow['Contact']['id']){
                                        echo $this->Html->link($follow['Contact']['first_name'].' '. $follow['Contact']['last_name'], array('controller' => 'contacts', 'action' => 'view', $follow['Contact']['id']), array('class' => 'green'));
                                    }
                                    elseif($follow['User']['id']){
                                        echo $this->Html->link($follow['Profile']['first_name'].' '. $follow['Profile']['last_name'], array('controller' => 'users', 'action' => 'details', $follow['User']['uuid']), array('class' => 'green'));
                                    }
                                    else{
                                        echo 'N/A';
                                    }

                                    ;?>
                                </td>
                                <td>
                                    <?php
                                    if($follow['Contact']['id']){
                                        echo $this->Html->link($this->Text->excerpt($follow['Note']['note'], null, 35, ' <span class="green">...more</span>'), array('controller' => 'contacts', 'action' => 'view/'.$follow['Contact']['id'].'#'.$follow['Note']['id'] ), array('escape' => false));
                                    }
                                    if($follow['User']['id']){
                                        echo $this->Html->link($this->Text->excerpt($follow['Note']['note'], null, 35, ' <span class="green">...more</span>'), array('controller' => 'users', 'action' => 'details/'.$follow['User']['uuid'].'#'.$follow['Note']['id'] ), array('escape' => false));
                                    }

                                    ;?>
                                </td>

                                <td>
                                    <?php
                                    if($follow['Contact']['id']){
                                        echo '<label class="label label-info">contact</label>';
                                    }
                                    elseif($follow['User']['id']){
                                        echo '<label class="label label-success">student</label>';
                                    }
                                    else{
                                        echo '<label class="label label-default">N/A</label>';
                                    }
                                    ?>
                                </td>

                                <td>
                                    <?php  echo $this->Time->format('d M, Y', $follow['Note']['note_date']);?>
                                </td>
                            </tr>
                        <?php endforeach;?>

                        </tbody>
                    </table>
                <?php else:?>
                    <?php echo $this->element('not-found')?>
                <?php endif;?>

                <div class="clearfix"></div>
            </div>
            <!----- /FOLLOW UP NOTE ------>

            <!----- SLIPPING AWAY STUDENT LIST ------>
            <div class="d-box">
                <h4 class="d-title padding-bottom-10">
                    <i class="fa fa-binoculars slate-blue"></i> Students Slipping Away
                    <?php
                    if($slippingAway){
                        echo $this->html->link('See Full List', array('controller' => 'users', 'action' => 'student//slipping_away'), array('escape' => false, 'class' => 'full-list pull-right'));
                    }
                    ?>
                </h4>
                <?php if($slippingAway):?>
                    <table class="table margin-top-25">
                        <thead>
                        <th>Student Name</th>
                        <th>Last Activities</th>
                        <th style="text-align: right;">Action</th>
                        </thead>
                        <tbody>
                        <?php foreach($slippingAway as $slipping):?>
                            <tr>
                                <td>
                                    <?php
                                    echo $this->Html->link($slipping['Profile']['first_name']. ' '. $slipping['Profile']['last_name'], array('controller' => 'users', 'action' => 'details', $slipping['User']['uuid']), array('class' => 'green'));
                                    ?>
                                </td>
                                <td>
                                    <?php
                                    if(!strstr('0000-00-00 00:00:00', $slipping['User']['last_login']) && $slipping['User']['last_login']){
                                        echo $this->Time->timeAgoInWords($slipping['User']['last_login'],
                                            array('accuracy' => array('month' => 'month'),'end' => '1 year'));
                                    }
                                    else{
                                        echo '<strong class="danger">Never logged in</strong>';
                                    }
                                    ?>
                                </td>
                                <td style="text-align: right;">
                                    <?php
                                    echo $this->Html->link('Details', array('controller' => 'users', 'action' => 'details', $slipping['User']['uuid']), array('class' => 'd-btn'))
                                    ?>
                                </td>
                            </tr>
                        <?php endforeach;?>
                        </tbody>
                    </table>
                <?php else:?>
                    <?php echo $this->element('not-found')?>
                <?php endif;?>

                <div class="clearfix"></div>
            </div>
            <!----- SLIPPING AWAY STUDENT LIST ------>

            <!----- WAITING LIST ------>
            <div class="d-box">
                <h4 class="d-title padding-bottom-10">
                    <i class="fa fa-clock-o default"></i> Waiting List
                    <?php
                    if($waitingList){
                        echo $this->html->link('See Full List', array('controller' => 'appointments', 'action' => 'waiting'), array('escape' => false, 'class' => 'full-list pull-right'));
                    }
                    ?>
                </h4>

                <?php if($waitingList):?>
                    <table class="table">
                        <thead>
                        <th>Student Name</th>
                        <th>Package Name</th>
                        <th style="text-align: right;">Date</th>
                        </thead>
                        <tbody>
                        <?php foreach($waitingList as $waiting):?>
                            <tr>
                                <td><?php echo $this->Html->link($waiting['Profile']['first_name'].' '. $waiting['Profile']['last_name'], array('controller' => 'users', 'action' => 'details', $waiting['User']['uuid']), array('class' => 'green'));?></td>
                                <td>
                                    <?php echo $this->Html->link($waiting['Package']['name'], array('controller' => 'packages', 'action' => 'view', $waiting['Package']['uuid']));?>
                                    <i class="italic-sm">(lesson no: <?php echo $waiting['Waiting']['lesson_no']?>)</i>
                                </td>
                                <td style="text-align: right;">
                                    <?php echo $this->Time->format('M d, Y', $waiting['Waiting']['start']);?>
                                    <span class="italic-sm">( <?php echo $this->Time->format('h:m a', $waiting['Waiting']['start'])?> - <?php echo $this->Time->format('h:m a', $waiting['Waiting']['end'])?>)</span>
                                </td>
                            </tr>
                        <?php endforeach;?>

                        </tbody>
                    </table>

                <?php else:?>
                    <?php echo $this->element('not-found')?>
                <?php endif;?>

                <div class="clearfix"></div>
            </div>
            <!----- /WAITING LIST ------>
        </div>

        <div class="col-lg-6 col-md-6 col-sm-6">

            <!----- ADD NEW CONTACT NOTE ------>
            <div class="d-box">
                <h4 class="d-title padding-bottom-10"><i class="fa fa-file-text danger"></i> Follow Up Note</h4>
                <div class="">
                    <?php echo $this->Form->create(
                        'Contact',
                        array('controller' => 'contacts', 'action' => 'note', 'class' => 'form-horizontal form-custom')
                    ); ?>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label>Choose One</label>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="noteFor" id="noteForStudent" value="student" checked="">
                                    Student
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="noteFor" id="noteForContact" value="contact">
                                    Lead
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label>Contact Name</label>
                            <input name="data[Note][contactPerson]" class="typeahead form-control" type="text" placeholder="Type name">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-6">
                            <label>Follow up date:</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                                <input id="datepicker" name="data[Note][note_date]" type="text" class="form-control" placeholder="Follow up date">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            </div>
                            <?php echo $this->Form->error('Note.note_date');?>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-6">
                            <div class="input-group">
                                <label>Rating:</label>
                                <select class="noteRatings" name="data[Note][rating]">
                                    <option value="" selected="selected"></option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                </select>
                            </div>
                            <?php echo $this->Form->error('Note.note_date');?>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label>Note:</label>
                            <?php
                            echo $this->Form->input(
                                'Note.note',
                                array(
                                    'type' => 'textarea',
                                    'class' => 'form-control',
                                    'placeholder' => 'Place your note here',
                                    'rows' => 5,
                                    'label' => false,
                                    'required' => false,
                                )
                            );
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-4">
                            <button class="btn btn-theme">Save Note</button>
                        </div>
                    </div>
                    <?php echo $this->Form->end(); ?>
                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>
            </div>
            <!----- /ADD NEW CONTACT NOTE ------>

            <!----- RECENT TRANSACTION ------>
            <div class="d-box">
                <h4 class="d-title padding-bottom-10">
                    <i class="fa fa-money danger"></i> Recent Five Transactions
                    <?php
                    if($recentTransaction){
                        echo $this->html->link('See Full List', array('controller' => 'reports', 'action' => 'transaction'), array('escape' => false, 'class' => 'full-list pull-right'));
                    }
                    ?>
                </h4>

                <?php if($recentTransaction):?>
                    <table class="table margin-top-10">
                        <thead>
                        <th>Student Name</th>
                        <th>Amount</th>
                        <th style="text-align: right;">Action</th>
                        </thead>
                        <tbody>
                        <?php foreach($recentTransaction as $transaction):?>
                            <tr>
                                <td>
                                    <?php echo $this->Html->link($transaction['Profile']['first_name'].' '.$transaction['Profile']['last_name'], array('controller' => 'users', 'action' => 'details', $transaction['User']['uuid']), array('class' => 'green')) ?>
                                </td>
                                <td>
                                    <?php echo $this->Number->currency($transaction['Billing']['amount']);?>
                                </td>
                                <td style="text-align: right;">
                                    <?php echo $this->Html->link('View', array('controller' => 'reports', 'action' => 'transaction_view', $transaction['Billing']['uuid']), array('class' => 'd-btn'));?>
                                </td>
                            </tr>
                        <?php endforeach;?>
                        </tbody>
                    </table>

                <?php else:?>
                    <?php echo $this->element('not-found')?>
                <?php endif;?>

                <div class="clearfix"></div>
            </div>
            <!----- /RECENT TRANSACTION ------>

            <!----- CANCELLED APPOINTMENT ------>
            <div class="d-box">
                <h4 class="d-title padding-bottom-10">
                    <i class="fa fa-ban danger"></i> Cancelled Appointments
                    <?php
                    if($cancelledAppointments){
                        echo $this->html->link('See Full List', array('controller' => 'appointments', 'action' => 'list/cancelled'), array('escape' => false, 'class' => 'full-list pull-right'));
                    }
                    ?>
                </h4>

                <?php if($cancelledAppointments):?>
                    <table class="table">
                        <thead>
                        <th>Appointment Time</th>
                        <th>Student Name</th>
                        <th>Instructor</th>
                        <th style="text-align: right;">Action</th>
                        </thead>
                        <tbody>
                        <?php foreach($cancelledAppointments as $appointment):?>
                            <tr>
                                <td>
                                    <?php echo $this->Time->format('d M, Y', $appointment['Appointment']['start'])?>
                                    <i class="italic-sm">(
                                        <?php echo $this->Time->format('h:i A', $appointment['Appointment']['start'])?>
                                        - <?php echo $this->Time->format('h:i A', $appointment['Appointment']['end'])?>
                                        )
                                    </i>
                                </td>
                                <td>
                                    <?php echo $this->Html->link($appointment[0]['studentName'], array('controller' => 'users', 'action' => 'details', $appointment['User']['uuid']), array('class' => 'green'));?>
                                </td>

                                <td style="text-align: right;">
                                    <?php echo $this->Html->link('View', array('controller' => 'appointments', 'action' => 'view', $appointment['Appointment']['id']), array('class' => 'd-btn'));?>
                                </td>
                            </tr>
                        <?php endforeach;?>

                        </tbody>
                    </table>

                    <div class="see-more">
                        <?php echo $this->html->link('See More <i class="fa fa-long-arrow-right"></i>', array('controller' => 'appointments', 'action' => 'list'), array('escape' => false))?>
                    </div>

                <?php else:?>
                    <?php echo $this->element('not-found')?>
                <?php endif;?>

                <div class="clearfix"></div>
            </div>
            <!----- /CANCELLED APPOINTMENT ------>



            <!----- BEST STUDENT CHART ------>
            <div class="d-box">
                <h4 class="d-title"><i class="fa fa-bar-chart-o purple-dark"></i> Top Five Student Based on Purchases Amount</h4>
                <?php if($bestStudentsChart):?>
                <div id="best_student"><?php $this->GoogleCharts->createJsChart($bestStudentsChart);?></div>
                <?php else:?>
                    <?php echo $this->element('not-found')?>
                <?php endif;?>
            </div>
            <!----- /BEST STUDENT CHART ------>

            <!----- BEST PACKAGES CHART ------>
            <div class="d-box">
                <h4 class="d-title"><i class="fa fa-bar-chart-o orange"></i> Top Five Packages Based on Purchases</h4>
                <?php if($bestPackagesChart):?>
                <div id="best_packages"><?php $this->GoogleCharts->createJsChart($bestPackagesChart);?></div>
                <?php else:?>
                    <?php echo $this->element('not-found')?>
                <?php endif;?>
            </div>
            <!----- /BEST PACKAGES CHART ------>

        </div>

    </div>
</div>



<?php echo $this->Html->script('typehead');?>
<script>
    // Instantiate the Bloodhound suggestion engine
    var contacts = new Bloodhound({
        datumTokenizer: function (datum) {
            return Bloodhound.tokenizers.whitespace(datum.value);
        },
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: {
            url: '<?php echo Router::url('/', true);?>instructor/contacts/getContacts',
            replace: function (url, query) {

                if($('#noteForStudent').is(':checked')){
                    q = url+'/student/'+query;
                }

                if($('#noteForContact').is(':checked')){
                    q = url+'/contact/'+query;
                }
                return q;
            },
            filter: function (contacts) {
                return $.map(contacts, function (contact) {
                    console.log(contact);
                    return {
                        value: contact['0']['contactName']
                    };
                });
            }
        }
    });

    // Initialize the Bloodhound suggestion engine
    contacts.initialize();
    // Instantiate the Typeahead UI
    $('.typeahead').typeahead(null, {
        name: 'states',
        displayKey: 'value',
        source: contacts.ttAdapter()
    });


    $('.noteRatings').barrating('show', {
        theme: 'fontawesome-stars',
        initialRating: null
    });
</script>



