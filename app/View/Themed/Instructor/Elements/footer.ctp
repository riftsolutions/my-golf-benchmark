<!-- UI - X Starts -->
<div class="ui-258">
    <!-- Icon -->
    <div class="ui-icon bg-lblue">
        <!-- Apple -->
        <a href="<?php echo Router::url('/', true);?>instructor/contacts/add" class="ui-tooltip" data-toggle="tooltip" data-placement="left" ><i class="fa fa-leaf"></i></a>
    </div>

    <!-- Icon -->
    <div class="ui-icon bg-lblue">
        <!-- Apple -->
        <a href="<?php echo Router::url('/', true);?>instructor/users/add" class="ui-tooltip" data-toggle="tooltip" data-placement="left"><i class="fa fa-user"></i></a>
    </div>

    <!-- Icon -->
    <div class="ui-icon bg-lblue">
        <!-- Apple -->
        <a href="<?php echo Router::url('/', true);?>instructor/packages/created" class="ui-tooltip" data-toggle="tooltip" data-placement="left"><i class="fa fa-plus-circle"></i></a>
    </div>

    <!-- Icon -->
    <div class="ui-icon bg-lblue">
        <!-- Apple -->
        <a href="<?php echo Router::url('/', true);?>instructor/packages/pos" class="ui-tooltip" data-toggle="tooltip" data-placement="left"><i class="fa fa-shopping-cart"></i></a>
    </div>

    <!-- Icon -->
    <div class="ui-icon bg-lblue">
        <!-- Apple -->
        <a href="<?php echo Router::url('/', true);?>instructor/calendars/index" class="ui-tooltip" data-toggle="tooltip" data-placement="left"><i class="fa fa-calendar"></i></a>
    </div>

    <a href="#" class="ui-btn bg-lblue">+</a>
</div>
<script>
    <!-- Tooltip -->
    $(function () {
        $('.ui-tooltip').tooltip();
    });
    <!-- Animation -->
    $(document).ready(function(){
        $("a.ui-btn").click(function(e){
            e.preventDefault();
            if(!($(this).hasClass("active"))){
                $(this).addClass("active");
                $(this).parent(".ui-258").addClass("active");
            }
            else{
                $(this).removeClass("active");
                $(this).parent(".ui-258").removeClass("active");
            }
        });
    });
</script>

<!----- FOOTER AREA ----->
<footer>
    <div class="footer-text">
        &copy; All Rights Reserved  •  Golf Swing Prescription
    </div>
</footer>
<!----- /FOOTER AREA ------>

<style>
    .bg-lblue {
        background-color: #6f8e40 !important;
    }
</style>
<!----- Java Script Area ----->
<?php /*echo $this->Html->script(array('jquery.dataTables.min', 'jquery.dataTables.columnFilter', 'datatables', 'bootstrap-datepicker', 'bootstrap-slider', 'custom', 'underscore-min'));*/?>
<?php echo $this->Html->script(array('bootstrap-datepicker', 'bootstrap-slider',  'custom'));?>
<!----- /Java Script Area ----->


</body>
</html>