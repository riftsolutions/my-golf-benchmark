<div class="pagination-area pull-left">
    <div class="margin-top-10 italic-md">
        <?php
        echo $this->Paginator->counter(
            'Page {:page} of {:pages}, showing {:current} records out of
             {:count} total'
        );
        ?>
    </div>
</div>

<div class="pagination-area pull-right">
    <ul class="pagination">
        <?php
        $this->Paginator->options(array(
                'update' => '#mainBodyArea',
                'evalScripts' => true,
            ));
        echo $this->Paginator->prev(
            __('prev'),
            array('tag' => 'li'),
            null,
            array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a')
        );
        echo $this->Paginator->numbers(
            array('separator' => '', 'currentTag' => 'a', 'currentClass' => 'active', 'tag' => 'li', 'first' => 1)
        );
        echo $this->Paginator->next(
            __('next'),
            array('tag' => 'li', 'currentClass' => 'disabled'),
            null,
            array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a')
        );
        ?>
    </ul>
</div>