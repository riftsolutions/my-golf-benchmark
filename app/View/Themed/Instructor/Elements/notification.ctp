<div class="row">

    <div class="col-lg-2 col-md-2">
        <h2 class="page-title">Dashboard</h2>
        <div class="clearfix"></div>
    </div>

    <div class="col-lg-10 col-md-10">
        <div class="notification-area">
            <ul class="notify">
                <li>
                    <div>
                        <a href="#">
                            <i class="fa fa-flask"></i>
                            <p class="italic-md">10 New Lesson</p>
                        </a>
                    </div>
                </li>
                <li>
                    <div>
                        <a href="#">
                            <i class="fa fa-male"></i>
                            <p class="italic-md">12 New Instructor</p>
                        </a>
                    </div>
                </li>
                <li>
                    <div>
                        <a href="#">
                            <i class="fa fa-user"></i>
                            <p class="italic-md">10 New Students</p>
                        </a>
                    </div>
                </li>
                <li>
                    <div>
                        <a href="#">
                            <i class="fa fa-money"></i>
                            <p class="italic-md">Today's Sales</p>
                        </a>
                    </div>
                </li>
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>

<hr class="shadow-line"/>