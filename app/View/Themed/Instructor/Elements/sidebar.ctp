<div class="col-xs-6 col-sm-2 sidebar-offcanvas" id="sidebar" role="navigation">
    <div class="row">
        <div class="sidebar">
            <ul id="menu" class="nav nav-list">
                <li>
                    <?php echo $this->Html->link('<i class="fa fa-home"></i> Dashboard
', array('controller' => 'dashboards', 'action' => 'index'), array('escape' => false))?>
                </li>
                <li>
                    <a href="#">
                        <i class="fa fa-calendar"></i>
                        Calendar
                        <span class="glyphicon arrow"></span>
                    </a>
                    <ul class="nav nav-list submenu collapse">
                        <li>
                            <?php echo $this->Html->link(
                                'My Calendar',
                                array('controller' => 'calendars', 'action' => 'index'),
                                array('escape' => false)
                            ); ?>
                        </li>
                        <li>
                            <?php echo $this->Html->link(
                                'Color Legend',
                                array('controller' => 'calendars', 'action' => 'legend'),
                                array('escape' => false)
                            ); ?>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#">
                        <i class="fa fa-mortar-board"></i>
                        Students
                        <span class="glyphicon arrow"></span>
                    </a>
                    <ul class="nav nav-list submenu collapse">
                        <li>
                            <?php echo $this->Html->link(
                                'Add New Student',
                                array('controller' => 'users', 'action' => 'add'),
                                array('escape' => false)
                            ); ?>
                        </li>
                        <li>
                            <?php echo $this->Html->link(
                                'Students List',
                                array('controller' => 'users', 'action' => 'student'),
                                array('escape' => false)
                            ); ?>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#">
                        <i class="fa fa-leaf"></i>
                        Leads
                        <span class="glyphicon arrow"></span>
                    </a>
                    <ul class="nav nav-list submenu">
                        <li>
                            <?php echo $this->Html->link('Add New Lead', array('controller' => 'contacts', 'action' => 'add'), array('escape' => false))?>
                        </li>
                        <li>
                            <?php echo $this->Html->link('Leads List', array('controller' => 'contacts', 'action' => 'index'), array('escape' => false))?>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#">
                        <i class="fa fa-file-text"></i>
                        Contacting Area
                        <span class="glyphicon arrow"></span>
                    </a>
                    <ul class="nav nav-list submenu">
                        <li>
                            <?php echo $this->Html->link('Contact List', array('controller' => 'contacts', 'action' => 'note_list'), array('escape' => false))?>
                        </li>
                        <li>
                            <?php echo $this->Html->link('Take Note', array('controller' => 'contacts', 'action' => 'note'), array('escape' => false))?>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#">
                        <i class="fa fa-certificate"></i>
                        My Golf Benchmark
                        <span class="glyphicon arrow"></span>
                    </a>
                    <ul class="nav nav-list submenu collapse">
                        <li>
                            <?php echo $this->Html->link(
                                'New Benchmark',
                                array('controller' => 'benchmarks', 'action' => 'new'),
                                array('escape' => false)
                            ); ?>
                        </li>
                        <li>
                            <?php echo $this->Html->link(
                                'Benchmark History',
                                array('controller' => 'benchmarks', 'action' => 'history'),
                                array('escape' => false)
                            ); ?>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#">
                        <i class="fa fa-gift"></i>
                        Items
                        <span class="glyphicon arrow"></span>
                    </a>
                    <ul class="nav nav-list submenu">
                        <li>
                            <?php echo $this->Html->link(
                                'Item List',
                                array('controller' => 'packages', 'action' => 'list'),
                                array('escape' => false)
                            ); ?>
                        </li>
                        <li>
                            <?php echo $this->Html->link(
                                'Create Item',
                                array('controller' => 'packages', 'action' => 'create'),
                                array('escape' => false)
                            ); ?>
                        </li>
                        <li>
                            <?php echo $this->Html->link(
                                'Assign Item',
                                array('controller' => 'packages', 'action' => 'assign'),
                                array('escape' => false)
                            ); ?>
                        </li>
                    </ul>
                </li>
                <li>
                    <?php echo $this->Html->link(
                        '<i class="fa fa-thumb-tack"></i> Point of Sale',
                        array('controller' => 'packages', 'action' => 'pos'),
                        array('escape' => false)
                    ); ?>
                </li>

                <li>
                    <a href="#">
                        <i class="fa fa-line-chart"></i>
                        Reports
                        <span class="glyphicon arrow"></span>
                    </a>
                    <ul class="nav nav-list submenu">
                        <li>
                            <?php echo $this->Html->link(
                                'Student Report',
                                array('controller' => 'reports', 'action' => 'student'),
                                array('escape' => false)
                            ); ?>
                        </li>
                        <li>
                            <?php echo $this->Html->link(
                                'Contact Report',
                                array('controller' => 'reports', 'action' => 'contact'),
                                array('escape' => false)
                            ); ?>
                        </li>
                        <li>
                            <?php echo $this->Html->link(
                                'Referral Sources',
                                array('controller' => 'reports', 'action' => 'sources'),
                                array('escape' => false)
                            ); ?>
                        </li>
                        <li>
                            <?php echo $this->Html->link(
                                'My Items',
                                array('controller' => 'reports', 'action' => 'my_package'),
                                array('escape' => false)
                            ); ?>
                        </li>
                        <li>
                            <?php echo $this->Html->link(
                                'Transactions',
                                array('controller' => 'reports', 'action' => 'transaction'),
                                array('escape' => false)
                            ); ?>
                        </li>
                        <li>
                            <?php echo $this->Html->link(
                                'Appointment',
                                array('controller' => 'reports', 'action' => 'appointment'),
                                array('escape' => false)
                            ); ?>
                        </li>
                        <li>
                            <?php echo $this->Html->link(
                                'Student Feedback',
                                array('controller' => 'reports', 'action' => 'feedback'),
                                array('escape' => false)
                            ); ?>
                        </li>
                    </ul>
                </li>

                <li>
                    <?php echo $this->Html->link(
                        '<i class="fa fa-reply"></i> Refund Money',
                        array('controller' => 'billings', 'action' => 'refund'),
                        array('escape' => false)
                    ); ?>
                </li>

                <li>
                    <a href="#">
                        <i class="fa fa-bell"></i>
                        Notification Settings
                        <span class="glyphicon arrow"></span>
                    </a>
                    <ul class="nav nav-list submenu">
                        <li>
                            <?php echo $this->Html->link(
                                'Email Notification',
                                array('controller' => 'notifications', 'action' => 'email'),
                                array('escape' => false)
                            ); ?>
                        </li>
                    </ul>
                </li>


                <li>
                    <a href="#">
                        <i class="fa fa-clock-o"></i>
                        My Appointments
                        <span class="glyphicon arrow"></span>
                    </a>
                    <ul class="nav nav-list submenu collapse">
                        <li>
                            <?php echo $this->Html->link(
                                'Appointment List',
                                array('controller' => 'appointments', 'action' => 'list'),
                                array('escape' => false)
                            ); ?>
                        </li>
                    </ul>
                </li>
                <li>
                    <?php echo $this->Html->link(
                        '<i class="fa fa-object-group"></i> My Group Lesson',
                        array('controller' => 'lessons', 'action' => 'group'),
                        array('escape' => false)
                    ); ?>
                </li>
                <li>
                    <?php echo $this->Html->link(
                        '<i class="fa fa-exclamation-triangle"></i> My Waiting List',
                        array('controller' => 'appointments', 'action' => 'waiting'),
                        array('escape' => false)
                    ); ?>
                </li>
                <li>
                    <a href="#">
                        <i class="fa fa-user"></i>
                        My Account
                        <span class="glyphicon arrow"></span>
                    </a>
                    <ul class="nav nav-list submenu">
                        <li>
                            <?php echo $this->Html->link(
                                'Profile',
                                array('controller' => 'profiles', 'action' => 'index'),
                                array('escape' => false)
                            ); ?>
                        </li>
                        <li>
                            <?php echo $this->Html->link(
                                'Update Profile',
                                array('controller' => 'profiles', 'action' => 'update'),
                                array('escape' => false)
                            ); ?>
                        </li>
                        <li>
                            <?php echo $this->Html->link(
                                'Change Password',
                                array('controller' => 'profiles', 'action' => 'change_password'),
                                array('escape' => false)
                            ); ?>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#">
                        <i class="fa fa-trash"></i>
                        Trash
                        <span class="glyphicon arrow"></span>
                    </a>
                    <ul class="nav nav-list submenu">
                        <li>
                            <?php echo $this->Html->link(
                                'Students',
                                array('controller' => 'trash', 'action' => 'user'),
                                array('escape' => false)
                            ); ?>
                        </li>
                        <li>
                            <?php echo $this->Html->link(
                                'Leads',
                                array('controller' => 'trash', 'action' => 'contact'),
                                array('escape' => false)
                            ); ?>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div>