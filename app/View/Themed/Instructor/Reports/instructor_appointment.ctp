<?php
/**
 * @var $this View
 */
?>
<h2 class="page-title">Report of Appointment</h2>
<hr class="shadow-line"/>

<div class="content">
    <div class="row">

        <div class="col-lg-12 col-md-6 col-sm-12">
            <!----- USER REFERRAL SOURCES ------>
            <div class="d-box">
                <h4 class="d-title"><i class="fa fa-repeat slate-blue"></i> Appointment Overview</h4>
                <div id="student_referral_sources"><?php $this->GoogleCharts->createJsChart($appointmentOverview); ?></div>
            </div>
            <!----- USER REFERRAL SOURCES ------>
        </div>


    </div>
</div>