<?php
/**
 * @var $this View
 */
?>
<h2 class="page-title">Contact Report</h2>
<hr class="shadow-line"/>
<div class="clearfix"></div>

<div class="margin-bottom-20">
    <div class="pull-left">
        <form class="form-inline"method="get">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                <input id="datepicker" name="fromDate" type="text" class="form-control" placeholder="From">
                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
            </div>
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                <input id="datepicker2" name="toDate" type="text" class="form-control" placeholder="Today (<?php echo date('m/d/Y')?>)">
                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
            </div>
            <button type="submit" class="btn btn-default">Search</button>
        </form>
    </div>
    <div class="pull-right">
        <?php $condition = serialize($conditions);?>
        <?php echo $this->Html->link('Export', array('controller' => 'reports', 'action' => 'export_contact?conditions='.$condition), array('class' => 'btn btn-theme'))?>
    </div>
</div>
<div class="clearfix"></div>

<?php if($contacts):?>
<div class="table-responsive margin-top-10">
    <table class="table data-table table-bordered sort-table" colspecing=0>
        <thead>
        <tr>
            <th style="width: 15%;">
                <?php echo $this->Paginator->sort('name', 'Contact Name'); ?>
            </th>
            <th style="width: 15%;">
                <?php echo $this->Paginator->sort('account_type', 'Type'); ?>
            </th>
            <th style="width: 15%;">
                <?php echo $this->Paginator->sort('email'); ?>
            </th>
            <th style="width: 13%;">
                <?php echo $this->Paginator->sort('phone'); ?>
            </th>
            <th style="width: 13%;">
                <?php echo $this->Paginator->sort('comments', 'Note'); ?>
            </th>
            <th style="width: 9%;">Action</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach($contacts as $contact):?>
            <tr>
                <td>
                    <?php echo $this->Html->link($contact['Contact']['name'], array('controller' => 'contacts', 'action' => 'edit', $contact['Contact']['id']));?>
                </td>
                <td>
                    <?php
                    if($contact['Contact']['account_type'] == 1){
                        echo 'Student';
                    }
                    elseif($contact['Contact']['account_type'] == 2){
                        echo 'Customer';
                    }
                    elseif($contact['Contact']['account_type'] == 3){
                        echo 'Lead/Prospect';
                    }
                    else{
                        echo 'N/A';
                    }
                    ?>
                </td>

                <td><?php echo $contact['Contact']['email']; ?></td>
                <td><?php echo $contact['Contact']['phone']?></td>
                <td><?php echo $contact['Contact']['comments']?></td>
                <td>
                    <div class="icons-area">
                        <?php echo $this->Html->link('Details', array('controller' => 'contacts', 'action' => 'view', $contact['Contact']['id']), array('escape' => false, 'class' => 'd-btn'));?>
                    </div>
                </td>
            </tr>
        <?php endforeach;?>
        </tbody>
    </table>
    <?php echo $this->element('pagination');?>
</div>
<?php else:?>
    <h4 class="not-found">Sorry, lead not found</h4>
<?php endif;?>