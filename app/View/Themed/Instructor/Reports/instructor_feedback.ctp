<?php
/**
 * @var $this View
 */
?>
<h2 class="page-title">Student Feedback</h2>
<hr class="shadow-line"/>
<div class="clearfix"></div>

<?php //var_dump($feedbacks); die();?>
<?php if ($feedbacks): ?>
    <div class="table-responsive">
        <table class="table data-table table-bordered sort-table" colspecing=0>
            <thead>
            <th>Student Name</th>
            <th>Rating</th>
            <th>Feedback</th>
            <th>Created</th>
            <th>Action</th>
            </thead>
            <tbody>
            <?php foreach ($feedbacks as $feedback): ?>
                <tr>
                    <td>
                        <?php
                        echo $this->Html->link($feedback['Profile']['first_name'] . ' ' . $feedback['Profile']['last_name'], array('controller' => 'users', 'action' => 'details', $feedback['User']['uuid']),
                            array('class' => 'green'));
                        ?>
                    </td>
                    <td>
                        <div class="form-group">
                            <div class="col-sm-6">
                                <div class="input-group">
                                    <select class="leadsRatings"">
                                    <option value="" selected="selected"></option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </td>
                    <td>
                        <?php if ($feedback['Feedback']['is_feedback'] == 1) { ?>

                            <?php
                            echo $this->Html->link($this->Text->excerpt($feedback['Feedback']['feedback'], null, 100, ' <span class="green">...more</span>'),
                                array('controller' => 'reports', 'action' => 'feedback_view/' . $feedback['Feedback']['id']), array('escape' => false));
                            ?>

                        <?php } elseif ($feedback['Feedback']['is_feedback'] == 0) { ?>
                            <b class="text-danger">No feedback given</b>
                        <?php } else { ?>
                            N/A
                        <?php } ?>
                    </td>
                    <td>
                        <span class="label label-info">
                        <?php
                        echo $this->Time->format('M d, Y h:m a', $feedback['Feedback']['created']);
                        ?>
                         </span>
                    </td>
                    <td>
                        <div class="icons-area">
 <?php                           echo $this->Html->link('<i class="fa fa-search-plus"></i>', array('controller' => 'reports', 'action' => 'feedback_view', $feedback['Feedback']['id']) ,array('escape' => false)) ; ?>

                        </div>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
        <?php echo $this->element('pagination'); ?>
    </div>
<?php else: ?>
    <h4 class="not-found">Sorry, users not found</h4>
<?php endif; ?>

<script type="text/javascript">
    $(function () {

        $('.leadsRatings').barrating('show', {
            initialRating: <?php echo $feedback['Feedback']['rating']; ?>,
            theme: 'fontawesome-stars',
            readonly: true
        });

    });
</script>