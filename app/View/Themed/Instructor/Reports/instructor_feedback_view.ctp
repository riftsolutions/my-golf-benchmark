<?php
/**
 * @var $this View
 */
?>
<h2 class="page-title">Feedback Details</h2>
<hr class="shadow-line"/>
<div class="pull-right">

    <?php echo $this->Html->link('Feedback List', array('controller' => 'reports', 'action' => 'feedback'), array('class' => 'btn btn-theme')) ?>
</div>
<div class="clearfix"></div>
<div class="row">
    <div class="col-md-8 col-md-offset-2 col-sm-12">
        <div class="box box-padding-adding radius-5 margin-top-25 margin-bottom-25">
            <h2 class="page-title">Student: <span
                    class="text-info"><?php echo $feedback['Profile']['first_name'] . ' ' . $feedback['Profile']['last_name'] ?></span>
            </h2>
            <ul class="d-list">
                <li>
                    <?php if ($feedback['Feedback']['feedback']): ?>
                        <strong>Feedback: </strong>
                        <p><?php echo $feedback['Feedback']['feedback']; ?></p>
                    <?php else: ?>
                        <b class="text-danger">No feedback given</b>
                    <?php endif; ?>
                </li>
                <li>
                    <strong>Rating: </strong>
                    <div class="input-group">
                        <select class="leadsRatings"">
                        <option value="" selected="selected"></option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        </select>
                    </div>
                </li>
                <li>
                    <strong>Created: </strong>
                    <span class="label label-info"><?php echo $this->Time->format('M d, Y h:m a', $feedback['Feedback']['created']); ?></span>
                </li>
            </ul>

        </div>
    </div>

</div>

<script type="text/javascript">
    $(function () {

        $('.leadsRatings').barrating('show', {
            initialRating: <?php echo $feedback['Feedback']['rating']; ?>,
            theme: 'fontawesome-stars',
            readonly: true
        });

    });
</script>