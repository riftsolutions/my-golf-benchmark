<?php
/**
 * @var $this View
 */
?>
<h2 class="page-title">Report of Referral Sources</h2>
<hr class="shadow-line"/>

<div class="content">
    <div class="row">

        <div class="col-lg-6 col-md-6 col-sm-6">
            <!----- USER REFERRAL SOURCES ------>
            <div class="d-box">
                <h4 class="d-title"><i class="fa fa-repeat slate-blue"></i> Student Referral Source</h4>
                <div id="student_referral_sources"><?php $this->GoogleCharts->createJsChart($studentReferralSource); ?></div>
            </div>
            <!----- USER REFERRAL SOURCES ------>
        </div>

        <div class="col-lg-6 col-md-6 col-sm-6">
            <!----- CONTACT REFERRAL SOURCES ------>
            <div class="d-box">
                <h4 class="d-title"><i class="fa fa-repeat slate-blue"></i> Contact Referral Source</h4>
                <div id="contact_referral_sources"><?php $this->GoogleCharts->createJsChart($contactReferralSource); ?></div>
            </div>
            <!----- CONTACT REFERRAL SOURCES ------>
        </div>

        <div class="col-lg-12 col-md-12 col-sm-12">
            <!----- CONTACT REFERRAL SOURCES ------>
            <div class="d-box">
                <h4 class="d-title"><i class="fa fa-repeat slate-blue"></i> Demo Graphics</h4>
                <html>
                <head>
                    <script type='text/javascript' src='https://www.google.com/jsapi'></script>
                    <script type='text/javascript'>
                        google.load('visualization', '1', {'packages': ['geochart']});
                        google.setOnLoadCallback(drawMarkersMap);

                        function drawMarkersMap() {
                            var data = google.visualization.arrayToDataTable([
                                ['City',   'Population', 'Area'],
                                ['Los Angeles',      2761477,    1285.31],
                                ['Long Beach',      2761477,    1285.31],
                                ['Glendale',      2761477,    1285.31],
                                ['Santa Clarita',      2761477,    1285.31],
                                ['Lancaster',      2761477,    1285.31],
                                ['El Monte',      2761477,    1285.31],
                                ['Torrance',      2761477,    1285.31],
                                ['Downey',      2761477,    1285.31],
                                ['Norwalk',      2761477,    1285.31]
                            ]);

                            var options = {
                                region: 'US',
                                displayMode: 'markers',
                                colorAxis: {colors: ['green', 'blue']}
                            };

                            var chart = new google.visualization.GeoChart(document.getElementById('chart_div'));
                            chart.draw(data, options);
                        };
                    </script>
                </head>
                <body>
                <div id="chart_div" style="width: 1400px; height: 500px;"></div>
                </body>
                </html>
            </div>
            <!----- CONTACT REFERRAL SOURCES ------>
        </div>
    </div>
</div>