<?php
/**
 * @var $this View
 */
?>
<h2 class="page-title">Student Report</h2>
<hr class="shadow-line"/>
<div class="clearfix"></div>

<?php if($users):?>
<div class="table-responsive">
    <table class="table data-table table-bordered sort-table" colspecing=0>
        <thead>
        <th>Instructor Name</th>
        <th>Student Name</th>
        <th>Email</th>
        <th>Phone</th>
        <th>Lessons purchased</th>
        <th>Lessons Remaining</th>
        <th>lessons scheduled</th>
        </thead>
        <tbody>
        <?php foreach($users as $user):?>
            <tr>
                <td>
                    <?php
                    echo $this->Html->link($user['0']['instructorName'], array('controller' => 'users', 'action' => 'details', $user['InstructorUser']['uuid']));
                    ?>
                </td>
                <td>
                    <?php
                    echo $this->Html->link($user['Profile']['name'], array('controller' => 'users', 'action' => 'details', $user['User']['uuid']));
                    ?>
                </td>
                <td>
                    <?php
                    echo $this->Html->link($user['User']['username'], array('controller' => 'users', 'action' => 'details', $user['User']['uuid']));
                    ?>
                </td>
                <td>
                    <?php
                    echo $this->Html->link($user['Profile']['phone'], array('controller' => 'users', 'action' => 'details', $user['User']['uuid']));
                    ?>
                </td>
                <td>
                    <?php
                   echo $user['0']['totalLesson'];
                    ?>
                </td>
                <td>
                    <?php
                    echo $user['0']['availableLesson'];
                    ?>

                </td>
                <td>
                    <?php
                    echo $user['0']['countCompletedLesson'];
                    ?>
                </td>
            </tr>
        <?php endforeach;?>
        </tbody>
    </table>
    <?php echo $this->element('pagination');?>
</div>
<?php else:?>
    <h4 class="not-found">Sorry, users not found</h4>
<?php endif;?>
