<?php
/**
 * @var $this View
 */
?>
<h2 class="page-title"><?php echo $title; ?></h2>
<hr class="shadow-line"/>
<div class="page-option">
    <div class="btn-toolbar pull-left" role="toolbar" aria-label="...">
        <div class="btn-group" role="group" aria-label="...">
            <div class="dropdown pull-left page-option-left">
                <button class="btn btn-info dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown">
                    Quick Navigation
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                    <li role="presentation">
                        <?php echo $this->Html->link('Default Item (' . $packageOverview[0][0]['defaultPackage'] . ')',
                            array('controller' => 'packages', 'action' => 'list/default?sid=' . $sid)); ?>
                    </li>
                    <li role="presentation" class="divider"></li>
                    <li role="presentation">
                        <?php echo $this->Html->link('Your Created Item (' . $packageOverview[0][0]['customPackage'] . ')',
                            array('controller' => 'packages', 'action' => 'list/users?sid=' . $sid)); ?>
                    </li>
                    <li role="presentation" class="divider"></li>
                    <li role="presentation">
                        <?php echo $this->Html->link('Visible Item (' . $packageOverview[0][0]['visiblePackage'] . ')',
                            array('controller' => 'packages', 'action' => 'list/visible?sid=' . $sid)); ?>
                    </li>
                    <li role="presentation" class="divider"></li>
                    <li role="presentation">
                        <?php echo $this->Html->link('Invisible Item (' . $packageOverview[0][0]['invisiblePackage'] . ')',
                            array('controller' => 'packages', 'action' => 'list/invisible?sid=' . $sid)); ?>
                    </li>
                    <li role="presentation" class="divider"></li>
                    <li role="presentation">
                        <?php echo $this->Html->link('All Item (' . $packageOverview[0][0]['totalPackage'] . ')',
                            array('controller' => 'packages', 'action' => 'list?sid=' . $sid)); ?>
                    </li>
                </ul>
            </div>

        </div>
        <div class="btn-group" role="group" aria-label="...">
            <button type="button" class="btn btn-theme btn-theme " data-toggle="modal" data-target="#searchItemModal">
                Search Item
            </button>

        </div>
    </div>


    <div class="pull-right page-option-right">
        <?php echo $this->Html->link('Create New Item', array('controller' => 'packages', 'action' => 'create'),
            array('class' => 'btn btn-theme')) ?>

        <?php echo $this->Html->link('Item List', array('controller' => 'packages', 'action' => 'list'),
            array('class' => 'btn btn-theme')) ?>
    </div>
</div>

<div class="clearfix"></div>
<br/>
<div class="table-responsive">
    <?php if ($packages): ?>
        <table class="table data-table table-bordered sort-table" colspecing=0>
            <thead>
            <tr>
                <th><?php echo $this->Paginator->sort('Package.name', 'Name of Item'); ?></th>
                <th><?php echo $this->Paginator->sort('Package.price', 'Price'); ?></th>
                <th><?php echo $this->Paginator->sort('Package.tax', 'Tax'); ?></th>
                <th><?php echo $this->Paginator->sort('Package.total', 'Total'); ?></th>
                <th><?php echo $this->Paginator->sort('Package.lesson', 'Number of Sessions'); ?></th>
                <th><?php echo $this->Paginator->sort('Package.expiration_days', 'Expiration Days'); ?></th>
                <th><?php echo $this->Paginator->sort('Package.status', 'Visibility'); ?></th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($packages as $package): ?>
                <tr>
                    <td>
                        <?php
                        if ($package['Package']['pay_at_facility'] == 0) {
                            echo $this->Html->link($package['Package']['name'],
                                array('controller' => 'packages', 'action' => 'view', $package['Package']['uuid']),
                                array('class' => 'green'));
                        } else {
                            echo $package['Package']['name'];
                        }

                        ?>
                        <?php
                        if ($package['Package']['pay_at_facility'] == 1) {
                            echo '<label class="label label-danger">Pay at Facility</label>';
                        } elseif ($package['Package']['is_default'] == 1) {
                            echo '<label class="label label-info">default</label>';
                        } else {
                            echo '<label class="label label-success">custom</label>';
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if ($package['Package']['price']) {
                            echo $this->Number->currency($package['Package']['price']);
                        } else {
                            echo 'N/A';
                        }
                        ?>
                    </td>
                    <td>
                        <?php if ($package['Package']['tax']): ?>
                            <?php echo $this->Number->currency($package['Package']['tax']); ?>
                            <i class="italic-md u-l">(Tax <?php echo $package['Package']['tax_percentage'] ?>%)</i>
                        <?php else: ?>
                            N/A
                        <?php endif; ?>
                    </td>
                    <td>
                        <?php
                        if ($package['Package']['total']) {
                            echo $this->Number->currency($package['Package']['total']);
                        } else {
                            echo 'N/A';
                        }
                        ?>
                    </td>
                    <td>
                        <?php echo $package['Package']['lesson']; ?>
                    </td>
                    <td>
                        <?php echo $package['Package']['expiration_days']; ?>
                    </td>
                    <td>
                        <?php
                        if ($package['Package']['status'] == 0) {
                            echo '<label class="label label-danger">Invisible</label>';
                        } elseif ($package['Package']['status'] == 1) {
                            echo '<label class="label label-success">Visible</label>';
                        } else {
                            echo '<label class="label label-warning">N/A</label>';
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if ($package['Package']['is_default'] == 0) {
                            echo $this->Html->link('Edit',
                                array('controller' => 'packages', 'action' => 'edit', $package['Package']['uuid']),
                                array('class' => 'btn d-btn'));
                        }
                        ?>

                        <?php
                        if ($package['Package']['pay_at_facility'] == 0) {
                            echo $this->Html->link('View',
                                array('controller' => 'packages', 'action' => 'view', $package['Package']['uuid']),
                                array('class' => 'btn d-btn'));
                            /*echo $this->Html->link('Assign to Student', array('controller' => 'packages', 'action' => 'assign', $package['Package']['uuid']), array('class' => 'btn d-btn'));*/
                        }

                        ?>
                        <?php
                        if ($package['Package']['is_default'] == 0) {
                            echo $this->Form->postLink(
                                'Delete',
                                array(
                                    'controller' => 'packages',
                                    'action' => 'delete',
                                    $package['Package']['uuid']
                                ),
                                array(
                                    'class' => 'btn d-btn',
                                    'escape' => false
                                ),
                                __('Are you sure you want to delete this item')
                            );
                        }
                        ?>

                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
        <?php echo $this->element('pagination'); ?>
    <?php else: ?>
        <h2 class="not-found">Sorry, item not found</h2>
        <hr class="shadow-line"/>
    <?php endif; ?>
</div>

<!-- Modal For Search Item -->
<div class="modal event-modal fade" id="searchItemModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="border-bottom: 1px solid #e5e5e5;">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                </button>
                <h4 class="sm-title">Search Items</h4>
            </div>
            <div class="modal-body">
                <!----- ADD NEW CONTACT NOTE ------>
                <div class="">
                    <form action="<?php echo Router::url('/', true); ?>instructor/packages/list"
                          class="form-horizontal form-custom" method="get" accept-charset="utf-8">
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label>Item Name:</label>
                                <input name="item_name" class="form-control" type="text"
                                       placeholder="Type an item name">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label>Item Amount</label>
                                <select name="amount" class="form-control">
                                    <option>Choose Amount</option>
                                    <option value="100">$100+</option>
                                    <option value="500">$500+</option>
                                    <option value="1000">$1000+</option>
                                    <option value="2000">$2000+</option>
                                    <option value="5000">$5000+</option>
                                    <option value="10000">$10000+</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-12"><label>Item Amount Range</label></div>
                            <div class="col-lg-6">
                                <input name="starting_amount" class="form-control" type="text"
                                       placeholder="Starting Item Amount">
                            </div>
                            <div class="col-lg-6">
                                <input name="ending_amount" class="form-control" type="text"
                                       placeholder="Ending Item Amount">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-lg-12"><label>Item Type</label></div>
                            <div class="col-lg-12">
                                <select name="type" id="" class="form-control" aria-disabled="true">
                                    <option value="" selected>Choose Item Type</option>
                                    <option value="1">Default</option>
                                    <option value="0">Custom</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-lg-12"><label>Visibility</label></div>

                            <div class="col-lg-12">
                                <label class="radio-inline">
                                    <input type="radio" name="status" id="inlineRadio1" value="" checked="checked"> All
                                </label>

                                <label class="radio-inline">
                                    <input type="radio" name="status" id="inlineRadio2" value="1"> Visible
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" name="status" id="inlineRadio3" value="0"> Invisible
                                </label>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-4">
                                <button class="btn btn-theme">Search</button>
                            </div>
                        </div>
                        <?php echo $this->Form->end(); ?>
                        <div class="clearfix"></div>
                </div>
                <!----- /ADD NEW CONTACT NOTE ------>
            </div>
        </div>
    </div>
</div>
<!-- /Modal For Displaying Schedule -->

<div class="clearfix"></div>