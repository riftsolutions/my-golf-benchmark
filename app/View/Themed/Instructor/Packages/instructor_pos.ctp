<?php
/**
 * @var $this View
 */
?>
<h2 class="page-title">Point of Sale</h2>
<hr class="shadow-line"/>
<?php
$isHidden = "style='display:none'";
if($selectedPackagesIds){
    $isHidden = "style='display:visible'";
}
?>

<span id="purchase-btn" <?php echo $isHidden; ?>>
    <?php
    echo $this->Html->link('Purchase Items', array('controller' => 'billings', 'action' => 'checkout'), array('class' => 'btn btn-theme pull-right margin-bottom-10 margin-left-10'));

    echo $this->Html->link('Remove all Items', array('controller' => 'packages', 'action' => 'remove_items'), array('class' => 'btn btn-red pull-right margin-bottom-10'));
    ?>
</span>
<div class="clearfix"></div>

<div class="table-responsive">
    <?php if ($packages): ?>
        <table class="table data-table table-bordered sort-table" id="table">
            <thead>
            <tr>
                <th>Select</th>
                <th><?php echo $this->Paginator->sort('Package.name', 'Name of Item'); ?></th>
                <th><?php echo $this->Paginator->sort('Package.price', 'Price'); ?></th>
                <!--<th><?php /*echo $this->Paginator->sort('Package.tax', 'Tax'); */?></th>
                <th><?php /*echo $this->Paginator->sort('Package.total', 'Total'); */?></th>-->
                <th style="width: 10%"><?php echo $this->Paginator->sort('Package.lesson', 'Number of Sessions'); ?></th>
                <th style="width: 10%"><?php echo $this->Paginator->sort('Package.expiration_days', 'Expiration Days'); ?></th>
            </tr>
            </thead>
            <tbody class="sortable">
            <?php foreach($packages as $package):?>
                <tr id="<?php echo $currentPageNo.'_'.$package['PackagesOrder']['id']?>">
                    <td>
                        <?php
                        $selectedValue = 0;
                        $selectBtn = 'd-btn';
                        $packageText = 'select';
                        if($selectedPackagesIds){
                            if(in_array($package['Package']['id'], $selectedPackagesIds)){
                                $selectedValue = 1;
                                $selectBtn = 'd-btn-green';
                                $packageText = 'deselect';
                            }
                        }
                        ?>
                        <a class="btn <?php echo $selectBtn;?> select-package" is-selected="<?php echo $selectedValue;?>" package-uuid="<?php echo $package['Package']['uuid'];?>"><?php echo $packageText?></a>
                    </td>
                    <td>
                        <?php 
                        echo $this->Html->link($package['Package']['name'], array('controller' => 'packages', 'action' => 'pos_view', $package['Package']['uuid']), array('class' => 'green'));
                        //echo $this->Html->link($package['Package']['name'], array('controller' => 'packages', 'action' => 'view', $package['Package']['uuid']), array('class' => 'green'));
                        ?>
                    </td>
                    <td>
                        <?php echo $this->Number->currency($package['Package']['price']);?>
                    </td>
                    <!--<td>
                        <?php /*echo $this->Number->currency($package['Package']['tax']);*/?>
                        <i class="italic-md u-l">(Tax <?php /*echo $package['Package']['tax_percentage']*/?>%)</i>
                    </td>
                    <td>
                        <?php /*echo $this->Number->currency($package['Package']['total']);*/?>
                    </td>-->
                    <td>
                        <?php echo $package['Package']['lesson'];?>
                    </td>
                    <td>
                        <?php echo $package['Package']['expiration_days'];?>
                    </td>
                </tr>
            <?php endforeach;?>
            </tbody>
        </table>
        <?php echo $this->element('pagination');?>
    <?php else: ?>
        <h2 class="not-found">Sorry, point of sales empty</h2>
        <hr class="shadow-line"/>
    <?php endif; ?>
</div>
<div class="clearfix"></div>


<script>
    $(document).on('click', '.select-package', function(){
        var selectPackage = $(this);
        var packageUUID = $(this).attr('package-uuid');
        var isSelected = $(this).attr('is-selected');
        var packageInfo = {'packageUUID': packageUUID, 'action': isSelected};

        if(isSelected == 0){
            $(this).removeClass('d-btn');
            $(this).addClass('d-btn-green');
            $(this).attr('is-selected', 1);
            $(this).text('deselect');
            $('#purchase-btn').show();
        }
        else if(isSelected == 1){
            $(this).removeClass('d-btn-green');
            $(this).addClass('d-btn');
            $(this).attr('is-selected', 0);
            $(this).text('select');
        }

        $.ajax({
            url: '<?php echo Router::url('/', true);?>instructor/packages/choosePackage',
            type: "POST",
            dataType: "json",
            data: {'packageInfo': packageInfo},
            success: function (data) {
                console.log(data);
                if(data == 1){
                    $(selectPackage).parent().closest('tr').addClass('success');
                    manageFlashMessage('alert-success', 'Item has been selected for purchases')
                }
                else if(data == 0){
                    $(selectPackage).parent().closest('tr').removeClass('success');
                    manageFlashMessage('alert-danger', 'Item has been removed form selected package list')
                }
            },
            error: function (xhr, status, error) {
                console.log(error);
            }
        });

        $.ajax({
            url: '<?php echo Router::url('/', true);?>instructor/packages/countPackage',
            type: "POST",
            dataType: "json",
            success: function (data) {
                $('.cartBtn .badge-notify').text(data);
                if(data > 0)
                {
                    $('#purchase-btn').show();
                    $('.cartBtn').show();
                }
                else{
                    $('#purchase-btn').hide();
                    $('.cartBtn').hide();
                }
            },
            error: function (xhr, status, error) {
                console.log(error);
            }
        });
    });
</script>

<?php echo $this->Html->script(array('tablednd'))?>

<?php echo $this->Html->script('jquery.block.ui')?>

<script>
    $(document).ajaxStop($.unblockUI);
    $(document).ready(function () {
        $('#table').tableDnD({
            onDrop: function(table, row) {
                var orders = $.tableDnD.serialize();
                $.blockUI(
                    {
                        message: '<h1><img src="<?php echo Router::url('/', true);?>theme/Instructor/img/busy.gif" /> ' + 'Just a moment...</h1>'
                    }
                );
                $.ajax({
                    url: '<?php echo Router::url('/', true);?>/instructor/packages/arrangePOSOrder',
                    type: "POST",
                    dataType: "json",
                    data: {'orders': decodeURI(orders)},
                    success: function (data) {
                        console.log(data);
                        manageFlashMessage('alert-success', 'Order has been updated')
                    },
                    error: function (xhr, status, error) {
                        manageFlashMessage('alert-danger', 'Sorry, something went wrong')
                    }
                });

            }
        });

    });
</script>




