<?php
/**
 * @var $this View
 */
?>
<h2 class="page-title">Assign Item</h2>
<hr class="shadow-line"/>

<div class="pull-right">
    <?php echo $this->Html->link('Create New Item', array('controller' => 'packages', 'action' => 'create'), array('class' => 'btn btn-theme'))?>

    <?php echo $this->Html->link('Item List', array('controller' => 'packages', 'action' => 'list'), array('class' => 'btn btn-theme'))?>
</div>
<div class="clearfix"></div>

<div class="row">
    <div class="col-md-8 col-md-offset-2 col-sm-12">
        <div class="box box-padding-adding radius-5 margin-top-25 margin-bottom-25">
            <?php echo $this->Form->create(
                'Package',
                array(
                    'controller' => 'packages',
                    'action' => 'assign/'.$packageID.'/'.$userID,
                    'type' => 'file',
                    'method' => 'post',
                    'class' => 'form-horizontal form-custom'
                )
            ); ?>
            <div class="form-group">
                <label class="col-sm-4 control-label">Student</label>

                <div class="col-sm-8">
                    <?php
                    echo $this->Form->input(
                        'PackagesUser.created_for',
                        array(
                            'type' => 'select',
                            'class' => 'form-control',
                            'label' => false,
                            'div' => false,
                            'required' => false,
                            'options' => $studentList,
                            'empty' => 'Select Student',
                            'value' => $userID
                        )
                    )
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4 control-label">Item</label>

                <div class="col-sm-8">
                    <?php
                    echo $this->Form->input(
                        'PackagesUser.package_id',
                        array(
                            'type' => 'select',
                            'class' => 'form-control',
                            'label' => false,
                            'div' => false,
                            'required' => false,
                            'options' => $packages,
                            'empty' => 'Select Package',
                            'value' => $packageID
                        )
                    );
                    ?>
                </div>
            </div>


            <div class="form-group">
                <div class="col-sm-12">
                    <button class="btn btn-theme pull-right">Assign Item</button>
                </div>
            </div>
            <?php echo $this->Form->end(); ?>
        </div>
    </div>
</div>