<?php
/**
 * @var $this View
 */
?>
    <h2 class="page-title"><?php echo $title;?></h2>
    <hr class="shadow-line"/>
<div class="page-option">
    <div class="dropdown margin-bottom-5 page-option-left">
        <button class="btn btn-info dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown">
            Quick Navigation
            <span class="caret"></span>
        </button>
        <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
            <li role="presentation">
                <?php echo $this->Html->link('All', array('controller' => 'appointments', 'action' => 'waiting?sid='.$sid));?>
            </li>
            <li role="presentation" class="divider"></li>
            <li role="presentation">
                <?php echo $this->Html->link('Waiting', array('controller' => 'appointments', 'action' => 'waiting/waiting?sid='.$sid));?>
            </li>
            <li role="presentation" class="divider"></li>
            <li role="presentation">
                <?php echo $this->Html->link('Expired', array('controller' => 'appointments', 'action' => 'waiting/expired?sid='.$sid));?>
            </li>
            <li role="presentation" class="divider"></li>
            <li role="presentation">
                <?php echo $this->Html->link('Cancelled', array('controller' => 'appointments', 'action' => 'waiting/cancelled?sid='.$sid));?>
            </li>
        </ul>
        <button type="button" class="btn btn-theme btn-lg margin-left-15" data-toggle="modal" data-target="#searchWaitingListModal">
            Search
        </button>
        <a class="btn btn-theme text-uppercase" data-toggle="modal" data-target="#waitingListModal2"> Join Waitlist</a>

    </div>
</div>

<?php if($appointments):?>
    <div class="table-responsive">
        <table class="table data-table table-bordered sort-table">
            <thead>
            <tr>
                <th><?php echo $this->Paginator->sort('Instructor.first_name', 'Student Name'); ?></th>
                <th><?php echo $this->Paginator->sort('Package.name', 'Package Name'); ?></th>
                <th><?php echo $this->Paginator->sort('Waiting.lesson_no', 'Lesson No'); ?></th>
                <th><?php echo $this->Paginator->sort('Waiting.start', 'Appointment Data and Time'); ?></th>
                <th><?php echo $this->Paginator->sort('Waiting.status', 'Status'); ?></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($appointments as $appointment):?>
                <tr>
                    <td>
                        <?php echo $this->Html->link($appointment['Instructor']['first_name']. ' '. $appointment['Instructor']['last_name'], array('controller' => 'users', 'action' => 'details', $appointment['InstructorUser']['uuid']));?>
                    </td>
                    <td>
                        <?php echo $this->Html->link($appointment['Package']['name'], array('controller' => 'packages', 'action' => 'view', $appointment['Package']['uuid']));?>
                    </td>
                    <td>
                        <?php echo $appointment['Waiting']['lesson_no']; ?>
                    </td>
                    <td>
                        <?php echo $this->Time->format('M d, Y', $appointment['Waiting']['start']);?>
                        <span class="italic-sm">( <?php echo $this->Time->format('h:m a', $appointment['Waiting']['start'])?> - <?php echo $this->Time->format('h:m a', $appointment['Waiting']['end'])?>)</span>
                    </td>
                    <td>
                        <?php
                        if($appointment['Waiting']['status'] == 1){
                            echo '<label class="label label-default">waiting</label>';
                        }
                        elseif($appointment['Waiting']['status'] == 2){
                            echo '<label class="label label-info">awaiting for confirmation</label>';
                        }
                        elseif($appointment['Waiting']['status'] == 3){
                            echo '<label class="label label-success">converted into appointment</label>';
                        }
                        elseif($appointment['Waiting']['status'] == 4){
                            echo '<label class="label label-danger">expired</label>';
                        }
                        elseif($appointment['Waiting']['status'] == 5){
                            echo '<label class="label label-warning">cancelled</label>';
                        }
                        elseif($appointment['Waiting']['status'] == 6){
                            echo '<label class="label label-danger">invalid</label>';
                        }
                        else{
                            echo '<label class="label label-danger">N/A</label>';
                        }
                        ?>
                    </td>
                </tr>
            <?php endforeach;?>
            </tbody>
        </table>
        <?php echo $this->element('pagination')?>
    </div>
<?php else:?>
    <h4 class="not-found"><?php echo $notFount; ?></h4>
<?php endif;?>

<!-- Modal For Displaying Schedule -->
<div class="modal event-modal fade" id="searchWaitingListModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="border-bottom: 1px solid #e5e5e5;">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                </button>
                <h4 class="sm-title">Search Waiting List</h4>
            </div>
            <div class="modal-body">
                <!----- ADD NEW CONTACT NOTE ------>
                <div class="">

                    <form action="<?php echo Router::url('/', true);?>instructor/appointments/waiting" class="form-horizontal form-custom" method="get" accept-charset="utf-8">
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label>Student name:</label>
                            <input name="student" class="typeahead form-control" type="text" placeholder="Type student name">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label>Package:</label>
                            <input name="package" class="packageTypeahead form-control" type="text" placeholder="Type package name">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-12"><label>Date Range</label></div>
                        <div class="clearfix"></div>
                        <div class="col-sm-6">
                            <div class="input-group">
                                <input id="datepicker" name="appointment_starting_date" type="text" class="form-control" placeholder="Appointment Starting">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="input-group">
                                <input id="datepicker2" name="appointment_ending_date" type="text" class="form-control" placeholder="Appointment Ending">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-12"><label>Time Range</label></div>
                        <div class="col-lg-6">
                            <div class="input-group">
                                <select name="appointment_starting_time" class="form-control" id="GroupLessonStart">
                                    <?php echo $this->Utilities->getFromHours('Starting Time');?>
                                </select>
                                <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="input-group">
                                <select name="appointment_ending_time" class="form-control" id="GroupLessonEnd">
                                    <?php echo $this->Utilities->getFromHours('Ending Time');?>
                                </select>
                                <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-4">
                            <button class="btn btn-theme">Search</button>
                        </div>
                    </div>
                    <?php echo $this->Form->end(); ?>
                    <div class="clearfix"></div>
                </div>
                <!----- /ADD NEW CONTACT NOTE ------>
            </div>
        </div>
    </div>
</div>
<!-- /Modal For Displaying Schedule -->
<!-- Modal For range Waiting List -->
<div class="modal event-modal fade" id="waitingListModal2" tabindex="-1" role="dialog"
     aria-labelledby="setApointmentModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <form action="<?php echo Router::url('/', true); ?>calendars/setRangeWaitingList" method="post" class="form-horizontal form-custom">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                    </button>
                    <h4 class="sm-title">RESERVE TIMES ON THE WAITING LIST </h4>
                </div>
                <div class="modal-body">

                    <div class="form-group">
                        <div class="col-lg-6">
                            <label for="exampleInputEmail1">Start Date:</label>

                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                                <input id="datepicker3" name="fromDate" type="text" class="form-control"
                                       placeholder="From">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            </div>


                        </div>
                        <div class="col-lg-6">
                            <label for="exampleInputEmail1">End Date:</label>

                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                                <input id="datepicker4" name="toDate" type="text" class="form-control"
                                       placeholder="To">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-lg-12"><label>Time Range</label></div>
                        <div class="col-lg-6">
                            <div class="input-group">
                                <select name="waiting_starting_time" class="form-control" id="GroupLessonStart">
                                    <?php echo $this->Utilities->getFromHours('Starting Time');?>
                                </select>
                                <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="input-group">
                                <select name="waiting_ending_time" class="form-control" id="GroupLessonEnd">
                                    <?php echo $this->Utilities->getFromHours('Ending Time');?>
                                </select>
                                <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label>Student name:</label>
                            <input name="studentID" class="typeahead form-control" type="text" placeholder="Type student name">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-4">
                            <button type="submit" class="btn btn-theme pull-left">Submit</button>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <?php echo $this->Form->end(); ?>
    </div>
</div>
<!-- Modal For range Waiting List -->

<?php echo $this->Html->script('typehead');?>

<script>
    // Instantiate the Bloodhound suggestion engine
    var contacts = new Bloodhound({
        datumTokenizer: function (datum) {
            return Bloodhound.tokenizers.whitespace(datum.value);
        },
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: {
            url: '<?php echo Router::url('/', true);?>instructor/contacts/getContacts/student/%QUERY',
            filter: function (contacts) {
                return $.map(contacts, function (contact) {
                    console.log(contact);
                    return {
                        value: contact['0']['contactName']
                    };
                });
            }
        }
    });

    contacts.initialize();
    // Instantiate the Typeahead UI
    $('.typeahead').typeahead(null, {
        name: 'states',
        displayKey: 'value',
        source: contacts.ttAdapter()
    });

    // Instantiate the Bloodhound suggestion engine
    var packages = new Bloodhound({
        datumTokenizer: function (datum) {
            return Bloodhound.tokenizers.whitespace(datum.value);
        },
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: {
            url: '<?php echo Router::url('/', true);?>instructor/packages/getPackages/%QUERY',
            filter: function (packages) {
                return $.map(packages, function (package) {
                    return {
                        value: package['Package']['name']
                    };
                });
            }
        }
    });

    console.log(packages);
    packages.initialize();
    $('.packageTypeahead').typeahead(null, {
        name: 'states',
        displayKey: 'value',
        source: packages.ttAdapter()
    });

</script>