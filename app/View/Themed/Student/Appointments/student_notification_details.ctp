<?php
/**
 * @var $this View
 */
?>
<h2 class="page-title">Appointment Details</h2>
<hr class="shadow-line"/>
<div class="row">
    <div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-12">
        <div class="clearfix"></div>
        <div class="d-box-md margin-top-25">
            <h4 class="d-title"><i class="fa fa-filter purple"></i> Appointment Details</h4>
            <ul class="d-list">
                <li>
                    <strong>Package Name:</strong>
                    <?php
                    echo $this->Html->link($waiting['Package']['name'], array('controller' => 'packages', 'action' => 'view', $waiting['Package']['uuid']));
                    ?>
                </li>
                <li>
                    <strong>Lesson No:</strong>
                    <?php
                    echo $waiting['Waiting']['lesson_no'];
                    ?>
                </li>
                <li>
                    <strong>Instructor Name:</strong>
                    <?php
                    echo $this->Html->link($waiting['Instructor']['first_name']. ' '. $waiting['Instructor']['last_name'], array('controller' => 'users', 'action' => 'details', $waiting['InstructorUser']['uuid']));
                    ?>
                </li>
                <li>
                    <strong>Appointment Time:</strong>
                    <?php echo $this->Time->format('d M, Y', $waiting['Waiting']['start']) ?>
                    <i class="italic-sm">
                        (<?php
                        echo $this->Time->format('h:m a', $waiting['Waiting']['start']). ' - '. $this->Time->format('h:m a', $waiting['Waiting']['end']);
                        ?>)
                    </i>
                </li>
            </ul>
            <br/>
            <?php
            if($waiting['Waiting']['status'] == 2){

                echo $this->Form->postLink(
                    'Cancel Appointment',
                    array(
                        'controller' => 'appointments',
                        'action' => 'confirmAppointment',
                        $waiting['Waiting']['uuid'],
                    ),
                    array(
                        'class' => 'btn btn-theme',
                        'escape' => false
                    ),
                    __('Are you sure you want to confirm this appointment')
                );
            }
            ?>
        </div>
    </div>
</div>