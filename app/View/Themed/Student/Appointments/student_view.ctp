<?php
/**
 * @var $this View
 */
?>
<h2 class="page-title">Appointment Details</h2>
<hr class="shadow-line"/>
<div class="row">
    <div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-12">
        <div class="pull-right">
            <?php echo $this->Utilities->appointmentStatusButton($appointmentDetails['Appointment']['status']);?>
        </div>
        <div class="clearfix"></div>
        <div class="d-box-md margin-top-25">
            <h4 class="d-title"><i class="fa fa-filter purple"></i> Appointment Details</h4>
            <ul class="d-list">
                <li>
                    <strong>Student Name:</strong>
                    <?php
                    echo $this->Html->link($appointmentDetails['Profile']['first_name']. ' '. $appointmentDetails['Profile']['last_name'], array('controller' => 'users', 'action' => 'details', $appointmentDetails['User']['uuid']));
                    ?>
                </li>
                <li>
                    <strong>Email:</strong>
                    <?php
                    echo $appointmentDetails['User']['username'];
                    ?>
                </li>
                <li>
                    <strong>Phone:</strong>
                    <?php
                    echo $appointmentDetails['Profile']['phone'];
                    ?>
                </li>
                <li>
                    <strong>City:</strong>
                    <?php
                    echo $appointmentDetails['Profile']['first_name'];
                    ?>
                </li>
                <li>
                    <strong>State:</strong>
                    <?php
                    echo $appointmentDetails['Profile']['state'];
                    ?>
                </li>
                <li>
                    <strong>Postal Code:</strong>
                    <?php
                    echo $appointmentDetails['Profile']['postal_code'];
                    ?>
                </li>
                <li>
                    <strong>Appointment Date:</strong>
                    <?php
                    echo $this->Time->format('M d, Y', $appointmentDetails['Appointment']['start']);
                    ?>
                </li>
                <li>
                    <strong>Appointment Time:</strong>
                    <?php
                    echo $this->Time->format('h:m a', $appointmentDetails['Appointment']['start']). ' - '. $this->Time->format('h:m a', $appointmentDetails['Appointment']['end']);
                    ?>
                </li>
                <li>
                    <strong>Instructor Name:</strong>
                    <?php
                    echo $this->Html->link($appointmentDetails['Instructor']['first_name']. ' '. $appointmentDetails['Instructor']['last_name'], array('controller' => 'users', 'action' => 'details', $appointmentDetails['InstructorUser']['uuid']));
                    ?>
                </li>
            </ul>
            <br/>
            <?php
            if($appointmentDetails['Appointment']['status'] == 1){

                $date1 = $appointmentDetails['Appointment']['created'];
                $date2 = date('Y-m-d H:i:s');
                $timestamp1 = strtotime($date1);
                $timestamp2 = strtotime($date2);
                $hours = abs($timestamp2 - $timestamp1)/(60*60);

                if($hours < 24){
                    echo $this->Form->postLink(
                        'Cancel Appointment',
                        array(
                            'controller' => 'appointments',
                            'action' => 'appointmentActions',
                            $appointmentDetails['Appointment']['id'],
                            3
                        ),
                        array(
                            'class' => 'btn btn-black margin-left-10',
                            'escape' => false
                        ),
                        __('Are you sure you want to cancelled this appointment')
                    );
                }
            }

            if($appointmentDetails['Benchmark']['uuid']){
                echo $this->Html->link('View benchmark', array('controller' => 'benchmarks', 'action' => 'view', $appointmentDetails['Benchmark']['uuid']), array('class' => 'btn btn-theme'));
            }
            ?>
        </div>
    </div>
</div>