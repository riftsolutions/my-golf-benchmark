<?php
/**
 * @var $this View
 */
?>
    <h2 class="page-title"><?php echo $title;?></h2>
    <hr class="shadow-line"/>
    <div class="dropdown margin-bottom-5">
        <button class="btn btn-info dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown">
            Quick Navigation
            <span class="caret"></span>
        </button>
        <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
            <li role="presentation">
                <?php echo $this->Html->link('Waiting', array('controller' => 'appointments', 'action' => 'waiting/waiting'));?>
            </li>
            <li role="presentation" class="divider"></li>
            <li role="presentation">
                <?php echo $this->Html->link('Expired', array('controller' => 'appointments', 'action' => 'waiting/expired'));?>
            </li>
            <li role="presentation" class="divider"></li>
            <li role="presentation">
                <?php echo $this->Html->link('Cancelled', array('controller' => 'appointments', 'action' => 'waiting/cancelled'));?>
            </li>
        </ul>
        <a class="btn btn-theme text-uppercase" data-toggle="modal" data-target="#waitingListModal2"> Join Waitlist</a>

    </div>
<?php if($appointments):?>
    <div class="table-responsive">
        <table class="table data-table table-bordered sort-table">
            <thead>
            <tr>
                <th><?php echo $this->Paginator->sort('Instructor.first_name', 'Instructor Name'); ?></th>
                <th><?php echo $this->Paginator->sort('Package.name', 'Package Name'); ?></th>
                <th><?php echo $this->Paginator->sort('Waiting.lesson_no', 'Lesson No'); ?></th>
                <th><?php echo $this->Paginator->sort('Waiting.start', 'Appointment Data and Time'); ?></th>
                <th><?php echo $this->Paginator->sort('Waiting.status', 'Status'); ?></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($appointments as $appointment):?>
                <tr>
                    <td>
                        <?php echo $this->Html->link($appointment['Instructor']['first_name']. ' '. $appointment['Instructor']['last_name'], array('controller' => 'users', 'action' => 'details', $appointment['InstructorUser']['uuid']));?>
                    </td>
                    <td>
                        <?php echo $this->Html->link($appointment['Package']['name'], array('controller' => 'packages', 'action' => 'view', $appointment['Package']['uuid']));?>
                    </td>
                    <td>
                        <?php echo $appointment['Waiting']['lesson_no']; ?>
                    </td>
                    <td>
                        <?php echo $this->Time->format('M d, Y', $appointment['Waiting']['start']);?>
                        <span class="italic-sm">( <?php echo $this->Time->format('h:m a', $appointment['Waiting']['start'])?> - <?php echo $this->Time->format('h:m a', $appointment['Waiting']['end'])?>)</span>
                    </td>
                    <td>
                        <?php
                        if($appointment['Waiting']['status'] == 1){
                            echo '<label class="label label-warning">waiting</label>';
                        }
                        elseif($appointment['Waiting']['status'] == 2){
                            echo '<label class="label label-info">awaiting for confirmation</label>';
                        }
                        elseif($appointment['Waiting']['status'] == 3){
                            echo '<label class="label label-success">converted into appointment</label>';
                        }
                        elseif($appointment['Waiting']['status'] == 4){
                            echo '<label class="label label-danger">expired</label>';
                        }
                        elseif($appointment['Waiting']['status'] == 5){
                            echo '<label class="label label-danger">cancelled</label>';
                        }
                        elseif($appointment['Waiting']['status'] == 6){
                            echo '<label class="label label-danger">invalid</label>';
                        }
                        else{
                            echo '<label class="label label-danger">N/A</label>';
                        }
                        ?>

                        <?php
                        if($appointment['Waiting']['status'] == 2){
                            echo $this->Html->link('View Details', array('controller' => 'appointments', 'action' => 'notification_details', $appointment['Waiting']['uuid']), array('class' => 'd-btn pull-right'));
                        }
                        ?>
                    </td>
                </tr>
            <?php endforeach;?>
            </tbody>
        </table>
        <?php echo $this->element('pagination')?>
    </div>
<?php else:?>
    <h4 class="not-found">Sorry, Waiting list empty</h4>
<?php endif;?>
<!-- Modal For range Waiting List -->
<div class="modal event-modal fade" id="waitingListModal2" tabindex="-1" role="dialog"
     aria-labelledby="setApointmentModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <form action="<?php echo Router::url('/', true); ?>calendars/setRangeWaitingList" method="post" class="form-horizontal form-custom">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                    </button>
                    <h4 class="sm-title">RESERVE TIMES ON THE WAITING LIST </h4>
                </div>
                <div class="modal-body">

                    <div class="form-group">
                        <div class="col-lg-6">
                            <label for="exampleInputEmail1">Start Date:</label>

                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                                <input id="datepicker" name="fromDate" type="text" class="form-control"
                                       placeholder="From">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <label for="exampleInputEmail1">End Date:</label>

                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                                <input id="datepicker2" name="toDate" type="text" class="form-control"
                                       placeholder="To">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-lg-12"><label>Time Range</label></div>
                        <div class="col-lg-6">
                            <div class="input-group">
                                <select name="waiting_starting_time" class="form-control" id="GroupLessonStart">
                                    <?php echo $this->Utilities->getFromHours('Starting Time');?>
                                </select>
                                <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="input-group">
                                <select name="waiting_ending_time" class="form-control" id="GroupLessonEnd">
                                    <?php echo $this->Utilities->getFromHours('Ending Time');?>
                                </select>
                                <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-4">
                            <button type="submit" class="btn btn-theme pull-left">Submit</button>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <?php echo $this->Form->end(); ?>
    </div>
</div>
<!-- Modal For range Waiting List -->

