<?php
/**
 * @var $this View
 */
?>
    <h2 class="page-title"><?php echo $title;?></h2>
    <hr class="shadow-line"/>
<div class="page-option">
    <div class="dropdown margin-bottom-5 page-option-left">
        <button class="btn btn-info dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown">
            Quick Navigation
            <span class="caret"></span>
        </button>
        <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
            <li role="presentation">
                <?php echo $this->Html->link('All Appointments (' .$appointmentOverview[0][0]['totalAppointment']. ')', array('controller' => 'appointments', 'action' => 'list'));?>
            </li>
            <li role="presentation" class="divider"></li>
            <li role="presentation">
                <?php echo $this->Html->link('Rescheduled Appointments (' .$appointmentOverview[0][0]['countRescheduledAppointment']. ')', array('controller' => 'appointments', 'action' => 'list', 'reschedule'));?>
            </li>
            <li role="presentation" class="divider"></li>
            <li role="presentation">
                <?php echo $this->Html->link('Pending Appointments (' .$appointmentOverview[0][0]['countPendingAppointment']. ')', array('controller' => 'appointments', 'action' => 'list', 'pending'));?>
            </li>
            <li role="presentation" class="divider"></li>
            <li role="presentation">
                <?php echo $this->Html->link('Upcoming Appointments (' .$appointmentOverview[0][0]['countConfirmAppointment']. ')', array('controller' => 'appointments', 'action' => 'list', 'upcoming'));?>
            </li>
            <li role="presentation" class="divider"></li>
            <li role="presentation">
                <?php echo $this->Html->link('Cancelled Appointments (' .$appointmentOverview[0][0]['countCancelledAppointment']. ')', array('controller' => 'appointments', 'action' => 'list', 'cancelled'));?>
            </li>
            <li role="presentation" class="divider"></li>
            <li role="presentation">
                <?php echo $this->Html->link('Completed Appointments (' .$appointmentOverview[0][0]['countCompletedAppointment']. ')', array('controller' => 'appointments', 'action' => 'list', 'completed'));?>
            </li>
            <li role="presentation" class="divider"></li>
            <li role="presentation">
                <?php echo $this->Html->link('Completed Appointments (' .$appointmentOverview[0][0]['countExpiredAppointment']. ')', array('controller' => 'appointments', 'action' => 'list', 'expired'));?>
            </li>
        </ul>
    </div>
</div>

<?php if($appointments):?>
    <div class="table-responsive">
        <table class="table data-table table-bordered sort-table">
            <thead>
            <tr>
                <th><?php echo $this->Paginator->sort('Instructor.first_name', 'Instructor Name'); ?></th>
                <th><?php echo $this->Paginator->sort('Package.name', 'Package Name'); ?></th>
                <th><?php echo $this->Paginator->sort('Appointment.lesson_no', 'Lesson No'); ?></th>
                <th><?php echo $this->Paginator->sort('Appointment.start', 'Appointment Data and Time'); ?></th>
                <th><?php echo $this->Paginator->sort('Appointment.status', 'Status'); ?></th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($appointments as $appointment):?>
                <tr>
                    <td>
                        <?php echo $this->Html->link($appointment['Instructor']['first_name']. ' '. $appointment['Instructor']['last_name'], array('controller' => 'users', 'action' => 'details', $appointment['InstructorUser']['uuid']));?>
                    </td>
                    <td>
                        <?php echo $this->Html->link($appointment['Package']['name'], array('controller' => 'packages', 'action' => 'view', $appointment['Package']['uuid']));?>
                    </td>
                    <td>
                        <?php echo $appointment['Appointment']['lesson_no']; ?>
                    </td>
                    <td>
                        <?php echo $this->Time->format('M d, Y', $appointment['Appointment']['start']);?>
                        <span class="italic-sm">( <?php echo $this->Time->format('h:m a', $appointment['Appointment']['start'])?> - <?php echo $this->Time->format('h:m a', $appointment['Appointment']['end'])?>)</span>
                    </td>
                    <td>
                        <?php
                        if($appointment['Appointment']['status'] == 1){
                            echo '<label class="label label-gray">pending</label>';
                        }
                        elseif($appointment['Appointment']['status'] == 2){
                            echo '<label class="label label-success">confirm/upcoming</label>';
                        }
                        elseif($appointment['Appointment']['status'] == 3){
                            echo '<label class="label label-danger">cancelled</label>';
                        }
                        elseif($appointment['Appointment']['status'] == 4){
                            echo '<label class="label label-info">completed</label>';
                        }
                        elseif($appointment['Appointment']['status'] == 5){
                            echo '<label class="label label-warning">expired</label>';
                        }
                        else{
                            echo '<label class="label label-danger">N/A</label>';
                        }

                        if($appointment['Appointment']['is_benchmarked'] == 1 && ($appointment['Appointment']['status'] == 4)){
                            echo '<label class="label label-default">benchmark complete</label>';
                        }


                        if(($appointment['Appointment']['is_reschedule'] == 1)){
                        echo '<label class="label label-default margin-left-5">reschedule</label>';
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        echo $this->Html->link('Details', array('controller' => 'appointments', 'action' => 'view', $appointment['Appointment']['id']), array('class' => 'd-btn'));

                        if($appointment['Benchmark']['uuid']){
                            echo $this->Html->link('View benchmark', array('controller' => 'benchmarks', 'action' => 'view', $appointment['Benchmark']['uuid']), array('class' => 'd-btn margin-left-5'));
                        }
                        ?>
                    </td>
                </tr>
            <?php endforeach;?>
            </tbody>
        </table>
        <?php echo $this->element('pagination')?>
    </div>
<?php else:?>
    <h4 class="not-found">Sorry, appointment list empty</h4>
<?php endif;?>