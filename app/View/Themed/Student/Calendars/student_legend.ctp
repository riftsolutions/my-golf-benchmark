<?php
/**
 * @var $this View
 */
?>

<h2 class="page-title">Color Legend</h2>
<hr class="shadow-line"/>

<div class="row">
    <div class="col-lg-6">
        <fieldset class="color_fieldset ">
            <legend>Lesson Left (Middle Circle)</legend>
            <div class="media">
                <div class="media-left media-bottom">
                    <span class="sm-circle sm-circle-red static" title="Lesson Left">0</span>
                </div>
                <div class="media-body">
                    <h4 class="media-heading">No lessons left</h4>
                    <p><strong style="color: #d9534f">Red</strong> indicates there are no more lessons left</p>
                </div>
            </div>
            <div class="media">
                <div class="media-left media-bottom">
                    <span class="sm-circle sm-circle-orange static" title="Lesson Left">1</span>
                </div>
                <div class="media-body">
                    <h4 class="media-heading">1 Lessons Left</h4>
                    <p><strong style="color: orange;">Orange</strong> indicates there is only one more lesson left</p>
                </div>
            </div>
            <div class="media">
                <div class="media-left media-bottom">
                    <span class="sm-circle sm-circle-green static" >X</span>
                </div>
                <div class="media-body">
                    <h4 class="media-heading">more than 1 lessons left</h4>
                    <p><strong style="color: yellowgreen;">Green</strong> indicates there is more than one lesson left</p>
                </div>
            </div>
        </fieldset>

        <fieldset class="color_fieldset ">
            <legend>Scheduled (Right Circle)</legend>
            <div class="media">
                <div class="media-left media-bottom">
                    <span class="sm-circle sm-circle-red static" title="Lesson Left">0</span>
                </div>
                <div class="media-body">
                    <h4 class="media-heading">No Lessons scheduled</h4>
                    <p><strong style="color: #d9534f">Red</strong> indicates there are no lessons scheduled</p>
                </div>
            </div>
            <div class="media">
                <div class="media-left media-bottom">
                    <span class="sm-circle-scheduled sm-circle-yellow static" title="Lesson Scheduled">X</span>
                </div>
                <div class="media-body">
                    <h4 class="media-heading">Lessons scheduled less than current series</h4>
                    <p><strong style="color: #ffff00;">Yellow</strong> indicates there are lessons scheduled less than current series (e.g X number of lessons have been scheduled)</p>
                </div>
            </div>
            <div class="media">
                <div class="media-left media-bottom">
                    <span class="sm-circle sm-circle-green static" >X</span>
                </div>
                <div class="media-body">
                    <h4 class="media-heading">Lessons scheduled equaling series</h4>
                    <p><strong style="color: yellowgreen;">Green</strong> indicates lessons scheduled equaling the series (e.g X number of lessons have been scheduled)</p>
                </div>
            </div>
            <div class="media">
                <div class="media-left media-bottom">
                    <span class="sm-circle-scheduled sm-circle-gold static" title="Lesson Scheduled">X</span>
                </div>
                <div class="media-body">
                    <h4 class="media-heading">Lessons scheduled beyond series</h4>
                    <p><strong style="color: #b17630;">Brown</strong> indicates lessons scheduled beyond series (e.g X number of lessons have been scheduled)</p>
                </div>
            </div>
        </fieldset>
    </div>
    <div class="col-lg-6">
        <fieldset class="color_fieldset ">
            <legend>Status (Label)</legend>

            <div class="media">
                <div class="media-left media-bottom">
                    <label class="appointment-status-for-box label label-info static">Complete</label>
                </div>
                <div class="media-body">
                    <h4 class="media-heading">Appointment Complete</h4>
                    <p>This label indicates that an appointment has been successfully completed with a benchmark.</p>
                </div>
            </div>

            <div class="media">
                <div class="media-left media-bottom">
                    <label class="appointment-status-for-box label label-warning static">Incomplete</label>
                </div>
                <div class="media-body">
                    <h4 class="media-heading">Appointment Incomplete</h4>
                    <p>This label indicates that an appointment has not been completed</p>
                </div>
            </div>

            <div class="media">
                <div class="media-left media-bottom">
                    <label class="appointment-status-for-box label label-danger static">appointment cancelled</label>
                </div>
                <div class="media-body">
                    <h4 class="media-heading">Appointment Cancelled</h4>
                    <p>This label indicates that an appointment has been canceled by an instructor or student</p>
                </div>
            </div>

            <div class="media">
                <div class="media-left media-bottom">
                    <label class="appointment-status-for-box label label-warning static">Blocked Time</label>
                </div>
                <div class="media-body">
                    <h4 class="media-heading">Blocked Time</h4>
                    <p>This label indicates that this time is blocked by the instructor</p>
                </div>
            </div>
        </fieldset>

        <fieldset class="color_fieldset ">
            <legend>Extras (Label/Left Circle)</legend>

            <div class="media">
                <div class="media-left media-bottom">
                    <span class="paid-lesson static" style="padding: 2px 9px 3px 9px;">$</span>
                </div>
                <div class="media-body">
                    <h4 class="media-heading">Paid Lesson</h4>
                    <p>The $ symbol indicates the lesson has been paid</p>
                </div>
            </div>

            <div class="media">
                <div class="media-left media-bottom">
                    <label class="label pay-at-facility-label static">Rescheduled</label>
                </div>
                <div class="media-body">
                    <h4 class="media-heading">Appointment Rescheduled</h4>
                    <p>This label indicates that an appointment has been canceled and Rescheduled to another student</p>
                </div>
            </div>

            <div class="media">
                <div class="media-left media-bottom">
                    <label class="label pay-at-facility-label static">Pay At Facility</label>
                </div>
                <div class="media-body">
                    <h4 class="media-heading">Pay At Facility</h4>
                    <p>This label indicates that the student needs to pay at the facility</p>
                </div>
            </div>

        </fieldset>
    </div>
</div>

<!--<fieldset class="color_fieldset">
    <legend>Lesson Left</legend>
    <?php /*foreach($this->Utilities->getColorForLesson() as $color ):*/?>
        <div class="media">
            <div class="media-left media-bottom">
                <div class="color-box" style="background: <?php /*echo $color['code'];*/?>;"></div>
            </div>
            <div class="media-body">
                <h4 class="media-heading"><?php /*echo $color['title'];*/?></h4>
                <p><?php /*echo $color['desc'];*/?></p>
            </div>
        </div>
    <?php /*endforeach; */?>
</fieldset>

<fieldset class="color_fieldset">
    <legend>Scheduling</legend>
    <?php /*foreach($this->Utilities->getColorForSchedule() as $color ):*/?>
        <div class="media">
            <div class="media-left media-bottom">
                <div class="color-box" style="background: <?php /*echo $color['code'];*/?>;"></div>
            </div>
            <div class="media-body">
                <h4 class="media-heading"><?php /*echo $color['title'];*/?></h4>
                <p><?php /*echo $color['desc'];*/?></p>
            </div>
        </div>
    <?php /*endforeach; */?>
</fieldset>-->