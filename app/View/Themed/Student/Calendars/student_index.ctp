<?php
/**
 * @var $this View
 */
?>
<style>
    #calendar table tbody td:first-child {
        border-bottom: 3px solid #DDD !important;
        background-color: #fff;
        border-right: 3px solid #DDD !important;
    }

    .fc-slats td {
        height: 75px !important;
        border-bottom: 0;
        color: #d9edf7;
        background-color: #fff;
        border: 1px solid #EEE !important;
    }

    .fc-slats .fc-minor .fc-time {
        border-right: 3px solid #DDD !important;
    }

    .fc-ltr .fc-time-grid .fc-event-container {
        margin: 0px !important;
    }

    .fc-slats .fc-minor td {
        border-style: none !important;
    }

    .fc-time-grid-event > .fc-content {
        position: relative;
        z-index: 2;
        margin: 10px !important;
    }

    .fc-time-grid-event > .fc-content {
        position: relative;
        z-index: 2;
        margin: 10px !important;
    }

    .fc-unthemed .fc-today {
        background: none !important;
    }

    .fc-time-grid-event > .fc-content {
        margin: 5px !important;
    }

    .fc-slats .fc-minor td {
        border: 0px !important;

    }

    .fc-slats .fc-minor .fc-time {
        border-bottom: 3px solid #DDD !important;
    }
</style>
<h2 class="page-title">Calendar</h2>
<hr class="shadow-line"/>
<span id="userID" style="display: none;"><?php echo $userID; ?></span>
<div class="control-panel">
    <div class="element">
        <a class="btn btn-theme text-uppercase" data-toggle="modal" data-target="#waitingListModal2"> Join Waitlist</a>
    </div>
</div>

<div id="message-box" style="display: none;" class="flash-msg alert alert-dismissable">
    <div id="message"></div>
</div>
<div id="calendar"></div>

<script>
    var userID = parseInt($('#userID').html());
    $(document).ready(function () {
        var calendar = $('#calendar').fullCalendar({
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
            },
            ignoreTimezone: true,
            defaultView: 'agendaWeek',
            allDayDefault: false,
            allDaySlot: false,
            selectable: true,
            slotEventOverlap: true,
            slotDuration: "00:15:00",
            contentHeight: 700,
            scrollTime :  "7:00:00",
            axisFormat: 'HH:mm a',
            timeFormat: 'HH:mm A()',
            eventSources: [
                {
                    url: '<?php echo Router::url('/', true);?>calendars/getAllEventsOfStudent',
                    type: 'POST',
                    error: function () {
                        alert('there was an error while fetching some events!');
                    }
                }
            ],
            eventDataTransform: function (data) {
                return data;
            },
            select: function (start, end, allDay) {
                startingTimeForDisplay = $.fullCalendar.moment(start).format('ddd, MMMM DD, h:mm a');
                endingTimeForDisplay = $.fullCalendar.moment(end).format('h:mm a');
                startTime = $.fullCalendar.moment(start).format('YYYY-MM-DD HH:mm:ss');
                endTime = $.fullCalendar.moment(end).format('YYYY-MM-DD HH:mm:ss');
                var appointmentFrom = startingTimeForDisplay + ' - ' + endingTimeForDisplay;

                $('#setApointmentModal #appointmentDate').text($.fullCalendar.moment(start).format('ddd, MMMM DD, YYYY'))
                $('#setApointmentModal #appointmentTime').text($.fullCalendar.moment(start).format('h:mm a') + ' - ' + $.fullCalendar.moment(end).format('h:mm a'))

                $('#setApointmentModal #apptStartTime').val(startTime);
                $('#setApointmentModal #apptEndTime').val(endTime);

                $.getJSON('<?php echo Router::url('/', true);?>instructor/packages/usersPackages/' + parseInt(userID), function (data) {
                    if (data.package) {
                        $('#packageID')
                            .find('option')
                            .remove()
                            .end()
                        ;

                        $.each(data.package, function (key, value) {

                            $('#packageID').append(
                                $("<option></option>").text(value).val(key)
                            );
                        });

                        $('#setApointmentModal').modal('show');
                    }
                });
            },
            eventRender: function (event, element) {
                if (event.type == 'appointment') {
                    manageEventView(event, element);
                }
                else if (event.type == 'group_lesson') {
                    groupLessonEventView(event, element);
                }
                else if (event.type == 'block_time') {
                    blockTimeEventView(event, element);
                }
                else {
                    element.css('background-color', '#EEE');
                    element.css('border-color', '#EEE');
                    element.find('.fc-title').text('');
                    element.find('.fc-time').text('');
                }

                $('body').on('click', function (e) {
                    if (!element.is(e.target) && element.has(e.target).length === 0 && $('.popover').has(e.target).length === 0)
                        element.popover('hide');
                });
            },
            eventClick: function (data, allDay, jsEvent, view) {

                if (data.type == 'group_lesson') {
                    isSetAppointmentInGLByStudent(data.id);
                }

                if (data.type == 'appointment') {
                    if (parseInt(data.student_profile.user_id) != userID) {
                        checkWaitingListButton(data.id);
                    }
                }
            }
        });
    });

    function manageEventView(event, element) {

        element.css('color', '#3c763d');
        element.css('background-color', '#dff0d8');
        element.css('border-color', '#d6e9c6');

        var lesson_left_circle;
        var scheduled_circle;
        var lesson_left = parseInt(event.lesson_left);
        var scheduled = parseInt(event.scheduled);
        var total = parseInt(event.total);

        if(lesson_left > 1){
            lesson_left_circle = 'sm-circle-green';
        }
        else if(lesson_left == 1){
            lesson_left_circle = 'sm-circle-orange';
        }
        else if(lesson_left < 1){
            lesson_left_circle = 'sm-circle-red';
        }

        if(scheduled < 1){
            scheduled_circle = 'sm-circle-red';
        }
        else if(scheduled == total){
            scheduled_circle = 'sm-circle-green';
        }
        else if(scheduled < total){
            scheduled_circle = 'sm-circle-yellow';
        }
        else if(scheduled > total){
            scheduled_circle = 'sm-circle-gold';
        }

        element.find('.fc-title').html(event.title);

        var appointmentType = '<li><strong>Appointment Type: </strong><label class="status-text blue">Regular</label></li>';
        if (userID == parseInt(event.appointment_by)) {

            var lessonLeft = '<li><strong>Lessons Left: </strong>' + event.lesson_left + '</li>';
            var lessonScheduled = '<li><strong>Lessons Scheduled: </strong>' + event.scheduled + '</li>';
            var paymentStatus = '<li><strong>Payment Status: </strong><label class="status-text orange">UNPAID</label></li>';

            if (event.is_paid_lesson) {
                var paymentStatus = '<li><strong>Payment Status: </strong><label class="status-text yellowgreen">PAID</label></li>';
            }
        }
        else {
            var lessonLeft = '';
            var lessonScheduled = '';
            var paymentStatus = '';
        }



        if (event.isAppointmentRescheduled == 1) {
            element.find('.fc-time').append(' (Rescheduled) ');
        }

        var appointmentBtn = '';
        var countWaiting = '';
        var countWaiting = '<li><strong>Waiting List: </strong>' + event.count_waiting + '</li>';


        if (event.appointment_status == 1) {
            var label = '<label class="label label-gray">appointment pending</label>';
        }
        if (event.appointment_status == 2) {
            var label = '<!--<label class="label label-success">appointment confirm</label>-->';
        }
        if (event.appointment_status == 3) {
            var label = '<label class="label label-danger">appointment cancelled</label>';
            element.css('color', '#a94442');
            element.css('background-color', '#f2dede');
            element.css('border-color', '#f2dede');
        }
        if (event.appointment_status == 4) {
            var label = '<label class="label label-info">appointment completed</label>';
        }
        if (event.appointment_status == 5) {
            var label = '<label class="label label-warning">appointment expired</label>';
        }

        element.find('.fc-time').append('<span class="appointment-status-label">' + label + '</span>');

        var rescheduleAppointment = '';
        if (event.is_reschedule == 1) {
            var rescheduleAppointment = '<p><strong class="italic-sm danger">N.B.: This is rescheduled appointment</strong> </p>'
        }

        if (event.is_pay_at_facility == false) {
            if (userID == parseInt(event.appointment_by)) {
                element.find('.fc-time').append('<span class="sm-circle ' + lesson_left_circle + '" title="Lesson Left">' + event.lesson_left + '</span>');
                element.find('.fc-time').append('<span class="sm-circle-scheduled ' + scheduled_circle + '" title="Lesson Scheduled">' + event.scheduled + '</span>');
            }
            if (event.is_paid_lesson) {
                element.find('.fc-time').append('<span class="paid-lesson">$</span>');
            }
        }
        else {
            element.find('.fc-time').append('<label class="label pay-at-facility-label">Pay At Facility</label>');
            if (event.appointment_status == 2) {
                element.css('color', '#8a6d3b');
                element.css('background-color', '#fcf8e3');
                element.css('border-color', '#fcf8e3');
            }
            if (event.appointment_status == 3) {
                element.css('color', '#FFF');
                element.css('background-color', '#d08166');
                element.css('border-color', '#fcf8e3');
            }
        }

        if(event.is_reschedule == 1){
            element.find('.fc-time').append('<label class="label rechsuduled-label">Rescheduled</label>');

            element.css('color', '#525252');
            element.css('background-color', '#CCC');
            element.css('border-color', '#CCC');
        }

        var note = '';
        if(event.note)
        {
            note = '<li><i style="font-size: 12px !important; color: #999;">Note:'+ event.note +'</i></li>';
        }

        element.popover({
            title: "Appointment Information",
            placement: 'auto',
            html: true,
            trigger: 'click',
            animation: 'true',
            content: '<ul class="data-list">' +
/*            '<li><strong>Student Name: </strong>' + event.student_profile.first_name + ' ' + event.student_profile.last_name + '</li>' +
            '<li><strong>Email: </strong>' + event.username + '</li>' +
            '<li><strong>Phone: </strong>' + event.student_profile.phone + '</li>' +
            '<li><strong>City: </strong>' + event.student_profile.city + '</li>' +
            '<li><strong>State: </strong>' + event.student_profile.state + '</li>' +
            '<li><strong>Postal Code: </strong>' + event.student_profile.postal_code + '</li>' +*/
            '<li><strong>Appointment Date: </strong>' + $.fullCalendar.moment(event.start._i).format('MMM DD, YYYY') + '</li>' +
            '<li><strong>Appointment Time: </strong>' + $.fullCalendar.moment(event.start._i.start).format('h:mm a') + ' - ' + $.fullCalendar.moment(event.end._i).format('h:mm a') + '</li>' +
            '<li><strong>Package Name: </strong>' + event.package + '</li>' +
            lessonLeft +
            lessonScheduled +
            paymentStatus +
            '<li><strong>Current Status: </strong>' + label + '</li>' +
            countWaiting +
            rescheduleAppointment +
            note +
            '</ul>' + appointmentBtn + '<div id="appointment_' + event.id + '"></div>',
            container: 'body'
        });
    }

    <!---- CHECKING THE STUDENT SET APPOINTMENT FOR AMONG A GROUP LESSON ----->
    function isSetAppointmentInGLByStudent(groupID) {
        console.log(groupID);

        $.ajax({
            url: "<?php echo Router::url('/', true);?>/calendars/checkGroupLessonBtn/" + groupID + "/" + userID,
            type: "POST",
            dataType: "json",
            success: function (data) {

                console.log(data);

                if (data == 'join') {
                    $('#' + groupID).html('<a href="<?php echo Router::url('/', true);?>student/calendars/join_group_lesson/' + groupID + '/' + userID + '" class="btn btn-theme width-100-p text-uppercase">Join Group Lesson</a>');
                }
                else if (data == 'leave') {
                    $('#' + groupID).html('<label class="status-text yellowgreen">You already joined in this group lesson</label><br/><br/><a href="<?php echo Router::url('/', true);?>student/calendars/leave_group_lesson/' + groupID + '/' + userID + '" class="btn btn-theme-red width-100-p text-uppercase">Leave from Group Lesson</a>');
                }
            },
            error: function (xhr, status, error) {
                console.log(error);
            }
        });
    }
    <!---- /CHECKING THE STUDENT SET APPOINTMENT FOR AMONG A GROUP LESSON ----->

    <!---- CHECKING THE STUDENT SET APPOINTMENT FOR AMONG A GROUP LESSON ----->
    function checkWaitingListButton(appointmentID) {
        $.ajax({
            url: "<?php echo Router::url('/', true);?>/calendars/checkWaitingBtn/" + appointmentID + "/" + userID,
            type: "POST",
            dataType: "json",
            success: function (data) {

                if (data == 'join') {
                    console.log(data);

                    $('#appointment_' + appointmentID).html('<a class="btn btn-theme width-100-p text-uppercase" onclick="waitingAction(' + appointmentID + ')">Join to Waiting List<a>');
                }
                else if (data == 'leave') {
                    $('#appointment_' + appointmentID).html('<label class="status-text yellowgreen">You already joined in this waiting list</label><br/><br/><a href="<?php echo Router::url('/', true);?>student/calendars/leave_waiting_list/' + appointmentID + '/' + userID + '" class="btn btn-theme-red width-100-p text-uppercase">Leave from Waiting List</a>');
                }
            },
            error: function (xhr, status, error) {
                console.log(error);
            }
        });
    }
    <!---- /CHECKING THE STUDENT SET APPOINTMENT FOR AMONG A GROUP LESSON ----->

    <!---- Manage Group Lesson Event View ----->
    function groupLessonEventView(event, element) {

        element.css('background-color', '#d9edf7');
        element.css('border-color', '#bce8f1');
        element.css('color', '#31708f');

        element.find('.fc-time').text('');
        element.find('.fc-title').append('<br/> <br/> <strong style="font-size: 12px; text-transform: uppercase; margin-top: 12px; color: #31708f">Group Lesson</strong>');

        var title = '';
        if(event.title)
        {
            title = '<li><strong>Title: </strong>'+ event.title +'</li>';
        }

        var desc = '';
        if(event.description)
        {
            desc = '<li><strong>Description: </strong>'+ event.description +'</li>';
        }

        element.popover({
            title: "Group Lesson",
            placement: 'auto',
            html: true,
            trigger: 'click',
            animation: 'true',
            content: '<ul class="data-list">' +
             title  +
            desc +
            '<li><strong>price:  </strong>$' + event.price + '</li>' +
            '<li><strong>Student Limit: </strong>' + event.student_limit + '</li>' +
            '<li><strong>Filled Up: </strong>' + event.filled_up_student + '</li>' +
            '<li><strong>Available: </strong>' + (event.student_limit - event.filled_up_student) + '</li>' +
            '<li><strong>Appointment Date: </strong>' + $.fullCalendar.moment(event.start._i).format('ddd, MMMM DD, YYYY') + '</li>' +
            '<li><strong>Appointment Time: </strong>' + $.fullCalendar.moment(event.start._i).format('h:mm a') + ' - ' + $.fullCalendar.moment(event.end._i).format('h:mm a') + '</li>' +
            '<li><br/><div id="' + event.id + '"></div></li>' +
            '</ul>',
            container: 'body'
        });
    }
    <!---- /Manage Group Lesson Event View ----->

    <!---- Manage Block Time Event View ----->
    function blockTimeEventView(event, element) {

        element.css('color', '#a94442');
        element.css('background-color', '#FFF');
        element.css('border-color', '#a94442');

        element.find('.fc-time').text('');
        element.find('.fc-content').css('background', 'url(http://fullthrottle.co.in/images/prohibited-forbidden-no-banned-road-sign-u21423-r-fr_2x.png) 50% 40% / 25% no-repeat');

        var title = '';
        if(event.title)
        {
            title = '<li><strong>Title: </strong>'+ event.title +'</li>';
        }

        var message = '';
        if(event.message)
        {
            message = '<li><strong>Message: </strong>' + event.message + '</li>';
        }

        element.popover({
            title: "<span style='color: #a94442;'>Block Time</span>",
            placement: 'auto',
            html: true,
            trigger: 'click',
            animation: 'true',
            content: '<ul class="data-list block_time_list">' +
            title +
            message +
            '<li><strong>Block Date: </strong>' + $.fullCalendar.moment(event.start._i).format('ddd, MMMM DD, YYYY') + '</li>' +
            '<li><strong>Block Time: </strong>' + $.fullCalendar.moment(event.start._i).format('h:mm a') + ' - ' + $.fullCalendar.moment(event.end._i).format('h:mm a') + '</li>' +
            '</ul>',
            container: 'body'
        });
    }
    <!---- /Manage BLock Time Event View ----->

    <!----- Set Appointment ----->
    $(document).ready(function () {

        $('#wantToReschedule').click(function () {
            var isChecked = $('#wantToReschedule input').is(':checked');
            if (isChecked == true) {
                $('#rescheduleAppointment').show();
                $('#selectPackage').hide();
            }
            else {
                $('#selectPackage').show();
                $('#rescheduleAppointment').hide();
            }
        });

        $('#wantToRescheduleWaiting').click(function () {
            var isChecked = $('#wantToRescheduleWaiting input').is(':checked');
            if (isChecked == true) {
                $('#rescheduleAppointmentWaiting').show();
                $('#selectPackageWaiting').hide();
            }
            else {
                $('#selectPackageWaiting').show();
                $('#rescheduleAppointmentWaiting').hide();
            }
        });
    });
    <!----- /Set Appointment ----->

    <!---- Displaying Specific Events ----->
    function viewEvent(appointmentInfo) {
        if (appointmentInfo.student_profile) {

            $("#viewAppointmentModal").modal('show');
            $('.loadingEvent').show().delay(2000);
            $('#appointmentBy').html(56);
            $('#instructorName').html(62526);

            $('.loadingEvent').fadeOut(500);
            $('.eventInfoArea').delay(3000).fadeIn(500);

            $('#viewAppointmentModal').on('hidden.bs.modal', function () {
                clearArea();
            })
        }
    }
    <!---- /Displaying Specific Events ----->

    function clearArea() {
        $('.creatingEvent').hide();
        $('.loadingEvent').hide();
        $('.eventInfoArea').hide();
        ;
        $('.appointmentComplete').hide();
        $('.eventBtnArea').hide();
        $('#isAppointed').hide();
        $('.empty-title').empty();
    }

    function manageFlashMessage(alertType, message) {
        $('#message-box').show();
        $('#message-box').addClass(alertType);
        $('#message').html(message);
        removeMgsArea();
    }

    function removeMgsArea() {
        window.setTimeout(function () {
                if ($("#message-box").hasClass("alert-danger")) {
                    $('#message-box').removeClass("alert-danger");
                }

                if ($("#message-box").hasClass("alert-success")) {
                    $('#message-box').removeClass("alert-success");
                }
                $("#message-box").hide();
            },
            4000
        );
    }
</script>
<?php echo $this->Js->writeBuffer(); ?>

<!-- Modal For Creating Event -->
<div class="modal event-modal fade" id="setApointmentModal" tabindex="-1" role="dialog"
     aria-labelledby="setApointmentModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <form action="<?php echo Router::url('/', true); ?>/calendars/setAppointment" method="post">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                    </button>
                    <h4 class="sm-title">Set Appointment</h4>
                </div>
                <div class="modal-body">
                    <input name="start" type="hidden" id="apptStartTime"/>
                    <input name="end" type="hidden" id="apptEndTime"/>
                    <ul class="data-list">
                        <li>
                            <strong>Appointment Date</strong>
                            <span id="appointmentDate"></span>
                        </li>
                        <li>
                            <strong>Appointment Time</strong>
                            <span id="appointmentTime"></span>
                        </li>
                    </ul>

                    <div id="selectPackage">
                        <div class="margin-top-25">
                            <label>Select Package for Appointment</label>
                            <?php
                            echo $this->Form->input('packageID', array(
                                'type' => 'select',
                                'label' => false,
                                'options' => $packages,
                                'class' => 'form-control',
                                'id' => 'packageID'
                            ))
                            ?>
                        </div>
                    </div>

                    <div id="rescheduleAppointment" style="display: none;">
                        <div class="margin-top-25">
                            <label>Reschedule for cancelled appointment</label>
                            <?php
                            echo $this->Form->input('appointmentID', array(
                                'type' => 'select',
                                'label' => false,
                                'options' => $appointments,
                                'class' => 'form-control',
                                'id' => 'appointmentID'
                            ))
                            ?>
                        </div>
                    </div>

                    <?php if ($appointments): ?>
                        <div id="wantToReschedule" class="checkbox">
                            <label>
                                <input type="checkbox" value="reschedule"> Want to reschedule?
                            </label>
                        </div>
                    <?php endif; ?>

                </div>
                <div class="modal-footer">
                    <button id="appointmentBtn" class="btn btn-theme pull-left">Set Appointment</button>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- /Modal For Creating Event -->

<!-- Modal For Displaying Event -->
<div class="modal event-modal fade" id="viewAppointmentModal" tabindex="-1" role="dialog"
     aria-labelledby="createEventLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="border-bottom: 1px solid #e5e5e5;">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                </button>
                <h4 class="sm-title">Appointment Details</h4>
            </div>
            <div class="modal-body">
                <div class="loadingEvent" style="display: none;">
                    <?php echo $this->Html->image('ajax-loader2.gif') ?><br/>
                    Event is loading
                </div>

                <div class="eventInfoArea" style="display: none;">
                    <ul class="data-list">
                        <li>
                            <strong>Student Name: </strong> <span id="appointmentBy"></span>
                        </li>
                        <li>
                            <strong>Instructor Name: </strong> <span id="instructorName"></span>
                        </li>
                    </ul>

                    <div class="youAlreadyAppointed" style="display: none;">
                        <span class="italic-md not-found green">
                            N.B: You are already appointed for this event.
                        </span>
                    </div>
                </div>
            </div>
            <div class="modal-footer"></div>
        </div>
    </div>
</div>
<!-- /Modal For Displaying Event -->


<!-- Modal For Waiting List -->
<div class="modal event-modal fade" id="waitingListModal" tabindex="-1" role="dialog"
     aria-labelledby="setApointmentModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <form action="<?php echo Router::url('/', true); ?>calendars/setWaitingList" method="post">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                    </button>
                    <h4 class="sm-title">RESERVE TIMES ON THE WAITLIST </h4>
                </div>
                <div class="modal-body">
                    <?php if ($packages): ?>
                        <div id="selectPackageWaiting">
                            <div class="margin-top-25">
                                <label>Select Package for Appointment</label>
                                <input type="hidden" name="previousAppointmentID" id="appointmentIDForWaiting"/>
                                <?php
                                echo $this->Form->input('packageID', array(
                                    'type' => 'select',
                                    'label' => false,
                                    'options' => $packages,
                                    'class' => 'form-control',
                                    'id' => 'packageIDForWaiting'
                                ))
                                ?>
                            </div>
                        </div>
                    <?php endif; ?>

                    <div id="rescheduleAppointmentWaiting" style="display: none;">
                        <div class="margin-top-25">
                            <label>Reschedule for cancelled appointment</label>
                            <?php
                            echo $this->Form->input('appointmentID', array(
                                'type' => 'select',
                                'label' => false,
                                'options' => $appointments,
                                'class' => 'form-control',
                                'id' => 'appointmentID'
                            ))
                            ?>
                        </div>
                    </div>

                    <?php if ($appointments): ?>
                        <div id="wantToRescheduleWaiting" class="checkbox">
                            <label>
                                <input type="checkbox" value="reschedule"> Want to reschedule?
                            </label>
                        </div>
                    <?php endif; ?>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-theme pull-left">Submit</button>
                </div>
            </div>
            <?php echo $this->Form->end(); ?>
    </div>
</div>
<!-- Modal For Waiting List -->

<!-- Modal For range Waiting List -->
<div class="modal event-modal fade" id="waitingListModal2" tabindex="-1" role="dialog"
     aria-labelledby="setApointmentModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <form action="<?php echo Router::url('/', true); ?>calendars/setRangeWaitingList" method="post" class="form-horizontal form-custom">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                    </button>
                    <h4 class="sm-title">RESERVE TIMES ON THE WAITING LIST </h4>
                </div>
                <div class="modal-body">

                    <div class="form-group">
                        <div class="col-lg-6">
                            <label for="exampleInputEmail1">Start Date:</label>

                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                                <input id="datepicker" name="fromDate" type="text" class="form-control"
                                       placeholder="From">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <label for="exampleInputEmail1">End Date:</label>

                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                                <input id="datepicker2" name="toDate" type="text" class="form-control"
                                       placeholder="To">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-lg-12"><label>Time Range</label></div>
                        <div class="col-lg-6">
                            <div class="input-group">
                                <select name="waiting_starting_time" class="form-control" id="GroupLessonStart">
                                    <?php echo $this->Utilities->getFromHours('Starting Time');?>
                                </select>
                                <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="input-group">
                                <select name="waiting_ending_time" class="form-control" id="GroupLessonEnd">
                                    <?php echo $this->Utilities->getFromHours('Ending Time');?>
                                </select>
                                <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-4">
                            <button type="submit" class="btn btn-theme pull-left">Submit</button>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <?php echo $this->Form->end(); ?>
    </div>
</div>
<!-- Modal For range Waiting List -->


<!---- CONFIRMCANCEL APPOINTMENT ----->
<script>

    function appointmentAction(appointmentID, action) {
        $.ajax({
            url: "<?php echo Router::url('/', true);?>/calendars/appointmentActions/" + appointmentID + "/" + action + "/",
            type: "POST",
            dataType: "json",
            success: function (data) {
                console.log(data);
                if (data == 2) {
                    $('.popover').hide();
                    manageFlashMessage('alert-success', 'Appointment has been confirmed')
                }
                else if (data == 3) {
                    $('.popover').hide();
                    manageFlashMessage('alert-danger', 'Appointment has been cancelled')
                }
                else if (data == 'expiry') {
                    $('.popover').hide();
                    manageFlashMessage('alert-danger', 'Cancellation time has been over for the appoinment')
                }
                else {
                    manageFlashMessage('alert-danger', 'Sorry, something went wrong')
                }
            },
            error: function (xhr, status, error) {
                console.log(error);
            }
        });
        $('#calendar').fullCalendar('refetchEvents');
        $('#calendar').fullCalendar('refetchEvents');
    }

    function waitingAction(appointmentID) {
        $("#waitingListModal").modal('show');
        $('#appointmentIDForWaiting').val(appointmentID);

        $('body').popover('hide');
    }


    $('#waitingBtnSubmit').click(function () {

        var previousAppointmentID = $('#appointmentIDForWaiting').val();
        var packageID = $('#packageIDForWaiting').val();
        var appointmentID = $('#appointmentID').val();

        var isChecked = $('#wantToRescheduleWaiting input').is(':checked');
        if (isChecked == true) {
            packageID = null;
        }
        else {
            appointmentID = null
        }

        var eventInfo = {
            'previousAppointmentID': previousAppointmentID,
            'packageID': packageID,
            'appointmentID': appointmentID
        };

        console.log(eventInfo);

        if (appointmentID != null || packageID != null) {
            $.ajax({
                url: '<?php echo Router::url('/', true);?>calendars/setWaitingList',
                type: "POST",
                dataType: "json",
                data: eventInfo,
                success: function (data) {
                    console.log(data);
                    if (data == 'done') {
                        $('.popover').hide();
                        manageFlashMessage('alert-success', 'Your request has been placed into waiting list')
                    }
                    else {
                        manageFlashMessage('alert-danger', 'Sorry, something went wrong')
                    }
                },
                error: function (xhr, status, error) {
                    console.log(error);
                }
            });
        }
        else {
            manageFlashMessage('alert-danger', 'You have no purchased package available for appointment')
        }


        $("#waitingListModal").modal('hide');
        $('#calendar').fullCalendar('refetchEvents');
        $('#calendar').fullCalendar('refetchEvents');
    });


    function getTimeDiff(startingTime) {
        var startedTime = new Date(startingTime);
        var today = new Date();
        var diff = new Date(today - startedTime);
        var days = diff / 1000 / 60 / 60 / 24;
        return days;
    }

    $('div').on('hidden.bs.modal', function () {
        $(document).ready(function () {
            $('.form-group').find('input').val('');
            $('.form-group').find('textarea').val('');
            $('.form-group').find('select').val('');
        });
    })

    $('div').on('shown.bs.modal', function () {
        $(document).ready(function () {
            $('.right_button_option').hide();
            $('.popover').popover().hide();
        });
    })
</script>
<!---- /CONFIRM/CANCEL APPOINTMENT ----->