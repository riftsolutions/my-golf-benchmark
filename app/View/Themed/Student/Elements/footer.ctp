<!----- FOOTER AREA ----->
<footer>
    <div class="footer-text">
        &copy; All Rights Reserved  •  Golf Swing Prescription
    </div>
</footer>
<!----- /FOOTER AREA ------>

<!----- Java Script Area ----->
<?php echo $this->Html->script(array('jquery.dataTables.min', 'jquery.dataTables.columnFilter', 'datatables', 'bootstrap-datepicker', 'bootstrap-slider', 'custom', 'underscore-min' ));?>
<!----- /Java Script Area ----->


</body>
</html>