<div class="col-xs-6 col-sm-2 sidebar-offcanvas" id="sidebar" role="navigation">
    <div class="row">
        <div class="sidebar">
            <ul id="menu" class="nav nav-list">
                <li>
                    <?php echo $this->Html->link('<i class="fa fa-home"></i> Dashboard', array('controller' => 'dashboards', 'action' => 'index'), array('escape' => false))?>
                </li>
                <li>
                    <a href="#">
                        <i class="fa fa-calendar"></i>
                        Calendar
                        <span class="glyphicon arrow"></span>
                    </a>
                    <ul class="nav nav-list submenu collapse">
                        <li>
                            <?php echo $this->Html->link(
                                'My Calendar',
                                array('controller' => 'calendars', 'action' => 'index'),
                                array('escape' => false)
                            ); ?>
                        </li>
                        <li>
                            <?php echo $this->Html->link(
                                'Color Legend',
                                array('controller' => 'calendars', 'action' => 'legend'),
                                array('escape' => false)
                            ); ?>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#">
                        <i class="fa fa-credit-card"></i>
                        My Cards
                        <span class="glyphicon arrow"></span>
                    </a>
                    <ul class="nav nav-list submenu">
                        <li>
                            <?php echo $this->Html->link(
                                'My Cards',
                                array('controller' => 'cards', 'action' => 'index'),
                                array('escape' => false)
                            ); ?>
                        </li>
                        <li>
                            <?php echo $this->Html->link(
                                'Add New Card',
                                array('controller' => 'cards', 'action' => 'add'),
                                array('escape' => false)
                            ); ?>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#">
                        <i class="fa fa-certificate"></i>
                        My Golf Benchmark
                        <span class="glyphicon arrow"></span>
                    </a>
                    <ul class="nav nav-list submenu collapse">
                        <li>
                            <?php echo $this->Html->link(
                                'Benchmark List',
                                array('controller' => 'benchmarks', 'action' => 'list'),
                                array('escape' => false)
                            ); ?>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#">
                        <i class="fa fa-clock-o"></i>
                        My Appointment
                        <span class="glyphicon arrow"></span>
                    </a>
                    <ul class="nav nav-list submenu collapse">
                        <li>
                            <?php echo $this->Html->link(
                                'Appointment List',
                                array('controller' => 'appointments', 'action' => 'list'),
                                array('escape' => false)
                            ); ?>
                        </li>
                    </ul>
                </li>
                <li>
                    <?php echo $this->Html->link(
                        '<i class="fa fa-object-group"></i> My Group Lesson',
                        array('controller' => 'lessons', 'action' => 'group'),
                        array('escape' => false)
                    ); ?>
                </li>
                <li>
                    <?php echo $this->Html->link(
                        '<i class="fa fa-exclamation-triangle"></i> My Waiting List',
                        array('controller' => 'appointments', 'action' => 'waiting'),
                        array('escape' => false)
                    ); ?>
                </li>
                <?php if($hasBenchmark > 0):?>
                <li>
                    <?php echo $this->Html->link(
                        '<i class="fa fa-asterisk"></i> Point of Sale',
                        array('controller' => 'packages', 'action' => 'pos'),
                        array('escape' => false)
                    ); ?>
                </li>
                <?php endif;?>
                <li>
                    <a href="#">
                        <i class="fa fa-line-chart"></i>
                        Reports
                        <span class="glyphicon arrow"></span>
                    </a>
                    <ul class="nav nav-list submenu">
                        <li>
                            <?php echo $this->Html->link(
                                'My Item',
                                array('controller' => 'reports', 'action' => 'my_package'),
                                array('escape' => false)
                            ); ?>
                        </li>
                        <li>
                            <?php echo $this->Html->link(
                                'Transaction',
                                array('controller' => 'reports', 'action' => 'transaction'),
                                array('escape' => false)
                            ); ?>
                        </li>
                    </ul>
                </li>
                <li>
                    <?php echo $this->Html->link(
                        '<i class="fa fa-bell"></i> Notification <span class="d-badge d-badge-orange"> '.$countNotifications.'</span>',
                        array('controller' => 'appointments', 'action' => 'waiting/notification'),
                        array('escape' => false)
                    ); ?>
                </li>
                <li>
                    <a href="#">
                        <i class="fa fa-user"></i>
                        My Account
                        <span class="glyphicon arrow"></span>
                    </a>
                    <ul class="nav nav-list submenu">
                        <li>
                            <?php echo $this->Html->link(
                                'Profile',
                                array('controller' => 'profiles', 'action' => 'index'),
                                array('escape' => false)
                            ); ?>
                        </li>
                        <li>
                            <?php echo $this->Html->link(
                                'Update Profile',
                                array('controller' => 'profiles', 'action' => 'update'),
                                array('escape' => false)
                            ); ?>
                        </li>
                        <!--<li>
                            <?php /*echo $this->Html->link(
                                'Change Profile Picture',
                                array('controller' => 'profiles', 'action' => 'change_photo'),
                                array('escape' => false)
                            ); */?>
                        </li>-->
                        <li>
                            <?php echo $this->Html->link(
                                'Change Password',
                                array('controller' => 'profiles', 'action' => 'change_password'),
                                array('escape' => false)
                            ); ?>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div>