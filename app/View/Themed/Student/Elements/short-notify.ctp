<a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <span style="line-height: 35px;">

                                    <strong class="fa fa-envelope"></strong>
                                    <span class="badge badge-notify">3</span>

                                    <b class="fa fa-cogs"></b>
                                </span>
</a>
<ul class="dropdown-menu short-notification-area">
    <li>
        <div class="short-notification">
            <div class="short-notification-header">
                <span class="pull-left short-title"><i class="fa fa-bell"></i> Notification(3)</span>
                <a class="pull-right green"><i class="fa fa-cogs"></i> Account Settings</a>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
            <ul class="media-list">
                <li class="media">
                    <div class="pull-left" href="#">
                        <?php echo $this->Html->image('profile.jpg', array('class' => 'media-object pro-img-md'));?>
                    </div>
                    <div class="media-body">
                        <a>Matthew Fouts</a>
                        <p>
                            Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin... <span class="italic-md green">3 min ago</span>
                        </p>
                    </div>
                </li>
                <li class="media">
                    <div class="pull-left" href="#">
                        <?php echo $this->Html->image('profile.jpg', array('class' => 'media-object pro-img-md'));?>
                    </div>
                    <div class="media-body">
                        <a>Matthew Fouts</a>
                        <p>
                            Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin... <span class="italic-md green">3 min ago</span>
                        </p>
                    </div>
                </li>
                <li class="media">
                    <div class="pull-left" href="#">
                        <?php echo $this->Html->image('profile.jpg', array('class' => 'media-object pro-img-md'));?>
                    </div>
                    <div class="media-body">
                        <a>Matthew Fouts</a>
                        <p>
                            Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin... <span class="italic-md green">3 min ago</span>
                        </p>
                    </div>
                </li>
                <li class="media">
                    <div class="pull-left" href="#">
                        <?php echo $this->Html->image('profile.jpg', array('class' => 'media-object pro-img-md'));?>
                    </div>
                    <div class="media-body">
                        <a>Matthew Fouts</a>
                        <p>
                            Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin... <span class="italic-md green">3 min ago</span>
                        </p>
                    </div>
                </li>
                <li class="media">
                    <div class="pull-left" href="#">
                        <?php echo $this->Html->image('profile.jpg', array('class' => 'media-object pro-img-md'));?>
                    </div>
                    <div class="media-body">
                        <a>Matthew Fouts</a>
                        <p>
                            Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin... <span class="italic-md green">3 min ago</span>
                        </p>
                    </div>
                </li>
            </ul>
        </div>
    </li>
</ul>