<?php
/**
 * @var $this View
 */
?>
<h2 class="page-title">My Profile</h2>
<hr class="shadow-line"/>
<div class="row">
    <div class="col-md-8 col-md-offset-2 col-sm-12">
        <div class="box box-padding-normal radius-5">
            <div class="row">
                <div class="col-lg-5 col-md-6 col-sm-6">
                    <?php
                    if($userInfo['Profile']['profile_pic_url']){
                        echo $this->Html->image('profiles/'.$userInfo['Profile']['profile_pic_url'], array('class' => 'img-thumbnail profile_photo', 'url' => array('controller' => 'profiles', 'action' => 'index')));
                    }
                    elseif($userInfo['User']['social_pic']){
                        echo $this->Html->image($userInfo['User']['social_pic'], array('class' => 'img-thumbnail profile-pic-small', 'url' => array('controller' => 'profiles', 'action' => 'index')));
                    }
                    else{
                        echo $this->Html->image('avatar.jpg', array('class' => 'img-thumbnail profile-pic-small', 'url' => array('controller' => 'profiles', 'action' => 'index')));
                    }
                    ?>
                    <br/>
                    <br/>
                    <a class="btn btn-theme text-uppercase" data-toggle="modal" data-target="#profilePhoto"> Change Profile Picture</a>
                </div>
                <div class="col-lg-7 col-md-6 col-sm-6">
                    <?php echo $this->Html->link('<i class="fa fa-pencil"></i> Update Profile', array('controller' => 'profiles', 'action' => 'update'), array('escape' => false, 'class' => 'fly-edit'));?>
                    <ul class="data-list">
                        <li>
                            <span class="name-lg green">
                                <?php echo $userInfo['Profile']['first_name'].' '.$userInfo['Profile']['last_name']; ?>
                            </span>
                        </li>
                        <li>
                            <strong>Email:</strong> <span><?php echo $userInfo['User']['username'];?></span>
                        </li>
                        <li>
                            <strong>Phone: </strong>
                            <span>
                                <?php
                                if($userInfo['Profile']['phone']){
                                    echo $userInfo['Profile']['phone'];
                                }
                                else{
                                    echo 'N/A';
                                }
                                ?>
                            </span>
                        </li>
                        <li>
                            <strong>Street 1: </strong>
                            <?php
                            if($userInfo['Profile']['street_1']){
                                echo $userInfo['Profile']['street_1'];
                            }
                            else{
                                echo 'N/A';
                            }
                            ?>
                        </li>
                        <li>
                            <strong>Street 2: </strong>
                            <?php
                            if($userInfo['Profile']['street_2']){
                                echo $userInfo['Profile']['street_2'];
                            }
                            else{
                                echo 'N/A';
                            }
                            ?>
                        </li>
                        <li>
                            <strong>State: </strong>
                            <?php
                            if($userInfo['Profile']['state']){
                                echo $userInfo['Profile']['state'];
                            }
                            else{
                                echo 'N/A';
                            }
                            ?>
                        </li>
                        <li>
                            <strong>City: </strong>
                            <?php
                            if($userInfo['Profile']['city']){
                                echo $userInfo['Profile']['city'];
                            }
                            else{
                                echo 'N/A';
                            }
                            ?>
                        </li>
                        <li>
                            <strong>Zip Code: </strong>
                            <?php
                            if($userInfo['Profile']['postal_code']){
                                echo $userInfo['Profile']['postal_code'];
                            }
                            else{
                                echo 'N/A';
                            }
                            ?>
                        </li>
                        <li>
                            <strong>Member Since: </strong>
                            <?php
                            echo $this->Time->format('M d, Y', $userInfo['User']['created']);
                            ?>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="margin-top-15">
                <hr class="shadow-line">
                <h2 class="short-title theme-color">Biography:</h2>
                <i class="italic-md theme-color">
                    <?php
                    if($userInfo['Profile']['biography']){
                        echo $userInfo['Profile']['biography'];
                    }
                    else{
                        echo 'N/A';
                    }
                    ?>
                </i>
            </div>
        </div>
    </div>
</div>

<!-- Modal For range Waiting List -->
<div class="modal event-modal fade" id="profilePhoto" tabindex="-1" role="dialog"
     aria-labelledby="setApointmentModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                </button>
                <h4 class="sm-title">Change Profile Picture</h4>
            </div>
            <div class="modal-body">
                <?php echo $this->Form->create('Profile', array('controller' => 'profiles', 'action' => 'change_photo', 'class' => 'form-horizontal form-custom', 'type' => 'file', 'method' => 'post'));?>
                <div class="form-group">
                    <div class="col-sm-12">
                        <?php echo $this->Form->input('Profile.profile_pic_url', array('type' => 'file', 'class' => 'form-control', 'label' => false, 'error' => false, 'div' => 'false', 'required' => false));?>
                        <i class="">Image resolution should be 400 X 400 (Recommended)</i>
                        <?php echo $this->Form->error('Profile.profile_pic_url');?>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-12">
                        <button class="btn btn-theme">Upload Now</button>
                    </div>
                </div>
                <?php echo $this->Form->end();?>
            </div>
        </div>
    </div>
</div>
<!-- Modal For range Waiting List -->
