<?php
/**
 * @var $this View
 */
?>
<style>
    label{
        cursor: pointer;
    }
</style>
<h2 class="page-title">Email Notification</h2>
<hr class="shadow-line"/>


<div class="row">
    <div class="col-md-8 col-md-offset-2 col-sm-12">
        <div class="d-box padding-15 margin-top-25 margin-bottom-25">
            <h4 class="d-title padding-bottom-10"><i class="fa fa-wrench danger"></i> Setting Email Notification</h4>
            <hr class="shadow-line"/>
            <?php if($events):?>
            <form class="form-horizontal form-custom" method="post">

                <div class="form-group">
                    <label class="col-lg-3 control-label">Send email to student</label>
                    <div class="col-lg-9">
                        <br/>
                        <?php foreach($events as $event):?>
                            <div class="input-group">
                                <label>
                                    <input class="emailNotificationSettings" type="checkbox" <?php if($event['EmailsEventsSetting']['value'] == 1){echo 'checked';} ?> id="<?php echo $event['EmailsEventsSetting']['id']?>" name="<?php echo $event['EmailsEventsSetting']['id']?>" value="<?php echo $event['EmailsEventsSetting']['value']?>">
                                    <?php echo $event['EmailsEvent']['title'];?>
                                </label>
                            </div>
                        <?php endforeach;?>
                    </div>
                </div>
                <?php else:?>
                    <?php echo $this->element('not-found')?>
                <?php endif;?>
        </div>
    </div>
</div>


<script>
    $('.emailNotificationSettings').click(function(){
        var emailNotificationSetting = $(this);
        var emailNotificationSettingID = emailNotificationSetting.attr('id');

        var isChecked = emailNotificationSetting.is(':checked');
        if(isChecked == true){
            var emailNotificationSettingValue = 1;
        }
        else{
            var emailNotificationSettingValue = 0;
        }

        var emailNotification = {id: emailNotificationSettingID, value: emailNotificationSettingValue};

        $.ajax({
            url: '<?php echo Router::url('/', true);?>/instructor/notifications/saveNotifications',
            type: "POST",
            dataType: "json",
            data: emailNotification,
            success: function (data) {
                console.log(data);

                if(data == 1){
                    manageFlashMessage('alert-success', 'Your email notification setting has been updated');
                }
                if(data == 0){
                    manageFlashMessage('alert-danger', 'Sorry, something went wrong');
                }
                if(data == 'mandatory'){
                    emailNotificationSetting.prop( "checked", true );
                    manageFlashMessage('alert-danger', 'Sorry, this is mandatory email notification you can not change it');
                }
            },
            error: function (xhr, status, error) {
                manageFlashMessage('alert-danger', 'Sorry, something went wrong');
            }
        });
    });
</script>