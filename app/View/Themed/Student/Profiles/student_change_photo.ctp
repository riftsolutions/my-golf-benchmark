<?php
/**
 * @var $this View
 */
?>
<h2 class="page-title">Change Profile Photo</h2>
<hr class="shadow-line"/>
<div class="row">
    <div class="col-md-8 col-md-offset-2 col-sm-12">
        <div class="box box-padding-adding radius-5 margin-top-25 margin-bottom-25">
            <?php echo $this->Form->create('Profile', array('controller' => 'profiles', 'action' => 'change_photo', 'class' => 'form-horizontal form-custom', 'type' => 'file', 'method' => 'post'));?>
                <div class="form-group">
                    <div class="col-sm-8 col-sm-offset-2">
                        <?php echo $this->Form->input('Profile.profile_pic_url', array('type' => 'file', 'class' => 'form-control', 'label' => false, 'error' => false, 'div' => 'false', 'required' => false));?>
                        <i class="">Image resolution must be 400 X 400</i>
                        <?php echo $this->Form->error('Profile.profile_pic_url');?>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-8 col-sm-offset-2">
                        <button class="btn btn-theme">Upload Now</button>
                    </div>
                </div>
            <?php echo $this->Form->end();?>
        </div>
    </div>
</div>