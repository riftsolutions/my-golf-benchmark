<?php
/**
 * @var $this View
 */
?>
<h2 class="page-title">Instructor Details</h2>
<hr class="shadow-line"/>
<div class="row">
    <div class="col-lg-6 col-md-6 col-sm-6">
        <div class="d-box">
            <div class="row">
                <div class="col-lg-5 col-md-6 col-sm-6">
                    <?php

                    if($userInformation['Profile']['profile_pic_url']){
                        echo $this->Html->image('profiles/'.$userInformation['Profile']['profile_pic_url'], array('class' => 'img-thumbnail profile-pic-small', 'url' => array('controller' => 'users', 'action' => 'details', $userInformation['User']['uuid'])));
                    }
                    else{
                        echo $this->Html->image('avatar.jpg', array('class' => 'img-thumbnail profile-pic-small', 'url' => array('controller' => 'users', 'action' => 'details', $userInformation['User']['uuid'])));
                    }
                    ?>
                </div>
                <div class="col-lg-7 col-md-6 col-sm-6">
                    <ul class="data-list">
                        <li>
                        <span class="name-lg">
                            <?php  echo $this->Html->link($userInformation['Profile']['name'], array('controller' => 'users', 'action' => 'details', $userInformation['User']['uuid']))?>
                        </span>
                        </li>

                        <li>
                            <strong><i class="fa fa-envelope"></i> Email: </strong>
                        <span>
                            <?php echo $this->Html->link($userInformation['User']['username'], array('controller' => 'users', 'action' => 'details', $userInformation['User']['uuid']))?>
                        </span>
                        </li>

                        <?php if($userInformation['Profile']['phone']): ?>
                            <li>
                                <strong><i class="fa fa-phone"></i> Phone Number:</strong>
                        <span>
                            <?php echo $userInformation['Profile']['phone'];?>
                        </span>
                            </li>
                        <?php endif;?>

                        <?php if($userInformation['Profile']['city']): ?>
                            <li>
                                <strong><i class="fa fa-globe"></i> City:</strong>
                        <span>
                            <?php echo $userInformation['Profile']['city'];?>
                        </span>
                            </li>
                        <?php endif;?>

                        <?php if($userInformation['Profile']['postal_code']): ?>
                            <li>
                                <strong><i class="fa fa-globe"></i> Postal Code:</strong>
                        <span>
                            <?php echo $userInformation['Profile']['postal_code'];?>
                        </span>
                            </li>
                        <?php endif;?>

                        <?php if($userInformation['Profile']['state']): ?>
                            <li>
                                <strong><i class="fa fa-globe"></i> State:</strong>
                        <span>
                            <?php echo $userInformation['Profile']['state'];?>
                        </span>
                            </li>
                        <?php endif;?>

                        <?php if($userInformation['Profile']['street_1']): ?>
                            <li>
                                <strong><i class="fa fa-home"></i> Address:</strong>
                        <span>
                            <?php echo $userInformation['Profile']['street_1'];?>
                        </span>
                            </li>
                        <?php endif;?>

                        <li>
                            <strong><i class="fa fa-user"></i> Member Since:</strong>
                            <span><?php echo $this->Time->format('M d, Y', $userInformation['User']['created'])?></span>
                        </li>
                    </ul>
                </div>
                <div class="clearfix"></div>
                <div class="col-lg-12">
                    <?php if($userInformation['Profile']['biography']):?>
                        <div class="margin-top-15">
                            <hr class="shadow-line">
                            <h2 class="short-title theme-color"><strong>Biography:</strong></h2>
                            <i class="italic-sm">
                                <?php echo $userInformation['Profile']['biography'];?>
                            </i>
                        </div>
                    <?php endif;?>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-6"></div>
</div>