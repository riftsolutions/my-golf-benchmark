<?php
/**
 * @var $this View
 */
?>
<h2 class="page-title">Payment</h2>
<hr class="shadow-line"/>
<div class="row">
    <div class="col-md-8 col-md-offset-2 col-sm-12">
        <div class="box box-padding-adding radius-5 margin-top-25 margin-bottom-25">
            <div class="payment">
                    <?php echo $this->Form->create('Billing', array('controller' => 'billings', 'action' => 'payment/'. $package['Package']['uuid'], 'class' => 'form-horizontal'));?>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Payment Through</label>
                        <div class="col-sm-9">
                            <div class="radio">
                                <label>
                                    <input type="radio" name="payment" onclick="paymentThroughCash()">
                                    Cash or Cheque
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="payment" onclick="paymentThroughCard()">
                                    Credit or Debit Card
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group" id="cardPayment" style="display: block;">
                        <label class="col-sm-3 control-label">Credit Card</label>
                        <div class="col-sm-9">
                            <?php echo $this->Html->image('payment.jpg', array('class' => 'img-responsive'));?>
                            <input type="text" class="form-control" placeholder="Cardholder's Name">
                            <div class="row">
                                <div class="col-lg-9 col-md-9 col-sm-9">
                                    <input type="text" class="form-control" placeholder="Card Number (e.g xxxx-xxxx-xxxx-xxxx)">
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3">
                                    <input type="text" class="form-control" placeholder="CVS">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6 col-md-4 col-sm-6">
                                    <select class="form-control">
                                        <option>-- Select Year --</option>
                                        <option>2015</option>
                                        <option>2016</option>
                                        <option>2017</option>
                                        <option>2018</option>
                                        <option>2019</option>
                                    </select>
                                </div>
                                <div class="col-lg-6 col-md-4 col-sm-6">
                                    <select class="form-control">
                                        <option>-- Select Month --</option>
                                        <option>January</option>
                                        <option>January</option>
                                        <option>January</option>
                                        <option>January</option>
                                        <option>January</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group" id="cashPayment" style="display: none;">
                        <label class="col-sm-3 control-label">Cash</label>
                        <div class="col-sm-9">
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-dollar"></i></span>
                                        <input type="text" class="form-control" placeholder="amount">
                                        <span class="input-group-addon" style="border-left: none;border-right: none;">.</span>
                                        <input type="text" class="form-control" placeholder=".00" style="width: 50px;">
                                    </div>
                                </div>
                            </div>
                            <div class="row margin-top-5">
                                <div class="col-lg-12">
                                    <textarea class="form-control" rows="5" placeholder="Place some notes!"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group" style="" id="paymentButton">
                        <div class="col-lg-offset-3 col-md-offset-3 col-sm-3">
                            <button  class="btn btn-theme">Pay Now</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    function paymentThroughCash()
    {
        $('#cardPayment').fadeOut(500);
        $('#cashPayment').fadeIn(500);
        $('#paymentButton').fadeIn();
    }
    function paymentThroughCard()
    {
        $('#cashPayment').fadeOut(500);
        $('#cardPayment').fadeIn(500);
        $('#paymentButton').fadeIn();
    }
</script>