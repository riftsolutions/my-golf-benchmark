<?php
/**
 * @var $this View
 */
?>
<style>
    .form-group {
        clear: both;
    }

    .d-box {
        min-height: 100px;
    }
</style>

<?php
if($this->Session->read('cardValidationError') == true){
    $oldCard = 'style="display: none;"';
    $newCard = 'style="display: inline;"';

    $payWithNewBtn = 'style="display: none;"';
    if($cards){
        $payWithExistingBtn = 'style="display: inline;"';
    }
    else{
        $payWithExistingBtn = 'style="display: none;"';
    }
}
elseif(empty($cards)){
    $oldCard = 'style="display: none;"';
    $newCard = 'style="display: inline;"';

    $payWithExistingBtn = 'style="display: none;"';
    $payWithNewBtn = 'style="display: none;"';
}
else{
    $oldCard = 'style="display: inline;"';
    $newCard = 'style="display: none;"';

    $payWithExistingBtn = 'style="display: none;"';
    $payWithNewBtn = 'style="display: inline;"';
}

?>

<h2 class="page-title">Payment</h2>
<hr class="shadow-line"/>
<div class="row">
    <div class="col-lg-6 col-md-6">
        <div class="d-box">
            <h4 class="d-title"><i class="fa fa-check green"></i> Review</h4>
            <table class="table margin-top-25">
                <thead>
                <tr>
                    <th>Instructor</th>
                    <th>Title</th>
                    <th><span class="pull-right">Sub total</span></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($packages as $package):?>
                <tr>
                    <td class="padding-left-0 padding-right-0">
                        <?php echo $instructorInfo['Profile']['name']?>
                    </td>
                    <td class="padding-left-0 padding-right-0">
                        <?php echo $this->Html->link($package['Package']['name'], array('controller' => 'package', 'action' => 'view', $package['Package']['uuid']))?>
                    </td>
                    <td class="padding-left-0 padding-right-0">
                        <span class="pull-right"><?php echo $this->Number->currency($package['Package']['price']);?></span>
                    </td>
                </tr>
                <?php endforeach;?>
                </tbody>
            </table>
            <div class="subtoal">
                <ul class="data-list">
                    <li>
                        <strong>Sub Total:</strong>
                        <?php echo $this->Number->currency($cost['subtotal']);?>
                    </li>
                    <li>
                        <strong>Tax:</strong>
                        <?php echo $this->Number->currency($cost['tax']);?>
                    </li>
                    <li>
                        <strong>Total</strong>
                        <?php echo $this->Number->currency($cost['total']);?>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="col-lg-6 col-md-6">
        <div class="d-box">
            <?php echo $this->Form->create('Billing', array('controller' => 'billings', 'action' => 'payment'));?>
            <?php if($cards):?>
                <div class="exiesting-card" <?php echo $oldCard;?>>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Pay With</label>
                        <div class="col-sm-9">
                            <select class="form-control" name="existingCard">
                                <option>Select Card</option>
                                <?php foreach($cards as $card):?>
                                    <option value="<?php echo $card['Card']['id']?>">
                                        <?php echo $card['Card']['name']?>
                                        (<?php echo $card['Card']['identifier']?>)
                                    </option>
                                <?php endforeach;?>
                            </select>
                            <div><?php echo $this->Form->error('Card.cardID');?></div>
                        </div>
                    </div>
                </div>
            <?php endif;?>
            <div class="add-new-card" <?php echo $newCard;?>>
                <div class="form-group">
                    <label class="col-sm-3 control-label"></label>
                    <div class="col-sm-9">
                        <?php echo $this->Html->image('payment.jpg', array('class' => 'img-responsive'));?>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Card Name</label>
                    <div class="col-sm-9">
                        <?php echo $this->Form->input('Card.cardName', array('type' => 'text', 'label' => false, 'required' => false, 'class' => 'form-control', 'placeholder' => 'Name on Card'))?>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Card Number</label>
                    <div class="col-sm-9">
                        <?php echo $this->Form->input('Card.cardNumber', array('type' => 'text', 'label' => false, 'required' => false, 'class' => 'form-control', 'placeholder' => 'Card Number'))?>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Expiration Year</label>
                    <div class="col-sm-9">
                        <?php
                        echo $this->Form->input('Card.ExpireYear', array(
                                'type' => 'select',
                                'label' => false,
                                'required' => false,
                                'empty' => 'Select Year',
                                'class' => 'form-control',
                                'options' => array(
                                    '2015' => '2015',
                                    '2016' => '2016',
                                    '2017' => '2017',
                                    '2018' => '2018',
                                    '2019' => '2019',
                                    '2020' => '2020',
                                )
                            ))
                        ?>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Expiration Month</label>
                    <div class="col-sm-9">
                        <?php
                        echo $this->Form->input('Card.ExpireMonth', array(
                                'type' => 'select',
                                'label' => false,
                                'required' => false,
                                'empty' => 'Select Year',
                                'class' => 'form-control',
                                'options' => array(
                                    '01' => 'January (01)',
                                    '02' => 'February (02)',
                                    '03' => 'March (03)',
                                    '04' => 'April (04)',
                                    '05' => 'May (05)',
                                    '06' => 'June (06)',
                                    '07' => 'Jule (07)',
                                    '08' => 'August (08)',
                                    '09' => 'September (09)',
                                    '10' => 'October (10)',
                                    '11' => 'November (11)',
                                    '12' => 'December (12)',
                                )
                            ))
                        ?>
                        <div><?php echo $this->Form->error('Card.Expiry');?></div>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>
            <div class="form-group">
                <div class="col-sm-3">
                    <button  class="btn btn-theme">Submit Payment</button>
                </div>
                <div class="col-sm-9">
                    <a class="d-btn pull-right" id="new-card-btn" <?php echo $payWithNewBtn;?>>Pay by new card</a>
                    <a class="d-btn pull-right" id="old-card-btn" <?php echo $payWithExistingBtn;?>>Pay With Existing</a>
                </div>
            </div>
            <div class="clearfix"></div>

            <?php echo $this->Form->end();?>
        </div>
    </div>
</div>


<script>
    $(document).ready(function(){
        $('#new-card-btn').click(function(){
            $('#new-card-btn').hide();
            $('.exiesting-card').hide();
            $('#old-card-btn').show();
            $('.add-new-card').show();
        });

        $('#old-card-btn').click(function(){
            $('#old-card-btn').hide();
            $('.add-new-card').hide();

            $('#new-card-btn').show();
            $('.exiesting-card').show();
        });
    });
</script>