<?php
/**
 * @var $this View
 */
?>
<h2 class="page-title">Checkout</h2>
<hr class="shadow-line"/>
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="box box-padding-normal">
            <div class="row">
                <div class="col-lg-6  col-md-6 col-sm-12">
                    <div class="margin-top-25">
                        <ul class="data-list">
                            <li>
                                <b>Invoice ID:</b>
                            </li>
                            <li class="green">
                                #<?php echo sprintf('%011d', $invoiceID);;?>
                            </li>
                        </ul>
                        <ul class="data-list">
                            <li>
                                <strong>Payment To:</strong><br>
                            </li>
                            <li>
                                <strong class="u-p"><?php echo $instructorInfo['Profile']['name']?></strong>

                                <?php
                                if($instructorInfo['Profile']['phone']){
                                    echo '<br/>'.$instructorInfo['Profile']['phone'];
                                }
                                elseif($instructorInfo['Profile']['fax']){
                                    echo '<br/>'.$instructorInfo['Profile']['fax'];
                                }

                                if($instructorInfo['Profile']['street_1']){
                                    echo '<br/>'.$instructorInfo['Profile']['street_1'];
                                }
                                elseif($instructorInfo['Profile']['street_2']){
                                    echo '<br/>'.$instructorInfo['Profile']['street_2'];
                                }

                                echo '<br/>';

                                if($instructorInfo['Profile']['city']){
                                    echo $instructorInfo['Profile']['state'].', ';
                                }

                                if($instructorInfo['Profile']['state']){
                                    echo $instructorInfo['Profile']['state'].', ';
                                }

                                if($instructorInfo['Profile']['postal_code']){
                                    echo $instructorInfo['Profile']['postal_code'];
                                }

                                echo '<br/>';

                                if($instructorInfo['User']['username']){
                                    echo '<a class="green">'.$instructorInfo['User']['username'].'</a>';
                                }
                                ?>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="col-lg-6  col-md-6 col-sm-12">
                    <div class="margin-top-25">
                        <ul class="data-list" style="text-align: right">
                            <li>
                                <strong>Date of Invoice:</strong><br>
                                <span>
                                    <?php echo date('M d, Y, H:i A');?>
                                </span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <table class="table margin-top-25">
                        <thead>
                        <tr>
                            <th>Instructor</th>
                            <th>Title</th>
                            <th>No. of Lesson</th>
                            <th><span class="pull-right">Sub total</span></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach($packages as $package):?>
                            <tr>
                                <td class="padding-left-0 padding-right-0">
                                    <?php echo $instructorInfo['Profile']['name']?>
                                </td>
                                <td class="padding-left-0 padding-right-0">
                                    <?php echo $this->Html->link($package['Package']['name'], array('controller' => 'package', 'action' => 'view', $package['Package']['uuid']))?>
                                </td>
                                <td>
                                    <?php echo $package['Package']['lesson'];?>
                                </td>
                                <td class="padding-left-0 padding-right-0">
                                    <span class="pull-right"><?php echo $this->Number->currency($package['Package']['price']);?></span>
                                </td>
                            </tr>
                        <?php endforeach;?>
                        </tbody>
                    </table>
                    <div class="subtoal">
                        <ul class="data-list">
                            <li>
                                <strong>Total:</strong>
                                <?php echo $this->Number->currency($cost['subtotal']);?>
                            </li>
                            <li>
                                <strong>Tax:</strong>
                                <?php echo $this->Number->currency($cost['tax']);?>
                            </li>
                            <li>
                                <strong>Total</strong>
                                <?php echo $this->Number->currency($cost['total']);?>
                            </li>
                        </ul>
                    </div>
                    <?php
                    echo $this->Html->link(
                        'Process Payment',
                        array(
                            'controller' => 'billings',
                            'action' => 'payment',
                        ),
                        array(
                            'class' => 'btn btn-theme',
                            'escape' => false
                        )
                    );
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
