<?php
/**
 * @var $this View
 */
?>
<h2 class="page-title"><?php echo $title; ?></h2>
<hr class="shadow-line"/>
    <form action="<?php echo Router::url('/', true);?>student/benchmarks/list" method=get class="form-inline pull-right margin-left-15">
        <div class="form-group">
            <input type="text" name="benchmark" class="form-control" placeholder="Search benchmark...">
        </div>
        <div class="form-group">
            <input type="submit" class="form-control btn btn-default" value="Search">
        </div>
    </form>
<?php if($benchmarkList):?>
<div class="table-responsive">
    <table class="table data-table table-bordered sort-table">
        <thead>
        <tr>
            <th><?php echo $this->Paginator->sort('Package.name', 'Name of Package'); ?></th>
            <th><?php echo $this->Paginator->sort('Profile.name', 'Student Name'); ?></th>
            <th><?php echo $this->Paginator->sort('Profile.lesson_no', 'Lesson Number'); ?></th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach($benchmarkList as $benchmark):?>
        <tr>
            <td>
                <?php echo $this->Html->link($benchmark['Package']['name'], array('controller' => 'packages', 'action' => 'view', $benchmark['Package']['uuid']), array('class' => 'green'));?>
            </td>
            <td>
                <?php echo $this->Html->link($benchmark['Profile']['first_name'].' '. $benchmark['Profile']['last_name'], array('controller' => 'users', 'action' => 'view', $benchmark['User']['uuid']), array('class' => 'green'));?>
            </td>
            <td>
                <?php echo $benchmark['Benchmark']['lesson_no'];?>
            </td>
            <td>
                <?php echo $this->Html->link('See Details', array('controller' => 'benchmarks', 'action' => 'view', $benchmark['Benchmark']['uuid']), array('class' => 'd-btn'))?>
            </td>
        </tr>
        <?php endforeach;?>
        </tbody>
    </table>
    <?php echo $this->element('pagination')?>
</div>
<?php else:?>
    <h4 class="not-found">Sorry, benchmark not found</h4>
<?php endif;?>