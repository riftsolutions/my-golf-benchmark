<?php
/**
 * @var $this View
 */
?>
<h2 class="page-title"><?php echo $package['Package']['name'];?></h2>
<hr class="shadow-line"/>
<div class="pull-right">
    <?php
    if($package['PackagesUser']['status'] == 1){
        echo '<button class="status-button status-green">Package Status: Running</button>';
    }
    elseif($package['PackagesUser']['status'] == 2){
        echo '<button class="status-button status-info">Package Status: Completed</button>';
    }
    elseif($package['PackagesUser']['status'] == 3){
        echo '<button class="status-button status-danger">Package Status: Expired</button>';
    }
    else{
        echo '<button class="status-button status-gray">Package Status: N/A</button>';
    }
    echo '<br/>';
    echo '<br/>';
    $this->Utilities->packageStatusButton($package['PackagesUser']['payment_status']);
    ?>
</div>
<div class="clearfix"></div>
<div class="row">
    </div>
    <div class="col-md-8 col-md-offset-2 col-sm-12">

        <div class="clearfix"></div>
        <div class="box box-padding-adding radius-5 margin-top-25 margin-bottom-25">

            <h2 class="page-title"><?php echo $package['Package']['name']?></h2>
            <ul class="d-list">
                <li>
                    <?php echo $package['Package']['description']?>
                </li>
                <li><strong>Number of lesson:</strong> <?php echo $package['Package']['lesson']?></li>
                <li>
                    <strong>Price:</strong> <?php echo $this->Number->currency($package['Package']['total']);?>
                    <i class="italic-sm">(Including tax <?php echo $this->Number->currency($package['Package']['tax'])?>)</i>
                </li>
                <li><strong>Available lesson:</strong> <?php echo $package['PackagesUser']['available']?></li>
                <li>
                    <strong>Total Benchmark:</strong> <span style="padding: 5px 8px;" class="d-badge d-badge-info"><?php echo $package['PackagesUser']['count_benchmark']?></span>
                    <?php
                    if($package['PackagesUser']['count_benchmark'] > 0){
                        echo $this->Html->link('View all Benchmark', array('controller' => 'benchmarks', 'action' => 'list', $package['Package']['uuid']), array('class' => 'btn d-btn'));
                    }
                    ?>
                </li>
                <li>
                    <strong>Instructor Name:</strong> <?php echo $this->Html->link($package['Instructor']['first_name'].  ' '. $package['Instructor']['last_name'], array('controller' => 'users', 'action' => 'view', $package['InstructorUsers']['uuid']));?>
                </li>
                <li>
                    <strong>Expire Date:</strong> <?php echo $this->Time->format('M d, Y', $package['PackagesUser']['expiration_date']);?>
                    <i class="italic-sm">( <?php echo $this->Time->format('H:i A', $package['PackagesUser']['expiration_date']);?>)</i>
                </li>
            </ul>
            <?php
            if($package['PackagesUser']['payment_status'] == 0){
                echo $this->Html->link('Pay Now', array('controller' => 'billings', 'action' => 'checkout', $package['Package']['uuid']), array('class' => 'btn btn-theme pull-right'));
            }
            ?>
            <div class="clearfix"></div>
        </div>
    </div>

</div>