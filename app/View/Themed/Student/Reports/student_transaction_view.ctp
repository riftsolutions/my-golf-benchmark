<?php
/**
 * @var $this View
 */
?>
<h2 class="page-title">Transaction Details</h2>
<hr class="shadow-line"/>
<div class="row">
    <div class="col-lg-6 col-md-8 col-sm-12">
        <div class="clearfix"></div>
        <div class="d-box-md">
            <h4 class="d-title"><i class="fa fa-money purple"></i> Transaction Details</h4>
            <ul class="d-list">
                <li>
                    <strong>Student Name:</strong>
                    <?php
                    echo $this->Html->link($transactionDetails[0]['studentName'], array('controller' => 'users', 'action' => 'details', $transactionDetails['User']['uuid']));
                    ?>
                </li>
                <li>
                    <strong>Email:</strong>
                    <?php
                    echo $transactionDetails['User']['username'];
                    ?>
                </li>
                <li>
                    <strong>Transaction ID:</strong>
                    <?php
                    echo $transactionDetails['Billing']['payment_transaction_id'];
                    ?>
                </li>

                <li>
                    <strong>Tax:</strong>
                    <?php
                    echo $this->Number->currency($transactionDetails['Billing']['tax']);
                    ?>
                </li>
                <li>
                    <strong>Subtotal:</strong>
                    <?php
                    echo $this->Number->currency($transactionDetails['Billing']['subtotal']);
                    ?>
                </li>

                <li>
                    <strong>Transaction Amount:</strong>
                    <?php
                    echo $this->Number->currency($transactionDetails['Billing']['amount']);
                    ?>
                </li>

                <li>
                    <strong>Transaction Card Number:</strong>
                    <?php
                    echo $transactionDetails['Billing']['payment_account_number'];
                    ?>
                </li>

                <li>
                    <strong>Transaction Type:</strong>
                    <?php
                    if($transactionDetails['Billing']['payment_transaction_type'] == 'auth_capture'){
                        echo '<label class="label label-success">purchased items</label>';
                    }
                    elseif($transactionDetails['Billing']['payment_transaction_type'] == 'refund'){
                        echo '<label class="label label-danger">refunded</label>';
                    }
                    else{
                        echo '<label class="label label-warning">N/A</label>';
                    }
                    ?>
                </li>

                <li>
                    <strong>Payment Status:</strong>
                    <?php
                    if($transactionDetails['Billing']['payment_final_status'] == 1){
                        echo '<label class="label label-success">payment complete</label>';
                    }
                    elseif($transactionDetails['Billing']['payment_final_status'] == 2){
                        echo '<label class="label label-default">N/A</label>';
                    }
                    ?>
                </li>

                <li>
                    <strong>Date:</strong>
                    <?php
                    echo $this->Time->format('M d, Y', $transactionDetails['Billing']['created'])
                    ?>
                    - <i class="italic-sm">(at <?php
                        echo $this->Time->format('H:i A', $transactionDetails['Billing']['created'])
                        ?>)</i>
                </li>
            </ul>
            <br/>
        </div>
    </div>
    <div class="col-lg-6 col-md-8 col-sm-12">
        <div class="clearfix"></div>
        <div class="d-box">
            <h4 class="d-title padding-bottom-10"><i class="fa fa-asterisk danger"></i> Package List</h4>

            <table class="table margin-top-10">
                <thead>
                <tr><th>Package Name</th>
                    <th>Price</th>
                    <th>Tax</th>
                    <th>Total</th>
                    <th>Action</th>
                </tr></thead>
                <tbody>
                <?php foreach ($items as $item): ?>
                    <tr>
                        <td>
                            <?php echo $this->Html->link($item['Package']['name'], array('controller' => 'packages', 'action' => 'view', $item['Package']['uuid']), array('class' => 'green'));?>
                        </td>
                        <td>
                            <?php echo $this->Number->currency($item['Package']['price']);?>
                        </td>
                        <td>
                            <?php echo $this->Number->currency($item['Package']['tax']);?>
                            <i class="italic-md u-l">(Tax <?php echo $item['Package']['tax_percentage']?>%)</i>
                        </td>
                        <td>
                            <?php echo $this->Number->currency($item['Package']['total']);?>
                        </td>
                        <td>
                            <?php echo $this->Html->link('Details', array('controller' => 'packages', 'action' => 'view', $item['Package']['uuid']), array('class' => 'btn d-btn'));?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
            <div class="clearfix"></div>
        </div>
    </div>
</div>