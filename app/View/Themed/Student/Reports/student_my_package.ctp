<?php
/**
 * @var $this View
 */
?>
<h2 class="page-title"><?php echo $title; ?></h2>
<hr class="shadow-line"/>

<div class="dropdown margin-bottom-5">
    <button class="btn btn-info dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown">
        Quick Navigation
        <span class="caret"></span>
    </button>
    <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
        <li role="presentation">
            <?php echo $this->Html->link('All Item (' .$posOverview[0][0]['totalPOS']. ')', array('controller' => 'reports', 'action' => 'my_package'));?>
        </li>
        <li role="presentation" class="divider"></li>
        <li role="presentation">
            <?php echo $this->Html->link('Unpaid Item (' .$posOverview[0][0]['unpaidPOS']. ')', array('controller' => 'reports', 'action' => 'my_package', 'unpaid'));?>
        </li>
        <li role="presentation" class="divider"></li>
        <li role="presentation">
            <?php echo $this->Html->link('Paid Item (' .$posOverview[0][0]['paidPOS']. ')', array('controller' => 'reports', 'action' => 'my_package', 'paid'));?>
        </li>
        <li role="presentation" class="divider"></li>
        <li role="presentation">
            <?php echo $this->Html->link('Running Item (' .$posOverview[0][0]['countRunningPOS']. ')', array('controller' => 'reports', 'action' => 'my_package', 'running'));?>
        </li>
        <li role="presentation" class="divider"></li>
        <li role="presentation">
            <?php echo $this->Html->link('Completed Item (' .$posOverview[0][0]['countCompletedPOS']. ')', array('controller' => 'reports', 'action' => 'my_package', 'completed'));?>
        </li>
        <li role="presentation" class="divider"></li>
        <li role="presentation">
            <?php echo $this->Html->link('Expired Item (' .$posOverview[0][0]['countExpiredPOS']. ')', array('controller' => 'reports', 'action' => 'my_package', 'expired'));?>
        </li>
    </ul>
</div>

<div class="table-responsive">
    <?php if ($packages): ?>
        <table class="table data-table table-bordered sort-table" colspecing=0>
            <thead>
            <tr>
                <th><?php echo $this->Paginator->sort('Package.name', 'Name of Package'); ?></th>
                <th><?php echo $this->Paginator->sort('Package.price', 'Price'); ?></th>
                <th><?php echo $this->Paginator->sort('Package.tax', 'Tax'); ?></th>
                <th><?php echo $this->Paginator->sort('Package.total', 'Total'); ?></th>
                <th><?php echo $this->Paginator->sort('PackagesUser.payment_status', 'payment status'); ?></th>
                <th><?php echo $this->Paginator->sort('PackagesUser.status', 'status'); ?></th>
                <th><?php echo $this->Paginator->sort('Package.lesson', 'Available Lesson'); ?></th>
                <th><?php echo $this->Paginator->sort('Package.count_benchmark', 'Total Benchmark'); ?></th>
                <th><?php echo $this->Paginator->sort('PackagesUser.expiration_date', 'Expire Date'); ?></th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($packages as $package):?>
                <tr>
                    <td>
                        <?php echo $this->Html->link($package['Package']['name'], array('controller' => 'packages', 'action' => 'view', $package['Package']['uuid']), array('class' => 'green'));?>
                    </td>
                    <td>
                        <?php echo $this->Number->currency($package['Package']['price']);?>
                    </td>
                    <td>
                        <?php echo $this->Number->currency($package['Package']['tax']);?>
                        <i class="italic-md u-l">(Tax <?php echo $package['Package']['tax_percentage']?>%)</i>
                    </td>
                    <td>
                        <?php echo $this->Number->currency($package['Package']['total']);?>
                    </td>
                    <td>
                        <?php echo $this->Utilities->managePackageStatus($package['PackagesUser']['payment_status']);?>
                    </td>
                    <td>
                        <?php
                        if($package['PackagesUser']['status'] == 1){
                            echo '<label class="label label-success">running</label>';
                        }
                        elseif($package['PackagesUser']['status'] == 2){
                            echo '<label class="label label-info">completed</label>';
                        }
                        elseif($package['PackagesUser']['status'] == 3){
                            echo '<label class="label label-warning">expired</label>';
                        }
                        else{
                            echo '<label class="label label-default">N/A</label>';
                        }
                        ?>
                    </td>
                    <td>
                        <?php echo $package['PackagesUser']['available'];?>
                    </td>
                    <td>
                        <?php echo $package['PackagesUser']['count_benchmark'];?>
                    </td>
                    <td>
                        <?php echo $this->Time->format('M d, Y', $package['PackagesUser']['expiration_date']);?>
                        <i class="italic-sm">( <?php echo $this->Time->format('H:i A', $package['PackagesUser']['expiration_date']);?>)</i>
                    </td>
                    <td>
                        <?php echo $this->Html->link('Details', array('controller' => 'reports', 'action' => 'my_package_view', $package['PackagesUser']['id']), array('class' => 'btn d-btn'));?>

                        <?php /*echo $this->Html->link('Reassign', array('controller' => 'packages', 'action' => 'assign', $package['Package']['uuid'], $package['User']['id']), array('class' => 'btn d-btn'));*/?>

                        <?php
                        if($package['PackagesUser']['count_benchmark'] > 0){
                            echo $this->Html->link('View all Benchmark', array('controller' => 'benchmarks', 'action' => 'list', $package['Package']['uuid']), array('class' => 'btn d-btn'));
                        }
                        ?>

                    </td>

                </tr>
            <?php endforeach;?>
            </tbody>
        </table>
        <?php echo $this->element('pagination');?>
    <?php else: ?>
        <h2 class="not-found">Sorry, item not found</h2>
        <hr class="shadow-line"/>
    <?php endif; ?>
</div>
<div class="clearfix"></div>