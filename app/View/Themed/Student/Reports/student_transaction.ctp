<?php
/**
 * @var $this View
 */
?>
<h2 class="page-title">My Transaction List</h2>
<hr class="shadow-line"/>
<div class="table-responsive">
    <?php if ($transactions): ?>
        <table class="table data-table table-bordered sort-table" colspecing=0>
            <thead>
            <tr>
                <th><?php echo $this->Paginator->sort('Billing.amount', 'Transaction Amount'); ?></th>
                <th><?php echo $this->Paginator->sort('Billing.payment_transaction_type', 'Transaction Type'); ?></th>
                <th><?php echo $this->Paginator->sort('Billing.payment_account_number', 'Card'); ?></th>
                <th><?php echo $this->Paginator->sort('Billing.created', 'Date'); ?></th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($transactions as $transaction):?>
                <tr>
                    <td>
                        <?php echo $this->Number->currency($transaction['Billing']['amount']);?>
                    </td>
                    <td>
                        <?php
                        if($transaction['Billing']['payment_transaction_type'] == 'auth_capture'){
                            echo '<label class="label label-success">purchased items</label>';
                        }
                        elseif($transaction['Billing']['payment_transaction_type'] == 'refund'){
                            echo '<label class="label label-danger">refunded</label>';
                        }
                        else{
                            echo '<label class="label label-warning">N/A</label>';
                        }
                        ?>
                    </td>
                    <td>
                        <?php echo $transaction['Billing']['payment_account_number'];?>
                    </td>
                    <td>
                        <?php echo $this->Time->format('d M, Y', $transaction['Billing']['created'])?>
                        <i class="italic-sm">(
                            <?php echo $this->Time->format('h:i A', $transaction['Billing']['created'])?>
                            )
                        </i>
                    </td>
                    <td>
                        <?php echo $this->Html->link('Details', array('controller' => 'reports', 'action' => 'student_transaction_view', $transaction['Billing']['uuid']), array('class' => 'd-btn'));?>
                    </td>

                </tr>
            <?php endforeach;?>
            </tbody>
        </table>
        <?php echo $this->element('pagination');?>
    <?php else: ?>
        <h2 class="not-found">Sorry, transaction not found</h2>
        <hr class="shadow-line"/>
    <?php endif; ?>
</div>