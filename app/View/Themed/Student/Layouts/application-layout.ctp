<!----- HEADER AREA ----->
<?php echo $this->element('header');?>
<!----- /HEADER AREA ----->

<!----- SITES CONTENT ----->
<div class="container-fluid" id="content-area">
<div class="row row-offcanvas row-offcanvas-left">
<!----- SIDEBAR AREA ----->
<?php echo $this->element('sidebar');?>
<!----- SIDEBAR AREA ----->

<!----- CONTENT AREA ----->
<div class="col-xs-12 col-sm-10">
    <div class="margin-top-15">
        <?php echo $this->Session->flash();?>
    </div>
    <?php /*echo $this->element('notification');*/?>
    <?php echo $this->element('bread-cumbs');?>

    <div id="message-box" class="alert-success flash-msg alert alert-dismissable">
        <div id="message"></div>
    </div>

    <?php echo $content_for_layout;?>
</div>
<!----- CONTENT AREA ----->
</div>
</div>
<!----- /SITES CONTENT ---->

<!----- FOOTER AREA ----->
<?php echo $this->element('footer');?>
<!----- /FOOTER AREA ----->
