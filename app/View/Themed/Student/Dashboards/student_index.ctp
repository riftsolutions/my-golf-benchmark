<?php
/**
 * @var $this View
 */
?>
<h2 class="page-title">My Dashboard</h2>
<hr class="shadow-line"/>
<div class="content">
    <div class="row">

        <div class="col-lg-6 col-md-6 col-sm-6">
            <!----- APPLICATION OVERVIEW ------>
            <div class="d-box">
                <h4 class="d-title"><i class="fa fa-flask yellowgreen"></i> Application Overview</h4>
                <ul class="d-list">
                    <li>
                        <span class="green">Total Spent</span>
                        <span class="pull-right d-badge d-badge-gray"><?php echo $this->Number->currency($purchaseOverview[0][0]['totalPaid']);?></span>
                    </li>
                    <li>
                        <?php echo $this->Html->link('Total Package Purchase', array('controller' => 'packages', 'action' => 'pos',)); ?>
                        <span class="pull-right d-badge d-badge-info"><?php echo $totalPurchasePackages;;?></span>
                    </li>
                    <li>
                        <?php echo $this->Html->link('Running POS', array('controller' => 'packages', 'action' => 'pos', 'running',)); ?>
                        <span class="pull-right d-badge d-badge-green"><?php echo $countRunningPOS;?></span>
                    </li>
                    <li>
                        <?php echo $this->Html->link('Upcoming Appointment', array('controller' => 'appointments', 'action' => 'list', 'upcoming',)); ?>
                        <span class="pull-right d-badge d-badge-orange"><?php echo sizeof($upComingAppointments);?></span>
                    </li>
                    <li>
                        <?php echo $this->Html->link('Total Benchmark', array('controller' => 'benchmarks', 'action' => 'list',)); ?>
                        <span class="pull-right d-badge d-badge-purple"><?php echo $countBenchmark;?></span>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <!----- /APPLICATION OVERVIEW ------>

            <!----- RECENT BENCHMARK ------>
            <div class="d-box">
                <h4 class="d-title padding-bottom-10"><i class="fa fa-fighter-jet danger"></i> Recent Five Benchmarks</h4>

                <?php if($recentBenchmarks):?>
                    <table class="table margin-top-10">
                        <thead>
                        <th>Package Name</th>
                        <th>Instructor</th>
                        <th>Action</th>
                        </thead>
                        <tbody>
                        <?php foreach($recentBenchmarks as $benchmark):?>
                            <tr>
                                <td>
                                    <?php echo $this->Html->link($benchmark['Package']['name'], array('controller' => 'packages', 'action' => 'view', $benchmark['Package']['uuid']), array('class' => 'green'));?>
                                </td>
                                <td>
                                    <?php echo $this->Html->link($benchmark[0]['instructorName'], array('controller' => 'users', 'action' => 'details', $benchmark['User']['uuid']), array('class' => 'green'));?>
                                </td>
                                <td>
                                    <?php echo $this->Html->link('Details', array('controller' => 'benchmarks', 'action' => 'view', $benchmark['Benchmark']['uuid']), array('class' => 'd-btn'));?>
                                </td>
                            </tr>
                        <?php endforeach;?>
                        </tbody>
                    </table>

                    <div class="see-more">
                        <?php echo $this->html->link('See More <i class="fa fa-long-arrow-right"></i>', array('controller' => 'billings', 'action' => 'list'), array('escape' => false))?>
                    </div>

                <?php else:?>
                    <?php echo $this->element('not-found')?>
                <?php endif;?>

                <div class="clearfix"></div>
            </div>
            <!----- /RECENT BENCHMARK ------>
        </div>

        <div class="col-lg-6 col-sm-6">
            <!----- UPCOMING APPOINTMENT ------>
            <div class="d-box">
                <h4 class="d-title padding-bottom-10"><i class="fa fa-fighter-jet danger"></i> Upcoming/Confirm Five Appointment</h4>

                <?php if($upComingAppointments):?>
                    <table class="table margin-top-10">
                        <thead>
                        <th>Appointment Time</th>
                        <th>Instructor Name</th>
                        <th>Action</th>
                        </thead>
                        <tbody>
                        <?php foreach($upComingAppointments as $appointment):?>
                            <tr>
                                <td>
                                    <?php echo $this->Time->format('d M, Y', $appointment['Appointment']['start'])?>
                                    <i class="italic-sm">(
                                        <?php echo $this->Time->format('h:i A', $appointment['Appointment']['start'])?>
                                        - <?php echo $this->Time->format('h:i A', $appointment['Appointment']['end'])?>
                                        )
                                    </i>
                                </td>
                                <td>
                                    <?php echo $this->Html->link($appointment[0]['instructorName'], array('controller' => 'users', 'action' => 'details', $appointment['InstructorUser']['uuid']), array('class' => 'green'));?>
                                </td>
                                <td>
                                    <?php echo $this->Html->link('View', array('controller' => 'appointments', 'action' => 'view', $appointment['Appointment']['id']), array('class' => 'd-btn'));?>
                                </td>
                            </tr>
                        <?php endforeach;?>
                        </tbody>
                    </table>

                    <div class="see-more">
                        <?php echo $this->html->link('See More <i class="fa fa-long-arrow-right"></i>', array('controller' => 'appointments', 'action' => 'list'), array('escape' => false))?>
                    </div>

                <?php else:?>
                    <?php echo $this->element('not-found')?>
                <?php endif;?>

                <div class="clearfix"></div>
            </div>
            <!----- /UPCOMING APPOINTMENT ------>

            <!----- RECENT TRANSACTION ------>
            <div class="d-box">
                <h4 class="d-title padding-bottom-10"><i class="fa fa-money danger"></i> Recent Five Transactions</h4>

                <?php if($recentTransaction):?>
                    <table class="table margin-top-10">
                        <thead>
                        <th>Appointment Time</th>
                        <th>Amount</th>
                        <th>Action</th>
                        </thead>
                        <tbody>
                        <?php foreach($recentTransaction as $transaction):?>
                            <tr>
                                <td>
                                    <?php echo $this->Time->format('d M, Y', $transaction['Billing']['created'])?>
                                    <i class="italic-sm">(
                                        <?php echo $this->Time->format('h:i A', $transaction['Billing']['created'])?>
                                        )
                                    </i>
                                </td>
                                <td>
                                    <?php echo $this->Number->currency($transaction['Billing']['amount']);?>
                                </td>
                                <td>
                                    <?php echo $this->Html->link('View', array('controller' => 'reports', 'action' => 'transaction_view', $transaction['Billing']['uuid']), array('class' => 'd-btn'));?>
                                </td>
                            </tr>
                        <?php endforeach;?>
                        </tbody>
                    </table>

                    <div class="see-more">
                        <?php echo $this->html->link('See More <i class="fa fa-long-arrow-right"></i>', array('controller' => 'reports', 'action' => 'transaction'), array('escape' => false))?>
                    </div>

                <?php else:?>
                    <?php echo $this->element('not-found')?>
                <?php endif;?>

                <div class="clearfix"></div>
            </div>
            <!----- /RECENT TRANSACTION ------>
        </div>

    </div>
    <!-- Start Modal For User Feeling -->
    <div class="modal event-modal fade" id="userFeelings" tabindex="-1" role="dialog"
         aria-labelledby="setApointmentModalLabel" aria-hidden="true">
        <div class="modal-dialog">

            <form action=" <?php echo Router::url('/', true);?>dashboards/userFeedback" method = "post" class="form-horizontal form-custom">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                        </button>
                        <h4 class="sm-title">Give us your Feedback</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <div class="col-lg-12">
                                <label for="note">Your Feedback</label>
                                <textarea name="data[Feedback][feedback]" class="form-control" required="required" id="note"></textarea>
                                <?php echo $this->Form->error('Feedback.feedback');?>

                            </div>

                        </div>
                        <div class="form-group">
                            <div class="col-sm-6">
                                <div class="input-group">
                                    <label>Rating:</label>
                                    <select class="noteRatings" name="data[Feedback][rating]">
                                        <option value="" selected="selected"></option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                    </select>
                                </div>
                                <?php echo $this->Form->error('UserFeeling.rating');?>
                            </div>
                        </div>
                        <input type="hidden" name="data[Feedback][user_id]" value="<?php echo $userID; ?>">

                        <div class="form-group">
                            <div class="col-sm-12">

                                <input type="submit" class="btn btn-theme pull-left" value="Save">
                                <input type="submit" class="btn  btn-link danger pull-right " name="data[Feedback][no_thanks]" value="No Thanks" id="target">
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <?php echo $this->Form->end(); ?>
        </div>
    </div>
    <!-- End Modal For User Feelings -->

</div>
<?php if($feelingsCount == 0 ): ?>
<script type="text/javascript">
    $(window).load(function(){
        $('#userFeelings').modal('show');
    });
    $('.noteRatings').barrating('show', {
        theme: 'fontawesome-stars',
        initialRating: null
    });

    $( document ).ready(function() {
        //console.log( "ready!" );
    $( "#target" ).click(function() {
        //alert( "Handler for .click() called." );
        $('textarea#note').removeAttr( "required" )
    });
    });

</script>
<?php endif; ?>
