<?php
/**
 * @var $this View
 */
?>
<h2 class="page-title">Point of Sale</h2>
<hr class="shadow-line"/>
<div id="message-box" style="display: none;" class="flash-msg alert alert-dismissable">
    <div id="message"></div>
</div>
<?php
$isHidden = "style='display:none'";
if($selectedPackagesIds){
    $isHidden = "style='display:visible'";
}
?>

<span id="purchase-btn" <?php echo $isHidden; ?>>
    <?php
    echo $this->Html->link('Purchase Items', array('controller' => 'billings', 'action' => 'checkout'), array('class' => 'btn btn-theme pull-right margin-bottom-10 margin-left-10'));

    echo $this->Html->link('Remove all Items', array('controller' => 'packages', 'action' => 'remove_items'), array('class' => 'btn btn-theme-red pull-right margin-bottom-10'));
    ?>
</span>
<div class="clearfix"></div>

<div class="table-responsive">
    <?php if ($packages): ?>
        <table class="table data-table table-bordered sort-table" colspecing=0>
            <thead>
            <tr>
                <th><?php echo $this->Paginator->sort('Package.name', 'Name of Item'); ?></th>
                <th><?php echo $this->Paginator->sort('Package.price', 'Price'); ?></th>
                <!--<th><?php /*echo $this->Paginator->sort('Package.tax', 'Tax'); */?></th>
                <th><?php /*echo $this->Paginator->sort('Package.total', 'Total'); */?></th>-->
                <th><?php echo $this->Paginator->sort('Package.lesson', 'Number of Sessions'); ?></th>
                <th><?php echo $this->Paginator->sort('Package.expiration_days', 'Expiration Days'); ?></th>
                <th>Select</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($packages as $package):?>
                <tr>
                    <td>
                        <?php echo $this->Html->link($package['Package']['name'], array('controller' => 'packages', 'action' => 'view', $package['Package']['uuid']), array('class' => 'green'));?>
                    </td>
                    <td>
                        <?php echo $this->Number->currency($package['Package']['price']);?>
                    </td>
                    <!--<td>
                        <?php /*echo $this->Number->currency($package['Package']['tax']);*/?>
                        <i class="italic-md u-l">(Tax <?php /*echo $package['Package']['tax_percentage']*/?>%)</i>
                    </td>
                    <td>
                        <?php /*echo $this->Number->currency($package['Package']['total']);*/?>
                    </td>-->
                    <td>
                        <?php echo $package['Package']['lesson'];?>
                    </td>
                    <td>
                        <?php echo $package['Package']['expiration_days'];?>
                    </td>
                    <td>
                        <?php
                        $selectedValue = 0;
                        $selectBtn = 'd-btn';
                        $packageText = 'Select Item';
                        if($selectedPackagesIds){
                            if(in_array($package['Package']['id'], $selectedPackagesIds)){
                                $selectedValue = 1;
                                $selectBtn = 'd-btn-green';
                                $packageText = 'Unselect Item';
                            }
                        }
                        ?>
                        <a class="btn <?php echo $selectBtn;?> select-package" is-selected="<?php echo $selectedValue;?>" package-uuid="<?php echo $package['Package']['uuid'];?>"><?php echo $packageText?></a>
                    </td>
                    <td>
                        <?php echo $this->Html->link('Details', array('controller' => 'packages', 'action' => 'view', $package['Package']['uuid']), array('class' => 'btn d-btn'));?>
                    </td>
                </tr>
            <?php endforeach;?>
            </tbody>
        </table>
        <?php echo $this->element('pagination');?>
    <?php else: ?>
        <h2 class="not-found">Sorry, package not found</h2>
        <hr class="shadow-line"/>
    <?php endif; ?>
</div>
<div class="clearfix"></div>


<script>
    $(document).on('click', '.select-package', function(){
        var selectPackage = $(this);
        var packageUUID = $(this).attr('package-uuid');
        var isSelected = $(this).attr('is-selected');
        var packageInfo = {'packageUUID': packageUUID, 'action': isSelected};

        if(isSelected == 0){
            $(this).removeClass('d-btn');
            $(this).addClass('d-btn-green');
            $(this).attr('is-selected', 1);
            $(this).text('Unselect Item');
        }
        else if(isSelected == 1){
            $(this).removeClass('d-btn-green');
            $(this).addClass('d-btn');
            $(this).attr('is-selected', 0);
            $(this).text('Select Item');
        }

        $.ajax({
            url: '<?php echo Router::url('/', true);?>student/packages/choosePackage',
            type: "POST",
            dataType: "json",
            data: {'packageInfo': packageInfo},
            success: function (data) {
                if(data == 1){
                    $(selectPackage).parent().closest('tr').addClass('success');
                    manageFlashMessage('alert-success', 'Item has been selected for purchases')
                }
                else if(data == 0){
                    $(selectPackage).parent().closest('tr').removeClass('success');
                    manageFlashMessage('alert-danger', 'Item has been removed form selected Item list')
                }
            },
            error: function (xhr, status, error) {
                console.log(error);
            }
        });

        $.ajax({
            url: '<?php echo Router::url('/', true);?>student/packages/countPackage',
            type: "POST",
            dataType: "json",
            success: function (data) {
                $('.cartBtn .badge-notify').text(data);
                if(data > 0)
                {
                    $('#purchase-btn').show();
                    $('.cartBtn').show();
                }
                else{
                    $('#purchase-btn').hide();
                    $('.cartBtn').hide();
                }
            },
            error: function (xhr, status, error) {
                console.log(error);
            }
        });
    });
</script>

<script>
    function manageFlashMessage(alertType, message)
    {
        $('#message-box').show();
        $('#message-box').addClass(alertType);
        $('#message').html(message);
        removeMgsArea();
    }
    function removeMgsArea()
    {
        window.setTimeout(function ()
            {
                if($( "#message-box" ).hasClass( "alert-danger" )){
                    $('#message-box').removeClass("alert-danger" );
                }
                if($( "#message-box" ).hasClass( "alert-success" )){
                    $('#message-box').removeClass("alert-success" );
                }
                $("#message-box").hide();
            },
            4000
        );
    }
</script>
