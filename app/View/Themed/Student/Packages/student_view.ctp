<?php
/**
 * @var $this View
 */
?>
<h2 class="page-title">POS Details</h2>
<hr class="shadow-line"/>
<div class="clearfix"></div>
<div class="row">
    <div class="col-md-8 col-md-offset-2 col-sm-12">
        <div class="box box-padding-adding radius-5 margin-top-25 margin-bottom-25">
            <h2 class="page-title"><?php echo $package['Package']['name']?></h2>
            <ul class="d-list">
                <li>
                    <?php echo $package['Package']['description']?>
                </li>
                <li><strong>Number of lesson:</strong> <?php echo $package['Package']['lesson']?></li>
                <li>
                    <strong>Price:</strong> <?php echo $this->Number->currency($package['Package']['total']);?>
                    <i class="italic-sm">(Including <?php echo $package['Package']['tax_percentage'];?>% tax, tax amount <?php echo $this->Number->currency($package['Package']['tax'])?>)</i>
                </li>
                <li>
                    <strong>Expiration Days:</strong> <?php echo $package['Package']['expiration_days'];?> Days
                    <i class="italic-sm">(Expiration starts counting from the time of purchase)</i>
                </li>
            </ul>
            <?php
            $selectedValue = 0;
            $packageText = 'Purchase Item';
            $packageBtn = 'btn btn-theme pull-right';
            if($selectedPackagesIds){
                if(in_array($package['Package']['id'], $selectedPackagesIds)){
                    $selectedValue = 1;
                    $packageText = 'Remove Item';
                    $packageBtn = 'btn btn-black pull-right';
                }
            }
            echo $this->Html->link($packageText, array('controller' => 'packages', 'action' => 'choosePackage', $package['Package']['uuid'], $selectedValue), array('class' => $packageBtn));
            ?>
            <div class="clearfix"></div>
        </div>
    </div>

</div>