<?php
/**
 * @var $this View
 */
?>
<h2 class="page-title">Add New Card</h2>
<hr class="shadow-line"/>
<div class="row">
    <div class="col-md-4 col-md-offset-4 col-sm-12">
        <div class="box box-padding-adding radius-5 margin-top-25 margin-bottom-25">
            <div class="payment">
                <?php echo $this->Form->create('Card', array('controller' => 'cards', 'action' => 'add', 'class' => 'form-horizontal', 'id' => 'payment-form'));?>

                <div class="row">
                    <div class="form-group col-xs-12">
                        <label for="CardCardNumber">CARD NUMBER</label>
                        <?php echo $this->Form->error('Card.cardNumber', array('error-mgs'));?>
                        <div class="input-group">
                            <?php echo $this->Form->input('Card.cardNumber', array('type' => 'tel', 'size' => '20', 'label' => false, 'error' => false, 'required' => true, 'autofocus' => true, 'class' => 'form-control', 'placeholder' => 'Card Number', 'data-stripe' => 'number'))?>
                            <span class="input-group-addon"><i class="fa fa-credit-card"></i></span>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-xs-9">
                        <label for="CardCardNumber">EXPIRATION DATE</label>
                        <?php echo $this->Form->error('Card.ExpireYear', array('error-mgs'));?>
                        <?php echo $this->Form->error('Card.ExpireMonth', array('error-mgs'));?>
                        <div class="row full-width">
                            <div class="col-xs-6 nopadding">
                            <?php
                            echo $this->Form->input('Card.ExpireMonth', array(
                                    'type' => 'select',
                                    'label' => false,
                                    'div' => false,
                                    'required' => false,
                                    'error' => false,
                                    'empty' => 'MM',
                                    'class' => 'form-control',
                                    'options' => array(
                                        '01' => 'Jan (01)',
                                        '02' => 'Feb (02)',
                                        '03' => 'Mar (03)',
                                        '04' => 'Apr (04)',
                                        '05' => 'May (05)',
                                        '06' => 'Jun (06)',
                                        '07' => 'Jul (07)',
                                        '08' => 'Aug (08)',
                                        '09' => 'Sep (09)',
                                        '10' => 'Oct (10)',
                                        '11' => 'Nov (11)',
                                        '12' => 'Dec (12)',
                                    ),
                                    'data-stripe' => 'exp-month'
                                ))
                            ?>
                            </div>
                            <div class="col-xs-6 nopadding">
                            <?php
                            echo $this->Form->input('Card.ExpireYear', array(
                                    'type' => 'select',
                                    'label' => false,
                                    'div' => false,
                                    'required' => false,
                                    'empty' => 'YYYY',
                                    'error' => false,
                                    'class' => 'form-control',
                                    'options' => array(
                                        '2016' => '2016',
                                        '2017' => '2017',
                                        '2018' => '2018',
                                        '2019' => '2019',
                                        '2020' => '2020',
                                        '2021' => '2021',
                                        '2022' => '2022',
                                        '2023' => '2023',
                                        '2024' => '2024',
                                        '2025' => '2025',
                                        '2026' => '2026',
                                        '2027' => '2027',
                                        '2028' => '2028',
                                        '2029' => '2029',
                                        '2030' => '2030',
                                    ),
                                    'data-stripe' => 'exp-year'
                                ))
                            ?>
                            </div>
                        </div>
                        <div><?php echo $this->Form->error('Card.Expiry');?></div>
                    </div>
                    <div class="col-xs-3 col-xs-offset-1">
                        <label for="Cardcvc">CVC</label>
                        <?php echo $this->Form->error('Card.cvc', array('error-mgs'));?>
                        <div class="full-width">
                            <?php echo $this->Form->input('Card.cvc', array('type' => 'text', 'size' => '4', 'label' => false, 'error' => false, 'required' => false, 'class' => 'form-control', 'placeholder' => 'CVC', 'data-stripe' => 'cvc'))?>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-3 col-lg-offset-4">
                        <button  class="btn btn-theme">Save Card</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
<script>
    // This identifies the website in the createToken call below
    Stripe.setPublishableKey('<?php echo STRIPE_PUBLIC;?>');
    
    function stripeResponseHandler(status, response) {
        var $form = $('#payment-form');

        if (response.error) {
            // Show the errors on the form
            $form.find('#form-error').text(response.error.message);
            $form.find('button').prop('disabled', false);
        } else {
            // response contains id and card, which contains additional card details
            var token = response.id;
            // Insert the token into the form so it gets submitted to the server
            $form.append($('<input type="hidden" name="data[stripe][token]" />').val(token));
            // and submit
            $form.get(0).submit();
        }
    };

    $(document).ready(function() {
        $('#payment-form').submit(function(e) {
            e.preventDefault();

            var $form = $(this);

            // Disable the submit button to prevent repeated clicks
            $form.find('#form-error').text('');
            $form.find('button').prop('disabled', true);

            Stripe.card.createToken($form, stripeResponseHandler);

            // Prevent the form from submitting with the default action
            return false;
        });
    });
</script>