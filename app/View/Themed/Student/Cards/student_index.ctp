<?php
/**
 * @var $this View
 */
?>
<h2 class="page-title">My Cards</h2>
<hr class="shadow-line"/>
<div id="message-box" style="position: absolute; width: 400px; z-index: 99999; right: 15px;
top: 15px; display: none;" class="flash-msg alert alert-dismissable">
    <div id="message"></div>
</div>
<?php if($cards):?>

<div class="row">
    <?php foreach($cards as $card):?>
    <div class="col-lg-4">
        <div class="card-box">
            <div class="card">
                <h3 class="card-name"><?php echo $card['brand'];?></h3>
                <h3 class="card-number">xxxx xxxx xxxx <?php echo $card['last4'];?></h3>
                <span class="card-remove-link">
                    <?php
                    echo $this->Form->postLink(
                        '<i class="fa fa-times"></i>',
                        array(
                            'controller' => 'cards',
                            'action' => 'delete',
                            $card['id']
                        ),
                        array(
                            'class' => '',
                            'escape' => false
                        ),
                        __('Are you sure you want to remove this card?')
                    );
                    ?>
                </span>
            </div>
        </div>
    </div>
    <?php endforeach;?>
</div>
<?php else:?>
    <h4 class="not-found">No cards have been added. Please add one.</h4>
<?php endif;?>

<script>
    function manageFlashMessage(alertType, message)
    {
        $('#message-box').show();
        $('#message-box').addClass(alertType);
        $('#message').html(message);
        removeMgsArea();
    }
    function removeMgsArea()
    {
        window.setTimeout(function ()
            {
                if($( "#message-box" ).hasClass( "alert-danger" )){
                    $('#message-box').removeClass("alert-danger" );
                }
                if($( "#message-box" ).hasClass( "alert-success" )){
                    $('#message-box').removeClass("alert-success" );
                }
                $("#message-box").hide();
            },
            4000
        );
    }
</script>