<?php
/**
 * @var $this View
 */
?>
<h2 class="page-title">List of Group Lesson</h2>
<hr class="shadow-line"/>
<div class="table-responsive">
<?php if ($lessons): ?>
    <table class="table data-table table-bordered sort-table" colspecing=0>
    <thead>
    <tr>
        <th><?php echo $this->Paginator->sort('GroupLesson.title', 'Title'); ?></th>
        <th><?php echo $this->Paginator->sort('GroupLesson.price', 'Price'); ?></th>
        <th><?php echo $this->Paginator->sort('GroupLesson.start', 'Time'); ?></th>
        <th><?php echo $this->Paginator->sort('GroupLesson.student_limit', 'Student Limit'); ?></th>
        <th><?php echo $this->Paginator->sort('GroupLesson.filled_up_student', 'Student Filled up'); ?></th>
        <th>Available</th>
        <th>Status</th>
        <th style="width: 10%;">Action</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach($lessons as $lesson):?>
    <tr>
        <td>
            <?php echo $this->Html->link($this->Text->excerpt($lesson['GroupLesson']['title'], 'method', 30, '...'), array('controller' => 'lessons', 'action' => 'details', $lesson['GroupLesson']['id']), array('class' => 'green'));?>
        </td>
        <td>
            <?php echo $this->Number->currency($lesson['GroupLesson']['price']); ?>
        </td>
        <td>
            <?php echo $this->Time->format('M d, Y', $lesson['GroupLesson']['start']);?>
            <span class="italic-sm">( <?php echo $this->Time->format('h:m a', $lesson['GroupLesson']['start'])?> - <?php echo $this->Time->format('h:m a', $lesson['GroupLesson']['end'])?>)</span>
        </td>
        <td>
            <?php echo $lesson['GroupLesson']['student_limit'];?>
        </td>
        <td>
            <?php echo $lesson['GroupLesson']['filled_up_student'];?>
        </td>
        <td>
            <?php echo $lesson['GroupLesson']['student_limit'] - $lesson['GroupLesson']['filled_up_student'];?>
        </td>
        <td>
            <?php
            if($lesson['GroupLesson']['status']){
                echo '<label class="label label-success">Active</label>';
            }
            else{
                echo '<label class="label label-danger">Inactive</label>';
            }
            ?>
        </td>

        <td>
            <div class="icons-area">
                <?php echo $this->Html->link('view details', array('controller' => 'lessons', 'action' => 'details', $lesson['GroupLesson']['id']), array('escape' => false, 'class' => 'd-btn'));?>
            </div>
        </td>
    </tr>
    <?php endforeach;?>
    </tbody>
    </table>
    <?php echo $this->element('pagination');?>
    <?php else: ?>
    <h2 class="not-found">Sorry, group lesson not found</h2>
    <hr class="shadow-line"/>
<?php endif; ?>
</div>
<div class="clearfix"></div>