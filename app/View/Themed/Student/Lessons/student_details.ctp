<?php
/**
 * @var $this View
 */
echo $this->Html->css('kit/style-100');
?>

<h2 class="page-title">Group Lesson Details</h2>
<hr class="shadow-line"/>

<div class="clearfix"></div>
<div class="row">
    <div class="col-lg-4 col-md-3 col-sm-3">
        <div class="d-box">
            <h4 class="d-title margin-bottom-15"><i class="fa fa-info-circle yellowgreen"></i> Group Lesson Information</h4>
            <ul class="d-list">
                <li>
                    <strong>Title:</strong>
                    <span>
                        <?php echo $this->Html->link($groupLessonDetails['GroupLesson']['title'], array('controller' => 'lessons', 'action' => 'details', $groupLessonDetails['GroupLesson']['id'])) ?>
                    </span>
                </li>
                <li>
                    <strong>Description:</strong>
                    <span>
                        <?php echo $groupLessonDetails['GroupLesson']['description']; ?>
                    </span>
                </li>
                <li>
                    <strong>Time:</strong>
                    <?php echo $this->Time->format('M d, Y', $groupLessonDetails['GroupLesson']['start']);?>
                    <span class="italic-sm">( <?php echo $this->Time->format('h:m a', $groupLessonDetails['GroupLesson']['start'])?> - <?php echo $this->Time->format('h:m a', $groupLessonDetails['GroupLesson']['end'])?>)</span>
                </li>
                <li>
                    <strong>Price:</strong>
                    <span>
                        <?php echo $this->Number->currency($groupLessonDetails['GroupLesson']['price']); ?>
                    </span>
                </li>
                <li>
                    <strong>Student Limit:</strong>
                    <span>
                        <?php echo $groupLessonDetails['GroupLesson']['student_limit'];?>
                    </span>
                </li>
                <li>
                    <strong>Filled up Student:</strong>
                    <span>
                        <?php echo $groupLessonDetails['GroupLesson']['filled_up_student'];?>
                    </span>
                </li>
                <li>
                    <strong>Available:</strong>
                    <span>
                        <?php echo $groupLessonDetails['GroupLesson']['student_limit'] - $groupLessonDetails['GroupLesson']['filled_up_student'];?>
                    </span>
                </li>
                <li>
                    <strong>Status:</strong>
                    <span>
                        <?php
                        if($groupLessonDetails['GroupLesson']['status']){
                            echo '<label class="label label-success">Active</label>';
                        }
                        else{
                            echo '<label class="label label-danger">Inactive</label>';
                        }
                        ?>
                    </span>
                </li>

            </ul>
        </div>
    </div>
</div>