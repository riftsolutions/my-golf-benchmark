<?php
/**
 * @var $this View
 */
?>
<h2 class="page-title margin-top-20">My Calendar</h2>
<hr class="shadow-line"/>
<span id="userID" style="display: none;"><?php echo $userID; ?></span>
<div class="control-panel">
    <div class="element">
        <select class="form-control" id="selectInstructor">
            <?php foreach($instructors as $key => $value):?>
                <option value="<?php echo $key; ?>" <?php if($key == $this->Session->read('instructorID')){echo 'selected'; }?>>
                    <?php echo $value; ?>
                </option>
            <?php endforeach;?>
        </select>

        <div></div>

    </div>
    <div class="element">
        <a class="btn btn-theme" data-toggle="modal" data-target="#calendarSettings">My Schedule</a>
        <a class="btn btn-theme" data-toggle="modal" data-target="#createGroupLesson">Create Group Lesson</a>
    </div>
</div>

<div id="calendar"></div>

<!---- Displaying Schedule ----->
<script>
    $(document).ajaxStop($.unblockUI);
    var LoggedInInstructor = parseInt($('#selectInstructor').val());
    var defaultSource = '<?php echo Router::url('/', true);?>calendars/getAllEventsOfInstructor/' + LoggedInInstructor;
    console.log(defaultSource);

    $(document).ready(function() {
        var calendar = $('#calendar').fullCalendar({
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
            },
            ignoreTimezone: true,
            defaultView: 'agendaWeek',
            allDayDefault: false,
            allDaySlot: false,
            slotEventOverlap: true,
            contentHeight: 'auto',
            eventDataTransform: function (data) {
                return data;
            },
            select: function(start, end, allDay) {

            },
            eventRender: function(event, element) {

                if(event.type == 'event'){
                    availabilityEventView(event, element);
                }
                else if(event.type == 'group_lesson'){
                    groupLessonEventView(event, element);
                }
                else if(event.type == 'appointment'){
                    appointmentEventView(event, element);
                    appointmentEventPopover(event, element);
                }

                $('body').on('click', function (e) {
                    if (!element.is(e.target) && element.has(e.target).length === 0 && $('.popover').has(e.target).length === 0)
                        element.popover('hide');
                });
            },
            dayRender: function (date, cell) {

            },
            eventClick: function (data, allDay, jsEvent, view) {

                if(data.type == 'event'){
                    setAppointment(parseInt(data.id));
                }

                if(data.type == 'appointment'){
                    manageAppointmentView(data);
                }
            }
        });

        $('#calendar').fullCalendar('addEventSource', defaultSource);
    });

    <!----- Displaying different instructor ----->
    $(document).ready(function() {
        $(document).ajaxStart($.blockUI(
            {
                message: '<h1><img src="<?php echo Router::url('/', true);?>theme/Instructor/img/busy.gif" /> ' + 'Just a moment...</h1>'
            }
        )).ajaxStop($.unblockUI);
        $('#calendar').fullCalendar('addEventSource', defaultSource);
        var currentInstructorID = LoggedInInstructor;


        $('#selectInstructor').change(function(){
            $(document).ajaxStart($.blockUI(
                {
                    message: '<h1><img src="<?php echo Router::url('/', true);?>theme/Instructor/img/busy.gif" /> ' + 'Just a moment...</h1>'
                }
            )).ajaxStop($.unblockUI);

            newInstructorID = parseInt($('#selectInstructor').val());
            if(newInstructorID != '')
            {
                $('#calendar').fullCalendar('removeEventSource', defaultSource);
                $('#userID').html(newInstructorID);
                var removeEventSources = '<?php echo Router::url('/', true);?>calendars/getAllEventsOfInstructor/' + currentInstructorID;
                var AddEventSources = '<?php echo Router::url('/', true);?>calendars/getAllEventsOfInstructor/' + newInstructorID;
                $('#calendar').fullCalendar('addEventSource', AddEventSources);
            }

            $('#calendar').fullCalendar('refetchEvents');

        });
    });
    <!----- /Displaying different instructor ----->



    <!---- Displaying Schedule ----->

    function manageAppointmentView(appointmentDetails)
    {
        if(appointmentDetails.appointment_status == 3){
            $.ajax({
                url: '<?php echo Router::url('/', true);?>/calendars/getStudentForReassignAppointments',
                type: "POST",
                dataType: "json",
                data:  {studentID: appointmentDetails.student_id},
                success: function(students) {

                    var reassignTo = '<br/><form action="<?php echo Router::url('/', true);?>calendars/reassign" method="post"><label>Assign To:</label><input name="appointmentID" type="hidden" value="'+appointmentDetails.id+'"/><select class="form-control student_list" name="student_id"><option>Choose Student</option>';

                    $.each(students, function (key, student) {
                        reassignTo += '<option value="'+student['id']+'">'+student['name']+'</option>';
                    });

                    reassignTo += '</select><div class="student_package_list margin-top-15"></div><button class="btn btn-theme margin-top-15">Assign</button></form>';
                    $('.popover-content .student-box').html();
                    $('.popover-content .student-box').html(reassignTo);

                },
                error: function (xhr, status, error) {
                    console.log(error);
                }
            });


            $(document).on('change', '.student_list', function(){
                var studentID = parseInt($(this).val());
                if(studentID != 0){

                    $.getJSON('<?php echo Router::url('/', true);?>instructor/packages/usersPackages/'+studentID, function (data) {
                        $('.loadingEvent').hide();
                        if(data.package){
                            var studentPackages = '<label>Select Package for Appointment</label><select class="form-control" name="package_id">';
                            $.each(data.package, function (key, value) {
                                studentPackages += '<option value="' + key + '">' + value + '</option>';
                            });
                            studentPackages += '</select>';
                            $('.student_package_list').html();
                            $('.student_package_list').html(studentPackages);
                        }
                        else{
                            $('#student_package_list').html();
                        }
                    });
                }
            });
        }


    }



    function setAppointment(eventID)
    {
        $("#setAppointmentModal" ).modal('show');
        $('.loadingEvent').show();
        $.getJSON('<?php echo Router::url('/', true);?>calendars/getEventByID/' + parseInt(eventID), function (eventInfo) {
            $('#setAppointmentDate').text($.fullCalendar.moment(eventInfo.Event.start).format('dddd, MMMM DD YYYY'));


            $('.not-able').hide();
            $('#setAppointmentBtn').hide();
            $('.appointmentTimeFrame').hide();
            $('#packageList').hide();
            $('#appointmentNote').hide();

            $.getJSON('<?php echo Router::url('/', true);?>calendars/getStudents', function (students) {
                $('.loadingEvent').hide();
                $('#studentID')
                    .find('option')
                    .remove()
                    .end()
                ;

                var packageFetched = false;

                $('#studentID').append(
                    $("<option></option>").text("Choose Student").val(0)
                );
                $.each(students, function () {
                    $('#studentID').append(
                        $("<option></option>").text(this.Profile.name).val(this.Profile.user_id)
                    );
                });
            });

            $('.loadingEvent').fadeOut(500);
            $('.eventInfoArea').delay(3000).fadeIn(500);
        });

        $('#setAppointmentBtn').click(function(){
            var start = $('#setAppointmentDate').html() + ' '+ $('#appointmentStart').val();
            var end = $('#setAppointmentDate').html() + ' '+ $('#appointmentEnd').val();

            var studentID = $('#studentID').val();
            var packageID = $('#packageID').val();
            var appointmentNote = $('#appointmentNote').val();
            var appointmentID = $('#appointmentID').val();
            /*var start = $.fullCalendar.moment(startingTime).format('YYYY-MM-DD HH:mm:ss');
             var end = $.fullCalendar.moment(endingTime).format('YYYY-MM-DD HH:mm:ss');*/

            var isChecked = $('#wantToReschedule input').is(':checked');
            if(isChecked == true){
                packageID = null;
            }
            else{
                appointmentID = null
            }

            var appointmentInfo = {
                'studentID': studentID,
                'packageID': packageID,
                'appointmentID': appointmentID,
                'note': appointmentNote,
                'start': start,
                'end': end
            };

            var d1 = new Date(start);
            var d2 = new Date(end);
            var diff = (d2 - d1)/1000/60/60/24;

            if(diff > 0){
                if(studentID != null){
                    if(appointmentID != null || packageID != null){

                        $.ajax({
                            url: '<?php echo Router::url('/', true);?>/calendars/setAppointmentByInstructor',
                            type: "POST",
                            dataType: "json",
                            data:  appointmentInfo,
                            success: function(data) {
                                if(data == 'done'){
                                    manageFlashMessage('alert-success', 'Appointment has been completed');
                                }
                                else if(data == 'reschedule'){
                                    manageFlashMessage('alert-success', 'Appointment has been rescheduled successfully');
                                }
                                else if(data == 'invalid-time'){
                                    manageFlashMessage('alert-danger', 'Sorry, appointment time invalid');
                                }
                                else{
                                    manageFlashMessage('alert-danger', 'Sorry, something went wrong');
                                }
                            }
                        });


                    }
                    else{
                        manageFlashMessage('alert-danger', 'You have no purchased package available for appointment')
                    }
                }
                else{
                    manageFlashMessage('alert-danger', 'Sorry, you have no student for appointment')
                }
            }
            else{
                manageFlashMessage('alert-danger', 'Sorry, starting time should be less then ending time')
            }

            $("#setAppointmentModal" ).modal('hide');
            $('#calendar').fullCalendar('refetchEvents');
            $('#calendar').fullCalendar('refetchEvents');
        });
    }



    $(document).on('change', '#studentID', function(){

        $('.not-able').hide();
        $('#setAppointmentBtn').hide();
        $('.appointmentTimeFrame').hide();
        $('#packageList').hide();

        $('.loadingEvent').show().delay(2000);

        var studentID = parseInt($(this).val());
        if(studentID != 0){


            $.getJSON('<?php echo Router::url('/', true);?>instructor/packages/usersPackages/'+studentID, function (data) {
                $('.loadingEvent').hide();
                if(data.package){
                    var studentPackages = '<label>Select Package for Appointment</label><select class="form-control" id="packageID">';
                    $.each(data.package, function (key, value) {
                        studentPackages += '<option value="' + key + '">' + value + '</option>';
                    });
                    studentPackages += '</select>';
                    $('#packageList').html(studentPackages);
                }
                else{
                    $('#packageList').html(null);
                }


                if(data.appointments){
                    var calcelledAppointments = '<label>Reschedule for cancelled appointment</label><select class="form-control" id="appointmentID"> <option>Choose Lesson</option>';
                    if(data.package != 'error'){
                        $.each(data.appointments, function (key, value) {
                            calcelledAppointments += '<option value="' + key + '">' + value + '</option>';
                        });
                    }
                    calcelledAppointments += '</select>';
                    $('#rescheduleAppointments').html(calcelledAppointments);
                    $('#rescheduleAppointments').hide();
                    $('#wantToReschedule').show();
                    $('#wantToReschedule').addClass('animated fadeIn');
                }
                else{
                    $('#rescheduleAppointments').html(null);
                    $('#wantToReschedule').hide();
                }

                if(data.package == null && data.appointments == null){
                    $('.not-able').show();
                    $('.not-able').html("N.B. You can not able to set appointment with this student");
                    $('#setAppointmentBtn').hide();
                    $('.appointmentTimeFrame').hide();
                    $('#packageList').hide();
                    $('.not-able').addClass('animated fadeIn');
                }
                else{
                    $('.not-able').hide();
                    $('#setAppointmentBtn').show();
                    $('.appointmentTimeFrame').show();
                    $('#packageList').show();
                    $('#appointmentNote').show();
                }
            });
        }
        else{
            $('.loadingEvent').hide();
            $('.not-able').show();
            $('.not-able').html("Please choose student for set appointment");
        }
    });


    $('#wantToReschedule').click(function(){
        var isChecked = $('#wantToReschedule input').is(':checked');
        if(isChecked == true){
            $('#packageList').hide();
            $('#rescheduleAppointments').show();
            $('#rescheduleAppointments').addClass('animated fadeIn');
        }
        else{
            $('#rescheduleAppointments').hide();
            $('#packageList').show();
            $('#packageList').addClass('animated fadeIn');
        }
    });



    <!---- Manage Availability Event View ----->
    function availabilityEventView(event, element)
    {
        element.css('background-color', '#EEE');
        element.css('border-color', '#EEE');
        element.css('color', '#333');
        element.find('.fc-time').text('');
    }
    <!---- /Manage Availability Event View ----->

    <!---- Manage Group Lesson Event View ----->
    function groupLessonEventView(event, element)
    {

        element.css('background-color', '#d9edf7');
        element.css('border-color', '#bce8f1');
        element.css('color', '#31708f');

        element.find('.fc-time').text('');
        element.find('.fc-title').append('<br/> <br/> <strong style="font-size: 12; text-transform: uppercase; margin-top: 12; color: #31708f">Group Lesson</strong>');

        element.popover({
            title: "Group Lesson",
            placement:'auto',
            html:true,
            trigger : 'click',
            animation : 'true',
            content: '<ul class="data-list">' +
            '<li><strong>Title: </strong>'+ event.title +'</li>' +
            '<li><strong>Student Limit: </strong>'+ event.student_limit +'</li>' +
            '<li><strong>Filled Up: </strong>'+ event.filled_up_student +'</li>' +
            '<li><strong>Available: </strong>'+ (event.student_limit - event.filled_up_student) +'</li>' +
            '<li><strong>Appointment Date: </strong>'+ $.fullCalendar.moment(event.start._i).format('ddd, MMMM DD, YYYY') +'</li>' +
            '<li><strong>Appointment Time: </strong>'+ $.fullCalendar.moment(event.start._i).format('h:mm a')+ ' - '+ $.fullCalendar.moment(event.end._i).format('h:mm a') +'</li>' +
            '</ul>',
            container:'body'
        });
    }
    <!---- /Manage Group Lesson Event View ----->

    <!---- Manage Appointment Event View ----->
    function appointmentEventView(event, element)
    {
        element.css('color', '#3c763d');
        element.css('background-color', '#dff0d8');
        element.css('border-color', '#d6e9c6');


        var lesson_left_circle;
        if(event.lesson_left > 1){
            lesson_left_circle = 'sm-circle-green';
        }
        else if(event.lesson_left == 1){
            lesson_left_circle = 'sm-circle-orange';
        }
        else if(event.lesson_left < 1){
            lesson_left_circle = 'sm-circle-red';
        }

        if(event.appointment_status == 1){
            var appointmentStatusForBox = '<label class="appointment-status-for-box label label-gray">appointment pending</label>';
        }
        if(event.appointment_status == 2){
            var appointmentStatusForBox = '<label class="appointment-status-for-box label label-success">appointment confirm</label>';
        }
        if(event.appointment_status == 3){
            var appointmentStatusForBox = '<label class="appointment-status-for-box label label-danger">appointment cancelled</label>';

            element.css('color', '#a94442');
            element.css('background-color', '#f2dede');
            element.css('border-color', '#f2dede');

        }
        if(event.appointment_status == 4){
            var appointmentStatusForBox = '<label class="appointment-status-for-box label label-info">appointment completed</label>';
        }
        if(event.appointment_status == 5){
            var appointmentStatusForBox = '<label class="appointment-status-for-box label label-warning">appointment expired</label>';
        }

        element.find('.fc-time').append(appointmentStatusForBox);


        if(event.is_pay_at_facility == false){
            element.find('.fc-time').append('<span class="sm-circle '+lesson_left_circle+'">'+event.lesson_left+'</span>');

            if(event.is_paid_lesson){
                element.find('.fc-time').append('<span class="paid-lesson">$</span>');
            }
        }
        else{
            element.find('.fc-time').append('<label class="label pay-at-facility-label">Pay At Facility</label>');

            if(event.appointment_status == 2){

                element.css('color', '#8a6d3b');
                element.css('background-color', '#fcf8e3');
                element.css('border-color', '#fcf8e3');

            }
            if(event.appointment_status == 3){

                element.css('color', '#FFF');
                element.css('background-color', '#d08166');
                element.css('border-color', '#fcf8e3');
            }
        }
    }


    function appointmentEventPopover(event, element)
    {


        var appointmentType = '<li><strong>Appointment Type: </strong><label class="status-text blue">Regular</label></li>';
        if(event.is_pay_at_facility == false){

            var lessonLeft = '<li><strong>Lesson Left: </strong>'+ event.lesson_left +'</li>';
            var paymentStatus = '<li><strong>Payment Status: </strong><label class="status-text orange">UNPAID</label></li>';

            if(event.is_paid_lesson){
                var paymentStatus = '<li><strong>Payment Status: </strong><label class="status-text yellowgreen">PAID</label></li>';
            }
            var payAtFacility = '';
        }
        else{
            var lessonLeft = '';
            var paymentStatus = '';
            var payAtFacility = '<label class="label label-info">PAY AT FACILITY</label>';
            var appointmentType = '<li><strong>Appointment Type: </strong><label class="status-text yellowgreen">PAY AT FACILITY</label></li>';
        }

        var appointmentBtn = '';
        if(event.appointment_status == 2){
            if(parseInt(event.student_profile.instructor_id) == parseInt(LoggedInInstructor)){
                var appointmentBtn = '<a class="btn btn-black" id="'+parseInt(event.appointmentID)+'" onclick="appointmentAction('+parseInt(event.id)+', 3)">Cancel <a>';
            }
            var appointmentStatusLabel = '<label class="status-text yellow">appointment pending</label>';
            var appointmentStatusForBox = '<label class="appointment-status-for-box label label-gray">appointment pending</label>';
        }
        if(event.appointment_status == 2){
            var appointmentStatusLabel = '<label class="status-text yellowgreen">appointment confirm</label>';
            var appointmentStatusForBox = '<label class="appointment-status-for-box label label-success">appointment confirm</label>';
        }
        if(event.appointment_status == 3){
            var appointmentStatusLabel = '<label class="status-text orange">appointment cancelled</label>';
            /*var appointmentBtn = '<a class="btn btn-theme-square" id="'+parseInt(event.appointmentID)+'" onclick="appointmentAction('+parseInt(event.id)+', 2)">Confirn<a>';*/
            var appointmentStatusForBox = '<label class="appointment-status-for-box label label-danger">appointment cancelled</label>';
        }
        if(event.appointment_status == 4){
            var appointmentStatusLabel = '<label class="status-text green">appointment completed</label>';
            var appointmentStatusForBox = '<label class="appointment-status-for-box label label-info">appointment completed</label>';
        }
        if(event.appointment_status == 5){
            var appointmentStatusLabel = '<label class="status-text red">appointment expired</label>';
            var appointmentStatusForBox = '<label class="appointment-status-for-box label label-warning">appointment expired</label>';
        }


        if(event.appointment_status == 1){
            if(parseInt(event.studentProfile.instructor_id) == parseInt(LoggedInInstructor)){
                var appointmentBtn = '<a class="btn btn-theme-square" id="'+parseInt(event.appointmentID)+'" onclick="appointmentAction('+parseInt(event.appointmentID)+', 2)">Confirn<a> <a class="btn btn-black" id="'+parseInt(event.appointmentID)+'" onclick="appointmentAction('+parseInt(event.appointmentID)+', 3)">Cancel<a>';
            }

        }
        var rescheduleAppointment = '';
        if(event.isAppointmentRescheduled == 1)
        {
            var rescheduleAppointment = '<p><strong class="italic-sm danger">N.B.: This is rescheduled appointment</strong> </p>'
        }

        if(event.next_lesson_start)
        {
            var nextAppointment = $.fullCalendar.moment(event.next_lesson_start).format('MMMM DD, YYYY') + '<i style="font-size: 10px; font-style: italic; color; #999; ">('+ $.fullCalendar.moment(event.next_lesson_start).format('h:mm a')+ ' - '+ $.fullCalendar.moment(event.next_lesson_end).format('h:mm a')+')</i>';
        }
        else{
            var nextAppointment = 'N/A'
        }

        var packageName = '';
        if(event.package != "Pay at Facility")
        {
            var packageName = '<li><strong>Package: </strong>'+ event.package +'</li>';
        }

        element.popover({
            title: "Appointment Details",
            placement:'auto',
            html:true,
            trigger : 'click',
            animation : 'true',
            content: payAtFacility + '<ul class="data-list">' +
            '<li><strong>Student Name: </strong>'+ event.title +'</li>' +
            '<li><strong>Email: </strong>'+ event.username +'</li>' +
            '<li><strong>Address: </strong>'+ event.student_profile.street_1 +'</li>' +
            '<li><strong>Phone: </strong>'+ event.student_profile.phone +'</li>' +
            '<li><strong>City: </strong>'+ event.student_profile.city +'</li>' +
            '<li><strong>State: </strong>'+ event.student_profile.state +'</li>' +
            '<li><strong>Postal Code: </strong>'+ event.student_profile.postal_code +'</li>' +
            packageName  +
            lessonLeft  +
            paymentStatus  +
            '<li><strong>Appointment Date: </strong>'+ $.fullCalendar.moment(event.start._i).format('ddd, MMMM DD, YYYY') +'</li>' +
            '<li><strong>Appointment Time: </strong>'+ $.fullCalendar.moment(event.start._i).format('h:mm a')+ ' - '+ $.fullCalendar.moment(event.end._i).format('h:mm a') +'</li>' +
            '<li><strong>Next Lesson: </strong>'+ nextAppointment +'</li>' +
            '<li><strong>Current Status: </strong>'+ appointmentStatusLabel +'</li>' +
            '<li><strong>Waiting List: </strong>'+ event.count_waiting +'</li>' +
            appointmentType +
            '<li><i style="font-size: 10px !important; color: #999;">Note:'+ event.note +'</i></li>' +
            '</ul><div class="student-box"></div>' +
            '<div>'+ appointmentBtn +'</div>',
            container:'body'
        });
    }
    <!---- Manage Appointment Event View ----->
</script>




<?php echo $this->Js->writeBuffer(); ?>
<script>
    /*---- ADD Attachment -----*/
    $(document).ready(function () {
        var counter = 1;
        $("#sunAdd").on("click", function () {
            $("#addAllCatBrands").slideUp();
            counter++;
            var newRow = $("<div class='newRow'>");
            var rowData = "";
            rowData += $('.sunScheduleTime').html();
            rowData += '<a class="deleteRow danger"><i class="fa fa-times"></i></a></div>';
            newRow.append(rowData);
            $("div.sunSchedule").append(newRow);
        });
        $("div.sunSchedule").on("click", "a.deleteRow", function (event) {
            $(this).closest(".newRow").remove();
            counter--;
            if (counter < 2) {
                $("#addAllCatBrands").slideDown();
            }
        });

        var counter = 1;
        $("#monAdd").on("click", function () {
            $("#addAllCatBrands").slideUp();
            counter++;
            var newRow = $("<div class='newRow'>");
            var rowData = "";
            rowData += $('.monScheduleTime').html();
            rowData += '<a class="deleteRow danger"><i class="fa fa-times"></i></a></div>';
            newRow.append(rowData);
            $("div.monSchedule").append(newRow);
        });
        $("div.monSchedule").on("click", "a.deleteRow", function (event) {
            $(this).closest(".newRow").remove();
            counter--;
            if (counter < 2) {
                $("#addAllCatBrands").slideDown();
            }
        });

        var counter = 1;
        $("#tueAdd").on("click", function () {
            $("#addAllCatBrands").slideUp();
            counter++;
            var newRow = $("<div class='newRow'>");
            var rowData = "";
            rowData += $('.tueScheduleTime').html();
            rowData += '<a class="deleteRow danger"><i class="fa fa-times"></i></a></div>';
            newRow.append(rowData);
            $("div.tueSchedule").append(newRow);
        });
        $("div.tueSchedule").on("click", "a.deleteRow", function (event) {
            $(this).closest(".newRow").remove();
            counter--;
            if (counter < 2) {
                $("#addAllCatBrands").slideDown();
            }
        });

        var counter = 1;
        $("#wedAdd").on("click", function () {
            $("#addAllCatBrands").slideUp();
            counter++;
            var newRow = $("<div class='newRow'>");
            var rowData = "";
            rowData += $('.wedScheduleTime').html();
            rowData += '<a class="deleteRow danger"><i class="fa fa-times"></i></a></div>';
            newRow.append(rowData);
            $("div.wedSchedule").append(newRow);
        });
        $("div.wedSchedule").on("click", "a.deleteRow", function (event) {
            $(this).closest(".newRow").remove();
            counter--;
            if (counter < 2) {
                $("#addAllCatBrands").slideDown();
            }
        });

        var counter = 1;
        $("#thuAdd").on("click", function () {
            $("#addAllCatBrands").slideUp();
            counter++;
            var newRow = $("<div class='newRow'>");
            var rowData = "";
            rowData += $('.thuScheduleTime').html();
            rowData += '<a class="deleteRow danger"><i class="fa fa-times"></i></a></div>';
            newRow.append(rowData);
            $("div.thuSchedule").append(newRow);
        });
        $("div.thuSchedule").on("click", "a.deleteRow", function (event) {
            $(this).closest(".newRow").remove();
            counter--;
            if (counter < 2) {
                $("#addAllCatBrands").slideDown();
            }
        });

        var counter = 1;
        $("#friAdd").on("click", function () {
            $("#addAllCatBrands").slideUp();
            counter++;
            var newRow = $("<div class='newRow'>");
            var rowData = "";
            rowData += $('.friScheduleTime').html();
            rowData += '<a class="deleteRow danger"><i class="fa fa-times"></i></a></div>';
            newRow.append(rowData);
            $("div.friSchedule").append(newRow);
        });
        $("div.friSchedule").on("click", "a.deleteRow", function (event) {
            $(this).closest(".newRow").remove();
            counter--;
            if (counter < 2) {
                $("#addAllCatBrands").slideDown();
            }
        });

        var counter = 1;
        $("#satAdd").on("click", function () {
            $("#addAllCatBrands").slideUp();
            counter++;
            var newRow = $("<div class='newRow'>");
            var rowData = "";
            rowData += $('.satScheduleTime').html();
            rowData += '<a class="deleteRow danger"><i class="fa fa-times"></i></a></div>';
            newRow.append(rowData);
            $("div.satSchedule").append(newRow);
        });
        $("div.satSchedule").on("click", "a.deleteRow", function (event) {
            $(this).closest(".newRow").remove();
            counter--;
            if (counter < 2) {
                $("#addAllCatBrands").slideDown();
            }
        });
    });
    /*---- ADD Attachment -----*/


    <!---- CONFIRM/CANCEL APPOINTMENT ----->
    function appointmentAction(appointmentID, action)
    {
        $.ajax({
            url: "<?php echo Router::url('/', true);?>/calendars/appointmentActions/"+appointmentID+"/"+action+"/",
            type: "POST",
            dataType: "json",
            success: function (data) {
                if (data == 2) {
                    $('.popover').hide();
                    manageFlashMessage('alert-success', 'Appointment has been confirmed')
                }
                else if (data == 3) {
                    $('.popover').hide();
                    manageFlashMessage('alert-danger', 'Appointment has been cancelled')
                }
                else {
                    manageFlashMessage('alert-danger', 'Sorry, something went wrong')
                }
            },
            error: function (xhr, status, error) {}
        });
        $('#calendar').fullCalendar('refetchEvents');
        $('#calendar').fullCalendar('refetchEvents');
    }
    <!---- /CONFIRM/CANCEL APPOINTMENT ----->
</script>

<!-- Modal for the manage schedule -->
<div class="modal event-modal fade" id="calendarSettings" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <form action="<?php echo Router::url('/', true);?>calendars/available" id="setAvailabilityForm" method="post">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="sm-title">Manage Your Available Time</h4>
                </div>
                <div class="modal-body">
                    <table class="table set-schedule">
                        <thead>
                        <tr>
                            <th>Day</th>
                            <th>Schedule</th>
                            <th>Repeat</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>Sunday</td>
                            <td>
                                <div class="sunSchedule" style="width: 100%;">
                                    <div class="sunScheduleTime">
                                        <?php $starting  =  $this->Utilities->getFromHours('From');?>
                                        <?php $ending =  $this->Utilities->getFromHours('to');?>
                                        <?php $hoursList =  $this->Utilities->getHours();?>

                                        <select class="form-control schedule-to-form" name="data[sunday][start][]">
                                            <?php echo $starting;?>
                                        </select>
                                        <select class="form-control schedule-to-form" name="data[sunday][end][]">
                                            <?php echo $ending;?>
                                        </select>
                                    </div>

                                    <!-- Sunday Previous Day Schedule -->
                                    <div class="clearfix"></div>
                                    <?php
                                    $isRepeated = false;
                                    $singleSchedule = $this->Utilities->setPreviousEvents($events, 'sun');
                                    if(isset($singleSchedule['repeated'])){
                                        $isRepeated = 'checked';
                                        unset($singleSchedule['repeated']);
                                    }
                                    ?>
                                    <?php if(sizeof($singleSchedule > 0)):?>
                                        <?php foreach($singleSchedule as $schedule):?>
                                            <div class="newRow">
                                                <select class="form-control schedule-to-form" name="data[sunday][start][]">
                                                    <?php foreach($hoursList as $hour):?>
                                                        <option <?php if($schedule['start'] == $hour ){echo 'selected';}?> value="<?php echo $hour;?>">
                                                            <?php echo $hour;?>

                                                        </option>
                                                    <?php endforeach;?>

                                                </select>
                                                <select class="form-control schedule-to-form" name="data[sunday][end][]">
                                                    <?php foreach($hoursList as $hour):?>
                                                        <option <?php if($schedule['end'] == $hour ){echo 'selected';}?> value="<?php echo $hour;?>">
                                                            <?php echo $hour;?>

                                                        </option>
                                                    <?php endforeach;?>
                                                </select>
                                                <a class="deleteRow danger"><i class="fa fa-times"></i></a>
                                            </div>
                                        <?php endforeach;?>
                                    <?php endif;?>
                                    <!-- Sunday Previous Day Schedule -->
                                </div>
                                <p class="addScheduleRow italic-sm" id="sunAdd"> Add another schedule</p>
                            </td>
                            <td><input name="data[sunday][select]" <?php echo $isRepeated; ?> type="checkbox" value="1"/></td>
                        </tr>
                        <tr>
                            <td>Monday</td>
                            <td>
                                <div class="monSchedule" style="width: 100%;">
                                    <div class="monScheduleTime">
                                        <select class="form-control schedule-to-form" name="data[monday][start][]">
                                            <?php echo $starting;?>
                                        </select>
                                        <select class="form-control schedule-to-form" name="data[monday][end][]">
                                            <?php echo $ending;?>
                                        </select>
                                    </div>
                                    <!-- Monday Previous Day Schedule -->
                                    <div class="clearfix"></div>
                                    <?php
                                    $isRepeated = false;
                                    $singleSchedule = $this->Utilities->setPreviousEvents($events, 'mon');
                                    if(isset($singleSchedule['repeated'])){
                                        $isRepeated = 'checked';
                                        unset($singleSchedule['repeated']);
                                    }
                                    ?>
                                    <?php if(sizeof($singleSchedule > 0)):?>
                                        <?php foreach($singleSchedule as $schedule):?>
                                            <div class="newRow">
                                                <select class="form-control schedule-to-form" name="data[monday][start][]">
                                                    <?php foreach($hoursList as $hour):?>
                                                        <option <?php if($schedule['start'] == $hour ){echo 'selected';}?> value="<?php echo $hour;?>">
                                                            <?php echo $hour;?>

                                                        </option>
                                                    <?php endforeach;?>

                                                </select>
                                                <select class="form-control schedule-to-form" name="data[monday][end][]">
                                                    <?php foreach($hoursList as $hour):?>
                                                        <option <?php if($schedule['end'] == $hour ){echo 'selected';}?> value="<?php echo $hour;?>">
                                                            <?php echo $hour;?>

                                                        </option>
                                                    <?php endforeach;?>
                                                </select>
                                                <a class="deleteRow danger"><i class="fa fa-times"></i></a>
                                            </div>
                                        <?php endforeach;?>
                                    <?php endif;?>
                                    <!-- Monday Previous Day Schedule -->
                                </div>
                                <p class="addScheduleRow italic-sm" id="monAdd"> Add another schedule</p>
                            </td>
                            <td><input name="data[monday][select]" <?php echo $isRepeated; ?> type="checkbox" value="1"/></td>
                        </tr>

                        <tr>
                            <td>Tuesday</td>
                            <td>
                                <div class="tueSchedule" style="width: 100%;">
                                    <div class="tueScheduleTime">
                                        <select class="form-control schedule-to-form" name="data[tuesday][start][]">
                                            <?php echo $starting;?>
                                        </select>
                                        <select class="form-control schedule-to-form" name="data[tuesday][end][]">
                                            <?php echo $ending;?>
                                        </select>
                                    </div>
                                    <!-- Tuesday Previous Day Schedule -->
                                    <div class="clearfix"></div>
                                    <?php
                                    $isRepeated = false;
                                    $singleSchedule = $this->Utilities->setPreviousEvents($events, 'tue');
                                    if(isset($singleSchedule['repeated'])){
                                        $isRepeated = 'checked';
                                        unset($singleSchedule['repeated']);
                                    }
                                    ?>
                                    <?php if(sizeof($singleSchedule > 0)):?>
                                        <?php foreach($singleSchedule as $schedule):?>
                                            <div class="newRow">
                                                <select class="form-control schedule-to-form" name="data[tuesday][start][]">
                                                    <?php foreach($hoursList as $hour):?>
                                                        <option <?php if($schedule['start'] == $hour ){echo 'selected';}?> value="<?php echo $hour;?>">
                                                            <?php echo $hour;?>

                                                        </option>
                                                    <?php endforeach;?>

                                                </select>
                                                <select class="form-control schedule-to-form" name="data[tuesday][end][]">
                                                    <?php foreach($hoursList as $hour):?>
                                                        <option <?php if($schedule['end'] == $hour ){echo 'selected';}?> value="<?php echo $hour;?>">
                                                            <?php echo $hour;?>

                                                        </option>
                                                    <?php endforeach;?>
                                                </select>
                                                <a class="deleteRow danger"><i class="fa fa-times"></i></a>
                                            </div>
                                        <?php endforeach;?>
                                    <?php endif;?>
                                    <!-- Tuesday Previous Day Schedule -->
                                </div>
                                <p class="addScheduleRow italic-sm" id="tueAdd"> Add another schedule</p>
                            </td>
                            <td><input name="data[tuesday][select]" <?php echo $isRepeated; ?> type="checkbox" value="1"/></td>
                        </tr>

                        <tr>
                            <td>Wednesday</td>
                            <td>
                                <div class="wedSchedule" style="width: 100%;">
                                    <div class="wedScheduleTime">
                                        <select class="form-control schedule-to-form" name="data[wednesday][start][]">
                                            <?php echo $starting;?>
                                        </select>
                                        <select class="form-control schedule-to-form" name="data[wednesday][end][]">
                                            <?php echo $ending;?>
                                        </select>
                                    </div>
                                    <!-- Wednesday Previous Day Schedule -->
                                    <div class="clearfix"></div>
                                    <?php
                                    $isRepeated = false;
                                    $singleSchedule = $this->Utilities->setPreviousEvents($events, 'wed');
                                    if(isset($singleSchedule['repeated'])){
                                        $isRepeated = 'checked';
                                        unset($singleSchedule['repeated']);
                                    }
                                    ?>
                                    <?php if(sizeof($singleSchedule > 0)):?>
                                        <?php foreach($singleSchedule as $schedule):?>
                                            <div class="newRow">
                                                <select class="form-control schedule-to-form" name="data[wednesday][start][]">
                                                    <?php foreach($hoursList as $hour):?>
                                                        <option <?php if($schedule['start'] == $hour ){echo 'selected';}?> value="<?php echo $hour;?>">
                                                            <?php echo $hour;?>

                                                        </option>
                                                    <?php endforeach;?>

                                                </select>
                                                <select class="form-control schedule-to-form" name="data[wednesday][end][]">
                                                    <?php foreach($hoursList as $hour):?>
                                                        <option <?php if($schedule['end'] == $hour ){echo 'selected';}?> value="<?php echo $hour;?>">
                                                            <?php echo $hour;?>

                                                        </option>
                                                    <?php endforeach;?>
                                                </select>
                                                <a class="deleteRow danger"><i class="fa fa-times"></i></a>
                                            </div>
                                        <?php endforeach;?>
                                    <?php endif;?>
                                    <!-- Wednesday Previous Day Schedule -->
                                </div>
                                <p class="addScheduleRow italic-sm" id="wedAdd"> Add another schedule</p>
                            </td>
                            <td><input name="data[wednesday][select]" <?php echo $isRepeated; ?> type="checkbox" value="1"/></td>
                        </tr>

                        <tr>
                            <td>Thursday</td>
                            <td>
                                <div class="thuSchedule" style="width: 100%;">
                                    <div class="thuScheduleTime">
                                        <select class="form-control schedule-to-form" name="data[thursday][start][]">
                                            <?php echo $starting;?>
                                        </select>
                                        <select class="form-control schedule-to-form" name="data[thursday][end][]">
                                            <?php echo $ending;?>
                                        </select>
                                    </div>
                                    <!-- Thursday Previous Day Schedule -->
                                    <div class="clearfix"></div>
                                    <?php
                                    $isRepeated = false;
                                    $singleSchedule = $this->Utilities->setPreviousEvents($events, 'thu');
                                    if(isset($singleSchedule['repeated'])){
                                        $isRepeated = 'checked';
                                        unset($singleSchedule['repeated']);
                                    }
                                    ?>
                                    <?php if(sizeof($singleSchedule > 0)):?>
                                        <?php foreach($singleSchedule as $schedule):?>
                                            <div class="newRow">
                                                <select class="form-control schedule-to-form" name="data[thursday][start][]">
                                                    <?php foreach($hoursList as $hour):?>
                                                        <option <?php if($schedule['start'] == $hour ){echo 'selected';}?> value="<?php echo $hour;?>">
                                                            <?php echo $hour;?>

                                                        </option>
                                                    <?php endforeach;?>

                                                </select>
                                                <select class="form-control schedule-to-form" name="data[thursday][end][]">
                                                    <?php foreach($hoursList as $hour):?>
                                                        <option <?php if($schedule['end'] == $hour ){echo 'selected';}?> value="<?php echo $hour;?>">
                                                            <?php echo $hour;?>

                                                        </option>
                                                    <?php endforeach;?>
                                                </select>
                                                <a class="deleteRow danger"><i class="fa fa-times"></i></a>
                                            </div>
                                        <?php endforeach;?>
                                    <?php endif;?>
                                    <!-- Thursday Previous Day Schedule -->
                                </div>
                                <p class="addScheduleRow italic-sm" id="thuAdd"> Add another schedule</p>
                            </td>
                            <td><input name="data[thursday][select]" <?php echo $isRepeated; ?> type="checkbox" value="1"/></td>
                        </tr>

                        <tr>
                            <td>Friday</td>
                            <td>
                                <div class="friSchedule" style="width: 100%;">
                                    <div class="friScheduleTime">
                                        <select class="form-control schedule-to-form" name="data[friday][start][]">
                                            <?php echo $starting;?>
                                        </select>
                                        <select class="form-control schedule-to-form" name="data[friday][end][]">
                                            <?php echo $ending;?>
                                        </select>
                                    </div>
                                    <!-- Friday Previous Day Schedule -->
                                    <div class="clearfix"></div>
                                    <?php
                                    $isRepeated = false;
                                    $singleSchedule = $this->Utilities->setPreviousEvents($events, 'fri');
                                    if(isset($singleSchedule['repeated'])){
                                        $isRepeated = 'checked';
                                        unset($singleSchedule['repeated']);
                                    }
                                    ?>
                                    <?php if(sizeof($singleSchedule > 0)):?>
                                        <?php foreach($singleSchedule as $schedule):?>
                                            <div class="newRow">
                                                <select class="form-control schedule-to-form" name="data[friday][start][]">
                                                    <?php foreach($hoursList as $hour):?>
                                                        <option <?php if($schedule['start'] == $hour ){echo 'selected';}?> value="<?php echo $hour;?>">
                                                            <?php echo $hour;?>

                                                        </option>
                                                    <?php endforeach;?>

                                                </select>
                                                <select class="form-control schedule-to-form" name="data[friday][end][]">
                                                    <?php foreach($hoursList as $hour):?>
                                                        <option <?php if($schedule['end'] == $hour ){echo 'selected';}?> value="<?php echo $hour;?>">
                                                            <?php echo $hour;?>

                                                        </option>
                                                    <?php endforeach;?>
                                                </select>
                                                <a class="deleteRow danger"><i class="fa fa-times"></i></a>
                                            </div>
                                        <?php endforeach;?>
                                    <?php endif;?>
                                    <!-- Friday Previous Day Schedule -->
                                </div>
                                <p class="addScheduleRow italic-sm" id="friAdd"> Add another schedule</p>
                            </td>
                            <td><input name="data[friday][select]" <?php echo $isRepeated; ?> type="checkbox" value="1"/></td>
                        </tr>

                        <tr>
                            <td>Saturday</td>
                            <td>
                                <div class="satSchedule" style="width: 100%;">
                                    <div class="satScheduleTime">
                                        <select class="form-control schedule-to-form" name="data[saturday][start][]">
                                            <?php echo $starting;?>
                                        </select>
                                        <select class="form-control schedule-to-form" name="data[saturday][end][]">
                                            <?php echo $ending;?>
                                        </select>
                                    </div>
                                    <!-- Saturday Previous Day Schedule -->
                                    <div class="clearfix"></div>
                                    <?php
                                    $isRepeated = false;
                                    $singleSchedule = $this->Utilities->setPreviousEvents($events, 'sat');
                                    if(isset($singleSchedule['repeated'])){
                                        $isRepeated = 'checked';
                                        unset($singleSchedule['repeated']);
                                    }
                                    ?>
                                    <?php if(sizeof($singleSchedule > 0)):?>
                                        <?php foreach($singleSchedule as $schedule):?>
                                            <div class="newRow">
                                                <select class="form-control schedule-to-form" name="data[saturday][start][]">
                                                    <?php foreach($hoursList as $hour):?>
                                                        <option <?php if($schedule['start'] == $hour ){echo 'selected';}?> value="<?php echo $hour;?>">
                                                            <?php echo $hour;?>

                                                        </option>
                                                    <?php endforeach;?>

                                                </select>
                                                <select class="form-control schedule-to-form" name="data[saturday][end][]">
                                                    <?php foreach($hoursList as $hour):?>
                                                        <option <?php if($schedule['end'] == $hour ){echo 'selected';}?> value="<?php echo $hour;?>">
                                                            <?php echo $hour;?>

                                                        </option>
                                                    <?php endforeach;?>
                                                </select>
                                                <a class="deleteRow danger"><i class="fa fa-times"></i></a>
                                            </div>
                                        <?php endforeach;?>
                                    <?php endif;?>
                                    <!-- Saturday Previous Day Schedule -->
                                </div>
                                <p class="addScheduleRow italic-sm" id="satAdd"> Add another schedule</p>
                            </td>

                            <td><input name="data[saturday][select]" <?php echo $isRepeated; ?> type="checkbox" value="1"/></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-black-radius" data-dismiss="modal">Close</button>
                    <button type="button" id="setAvailability" class="btn btn-theme">Save changes</button>
                </div>
            </div>
        </form>
    </div>
</div>

<!-- Modal for the create group lesson -->
<div class="modal event-modal fade" id="createGroupLesson" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <form action="<?php echo Router::url('/', true);?>calendars/save_group_lesson" id="setAvailabilityForm" method="post">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="sm-title">Create Group Lesson</h4>
                </div>
                <div class="modal-body">
                    <div class="eventInfoArea">

                        <div class="form-group">
                            <label for="exampleInputEmail1">Title</label>
                            <input name="data[GroupLesson][title]" type="text" class="form-control" placeholder="Title">
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-lg-12">
                                    <label for="exampleInputEmail1">Date:</label>
                                    <div class="input-group">
                                        <input id="datepicker" name="data[GroupLesson][date]" type="text" class="form-control" placeholder="Date">
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="row">
                                <div class="col-lg-6">
                                    <label for="exampleInputEmail1">Starting Time:</label>
                                    <div class="input-group">
                                        <select name="data[GroupLesson][starting_time]" class="form-control">
                                            <?php echo $this->Utilities->getFromHours('Starting Time');?>
                                        </select>
                                        <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <label for="exampleInputEmail1">Ending Time:</label>
                                    <div class="input-group">
                                        <select name="data[GroupLesson][ending_time]" class="form-control">
                                            <?php echo $this->Utilities->getFromHours('Ending Time');?>
                                        </select>
                                        <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Maximum Number of Student:</label>
                            <input name="data[GroupLesson][student_limit]" type="text" class="form-control" placeholder="Maximum number of student">
                        </div>

                        <div class="pull-right">
                            <button type="button" class="btn btn-black-radius" data-dismiss="modal">Close</button>
                            <button class="btn btn-theme"  style="display: inline-block;">Save</button>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>


<!-- Modal For Displaying Schedule -->
<div class="modal event-modal fade" id="setAppointmentModal" tabindex="-1" role="dialog" aria-labelledby="createEventLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="border-bottom: 1px solid #e5e5e5;">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                </button>
                <h4 class="sm-title">My Appointment</h4>
            </div>
            <div class="modal-body">
                <div class="loadingEvent" style="display: none;">
                    <?php echo $this->Html->image('ajax-loader2.gif')?><br/>
                    Loading, please wait...
                </div>

                <div class="eventInfoArea" style="display: none;">
                    <div class="form-group">
                        <label for="exampleInputEmail1" id="setAppointmentDate"></label>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Select Student</label>
                        <select class="form-control" id="studentID">
                        </select>
                    </div>
                    <div class="form-group" id="packageList">

                    </div>
                    <div class="form-group" id="rescheduleAppointments">

                    </div>
                    <div id="wantToReschedule" class="checkbox" style="display: none;">
                        <label>
                            <input type="checkbox" value="reschedule"> Want to reschedule?
                        </label>
                    </div>
                    <div class="row appointmentTimeFrame">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Appointment Start</label>
                                <?php
                                echo $this->Form->input('start', array(
                                    'id' => 'appointmentStart',
                                    'class' => 'form-control',
                                    'type' => 'select',
                                    'label' => false,
                                    'div' => false,
                                    'options' => $this->Utilities->getHours()
                                ));
                                ?>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Appointment End</label>
                                <?php
                                echo $this->Form->input('end', array(
                                    'id' => 'appointmentEnd',
                                    'class' => 'form-control',
                                    'type' => 'select',
                                    'label' => false,
                                    'div' => false,
                                    'options' => $this->Utilities->getHours()
                                ));
                                ?>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <textarea class="form-control" id="appointmentNote" rows="4" placeholder="Appointment Note" style="display: none;"></textarea>
                    </div>

                    <br/>
                    <p style="display: none;" class="not-found not-able">N.B. You can not able to set appointment with this student</p>
                    <button class="btn btn-theme" id="setAppointmentBtn">Set Appointment</button>
                </div>
            </div>
            <br/>
            <div class="modal-footer"></div>
        </div>
    </div>
</div>
<!-- /Modal For Displaying Schedule -->



<?php echo $this->Html->css(array('jq'))?>
<?php echo $this->Html->script('jquery.block.ui')?>
<!-- /Modal for the manage schedule -->

<script>
    $(document).ready(function(){
        $("#setAvailability").on("click", function () {
            $('#setAvailabilityForm').submit();
        });
    });

    $(document).ready(function(){
        $('#datepickerSlot').datepicker()
    });
</script>

<style>
    .datepicker.dropdown-menu {
        z-index: 11111;
    }
</style>

