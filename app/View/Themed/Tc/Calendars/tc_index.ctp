<h2 class="page-title margin-top-20">My Calendar</h2>
<hr class="shadow-line"/>

<div class="btn-group" role="group" aria-label="...">
    <button type="button" class="btn btn-default calendarView" calendar-view="agendaWeek">Single View</button>
    <button type="button" class="btn btn-default calendarView" calendar-view="resourceDay">Multiple View</button>
</div>
<br/>
<br/>

<div id="selectInstructor" style="display: none;">
    <label>Choose Instructor</label>
    <select class="form-control form-inline">
        <?php $counter = 0; foreach ($instructors as $instructor => $value):?>
            <option <?php if($counter == 0){echo 'selected'; $counter++;}?> value="<?php echo $instructor; ?>">
                <?php echo $value; ?>
            </option>
        <?php endforeach;?>
    </select>
</div>


<div class="form-inline" id="instructorIds">
    <?php $count = 0; foreach ($instructors as $instructor => $value): ?>
        <div class="checkbox">
            <label class="text-uppercase margin-right-25">
                <input class="instructorIds" type="checkbox" name="<?php echo $value; ?>" <?php if($count <= 4){echo 'checked'; $count++;}?>
                       value="<?php echo $instructor; ?>"> <?php echo $value; ?>
            </label>
        </div>
    <?php endforeach; ?>
</div>

<input id="defaultView" value="resourceDay" type="hidden">
<br/>

<div class="right_button_option">
    <a class="btn right_click_btn btn-theme" onclick="clickRightOptionsBtn(1)">Set Appointment</a>
    <a class="btn right_click_btn btn-black" onclick="clickRightOptionsBtn(2)">Group Lesson</a>
    <a class="btn right_click_btn btn-danger" onclick="clickRightOptionsBtn(3)">Block Time</a>
    <a class="not_now pull-right">Not now?</a>
    <div class="clearfix"></div>
</div>

<div id="calendar"></div>

<?php echo $this->Html->script('jquery.block.ui') ?>
<script>

    function clickRightOptionsBtn(value)
    {
        $('.right_button_option').hide();

        if(value == 1)
        {
            setAppointment(false);
        }
        else if(value == 2)
        {
            $("#createGroupLesson" ).modal('show');
        }
        else if(value == 3)
        {
            $("#blockTimeModal" ).modal('show');
        }
    }

    function setAppointment(eventID, appointmentID)
    {
        $('.popover').popover().hide();

        if(typeof(appointmentID) != "undefined"){
            $.ajax({
                url: "<?php echo Router::url('/', true);?>instructor/appointments/getAppointmentByID",
                type: "POST",
                data:  {appointmentID: appointmentID},
                dataType: "json",
                success: function (data) {

                    $.getJSON('<?php echo Router::url('/', true);?>instructor/users/userUserByID/'+parseInt(data.appointment_by), function (student) {
                        $('#studentID').val(student.Profile.first_name +'  '+student.Profile.last_name +' - '+student.User.username);
                    });

                    fetchStudentDetailsForStudent(parseInt(data.appointment_by));

                    var date = $.fullCalendar.moment(data.start).format('MM/DD/YYYY');
                    var start = $.fullCalendar.moment(data.start).format('hh:mm A');
                    var end = $.fullCalendar.moment(data.end).format('hh:mm A');

                    $('.setAppointmentDate').val(date);
                    $('#lessonWillDeduct').val(data['lesson_sum']);
                    $('#appointmentStart').val(start);
                    $('#appointmentEnd').val(end);
                    $('#appointmentNote').val(data['note']);
                    $('#appointment_title input').val(data['title']);

                    $('#appointmentIDInEditAppointment').html('<input type="hidden" name="beforeAppointmentID" value="'+appointmentID+'"><input type="hidden" name="beforePackageID" value="'+data.package_id+'"><input type="hidden" name="beforeEditPayItFacility" value="'+data.pay_at_facility+'"><input type="hidden" name="beforeEditLessonWillDeduct" value="'+data['lesson_sum']+'"><input type="hidden" name="beforeEditAppointmentBy" value="'+data['appointment_by']+'">');

                    $('#setAppointmentBtn').text('Update Appointment');
                },
                error: function (xhr, status, error) {}
            });
        }

        $("#setAppointmentModal" ).modal('show');
        $('.loadingEvent').show();

        if(eventID != false)
        {
            $.getJSON('<?php echo Router::url('/', true);?>calendars/getEventByID/' + parseInt(eventID), function (eventInfo) {
                $('#setAppointmentDate').text($.fullCalendar.moment(eventInfo.Event.start).format('dddd, MMMM DD YYYY'));
                $('.setAppointmentDate').val($.fullCalendar.moment(eventInfo.Event.start).format('dddd, MMMM DD YYYY'));

                $('.loadingEvent').fadeOut(500);
            });
        }

        $('.not-able').hide();
        $('#setAppointmentBtn').hide();
        $('.appointmentTimeFrame').show();
        $('#packageList').show();
        $('#appointmentNote').show();


        $.getJSON('<?php echo Router::url('/', true);?>calendars/getStudents', function (students) {
            $('.loadingEvent').hide();
            $('#studentID')
                .find('option')
                .remove()
                .end()
            ;

            var packageFetched = false;

            $('#studentID').append(
                $("<option></option>").text("Choose Student").val(0)
            );
            $.each(students, function () {
                $('#studentID').append(
                    $("<option></option>").text(this.Profile.name).val(this.Profile.user_id)
                );
            });
        });
    }

    $(document).ready(function () {
            loadCalendar();
            getDefaultView();

            $('#instructorIds input').click(function () {
                $('#calendar').fullCalendar('destroy');
                loadCalendar();
            })

            $('.calendarView').click(function () {
                $('#calendar').fullCalendar('destroy');
                var calendarView  = $(this).attr('calendar-view');

                if(calendarView == 'agendaWeek')
                {
                    $('#instructorIds').hide();
                    $('#selectInstructor').show();
                }
                else{
                    $('#selectInstructor').hide();
                    $('#instructorIds').show();
                }

                $('#defaultView').val(calendarView);
                loadCalendar();
            })

            $('#selectInstructor select').change(function(){
                var instructorID = parseInt($(this).val());
                $('#calendar').fullCalendar('destroy');
                loadCalendar();
            });

            function getDefaultView() {
                var view = $('#defaultView').val();
                return view;
            }

            function getRightOptions() {
                var options = 'resourceDay';
                var view = $('#defaultView').val();
                if(view == 'agendaWeek')
                {
                    options = 'month,agendaWeek,resourceDay';
                }

                return options;
            }

            function getInstructors() {

                var instructors = [];
                var view = $('#defaultView').val();

                if(view == 'resourceDay'){
                    $('#instructorIds :checked').each(function () {
                        instructors.push($(this).val());
                    });
                }
                else{
                    instructors.push($('#selectInstructor select').val());
                }
                return instructors;
            }

            function getResources() {
                var resources = [];
                $('#instructorIds :checked').each(function () {
                    singleresource = {
                        'id': parseInt($(this).val()),
                        'name': $(this).attr('name')
                    };
                    resources.push(singleresource);
                });

                return resources;
            }

            function setCalendarHeader() {
                var headerText = $('.fc-header-title h2').text();
                if(getDefaultView() == 'agendaWeek')
                {
                    var instructorName = $('#selectInstructor select option:selected').text();
                    headerText = headerText + ' ('+instructorName+')';
                }
                $('.fc-header-title h2').text(headerText);
            }

            function closeModalPopover()
            {
                $('.popover').popover().hide();
            }

            <!---- Manage Appointment Event View ----->
            function appointmentEventView(event, element)
            {
                element.css('color', '#3c763d');
                element.css('background-color', '#dff0d8');
                element.css('border-color', '#d6e9c6');


                var lesson_left_circle;
                var scheduled_circle;
                var lesson_left = parseInt(event.lesson_left);
                var scheduled = parseInt(event.scheduled);
                var total = parseInt(event.total);

                if(lesson_left > 1){
                    lesson_left_circle = 'sm-circle-green';
                }
                else if(lesson_left == 1){
                    lesson_left_circle = 'sm-circle-orange';
                }
                else if(lesson_left < 1){
                    lesson_left_circle = 'sm-circle-red';
                }

                if(scheduled < 1){
                    scheduled_circle = 'sm-circle-red';
                }
                else if(scheduled == total){
                    scheduled_circle = 'sm-circle-green';
                }
                else if(scheduled < total){
                    scheduled_circle = 'sm-circle-yellow';
                }
                else if(scheduled > total){
                    scheduled_circle = 'sm-circle-gold';
                }




                if(event.appointment_status == 1){
                    var appointmentStatusForBox = '<label class="appointment-status-for-box label label-gray">appointment pending</label>';
                }
                if(event.appointment_status == 2){
                    var appointmentStatusForBox = '<!--<label class="appointment-status-for-box label label-success">appointment confirm</label>-->';
                }
                if(event.appointment_status == 3){
                    var appointmentStatusForBox = '<label class="appointment-status-for-box label label-danger">appointment cancelled</label>';

                    element.css('color', '#a94442');
                    element.css('background-color', '#f2dede');
                    element.css('border-color', '#f2dede');

                }
                if(event.appointment_status == 4){
                    var appointmentStatusForBox = '<label class="appointment-status-for-box label label-info">Complete</label>';
                }
                if(event.appointment_status == 5){
                    var appointmentStatusForBox = '<label class="appointment-status-for-box label label-warning">Incomplete</label>';
                }




                element.find('.fc-event-time').append('<div class="abs_label">');




                if(event.is_pay_at_facility == false){
                    element.find('.fc-event-time').append('<span class="sm-circle '+lesson_left_circle+'" title="Lesson Left">'+event.lesson_left+'</span>');
                    element.find('.fc-event-time').append('<span class="sm-circle-scheduled '+scheduled_circle+'" title="Lesson Scheduled">'+event.scheduled+'</span>');

                    if(event.is_paid_lesson){
                        element.find('.fc-event-time').append('<span class="paid-lesson">$</span>');
                    }
                }
                else{

                    element.find('.fc-event-time div').append('<label class="label pay-at-facility-label">Pay At Facility</label><br/>');

                    if(event.appointment_status == 2){

                        element.css('color', '#8a6d3b');
                        element.css('background-color', '#fcf8e3');
                        element.css('border-color', '#fcf8e3');

                    }
                    if(event.appointment_status == 3){

                        element.css('color', '#FFF');
                        element.css('background-color', '#d08166');
                        element.css('border-color', '#fcf8e3');
                    }
                }

                console.log(event);
                if(event.is_reassigned == 1){

                    element.find('.fc-event-time div').append('<label class="label pay-at-facility-label">Reassigned</label><br/>');

                    element.css('color', '#525252');
                    element.css('background-color', '#CCC');
                    element.css('border-color', '#CCC');
                }

                element.find('.fc-event-time div').append(appointmentStatusForBox);

            }


            function appointmentEventPopover(event, element)
            {


                var appointmentType = '<li><strong>Appointment Type: </strong><label class="status-text blue">Regular</label></li>';
                if(event.is_pay_at_facility == false){

                    var lessonDeduct = '<li><strong>Lessons Deducted: </strong>'+ event.lesson_sum +'</li>';
                    var lessonLeft = '<li><strong>Lessons Left: </strong>'+ event.lesson_left +'</li>';
                    var lessonScheduled = '<li><strong>Lessons Scheduled: </strong>'+ event.scheduled +'</li>';
                    var paymentStatus = '<li><strong>Payment Status: </strong><label class="status-text orange">UNPAID</label></li>';

                    if(event.is_paid_lesson){
                        var paymentStatus = '<li><strong>Payment Status: </strong><label class="status-text yellowgreen">PAID</label></li>';
                    }
                    var payAtFacility = '';
                }
                else{
                    var lessonDeduct = '';
                    var lessonLeft = '';
                    var lessonScheduled = '';
                    var paymentStatus = '';
                    var payAtFacility = '<label class="label label-info">PAY AT FACILITY</label>';
                    var appointmentType = '<li><strong>Appointment Type: </strong><label class="status-text yellowgreen">PAY AT FACILITY</label></li>';
                }

                var appointmentBtn = '';
                var editAppointment = '<a onclick="setAppointment(false, '+parseInt(event.id)+')" class="btn btn-theme-info width-100-p text-uppercase">Edit Appointment</a><br/><br/>';
                if(event.appointment_status == 2){
                    /*if(parseInt(event.student_profile.instructor_id) == parseInt(LoggedInInstructor)){
                        var appointmentBtn = '<a class="btn btn-theme-red width-100-p text-uppercase" id="'+parseInt(event.id)+'" href="<?php echo Router::url('/', true);?>calendars/appointmentActions/'+parseInt(event.id)+'/3">Cancel Appointment<a>';
                    }*/
                    var appointmentBtn = '<a class="btn btn-theme-red width-100-p text-uppercase" id="'+parseInt(event.id)+'" href="<?php echo Router::url('/', true);?>calendars/appointmentActions/'+parseInt(event.id)+'/3">Cancel Appointment<a>';
                    var appointmentStatusLabel = '<label class="status-text yellow">appointment pending</label>';
                    var appointmentStatusForBox = '<label class="appointment-status-for-box label label-gray">appointment pending</label>';
                }
                if(event.appointment_status == 2){
                    var appointmentStatusLabel = '<!--<label class="status-text yellowgreen">appointment confirm</label>-->';
                    var appointmentStatusForBox = '<!--<label class="appointment-status-for-box label label-success">appointment confirm</label>-->';
                }
                if(event.appointment_status == 3){
                    var appointmentStatusLabel = '<label class="status-text orange">appointment cancelled</label>';
                    var appointmentStatusForBox = '<label class="appointment-status-for-box label label-danger">appointment cancelled</label>';
                }
                if(event.appointment_status == 4){
                    var appointmentStatusLabel = '<label class="status-text green">Complete</label>';
                    var appointmentStatusForBox = '<label class="appointment-status-for-box label label-info">Complete</label>';
                    editAppointment = '';
                }
                if(event.appointment_status == 5){
                    var appointmentStatusLabel = '<label class="status-text red">Incomplete</label>';
                    var appointmentStatusForBox = '<label class="appointment-status-for-box label label-warning">Incomplete</label>';
                    editAppointment = '';
                }


                var rescheduleAppointment = '';
                if(event.isAppointmentRescheduled == 1)
                {
                    var rescheduleAppointment = '<p><strong class="italic-sm danger">N.B.: This is rescheduled appointment</strong> </p>'
                }

                if(event.next_lesson_start)
                {
                    var nextAppointment = $.fullCalendar.moment(event.next_lesson_start).format('MMMM DD, YYYY') + '<i style="font-size: 10px; font-style: italic; color; #999; ">('+ $.fullCalendar.moment(event.next_lesson_start).format('h:mm a')+ ' - '+ $.fullCalendar.moment(event.next_lesson_end).format('h:mm a')+')</i>';
                }
                else{
                    var nextAppointment = 'N/A'
                }

                var packageName = '';
                if(event.package != "Pay at Facility")
                {
                    var packageName = '<li><strong>Package: </strong>'+ event.package +'</li>';
                }

                var title = '';
                if(event.title)
                {
                    title = '<li><strong>Title: </strong>'+ event.title +'</li>';
                }

                var note = '';
                if(event.note)
                {
                    note = '<li><i style="font-size: 12px !important; color: #999;">Note:'+ event.note +'</i></li>';
                }

                element.popover({
                    title: "Appointment Details",
                    placement:'auto',
                    html:true,
                    trigger : 'click',
                    animation : 'true',
                    content: payAtFacility + '<ul class="data-list">' +
                    title +
                    '<li><strong>Student Name: </strong>'+ event.name +' <a href="<?php echo Router::url('/', true);?>instructor/users/details/'+event.uuid+'" class="d-btn margin-left-15">View Profile</a></li>' +
                    '<li><strong>Email: </strong>'+ event.username +'</li>' +
                    '<li><strong>Phone: </strong>'+ event.student_profile.phone +'</li>' +
                    packageName  +
                    lessonDeduct  +
                    lessonLeft  +
                    lessonScheduled  +
                    paymentStatus  +
                    '<li><strong>Appointment Date: </strong>'+ $.fullCalendar.moment(event.start._i).format('ddd, MMMM DD, YYYY') +'</li>' +
                    '<li><strong>Appointment Time: </strong>'+ $.fullCalendar.moment(event.start._i).format('h:mm a')+ ' - '+ $.fullCalendar.moment(event.end._i).format('h:mm a') +'</li>' +
                    '<li><strong>Next Lesson: </strong>'+ nextAppointment +'</li>' +
                    '<li><strong>Current Status: </strong>'+ appointmentStatusLabel +'</li>' +
                    '<li><strong>Waiting List: </strong>'+ event.count_waiting +'</li>' +
                    appointmentType +
                    note +
                    '</ul><div class="student-box"></div>' +
                    '<div>'+editAppointment+' '+appointmentBtn+'</div>',
                    container:'body'
                });
            }

            <!---- Manage Group Lesson Event View ----->
            function groupLessonEventView(event, element)
            {

                element.css('background-color', '#d9edf7');
                element.css('border-color', '#bce8f1');
                element.css('color', '#31708f');

                element.find('.fc-event-time').text('');
                element.find('.fc-title').append('<br/> <br/> ' +
                    '<strong class="fc_title_group_lesson" style="font-size: 11px; text-transform: uppercase; margin-top: 11px; color: #31708f">Group Lesson</strong>' +
                    '<div class="delete_spot"><a href="<?php echo Router::url('/', true);?>calendars/delete_group_lesson/'+event.id+'"><i class="fa fa-times"></i></a></div>');

                var title = '';
                if(event.title)
                {
                    title = '<li><strong>Title: </strong>'+ event.title +'</li>';
                }

                var desc = '';
                if(event.description)
                {
                    desc = '<li><strong>Description: </strong>'+ event.description +'</li>';
                }


                element.popover({
                    title: "Group Lesson",
                    placement:'auto',
                    html:true,
                    trigger : 'click',
                    animation : 'true',
                    content: '<ul class="data-list">' +
                    title +
                    desc +
                    '<li><strong>price:  </strong>$'+ event.price +'</li>' +
                    '<li><strong>Student Limit: </strong>'+ event.student_limit +'</li>' +
                    '<li><strong>Filled Up: </strong>'+ event.filled_up_student +'</li>' +
                    '<li><strong>Available: </strong>'+ (event.student_limit - event.filled_up_student) +'</li>' +
                    '<li><strong>Appointment Date: </strong>'+ $.fullCalendar.moment(event.start._i).format('ddd, MMMM DD, YYYY') +'</li>' +
                    '<li><strong>Appointment Time: </strong>'+ $.fullCalendar.moment(event.start._i).format('h:mm a')+ ' - '+ $.fullCalendar.moment(event.end._i).format('h:mm a') +'</li>' +
                    '<li><br/><a onclick="createGroupLesson('+event.id+')" class="btn btn-theme-info width-100-p text-uppercase">Edit Group lesson</a><br/><br/><a id="'+event.id+'" href="<?php echo Router::url('/', true);?>instructor/lessons/details/'+event.id+'" class="btn btn-theme width-100-p text-uppercase">View Details</a></li>' +
                    '</ul>',
                    container:'body'
                });
            }
            <!---- /Manage Group Lesson Event View ----->


            <!---- Manage Block Time Event View ----->
            function blockTimeEventView(event, element)
            {

                element.css('color', '#a94442');
                element.css('background-color', '#EEE');
                element.css('border-color', '#DDD');

                element.find('.fc-event-time').text('');
                element.find('.fc-event-time').append('<label class="appointment-status-for-box label label-warning">Blocked Time</label><div class="delete_spot"><a href="<?php echo Router::url('/', true);?>calendars/delete_block_time/'+event.id+'"><i class="fa fa-times"></i></a></div>');


                var title = '';
                if(event.title)
                {
                    title = '<li><strong>Title: </strong>'+ event.title +'</li>';
                }

                var message = '';
                if(event.message)
                {
                    message = '<li><strong>Message: </strong>' + event.message + '</li>';
                }

                element.popover({
                    title: "<span style='color: #a94442;'>Block Time</span>",
                    placement:'auto',
                    html:true,
                    trigger : 'click',
                    animation : 'true',
                    content: '<ul class="data-list block_time_list">' +
                    title +
                    message +
                    '<li><strong>Block Date: </strong>'+ $.fullCalendar.moment(event.start._i).format('ddd, MMMM DD, YYYY') +'</li>' +
                    '<li><strong>Block Time: </strong>'+ $.fullCalendar.moment(event.start._i).format('h:mm a')+ ' - '+ $.fullCalendar.moment(event.end._i).format('h:mm a') +'</li>' +
                    '</ul>',
                    container:'body'
                });
            }
            <!---- /Manage BLock Time Event View ----->


            function fetchStudentDetailsForStudent(studentID)
            {
                $('.not-able').hide();
                $('#setAppointmentBtn').hide();
                $('.appointmentTimeFrame').hide();
                $('#packageList').hide();

                $('.loadingEvent').show().delay(2000);


                if(studentID != 0){


                    $.getJSON('<?php echo Router::url('/', true);?>instructor/packages/usersPackages/'+studentID, function (data) {
                        $('.loadingEvent').hide();
                        if(data.package){
                            var studentPackages = '<label>Select Package for Appointment</label><select class="form-control" name="packageID" id="packageID">';
                            $.each(data.package, function (key, value) {
                                studentPackages += '<option value="' + key + '">' + value + '</option>';
                            });
                            studentPackages += '</select>';
                            $('#packageList').html(studentPackages);
                        }
                        else{
                            $('#packageList').html(null);
                        }

                        $('#lesson_left').html('<strong>Lessons Left: '+ data.lessonLeft +'</strong>');


                        var lessonWillDeduct = '';


                        if(data.appointments){
                            var calcelledAppointments = '<label>Reschedule for cancelled appointment</label><select class="form-control" name="appointmentID" id="appointmentID"> <option>Choose Lesson</option>';
                            if(data.package != 'error'){
                                $.each(data.appointments, function (key, value) {
                                    calcelledAppointments += '<option value="' + key + '">' + value + '</option>';
                                });
                            }


                            calcelledAppointments += '</select>';
                            $('#rescheduleAppointments').html(calcelledAppointments);
                            $('#rescheduleAppointments').hide();
                            $('#wantToReschedule').show();
                            $('#wantToReschedule').addClass('animated fadeIn');
                        }
                        else{
                            $('#rescheduleAppointments').html(null);
                            $('#wantToReschedule').hide();
                        }

                        if(data.package == null && data.appointments == null){
                            $('.not-able').show();
                            $('.not-able').html("N.B. You can not able to set appointment with this student");
                            $('#setAppointmentBtn').hide();
                            $('.appointmentTimeFrame').hide();
                            $('#packageList').hide();
                            $('.not-able').addClass('animated fadeIn');
                        }
                        else{
                            $('.not-able').hide();
                            $('#setAppointmentBtn').show();
                            $('.appointmentTimeFrame').show();
                            $('#packageList').show();
                            $('#appointmentNote').show();
                        }
                    });
                }
                else{
                    $('.loadingEvent').hide();
                    $('.not-able').show();
                    $('.not-able').html("Please choose student for set appointment");
                }
            }



            function loadCalendar() {
                $(document).ajaxStart($.blockUI(
                    {
                        message: '<h1><img src="<?php echo Router::url('/', true);?>theme/Instructor/img/busy.gif" /> ' + 'Just a moment...</h1>'
                    }
                )).ajaxStop($.unblockUI);

                $('#calendar').fullCalendar({
                    header: {
                        left: 'prev,next today',
                        center: 'title',
                        right: getRightOptions()
                    },
                    defaultView: getDefaultView(),
                    allDayDefault: false,
                    allDaySlot: false,
                    slotEventOverlap: true,
                    slotDuration: "00:15:00",
                    contentHeight: 700,
                    scrollTime: "7:00:00",
                    axisFormat: 'HH:mm a',
                    timeFormat: 'HH:mm A()',
                    resources: [
                        getResources()
                    ],
                    events: {
                        url: '<?php echo Router::url('/', true);?>calendars/getTCEvents',
                        type: 'POST',
                        data: {
                            instructors: getInstructors()
                        },
                        error: function () {

                        },
                        success: function (data) {

                        }
                    },
                    eventRender: function (event, element) {
                        if(event.type == 'event'){
                            availabilityEventView(event, element);
                        }
                        else if(event.type == 'group_lesson'){
                            console.log(event);
                            groupLessonEventView(event, element);
                        }
                        else if(event.type == 'block_time'){
                            blockTimeEventView(event, element);
                        }
                        else if(event.type == 'appointment'){
                            appointmentEventView(event, element);
                            appointmentEventPopover(event, element);
                        }

                        $('body').on('click', function (e) {
                            if (!element.is(e.target) && element.has(e.target).length === 0 && $('.popover').has(e.target).length === 0)
                                element.popover('hide');
                        });
                    },
                    dayClick: function(date, allDay, jsEvent, view, e) {

                        if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
                            $('.right_button_option').show();
                            closeModalPopover();
                            $('.right_button_option').css('left', '0px');
                            $('.right_button_option').css('bottom', '200px');
                        }
                        else{

                            var dateTime = $.fullCalendar.moment(date._d).format('dddd, MMMM DD YYYY');
                            $('#setAppointmentDate').text(dateTime);
                            $('.setAppointmentDate').val(dateTime);
                            $('#instructorList').val(allDay.data.id);
                            studentTypeAhead(allDay.data.id);

                            var left = 50;
                            var theHeight = $('.right_button_option').height();
                            var windowHeight = $( window ).height();
                            var top = (windowHeight - theHeight) / 2;

                            $('.right_button_option').css('left', 40 + '%');
                            $('.right_button_option').css('top', 40 + '%');
                            $('.right_button_option').show();
                            closeModalPopover();
                        }
                    },
                    eventClick: function (data, allDay, jsEvent, view) {

                        if(data.type == 'event'){
                            //setAppointment(parseInt(data.id));

                            var dateTime = $.fullCalendar.moment(data.start._i).format('dddd, MMMM DD YYYY');
                            $('#setAppointmentDate').text(dateTime);
                            $('.setAppointmentDate').val(dateTime);

                            var left = 50;
                            var theHeight = $('.right_button_option').height();
                            var windowHeight = $( window ).height();
                            var top = (windowHeight - theHeight) / 2;

                            $('.right_button_option').css('left', 40 + '%');
                            $('.right_button_option').css('top', 40 + '%');
                            $('.right_button_option').show();
                            closeModalPopover();

                        }

                        if(data.type == 'appointment'){
                            manageAppointmentView(data);
                        }
                    }
                });

                $('#calendar').fullCalendar('refetchEvents');

                setCalendarHeader();
            }


            var students = new Bloodhound({
                datumTokenizer: function (datum) {
                    return Bloodhound.tokenizers.whitespace(datum.value);
                },
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                remote: {
                    url: '<?php echo Router::url('/', true);?>instructor/contacts/getStudent/'+parseInt($('#instructorList').val())+'/%QUERY',
                    filter: function (students) {
                        return $.map(students, function (contact) {
                            return {
                                value: contact['0']['contactName']
                            };
                        });
                    }
                }
            });

            students.initialize();

           var myTypeahead = $('#studentID').typeahead(null, {
                name: 'states',
                displayKey: 'value',
                source: students.ttAdapter()
            }).on('typeahead:selected', function (obj, datum) {
                var studentInfo = datum.value.split(' - ');
                var email = studentInfo[1];

                $.ajax({
                    url: "<?php echo Router::url('/', true);?>instructor/users/getUserIDByEmail",
                    type: "POST",
                    data:  {email: email},
                    dataType: "json",
                    success: function (studentID) {
                        fetchStudentDetailsForStudent(studentID);
                    },
                    error: function (xhr, status, error) {}
                });

            });

            $('#instructorList').change(function(){

                $('#packageList').html('<label>Select Package for Appointment</label><select class="form-control" name="packageID" id="packageID"> <option value="00000000001">Pay at Facility</option> </select>');
                $('#lesson_left').html('<strong>Lessons Left: 0</strong>');
                myTypeahead.typeahead('val','');

                $('#lessonWillDeduct').val(1);
                $('#appointmentStart').val('07:00 AM');
                $('#appointmentEnd').val('07:15 AM');
                $('#appointmentNote').val('');
                $('#appointment_title input').val('');
                $('#setAppointmentBtn').hide();

                studentTypeAhead($(this).val())
            });


            function studentTypeAhead(instructorID)
            {
                students.clear();
                students.remote.url = '<?php echo Router::url('/', true);?>instructor/contacts/getStudent/'+instructorID+'/%QUERY';
                students.initialize(true);
            }

            $(".not_now").on("click", function () {
                $('.right_button_option').hide();
            });

            $('div').on('hidden.bs.modal', function () {

                $('#packageList').html('<label>Select Package for Appointment</label><select class="form-control" name="packageID" id="packageID"> <option value="00000000001">Pay at Facility</option> </select>');
                $('#lesson_left').html('<strong>Lessons Left: 0</strong>');
                myTypeahead.typeahead('val','');
                $('#lessonWillDeduct').val(1);
                $('#appointmentStart').val('07:00 AM');
                $('#appointmentEnd').val('07:15 AM');
                $('#appointmentNote').val('');
                $('#appointment_title input').val('');
                $('#setAppointmentBtn').hide();

            })

            $('div').on('shown.bs.modal', function () {
                $(document).ready(function() {
                    $('.right_button_option').hide();
                    $('.popover').popover().hide();
                });
            })
        }
    );
</script>


<!-- Modal For Displaying Schedule -->
<div class="modal event-modal fade" id="setAppointmentModal" tabindex="-1" role="dialog" aria-labelledby="createEventLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="border-bottom: 1px solid #e5e5e5;">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                </button>
                <h4 class="sm-title">Create an Appointment</h4>
            </div>
            <div class="modal-body">
                <form action="<?php echo Router::url('/', true);?>/calendars/setAppointmentByInstructor/" method="post">
                    <div class="loadingEvent" style="display: none;">
                        <?php echo $this->Html->image('ajax-loader2.gif')?><br/>
                        Loading, please wait...
                    </div>
                    <div class="setAppointmentEventInfoArea">
                        <div class="form-group">
                            <label for="exampleInputEmail1" id="setAppointmentDate"></label>
                            <input name="setAppointmentDate" type="hidden" class="setAppointmentDate"/>
                            <div id="appointmentIDInEditAppointment"></div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-lg-12">
                                    <label for="exampleInputEmail1">Want to custom date?</label>
                                    <div class="input-group">
                                        <input name="custom_date" type="text" class="form-control GroupLessonDate datepicker" placeholder="Date">
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Choose Instructor</label>
                            <select class="form-control form-inline" id="instructorList" name="instructor_id">
                                <?php foreach ($instructors as $instructor => $value):?>
                                    <option value="<?php echo (int)$instructor; ?>">
                                        <?php echo $value; ?>
                                    </option>
                                <?php endforeach;?>
                            </select>
                        </div>

                        <div class="form-group" id="studentList">
                            <label>Student name:</label>
                            <input name="studentID" id="studentID" class="form-control" type="text" placeholder="Type student name">
                        </div>

                        <div class="form-group" id="appointment_title">
                            <label>Appointment Title:</label>
                            <input name="appointment_title" class="form-control" type="text" placeholder="Appointment Title">
                        </div>

                        <div class="form-group" id="packageList" style="display: block;">
                            <label>Select Package for Appointment</label>
                            <select class="form-control" name="packageID" id="packageID">
                                <option value="00000000001">Pay at Facility</option>
                            </select>
                        </div>

                        <div class="" id="lesson_left">Lessons Left: 0</div>
                        <div id="lesson_will_deduct">
                            <label>Number of Lessons to Subtract</label>
                            <select class="form-control" id="lessonWillDeduct" name="lessonWillDeduct">
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                            </select>
                        </div>
                        <div class="form-group" id="rescheduleAppointments">

                        </div>
                        <div id="wantToReschedule" class="checkbox" style="display: none;">
                            <label>
                                <input type="checkbox" value="reschedule"> Want to reschedule?
                            </label>
                        </div>
                        <div class="row appointmentTimeFrame">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Appointment Start</label>
                                    <?php
                                    echo $this->Form->input('start', array(
                                        'id' => 'appointmentStart',
                                        'class' => 'form-control',
                                        'type' => 'select',
                                        'label' => false,
                                        'div' => false,
                                        'options' => $this->Utilities->getHours()
                                    ));
                                    ?>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Appointment End</label>
                                    <?php
                                    echo $this->Form->input('end', array(
                                        'id' => 'appointmentEnd',
                                        'class' => 'form-control',
                                        'type' => 'select',
                                        'label' => false,
                                        'div' => false,
                                        'options' => $this->Utilities->getHours()
                                    ));
                                    ?>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <textarea class="form-control" name="appointmentNote" id="appointmentNote" rows="4" placeholder="Appointment Note" style="display: none;"></textarea>
                        </div>

                        <br/>
                        <p style="display: none;" class="not-found not-able">N.B. You can not able to set appointment with this student</p>
                        <button class="btn btn-theme" id="setAppointmentBtn">Set Appointment</button>
                    </div>
                </form>

            </div>
            <br/>
            <div class="modal-footer"></div>
        </div>
    </div>
</div>
<!-- /Modal For Displaying Schedule -->

<!-- Modal for the create group lesson -->
<div class="modal event-modal fade" id="createGroupLesson" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <form action="<?php echo Router::url('/', true);?>calendars/save_group_lesson" id="setAvailabilityForm" method="post">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="sm-title">Create Group Lesson</h4>
                </div>
                <div class="modal-body">
                    <div class="eventInfoArea">
                        <div id="groupLessonID"></div>
                        <div class="form-group">
                            <label>Choose Instructor</label>
                            <select class="form-control form-inline" id="instructorList" name="instructor_id">
                                <?php foreach ($instructors as $instructor => $value):?>
                                    <option value="<?php echo $instructor; ?>">
                                        <?php echo $value; ?>
                                    </option>
                                <?php endforeach;?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Title</label>
                            <input name="data[GroupLesson][title]" type="text" class="form-control" placeholder="Title" id="GroupLessonTitle">
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Description</label>
                            <textarea name="data[GroupLesson][description]" class="form-control" placeholder="description" rows="4" id="GroupLessonDesc"></textarea>
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Price</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-money"></i></span>
                                <input name="data[GroupLesson][price]" type="text" class="form-control" placeholder="Price" id="GroupLessonPrice">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-lg-12">
                                    <label for="exampleInputEmail1">Date:</label>
                                    <div class="input-group">
                                        <input name="data[GroupLesson][date]" type="text" class="form-control GroupLessonDate datepicker" placeholder="Date">
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-lg-6">
                                    <label for="exampleInputEmail1">Starting Time:</label>
                                    <div class="input-group">
                                        <select name="data[GroupLesson][starting_time]" class="form-control" id="GroupLessonStart">
                                            <?php echo $this->Utilities->getFromHours('Starting Time');?>
                                        </select>
                                        <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <label for="exampleInputEmail1">Ending Time:</label>
                                    <div class="input-group">
                                        <select name="data[GroupLesson][ending_time]" class="form-control" id="GroupLessonEnd">
                                            <?php echo $this->Utilities->getFromHours('Ending Time');?>
                                        </select>
                                        <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Maximum Number of Student:</label>
                            <input name="data[GroupLesson][student_limit]" type="text" class="form-control" placeholder="Maximum number of student" id="GroupLessonStudentLimit">
                        </div>

                        <div class="pull-right">
                            <button type="button" class="btn btn-black-radius" data-dismiss="modal">Close</button>
                            <button class="btn btn-theme"  style="display: inline-block;" id="groupLessonSaveBtn">Save</button>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<!-- Modal for the create block time -->
<div class="modal event-modal fade" id="blockTimeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <form action="<?php echo Router::url('/', true);?>calendars/block_time" id="setAvailabilityForm" method="post">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="sm-title">Block Time</h4>
                </div>
                <div class="modal-body">
                    <div class="eventInfoArea">

                        <div class="form-group">
                            <label>Choose Instructor</label>
                            <select class="form-control form-inline" id="instructorList" name="instructor_id">
                                <?php foreach ($instructors as $instructor => $value):?>
                                    <option value="<?php echo $instructor; ?>">
                                        <?php echo $value; ?>
                                    </option>
                                <?php endforeach;?>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Title</label>
                            <input name="data[BlockTime][title]" type="text" class="form-control" placeholder="Title">
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Description</label>
                            <textarea name="data[BlockTime][message]" class="form-control" placeholder="description" rows="4"></textarea>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-lg-12">
                                    <label for="exampleInputEmail1">Date:</label>
                                    <div class="input-group">
                                        <input name="data[BlockTime][date]" type="text" class="form-control datepicker" placeholder="Date">
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-lg-6">
                                    <label for="exampleInputEmail1">Starting Time:</label>
                                    <div class="input-group">
                                        <select name="data[BlockTime][starting_time]" class="form-control">
                                            <?php echo $this->Utilities->getFromHours('Starting Time');?>
                                        </select>
                                        <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <label for="exampleInputEmail1">Ending Time:</label>
                                    <div class="input-group">
                                        <select name="data[BlockTime][ending_time]" class="form-control">
                                            <?php echo $this->Utilities->getFromHours('Ending Time');?>
                                        </select>
                                        <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="pull-right">
                            <button type="button" class="btn btn-black-radius" data-dismiss="modal">Close</button>
                            <button class="btn btn-theme"  style="display: inline-block;">Save</button>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- Modal for the create block time -->

<?php echo $this->Html->css(array('jq'))?>
<?php echo $this->Html->script('jquery.block.ui')?>
<?php echo $this->Html->script('typehead');?>
