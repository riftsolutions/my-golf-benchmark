<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../../assets/ico/favicon.ico">

    <title><?php echo $title_for_layout; ?></title>

    <!----- Bootstrap and fontAwesome core CSS ----->
    <?php echo $this->Html->css(array('bootstrap.min', 'font-awesome'));?>
    <!--<link href='http://fonts.googleapis.com/css?family=Ubuntu:400,500,700,300italic,400italic,500italic,700italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,500' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Overlock:400,700,400italic' rel='stylesheet' type='text/css'>-->
    <!----- /Bootstrap and fontAwesome core CSS ----->

    <!----- Custom styles for this template ----->
    <?php echo $this->Html->css(array('datatables', 'datepicker', 'range_picker', 'calendar', 'style'));?>
    <!----- /Custom styles for this template ----->

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="assets/js/html5shiv.js"></script>
    <script src="assets/js/respond.min.js"></script>

    <![endif]-->

    <?php echo $this->Html->css('fullcalender')?>
    <?php echo $this->Html->script(array('moment.min', 'jquery.min', 'bootstrap.min', 'jquery-ui.custom.min', 'fullcalender'));?>

</head>
<body>

<!----- HEADER AREA ----->
<header class="header">
    <nav class="navbar navbar-inverse" role="navigation">
        <div class="container-fluid">
            <div class="row">


                <button type="button" class="navbar-toggle open-sidebar" data-toggle="offcanvas">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <div class="navbar-brand" href="dashboard.html">
                        <?php echo $this->Html->image('logo.png', array('class' => 'img-responsive visible-lg visible-md', 'url' => array('controller' => 'dashboards', 'action' => 'index')));?>
                        <?php echo $this->Html->image('logo-small.png', array('class' => 'img-responsive visible-xs visible-sm', 'url' => array('controller' => 'dashboards', 'action' => 'index')));?>
                    </div>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right profile-nav navigation">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <?php
                                if($userInfo['Profile']['profile_pic_url']){
                                    echo $this->Html->image('profiles/'.$userInfo['Profile']['profile_pic_url'], array('class' => 'img-circle pro-img-sm'));
                                }
                                else{
                                    echo $this->Html->image('avatar.jpg', array('class' => 'img-circle pro-img-sm'));
                                }
                                ?>
                                <b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu profile-dropdown">
                                <li class="active">
                                    <?php echo $this->Html->link(
                                        '<i class="fa fa-user"></i> Profile</a>',
                                        array('controller' => 'profiles', 'action' => 'index'),
                                        array('escape' => false)
                                    ); ?>
                                </li>
                                <li>
                                    <?php echo $this->Html->link(
                                        '<i class="fa fa-wrench"></i> Setting</a>',
                                        array('controller' => 'profiles', 'action' => 'settings'),
                                        array('escape' => false)
                                    ); ?>
                                </li>
                                <li>
                                    <?php echo $this->Html->link(
                                        '<i class="fa fa-power-off"></i> Logout</a>',
                                        array('controller' => 'users', 'action' => 'logout', 'admin' => false),
                                        array('escape' => false)
                                    ); ?>
                                </li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <?php echo $this->element('short-notify');?>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>
</header>
<!----- /HEADER AREA ----->