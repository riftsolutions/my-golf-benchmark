<div class="col-xs-6 col-sm-2 sidebar-offcanvas" id="sidebar" role="navigation">
    <div class="row">
        <div class="sidebar">
            <ul id="menu" class="nav nav-list">
                <!-- Dashboard -->
                <li>
                    <?php echo $this->Html->link('<i class="fa fa-home"></i> Dashboard
', array('controller' => 'dashboards', 'action' => 'index'), array('escape' => false))?>
                </li>

                <!-- Calender -->
                <li>
                    <a href="#">
                        <i class="fa fa-calendar"></i>
                        Calendar
                        <span class="glyphicon arrow"></span>
                    </a>
                    <ul class="nav nav-list submenu collapse">
                        <li>
                            <?php echo $this->Html->link(
                                'My Calendar',
                                array('controller' => 'calendars', 'action' => 'index'),
                                array('escape' => false)
                            ); ?>
                        </li>
                        <li>
                            <?php echo $this->Html->link(
                                'Color Legend',
                                array('controller' => 'calendars', 'action' => 'legend'),
                                array('escape' => false)
                            ); ?>
                        </li>
                    </ul>
                </li>

                <!-- Student -->
                <li>
                    <a href="#">
                        <i class="fa fa-mortar-board"></i>
                        Students
                        <span class="glyphicon arrow"></span>
                    </a>
                    <ul class="nav nav-list submenu collapse">
                        <li>
                            <?php echo $this->Html->link(
                                'Add New Student',
                                array('controller' => 'users', 'action' => 'add_student'),
                                array('escape' => false)
                            ); ?>
                        </li>
                        <li>
                            <?php echo $this->Html->link(
                                'Student List',
                                array('controller' => 'users', 'action' => 'student'),
                                array('escape' => false)
                            ); ?>
                        </li>
                    </ul>
                </li>

                <!-- Leads -->
                <li>
                    <a href="#">
                        <i class="fa fa-leaf"></i>
                        Leads
                        <span class="glyphicon arrow"></span>
                    </a>
                    <ul class="nav nav-list submenu collapse">
                        <li>
                            <?php echo $this->Html->link(
                                'Add New Lead',
                                array('controller' => 'contacts', 'action' => 'add'),
                                array('escape' => false)
                            ); ?>
                        </li>
                        <li>
                            <?php echo $this->Html->link(
                                'Lead List',
                                array('controller' => 'contacts', 'action' => 'list'),
                                array('escape' => false)
                            ); ?>
                        </li>
                    </ul>
                </li>

                <!-- Instructor -->
                <li>
                    <a href="#">
                        <i class="fa fa-male"></i>
                        Instructors
                        <span class="glyphicon arrow"></span>
                    </a>
                    <ul class="nav nav-list submenu collapse">
                        <li>
                            <?php echo $this->Html->link(
                                'Add New Instructor',
                                array('controller' => 'users', 'action' => 'add'),
                                array('escape' => false)
                            ); ?>
                        </li>
                        <li>
                            <?php echo $this->Html->link(
                                'Instructor List',
                                array('controller' => 'users', 'action' => 'instructor'),
                                array('escape' => false)
                            ); ?>
                        </li>
                    </ul>
                </li>

                <!-- Reports -->
                <li>
                    <a href="#">
                        <i class="fa fa-line-chart"></i>
                        Reports
                        <span class="glyphicon arrow"></span>
                    </a>
                    <ul class="nav nav-list submenu">
                        <li>
                            <?php echo $this->Html->link(
                                'User Report',
                                array('controller' => 'reports', 'action' => 'user'),
                                array('escape' => false)
                            ); ?>
                        </li>
                        <li>
                            <?php echo $this->Html->link(
                                'Lead Report',
                                array('controller' => 'reports', 'action' => 'lead'),
                                array('escape' => false)
                            ); ?>
                        </li>
                        <li>
                            <?php echo $this->Html->link(
                                'Referral Sources',
                                array('controller' => 'reports', 'action' => 'sources'),
                                array('escape' => false)
                            ); ?>
                        </li>
                        <li>
                            <?php echo $this->Html->link(
                                'Transactions',
                                array('controller' => 'reports', 'action' => 'transaction'),
                                array('escape' => false)
                            ); ?>
                        </li>
                    </ul>
                </li>

                <!-- Appointment List -->
                <li>
                    <?php echo $this->Html->link(
                        '<i class="fa fa-clock-o"></i> Appointment List',
                        array('controller' => 'appointments', 'action' => 'tc_list'),
                        array('escape' => false)
                    ); ?>



                </li>

                <!-- Group Lesson -->
                <li>
                    <?php echo $this->Html->link(
                        '<i class="fa fa-object-group"></i> My Group Lesson',
                        array('controller' => 'lessons', 'action' => 'group'),
                        array('escape' => false)
                    ); ?>
                </li>

                <!-- Accounts -->
                <li>
                    <a href="#">
                        <i class="fa fa-user"></i>
                        My Account
                        <span class="glyphicon arrow"></span>
                    </a>
                    <ul class="nav nav-list submenu">
                        <li>
                            <?php echo $this->Html->link(
                                'Profile</a>',
                                array('controller' => 'profiles', 'action' => 'index'),
                                array('escape' => false)
                            ); ?>
                        </li>
                        <li>
                            <?php echo $this->Html->link(
                                'Update Profile</a>',
                                array('controller' => 'profiles', 'action' => 'update'),
                                array('escape' => false)
                            ); ?>
                        </li>
                        <li>
                            <?php echo $this->Html->link(
                                'Change Profile Picture</a>',
                                array('controller' => 'profiles', 'action' => 'change_photo'),
                                array('escape' => false)
                            ); ?>
                        </li>
                        <li>
                            <?php echo $this->Html->link(
                                'Change Password</a>',
                                array('controller' => 'profiles', 'action' => 'change_password'),
                                array('escape' => false)
                            ); ?>
                        </li>
                    </ul>
                </li>

                <li><a></a></li>
            </ul>
        </div>
    </div>
</div>