<?php
/**
 * @var $this View
 */
?>
<h2 class="page-title">Instructor Details</h2>
<hr class="shadow-line"/>
<div class="content">
    <div class="row">

        <div class="col-lg-4 col-md-4 col-sm-4">

            <!----- APPLICATION OVERVIEW ------>
            <div class="d-box">
                <h4 class="d-title"><i class="fa fa-flask yellowgreen"></i> Application Overview</h4>
                <ul class="d-list">
                    <li>
                        <?php echo $this->Html->link('Total leads', array('controller' => 'contacts', 'action' => 'list', 'all')); ?>
                        <span class="pull-right d-badge d-badge-danger"><?php echo $contactsOverview[0][0]['totalContact']; ?></span>
                    </li>
                    <li>
                        <?php echo $this->Html->link('Total Students', array('controller' => 'users', 'action' => 'list', 'all')); ?>
                        <span class="pull-right d-badge d-badge-gray"><?php echo $countStudent; ?></span>
                    </li>
                    <li>
                        <?php echo $this->Html->link('Total Running POS', array('controller' => 'packages', 'action' => 'pos', 'all')); ?>
                        <?php echo $this->Html->link('Total Running POS', array('controller' => 'packages', 'action' => 'pos', 'all')); ?>
                        <span class="pull-right d-badge d-badge-green"><?php echo $countPOS; ?></span>
                    </li>
                    <li>
                        <?php echo $this->Html->link('Total Benchmarks', array('controller' => 'benchmarks', 'action' => 'list', 'all')); ?>
                        <span class="pull-right d-badge d-badge-orange"><?php echo $countBenchmark; ?></span>
                    </li>
                </ul>
                <div class="see-more">
                    <?php echo $this->html->link('See More <i class="fa fa-long-arrow-right"></i>', array('controller' => 'lessons', 'action' => 'list'), array('escape' => false)) ?>
                </div>

                <div class="clearfix"></div>
            </div>
            <!----- /APPLICATION OVERVIEW ------>

            <!----- UPCOMING APPOINTMENT ------>
            <div class="d-box">
                <h4 class="d-title padding-bottom-10">
                    <i class="fa fa-fighter-jet danger"></i> Upcoming Appointments
                    <?php
                    if ($upComingAppointments) {
                        echo $this->html->link('See Full List', array('controller' => 'appointments', 'action' => 'list/upcoming'), array('escape' => false, 'class' => 'full-list pull-right'));
                    }
                    ?>
                </h4>

                <?php if ($upComingAppointments): ?>
                    <table class="table margin-top-25">
                        <thead>
                        <th>Appointment Time</th>
                        <th>Student Name</th>
                        <th style="text-align: right;">Action</th>
                        </thead>
                        <tbody>
                        <?php foreach ($upComingAppointments as $appointment): ?>
                            <tr>
                                <td>
                                    <?php echo $this->Time->format('d M, Y', $appointment['Appointment']['start']) ?>
                                    <i class="italic-sm">(
                                        <?php echo $this->Time->format('h:i A', $appointment['Appointment']['start']) ?>
                                        - <?php echo $this->Time->format('h:i A', $appointment['Appointment']['end']) ?>
                                        )
                                    </i>
                                </td>
                                <td>
                                    <?php echo $this->Html->link($appointment[0]['studentName'], array('controller' => 'users', 'action' => 'details', $appointment['User']['uuid']), array('class' => 'green')); ?>
                                </td>
                                <td style="text-align: right;">
                                    <?php echo $this->Html->link('View', array('controller' => 'appointments', 'action' => 'view', $appointment['Appointment']['id']), array('class' => 'd-btn')); ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>

                        </tbody>
                    </table>

                    <div class="see-more">
                        <?php echo $this->html->link('See More <i class="fa fa-long-arrow-right"></i>', array('controller' => 'appointments', 'action' => 'list'), array('escape' => false)) ?>
                    </div>

                <?php else: ?>
                    <?php echo $this->element('not-found') ?>
                <?php endif; ?>

                <div class="clearfix"></div>
            </div>
            <!----- /UPCOMING APPOINTMENT ------>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4">
            <div class="d-box">
                <h4 class="d-title margin-bottom-15"><i class="fa fa-info-circle yellowgreen"></i> General Information</h4>
                <?php
                if ($userInformation['Profile']['profile_pic_url']) {
                    echo $this->Html->image('profiles/' . $userInformation['Profile']['profile_pic_url'],
                        array('class' => 'img-thumbnail profile-pic-small', 'url' => array('controller' => 'users', 'action' => 'details', $userInformation['User']['uuid'])));
                } else {
                    echo $this->Html->image('avatar.jpg', array('class' => 'img-thumbnail profile-pic-small', 'url' => array('controller' => 'profiles', 'action' => 'index')));
                }
                ?>

                <ul class="d-list">
                    <li>
                        <strong>Name:</strong>
                    <span>
                        <?php echo $this->Html->link($userInformation['Profile']['name'], array('controller' => 'users', 'action' => 'details', $userInformation['User']['uuid'])) ?>
                    </span>
                    </li>
                    <li>
                        <strong>Email:</strong>
                    <span>
                        <?php echo $this->Html->link($userInformation['User']['username'], array('controller' => 'users', 'action' => 'details', $userInformation['User']['uuid'])) ?>
                    </span>
                    </li>
                    <li>
                        <strong>Phone: </strong>
                            <span>
                                <?php
                                if ($userInformation['Profile']['phone']) {
                                    echo $userInformation['Profile']['phone'];
                                } else {
                                    echo 'N/A';
                                }
                                ?>
                            </span>
                    </li>
                    <li>
                        <strong>Street 1: </strong>
                        <?php
                        if ($userInformation['Profile']['street_1']) {
                            echo $userInformation['Profile']['street_1'];
                        } else {
                            echo 'N/A';
                        }
                        ?>
                    </li>
                    <li>
                        <strong>Street 2: </strong>
                        <?php
                        if ($userInformation['Profile']['street_2']) {
                            echo $userInformation['Profile']['street_2'];
                        } else {
                            echo 'N/A';
                        }
                        ?>
                    </li>
                    <li>
                        <strong>City: </strong>
                        <?php
                        if ($userInformation['Profile']['city']) {
                            echo $userInformation['Profile']['city'];
                        } else {
                            echo 'N/A';
                        }
                        ?>
                    </li>
                    <li>
                        <strong>State: </strong>
                        <?php
                        if ($userInformation['Profile']['state']) {
                            echo $userInformation['Profile']['state'];
                        } else {
                            echo 'N/A';
                        }
                        ?>
                    </li>
                    <li>
                        <strong>Zip Code: </strong>
                        <?php
                        if ($userInformation['Profile']['postal_code']) {
                            echo $userInformation['Profile']['postal_code'];
                        } else {
                            echo 'N/A';
                        }
                        ?>
                    </li>
                    <li>
                        <strong>Member Since: </strong>
                        <?php
                        echo $this->Time->format('M d, Y', $userInformation['User']['created']);
                        ?>
                    </li>
                    <li>
                        <strong>Source: </strong>
                        <?php
                        echo $userInformation['Profile']['source'];
                        ?>
                    </li>
                    <li>
                        <strong>Rating:</strong>
                        <select class="singleUserRating">
                            <option value="" selected="selected"></option>
                            <?php
                            for ($counter = 1; $counter <= 5; $counter++) {
                                $select = '';
                                if ($counter == $userInformation['User']['rating']) {
                                    $select = 'selected';
                                }
                                echo '<option ' . $select . ' value="' . $counter . '">' . $userInformation['User']['id'] . '</option>';
                            }
                            ?>
                        </select>
                    </li>
                </ul>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4">
            <!----- UPCOMING APPOINTMENT ------>
            <div class="d-box">
                <h4 class="d-title padding-bottom-10">
                    <i class="fa fa-fighter-jet danger"></i> Recent Transactions
                    <?php
                    if ($recentBilling) {
                        echo $this->html->link('See Full List', array('controller' => 'reports', 'action' => 'transaction'), array('escape' => false, 'class' => 'full-list pull-right'));
                    }
                    ?>
                </h4>

                <?php if ($recentBilling): ?>
                    <table class="table margin-top-25">
                        <thead>
                        <th>Student Name</th>
                        <th>Amount</th>
                        <th>Created</th>
                        <th style="text-align: right;">Action</th>
                        </thead>
                        <tbody>
                        <?php foreach ($recentBilling as $billing): ?>
                            <tr>
                                <td>
                                    <?php echo $this->Html->link($billing['Profile']['first_name'] . ' ' . $billing['Profile']['last_name'],
                                        array('controller' => 'users', 'action' => 'tc_details', $billing['User']['uuid']), array('class' => 'green')); ?>
                                </td>
                                <td>
                                    <?php echo $this->Html->link($this->Number->currency($billing['Billing']['amount']), array('controller' => 'users', 'action' => 'tc_details', $billing['User']['uuid']),
                                        array('class' => 'green')); ?>
                                </td>
                                <td>
                                    <?php echo $this->Time->format('d M, Y', $billing['Billing']['created']); ?>
                                    <br>
                                    <i class="italic-sm">(
                                        <?php echo $this->Time->format('h:i A', $billing['Billing']['created']); ?>
                                        - <?php echo $this->Time->format('h:i A', $billing['Billing']['created']); ?>
                                        )
                                    </i>

                                </td>
                                <td style="text-align: right;">
                                    <?php echo $this->Html->link('View', array('controller' => 'reports', 'action' => 'transaction_view', $billing['Billing']['uuid']), array('class' => 'd-btn')); ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>

                        </tbody>
                    </table>

                    <div class="see-more">
                        <?php echo $this->html->link('See More <i class="fa fa-long-arrow-right"></i>', array('controller' => 'reports', 'action' => 'transaction'), array('escape' => false)) ?>
                    </div>

                <?php else: ?>
                    <?php echo $this->element('not-found') ?>
                <?php endif; ?>

                <div class="clearfix"></div>
            </div>
            <!----- /UPCOMING APPOINTMENT ------>

            <!----- UPCOMING APPOINTMENT ------>
            <div class="d-box">
                <h4 class="d-title padding-bottom-10">
                    <i class="fa fa-fighter-jet danger"></i> Recent Students
                    <?php
                    if ($userList) {
                        echo $this->html->link('See Full List', array('controller' => 'users', 'action' => 'student'), array('escape' => false, 'class' => 'full-list pull-right'));
                    }
                    ?>
                </h4>

                <?php if ($userList): ?>
                    <table class="table margin-top-25">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Email/Username</th>
                            <th>Phone</th>
                            <th>Created</th>
                            <th style="text-align: right;">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($userList as $users): ?>
                            <tr>
                                <td>
                                    <?php echo $this->Html->link($users['Profile']['first_name'] . ' ' . $users['Profile']['last_name'], array('controller' => 'users', 'action' => 'tc_details', $users['User']['uuid']),
                                        array('class' => 'green')) ?>
                                </td>
                                <td>
                                    <?php echo $this->Html->link($users['User']['username'], array('controller' => 'users', 'action' => 'tc_details', $users['User']['uuid'])); ?>
                                </td>
                                <td>
                                    <?php if ($users['Profile']['phone']): ?>
                                        <?php echo $this->Html->link($users['Profile']['phone'], array('controller' => 'users', 'action' => 'tc_details', $users['User']['uuid'])); ?>
                                    <?php else: ?>
                                        N/A
                                    <?php endif; ?>
                                </td>
                                <td>
                                    <?php echo $this->Time->format('d M, Y', $users['User']['created']); ?>
                                    <br>
                                    <i class="italic-sm">(
                                        <?php echo $this->Time->format('h:i A', $users['User']['created']); ?>
                                        - <?php echo $this->Time->format('h:i A', $users['User']['created']); ?>
                                        )
                                    </i>
                                </td>
                                <td style="text-align: right;">
                                    <?php echo $this->Html->link('View', array('controller' => 'users', 'action' => 'tc_details', $users['User']['uuid']), array('class' => 'd-btn')); ?>
                                </td>

                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>

                    <div class="see-more">
                        <?php echo $this->html->link('See More <i class="fa fa-long-arrow-right"></i>', array('controller' => 'users', 'action' => 'student'), array('escape' => false)) ?>
                    </div>

                <?php else: ?>
                    <?php echo $this->element('not-found') ?>
                <?php endif; ?>

                <div class="clearfix"></div>
            </div>
            <!----- /UPCOMING APPOINTMENT ------>

        </div>

    </div>
</div>


<?php echo $this->Html->script('typehead'); ?>


<script>
    $(document).ready(function () {
        var fullUrl = window.location.href.split('#');
        var noteID = fullUrl[1];
        $('#' + noteID).addClass('current-note');
    });


    $('.singleUserRating').barrating('show', {
        initialRating: 0,
        theme: 'fontawesome-stars',
        onSelect: function (value, text) {

            $.ajax({
                url: '<?php echo Router::url('/', true);?>instructor/users/set_rating',
                type: "POST",
                dataType: "json",
                data: {userID: text, rating: value},
                success: function (response) {

                    console.log(response);

                    if (response == 1) {
                        manageFlashMessage('alert-success', 'Your rating has been successfully complete');
                    }
                    else {
                        manageFlashMessage('alert-danger', 'Sorry, something went wrong');
                    }
                },
                error: function (xhr, status, error) {
                    console.log(error);
                }
            });

        }
    });


</script>


