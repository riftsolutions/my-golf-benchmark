<h2 class="page-title">Instructor Details</h2>
<hr class="shadow-line"/>

<div class="dropdown pull-right margin-bottom-10">
    <?php
    if($userInformation['User']['role'] == 3){
        echo $this->Html->link('Edit This Person', array('controller' => 'users', 'action' => 'edit', $userInformation['User']['uuid']), array('class' => 'btn btn-theme'));
    }
    elseif($userInformation['User']['role'] == 2){
        echo $this->Html->link('Edit This Person', array('controller' => 'users', 'action' => 'edit_student', $userInformation['User']['uuid']), array('class' => 'btn btn-theme'));
    }

    ?>
</div>
<div class="clearfix"></div>

<div class="row">
    <div class="col-lg-4 col-md-3 col-sm-3">
        <div class="d-box">
            <?php
            if($userInformation['Profile']['profile_pic_url']){
                echo $this->Html->image('profiles/'.$userInformation['Profile']['profile_pic_url'], array('class' => 'img-thumbnail profile-pic-small', 'url' => array('controller' => 'users', 'action' => 'details', $userInformation['User']['uuid'])));
            }
            else{
                echo $this->Html->image('avatar.jpg', array('class' => 'img-thumbnail profile-pic-small', 'url' => array('controller' => 'profiles', 'action' => 'index')));
            }
            ?>

            <ul class="d-list">
                <li>
                    <strong>Name:</strong>
                    <span>
                        <?php echo $this->Html->link($userInformation['Profile']['name'], array('controller' => 'users', 'action' => 'details', $userInformation['User']['uuid'])) ?>
                    </span>
                </li>
                <li>
                    <strong>Email:</strong>
                    <span>
                        <?php echo $this->Html->link($userInformation['User']['username'], array('controller' => 'users', 'action' => 'details', $userInformation['User']['uuid'])) ?>
                    </span>
                </li>
                <li>
                    <strong>Phone: </strong>
                            <span>
                                <?php
                                if($userInformation['Profile']['phone']){
                                    echo $userInformation['Profile']['phone'];
                                }
                                else{
                                    echo 'N/A';
                                }
                                ?>
                            </span>
                </li>
                <li>
                    <strong>Street 1: </strong>
                    <?php
                    if($userInformation['Profile']['street_1']){
                        echo $userInformation['Profile']['street_1'];
                    }
                    else{
                        echo 'N/A';
                    }
                    ?>
                </li>
                <li>
                    <strong>Street 2: </strong>
                    <?php
                    if($userInformation['Profile']['street_2']){
                        echo $userInformation['Profile']['street_2'];
                    }
                    else{
                        echo 'N/A';
                    }
                    ?>
                </li>
                <li>
                    <strong>City: </strong>
                    <?php
                    if($userInformation['Profile']['city']){
                        echo $userInformation['Profile']['city'];
                    }
                    else{
                        echo 'N/A';
                    }
                    ?>
                </li>
                <li>
                    <strong>State: </strong>
                    <?php
                    if($userInformation['Profile']['state']){
                        echo $userInformation['Profile']['state'];
                    }
                    else{
                        echo 'N/A';
                    }
                    ?>
                </li>
                <li>
                    <strong>Zip Code: </strong>
                    <?php
                    if($userInformation['Profile']['postal_code']){
                        echo $userInformation['Profile']['postal_code'];
                    }
                    else{
                        echo 'N/A';
                    }
                    ?>
                </li>
                <li>
                    <strong>Member Since: </strong>
                    <?php
                    echo $this->Time->format('M d, Y', $userInformation['User']['created']);
                    ?>
                </li>
                <li>
                    <strong>Source: </strong>
                    <?php
                    echo $userInformation['Profile']['source'];
                    ?>
                </li>
            </ul>
        </div>
    </div>
</div>

<div class="clearfix"></div>
<div class="margin-top-15">
    <hr class="shadow-line">
    <h2 class="short-title theme-color">Biography:</h2>
    <i class="italic-md theme-color">
        <?php
        if($userInformation['Profile']['biography']){
            echo $userInformation['Profile']['biography'];
        }
        else{
            echo 'N/A';
        }
        ?>
    </i>
</div>
