<?php
/**
 * @var $this View
 */
echo $this->Html->css('kit/style-217.css')
?>
<style>
    .ui-217 .ui-item {
        max-width: 100%;
        margin: 10px auto 40px auto;
        position: relative;
    }

    .bg-lblue {
        background-color: #6f8e40 !important;
    }

    .ui-217 .ui-item .ui-details {
        border-bottom: 2px solid #6f8e40;
    }

    .ui-217 .ui-item .ui-details:before, .ui-217 .ui-item .ui-details:after {
        border-top-color: #6f8e40;
    }
</style>

<h2 class="page-title">Student Details</h2>
    <div class="pull-left">
        <?php
        echo $this->Form->create(null, array(
            'url' => array('controller' => 'users', 'action' => 'change_assign',$userInformation['User']['uuid']),
            'class' => 'form-inline'
        ));
        ?>
        <div class="form-group">
        <label for="ins" class="">Instructor : </label>
        <select name="instructor_change" id="ins" onchange="this.form.submit()" class="pull-right form-control">
            <option value="">Unasigned</option>
            <?php foreach ($instructors as $instructor): ?>
                <option value="<?php echo $instructor['User']['id'] ?>" <?php echo ($userInformation['Profile']['instructor_id'] == $instructor['User']['id']) ? 'selected = true' : ''; ?>><?php echo $instructor[0]['instructorName']; ?></option>
            <?php endforeach; ?>
        </select>
            </div>
        </form>
    </div>

<hr class="shadow-line"/>

<div class="clearfix"></div>

<div class="row">
    <div class="col-lg-4 col-md-4 col-sm-4">
        <!----- USER'S OVERVIEW ------>
        <div class="d-box">
            <h4 class="d-title"><i class="fa fa-thumb-tack info"></i> Activities Overview</h4>
            <?php
            $totalLesson = 0;
            $lessonLeft = 0;
            $lessonScheduled = 0;

            if ($userInformation['CountLesson']['purchased_lesson_count']) {
                $totalLesson = $userInformation['CountLesson']['purchased_lesson_count'];
            }


            if ($userInformation['CountLesson']['lesson_left']) {
                $lessonLeft = $userInformation['CountLesson']['lesson_left'];
            }


            if ($userInformation['CountLesson']['appointed_lesson_count']) {
                $lessonScheduled = $userInformation['CountLesson']['appointed_lesson_count'];
            }

            ?>
            <ul class="d-list d-uppercase">

                <li>
                    <?php echo $this->Html->link('Total Spent', array('controller' => 'benchmarks', 'action' => 'list')); ?>
                    <span class="pull-right d-badge d-badge-danger d-uppercase"><?php echo $this->Number->currency($overview['totalSpendAmount']); ?></span>
                </li>
                <li>
                    <?php echo $this->Html->link('Items Purchased', array('controller' => 'packages', 'action' => 'list')); ?>
                    <span class="pull-right d-badge d-badge-green"><?php echo $overview['totalPackage']; ?></span>
                </li>
                <li>
                    <?php echo $this->Html->link('Upcoming APPOINTMENTS', array('controller' => 'appointments', 'action' => 'list')); ?>
                    <span class="pull-right d-badge d-badge-gray"><?php echo $overview['upcomingAppointment']; ?></span>
                </li>
                <li>
                    <?php echo $this->Html->link('Benchmarks', array('controller' => 'benchmarks', 'action' => 'list')); ?>
                    <span class="pull-right d-badge d-badge-info"><?php echo $overview['totalBenchmark']; ?></span>
                </li>
            </ul>
            <div class="clearfix"></div>
        </div>
        <!----- /USER'S OVERVIEW ------>

        <!----- RECENT APPOINTMENT ------>
        <div class="d-box">
            <h4 class="d-title padding-bottom-10">
                <i class="fa fa-clock-o green"></i> Recent Appointments List
                <?php
                if ($userInformation['Appointment']) {
                    echo $this->html->link('See Full List', array('controller' => 'appointments', 'action' => 'list?sid=' . $userInformation['User']['uuid']), array('escape' => false, 'class' => 'full-list pull-right'));
                }
                ?>
            </h4>

            <?php if ($userInformation['Appointment']): ?>
                <table class="table margin-top-25">
                    <thead>
                    <th>Appointment Time</th>
                    <th>Item</th>
                    <th style="text-align: right;">Action</th>
                    </thead>
                    <tbody>
                    <?php foreach ($userInformation['Appointment'] as $appointment): ?>
                        <tr>
                            <td>
                                <?php echo $this->Time->format('d M, Y', $appointment['start']) ?>
                                <i class="italic-sm">(
                                    <?php echo $this->Time->format('h:i A', $appointment['start']) ?>
                                    - <?php echo $this->Time->format('h:i A', $appointment['end']) ?>
                                    )
                                </i>
                            </td>
                            <td>
                                <?php
                                if ($appointment['pay_at_facility'] == null) {
                                    echo $this->Html->link($appointment['Package']['name'], array('controller' => 'packages', 'action' => 'view', $appointment['Package']['uuid']), array('class' => 'green'));
                                } else {
                                    echo '<label class="label label-info">' . $appointment['Package']['name'] . '</label>';
                                }

                                ?>
                            </td>
                            <td style="text-align: right;">
                                <?php echo $this->Html->link('View', array('controller' => 'appointments', 'action' => 'view', $appointment['uuid']), array('class' => 'd-btn')); ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>

                    </tbody>
                </table>
            <?php else: ?>
                <?php echo $this->element('not-found') ?>
            <?php endif; ?>

            <div class="clearfix"></div>
        </div>
        <!----- /RECENT APPOINTMENT ------>


    </div>
    <div class="col-lg-4 col-md-4 col-sm-4">
        <div class="d-box">
            <h4 class="d-title margin-bottom-15"><i class="fa fa-info-circle yellowgreen"></i> General Information</h4>
            <?php
            if ($userInformation['Profile']['profile_pic_url']) {
                echo $this->Html->image('profiles/' . $userInformation['Profile']['profile_pic_url'],
                    array('class' => 'img-thumbnail profile-pic-small', 'url' => array('controller' => 'users', 'action' => 'details', $userInformation['User']['uuid'])));
            } else {
                echo $this->Html->image('avatar.jpg', array('class' => 'img-thumbnail profile-pic-small', 'url' => array('controller' => 'profiles', 'action' => 'index')));
            }
            ?>

            <ul class="d-list">
                <li>
                    <strong>Name:</strong>
                    <span>
                        <?php echo $this->Html->link($userInformation['Profile']['name'], array('controller' => 'users', 'action' => 'details', $userInformation['User']['uuid'])) ?>
                    </span>
                </li>
                <li>
                    <strong>Email:</strong>
                    <span>
                        <?php echo $this->Html->link($userInformation['User']['username'], array('controller' => 'users', 'action' => 'details', $userInformation['User']['uuid'])) ?>
                    </span>
                </li>
                <li>
                    <strong>Phone: </strong>
                            <span>
                                <?php
                                if ($userInformation['Profile']['phone']) {
                                    echo $userInformation['Profile']['phone'];
                                } else {
                                    echo 'N/A';
                                }
                                ?>
                            </span>
                </li>
                <li>
                    <strong>Street 1: </strong>
                    <?php
                    if ($userInformation['Profile']['street_1']) {
                        echo $userInformation['Profile']['street_1'];
                    } else {
                        echo 'N/A';
                    }
                    ?>
                </li>
                <li>
                    <strong>Street 2: </strong>
                    <?php
                    if ($userInformation['Profile']['street_2']) {
                        echo $userInformation['Profile']['street_2'];
                    } else {
                        echo 'N/A';
                    }
                    ?>
                </li>
                <li>
                    <strong>City: </strong>
                    <?php
                    if ($userInformation['Profile']['city']) {
                        echo $userInformation['Profile']['city'];
                    } else {
                        echo 'N/A';
                    }
                    ?>
                </li>
                <li>
                    <strong>State: </strong>
                    <?php
                    if ($userInformation['Profile']['state']) {
                        echo $userInformation['Profile']['state'];
                    } else {
                        echo 'N/A';
                    }
                    ?>
                </li>
                <li>
                    <strong>Zip Code: </strong>
                    <?php
                    if ($userInformation['Profile']['postal_code']) {
                        echo $userInformation['Profile']['postal_code'];
                    } else {
                        echo 'N/A';
                    }
                    ?>
                </li>
                <li>
                    <strong>Member Since: </strong>
                    <?php
                    echo $this->Time->format('M d, Y', $userInformation['User']['created']);
                    ?>
                </li>
                <li>
                    <strong>Source: </strong>
                    <?php
                    echo $userInformation['Profile']['source'];
                    ?>
                </li>
                <li>
                    <strong>Rating:</strong>
                    <select class="singleUserRating">
                        <option value="" selected="selected"></option>
                        <?php
                        for ($counter = 1; $counter <= 5; $counter++) {
                            $select = '';
                            if ($counter == $userInformation['User']['rating']) {
                                $select = 'selected';
                            }
                            echo '<option ' . $select . ' value="' . $counter . '">' . $userInformation['User']['id'] . '</option>';
                        }
                        ?>
                    </select>
                </li>
            </ul>
        </div>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-4">
        <!----- WAITING LIST ------>
        <div class="d-box">
            <h4 class="d-title padding-bottom-10">
                <i class="fa fa-exclamation-triangle orange"></i> Recent Waiting List
                <?php
                if ($userInformation['Waiting']) {
                    echo $this->html->link('See Full List', array('controller' => 'appointments', 'action' => 'waiting?sid=' . $userInformation['User']['uuid']),
                        array('escape' => false, 'class' => 'full-list pull-right'));
                }
                ?>
            </h4>

            <?php if ($userInformation['Waiting']): ?>
                <table class="table margin-top-25">
                    <thead>
                    <th>Appointment Time</th>
                    <th>Item</th>
                    </thead>
                    <tbody>
                    <?php foreach ($userInformation['Waiting'] as $waiting): ?>
                        <tr>
                            <td>
                                <?php echo $this->Time->format('d M, Y', $waiting['start']) ?>
                                <i class="italic-sm">(
                                    <?php echo $this->Time->format('h:i A', $waiting['start']) ?>
                                    - <?php echo $this->Time->format('h:i A', $waiting['end']) ?>
                                    )
                                </i>
                            </td>
                            <td>
                                <?php
                                if ($waiting['Package']['name'] != 'Pay at Facility') {
                                    echo $this->Html->link($waiting['Package']['name'], array('controller' => 'packages', 'action' => 'view', $waiting['Package']['uuid']), array('class' => 'green'));
                                } else {
                                    echo '<label class="label label-info">' . $waiting['Package']['name'] . '</label>';
                                }

                                ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>

                    </tbody>
                </table>
            <?php else: ?>
                <?php echo $this->element('not-found') ?>
            <?php endif; ?>

            <div class="clearfix"></div>
        </div>
        <!----- /WAITING LIST ------>
        <!----- PACKAGES ------>
        <div class="d-box">
            <h4 class="d-title padding-bottom-10">
                <i class="fa fa-gift purple"></i> Purchased Items
                <?php
                if ($userInformation['PackagesUser']) {
                    echo $this->html->link('See Full List', array('controller' => 'packages', 'action' => 'list?sid=' . $userInformation['User']['uuid']), array('escape' => false, 'class' => 'full-list pull-right'));
                }
                ?>
            </h4>

            <?php if ($userInformation['PackagesUser']): ?>
                <table class="table margin-top-25">
                    <thead>
                    <th>Item</th>
                    <th>Price</th>
                    <th>Lesson</th>
                    <th>Action</th>
                    </thead>
                    <tbody>
                    <?php foreach ($userInformation['PackagesUser'] as $package): ?>
                        <tr>
                            <td>
                                <?php
                                echo $this->Html->link($package['Package']['name'], array('controller' => 'packages', 'action' => 'view', $package['Package']['uuid']), array('class' => 'green'));
                                ?>
                            </td>
                            <td>
                                <?php echo $this->Number->currency($package['Package']['total']); ?>
                            </td>
                            <td>
                                <?php echo $package['total_lesson'] ?>
                            </td>
                            <td>
                                <?php echo $this->Html->link('View', array('controller' => 'packages', 'action' => 'view', $package['Package']['uuid']), array('class' => 'd-btn')); ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>

                    </tbody>
                </table>
            <?php else: ?>
                <?php echo $this->element('not-found') ?>
            <?php endif; ?>

            <div class="clearfix"></div>
        </div>
        <!----- /PACKAGES ------>

    </div>
</div>

<div class="clearfix"></div>
<div class="margin-top-15">
    <hr class="shadow-line">
    <h2 class="short-title theme-color">Biography:</h2>
    <i class="italic-md theme-color">
        <?php
        if ($userInformation['Profile']['biography']) {
            echo $userInformation['Profile']['biography'];
        } else {
            echo 'N/A';
        }
        ?>
    </i>
</div>


<?php if ($notes): ?>
    <br/>
    <br/>
    <h2 class="page-title">Note</h2>
    <hr class="shadow-line"/>
    <div class="ui-217">
        <?php foreach ($notes as $note): ?>
            <div class="ui-item clearfix" id="<?php echo $note['Note']['id']; ?>">
                <!-- Quotes -->
                <span class="bg-lblue">&#8220;</span>
                <!-- Details -->
                <div class="ui-details">
                    <em>
                        <a name="<?php echo $note['Note']['id']; ?>"><?php echo $note['Note']['note']; ?></a>
                    </em>
                    <br/>
                    <select class="noteRatings">
                        <option value="" selected="selected"></option>
                        <?php
                        for ($counter = 1; $counter <= 5; $counter++) {
                            $select = '';
                            if ($counter == $note['Note']['rating']) {
                                $select = 'selected';
                            }
                            echo '<option ' . $select . ' value="' . $counter . '">' . $note['Note']['id'] . '</option>';
                        }
                        ?>
                    </select>

                    <p>
                        <?php
                        $today = (array)new DateTime("now");
                        $noteDate = $note['Note']['note_date'];
                        if ($note['Note']['status'] == 2) {
                            echo '<i class="label label-success">followed</i>';
                        }
                        if ($note['Note']['status'] == 3) {
                            echo '<i class="label label-warning">unfollowed</i>';
                        } elseif (strtotime($today['date']) < strtotime($noteDate) && $note['Note']['status'] == 1) {
                            echo '<i class="label label-info">upcoming</i>';
                        } elseif (strtotime($today['date']) > strtotime($noteDate) && $note['Note']['status'] == 1) {
                            echo '<i class="label label-danger">expired</i>';
                        }
                        ?>
                    </p>
                    <strong style="font-size: 10px;">
                        <em>
                            <?php echo $this->Time->format('d M, Y', $note['Note']['created']) ?>
                        </em>
                    </strong>
                    <i class="italic-sm">(<?php echo $this->Time->format('h:i A', $note['Note']['created']) ?>)</i>
                </div>
                <?php
                if ($userInfo['Profile']['profile_pic_url']) {
                    echo $this->Html->image('profiles/' . $userInfo['Profile']['profile_pic_url'], array('class' => 'img-circle pro-img-sm ui-img'));
                } else {
                    echo $this->Html->image('avatar.jpg', array('class' => 'img-circle pro-img-sm'));
                }
                ?>
                <h3>
                    <?php echo $this->Html->link($userInfo['Profile']['name'], array('controller' => 'profiles', 'action' => 'index')); ?>
                </h3>
            </div>

        <?php endforeach; ?>
    </div>
<?php else: ?>
    <?php echo $this->element('not-found') ?>
<?php endif; ?>


<script>
    $(document).ready(function () {
        var fullUrl = window.location.href.split('#');
        var noteID = fullUrl[1];
        $('#' + noteID).addClass('current-note');
    });


    $('.takeNoteRating').barrating('show', {
        theme: 'fontawesome-stars'
    });

    $('.singleUserRating').barrating('show', {
        initialRating: 0,
        theme: 'fontawesome-stars',
        onSelect: function (value, text) {

            $.ajax({
                url: '<?php echo Router::url('/', true);?>instructor/users/set_rating',
                type: "POST",
                dataType: "json",
                data: {userID: text, rating: value},
                success: function (response) {

                    console.log(response);

                    if (response == 1) {
                        manageFlashMessage('alert-success', 'Your rating has been successfully complete');
                    }
                    else {
                        manageFlashMessage('alert-danger', 'Sorry, something went wrong');
                    }
                },
                error: function (xhr, status, error) {
                    console.log(error);
                }
            });

        }
    });

    $('.noteRatings').barrating('show', {
        initialRating: 0,
        theme: 'fontawesome-stars',
        onSelect: function (value, text) {

            $.ajax({
                url: '<?php echo Router::url('/', true);?>instructor/contacts/setRating',
                type: "POST",
                dataType: "json",
                data: {noteID: text, rating: value},
                success: function (response) {

                    console.log(response);

                    if (response == 1) {
                        manageFlashMessage('alert-success', 'Your rating has been successfully complete');
                    }
                    else {
                        manageFlashMessage('alert-danger', 'Sorry, something went wrong');
                    }
                },
                error: function (xhr, status, error) {
                    console.log(error);
                }
            });

        }
    });

</script>