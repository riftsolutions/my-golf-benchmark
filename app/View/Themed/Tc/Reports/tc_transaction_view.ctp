<?php
/**
 * @var $this View
 */
?>
<h2 class="page-title">Transaction Details</h2>
<hr class="shadow-line"/>
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="box box-padding-normal">
            <div class="row">
                <div class="col-lg-6  col-md-6 col-sm-12">
                    <div class="margin-top-25">
                        <ul class="data-list">
                            <li>
                                <b>Student Name:</b>
                            </li>
                            <li class="green">
                                <?php
                                echo $this->Html->link($student['Profile']['name'], array('controller' => 'users', 'action' => 'details', $student['User']['uuid']));
                                ?>
                            </li>
                        </ul>
                        <ul class="data-list">
                            <li>
                                <strong>Payment To:</strong><br>
                            </li>
                            <li>
                                <strong class="u-p"><?php echo $userInfo['Profile']['name']?></strong>

                                <?php
                                if($userInfo['Profile']['phone']){
                                    echo '<br/>'.$userInfo['Profile']['phone'];
                                }
                                elseif($userInfo['Profile']['fax']){
                                    echo '<br/>'.$userInfo['Profile']['fax'];
                                }

                                if($userInfo['Profile']['street_1']){
                                    echo '<br/>'.$userInfo['Profile']['street_1'];
                                }
                                elseif($userInfo['Profile']['street_2']){
                                    echo '<br/>'.$userInfo['Profile']['street_2'];
                                }

                                echo '<br/>';

                                if($userInfo['Profile']['city']){
                                    echo $userInfo['Profile']['state'].', ';
                                }

                                if($userInfo['Profile']['state']){
                                    echo $userInfo['Profile']['state'].', ';
                                }

                                if($userInfo['Profile']['postal_code']){
                                    echo $userInfo['Profile']['postal_code'];
                                }

                                echo '<br/>';

                                if($userInfo['User']['username']){
                                    echo '<a class="green">'.$userInfo['User']['username'].'</a>';
                                }
                                ?>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="col-lg-6  col-md-6 col-sm-12">
                    <div class="margin-top-25">
                        <ul class="data-list"  style="text-align: right">
                            <li>
                                <b>Invoice ID:</b>
                            </li>
                            <li class="green">
                                #<?php echo sprintf('%011d', $transactionID);;?>
                            </li>
                        </ul>
                        <ul class="data-list" style="text-align: right">
                            <li>
                                <strong>Date of Invoice:</strong><br>
                                <span>
                                    <?php echo date('M d, Y, H:i A');?>
                                </span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <table class="table margin-top-25">
                        <thead>
                        <tr>
                            <th>Instructor</th>
                            <th>Title</th>
                            <th>No. of Lesson</th>
                            <th><span class="pull-right">Sub total</span></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach($items as $package):?>
                            <tr>
                                <td class="padding-left-0 padding-right-0">
                                    <?php echo $userInfo['Profile']['name']?>
                                </td>
                                <td class="padding-left-0 padding-right-0">
                                    <?php echo $this->Html->link($package['Package']['name'], array('controller' => 'package', 'action' => 'view', $package['Package']['uuid']))?>
                                </td>
                                <td>
                                    <?php echo $package['Package']['lesson'];?>
                                </td>
                                <td class="padding-left-0 padding-right-0">
                                    <span class="pull-right"><?php echo $this->Number->currency($package['Package']['price']);?></span>
                                </td>
                            </tr>
                        <?php endforeach;?>
                        </tbody>
                    </table>
                    <div class="subtoal">
                        <ul class="data-list">
                            <li>
                                <strong>Sub Total:</strong>
                                <?php echo $this->Number->currency($cost['subtotal']);?>
                            </li>
                            <li>
                                <strong>Tax:</strong>
                                <?php echo $this->Number->currency($cost['tax']);?>
                            </li>
                            <li>
                                <strong>Total</strong>
                                <?php echo $this->Number->currency($cost['total']);?>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
