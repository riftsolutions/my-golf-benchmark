<?php
/**
 * @var $this View
 */
?>
<h2 class="page-title"><?php echo $title; ?></h2>
<hr class="shadow-line"/>
<div class="clearfix"></div>

<?php if($users):?>
<div class="table-responsive">
    <table class="table data-table table-bordered sort-table" colspecing=0>
        <thead>
        <th>Name</th>
        <th>Email</th>
        <th>Phone</th>
        <th>City</th>
        <th>Role</th>
        <th>Details</th>
        </thead>
        <tbody>
        <?php foreach($users as $user):?>
            <tr>
                <td>
                    <?php
                    echo $this->Html->link($user['Profile']['name'], array('controller' => 'users', 'action' => 'details', $user['User']['uuid']));
                    ?>
                </td>
                <td>
                    <?php
                    echo $this->Html->link($user['User']['username'], array('controller' => 'users', 'action' => 'details', $user['User']['uuid']));
                    ?>
                </td>
                <td>
                    <?php
                    if($user['Profile']['phone'])
                    {
                        echo $user['Profile']['phone'];
                    }
                    else{
                        echo 'N/A';
                    }
                    ?>
                </td>
                <td>
                    <?php
                    if($user['Profile']['city'])
                    {
                        echo $user['Profile']['city'];
                    }
                    else{
                        echo 'N/A';
                    }
                    ?>
                </td>
                <td>
                    <?php
                    if($user['User']['role'] == 2){
                        $role = '<label class="text-success text-md">Student</label>';
                    }
                    elseif($user['User']['role'] == 3){
                        $role = '<label class="text-info text-md">Instructor</label>';
                    }
                    echo $role;
                    ?>
                </td>
                <td>
                    <?php
                    echo $this->Html->link('Details', array('controller' => 'users', 'action' => 'details', $user['User']['uuid']), array('class' => 'd-btn'));
                    ?>

                </td>
            </tr>
        <?php endforeach;?>
        </tbody>
    </table>
    <?php echo $this->element('pagination');?>
</div>
<?php else:?>
    <h4 class="not-found">Sorry, users not found</h4>
<?php endif;?>
