<?php
/**
 * @var $this View
 */
?>
<h2 class="page-title"><?php echo $title; ?></h2>
<hr class="shadow-line"/>
<div class="dropdown margin-bottom-5">

    <button class="btn btn-info dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown">
        Quick Navigation
        <span class="caret"></span>
    </button>
    <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
        <li role="presentation">
            <?php echo $this->Html->link('All Item (' . $posOverview[0][0]['totalPOS'] . ')',
                array('controller' => 'reports', 'action' => 'my_package')); ?>
        </li>
        <li role="presentation" class="divider"></li>
        <li role="presentation">
            <?php echo $this->Html->link('Unpaid Item (' . $posOverview[0][0]['unpaidPOS'] . ')',
                array('controller' => 'reports', 'action' => 'my_package', 'unpaid')); ?>
        </li>
        <li role="presentation" class="divider"></li>
        <li role="presentation">
            <?php echo $this->Html->link('Paid Item (' . $posOverview[0][0]['paidPOS'] . ')',
                array('controller' => 'reports', 'action' => 'my_package', 'paid')); ?>
        </li>
        <li role="presentation" class="divider"></li>
        <li role="presentation">
            <?php echo $this->Html->link('Running Item (' . $posOverview[0][0]['countRunningPOS'] . ')',
                array('controller' => 'reports', 'action' => 'my_package', 'running')); ?>
        </li>
        <li role="presentation" class="divider"></li>
        <li role="presentation">
            <?php echo $this->Html->link('Completed Item (' . $posOverview[0][0]['countCompletedPOS'] . ')',
                array('controller' => 'reports', 'action' => 'my_package', 'completed')); ?>
        </li>
        <li role="presentation" class="divider"></li>
        <li role="presentation">
            <?php echo $this->Html->link('Expired Item (' . $posOverview[0][0]['countExpiredPOS'] . ')',
                array('controller' => 'reports', 'action' => 'my_package', 'expired')); ?>
        </li>
    </ul>
    <button type="button" class="btn btn-theme btn-lg " data-toggle="modal"
            data-target="#searchItemModal">
        Search Item
    </button>

</div>

<div class="table-responsive">
    <?php if ($packages): ?>
        <table class="table data-table table-bordered sort-table" colspecing=0>
            <thead>
            <tr>
                <th><?php echo $this->Paginator->sort('Package.0.name', 'Name of Student'); ?></th>
                <th><?php echo $this->Paginator->sort('Package.name', 'Name of Item'); ?></th>
                <th><?php echo $this->Paginator->sort('Package.price', 'Price'); ?></th>
                <th><?php echo $this->Paginator->sort('Package.tax', 'Tax'); ?></th>
                <th><?php echo $this->Paginator->sort('Package.total', 'Total'); ?></th>
                <th><?php echo $this->Paginator->sort('PackagesUser.payment_status', 'payment status'); ?></th>
                <th><?php echo $this->Paginator->sort('PackagesUser.status', 'status'); ?></th>
                <th><?php echo $this->Paginator->sort('Package.lesson', 'Available Lesson'); ?></th>
                <th><?php echo $this->Paginator->sort('Package.count_benchmark', 'Total Benchmark'); ?></th>
                <th><?php echo $this->Paginator->sort('PackagesUser.expiration_date', 'Expire Date'); ?></th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($packages as $package): ?>
                <tr>
                    <td>
                        <?php echo $this->Html->link($package[0]['name'],
                            array('controller' => 'users', 'action' => 'details', $package['User']['uuid']),
                            array('class' => 'green')); ?>
                    </td>
                    <td>
                        <?php echo $this->Html->link($package['Package']['name'],
                            array('controller' => 'packages', 'action' => 'view', $package['Package']['uuid']),
                            array('class' => 'green')); ?>
                    </td>
                    <td>
                        <?php echo $this->Number->currency($package['Package']['price']); ?>
                    </td>
                    <td>
                        <?php echo $this->Number->currency($package['Package']['tax']); ?>
                        <i class="italic-md u-l">(Tax <?php echo $package['Package']['tax_percentage'] ?>%)</i>
                    </td>
                    <td>
                        <?php echo $this->Number->currency($package['Package']['total']); ?>
                    </td>
                    <td>
                        <?php echo $this->Utilities->managePackageStatus($package['PackagesUser']['payment_status']); ?>
                    </td>
                    <td>
                        <?php
                        if ($package['PackagesUser']['status'] == 1) {
                            echo '<label class="label label-success">running</label>';
                        } elseif ($package['PackagesUser']['status'] == 2) {
                            echo '<label class="label label-info">completed</label>';
                        } elseif ($package['PackagesUser']['status'] == 3) {
                            echo '<label class="label label-warning">expired</label>';
                        } else {
                            echo '<label class="label label-default">N/A</label>';
                        }
                        ?>
                    </td>
                    <td>
                        <?php echo $package['PackagesUser']['available']; ?>
                    </td>
                    <td>
                        <?php echo $package['PackagesUser']['count_benchmark']; ?>
                    </td>
                    <td>
                        <?php echo $this->Time->format('M d, Y', $package['PackagesUser']['expiration_date']); ?>
                        <i class="italic-sm">( <?php echo $this->Time->format('H:i A',
                                $package['PackagesUser']['expiration_date']); ?>)</i>
                    </td>
                    <td>
                        <?php echo $this->Html->link('Details', array(
                            'controller' => 'reports',
                            'action' => 'my_package_view',
                            $package['PackagesUser']['id']
                        ), array('class' => 'btn d-btn')); ?>

                        <?php /*echo $this->Html->link('Reassign', array('controller' => 'packages', 'action' => 'assign', $package['Package']['uuid'], $package['User']['id']), array('class' => 'btn d-btn'));*/ ?>

                        <?php
                        if ($package['PackagesUser']['count_benchmark'] > 0) {
                            echo $this->Html->link('View all Benchmark',
                                array('controller' => 'benchmarks', 'action' => 'list', $package['Package']['uuid']),
                                array('class' => 'btn d-btn'));
                        }
                        ?>

                    </td>

                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
        <?php echo $this->element('pagination'); ?>
    <?php else: ?>
        <h2 class="not-found">Sorry, item not found</h2>
        <hr class="shadow-line"/>
    <?php endif; ?>
</div>
<!-- Modal For Search Item -->
<div class="modal event-modal fade" id="searchItemModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="border-bottom: 1px solid #e5e5e5;">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                </button>
                <h4 class="sm-title">Search My Items</h4>
            </div>
            <div class="modal-body">
                <!----- ADD NEW CONTACT NOTE ------>
                <div class="">
                    <form action="<?php echo Router::url('/', true);?>instructor/reports/my_package" class="form-horizontal form-custom" method="get" accept-charset="utf-8">
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label>Student name:</label>
                                <input name="student" class="typeahead form-control" type="text" placeholder="Type student name">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label>Item Name:</label>
                                <input name="item_name" class="form-control" type="text" placeholder="Type an item name">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label>Expire Date</label>
                                <select name="date" class="form-control">
                                    <option>Choose Date</option>
                                    <option value="current_month">Current Month</option>
                                    <option value="next_3_month">Next Three Month</option>
                                    <option value="next_6_month">Next Six Month</option>
                                    <option value="next_12_month">Next Year</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-12"><label>Date Expire Range</label></div>
                            <div class="clearfix"></div>
                            <div class="col-sm-6">
                                <div class="input-group">
                                    <input id="datepicker" name="starting" type="text" class="form-control" placeholder="Starting Date">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="input-group">
                                    <input id="datepicker2" name="ending" type="text" class="form-control" placeholder="Ending Date">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label>Amount</label>
                                <select name="amount" class="form-control">
                                    <option>Choose Amount</option>
                                    <option value="100">$100+</option>
                                    <option value="500">$500+</option>
                                    <option value="1000">$1000+</option>
                                    <option value="2000">$2000+</option>
                                    <option value="5000">$5000+</option>
                                    <option value="10000">$10000+</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-12"><label>Amount Range</label></div>
                            <div class="col-lg-6">
                                <input name="starting_amount" class="form-control" type="text" placeholder="Starting Amount">
                            </div>
                            <div class="col-lg-6">
                                <input name="ending_amount" class="form-control" type="text" placeholder="Ending Amount">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-12"><label>Payment Status</label></div>

                            <div class="col-lg-12">
                                <label class="radio-inline">
                                    <input type="radio" name="payment_status" id="inlineRadio1" value="" checked="checked"> All
                                </label>

                                <label class="radio-inline">
                                    <input type="radio" name="payment_status" id="inlineRadio2" value="1"> Paid
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" name="payment_status" id="inlineRadio3" value="0"> Unpaid
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" name="payment_status" id="inlineRadio3" value="0"> Partial Paid
                                </label>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-4">
                                <button class="btn btn-theme">Search</button>
                            </div>
                        </div>
                        <?php echo $this->Form->end(); ?>
                        <div class="clearfix"></div>
                </div>
                <!----- /ADD NEW CONTACT NOTE ------>
            </div>
        </div>
    </div>
</div>
<!-- /Modal For Displaying Schedule -->

<div class="clearfix"></div>

<?php echo $this->Html->script('typehead');?>
<script>
    // Instantiate the Bloodhound suggestion engine
    var contacts = new Bloodhound({
        datumTokenizer: function (datum) {
            return Bloodhound.tokenizers.whitespace(datum.value);
        },
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: {
            url: '<?php echo Router::url('/', true);?>instructor/contacts/getContacts/student/%QUERY',
            filter: function (contacts) {
                return $.map(contacts, function (contact) {
                    console.log(contact);
                    return {
                        value: contact['0']['contactName']
                    };
                });
            }
        }
    });

    contacts.initialize();
    // Instantiate the Typeahead UI
    $('.typeahead').typeahead(null, {
        name: 'states',
        displayKey: 'value',
        source: contacts.ttAdapter()
    });
</script>