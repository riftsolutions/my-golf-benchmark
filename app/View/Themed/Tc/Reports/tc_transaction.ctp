<?php
/**
 * @var $this View
 */
?>
<h2 class="page-title"><?php echo $title;?></h2>
<hr class="shadow-line"/>
<div class="page-option">
    <div class="page-option-left">
        <?php
        echo $this->Html->link('Export', array('controller' => 'reports', 'action' => 'export_transaction?conditions='.serialize($conditions)), array('class' => 'btn btn-theme btn-lg pull-right margin-left-5'));
        ?>
        <button type="button" class="btn btn-theme btn-lg pull-right" data-toggle="modal" data-target="#searchTransactionModal">
            Search Transaction
        </button>
    </div>
</div>
<br/>
<br/>
<div class="table-responsive">
    <?php if ($transactions): ?>
        <table class="table data-table table-bordered sort-table" colspecing=0>
            <thead>
            <tr>
                <th><?php echo $this->Paginator->sort('Billing.0.studentName', 'Student Name'); ?></th>
                <th><?php echo $this->Paginator->sort('0.instructorName', 'Instructor Name'); ?></th>
                <th><?php echo $this->Paginator->sort('Billing.payment_transaction_id', 'Transaction ID'); ?></th>
                <th><?php echo $this->Paginator->sort('Billing.amount', 'Transaction Amount'); ?></th>
                <th><?php echo $this->Paginator->sort('Billing.payment_transaction_type', 'Transaction Type'); ?></th>
                <th><?php echo $this->Paginator->sort('Billing.payment_account_number', 'Card'); ?></th>
                <th><?php echo $this->Paginator->sort('Billing.created', 'Date'); ?></th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($transactions as $transaction):?>
                <tr>
                    <td>
                        <?php echo $this->Html->link($transaction[0]['studentName'], array('controller' => 'users', 'action' => 'details', $transaction['User']['uuid']), array('class' => 'green'));?>
                    </td>
                    <td>
                        <?php echo $transaction[0]['instructorName'];?>
                    </td>

                    <td>
                        <?php echo $transaction['Billing']['payment_transaction_id'];?>
                    </td>
                    <td>
                        <?php echo $this->Number->currency($transaction['Billing']['amount']);?>
                    </td>
                    <td>
                        <?php
                        if($transaction['Billing']['payment_transaction_type'] == 'auth_capture'){
                            echo '<label class="label label-success">purchased items</label>';
                        }
                        elseif($transaction['Billing']['payment_transaction_type'] == 'refund'){
                            echo '<label class="label label-danger">refunded</label>';
                        }
                        else{
                            echo '<label class="label label-warning">N/A</label>';
                        }
                        ?>
                    </td>
                    <td>
                        <?php echo $transaction['Billing']['payment_account_number'];?>
                    </td>
                    <td>
                        <?php echo $this->Time->format('d M, Y', $transaction['Billing']['created'])?>
                        <i class="italic-sm">(
                            <?php echo $this->Time->format('h:i A', $transaction['Billing']['created'])?>
                            )
                        </i>
                    </td>
                    <td>
                        <?php echo $this->Html->link('Details', array('controller' => 'reports', 'action' => 'transaction_view', $transaction['Billing']['uuid']), array('class' => 'd-btn'));?>
                    </td>

                </tr>
            <?php endforeach;?>
            </tbody>
        </table>
        <?php echo $this->element('pagination');?>
    <?php else: ?>
        <h2 class="not-found">Sorry, transaction not found</h2>
        <hr class="shadow-line"/>
    <?php endif; ?>
</div>

<!-- Modal For Displaying Schedule -->
<div class="modal event-modal fade" id="searchTransactionModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="border-bottom: 1px solid #e5e5e5;">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                </button>
                <h4 class="sm-title">Search Transactions</h4>
            </div>
            <div class="modal-body">
                <!----- ADD NEW CONTACT NOTE ------>
                <div class="">
                    <form action="<?php echo Router::url('/', true);?>tc/reports/transaction" class="form-horizontal form-custom" method="get" accept-charset="utf-8">
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label>Instructor name:</label>
                                <select class="form-control form-inline" name="instructor[]" multiple>
                                    <option value="" selected="selected">Select Instructor</option>
                                    <?php foreach ($instructors as $instructor => $value): ?>
                                        <option  value="<?php echo $instructor; ?>">
                                            <?php echo $value; ?>
                                        </option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>

                    <div class="form-group">
                        <div class="col-sm-12">
                            <label>Student name:</label>
                            <input name="student" class="typeahead form-control" type="text" placeholder="Type student name">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label>Date</label>
                            <select name="date" class="form-control">
                                <option>Choose Date</option>
                                <option value="current_month">Current Month</option>
                                <option value="last_month">Last Month</option>
                                <option value="last_3_month">Last Three Month</option>
                                <option value="last_6_month">Last Six Month</option>
                                <option value="last_12_month">Last Year</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-12"><label>Date Range</label></div>
                        <div class="clearfix"></div>
                        <div class="col-sm-6">
                            <div class="input-group">
                                <input id="datepicker" name="starting" type="text" class="form-control" placeholder="Starting Date">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="input-group">
                                <input id="datepicker2" name="ending" type="text" class="form-control" placeholder="Ending Date">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label>Amount</label>
                            <select name="amount" class="form-control">
                                <option>Choose Amount</option>
                                <option value="100">$100+</option>
                                <option value="500">$500+</option>
                                <option value="1000">$1000+</option>
                                <option value="2000">$2000+</option>
                                <option value="5000">$5000+</option>
                                <option value="10000">$10000+</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-12"><label>Amount Range</label></div>
                        <div class="col-lg-6">
                            <input name="starting_amount" class="form-control" type="text" placeholder="Starting Amount">
                        </div>
                        <div class="col-lg-6">
                            <input name="ending_amount" class="form-control" type="text" placeholder="Ending Amount">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-4">
                            <button class="btn btn-theme">Search</button>
                        </div>
                    </div>
                    <?php echo $this->Form->end(); ?>
                    <div class="clearfix"></div>
                </div>
                <!----- /ADD NEW CONTACT NOTE ------>
            </div>
        </div>
    </div>
</div>
<!-- /Modal For Displaying Schedule -->

<?php echo $this->Html->script('typehead');?>
<script>
    // Instantiate the Bloodhound suggestion engine
    var contacts = new Bloodhound({
        datumTokenizer: function (datum) {
            return Bloodhound.tokenizers.whitespace(datum.value);
        },
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: {
            url: '<?php echo Router::url('/', true);?>instructor/contacts/getContacts/student/%QUERY',
            filter: function (contacts) {
                return $.map(contacts, function (contact) {
                    console.log(contact);
                    return {
                        value: contact['0']['contactName']
                    };
                });
            }
        }
    });

    contacts.initialize();
    // Instantiate the Typeahead UI
    $('.typeahead').typeahead(null, {
        name: 'states',
        displayKey: 'value',
        source: contacts.ttAdapter()
    });
</script>