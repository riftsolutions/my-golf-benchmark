<?php
/**
 * @var $this View
 */
?>
<h2 class="page-title">Lead Report</h2>
<hr class="shadow-line"/>
<div class="clearfix"></div>
<div class="page-option">
    <div class="page-option-left">
        <button type="button" class="btn btn-theme btn-lg pull-right" data-toggle="modal" data-target="#searchContactModal">
            Search Lead
        </button>
    </div>
</div>
<br/>
<br/>

<?php if ($contacts): ?>
    <div class="table-responsive margin-top-10">
        <table class="table data-table table-bordered sort-table" colspecing=0>
            <thead>
            <tr>
                <th style="width: 15%;">
                    <?php echo $this->Paginator->sort('name', 'Lead Name'); ?>
                </th>
                <th style="width: 15%;">
                    <?php echo $this->Paginator->sort('contactCreated', 'Lead Creator'); ?>
                </th>
                <th style="width: 15%;">
                    <?php echo $this->Paginator->sort('email'); ?>
                </th>
                <th style="width: 13%;">
                    <?php echo $this->Paginator->sort('phone'); ?>
                </th>
                <th style="width: 13%;">
                    <?php echo $this->Paginator->sort('created', 'Created'); ?>
                </th>
                <th style="width: 9%;">Action</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($contacts as $contact): ?>
                <tr>
                    <td>
                        <?php echo $this->Html->link($contact['Contact']['name'], array('controller' => 'contacts', 'action' => 'edit', $contact['Contact']['id'])); ?>
                    </td>
                    <td>
                        <?php echo $this->Html->link($contact['User']['Profile']['first_name'] . ' ' . $contact['User']['Profile']['last_name'],
                            array('controller' => 'contacts', 'action' => 'edit', $contact['Contact']['id'])); ?>
                    </td>
                    <td><?php echo $contact['Contact']['email']; ?></td>
                    <td><?php echo $contact['Contact']['phone'] ?></td>
                    <td><?php echo $this->Time->format('M d, Y', $contact['Contact']['created']) ?></td>
                    <td>
                        <div class="icons-area">
                            <?php echo $this->Html->link('Details', array('controller' => 'contacts', 'action' => 'view', $contact['Contact']['id']), array('escape' => false, 'class' => 'd-btn')); ?>
                        </div>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
        <?php echo $this->element('pagination'); ?>
    </div>
<?php else: ?>
    <h4 class="not-found">Sorry, lead not found</h4>
<?php endif; ?>

<!-- Modal For Displaying Schedule -->
<div class="modal event-modal fade" id="searchContactModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="border-bottom: 1px solid #e5e5e5;">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                </button>
                <h4 class="sm-title">Search Lead</h4>
            </div>
            <div class="modal-body">
                <!----- ADD NEW CONTACT NOTE ------>
                <div class="">
                    <form action="<?php echo Router::url('/', true); ?>tc/reports/lead" class="form-horizontal form-custom" method="get" accept-charset="utf-8">

                        <div class="form-group">
                            <div class="col-sm-12">
                                <label>Instructor name:</label>
                                <select class="form-control form-inline" name="instructor[]" multiple>
                                    <option value="" selected="selected">Select Instructor</option>
                                    <?php foreach ($instructors as $instructor => $value): ?>
                                        <option  value="<?php echo $instructor; ?>">
                                            <?php echo $value; ?>
                                        </option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-sm-12">
                                <label>Lead name:</label>
                                <input name="contact" class="typeahead form-control" type="text" placeholder="Type Lead name">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12">
                                <label>Lead Email:</label>
                                <input name="email" class="form-control" type="text" placeholder="Type Lead Email">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12">
                                <label>Lead Phone Number:</label>
                                <input name="phone" class="form-control" type="text" placeholder="Type Lead Phone number">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12">
                                <label>Date</label>
                                <select name="date" class="form-control">
                                    <option>Choose Date</option>
                                    <option value="current_month">Current Month</option>
                                    <option value="last_month">Last Month</option>
                                    <option value="last_3_month">Last Three Month</option>
                                    <option value="last_6_month">Last Six Month</option>
                                    <option value="last_12_month">Last Year</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-lg-12"><label>Date Range</label></div>
                            <div class="clearfix"></div>
                            <div class="col-sm-6">
                                <div class="input-group">
                                    <input name="fromDate" type="text" class="form-control datepicker" placeholder="Starting Date">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="input-group">
                                    <input name="toDate" type="text" class="form-control datepicker" placeholder="Ending Date">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-4">
                                <button class="btn btn-theme">Search</button>
                            </div>
                        </div>
                        <?php echo $this->Form->end(); ?>
                        <div class="clearfix"></div>
                </div>
                <!----- /ADD NEW CONTACT NOTE ------>
            </div>
        </div>
    </div>
</div>
<!-- /Modal For Displaying Schedule -->
<?php echo $this->Html->script('typehead'); ?>
<script>
    // Instantiate the Bloodhound suggestion engine
    var contacts = new Bloodhound({
        datumTokenizer: function (datum) {
            return Bloodhound.tokenizers.whitespace(datum.value);
        },
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: {
            url: '<?php echo Router::url('/', true);?>tc/contacts/getContacts/contact/%QUERY',
            filter: function (contacts) {
                return $.map(contacts, function (contact) {
                    console.log(contact);
                    return {
                        value: contact['0']['contactName']
                    };
                });
            }
        }
    });

    contacts.initialize();
    // Instantiate the Typeahead UI
    $('.typeahead').typeahead(null, {
        name: 'states',
        displayKey: 'value',
        source: contacts.ttAdapter()
    });
</script>
