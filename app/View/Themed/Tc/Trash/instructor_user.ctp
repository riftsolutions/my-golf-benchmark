<?php
/**
 * @var $this View
 */
?>
<h2 class="page-title">
    <?php if(isset($title)){
        echo $title;
    }
    else{
        echo "Student's Trash";
    }
    ?>
</h2>
<hr class="shadow-line"/>

<div class="table-responsive">
    <?php if($userList):?>
        <table class="table data-table table-bordered sort-table" colspecing = 0>
        <thead>
        <tr>
            <th><?php echo $this->Paginator->sort('Profile.name', 'Name'); ?></th>
            <th><?php echo $this->Paginator->sort('User.username', 'Email/Username'); ?></th>
            <th><?php echo $this->Paginator->sort('Profile.phone', 'Phone'); ?></th>
            <th><?php echo $this->Paginator->sort('User.status', 'Status'); ?></th>
            <th><?php echo $this->Paginator->sort('User.created', 'Notes'); ?></th>
            <th><?php echo $this->Paginator->sort('User.last_login', 'Last Login'); ?></th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach($userList as $users):?>
        <tr>
            <td>
                <?php echo $this->Html->link($users['Profile']['name'], array('controller' => 'users', 'action' => 'details', $users['User']['uuid']), array('class' => 'green'))?>
            </td>
            <td>
                <?php echo $this->Html->link($users['User']['username'], array('controller' => 'users', 'action' => 'details', $users['User']['uuid']));?>
            </td>
            <td>
                <?php if($users['Profile']['phone']):?>
                    <?php echo $this->Html->link($users['Profile']['phone'], array('controller' => 'users', 'action' => 'details', $users['User']['uuid']));?>
                <?php else:?>
                    N/A
                <?php endif;?>
            </td>
            <td>
                <?php echo $this->Utilities->getUserStatus($users['User']['status']);?>
            </td>
            <td>
                <?php echo $this->Html->link('see note', array('controller' => 'users', 'action' => 'details/' . $users['User']['uuid'] . '#note'), array('class' => 'd-btn'))?>
            </td>
            <td>
                <time>
                    <?php
                    if(!strstr('0000-00-00 00:00:00', $users['User']['last_login']) && $users['User']['last_login']){
                        echo $this->Time->timeAgoInWords($users['User']['last_login'],
                            array('accuracy' => array('month' => 'month'),'end' => '1 year'));
                    }
                    else{
                        echo '<strong class="danger">Never logged in</strong>';
                    }
                    ?>
                </time>
            </td>
            <td>
                <?php
                echo $this->Form->postLink(
                    'Restore <i class="fa fa-mail-reply"></i>',
                    array(
                        'controller' => 'users',
                        'action' => 'changeStatus',
                        $users['User']['uuid'],
                        1
                    ),
                    array(
                        'class' => 'icon green',
                        'escape' => false
                    ),
                    __('Are you sure you want to restore this user')
                );
                ?>
            </td>
        </tr>
        <?php endforeach;?>
        </tbody>
    </table>
        <?php echo $this->element('pagination');?>
    <?php else:?>
        <h2 class="not-found">Sorry, student's trash list empty</h2>
        <hr class="shadow-line"/>
    <?php endif;?>
</div>
