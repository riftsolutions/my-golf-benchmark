<?php
/**
 * @var $this View
 */
?>
<h2 class="page-title">Edit Lesson</h2>
<hr class="shadow-line"/>
<div class="row">
    <div class="col-md-8 col-md-offset-2 col-sm-12">
        <div class="box box-padding-adding radius-5 margin-top-25 margin-bottom-25">
            <?php echo $this->Form->create('Lesson', array('controller' => 'lessons', 'action' => 'edit/'.$lessonDetails['Lesson']['uuid'], 'type' => 'file', 'method' => 'post', 'class' => 'form-horizontal form-custom'));?>
            <div class="form-group">
                <label class="col-sm-4 control-label">Lesson For</label>
                <div class="col-sm-8">
                    <?php echo $this->Form->input('Lesson.id', array('type' => 'hidden', 'value' => $lessonDetails['Lesson']['id']));?>
                    <?php
                    echo $this->Form->input(
                        'Lesson.created_for',
                        array(
                            'type' => 'select',
                            'class' => 'form-control',
                            'label' => false,
                            'div' => false,
                            'required' => false,
                            'options' => $studentList,
                            'value' => $lessonDetails['Lesson']['created_for']
                        )
                    )
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4 control-label">Title</label>
                <div class="col-sm-8">
                    <?php
                    echo $this->Form->input(
                        'Lesson.title',
                        array(
                            'type' => 'text',
                            'class' => 'form-control',
                            'label' => false,
                            'div' => false,
                            'required' => false,
                            'value' => $lessonDetails['Lesson']['title']
                        )
                    )
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4 control-label">Description</label>
                <div class="col-sm-8">
                    <?php
                    echo $this->Form->input(
                        'Lesson.description',
                        array(
                            'type' => 'textarea',
                            'class' => 'form-control',
                            'label' => false,
                            'div' => false,
                            'required' => false,
                            'rows' => 6,
                            'value' => $lessonDetails['Lesson']['description']
                        )
                    )
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4 control-label">Lesson Price</label>
                <div class="col-sm-6">
                    <div class="input-group">
                        <span class="input-group-addon">$</span>
                        <?php
                        echo $this->Form->input(
                            'Lesson.price',
                            array(
                                'type' => 'text',
                                'class' => 'form-control',
                                'label' => false,
                                'div' => false,
                                'required' => false,
                                'value' => $lessonDetails['Lesson']['price'],
                                'error' => false
                            )
                        )
                        ?>
                        <span class="input-group-addon">.00</i></span>
                    </div>
                    <?php echo $this->Form->error('Lesson.price');?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4 control-label">Starting From</label>
                <div class="col-sm-5">
                    <?php
                    echo $this->Form->input(
                        'Lesson.starting_time',
                        array(
                            'type' => 'select',
                            'class' => 'form-control',
                            'label' => false,
                            'div' => false,
                            'required' => false,
                            'value' => $lessonDetails['Lesson']['starting_time'],
                            'options' => $this->Utilities->getHours()
                        )
                    )
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4 control-label">Duration</label>
                <div class="col-sm-4">
                    <?php
                    echo $this->Form->input(
                        'Lesson.hours',
                        array(
                            'type' => 'text',
                            'class' => 'form-control',
                            'label' => false,
                            'div' => false,
                            'required' => false,
                            'value' => $lessonDetails['Lesson']['hours']
                        )
                    )
                    ?>
                </div>
                <div class="col-sm-4">
                    <?php
                    echo $this->Form->input(
                        'Lesson.mins',
                        array(
                            'type' => 'text',
                            'class' => 'form-control',
                            'label' => false,
                            'div' => false,
                            'required' => false,
                            'value' => $lessonDetails['Lesson']['mins']
                        )
                    )
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4 control-label">Data and Time</label>
                <div class="col-sm-6">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                        <input id="datepicker" name="data[Lesson][schedule_date]" type="text" class="form-control" value="<?php echo $lessonDetails['Lesson']['schedule_date'];?>">
                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                    </div>
                    <?php echo $this->Form->error('Lesson.schedule_date');?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4 control-label">Visibility</label>
                <div class="col-sm-6">
                    <div class="checkbox">
                        <label>
                        <?php if($lessonDetails['Lesson']['visible'] == 2):?>
                            <input name="data[Lesson][visible]" type="checkbox" value="1"> Visible to student? (Currently invisible to student)
                        <?php elseif($lessonDetails['Lesson']['visible'] == 1):?>
                            <input name="data[Lesson][visible]" type="checkbox" value="2"> Invisible to student? (Currently visible to student)
                        <?php endif;?>
                        </label>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4 control-label">Add Attachment</label>
                <div class="col-sm-8">
                    <table class="addAttachment" style="width: 100%;">
                        <tbody>
                        <tr>
                            <td colspan="2" style="height: 55px;">
                                <input class="form-control" name="data[Attachment][][attachment]" id="phone_number" type="file">
                            </td>
                        </tr>
                        </tbody>
                        <tfoot>
                        <tr>
                            <td>

                            </td>
                        </tr>
                        </tfoot>
                    </table>

                    <button type="button" class="pull-left btn btn-xs btn-default" id="addAttachment">
                        <i class="fa fa-paperclip"></i> Add More Attachment <i class="fa fa-plus"></i>
                    </button>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12">
                    <button class="btn btn-theme pull-right">Update Lesson</button>
                </div>
            </div>
            <?php echo $this->Form->end();?>
        </div>
    </div>
</div>