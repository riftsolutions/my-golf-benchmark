<?php
/**
 * @var $this View
 */
?>
<h2 class="page-title">Lesson Details</h2>
<hr class="shadow-line"/>
<div class="row">
    <div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-12">
        <?php $this->Utilities->lessonStatusButton($lessonDetails['Lesson']['status']); ?>
        <div class="clearfix"></div>
        <div class="box box-padding-normal radius-5 margin-top-25">
            <?php echo $this->Html->link('<i class="fa fa-pencil"></i> Edit Lesson', array('controller' => 'lessons', 'action' => 'edit', $lessonDetails['Lesson']['uuid']), array('escape' => false, 'class' => 'fly-edit-button'));?>
            <h2 class="box-title">Lesson Details</h2>
            <ul class="data-list">
                <li>
                    <strong>Lesson Plan: </strong>
                    <?php echo $this->Html->link($lessonDetails['Lesson']['title'], array('controller' => 'lessons', 'action' => 'view', $lessonDetails['Lesson']['uuid']));?>
                </li>
                <li>
                    <strong>Description: </strong>
                    <p>
                        <?php echo $lessonDetails['Lesson']['description'];?>
                    </p>

                </li>
                <li>
                    <strong>Date: </strong>
                    <time><?php echo $this->Time->format('M d, Y', $lessonDetails['Lesson']['schedule_date']);?></time>
                </li>
                <li>
                    <strong>Time: </strong>
                    <time><?php echo $lessonDetails['Lesson']['starting_time'];?></time>
                </li>
                <li>
                    <strong>Duration: </strong><span><?php echo $lessonDetails['Lesson']['duration']?> Hours</span>
                </li>
                <li>
                    <strong>Status</strong>
                    <?php if ($lessonDetails['Lesson']['status'] == 1): ?>
                        <label class="label label-info">active</label>
                    <?php elseif($lessonDetails['Lesson']['status'] == 2): ?>
                        <label class="label label-info">Completed</label>
                    <?php else: ?>
                        N/A
                    <?php endif; ?>
                </li>
                <?php if($lessonDetails['Attachment']): ?>
                    <li>
                        <div class="download-files">
                            <h2 class="sm-title u-p">Download Attachment</h2>
                            <?php foreach ($lessonDetails['Attachment'] as $attachment): ?>
                                <a href="<?php echo Router::url('/', true) . '/files/attachment/attachment/' . $attachment['attachment_dir'] . '/' . $attachment['attachment']; ?>"
                                   download="<?php echo $attachment['attachment'];?>" title="<?php echo $attachment['attachment'];?>">
                                    <i class="fa fa-paperclip"></i>
                                    (<?php echo $attachment['attachment']; ?>)
                                </a>
                                <br/>
                            <?php endforeach; ?>
                        </div>
                    </li>
                <?php endif; ?>
            </ul>
            <div class="clearfix"></div>
            <div class="margin-top-10 the">
                <?php
                if ($lessonDetails['Lesson']['status'] == 3) {
                    echo $this->Form->postLink(
                        'Make it Complete',
                        array(
                            'controller' => 'lessons',
                            'action' => 'change_status',
                            $lessonDetails['Lesson']['uuid'],
                            6
                        ),
                        array(
                            'class' => 'btn btn-theme',
                            'escape' => false
                        ),
                        __('Are you sure you want to complete this lesson?')
                    );
                }
                elseif ($lessonDetails['Lesson']['status'] == 6 && $lessonDetails['Lesson']['is_benchmark'] == 0) {
                    echo $this->Html->link(
                        'Want to Benchmark It?',
                        array('controller' => 'lessons', 'action' => 'benchmark', $lessonDetails['Lesson']['uuid']),
                        array('class' => 'btn btn-theme')
                    );
                }
                elseif ($lessonDetails['Lesson']['status'] == 6 && $lessonDetails['Lesson']['is_benchmark'] == 1) {
                    echo $this->Html->link(
                        'View Benchmark',
                        array('controller' => 'benchmarks', 'action' => 'view', $lessonDetails['Benchmark']['uuid']),
                        array('class' => 'btn btn-theme')
                    );
                }
                ?>
            </div>
        </div>
    </div>
</div>