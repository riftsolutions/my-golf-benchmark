<?php
/**
 * @var $this View
 */
?>
<h2 class="page-title">Benchmark</h2>
<hr class="shadow-line"/>
<?php echo $this->Form->create('Lesson', array('controller' => 'lessons', 'action' => 'benchmark/'.$lessonDetails['Lesson']['uuid']));?>

<input type="hidden" name="created_by" value="<?php echo $userID;?>">
<input type="hidden" name="lesson_id" value="<?php echo $lessonDetails['Lesson']['id'];?>">



<!---- Ball Position Widget ----->
<div class="widget black-widget">
    <div class="widget-header u-p">
        <i class="fa fa-bar-chart-o"></i> Past Areas / Future Areas
    </div>
    <div class="widget-body padding-15">
        <h2 class="heading-sm u-p">Athletic Posture</h2>
        <hr class="shadow-line">
        <div class="container-fluid">
            <div class="row">

                <!--Knee Posture BOX -->
                <div class="col-md-3 col-sm-6 scoring-box">
                    <h4 class="scoring-title u-p">Knee Posture</h4>
                    <div class="scoring-body">
                        <div class=benchmarking>
                            <input class="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                            <input name="posture_knee" type="text" class="textbox scoringValue" />
                            <input name="knee_posture_img" type="hidden" class="benchmark_img" />
                            <div id="scoringImages" class="scoring-single-img">
                                <div class="final-img-area">
                                    <?php echo $this->Html->image('golf/posture/knee/no_flex.jpg', array('class' => 'final-img'));?>
                                </div>
                                <div class="img-bundle">
                                    <?php echo $this->Html->image('golf/posture/knee/too_flex.jpg', array('class' => 'img-1'));?>
                                    <?php echo $this->Html->image('golf/posture/knee/no_flex.jpg', array('class' => 'img-2'));?>
                                    <?php echo $this->Html->image('golf/posture/knee/proper.jpg', array('class' => 'img-3'));?>
                                </div>
                            </div>
                        </div>
                        <textarea name="posture_knee_note" class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
                    </div>
                </div>
                <!--/Knee Posture BOX -->

                <!--Back Posture BOX -->
                <div class="col-md-3 col-sm-6 scoring-box">
                    <h4 class="scoring-title u-p">Back Posture</h4>
                    <div class="scoring-body">
                        <div class=benchmarking>
                            <input class="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                            <input name="posture_back" type="text" class="textbox scoringValue" />
                            <input name="knee_posture_img" type="hidden" class="benchmark_img" />
                            <div id="scoringImages" class="scoring-single-img">
                                <div class="final-img-area">
                                    <?php echo $this->Html->image('golf/posture/back/proper.jpg', array('class' => 'final-img'));?>
                                </div>
                                <div class="img-bundle">
                                    <?php echo $this->Html->image('golf/posture/back/too_lower_down.jpg', array('class' => 'img-1'));?>
                                    <?php echo $this->Html->image('golf/posture/back/too_tall_top.jpg', array('class' => 'img-2'));?>
                                    <?php echo $this->Html->image('golf/posture/back/to_face_down.jpg', array('class' => 'img-3'));?>
                                    <?php echo $this->Html->image('golf/posture/back/too_face_up.jpg', array('class' => 'img-4'));?>
                                    <?php echo $this->Html->image('golf/posture/back/proper.jpg', array('class' => 'img-5'));?>
                                </div>
                            </div>
                        </div>
                        <textarea name="posture_back_note" class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
                    </div>
                </div>
                <!--/Back Posture BOX -->

            </div>
        </div>
        <div class="margin-top-25"></div>
        <h2 class="heading-sm u-p">Stance Width:</h2>
        <hr class="shadow-line">
        <div class="container-fluid">
            <div class="row">

                <!--Foot Width BOX -->
                <div class="col-md-3 col-sm-6 scoring-box">
                    <h4 class="scoring-title u-p">Foot Width</h4>
                    <div class="scoring-body">
                        <div class=benchmarking>
                            <input class="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                            <input name="stance_foot_width" type="text" class="textbox scoringValue" />
                            <div id="scoringImages" class="scoring-single-img">
                                <div class="final-img-area">
                                    <?php echo $this->Html->image('golf/stance/width/proper.jpg', array('class' => 'final-img'));?>
                                </div>
                                <div class="img-bundle">

                                    <?php echo $this->Html->image('golf/stance/width/too_tall_left.jpg', array('class' => 'img-1'));?>
                                    <?php echo $this->Html->image('golf/stance/width/too_tall_right.jpg', array('class' => 'img-2'));?>
                                    <?php echo $this->Html->image('golf/stance/width/too_narrow_stance.jpg', array('class' => 'img-3'));?>
                                    <?php echo $this->Html->image('golf/stance/width/too_width_stance.jpg', array('class' => 'img-4'));?>
                                    <?php echo $this->Html->image('golf/stance/width/proper.jpg', array('class' => 'img-5'));?>
                                </div>
                            </div>
                        </div>
                        <textarea name="stance_foot_width_note" class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
                    </div>
                </div>
                <!--/Foot Width BOX -->

                <!--Foot Direction  BOX -->
                <div class="col-md-3 col-sm-6 scoring-box">
                    <h4 class="scoring-title u-p">Foot Direction</h4>
                    <div class="scoring-body">
                        <div class=benchmarking>
                            <input class="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                            <input name="stance_foot_direction" type="text" class="textbox scoringValue" />
                            <div id="scoringImages" class="scoring-single-img">
                                <div class="final-img-area">
                                    <?php echo $this->Html->image('golf/stance/direction/proper.jpg', array('class' => 'final-img'));?>
                                </div>
                                <div class="img-bundle">
                                    <?php echo $this->Html->image('golf/stance/direction/left_too_close.jpg', array('class' => 'img-1'));?>
                                    <?php echo $this->Html->image('golf/stance/direction/left_too_open.jpg', array('class' => 'img-2'));?>
                                    <?php echo $this->Html->image('golf/stance/direction/right_too_close.jpg', array('class' => 'img-3'));?>
                                    <?php echo $this->Html->image('golf/stance/direction/right_too_open.jpg', array('class' => 'img-4'));?>
                                    <?php echo $this->Html->image('golf/stance/direction/proper.jpg', array('class' => 'img-5'));?>
                                </div>
                            </div>
                        </div>
                        <textarea name="stance_foot_direction_note" class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
                    </div>
                </div>
                <!--/Foot Direction BOX -->

            </div>
        </div>

        <div class="margin-top-25"></div>
        <h2 class="heading-sm u-p">Arm Position:</h2>
        <hr class="shadow-line">
        <div class="container-fluid">
            <div class="row">

                <!--Right Arm BOX -->
                <div class="col-md-3 col-sm-6 scoring-box">
                    <h4 class="scoring-title u-p">Right Arm</h4>
                    <div class="scoring-body">
                        <div class=benchmarking>
                            <input class="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                            <input name="arm_position_right" type="text" class="textbox scoringValue" />
                            <div id="scoringImages" class="scoring-single-img">
                                <div class="final-img-area">
                                    <?php echo $this->Html->image('golf/arm_position/right/proper.jpg', array('class' => 'final-img'));?>
                                </div>
                                <div class="img-bundle">
                                    <?php echo $this->Html->image('golf/arm_position/right/both_band.jpg', array('class' => 'img-1'));?>
                                    <?php echo $this->Html->image('golf/arm_position/right/right_arm_band.jpg', array('class' => 'img-2'));?>
                                    <?php echo $this->Html->image('golf/arm_position/right/proper.jpg', array('class' => 'img-3'));?>
                                </div>
                            </div>
                        </div>
                        <textarea name="arm_position_right_note" class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
                    </div>
                </div>
                <!--/Right Arm BOX -->

                <!--Left Arm  BOX -->
                <div class="col-md-3 col-sm-6 scoring-box">
                    <h4 class="scoring-title u-p">Left Arm</h4>
                    <div class="scoring-body">
                        <div class=benchmarking>
                            <input class="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                            <input name="arm_position_left" type="text" class="textbox scoringValue" />
                            <div id="scoringImages" class="scoring-single-img">
                                <div class="final-img-area">
                                    <?php echo $this->Html->image('golf/arm_position/left/proper.jpg', array('class' => 'final-img'));?>
                                </div>
                                <div class="img-bundle">
                                    <?php echo $this->Html->image('golf/arm_position/left/both_band.jpg', array('class' => 'img-1'));?>
                                    <?php echo $this->Html->image('golf/arm_position/left/left_arm_band.jpg', array('class' => 'img-2'));?>
                                    <?php echo $this->Html->image('golf/arm_position/left/proper.jpg', array('class' => 'img-3'));?>
                                </div>
                            </div>
                        </div>
                        <textarea name="arm_position_left_note" class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
                    </div>
                </div>
                <!--/Left Arm BOX -->

            </div>
        </div>

        <div class="margin-top-25"></div>
        <h2 class="heading-sm u-p">Grip:</h2>
        <hr class="shadow-line">
        <div class="container-fluid">
            <div class="row">

                <!--Right Hand Grip BOX -->
                <div class="col-md-3 col-sm-6 scoring-box">
                    <h4 class="scoring-title u-p">Right Hand Grip</h4>
                    <div class="scoring-body">
                        <div class=benchmarking>
                            <input class="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                            <input name="grip_right_hand" type="text" class="textbox scoringValue" />
                            <div id="scoringImages" class="scoring-single-img">
                                <div class="final-img-area">
                                    <?php echo $this->Html->image('golf/grip/right/proper.jpg', array('class' => 'final-img'));?>
                                </div>
                                <div class="img-bundle">
                                    <?php echo $this->Html->image('golf/grip/right/far_away_right.jpg', array('class' => 'img-1'));?>
                                    <?php echo $this->Html->image('golf/grip/right/far_away_left.jpg', array('class' => 'img-2'));?>
                                    <?php echo $this->Html->image('golf/grip/right/proper.jpg', array('class' => 'img-3'));?>

                                </div>
                            </div>
                        </div>
                        <textarea name="grip_right_hand_note" class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
                    </div>
                </div>
                <!--/Right Hand Grip BOX -->

                <!--Left Hand Grip  BOX -->
                <div class="col-md-3 col-sm-6 scoring-box">
                    <h4 class="scoring-title u-p">Left Hand Grip</h4>
                    <div class="scoring-body">
                        <div class=benchmarking>
                            <input class="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                            <input name="grip_left_hand" type="text" class="textbox scoringValue" />
                            <div id="scoringImages" class="scoring-single-img">
                                <div class="final-img-area">
                                    <?php echo $this->Html->image('golf/grip/right/proper.jpg', array('class' => 'final-img'));?>
                                </div>
                                <div class="img-bundle">
                                    <?php echo $this->Html->image('golf/grip/left/far_away.jpg', array('class' => 'img-1'));?>
                                    <?php echo $this->Html->image('golf/grip/left/proper.jpg', array('class' => 'img-2'));?>
                                </div>
                            </div>
                        </div>
                        <textarea name="grip_left_hand_note" class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
                    </div>
                </div>
                <!--/Left Hand Grip BOX -->

            </div>
        </div>

        <div class="margin-top-25"></div>
        <h2 class="heading-sm u-p">Alignment: </h2>
        <hr class="shadow-line">
        <div class="container-fluid">
            <div class="row">

                <!--Feet Alignment BOX -->
                <div class="col-md-3 col-sm-6 scoring-box">
                    <h4 class="scoring-title u-p">Feet Alignment</h4>
                    <div class="scoring-body">
                        <div class=benchmarking>
                            <input class="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                            <input name="alignment_feet" type="text" class="textbox scoringValue" />
                            <div id="scoringImages" class="scoring-single-img">
                                <div class="final-img-area">
                                    <?php echo $this->Html->image('golf/position_alignment/feet/proper.jpg', array('class' => 'final-img'));?>
                                </div>
                                <div class="img-bundle">
                                    <?php echo $this->Html->image('golf/position_alignment/feet/too_far_left.jpg', array('class' => 'img-1'));?>
                                    <?php echo $this->Html->image('golf/position_alignment/feet/too_far_right.jpg', array('class' => 'img-2'));?>
                                    <?php echo $this->Html->image('golf/position_alignment/feet/proper.jpg', array('class' => 'img-3'));?>
                                </div>
                            </div>
                        </div>
                        <textarea name="alignment_feet_note" class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
                    </div>
                </div>
                <!--/Feet Alignment BOX -->

                <!--Knee Alignment  BOX -->
                <div class="col-md-3 col-sm-6 scoring-box">
                    <h4 class="scoring-title u-p">Knee Alignment</h4>
                    <div class="scoring-body">
                        <div class=benchmarking>
                            <input class="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                            <input name="alignment_knee" type="text" class="textbox scoringValue" />
                            <div id="scoringImages" class="scoring-single-img">
                                <div class="final-img-area">
                                    <?php echo $this->Html->image('golf/position_alignment/knee/proper.jpg', array('class' => 'final-img'));?>
                                </div>
                                <div class="img-bundle">
                                    <?php echo $this->Html->image('golf/position_alignment/knee/too_flex.jpg', array('class' => 'img-1'));?>
                                    <?php echo $this->Html->image('golf/position_alignment/knee/no_flex.jpg', array('class' => 'img-2'));?>
                                    <?php echo $this->Html->image('golf/position_alignment/knee/proper.jpg', array('class' => 'img-3'));?>

                                </div>
                            </div>
                        </div>
                        <textarea name="alignment_knee_note" class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
                    </div>
                </div>
                <!--/Knee Alignment BOX -->

                <!--Hip Alignment  BOX -->
                <div class="col-md-3 col-sm-6 scoring-box">
                    <h4 class="scoring-title u-p">Hip Alignment</h4>
                    <div class="scoring-body">
                        <div class=benchmarking>
                            <input class="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                            <input name="alignment_hip" type="text" class="textbox scoringValue" />
                            <div id="scoringImages" class="scoring-single-img">
                                <div class="final-img-area">
                                    <?php echo $this->Html->image('golf/position_alignment/hip/proper.jpg', array('class' => 'final-img'));?>
                                </div>
                                <div class="img-bundle">
                                    <?php echo $this->Html->image('golf/position_alignment/hip/too_far_left.jpg', array('class' => 'img-1'));?>
                                    <?php echo $this->Html->image('golf/position_alignment/hip/too_far_right.jpg', array('class' => 'img-2'));?>
                                    <?php echo $this->Html->image('golf/position_alignment/hip/proper.jpg', array('class' => 'img-3'));?>
                                </div>
                            </div>
                        </div>
                        <textarea name="alignment_hip_note" class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
                    </div>
                </div>
                <!--/Hip Alignment BOX -->


                <!--Shoulder Alignment  BOX -->
                <div class="col-md-3 col-sm-6 scoring-box">
                    <h4 class="scoring-title u-p">Shoulder Alignment</h4>
                    <div class="scoring-body">
                        <div class=benchmarking>
                            <input class="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                            <input name="alignment_shoulder" type="text" class="textbox scoringValue" />
                            <div id="scoringImages" class="scoring-single-img">
                                <div class="final-img-area">
                                    <?php echo $this->Html->image('golf/position_alignment/shoulder/proper.jpg', array('class' => 'final-img'));?>
                                </div>
                                <div class="img-bundle">
                                    <?php echo $this->Html->image('golf/position_alignment/shoulder/too_far_left.jpg', array('class' => 'img-1'));?>
                                    <?php echo $this->Html->image('golf/position_alignment/shoulder/too_far_right.jpg', array('class' => 'img-2'));?>
                                    <?php echo $this->Html->image('golf/position_alignment/shoulder/proper.jpg', array('class' => 'img-3'));?>
                                </div>
                            </div>
                        </div>
                        <textarea name="alignment_shoulder_note" class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
                    </div>
                </div>
                <!--/Shoulder Alignment BOX -->


                <!--Arm Alignment  BOX -->
                <div class="col-md-3 col-sm-6 scoring-box">
                    <h4 class="scoring-title u-p">Arm Alignment</h4>
                    <div class="scoring-body">
                        <div class=benchmarking>
                            <input class="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                            <input name="alignment_arm" type="text" class="textbox scoringValue" />
                            <div id="scoringImages" class="scoring-single-img">
                                <div class="final-img-area">
                                    <?php echo $this->Html->image('golf/position_alignment/arm/proper.jpg', array('class' => 'final-img'));?>
                                </div>
                                <div class="img-bundle">
                                    <?php echo $this->Html->image('golf/position_alignment/arm/both_band.jpg', array('class' => 'img-1'));?>
                                    <?php echo $this->Html->image('golf/position_alignment/arm/right_arm_band.jpg', array('class' => 'img-2'));?>
                                    <?php echo $this->Html->image('golf/position_alignment/arm/left_arm_band.jpg', array('class' => 'img-3'));?>
                                    <?php echo $this->Html->image('golf/position_alignment/arm/proper.jpg', array('class' => 'img-4'));?>
                                </div>
                            </div>
                        </div>
                        <textarea name="alignment_arm_note" class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
                    </div>
                </div>
                <!--/Arm Alignment BOX -->


                <!--Club Face Alignment  BOX -->
                <div class="col-md-3 col-sm-6 scoring-box">
                    <h4 class="scoring-title u-p">Club Face Alignment</h4>
                    <div class="scoring-body">
                        <div class=benchmarking>
                            <input class="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                            <input name="alignment_club_face" type="text" class="textbox scoringValue" />
                            <div id="scoringImages" class="scoring-single-img">
                                <div class="final-img-area">
                                    <?php echo $this->Html->image('golf/position_alignment/face/proper.jpg', array('class' => 'final-img'));?>
                                </div>
                                <div class="img-bundle">
                                    <?php echo $this->Html->image('golf/position_alignment/face/too_face_down.jpg', array('class' => 'img-1'));?>
                                    <?php echo $this->Html->image('golf/position_alignment/face/too_face_up.jpg', array('class' => 'img-2'));?>
                                    <?php echo $this->Html->image('golf/position_alignment/face/proper.jpg', array('class' => 'img-3'));?>
                                </div>
                            </div>
                        </div>
                        <textarea name="alignment_club_face_note" class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
                    </div>
                </div>
                <!--/Club Face Alignment BOX -->

            </div>
        </div>

        <div class="margin-top-25"></div>
        <h2 class="heading-sm u-p">Ball Position ( Where the person is relative to the ball):</h2>
        <hr class="shadow-line">
        <div class="container-fluid">
            <div class="row">

                <!--Distance Away BOX -->
                <div class="col-md-3 col-sm-6 scoring-box">
                    <h4 class="scoring-title u-p">Distance Away</h4>
                    <div class="scoring-body">
                        <div class=benchmarking>
                            <input class="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                            <input name="ball_position_distance_away" type="text" class="textbox scoringValue" />
                            <div id="scoringImages" class="scoring-single-img">
                                <div class="final-img-area">
                                   <?php echo $this->Html->image('golf/ball_position/distance_away/proper.jpg', array('class' => 'final-img'));?>
                                </div>
                                <div class="img-bundle">
                                    <?php echo $this->Html->image('golf/ball_position/distance_away/too_close.jpg', array('class' => 'img-1'));?>

                                    <?php echo $this->Html->image('golf/ball_position/distance_away/too_far.jpg', array('class' => 'img-2'));?>

                                    <?php echo $this->Html->image('golf/ball_position/distance_away/proper.jpg', array('class' => 'img-3'));?>
                                </div>
                            </div>
                        </div>
                        <textarea name="ball_position_distance_away_note" class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
                    </div>
                </div>
                <!--/Distance Away BOX -->

                <!--Forward and Back in Stance BOX -->
                <div class="col-md-3 col-sm-6 scoring-box">
                    <h4 class="scoring-title u-p">Forward and Back in Stance</h4>
                    <div class="scoring-body">
                        <div class=benchmarking>
                            <input class="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                            <input name="ball_position_forward_and_back" type="text" class="textbox scoringValue" />
                            <div id="scoringImages" class="scoring-single-img">
                                <div class="final-img-area">
                                    <?php echo $this->Html->image('golf/ball_position/front_and_back_stance/proper.jpg', array('class' => 'final-img'));?>
                                </div>
                                <div class="img-bundle">

                                    <?php echo $this->Html->image('golf/ball_position/front_and_back_stance/backward.jpg', array('class' => 'img-1'));?>

                                    <?php echo $this->Html->image('golf/ball_position/front_and_back_stance/forward.jpg', array('class' => 'img-2'));?>

                                    <?php echo $this->Html->image('golf/ball_position/front_and_back_stance/proper.jpg', array('class' => 'img-3'));?>


                                </div>
                            </div>
                        </div>
                        <textarea name="ball_position_forward_and_back_note" class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
                    </div>
                </div>
                <!--/Forward and Back in Stance BOX -->

            </div>
        </div>

    </div>
</div>
<!---- .Ball Position Widget ----->

<!---- Lower Body Stability ----->
<div class="widget black-widget">
<div class="widget-header u-p">
    <i class="fa fa-bar-chart-o"></i> Lower Body Stability
</div>
<div class="widget-body padding-15">
<h2 class="heading-sm u-p">Halfway Back: </h2>
<hr class="shadow-line">
<div class="container-fluid">
    <div class="row">

        <!--Halfway Back REAR BOX -->
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">REAR</h4>
            <div class="scoring-body">
                <div class=benchmarking>
                    <input class="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                    <input name="lower_halfway_back_pear" type="text" class="textbox scoringValue" />
                    <div id="scoringImages" class="scoring-single-img">
                        <div class="final-img-area">
                            <img src="http://placehold.it/350x150" class="final-img">
                        </div>
                        <div class="img-bundle">
                            <?php echo $this->Html->image('golf/facebook.png', array('class' => 'img-1'));?>
                            <?php echo $this->Html->image('golf/twitter.png', array('class' => 'img-2'));?>
                        </div>
                    </div>
                </div>
                <textarea name="lower_halfway_back_pear_note" class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <!--/Halfway Back REAR BOX -->

        <!--Halfway Back KNEE FLEX BOX -->
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">KNEE FLEX</h4>
            <div class="scoring-body">
                <div class=benchmarking>
                    <input class="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                    <input name="lower_halfway_back_knee_flex" type="text" class="textbox scoringValue" />
                    <div id="scoringImages" class="scoring-single-img">
                        <div class="final-img-area">
                            <img src="http://placehold.it/350x150" class="final-img">
                        </div>
                        <div class="img-bundle">
                            <?php echo $this->Html->image('golf/facebook.png', array('class' => 'img-1'));?>
                            <?php echo $this->Html->image('golf/twitter.png', array('class' => 'img-2'));?>
                        </div>
                    </div>
                </div>
                <textarea name="lower_halfway_back_knee_flex_note" class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <!--/Halfway Back KNEE FLEX BOX -->

        <!--Halfway Back HIP SWAY BOX -->
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">HIP SWAY</h4>
            <div class="scoring-body">
                <div class=benchmarking>
                    <input class="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                    <input name="lower_halfway_back_hip_sway" type="text" class="textbox scoringValue" />
                    <div id="scoringImages" class="scoring-single-img">
                        <div class="final-img-area">
                            <img src="http://placehold.it/350x150" class="final-img">
                        </div>
                        <div class="img-bundle">
                            <?php echo $this->Html->image('golf/facebook.png', array('class' => 'img-1'));?>
                            <?php echo $this->Html->image('golf/twitter.png', array('class' => 'img-2'));?>
                        </div>
                    </div>
                </div>
                <textarea name="lower_halfway_back_hip_sway_note" class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <!--/Halfway Back HIP SWAY BOX -->

        <!--Halfway Back HIP PIVOT BOX -->
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">HIP PIVOT</h4>
            <div class="scoring-body">
                <div class=benchmarking>
                    <input class="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                    <input name="lower_halfway_back_hip_pivot" type="text" class="textbox scoringValue" />
                    <div id="scoringImages" class="scoring-single-img">
                        <div class="final-img-area">
                            <img src="http://placehold.it/350x150" class="final-img">
                        </div>
                        <div class="img-bundle">
                            <?php echo $this->Html->image('golf/facebook.png', array('class' => 'img-1'));?>
                            <?php echo $this->Html->image('golf/twitter.png', array('class' => 'img-2'));?>
                        </div>
                    </div>
                </div>
                <textarea name="lower_halfway_back_hip_pivot_note" class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <!--/Halfway Back HIP PIVOT BOX -->

    </div>
</div>
<div class="margin-top-25"></div>
<h2 class="heading-sm u-p">¾ Swing: </h2>
<hr class="shadow-line">
<div class="container-fluid">
    <div class="row">

        <!--¾ Swing REAR BOX -->
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">REAR</h4>
            <div class="scoring-body">
                <div class=benchmarking>
                    <input class="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                    <input name="lower_swing_pear" type="text" class="textbox scoringValue" />
                    <div id="scoringImages" class="scoring-single-img">
                        <div class="final-img-area">
                            <img src="http://placehold.it/350x150" class="final-img">
                        </div>
                        <div class="img-bundle">
                            <?php echo $this->Html->image('golf/facebook.png', array('class' => 'img-1'));?>
                            <?php echo $this->Html->image('golf/twitter.png', array('class' => 'img-2'));?>
                        </div>
                    </div>
                </div>
                <textarea name="lower_swing_pear_note" class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <!--/¾ Swing REAR BOX -->

        <!--¾ Swing KNEE FLEX BOX -->
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">KNEE FLEX</h4>
            <div class="scoring-body">
                <div class=benchmarking>
                    <input class="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                    <input name="lower_swing_knee_flex" type="text" class="textbox scoringValue" />
                    <div id="scoringImages" class="scoring-single-img">
                        <div class="final-img-area">
                            <img src="http://placehold.it/350x150" class="final-img">
                        </div>
                        <div class="img-bundle">
                            <?php echo $this->Html->image('golf/facebook.png', array('class' => 'img-1'));?>
                            <?php echo $this->Html->image('golf/twitter.png', array('class' => 'img-2'));?>
                        </div>
                    </div>
                </div>
                <textarea name="lower_swing_knee_flex_note" class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <!--/¾ Swing KNEE FLEX BOX -->

        <!--¾ Swing HIP SWAY BOX -->
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">HIP SWAY</h4>
            <div class="scoring-body">
                <div class=benchmarking>
                    <input class="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                    <input name="lower_swing_hip_sway" type="text" class="textbox scoringValue" />
                    <div id="scoringImages" class="scoring-single-img">
                        <div class="final-img-area">
                            <img src="http://placehold.it/350x150" class="final-img">
                        </div>
                        <div class="img-bundle">
                            <?php echo $this->Html->image('golf/facebook.png', array('class' => 'img-1'));?>
                            <?php echo $this->Html->image('golf/twitter.png', array('class' => 'img-2'));?>
                        </div>
                    </div>
                </div>
                <textarea name="lower_swing_hip_sway_note" class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <!--/¾ Swing HIP SWAY BOX -->

        <!--¾ Swing HIP PIVOT BOX -->
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">HIP PIVOT</h4>
            <div class="scoring-body">
                <div class=benchmarking>
                    <input class="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                    <input name="lower_swing_hip_pivot" type="text" class="textbox scoringValue" />
                    <div id="scoringImages" class="scoring-single-img">
                        <div class="final-img-area">
                            <img src="http://placehold.it/350x150" class="final-img">
                        </div>
                        <div class="img-bundle">
                            <?php echo $this->Html->image('golf/facebook.png', array('class' => 'img-1'));?>
                            <?php echo $this->Html->image('golf/twitter.png', array('class' => 'img-2'));?>
                        </div>
                    </div>
                </div>
                <textarea name="lower_swing_hip_pivot_note" class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <!--/¾ Swing HIP PIVOT BOX -->

    </div>
</div>
<div class="margin-top-25"></div>
<h2 class="heading-sm u-p">At Top: </h2>
<hr class="shadow-line">
<div class="container-fluid">
    <div class="row">

        <!--At Top REAR BOX -->
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">REAR</h4>
            <div class="scoring-body">
                <div class=benchmarking>
                    <input class="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                    <input name="lower_at_top_pear" type="text" class="textbox scoringValue" />
                    <div id="scoringImages" class="scoring-single-img">
                        <div class="final-img-area">
                            <img src="http://placehold.it/350x150" class="final-img">
                        </div>
                        <div class="img-bundle">
                            <?php echo $this->Html->image('golf/facebook.png', array('class' => 'img-1'));?>
                            <?php echo $this->Html->image('golf/twitter.png', array('class' => 'img-2'));?>
                        </div>
                    </div>
                </div>
                <textarea name="lower_at_top_pear_note" class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <!--/At Top REAR BOX -->

        <!--At Top KNEE FLEX BOX -->
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">KNEE FLEX</h4>
            <div class="scoring-body">
                <div class=benchmarking>
                    <input class="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                    <input name="lower_at_top_knee_flex" type="text" class="textbox scoringValue" />
                    <div id="scoringImages" class="scoring-single-img">
                        <div class="final-img-area">
                            <img src="http://placehold.it/350x150" class="final-img">
                        </div>
                        <div class="img-bundle">
                            <?php echo $this->Html->image('golf/facebook.png', array('class' => 'img-1'));?>
                            <?php echo $this->Html->image('golf/twitter.png', array('class' => 'img-2'));?>
                        </div>
                    </div>
                </div>
                <textarea name="lower_at_top_knee_flex_note" class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <!--/At Top KNEE FLEX BOX -->

        <!--At Top HIP SWAY BOX -->
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">HIP SWAY</h4>
            <div class="scoring-body">
                <div class=benchmarking>
                    <input class="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                    <input name="lower_at_top_hip_sway" type="text" class="textbox scoringValue" />
                    <div id="scoringImages" class="scoring-single-img">
                        <div class="final-img-area">
                            <img src="http://placehold.it/350x150" class="final-img">
                        </div>
                        <div class="img-bundle">
                            <?php echo $this->Html->image('golf/facebook.png', array('class' => 'img-1'));?>
                            <?php echo $this->Html->image('golf/twitter.png', array('class' => 'img-2'));?>
                        </div>
                    </div>
                </div>
                <textarea name="lower_at_top_hip_sway_note" class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <!--/At Top HIP SWAY BOX -->

        <!--At Top HIP PIVOT BOX -->
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">HIP PIVOT</h4>
            <div class="scoring-body">
                <div class=benchmarking>
                    <input class="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                    <input name="lower_at_top_hip_pivot" type="text" class="textbox scoringValue" />
                    <div id="scoringImages" class="scoring-single-img">
                        <div class="final-img-area">
                            <img src="http://placehold.it/350x150" class="final-img">
                        </div>
                        <div class="img-bundle">
                            <?php echo $this->Html->image('golf/facebook.png', array('class' => 'img-1'));?>
                            <?php echo $this->Html->image('golf/twitter.png', array('class' => 'img-2'));?>
                        </div>
                    </div>
                </div>
                <textarea name="lower_at_top_hip_pivot_note" class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <!--/At Top HIP PIVOT BOX -->

    </div>
</div>
<div class="margin-top-25"></div>
<h2 class="heading-sm u-p">Downswing: </h2>
<hr class="shadow-line">
<div class="container-fluid">
    <div class="row">

        <!--Downswing REAR BOX -->
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">REAR</h4>
            <div class="scoring-body">
                <div class=benchmarking>
                    <input class="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                    <input name="lower_downswing_pear" type="text" class="textbox scoringValue" />
                    <div id="scoringImages" class="scoring-single-img">
                        <div class="final-img-area">
                            <img src="http://placehold.it/350x150" class="final-img">
                        </div>
                        <div class="img-bundle">
                            <?php echo $this->Html->image('golf/facebook.png', array('class' => 'img-1'));?>
                            <?php echo $this->Html->image('golf/twitter.png', array('class' => 'img-2'));?>
                        </div>
                    </div>
                </div>
                <textarea name="lower_downswing_pear_note" class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <!--/Downswing REAR BOX -->

        <!--Downswing KNEE FLEX BOX -->
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">KNEE FLEX</h4>
            <div class="scoring-body">
                <div class=benchmarking>
                    <input class="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                    <input name="lower_downswing_knee_flex" type="text" class="textbox scoringValue" />
                    <div id="scoringImages" class="scoring-single-img">
                        <div class="final-img-area">
                            <img src="http://placehold.it/350x150" class="final-img">
                        </div>
                        <div class="img-bundle">
                            <?php echo $this->Html->image('golf/facebook.png', array('class' => 'img-1'));?>
                            <?php echo $this->Html->image('golf/twitter.png', array('class' => 'img-2'));?>
                        </div>
                    </div>
                </div>
                <textarea name="lower_downswing_knee_flex_note" class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <!--/Downswing KNEE FLEX BOX -->

        <!--Downswing HIP SWAY BOX -->
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">HIP SWAY</h4>
            <div class="scoring-body">
                <div class=benchmarking>
                    <input class="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                    <input name="lower_downswing_hip_sway" type="text" class="textbox scoringValue" />
                    <div id="scoringImages" class="scoring-single-img">
                        <div class="final-img-area">
                            <img src="http://placehold.it/350x150" class="final-img">
                        </div>
                        <div class="img-bundle">
                            <?php echo $this->Html->image('golf/facebook.png', array('class' => 'img-1'));?>
                            <?php echo $this->Html->image('golf/twitter.png', array('class' => 'img-2'));?>
                        </div>
                    </div>
                </div>
                <textarea name="lower_downswing_hip_sway_note" class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <!--/Downswing HIP SWAY BOX -->

        <!--Downswing HIP PIVOT BOX -->
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">HIP PIVOT</h4>
            <div class="scoring-body">
                <div class=benchmarking>
                    <input class="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                    <input name="lower_downswing_hip_pivot" type="text" class="textbox scoringValue" />
                    <div id="scoringImages" class="scoring-single-img">
                        <div class="final-img-area">
                            <img src="http://placehold.it/350x150" class="final-img">
                        </div>
                        <div class="img-bundle">
                            <?php echo $this->Html->image('golf/facebook.png', array('class' => 'img-1'));?>
                            <?php echo $this->Html->image('golf/twitter.png', array('class' => 'img-2'));?>
                        </div>
                    </div>
                </div>
                <textarea name="lower_downswing_hip_pivot_note" class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <!--/Downswing HIP PIVOT BOX -->

    </div>
</div>
<div class="margin-top-25"></div>
<h2 class="heading-sm u-p">Impact: </h2>
<hr class="shadow-line">
<div class="container-fluid">
    <div class="row">

        <!--Impact REAR BOX -->
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">REAR</h4>
            <div class="scoring-body">
                <div class=benchmarking>
                    <input class="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                    <input name="lower_impact_pear" type="text" class="textbox scoringValue" />
                    <div id="scoringImages" class="scoring-single-img">
                        <div class="final-img-area">
                            <img src="http://placehold.it/350x150" class="final-img">
                        </div>
                        <div class="img-bundle">
                            <?php echo $this->Html->image('golf/facebook.png', array('class' => 'img-1'));?>
                            <?php echo $this->Html->image('golf/twitter.png', array('class' => 'img-2'));?>
                        </div>
                    </div>
                </div>
                <textarea name="lower_impact_pear_note" class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <!--/Impact REAR BOX -->

        <!--Impact KNEE FLEX BOX -->
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">KNEE FLEX</h4>
            <div class="scoring-body">
                <div class=benchmarking>
                    <input class="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                    <input name="lower_impact_knee_flex" type="text" class="textbox scoringValue" />
                    <div id="scoringImages" class="scoring-single-img">
                        <div class="final-img-area">
                            <img src="http://placehold.it/350x150" class="final-img">
                        </div>
                        <div class="img-bundle">
                            <?php echo $this->Html->image('golf/facebook.png', array('class' => 'img-1'));?>
                            <?php echo $this->Html->image('golf/twitter.png', array('class' => 'img-2'));?>
                        </div>
                    </div>
                </div>
                <textarea name="lower_impact_knee_flex_note" class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <!--/Impact KNEE FLEX BOX -->

        <!--Impact HIP SWAY BOX -->
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">HIP SWAY</h4>
            <div class="scoring-body">
                <div class=benchmarking>
                    <input class="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                    <input name="lower_impact_hip_sway" type="text" class="textbox scoringValue" />
                    <div id="scoringImages" class="scoring-single-img">
                        <div class="final-img-area">
                            <img src="http://placehold.it/350x150" class="final-img">
                        </div>
                        <div class="img-bundle">
                            <?php echo $this->Html->image('golf/facebook.png', array('class' => 'img-1'));?>
                            <?php echo $this->Html->image('golf/twitter.png', array('class' => 'img-2'));?>
                        </div>
                    </div>
                </div>
                <textarea name="lower_impact_hip_sway_note" class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <!--/Impact HIP SWAY BOX -->

        <!--Impact HIP PIVOT BOX -->
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">HIP PIVOT</h4>
            <div class="scoring-body">
                <div class=benchmarking>
                    <input class="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                    <input name="lower_impact_hip_pivot" type="text" class="textbox scoringValue" />
                    <div id="scoringImages" class="scoring-single-img">
                        <div class="final-img-area">
                            <img src="http://placehold.it/350x150" class="final-img">
                        </div>
                        <div class="img-bundle">
                            <?php echo $this->Html->image('golf/facebook.png', array('class' => 'img-1'));?>
                            <?php echo $this->Html->image('golf/twitter.png', array('class' => 'img-2'));?>
                        </div>
                    </div>
                </div>
                <textarea name="lower_impact_hip_pivot_note" class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <!--/Impact HIP PIVOT BOX -->

    </div>
</div>
<div class="margin-top-25"></div>
<h2 class="heading-sm u-p">Follow Through: </h2>
<hr class="shadow-line">
<div class="container-fluid">
    <div class="row">

        <!--Follow Through REAR BOX -->
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">REAR</h4>
            <div class="scoring-body">
                <div class=benchmarking>
                    <input class="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                    <input name="lower_through_pear" type="text" class="textbox scoringValue" />
                    <div id="scoringImages" class="scoring-single-img">
                        <div class="final-img-area">
                            <img src="http://placehold.it/350x150" class="final-img">
                        </div>
                        <div class="img-bundle">
                            <?php echo $this->Html->image('golf/facebook.png', array('class' => 'img-1'));?>
                            <?php echo $this->Html->image('golf/twitter.png', array('class' => 'img-2'));?>
                        </div>
                    </div>
                </div>
                <textarea name="lower_through_pear_note" class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <!--/Follow Through REAR BOX -->

        <!--Follow Through KNEE FLEX BOX -->
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">KNEE FLEX</h4>
            <div class="scoring-body">
                <div class=benchmarking>
                    <input class="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                    <input name="lower_through_knee_flex" type="text" class="textbox scoringValue" />
                    <div id="scoringImages" class="scoring-single-img">
                        <div class="final-img-area">
                            <img src="http://placehold.it/350x150" class="final-img">
                        </div>
                        <div class="img-bundle">
                            <?php echo $this->Html->image('golf/facebook.png', array('class' => 'img-1'));?>
                            <?php echo $this->Html->image('golf/twitter.png', array('class' => 'img-2'));?>
                        </div>
                    </div>
                </div>
                <textarea name="lower_through_knee_flex_note" class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <!--/Follow Through KNEE FLEX BOX -->

        <!--Follow Through HIP SWAY BOX -->
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">HIP SWAY</h4>
            <div class="scoring-body">
                <div class=benchmarking>
                    <input class="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                    <input name="lower_through_hip_sway" type="text" class="textbox scoringValue" />
                    <div id="scoringImages" class="scoring-single-img">
                        <div class="final-img-area">
                            <img src="http://placehold.it/350x150" class="final-img">
                        </div>
                        <div class="img-bundle">
                            <?php echo $this->Html->image('golf/facebook.png', array('class' => 'img-1'));?>
                            <?php echo $this->Html->image('golf/twitter.png', array('class' => 'img-2'));?>
                        </div>
                    </div>
                </div>
                <textarea name="lower_through_hip_sway_note" class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <!--/Follow Through HIP SWAY BOX -->

        <!--Follow Through HIP PIVOT BOX -->
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">HIP PIVOT</h4>
            <div class="scoring-body">
                <div class=benchmarking>
                    <input class="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                    <input name="lower_through_hip_pivot" type="text" class="textbox scoringValue" />
                    <div id="scoringImages" class="scoring-single-img">
                        <div class="final-img-area">
                            <img src="http://placehold.it/350x150" class="final-img">
                        </div>
                        <div class="img-bundle">
                            <?php echo $this->Html->image('golf/facebook.png', array('class' => 'img-1'));?>
                            <?php echo $this->Html->image('golf/twitter.png', array('class' => 'img-2'));?>
                        </div>
                    </div>
                </div>
                <textarea name="lower_through_hip_pivot_note" class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <!--/Follow Through HIP PIVOT BOX -->

    </div>
</div>
<div class="margin-top-25"></div>
<h2 class="heading-sm u-p">Finish: </h2>
<hr class="shadow-line">
<div class="container-fluid">
    <div class="row">

        <!--Finish REAR BOX -->
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">REAR</h4>
            <div class="scoring-body">
                <div class=benchmarking>
                    <input class="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                    <input name="lower_finish_pear" type="text" class="textbox scoringValue" />
                    <div id="scoringImages" class="scoring-single-img">
                        <div class="final-img-area">
                            <img src="http://placehold.it/350x150" class="final-img">
                        </div>
                        <div class="img-bundle">
                            <?php echo $this->Html->image('golf/facebook.png', array('class' => 'img-1'));?>
                            <?php echo $this->Html->image('golf/twitter.png', array('class' => 'img-2'));?>
                        </div>
                    </div>
                </div>
                <textarea name="lower_finish_pear_note" class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <!--/Finish REAR BOX -->

        <!--Finish KNEE FLEX BOX -->
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">KNEE FLEX</h4>
            <div class="scoring-body">
                <div class=benchmarking>
                    <input class="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                    <input name="lower_finish_knee_flex" type="text" class="textbox scoringValue" />
                    <div id="scoringImages" class="scoring-single-img">
                        <div class="final-img-area">
                            <img src="http://placehold.it/350x150" class="final-img">
                        </div>
                        <div class="img-bundle">
                            <?php echo $this->Html->image('golf/facebook.png', array('class' => 'img-1'));?>
                            <?php echo $this->Html->image('golf/twitter.png', array('class' => 'img-2'));?>
                        </div>
                    </div>
                </div>
                <textarea name="lower_finish_knee_flex_note" class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <!--/Finish KNEE FLEX BOX -->

        <!--Finish HIP SWAY BOX -->
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">HIP SWAY</h4>
            <div class="scoring-body">
                <div class=benchmarking>
                    <input class="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                    <input name="lower_finish_hip_sway" type="text" class="textbox scoringValue" />
                    <div id="scoringImages" class="scoring-single-img">
                        <div class="final-img-area">
                            <img src="http://placehold.it/350x150" class="final-img">
                        </div>
                        <div class="img-bundle">
                            <?php echo $this->Html->image('golf/facebook.png', array('class' => 'img-1'));?>
                            <?php echo $this->Html->image('golf/twitter.png', array('class' => 'img-2'));?>
                        </div>
                    </div>
                </div>
                <textarea name="lower_finish_hip_sway_note" class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <!--/Finish HIP SWAY BOX -->

        <!--Finish HIP PIVOT BOX -->
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">HIP PIVOT</h4>
            <div class="scoring-body">
                <div class=benchmarking>
                    <input class="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                    <input name="lower_finish_hip_pivot" type="text" class="textbox scoringValue" />
                    <div id="scoringImages" class="scoring-single-img">
                        <div class="final-img-area">
                            <img src="http://placehold.it/350x150" class="final-img">
                        </div>
                        <div class="img-bundle">
                            <?php echo $this->Html->image('golf/facebook.png', array('class' => 'img-1'));?>
                            <?php echo $this->Html->image('golf/twitter.png', array('class' => 'img-2'));?>
                        </div>
                    </div>
                </div>
                <textarea name="lower_finish_hip_pivot_note" class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <!--/Finish HIP PIVOT BOX -->

    </div>
</div>
</div>
</div>
<!---- /Lower Body Stability ----->

<!---- Upper Body Stability ----->
<div class="widget black-widget">
<div class="widget-header u-p">
    <i class="fa fa-bar-chart-o"></i> Upper Body Position
</div>
<div class="widget-body padding-15">
<h2 class="heading-sm u-p">Upper Body Halfway Back:</h2>
<hr class="shadow-line">
<div class="container-fluid">
    <div class="row">

        <!--Upper Body Halfway Back ROTATION BOX -->
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">ROTATION</h4>
            <div class="scoring-body">
                <div class=benchmarking>
                    <input class="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                    <input name="upper_halfway_back_rotation" type="text" class="textbox scoringValue" />
                    <div id="scoringImages" class="scoring-single-img">
                        <div class="final-img-area">
                            <img src="http://placehold.it/350x150" class="final-img">
                        </div>
                        <div class="img-bundle">
                            <?php echo $this->Html->image('golf/facebook.png', array('class' => 'img-1'));?>
                            <?php echo $this->Html->image('golf/twitter.png', array('class' => 'img-2'));?>
                        </div>
                    </div>
                </div>
                <textarea name="upper_halfway_back_rotation_note" class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <!--/Upper Body Halfway Back ROTATION BOX -->

        <!--Upper Body Halfway Back SPINE TILT BOX -->
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">SPINE TILT</h4>
            <div class="scoring-body">
                <div class=benchmarking>
                    <input class="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                    <input name="upper_halfway_back_spine" type="text" class="textbox scoringValue" />
                    <div id="scoringImages" class="scoring-single-img">
                        <div class="final-img-area">
                            <img src="http://placehold.it/350x150" class="final-img">
                        </div>
                        <div class="img-bundle">
                            <?php echo $this->Html->image('golf/facebook.png', array('class' => 'img-1'));?>
                            <?php echo $this->Html->image('golf/twitter.png', array('class' => 'img-2'));?>
                        </div>
                    </div>
                </div>
                <textarea name="upper_halfway_back_spine_note" class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <!--/Upper Body Halfway Back SPINE TILT BOX -->

        <!--Upper Body Halfway Back HEAD BOX -->
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">HEAD</h4>
            <div class="scoring-body">
                <div class=benchmarking>
                    <input class="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                    <input name="upper_halfway_back_head" type="text" class="textbox scoringValue" />
                    <div id="scoringImages" class="scoring-single-img">
                        <div class="final-img-area">
                            <img src="http://placehold.it/350x150" class="final-img">
                        </div>
                        <div class="img-bundle">
                            <?php echo $this->Html->image('golf/facebook.png', array('class' => 'img-1'));?>
                            <?php echo $this->Html->image('golf/twitter.png', array('class' => 'img-2'));?>
                        </div>
                    </div>
                </div>
                <textarea name="upper_halfway_back_head_note" class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <!--/Upper Body Halfway Back HEAD BOX -->

    </div>
</div>
<div class="margin-top-25"></div>
<h2 class="heading-sm u-p">Upper Body ¾ Swing:</h2>
<hr class="shadow-line">
<div class="container-fluid">
    <div class="row">

        <!--Upper Body ¾ Swing ROTATION BOX -->
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">ROTATION</h4>
            <div class="scoring-body">
                <div class=benchmarking>
                    <input class="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                    <input name="upper_swing_rotation" type="text" class="textbox scoringValue" />
                    <div id="scoringImages" class="scoring-single-img">
                        <div class="final-img-area">
                            <img src="http://placehold.it/350x150" class="final-img">
                        </div>
                        <div class="img-bundle">
                            <?php echo $this->Html->image('golf/facebook.png', array('class' => 'img-1'));?>
                            <?php echo $this->Html->image('golf/twitter.png', array('class' => 'img-2'));?>
                        </div>
                    </div>
                </div>
                <textarea name="upper_swing_rotation_note" class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <!--/Upper Body ¾ Swing ROTATION BOX -->

        <!--Upper Body ¾ Swing SPINE TILT BOX -->
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">SPINE TILT</h4>
            <div class="scoring-body">
                <div class=benchmarking>
                    <input class="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                    <input name="upper_swing_spine" type="text" class="textbox scoringValue" />
                    <div id="scoringImages" class="scoring-single-img">
                        <div class="final-img-area">
                            <img src="http://placehold.it/350x150" class="final-img">
                        </div>
                        <div class="img-bundle">
                            <?php echo $this->Html->image('golf/facebook.png', array('class' => 'img-1'));?>
                            <?php echo $this->Html->image('golf/twitter.png', array('class' => 'img-2'));?>
                        </div>
                    </div>
                </div>
                <textarea name="upper_swing_spine_note" class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <!--/Upper Body ¾ Swing SPINE TILT BOX -->

        <!--Upper Body ¾ Swing HEAD BOX -->
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">HEAD</h4>
            <div class="scoring-body">
                <div class=benchmarking>
                    <input class="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                    <input name="upper_swing_head" type="text" class="textbox scoringValue" />
                    <div id="scoringImages" class="scoring-single-img">
                        <div class="final-img-area">
                            <img src="http://placehold.it/350x150" class="final-img">
                        </div>
                        <div class="img-bundle">
                            <?php echo $this->Html->image('golf/facebook.png', array('class' => 'img-1'));?>
                            <?php echo $this->Html->image('golf/twitter.png', array('class' => 'img-2'));?>
                        </div>
                    </div>
                </div>
                <textarea name="upper_swing_head_note" class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <!--/Upper Body ¾ Swing HEAD BOX -->

    </div>
</div>
<div class="margin-top-25"></div>
<h2 class="heading-sm u-p">Upper Body At Top:</h2>
<hr class="shadow-line">
<div class="container-fluid">
    <div class="row">

        <!--Upper Body At Top ROTATION BOX -->
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">ROTATION</h4>
            <div class="scoring-body">
                <div class=benchmarking>
                    <input class="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                    <input name="upper_at_top_rotation" type="text" class="textbox scoringValue" />
                    <div id="scoringImages" class="scoring-single-img">
                        <div class="final-img-area">
                            <img src="http://placehold.it/350x150" class="final-img">
                        </div>
                        <div class="img-bundle">
                            <?php echo $this->Html->image('golf/facebook.png', array('class' => 'img-1'));?>
                            <?php echo $this->Html->image('golf/twitter.png', array('class' => 'img-2'));?>
                        </div>
                    </div>
                </div>
                <textarea name="upper_at_top_rotation_note" class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <!--/Upper Body At Top ROTATION BOX -->

        <!--SPINE TILT BOX -->
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">SPINE TILT</h4>
            <div class="scoring-body">
                <div class=benchmarking>
                    <input class="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                    <input name="upper_at_top_spine" type="text" class="textbox scoringValue" />
                    <div id="scoringImages" class="scoring-single-img">
                        <div class="final-img-area">
                            <img src="http://placehold.it/350x150" class="final-img">
                        </div>
                        <div class="img-bundle">
                            <?php echo $this->Html->image('golf/facebook.png', array('class' => 'img-1'));?>
                            <?php echo $this->Html->image('golf/twitter.png', array('class' => 'img-2'));?>
                        </div>
                    </div>
                </div>
                <textarea name="upper_at_top_spine_note" class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <!--/Upper Body At Top SPINE TILT BOX -->

        <!--Upper Body At Top HEAD BOX -->
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">HEAD</h4>
            <div class="scoring-body">
                <div class=benchmarking>
                    <input class="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                    <input name="upper_at_top_head" type="text" class="textbox scoringValue" />
                    <div id="scoringImages" class="scoring-single-img">
                        <div class="final-img-area">
                            <img src="http://placehold.it/350x150" class="final-img">
                        </div>
                        <div class="img-bundle">
                            <?php echo $this->Html->image('golf/facebook.png', array('class' => 'img-1'));?>
                            <?php echo $this->Html->image('golf/twitter.png', array('class' => 'img-2'));?>
                        </div>
                    </div>
                </div>
                <textarea name="upper_at_top_head_note" class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <!--/Upper Body At Top HEAD BOX -->

    </div>
</div>
<div class="margin-top-25"></div>
<h2 class="heading-sm u-p">Upper Body At Downswing:</h2>
<hr class="shadow-line">
<div class="container-fluid">
    <div class="row">

        <!--Upper Body At Downswing ROTATION BOX -->
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">ROTATION</h4>
            <div class="scoring-body">
                <div class=benchmarking>
                    <input class="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                    <input name="upper_downswing_rotation" type="text" class="textbox scoringValue" />
                    <div id="scoringImages" class="scoring-single-img">
                        <div class="final-img-area">
                            <img src="http://placehold.it/350x150" class="final-img">
                        </div>
                        <div class="img-bundle">
                            <?php echo $this->Html->image('golf/facebook.png', array('class' => 'img-1'));?>
                            <?php echo $this->Html->image('golf/twitter.png', array('class' => 'img-2'));?>
                        </div>
                    </div>
                </div>
                <textarea name="upper_downswing_rotation_note" class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <!--/Upper Body At Downswing ROTATION BOX -->

        <!--Upper Body At Downswing SPINE TILT BOX -->
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">SPINE TILT</h4>
            <div class="scoring-body">
                <div class=benchmarking>
                    <input class="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                    <input name="upper_downswing_spine" type="text" class="textbox scoringValue" />
                    <div id="scoringImages" class="scoring-single-img">
                        <div class="final-img-area">
                            <img src="http://placehold.it/350x150" class="final-img">
                        </div>
                        <div class="img-bundle">
                            <?php echo $this->Html->image('golf/facebook.png', array('class' => 'img-1'));?>
                            <?php echo $this->Html->image('golf/twitter.png', array('class' => 'img-2'));?>
                        </div>
                    </div>
                </div>
                <textarea name="upper_downswing_spine_note" class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <!--/Upper Body At Downswing SPINE TILT BOX -->

        <!--Upper Body At Downswing HEAD BOX -->
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">HEAD</h4>
            <div class="scoring-body">
                <div class=benchmarking>
                    <input class="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                    <input name="upper_downswing_head" type="text" class="textbox scoringValue" />
                    <div id="scoringImages" class="scoring-single-img">
                        <div class="final-img-area">
                            <img src="http://placehold.it/350x150" class="final-img">
                        </div>
                        <div class="img-bundle">
                            <?php echo $this->Html->image('golf/facebook.png', array('class' => 'img-1'));?>
                            <?php echo $this->Html->image('golf/twitter.png', array('class' => 'img-2'));?>
                        </div>
                    </div>
                </div>
                <textarea name="upper_downswing_head_note" class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <!--/Upper Body At Downswing HEAD BOX -->

    </div>
</div>
<div class="margin-top-25"></div>
<h2 class="heading-sm u-p">Upper Body At Impact:</h2>
<hr class="shadow-line">
<div class="container-fluid">
    <div class="row">

        <!--Upper Body At Impact ROTATION BOX -->
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">ROTATION</h4>
            <div class="scoring-body">
                <div class=benchmarking>
                    <input class="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                    <input name="upper_impact_rotation" type="text" class="textbox scoringValue" />
                    <div id="scoringImages" class="scoring-single-img">
                        <div class="final-img-area">
                            <img src="http://placehold.it/350x150" class="final-img">
                        </div>
                        <div class="img-bundle">
                            <?php echo $this->Html->image('golf/facebook.png', array('class' => 'img-1'));?>
                            <?php echo $this->Html->image('golf/twitter.png', array('class' => 'img-2'));?>
                        </div>
                    </div>
                </div>
                <textarea name="upper_impact_rotation_note" class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <!--/Upper Body At Impact ROTATION BOX -->

        <!--Upper Body At Impact SPINE TILT BOX -->
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">SPINE TILT</h4>
            <div class="scoring-body">
                <div class=benchmarking>
                    <input class="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                    <input name="upper_impact_spine" type="text" class="textbox scoringValue" />
                    <div id="scoringImages" class="scoring-single-img">
                        <div class="final-img-area">
                            <img src="http://placehold.it/350x150" class="final-img">
                        </div>
                        <div class="img-bundle">
                            <?php echo $this->Html->image('golf/facebook.png', array('class' => 'img-1'));?>
                            <?php echo $this->Html->image('golf/twitter.png', array('class' => 'img-2'));?>
                        </div>
                    </div>
                </div>
                <textarea name="upper_impact_spine_note" class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <!--/Upper Body At Impact SPINE TILT BOX -->

        <!--Upper Body At Impact HEAD BOX -->
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">HEAD</h4>
            <div class="scoring-body">
                <div class=benchmarking>
                    <input class="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                    <input name="upper_impact_head" type="text" class="textbox scoringValue" />
                    <div id="scoringImages" class="scoring-single-img">
                        <div class="final-img-area">
                            <img src="http://placehold.it/350x150" class="final-img">
                        </div>
                        <div class="img-bundle">
                            <?php echo $this->Html->image('golf/facebook.png', array('class' => 'img-1'));?>
                            <?php echo $this->Html->image('golf/twitter.png', array('class' => 'img-2'));?>
                        </div>
                    </div>
                </div>
                <textarea name="upper_impact_head_note" class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <!--/Upper Body At Impact HEAD BOX -->

    </div>
</div>
<div class="margin-top-25"></div>
<h2 class="heading-sm u-p">Upper Body Follow Through:</h2>
<hr class="shadow-line">
<div class="container-fluid">
    <div class="row">

        <!--Upper Body Follow Through ROTATION BOX -->
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">ROTATION</h4>
            <div class="scoring-body">
                <div class=benchmarking>
                    <input class="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                    <input name="upper_through_rotation" type="text" class="textbox scoringValue" />
                    <div id="scoringImages" class="scoring-single-img">
                        <div class="final-img-area">
                            <img src="http://placehold.it/350x150" class="final-img">
                        </div>
                        <div class="img-bundle">
                            <?php echo $this->Html->image('golf/facebook.png', array('class' => 'img-1'));?>
                            <?php echo $this->Html->image('golf/twitter.png', array('class' => 'img-2'));?>
                        </div>
                    </div>
                </div>
                <textarea name="upper_through_rotation_note" class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <!--/Upper Body Follow Through ROTATION BOX -->

        <!--Upper Body Follow Through SPINE TILT BOX -->
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">SPINE TILT</h4>
            <div class="scoring-body">
                <div class=benchmarking>
                    <input class="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                    <input name="upper_through_spine" type="text" class="textbox scoringValue" />
                    <div id="scoringImages" class="scoring-single-img">
                        <div class="final-img-area">
                            <img src="http://placehold.it/350x150" class="final-img">
                        </div>
                        <div class="img-bundle">
                            <?php echo $this->Html->image('golf/facebook.png', array('class' => 'img-1'));?>
                            <?php echo $this->Html->image('golf/twitter.png', array('class' => 'img-2'));?>
                        </div>
                    </div>
                </div>
                <textarea name="upper_through_spine_note" class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <!--/Upper Body Follow Through SPINE TILT BOX -->

        <!--Upper Body Follow Through HEAD BOX -->
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">HEAD</h4>
            <div class="scoring-body">
                <div class=benchmarking>
                    <input class="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                    <input name="upper_through_head" type="text" class="textbox scoringValue" />
                    <div id="scoringImages" class="scoring-single-img">
                        <div class="final-img-area">
                            <img src="http://placehold.it/350x150" class="final-img">
                        </div>
                        <div class="img-bundle">
                            <?php echo $this->Html->image('golf/facebook.png', array('class' => 'img-1'));?>
                            <?php echo $this->Html->image('golf/twitter.png', array('class' => 'img-2'));?>
                        </div>
                    </div>
                </div>
                <textarea name="upper_through_head_note" class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <!--/Upper Body Follow Through HEAD BOX -->

    </div>
</div>
<div class="margin-top-25"></div>
<h2 class="heading-sm u-p">Upper Body Follow Finish:</h2>
<hr class="shadow-line">
<div class="container-fluid">
    <div class="row">

        <!--Upper Body Follow Finish ROTATION BOX -->
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">ROTATION</h4>
            <div class="scoring-body">
                <div class=benchmarking>
                    <input class="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                    <input name="upper_finish_rotation" type="text" class="textbox scoringValue" />
                    <div id="scoringImages" class="scoring-single-img">
                        <div class="final-img-area">
                            <img src="http://placehold.it/350x150" class="final-img">
                        </div>
                        <div class="img-bundle">
                            <?php echo $this->Html->image('golf/facebook.png', array('class' => 'img-1'));?>
                            <?php echo $this->Html->image('golf/twitter.png', array('class' => 'img-2'));?>
                        </div>
                    </div>
                </div>
                <textarea name="upper_finish_rotation_note" class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <!--/Upper Body Follow Finish ROTATION BOX -->

        <!--Upper Body Follow Finish SPINE TILT BOX -->
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">SPINE TILT</h4>
            <div class="scoring-body">
                <div class=benchmarking>
                    <input class="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                    <input name="upper_finish_spine" type="text" class="textbox scoringValue" />
                    <div id="scoringImages" class="scoring-single-img">
                        <div class="final-img-area">
                            <img src="http://placehold.it/350x150" class="final-img">
                        </div>
                        <div class="img-bundle">
                            <?php echo $this->Html->image('golf/facebook.png', array('class' => 'img-1'));?>
                            <?php echo $this->Html->image('golf/twitter.png', array('class' => 'img-2'));?>
                        </div>
                    </div>
                </div>
                <textarea name="upper_finish_spine_note" class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <!--/Upper Body Follow Finish SPINE TILT BOX -->

        <!--HEAD BOX -->
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">HEAD</h4>
            <div class="scoring-body">
                <div class=benchmarking>
                    <input class="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                    <input name="upper_finish_head" type="text" class="textbox scoringValue" />
                    <div id="scoringImages" class="scoring-single-img">
                        <div class="final-img-area">
                            <img src="http://placehold.it/350x150" class="final-img">
                        </div>
                        <div class="img-bundle">
                            <?php echo $this->Html->image('golf/facebook.png', array('class' => 'img-1'));?>
                            <?php echo $this->Html->image('golf/twitter.png', array('class' => 'img-2'));?>
                        </div>
                    </div>
                </div>
                <textarea name="upper_finish_head_note" class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <!--/Upper Body Follow Finish HEAD BOX -->

    </div>
</div>
<div class="margin-top-25"></div>
<h2 class="heading-sm u-p">Connection Halfway Back:</h2>
<hr class="shadow-line">
<div class="container-fluid">
    <div class="row">

        <!--Connection Halfway TRIANGLE SHAPE BOX -->
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">TRIANGLE SHAPE</h4>
            <div class="scoring-body">
                <div class=benchmarking>
                    <input class="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                    <input name="upper_halfway_back_triangle" type="text" class="textbox scoringValue" />
                    <div id="scoringImages" class="scoring-single-img">
                        <div class="final-img-area">
                            <img src="http://placehold.it/350x150" class="final-img">
                        </div>
                        <div class="img-bundle">
                            <?php echo $this->Html->image('golf/facebook.png', array('class' => 'img-1'));?>
                            <?php echo $this->Html->image('golf/twitter.png', array('class' => 'img-2'));?>
                        </div>
                    </div>
                </div>
                <textarea name="upper_halfway_back_triangle_note" class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <!--/Connection Halfway TRIANGLE SHAPE BOX -->

        <!--Connection Halfway ARMS POSITION BOX -->
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">ARMS POSITION</h4>
            <div class="scoring-body">
                <div class=benchmarking>
                    <input class="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                    <input name="upper_halfway_back_arms" type="text" class="textbox scoringValue" />
                    <div id="scoringImages" class="scoring-single-img">
                        <div class="final-img-area">
                            <img src="http://placehold.it/350x150" class="final-img">
                        </div>
                        <div class="img-bundle">
                            <?php echo $this->Html->image('golf/facebook.png', array('class' => 'img-1'));?>
                            <?php echo $this->Html->image('golf/twitter.png', array('class' => 'img-2'));?>
                        </div>
                    </div>
                </div>
                <textarea name="upper_halfway_back_arms_note" class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <!--/Connection Halfway ARMS POSITION BOX -->


    </div>
</div>
<div class="margin-top-25"></div>
<h2 class="heading-sm u-p">Connection ¾ Swing:</h2>
<hr class="shadow-line">
<div class="container-fluid">
    <div class="row">
        <!--Connection ¾ Swing TRIANGLE SHAPE BOX -->
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">TRIANGLE SHAPE</h4>
            <div class="scoring-body">
                <div class=benchmarking>
                    <input class="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                    <input name="upper_swing_triangle" type="text" class="textbox scoringValue" />
                    <div id="scoringImages" class="scoring-single-img">
                        <div class="final-img-area">
                            <img src="http://placehold.it/350x150" class="final-img">
                        </div>
                        <div class="img-bundle">
                            <?php echo $this->Html->image('golf/facebook.png', array('class' => 'img-1'));?>
                            <?php echo $this->Html->image('golf/twitter.png', array('class' => 'img-2'));?>
                        </div>
                    </div>
                </div>
                <textarea name="upper_swing_triangle_note" class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <!--/Connection ¾ Swing TRIANGLE SHAPE BOX -->

        <!--Connection ¾ Swing ARMS POSITION BOX -->
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">ARMS POSITION</h4>
            <div class="scoring-body">
                <div class=benchmarking>
                    <input class="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                    <input name="upper_swing_arms" type="text" class="textbox scoringValue" />
                    <div id="scoringImages" class="scoring-single-img">
                        <div class="final-img-area">
                            <img src="http://placehold.it/350x150" class="final-img">
                        </div>
                        <div class="img-bundle">
                            <?php echo $this->Html->image('golf/facebook.png', array('class' => 'img-1'));?>
                            <?php echo $this->Html->image('golf/twitter.png', array('class' => 'img-2'));?>
                        </div>
                    </div>
                </div>
                <textarea name="upper_swing_arms_note" class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <!--/Connection ¾ Swing ARMS POSITION BOX -->
    </div>
</div>
<div class="margin-top-25"></div>
<h2 class="heading-sm u-p">Connection At Top:</h2>
<hr class="shadow-line">
<div class="container-fluid">
    <div class="row">
        <!--Connection At Top TRIANGLE SHAPE BOX -->
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">TRIANGLE SHAPE</h4>
            <div class="scoring-body">
                <div class=benchmarking>
                    <input class="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                    <input name="upper_at_top_triangle" type="text" class="textbox scoringValue" />
                    <div id="scoringImages" class="scoring-single-img">
                        <div class="final-img-area">
                            <img src="http://placehold.it/350x150" class="final-img">
                        </div>
                        <div class="img-bundle">
                            <?php echo $this->Html->image('golf/facebook.png', array('class' => 'img-1'));?>
                            <?php echo $this->Html->image('golf/twitter.png', array('class' => 'img-2'));?>
                        </div>
                    </div>
                </div>
                <textarea name="upper_at_top_triangle_note" class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <!--/Connection At Top TRIANGLE SHAPE BOX -->

        <!--Connection At Top ARMS POSITION BOX -->
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">ARMS POSITION</h4>
            <div class="scoring-body">
                <div class=benchmarking>
                    <input class="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                    <input name="upper_at_top_arms" type="text" class="textbox scoringValue" />
                    <div id="scoringImages" class="scoring-single-img">
                        <div class="final-img-area">
                            <img src="http://placehold.it/350x150" class="final-img">
                        </div>
                        <div class="img-bundle">
                            <?php echo $this->Html->image('golf/facebook.png', array('class' => 'img-1'));?>
                            <?php echo $this->Html->image('golf/twitter.png', array('class' => 'img-2'));?>
                        </div>
                    </div>
                </div>
                <textarea name="upper_at_top_arms_note" class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <!--/Connection At Top ARMS POSITION BOX -->
    </div>
</div>
<div class="margin-top-25"></div>
<h2 class="heading-sm u-p">Connection Downswing:</h2>
<hr class="shadow-line">
<div class="container-fluid">
    <div class="row">
        <!--Connection Downswing TRIANGLE SHAPE BOX -->
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">TRIANGLE SHAPE</h4>
            <div class="scoring-body">
                <div class=benchmarking>
                    <input class="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                    <input name="upper_downswing_triangle" type="text" class="textbox scoringValue" />
                    <div id="scoringImages" class="scoring-single-img">
                        <div class="final-img-area">
                            <img src="http://placehold.it/350x150" class="final-img">
                        </div>
                        <div class="img-bundle">
                            <?php echo $this->Html->image('golf/facebook.png', array('class' => 'img-1'));?>
                            <?php echo $this->Html->image('golf/twitter.png', array('class' => 'img-2'));?>
                        </div>
                    </div>
                </div>
                <textarea name="upper_downswing_triangle_note" class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <!--/Connection Downswing TRIANGLE SHAPE BOX -->

        <!--Connection Downswing ARMS POSITION BOX -->
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">ARMS POSITION</h4>
            <div class="scoring-body">
                <div class=benchmarking>
                    <input class="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                    <input name="upper_downswing_arms" type="text" class="textbox scoringValue" />
                    <div id="scoringImages" class="scoring-single-img">
                        <div class="final-img-area">
                            <img src="http://placehold.it/350x150" class="final-img">
                        </div>
                        <div class="img-bundle">
                            <?php echo $this->Html->image('golf/facebook.png', array('class' => 'img-1'));?>
                            <?php echo $this->Html->image('golf/twitter.png', array('class' => 'img-2'));?>
                        </div>
                    </div>
                </div>
                <textarea name="upper_downswing_arms_note" class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <!--/Connection Downswing ARMS POSITION BOX -->
    </div>
</div>
<div class="margin-top-25"></div>
<h2 class="heading-sm u-p">Connection Impact: </h2>
<hr class="shadow-line">
<div class="container-fluid">
    <div class="row">
        <!--Connection Impact TRIANGLE SHAPE BOX -->
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">TRIANGLE SHAPE</h4>
            <div class="scoring-body">
                <div class=benchmarking>
                    <input class="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                    <input name="upper_impact_triangle" type="text" class="textbox scoringValue" />
                    <div id="scoringImages" class="scoring-single-img">
                        <div class="final-img-area">
                            <img src="http://placehold.it/350x150" class="final-img">
                        </div>
                        <div class="img-bundle">
                            <?php echo $this->Html->image('golf/facebook.png', array('class' => 'img-1'));?>
                            <?php echo $this->Html->image('golf/twitter.png', array('class' => 'img-2'));?>
                        </div>
                    </div>
                </div>
                <textarea name="upper_impact_triangle_note" class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <!--/Connection Impact TRIANGLE SHAPE BOX -->

        <!--Connection Impact ARMS POSITION BOX -->
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">ARMS POSITION</h4>
            <div class="scoring-body">
                <div class=benchmarking>
                    <input class="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                    <input name="upper_impact_arms" type="text" class="textbox scoringValue" />
                    <div id="scoringImages" class="scoring-single-img">
                        <div class="final-img-area">
                            <img src="http://placehold.it/350x150" class="final-img">
                        </div>
                        <div class="img-bundle">
                            <?php echo $this->Html->image('golf/facebook.png', array('class' => 'img-1'));?>
                            <?php echo $this->Html->image('golf/twitter.png', array('class' => 'img-2'));?>
                        </div>
                    </div>
                </div>
                <textarea name="upper_impact_arms_note" class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <!--/Connection Impact ARMS POSITION BOX -->
    </div>
</div>
<div class="margin-top-25"></div>
<h2 class="heading-sm u-p">Connection Follow Through:  </h2>
<hr class="shadow-line">
<div class="container-fluid">
    <div class="row">
        <!--Connection Follow Through TRIANGLE SHAPE BOX -->
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">TRIANGLE SHAPE</h4>
            <div class="scoring-body">
                <div class=benchmarking>
                    <input class="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                    <input name="upper_through_triangle" type="text" class="textbox scoringValue" />
                    <div id="scoringImages" class="scoring-single-img">
                        <div class="final-img-area">
                            <img src="http://placehold.it/350x150" class="final-img">
                        </div>
                        <div class="img-bundle">
                            <?php echo $this->Html->image('golf/facebook.png', array('class' => 'img-1'));?>
                            <?php echo $this->Html->image('golf/twitter.png', array('class' => 'img-2'));?>
                        </div>
                    </div>
                </div>
                <textarea name="upper_through_triangle_note" class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <!--/Connection Follow Through TRIANGLE SHAPE BOX -->

        <!--Connection Follow Through ARMS POSITION BOX -->
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">ARMS POSITION</h4>
            <div class="scoring-body">
                <div class=benchmarking>
                    <input class="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                    <input name="upper_through_arms" type="text" class="textbox scoringValue" />
                    <div id="scoringImages" class="scoring-single-img">
                        <div class="final-img-area">
                            <img src="http://placehold.it/350x150" class="final-img">
                        </div>
                        <div class="img-bundle">
                            <?php echo $this->Html->image('golf/facebook.png', array('class' => 'img-1'));?>
                            <?php echo $this->Html->image('golf/twitter.png', array('class' => 'img-2'));?>
                        </div>
                    </div>
                </div>
                <textarea name="upper_through_arms_note" class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <!--/Connection Follow Through ARMS POSITION BOX -->
    </div>
</div>
<div class="margin-top-25"></div>
<h2 class="heading-sm u-p">Connection Finish:  </h2>
<hr class="shadow-line">
<div class="container-fluid">
    <div class="row">
        <!--Connection Finish TRIANGLE SHAPE BOX -->
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">TRIANGLE SHAPE</h4>
            <div class="scoring-body">
                <div class=benchmarking>
                    <input class="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                    <input name="upper_finish_triangle" type="text" class="textbox scoringValue" />
                    <div id="scoringImages" class="scoring-single-img">
                        <div class="final-img-area">
                            <img src="http://placehold.it/350x150" class="final-img">
                        </div>
                        <div class="img-bundle">
                            <?php echo $this->Html->image('golf/facebook.png', array('class' => 'img-1'));?>
                            <?php echo $this->Html->image('golf/twitter.png', array('class' => 'img-2'));?>
                        </div>
                    </div>
                </div>
                <textarea name="upper_finish_triangle_note" class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <!--/Connection Finish TRIANGLE SHAPE BOX -->

        <!--Connection Finish ARMS POSITION BOX -->
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">ARMS POSITION</h4>
            <div class="scoring-body">
                <div class=benchmarking>
                    <input class="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                    <input name="upper_finish_arms" type="text" class="textbox scoringValue" />
                    <div id="scoringImages" class="scoring-single-img">
                        <div class="final-img-area">
                            <img src="http://placehold.it/350x150" class="final-img">
                        </div>
                        <div class="img-bundle">
                            <?php echo $this->Html->image('golf/facebook.png', array('class' => 'img-1'));?>
                            <?php echo $this->Html->image('golf/twitter.png', array('class' => 'img-2'));?>
                        </div>
                    </div>
                </div>
                <textarea name="upper_finish_arms_note" class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <!--/Connection Finish ARMS POSITION BOX -->
    </div>
</div>
<div class="margin-top-25"></div>
<h2 class="heading-sm u-p">Swing Plane Halfway Back: </h2>
<hr class="shadow-line">
<div class="container-fluid">
    <div class="row">

        <!--Swing Plane Halfway Back BOX -->
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">CLUB SHAFT POSITION</h4>
            <div class="scoring-body">
                <div class=benchmarking>
                    <input class="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                    <input name="upper_halfway_back_club_shaft" type="text" class="textbox scoringValue" />
                    <div id="scoringImages" class="scoring-single-img">
                        <div class="final-img-area">
                            <img src="http://placehold.it/350x150" class="final-img">
                        </div>
                        <div class="img-bundle">
                            <?php echo $this->Html->image('golf/facebook.png', array('class' => 'img-1'));?>
                            <?php echo $this->Html->image('golf/twitter.png', array('class' => 'img-2'));?>
                        </div>
                    </div>
                </div>
                <textarea name="upper_halfway_back_club_shaft_note" class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <!--/Swing Plane Halfway Back BOX -->

    </div>
</div>
<div class="margin-top-25"></div>
<h2 class="heading-sm u-p">Swing Plane ¾ Swing:  </h2>
<hr class="shadow-line">
<div class="container-fluid">
    <div class="row">

        <!--Swing Plane ¾ Swing BOX -->
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">CLUB SHAFT POSITION</h4>
            <div class="scoring-body">
                <div class=benchmarking>
                    <input class="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                    <input name="upper_swing_club_shaft" type="text" class="textbox scoringValue" />
                    <div id="scoringImages" class="scoring-single-img">
                        <div class="final-img-area">
                            <img src="http://placehold.it/350x150" class="final-img">
                        </div>
                        <div class="img-bundle">
                            <?php echo $this->Html->image('golf/facebook.png', array('class' => 'img-1'));?>
                            <?php echo $this->Html->image('golf/twitter.png', array('class' => 'img-2'));?>
                        </div>
                    </div>
                </div>
                <textarea name="upper_swing_club_shaft_note" class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <!--/Swing Plane ¾ Swing BOX -->

    </div>
</div>
<div class="margin-top-25"></div>
<h2 class="heading-sm u-p">Swing Plane at Top:  </h2>
<hr class="shadow-line">
<div class="container-fluid">
    <div class="row">

        <!--Swing Plane at Top BOX -->
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">CLUB SHAFT POSITION</h4>
            <div class="scoring-body">
                <div class=benchmarking>
                    <input class="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                    <input name="upper_at_top_club_shaft" type="text" class="textbox scoringValue" />
                    <div id="scoringImages" class="scoring-single-img">
                        <div class="final-img-area">
                            <img src="http://placehold.it/350x150" class="final-img">
                        </div>
                        <div class="img-bundle">
                            <?php echo $this->Html->image('golf/facebook.png', array('class' => 'img-1'));?>
                            <?php echo $this->Html->image('golf/twitter.png', array('class' => 'img-2'));?>
                        </div>
                    </div>
                </div>
                <textarea name="upper_at_top_club_shaft_note" class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <!--/Swing Plane at Top BOX -->

    </div>
</div>
<div class="margin-top-25"></div>
<h2 class="heading-sm u-p">Swing Plane Downswing: </h2>
<hr class="shadow-line">
<div class="container-fluid">
    <div class="row">

        <!--Swing Plane Downswing BOX -->
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">CLUB SHAFT POSITION</h4>
            <div class="scoring-body">
                <div class=benchmarking>
                    <input class="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                    <input name="upper_downswing_club_shaft" type="text" class="textbox scoringValue" />
                    <div id="scoringImages" class="scoring-single-img">
                        <div class="final-img-area">
                            <img src="http://placehold.it/350x150" class="final-img">
                        </div>
                        <div class="img-bundle">
                            <?php echo $this->Html->image('golf/facebook.png', array('class' => 'img-1'));?>
                            <?php echo $this->Html->image('golf/twitter.png', array('class' => 'img-2'));?>
                        </div>
                    </div>
                </div>
                <textarea name="upper_downswing_club_shaft_note" class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <!--/Swing Plane Downswing BOX -->

    </div>
</div>
<div class="margin-top-25"></div>
<h2 class="heading-sm u-p">Swing Plane Impact: </h2>
<hr class="shadow-line">
<div class="container-fluid">
    <div class="row">

        <!--Swing Plane Impact BOX -->
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">CLUB SHAFT POSITION</h4>
            <div class="scoring-body">
                <div class=benchmarking>
                    <input class="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                    <input name="upper_impact_club_shaft" type="text" class="textbox scoringValue" />
                    <div id="scoringImages" class="scoring-single-img">
                        <div class="final-img-area">
                            <img src="http://placehold.it/350x150" class="final-img">
                        </div>
                        <div class="img-bundle">
                            <?php echo $this->Html->image('golf/facebook.png', array('class' => 'img-1'));?>
                            <?php echo $this->Html->image('golf/twitter.png', array('class' => 'img-2'));?>
                        </div>
                    </div>
                </div>
                <textarea name="upper_impact_club_shaft_note" class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <!--/Swing Plane Impact BOX -->

    </div>
</div>
<div class="margin-top-25"></div>
<h2 class="heading-sm u-p">Swing Plane Follow Through: </h2>
<hr class="shadow-line">
<div class="container-fluid">
    <div class="row">

        <!--Swing Plane Follow Through BOX -->
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">CLUB SHAFT POSITION</h4>
            <div class="scoring-body">
                <div class=benchmarking>
                    <input class="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                    <input name="upper_through_club_shaft" type="text" class="textbox scoringValue" />
                    <div id="scoringImages" class="scoring-single-img">
                        <div class="final-img-area">
                            <img src="http://placehold.it/350x150" class="final-img">
                        </div>
                        <div class="img-bundle">
                            <?php echo $this->Html->image('golf/facebook.png', array('class' => 'img-1'));?>
                            <?php echo $this->Html->image('golf/twitter.png', array('class' => 'img-2'));?>
                        </div>
                    </div>
                </div>
                <textarea name="upper_through_club_shaft_note" class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <!--/Swing Plane Follow Through BOX -->

    </div>
</div>
<div class="margin-top-25"></div>
<h2 class="heading-sm u-p">Swing Plane Finish: </h2>
<hr class="shadow-line">
<div class="container-fluid">
    <div class="row">


        <!--Swing Plane Finish BOX -->
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">CLUB SHAFT POSITION</h4>
            <div class="scoring-body">
                <div class=benchmarking>
                    <input class="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                    <input name="upper_finish_club_shaft" type="text" class="textbox scoringValue" />
                    <div id="scoringImages" class="scoring-single-img">
                        <div class="final-img-area">
                            <img src="http://placehold.it/350x150" class="final-img">
                        </div>
                        <div class="img-bundle">
                            <?php echo $this->Html->image('golf/facebook.png', array('class' => 'img-1'));?>
                            <?php echo $this->Html->image('golf/twitter.png', array('class' => 'img-2'));?>
                        </div>
                    </div>
                </div>
                <textarea name="upper_finish_club_shaft_note" class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <!--/Swing Plane Finish BOX -->

    </div>
</div>
<div class="margin-top-25"></div>
<h2 class="heading-sm u-p">Club Face Halfway Back: </h2>
<hr class="shadow-line">
<div class="container-fluid">
    <div class="row">

        <!--TITLE BOX -->
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">Club Face  Position</h4>
            <div class="scoring-body">
                <div class=benchmarking>
                    <input class="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                    <input name="upper_halfway_back_club_face" type="text" class="textbox scoringValue" />
                    <div id="scoringImages" class="scoring-single-img">
                        <div class="final-img-area">
                            <img src="http://placehold.it/350x150" class="final-img">
                        </div>
                        <div class="img-bundle">
                            <?php echo $this->Html->image('golf/facebook.png', array('class' => 'img-1'));?>
                            <?php echo $this->Html->image('golf/twitter.png', array('class' => 'img-2'));?>
                        </div>
                    </div>
                </div>
                <textarea name="upper_halfway_back_club_face_note" class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <!--/TITLE BOX -->

    </div>
</div>
<div class="margin-top-25"></div>
<h2 class="heading-sm u-p">Club Face ¾ Swing:</h2>
<hr class="shadow-line">
<div class="container-fluid">
    <div class="row">
        <!--Club Face ¾ Swing BOX -->
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">Club Face  Position</h4>
                <div class="scoring-body">
                    <div class=benchmarking>
                        <input class="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                        <input name="upper_swing_club_face" type="text" class="textbox scoringValue" />
                        <div id="scoringImages" class="scoring-single-img">
                            <div class="final-img-area">
                                <img src="http://placehold.it/350x150" class="final-img">
                            </div>
                            <div class="img-bundle">
                                <?php echo $this->Html->image('golf/facebook.png', array('class' => 'img-1'));?>
                                <?php echo $this->Html->image('golf/twitter.png', array('class' => 'img-2'));?>
                            </div>
                        </div>
                    </div>
                    <textarea name="upper_swing_club_face_note" class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
                </div>
        </div>
        <!--/Club Face ¾ Swing BOX -->
    </div>
</div>
<div class="margin-top-25"></div>
<h2 class="heading-sm u-p">Club Face at Top: </h2>
<hr class="shadow-line">
<div class="container-fluid">
    <div class="row">
        <!--Club Face at Top BOX -->
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">Club Face  Position</h4>
                <div class="scoring-body">
                    <div class=benchmarking>
                        <input class="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                        <input name="upper_at_top_club_face" type="text" class="textbox scoringValue" />
                        <div id="scoringImages" class="scoring-single-img">
                            <div class="final-img-area">
                                <img src="http://placehold.it/350x150" class="final-img">
                            </div>
                            <div class="img-bundle">
                                <?php echo $this->Html->image('golf/facebook.png', array('class' => 'img-1'));?>
                                <?php echo $this->Html->image('golf/twitter.png', array('class' => 'img-2'));?>
                            </div>
                        </div>
                    </div>
                    <textarea name="upper_at_top_club_face_note" class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
                </div>
        </div>
        <!--/Club Face at Top BOX -->
    </div>
</div>
<div class="margin-top-25"></div>
<h2 class="heading-sm u-p">Club Face Downswing: </h2>
<hr class="shadow-line">
<div class="container-fluid">
    <div class="row">
        <!--Club Face Downswing BOX -->
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">Club Face  Position</h4>
                <div class="scoring-body">
                    <div class=benchmarking>
                        <input class="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                        <input name="upper_downswing_club_face" type="text" class="textbox scoringValue" />
                        <div id="scoringImages" class="scoring-single-img">
                            <div class="final-img-area">
                                <img src="http://placehold.it/350x150" class="final-img">
                            </div>
                            <div class="img-bundle">
                                <?php echo $this->Html->image('golf/facebook.png', array('class' => 'img-1'));?>
                                <?php echo $this->Html->image('golf/twitter.png', array('class' => 'img-2'));?>
                            </div>
                        </div>
                    </div>
                    <textarea name="upper_downswing_club_face_note" class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
                </div>
        </div>
        <!--/Club Face Downswing BOX -->
    </div>
</div>
<div class="margin-top-25"></div>
<h2 class="heading-sm u-p">Club Face Impact: </h2>
<hr class="shadow-line">
<div class="container-fluid">
    <div class="row">
        <!--Club Face Impact BOX -->
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">Club Face  Position</h4>
                <div class="scoring-body">
                    <div class=benchmarking>
                        <input class="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                        <input name="upper_impact_club_face" type="text" class="textbox scoringValue" />
                        <div id="scoringImages" class="scoring-single-img">
                            <div class="final-img-area">
                                <img src="http://placehold.it/350x150" class="final-img">
                            </div>
                            <div class="img-bundle">
                                <?php echo $this->Html->image('golf/facebook.png', array('class' => 'img-1'));?>
                                <?php echo $this->Html->image('golf/twitter.png', array('class' => 'img-2'));?>
                            </div>
                        </div>
                    </div>
                    <textarea name="upper_impact_club_face_note" class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
                </div>
        </div>
        <!--/Club Face Impact BOX -->
    </div>
</div>
<div class="margin-top-25"></div>
<h2 class="heading-sm u-p">Club Face Follow Through: </h2>
<hr class="shadow-line">
<div class="container-fluid">
    <div class="row">
        <!--Club Face Follow Through BOX -->
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">Club Face  Position</h4>
                <div class="scoring-body">
                    <div class=benchmarking>
                        <input class="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                        <input name="upper_through_club_face" type="text" class="textbox scoringValue" />
                        <div id="scoringImages" class="scoring-single-img">
                            <div class="final-img-area">
                                <img src="http://placehold.it/350x150" class="final-img">
                            </div>
                            <div class="img-bundle">
                                <?php echo $this->Html->image('golf/facebook.png', array('class' => 'img-1'));?>
                                <?php echo $this->Html->image('golf/twitter.png', array('class' => 'img-2'));?>
                            </div>
                        </div>
                    </div>
                    <textarea name="upper_through_club_face_note" class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
                </div>
        </div>
        <!--/Club Face Follow Through BOX -->
    </div>
</div>
<div class="margin-top-25"></div>
<h2 class="heading-sm u-p">Club Face Follow Finish: </h2>
<hr class="shadow-line">
<div class="container-fluid">
    <div class="row">
        <!--Club Face Follow Finish BOX -->
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">Club Face  Position</h4>
                <div class="scoring-body">
                    <div class=benchmarking>
                        <input class="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                        <input name="upper_finish_club_face" type="text" class="textbox scoringValue" />
                        <div id="scoringImages" class="scoring-single-img">
                            <div class="final-img-area">
                                <img src="http://placehold.it/350x150" class="final-img">
                            </div>
                            <div class="img-bundle">
                                <?php echo $this->Html->image('golf/facebook.png', array('class' => 'img-1'));?>
                                <?php echo $this->Html->image('golf/twitter.png', array('class' => 'img-2'));?>
                            </div>
                        </div>
                    </div>
                    <textarea name="upper_finish_club_face_note" class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
                </div>
        </div>
        <!--/Club Face Follow Finish BOX -->
    </div>
</div>
<div class="margin-top-25"></div>
<h2 class="heading-sm u-p">Wrist Cock ¾ Swing: </h2>
<hr class="shadow-line">
<div class="container-fluid">
    <div class="row">
        <!--Wrist Cock ¾ Wrist Cock -->
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">Wrist Cock</h4>
                <div class="scoring-body">
                    <div class=benchmarking>
                        <input class="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                        <input name="wrist_cock_swing" type="text" class="textbox scoringValue" />
                        <div id="scoringImages" class="scoring-single-img">
                            <div class="final-img-area">
                                <img src="http://placehold.it/350x150" class="final-img">
                            </div>
                            <div class="img-bundle">
                                <?php echo $this->Html->image('golf/facebook.png', array('class' => 'img-1'));?>
                                <?php echo $this->Html->image('golf/twitter.png', array('class' => 'img-2'));?>
                            </div>
                        </div>
                    </div>
                    <textarea name="wrist_cock_swing_note" class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
                </div>
        </div>
        <!--/Wrist Cock ¾ Wrist Cock -->
    </div>
</div>
<div class="margin-top-25"></div>
<h2 class="heading-sm u-p">Wrist Cock at Top:  </h2>
<hr class="shadow-line">
<div class="container-fluid">
    <div class="row">
        <!--Wrist Cock at Top BOX -->
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">Wrist Cock</h4>
                <div class="scoring-body">
                    <div class=benchmarking>
                        <input class="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                        <input name="wrist_cock_at_top" type="text" class="textbox scoringValue" />
                        <div id="scoringImages" class="scoring-single-img">
                            <div class="final-img-area">
                                <img src="http://placehold.it/350x150" class="final-img">
                            </div>
                            <div class="img-bundle">
                                <?php echo $this->Html->image('golf/facebook.png', array('class' => 'img-1'));?>
                                <?php echo $this->Html->image('golf/twitter.png', array('class' => 'img-2'));?>
                            </div>
                        </div>
                    </div>
                    <textarea name="wrist_cock_at_top_note" class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
                </div>
        </div>
        <!--/Wrist Cock at Top BOX -->
    </div>
</div>
<div class="margin-top-25"></div>
<h2 class="heading-sm u-p">Downswing Sequence:</h2>
<hr class="shadow-line">
<div class="container-fluid">
    <div class="row">

        <!--Downswing Sequence Upper Body Position BOX -->
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">Upper Body Position</h4>
                <div class="scoring-body">
                    <div class=benchmarking>
                        <input class="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                        <input name="sequence_downswing_upper_body" type="text" class="textbox scoringValue" />
                        <div id="scoringImages" class="scoring-single-img">
                            <div class="final-img-area">
                                <img src="http://placehold.it/350x150" class="final-img">
                            </div>
                            <div class="img-bundle">
                                <?php echo $this->Html->image('golf/facebook.png', array('class' => 'img-1'));?>
                                <?php echo $this->Html->image('golf/twitter.png', array('class' => 'img-2'));?>
                            </div>
                        </div>
                    </div>
                    <textarea name="sequence_downswing_upper_body_note" class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
                </div>
        </div>
        <!--/Downswing Sequence Upper Body Position BOX -->

        <!--Downswing Sequence lower Body Position BOX -->
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">Lower Body Position</h4>
            <div class="scoring-body">
                <div class=benchmarking>
                    <input class="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                    <input name="sequence_downswing_lower_body" type="text" class="textbox scoringValue" />
                    <div id="scoringImages" class="scoring-single-img">
                        <div class="final-img-area">
                            <img src="http://placehold.it/350x150" class="final-img">
                        </div>
                        <div class="img-bundle">
                            <?php echo $this->Html->image('golf/facebook.png', array('class' => 'img-1'));?>
                            <?php echo $this->Html->image('golf/twitter.png', array('class' => 'img-2'));?>
                        </div>
                    </div>
                </div>
                <textarea name="sequence_downswing_lower_body_note" class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <!--/Downswing Sequence Lower Body Position BOX -->

    </div>
</div>
<div class="margin-top-25"></div>
<h2 class="heading-sm u-p">Hands/Club Position Impact: </h2>
<hr class="shadow-line">
<div class="container-fluid">
    <div class="row">

        <!--Hands/Club Position Impact HAND POSITION ROTATION Box -->
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">HAND POSITION ROTATION</h4>
            <div class="scoring-body">
                <div class=benchmarking>
                    <input class="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                    <input name="club_position_impact_hand_rotation" type="text" class="textbox scoringValue" />
                    <div id="scoringImages" class="scoring-single-img">
                        <div class="final-img-area">
                            <img src="http://placehold.it/350x150" class="final-img">
                        </div>
                        <div class="img-bundle">
                            <?php echo $this->Html->image('golf/facebook.png', array('class' => 'img-1'));?>
                            <?php echo $this->Html->image('golf/twitter.png', array('class' => 'img-2'));?>
                        </div>
                    </div>
                </div>
                <textarea name="club_position_impact_hand_rotation_note" class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <!--/Hands/Club Position Impact HAND POSITION ROTATION BOX -->

        <!--Hands/Club Position Impact HAND POSITION LOCATION Box -->
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">HAND POSITION LOCATION</h4>
            <div class="scoring-body">
                <div class=benchmarking>
                    <input class="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                    <input name="club_position_impact_hand_location" type="text" class="textbox scoringValue" />
                    <div id="scoringImages" class="scoring-single-img">
                        <div class="final-img-area">
                            <img src="http://placehold.it/350x150" class="final-img">
                        </div>
                        <div class="img-bundle">
                            <?php echo $this->Html->image('golf/facebook.png', array('class' => 'img-1'));?>
                            <?php echo $this->Html->image('golf/twitter.png', array('class' => 'img-2'));?>
                        </div>
                    </div>
                </div>
                <textarea name="club_position_impact_hand_location_note" class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <!--/Hands/Club Position Impact HAND POSITION LOCATION BOX -->

        <!--Hands/Club Position Impact TEMPO ON BACKSWING Box -->
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">TEMPO ON BACKSWING</h4>
            <div class="scoring-body">
                <div class=benchmarking>
                    <input class="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                    <input name="club_position_impact_tempo_backswing" type="text" class="textbox scoringValue" />
                    <div id="scoringImages" class="scoring-single-img">
                        <div class="final-img-area">
                            <img src="http://placehold.it/350x150" class="final-img">
                        </div>
                        <div class="img-bundle">
                            <?php echo $this->Html->image('golf/facebook.png', array('class' => 'img-1'));?>
                            <?php echo $this->Html->image('golf/twitter.png', array('class' => 'img-2'));?>
                        </div>
                    </div>
                </div>
                <textarea name="club_position_impact_tempo_backswing_note" class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <!--/Hands/Club Position Impact  TEMPO ON BACKSWING BOX -->

        <!--Hands/Club Position Impact TEMPO ON DOWNSWING Box -->
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">TEMPO ON DOWNSWING</h4>
            <div class="scoring-body">
                <div class=benchmarking>
                    <input class="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                    <input name="club_position_impact_tempo_downswing" type="text" class="textbox scoringValue" />
                    <div id="scoringImages" class="scoring-single-img">
                        <div class="final-img-area">
                            <img src="http://placehold.it/350x150" class="final-img">
                        </div>
                        <div class="img-bundle">
                            <?php echo $this->Html->image('golf/facebook.png', array('class' => 'img-1'));?>
                            <?php echo $this->Html->image('golf/twitter.png', array('class' => 'img-2'));?>
                        </div>
                    </div>
                </div>
                <textarea name="club_position_impact_tempo_downswing_note" class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <!--/Hands/Club Position Impact TEMPO ON DOWNSWING BOX -->

        <!--Hands/Club Position Impact LAG Box -->
        <div class="col-md-3 col-sm-6 scoring-box">
            <h4 class="scoring-title u-p">LAG</h4>
            <div class="scoring-body">
                <div class=benchmarking>
                    <input class="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                    <input name="club_position_impact_lag" type="text" class="textbox scoringValue" />
                    <div id="scoringImages" class="scoring-single-img">
                        <div class="final-img-area">
                            <img src="http://placehold.it/350x150" class="final-img">
                        </div>
                        <div class="img-bundle">
                            <?php echo $this->Html->image('golf/facebook.png', array('class' => 'img-1'));?>
                            <?php echo $this->Html->image('golf/twitter.png', array('class' => 'img-2'));?>
                        </div>
                    </div>
                </div>
                <textarea name="club_position_impact_lag_note" class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
            </div>
        </div>
        <!--/Hands/Club Position Impact LAG BOX -->

    </div>
</div>
</div>
</div>
<!---- /Upper Body Stability ----->

<!---- Scoring Area Widget ----->
<div class="widget black-widget">
    <div class="widget-header u-p">
        <i class="fa fa-bar-chart-o"></i> Scoring Areas of Training
    </div>
    <div class="widget-body padding-15">
        <h2 class="heading-sm u-p">Scoring Areas of Training</h2>
        <hr class="shadow-line">
        <div class="container-fluid">
            <div class="row">

                <!--PUTTING BOX -->
                <div class="col-md-3 col-sm-6 scoring-box">
                    <h4 class="scoring-title u-p">PUTTING</h4>
                    <div class="scoring-body">
                        <div class=benchmarking>
                            <input class="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                            <input name="scoring_putting" type="text" class="textbox scoringValue" />
                            <div id="scoringImages" class="scoring-single-img">
                                <div class="final-img-area">
                                    <img src="http://placehold.it/350x150" class="final-img">
                                </div>
                                <div class="img-bundle">
                                    <?php echo $this->Html->image('golf/facebook.png', array('class' => 'img-1'));?>
                                    <?php echo $this->Html->image('golf/twitter.png', array('class' => 'img-2'));?>
                                </div>
                            </div>
                        </div>
                        <textarea name="scoring_putting_note" class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
                    </div>
                </div>
                <!--/PUTTING BOX -->

                <!--CHIPPING BOX -->
                <div class="col-md-3 col-sm-6 scoring-box">
                    <h4 class="scoring-title u-p">CHIPPING</h4>
                    <div class="scoring-body">
                        <div class=benchmarking>
                            <input class="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                            <input name="scoring_chipping" type="text" class="textbox scoringValue" />
                            <div id="scoringImages" class="scoring-single-img">
                                <div class="final-img-area">
                                    <img src="http://placehold.it/350x150" class="final-img">
                                </div>
                                <div class="img-bundle">
                                    <?php echo $this->Html->image('golf/facebook.png', array('class' => 'img-1'));?>
                                    <?php echo $this->Html->image('golf/twitter.png', array('class' => 'img-2'));?>
                                </div>
                            </div>
                        </div>
                        <textarea name="scoring_chipping_note" class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
                    </div>
                </div>
                <!--/CHIPPING BOX -->

                <!--PITCHING BOX -->
                <div class="col-md-3 col-sm-6 scoring-box">
                    <h4 class="scoring-title u-p">PITCHING</h4>
                    <div class="scoring-body">
                        <div class=benchmarking>
                            <input class="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                            <input name="scoring_pitching" type="text" class="textbox scoringValue" />
                            <div id="scoringImages" class="scoring-single-img">
                                <div class="final-img-area">
                                    <img src="http://placehold.it/350x150" class="final-img">
                                </div>
                                <div class="img-bundle">
                                    <?php echo $this->Html->image('golf/facebook.png', array('class' => 'img-1'));?>
                                    <?php echo $this->Html->image('golf/twitter.png', array('class' => 'img-2'));?>
                                </div>
                            </div>
                        </div>
                        <textarea name="scoring_pitching_note" class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
                    </div>
                </div>
                <!--/PITCHING BOX -->

                <!--DISTANCE WEDGE BOX -->
                <div class="col-md-3 col-sm-6 scoring-box">
                    <h4 class="scoring-title u-p">DISTANCE WEDGE</h4>
                    <div class="scoring-body">
                        <div class=benchmarking>
                            <input class="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                            <input name="scoring_distance_wedge" type="text" class="textbox scoringValue" />
                            <div id="scoringImages" class="scoring-single-img">
                                <div class="final-img-area">
                                    <img src="http://placehold.it/350x150" class="final-img">
                                </div>
                                <div class="img-bundle">
                                    <?php echo $this->Html->image('golf/facebook.png', array('class' => 'img-1'));?>
                                    <?php echo $this->Html->image('golf/twitter.png', array('class' => 'img-2'));?>
                                </div>
                            </div>
                        </div>
                        <textarea name="scoring_distance_wedge_note" class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
                    </div>
                </div>
                <!--/DISTANCE WEDGE BOX -->

                <!--SAND PLAY UNEVEN LIES BOX -->
                <div class="col-md-3 col-sm-6 scoring-box">
                    <h4 class="scoring-title u-p">SAND PLAY UNEVEN LIES</h4>
                    <div class="scoring-body">
                        <div class=benchmarking>
                            <input class="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                            <input name="scoring_sand_play" type="text" class="textbox scoringValue" />
                            <div id="scoringImages" class="scoring-single-img">
                                <div class="final-img-area">
                                    <img src="http://placehold.it/350x150" class="final-img">
                                </div>
                                <div class="img-bundle">
                                    <?php echo $this->Html->image('golf/facebook.png', array('class' => 'img-1'));?>
                                    <?php echo $this->Html->image('golf/twitter.png', array('class' => 'img-2'));?>
                                </div>
                            </div>
                        </div>
                        <textarea name="scoring_sand_play_note" class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
                    </div>
                </div>
                <!--/SAND PLAY UNEVEN LIES BOX -->

            </div>
        </div>
        <div class="margin-top-25"></div>
        <h2 class="heading-sm u-p">Course Management Mental Game:</h2>
        <hr class="shadow-line">
        <div class="container-fluid">
            <div class="row">

                <!--PAYING STYLE BOX -->
                <div class="col-md-3 col-sm-6 scoring-box">
                    <h4 class="scoring-title u-p">PAYING STYLE</h4>
                    <div class="scoring-body">
                        <div class=benchmarking>
                            <input class="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                            <input name="course_management_paying_style" type="text" class="textbox scoringValue" />
                            <div id="scoringImages" class="scoring-single-img">
                                <div class="final-img-area">
                                    <img src="http://placehold.it/350x150" class="final-img">
                                </div>
                                <div class="img-bundle">
                                    <?php echo $this->Html->image('golf/facebook.png', array('class' => 'img-1'));?>
                                    <?php echo $this->Html->image('golf/twitter.png', array('class' => 'img-2'));?>
                                </div>
                            </div>
                        </div>
                        <textarea name="course_management_paying_style_note" class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
                    </div>
                </div>
                <!--/PAYING STYLE BOX -->

                <!--MIND SET BOX -->
                <div class="col-md-3 col-sm-6 scoring-box">
                    <h4 class="scoring-title u-p">MIND SET</h4>
                    <div class="scoring-body">
                        <div class=benchmarking>
                            <input class="uiSlider" data-slider-handle="triangle"  data-slider-min="0" data-slider-value="100"  data-slider-max="200"  type="text" /><br/>
                            <input name="course_management_mind_set" type="text" class="textbox scoringValue" />
                            <div id="scoringImages" class="scoring-single-img">
                                <div class="final-img-area">
                                    <img src="http://placehold.it/350x150" class="final-img">
                                </div>
                                <div class="img-bundle">
                                    <?php echo $this->Html->image('golf/facebook.png', array('class' => 'img-1'));?>
                                    <?php echo $this->Html->image('golf/twitter.png', array('class' => 'img-2'));?>
                                </div>
                            </div>
                        </div>
                        <textarea name="course_management_mind_set_note" class="form-control" rows="5"  placeholder="Add Notes Here"></textarea>
                    </div>
                </div>
                <!--/MIND SET BOX -->

            </div>
        </div>
    </div>
</div>
<!---- Scoring Area Widget ----->

<button class="btn btn-theme">Submit Benchmark</button>
<br/>
<br/>
<?php echo $this->Form->end();?>

<script>
    /* ------------ SINGLE BOX ------*/
    $(document).ready(function() {

        $('.uiSlider').slider().on('slide', function() {
            var value = $(this).data('slider').getValue();
            var benchmarking = $(this).closest(".benchmarking");

            if(value > 100){
                var scoring = (200 - value);
            }
            else{
                var scoring = value;
            }

            myColor = getTheColor( scoring );
            var imageCount = $(benchmarking).find(".img-bundle img").length;
            var divider = 100 / imageCount;
            var counter = 1;
            var displayImg ;

            console.log(imageCount);
            console.log(divider);

            while(counter <= imageCount)
            {
                if((scoring > (divider * counter)) && scoring < (divider * (counter + 1))){
                    var imageID = counter + 1;
                    displayImg = $(benchmarking).find(".img-"+imageID).attr('src');
                }
                else if(scoring < divider){
                    displayImg = $(benchmarking).find(".img-1").attr('src');
                }
                counter ++;

            }
            $(benchmarking).find('.final-img').attr("src", displayImg);
            $(benchmarking).find('.scoringValue').val(scoring);
            $(benchmarking).find('.scoringValue').css( 'background-color', myColor );
            convertImgToBase64(displayImg, function(base64Img){
                $(benchmarking).find('.benchmark_img').val(base64Img);
            });

        });

        $('.scoringValue').val(value);
        $('.scoringValue').css( 'background-color', getTheColor( value ) );
    });
    /* ------------ SINGLE BOX ------*/



    function getTheColor( colorVal ) {
        var theColor = "";
        if ( colorVal < 50 ) {
            myRed = 255;
            myGreen = parseInt( ( ( colorVal * 2 ) * 255 ) / 100 );
        }
        else 	{
            myRed = parseInt( ( ( 100 - colorVal ) * 2 ) * 255 / 100 );
            myGreen = 255;
        }
        theColor = "rgb(" + myRed + "," + myGreen + ",0)";
        return( theColor );
    }


    function convertImgToBase64(url, callback, outputFormat){
        var canvas = document.createElement('CANVAS');
        var ctx = canvas.getContext('2d');
        var img = new Image;
        img.crossOrigin = 'Anonymous';
        img.onload = function(){
            canvas.height = img.height;
            canvas.width = img.width;
            ctx.drawImage(img,0,0);
            var dataURL = canvas.toDataURL(outputFormat || 'image/png');
            callback.call(this, dataURL);
            // Clean up
            canvas = null;
        };
        img.src = url;
    }

</script>
