<?php
/**
 * @var $this View
 */
?>
<h2 class="page-title">Assign Lesson</h2>
<hr class="shadow-line"/>
<div class="row">
    <div class="col-md-8 col-md-offset-2 col-sm-12">
        <div class="box box-padding-adding radius-5 margin-top-25 margin-bottom-25">
            <?php echo $this->Form->create(
                'Lesson',
                array(
                    'controller' => 'lessons',
                    'action' => 'assign',
                    'type' => 'file',
                    'method' => 'post',
                    'class' => 'form-horizontal form-custom'
                )
            ); ?>
            <div class="form-group">
                <label class="col-sm-4 control-label">Student</label>

                <div class="col-sm-8">
                    <?php
                    echo $this->Form->input(
                        'Lesson.created_for',
                        array(
                            'type' => 'select',
                            'class' => 'form-control',
                            'label' => false,
                            'div' => false,
                            'required' => false,
                            'options' => $studentList,
                            'empty' => 'Select Student'
                        )
                    )
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4 control-label">Lesson</label>

                <div class="col-sm-8">
                    <?php
                    echo $this->Form->input(
                        'Lesson.lesson_id',
                        array(
                            'type' => 'select',
                            'class' => 'form-control',
                            'label' => false,
                            'div' => false,
                            'multiple' => true,
                            'required' => false,
                            'options' => $lessons,
                        )
                    )
                    ?>
                </div>
            </div>


            <div class="form-group">
                <label class="col-sm-4 control-label">Starting From</label>

                <div class="col-sm-5">
                    <?php
                    echo $this->Form->input(
                        'Lesson.starting_time',
                        array(
                            'type' => 'select',
                            'class' => 'form-control',
                            'label' => false,
                            'div' => false,
                            'required' => false,
                            'empty' => 'Select the starting hour',
                            'options' => $this->Utilities->getHours()
                        )
                    )
                    ?>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-4 control-label">Data and Time</label>

                <div class="col-sm-6">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                        <input id="datepicker" name="data[Lesson][schedule_date]" type="text" class="form-control"
                               placeholder="02-25-2014">
                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                    </div>
                    <?php echo $this->Form->error('Lesson.schedule_date'); ?>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-12">
                    <button class="btn btn-theme pull-right">Assign Lesson</button>
                </div>
            </div>
            <?php echo $this->Form->end(); ?>
        </div>
    </div>
</div>