<?php
/**
 * @var $this View
 */
?>
<h2 class="page-title">List of Lesson</h2>
<hr class="shadow-line"/>
<div class="dropdown margin-bottom-5">
    <button class="btn btn-info dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown">
        Quick Navigation
        <span class="caret"></span>
    </button>
    <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
        <li role="presentation">
            <?php echo $this->Html->link('All (' .$allLesson. ')', array('controller' => 'lessons', 'action' => 'list'));?>
        </li>
        <li role="presentation" class="divider"></li>
        <li role="presentation">
            <?php echo $this->Html->link('New List (' .$newLesson. ')', array('controller' => 'lessons', 'action' => 'list', 'new'));?>
        </li>
        <li role="presentation" class="divider"></li>
        <li role="presentation">
            <?php echo $this->Html->link('Waiting List (' .$waitingLesson. ')', array('controller' => 'lessons', 'action' => 'list', 'waiting'));?>
        </li>
        <li role="presentation" class="divider"></li>
        <li role="presentation">
            <?php echo $this->Html->link('Upcoming List (' .$upcomingLesson. ')', array('controller' => 'lessons', 'action' => 'list', 'upcoming'));?>
        </li>
        <li role="presentation" class="divider"></li>
        <li role="presentation">
            <?php echo $this->Html->link('Completed List (' .$completedLesson. ')', array('controller' => 'lessons', 'action' => 'list', 'completed'));?>
        </li>
        <li role="presentation" class="divider"></li>
        <li role="presentation">
            <?php echo $this->Html->link('Paid List (' .$paidLesson. ')', array('controller' => 'lessons', 'action' => 'list', 'paid'));?>
        </li>
        <li role="presentation" class="divider"></li>
        <li role="presentation">
            <?php echo $this->Html->link('Unpaid List (' .$unpaidLesson. ')', array('controller' => 'lessons', 'action' => 'list', 'unpaid'));?>
        </li>
    </ul>
</div>
<div class="table-responsive">
<?php if ($lessonList): ?>
    <table class="table data-table table-bordered" colspecing=0>
    <thead>
    <tr>
        <th>Title</th>
        <th style="width:7%;">Duration</th>
        <th style="width: 15%;">Time</th>
        <th style="width: 15%;">Created For</th>
        <th style="width: 8%;">Price</th>
        <th style="width: 13%;">Status</th>
        <th style="width: 10%;">Payment</th>
        <th style="width: 10%;">Action</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach($lessonList as $lesson):?>
    <tr>
        <td>
            <?php echo $this->Html->link($this->Text->excerpt($lesson['Lesson']['title'], 'method', 30, '...'), array('controller' => 'lessons', 'action' => 'view', $lesson['Lesson']['uuid']), array('class' => 'green'));?>
        </td>
        <td><?php echo $lesson['Lesson']['duration'];?> Hours</td>
        <td>
            <?php echo $this->Time->format('M d, Y', $lesson['Lesson']['schedule_date']);?>
            <i class="italic-sm">(<?php echo $lesson['Lesson']['starting_time'];?>)</i>
        </td>
        <td>
            <?php echo $this->Html->link($lesson['User']['Profile']['first_name']. ' '. $lesson['User']['Profile']['last_name'], array('controller' => 'users', 'action' => 'details', $lesson['User']['uuid']));?>
        </td>
        <td>
            $<?php echo $lesson['Lesson']['price']?>
        </td>
        <td>
            <?php $this->Utilities->manageLessonStatus($lesson['Lesson']['status']); ?>
            <div class="margin-top-5">
                <?php
                if($lesson['Benchmark']['uuid']):
                    echo $this->Html->link('view benchmark?', array('controller' => 'benchmarks', 'action' => 'view', $lesson['Benchmark']['uuid']), array('class' => 'd-btn'));
                endif;
                ?>
            </div>
        </td>
        <td>
            <?php if ($lesson['Lesson']['payment'] == 1): ?>
                <label class="label label-danger">unpaid</label>
            <?php elseif ($lesson['Lesson']['payment'] == 2): ?>
                <label class="label label-success">paid</label>
            <?php endif; ?>
        </td>
        <td>
            <div class="icons-area">
                <?php echo $this->Html->link('<i class="fa fa-pencil"></i>', array('controller' => 'lessons', 'action' => 'edit', $lesson['Lesson']['uuid']), array('escape' => false, 'class' => 'icon primary'));?>

                <?php echo $this->Html->link('<i class="fa fa-search-plus"></i>', array('controller' => 'lessons', 'action' => 'view', $lesson['Lesson']['uuid']), array('escape' => false, 'class' => 'icon info'));?>

                <?php
                echo $this->Form->postLink(
                    '<i class="glyphicon glyphicon-remove"></i>',
                    array(
                        'controller' => 'lessons',
                        'action' => 'delete',
                        $lesson['Lesson']['uuid']
                    ),
                    array(
                        'class' => 'icon danger',
                        'escape' => false
                    ),
                    __('Are you sure you want to delete this lesson')
                );
                ?>
            </div>
        </td>
    </tr>
    <?php endforeach;?>
    </tbody>
    </table>
    <?php echo $this->element('pagination');?>
    <?php else: ?>
    <h2 class="not-found">Sorry, lesson not found</h2>
    <hr class="shadow-line"/>
<?php endif; ?>
</div>
<div class="clearfix"></div>