<?php
/**
 * @var $this View
 */
?>
<h2 class="page-title">List of Group Lesson</h2>
<hr class="shadow-line"/>
<div class="page-option">
    <div class="pull-right page-option-right">
        <div class="btn-toolbar" role="toolbar" aria-label="...">
            <button type="button" class="btn btn-theme btn-lg btn-group" data-toggle="modal" data-target="#searchGroupLessonModal">
                Search Group Lesson
            </button>
        </div>
    </div>
</div>
<br>
<br>

<div class="table-responsive">
    <?php if ($lessons): ?>
        <table class="table data-table table-bordered sort-table" colspecing=0>
            <thead>
            <tr>
                <th><?php echo $this->Paginator->sort('GroupLesson.title', 'Title'); ?></th>
                <th><?php echo $this->Paginator->sort('0.instructorName', 'Instructor Name'); ?></th>
                <th><?php echo $this->Paginator->sort('GroupLesson.price', 'Price'); ?></th>
                <th><?php echo $this->Paginator->sort('GroupLesson.start', 'Time'); ?></th>
                <th><?php echo $this->Paginator->sort('GroupLesson.student_limit', 'Student Limit'); ?></th>
                <th><?php echo $this->Paginator->sort('GroupLesson.filled_up_student', 'Student Filled up'); ?></th>
                <th>Available</th>
                <th>Status</th>
                <th style="width: 10%;">Action</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($lessons as $lesson):?>
                <tr>
                    <td>
                        <?php echo $this->Html->link($this->Text->excerpt($lesson['GroupLesson']['title'], 'method', 30, '...'), array('controller' => 'lessons', 'action' => 'details', $lesson['GroupLesson']['id']), array('class' => 'green'));?>
                    </td>
                    <td>
                        <?php echo $this->Html->link($lesson[0]['instructorName'], array('controller' => 'users', 'action' => 'details', $lesson['InstructorUser']['uuid'])); ?>

                    </td>
                    <td>
                        <?php echo $this->Number->currency($lesson['GroupLesson']['price']); ?>
                    </td>
                    <td>
                        <?php echo $this->Time->format('M d, Y', $lesson['GroupLesson']['start']);?>
                        <span class="italic-sm">( <?php echo $this->Time->format('h:m a', $lesson['GroupLesson']['start'])?> - <?php echo $this->Time->format('h:m a', $lesson['GroupLesson']['end'])?>)</span>
                    </td>
                    <td>
                        <?php echo $lesson['GroupLesson']['student_limit'];?>
                    </td>
                    <td>
                        <?php echo $lesson['GroupLesson']['filled_up_student'];?>
                    </td>
                    <td>
                        <?php echo $lesson['GroupLesson']['student_limit'] - $lesson['GroupLesson']['filled_up_student'];?>
                    </td>
                    <td>
                        <?php
                        if($lesson['GroupLesson']['status']){
                            echo '<label class="label label-success">Active</label>';
                        }
                        else{
                            echo '<label class="label label-danger">Inactive</label>';
                        }
                        ?>
                    </td>

                    <td>
                        <div class="icons-area">
                            <div class="icons-area">
                                <?php echo $this->Html->link('view details', array('controller' => 'lessons', 'action' => 'details', $lesson['GroupLesson']['id']), array('escape' => false, 'class' => 'd-btn'));?>
                            </div>
                        </div>
                    </td>
                </tr>
            <?php endforeach;?>
            </tbody>
        </table>
        <?php echo $this->element('pagination');?>
    <?php else: ?>
        <h2 class="not-found">Sorry, group lesson not found</h2>
        <hr class="shadow-line"/>
    <?php endif; ?>
</div>
<div class="clearfix"></div>
<!-- Modal For Displaying Schedule -->
<div class="modal event-modal fade" id="searchGroupLessonModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="border-bottom: 1px solid #e5e5e5;">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                </button>
                <h4 class="sm-title">Search Group Lesson</h4>
            </div>
            <div class="modal-body">
                <!----- ADD NEW CONTACT NOTE ------>
                <div class="">
                    <form action="<?php echo Router::url('/', true); ?>tc/lessons/group" class="form-horizontal form-custom" method="get" accept-charset="utf-8">
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label>Instructor name:</label>
                                <select class="form-control form-inline" name="instructor[]" multiple>
                                    <option value="" selected="selected">Select Instructor</option>
                                    <?php foreach ($instructors as $instructor): ?>
                                        <option value="<?php echo $instructor['User']['id']; ?>">
                                            <?php echo $instructor[0]['instructorName']; ?>
                                        </option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12">
                                <label>Group lesson Name:</label>
                                <input name="lesson_name" class="form-control" type="text"
                                       placeholder="Type a lesson name">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12">
                                <label>Group Lesson Amount</label>
                                <select name="price" class="form-control">
                                    <option value="" selected="selected">Choose Amount</option>
                                    <option value="100">$100+</option>
                                    <option value="500">$500+</option>
                                    <option value="1000">$1000+</option>
                                    <option value="2000">$2000+</option>
                                    <option value="5000">$5000+</option>
                                    <option value="10000">$10000+</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-12"><label>Lesson Amount Range</label></div>
                            <div class="col-lg-6">
                                <input name="starting_amount" class="form-control" type="text"
                                       placeholder="Starting Lesson Amount">
                            </div>
                            <div class="col-lg-6">
                                <input name="ending_amount" class="form-control" type="text"
                                       placeholder="Ending Lesson Amount">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label>Date</label>
                                <select name="date" class="form-control">
                                    <option value="" selected="selected">Choose Date</option>
                                    <option value="current_month">Current Month</option>
                                    <option value="last_month">Last Month</option>
                                    <option value="last_3_month">Last Three Month</option>
                                    <option value="last_6_month">Last Six Month</option>
                                    <option value="last_12_month">Last Year</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-lg-12"><label>Date Range</label></div>
                            <div class="clearfix"></div>
                            <div class="col-sm-6">
                                <div class="input-group">
                                    <input id="datepicker" name="starting" type="text" class="form-control" placeholder="Starting Date">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="input-group">
                                    <input id="datepicker2" name="ending" type="text" class="form-control" placeholder="Ending Date">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12">
                                <label>Student Limit</label>
                                <select name="student_limit" class="form-control">
                                    <option value="" selected="selected">Choose </option>
                                    <option value="5">5+</option>
                                    <option value="15">15+</option>
                                    <option value="25">25+</option>
                                    <option value="40">40+</option>
                                    <option value="50">50+</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-4">
                                <button class="btn btn-theme">Search</button>
                            </div>
                        </div>
                        <?php echo $this->Form->end(); ?>
                        <div class="clearfix"></div>
                </div>
                <!----- /ADD NEW CONTACT NOTE ------>
            </div>
        </div>
    </div>
</div>
<!-- /Modal For Displaying Schedule -->
