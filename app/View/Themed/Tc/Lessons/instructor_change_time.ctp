<?php
/**
 * @var $this View
 */
?>
<h2 class="page-title">Edit Lesson</h2>
<hr class="shadow-line"/>
<div class="row">
    <div class="col-md-8 col-md-offset-2 col-sm-12">
        <div class="box box-padding-adding radius-5 margin-top-25 margin-bottom-25">
            <?php echo $this->Form->create('Lesson', array('controller' => 'lessons', 'action' => 'change_time/'.$lessonDetails['Lesson']['uuid'], 'type' => 'file', 'method' => 'post', 'class' => 'form-horizontal form-custom'));?>
            <div class="form-group">
                <ul class="data-list">
                    <li>
                        <strong>Lesson Plan: </strong>
                        <?php echo $this->Html->link($lessonDetails['Lesson']['title'], array('controller' => 'lessons', 'action' => 'view', $lessonDetails['Lesson']['id']));?>
                    </li>
                    <li>
                        <strong>Description: </strong>
                        <p>
                            <?php echo $lessonDetails['Lesson']['description'];?>
                        </p>

                    </li>
                    <li>
                        <strong>Duration: </strong><span><?php echo $lessonDetails['Lesson']['duration']?> Hours</span>
                        <i class="italic-sm">(<?php echo $lessonDetails['Lesson']['starting_time'];?>)</i>
                    </li>
                    <li>
                        <strong>Date: </strong>
                        <time><?php echo $this->Time->format('M d, Y', $lessonDetails['Lesson']['schedule_date']);?></time>
                    </li>
                    <li>
                        <strong>Status</strong>
                        <?php if ($lessonDetails['Lesson']['status'] == 1): ?>
                            <label class="label label-info">active</label>
                        <?php elseif($lessonDetails['Lesson']['status'] == 2): ?>
                            <label class="label label-info">Completed</label>
                        <?php else: ?>
                            N/A
                        <?php endif; ?>
                    </li>
                    <?php if($lessonDetails['Attachment']): ?>
                        <li>
                            <div class="download-files">
                                <h2 class="sm-title u-p">Download Attachment</h2>
                                <?php foreach ($lessonDetails['Attachment'] as $attachment): ?>
                                    <a href="<?php echo Router::url('/', true) . '/files/attachment/attachment/' . $attachment['attachment_dir'] . '/' . $attachment['attachment']; ?>"
                                       download="<?php echo $attachment['attachment'];?>" title="<?php echo $attachment['attachment'];?>">
                                        <i class="fa fa-paperclip"></i>
                                        (<?php echo $attachment['attachment']; ?>)
                                    </a>
                                    <br/>
                                <?php endforeach; ?>
                            </div>
                        </li>
                    <?php endif; ?>
                </ul>
            </div>


            <div class="form-group">
                <label class="col-sm-4 control-label">New Time</label>
                <div class="col-sm-8">
                    <?php
                    echo $this->Form->input('Lesson.id', array('type' => 'hidden', 'value' => $lessonDetails['Lesson']['id']));
                    echo $this->Form->input(
                        'Lesson.starting_time',
                        array(
                            'type' => 'select',
                            'class' => 'form-control',
                            'label' => false,
                            'div' => false,
                            'required' => false,
                            'empty' => 'Select Time',
                            'options' => $this->Utilities->getHours()
                        )
                    )
                    ?>
                </div>
            </div>


            <div class="form-group">
                <div class="col-sm-12">
                    <button class="btn btn-theme pull-right">Change Time</button>
                </div>
            </div>
            <?php echo $this->Form->end();?>
        </div>
    </div>
</div>