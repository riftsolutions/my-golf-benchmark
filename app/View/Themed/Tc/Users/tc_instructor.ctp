<?php
/**
 * @var $this View
 */
?>
<h2 class="page-title">
    <?php if(isset($title)){
        echo $title;
    }
    else{
        echo 'instructor List';
    }
    ?>
</h2>
<hr class="shadow-line"/>

<div class="table-responsive">
    <?php if($userList):?>
        <table class="table data-table table-bordered sort-table" colspecing = 0>
        <thead>
        <tr>
            <th><?php echo $this->Paginator->sort('Profile.name', 'Name'); ?></th>
            <th><?php echo $this->Paginator->sort('User.username', 'Email/Username'); ?></th>
            <th><?php echo $this->Paginator->sort('Profile.phone', 'Phone'); ?></th>
            <th><?php echo $this->Paginator->sort('User.status', 'Status'); ?></th>
            <th><?php echo $this->Paginator->sort('User.created', 'Notes'); ?></th>
            <th><?php echo $this->Paginator->sort('User.last_login', 'Last Login'); ?></th>
            <th style="text-align: right; width: 7%;">Action</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach($userList as $users):?>
        <tr>
            <td>
                <?php echo $this->Html->link($users['Profile']['first_name'].' '.$users['Profile']['last_name'], array('controller' => 'users', 'action' => 'details', $users['User']['uuid']), array('class' => 'green'))?>
            </td>
            <td>
                <?php echo $this->Html->link($users['User']['username'], array('controller' => 'users', 'action' => 'details', $users['User']['uuid']));?>
            </td>
            <td>
                <?php if($users['Profile']['phone']):?>
                    <?php echo $this->Html->link($users['Profile']['phone'], array('controller' => 'users', 'action' => 'details', $users['User']['uuid']));?>
                <?php else:?>
                    N/A
                <?php endif;?>
            </td>
            <td>
                <?php echo $this->Utilities->getUserStatus($users['User']['status']);?>
            </td>
            <td>
                <?php echo $this->Html->link('see note', array('controller' => 'users', 'action' => 'details/' . $users['User']['uuid'] . '#note'), array('class' => 'd-btn'))?>
            </td>
            <td>
                <time>
                    <?php
                    if(!strstr('0000-00-00 00:00:00', $users['User']['last_login']) && $users['User']['last_login']){
                        echo $this->Time->timeAgoInWords($users['User']['last_login'],
                            array('accuracy' => array('month' => 'month'),'end' => '1 year'));
                    }
                    else{
                        echo '<strong class="danger">Never logged in</strong>';
                    }
                    ?>
                </time>
            </td>
            <td>
                <div class="icons-area" style="text-align: right;">
                    <?php echo $this->Html->link('<i class="fa fa-search-plus"></i>', array('controller' => 'users', 'action' => 'details', $users['User']['uuid']), array('escape' => false, 'class' => 'icon green'))?>
                    <?php echo $this->Html->link('<i class="fa fa-pencil"></i>', array('controller' => 'users', 'action' => 'edit', $users['User']['uuid']), array('escape' => false, 'class' => 'icon primary'))?>
                    <?php
                    echo $this->Form->postLink(
                        '<i class="glyphicon glyphicon-remove"></i>',
                        array(
                            'controller' => 'users',
                            'action' => 'changeStatus',
                            $users['User']['uuid'],
                            9
                        ),
                        array(
                            'class' => 'icon danger',
                            'escape' => false
                        ),
                        __('Are you sure you want to delete this user')
                    );
                    ?>
                </div>
            </td>
        </tr>
        <?php endforeach;?>
        </tbody>
    </table>
        <?php echo $this->element('pagination');?>
    <?php else:?>
        <h2 class="not-found">Sorry, instructor not found</h2>
    <?php endif;?>
</div>