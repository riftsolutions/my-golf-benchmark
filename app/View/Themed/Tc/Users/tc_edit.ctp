<?php
/**
 * @var $this View
 */
?>
<?php
if($userInformation['Profile']['is_custom_source'] == 1 || $this->Session->read('isCustomSource') == true){
    $defaultSource = 'style="display: none"';
    $customSource = 'style="display: visible"';

    $defaultSourceBtn = 'style="display: visible"';
    $customSourceBtn = 'style="display: none"';

    $customValue = 1;
    $cSourceValue = $userInformation['Profile']['source'];
}
else{
    $defaultSource = 'style="display: visible"';
    $customSource = 'style="display: none"';

    $defaultSourceBtn = 'style="display: none"';
    $customSourceBtn = 'style="display: visible"';
    $customValue = 0;
    $cSourceValue = '';
}
?>
<h2 class="page-title">Edit Instructor</h2>
<hr class="shadow-line"/>
<div class="row">
    <div class="col-md-8 col-md-offset-2 col-sm-12">
        <div class="box box-padding-adding radius-5 margin-top-25 margin-bottom-25">
            <?php echo $this->Form->create('User', array('controller' => 'users', 'action' => 'edit/'.$userInformation['User']['uuid'], 'class' => 'form-horizontal', 'method' => 'post'))?>
                <div class="form-group">
                    <label class="col-sm-4 control-label">First Name</label>
                    <div class="col-sm-8">
                        <?php echo $this->Form->input('Profile.first_name', array('type' => 'text', 'class' => 'form-control', 'required' => false, 'div' => false, 'label' => false, 'value' => $userInformation['Profile']['first_name']))?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label">Last Name</label>
                    <div class="col-sm-8">
                        <?php echo $this->Form->input('Profile.last_name', array('type' => 'text', 'class' => 'form-control', 'required' => false, 'div' => false, 'label' => false, 'value' => $userInformation['Profile']['last_name']))?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label">Email</label>
                    <div class="col-sm-8">
                        <?php echo $this->Form->input('User.username', array('type' => 'text', 'class' => 'form-control', 'required' => false, 'div' => false, 'label' => false, 'value' => $userInformation['User']['username']))?>
                    </div>
                </div>
            <div class="form-group">
                <label class="col-sm-4 control-label">Phone</label>
                <div class="col-sm-8">
                    <?php echo $this->Form->input('Profile.phone', array('type' => 'text', 'class' => 'form-control', 'required' => false, 'div' => false, 'label' => false, 'value' => $userInformation['Profile']['phone']))?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4 control-label">Street 1</label>
                <div class="col-sm-8">
                    <?php echo $this->Form->input('Profile.street_1', array('type' => 'text', 'class' => 'form-control', 'required' => false, 'div' => false, 'label' => false, 'value' => $userInformation['Profile']['street_1']))?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4 control-label">Street 2</label>
                <div class="col-sm-8">
                    <?php echo $this->Form->input('Profile.street_2', array('type' => 'text', 'class' => 'form-control', 'required' => false, 'div' => false, 'label' => false, 'value' => $userInformation['Profile']['street_2']))?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4 control-label">City</label>
                <div class="col-sm-8">
                    <?php echo $this->Form->input('Profile.city', array('type' => 'text', 'class' => 'form-control', 'required' => false, 'div' => false, 'label' => false, 'value' => $userInformation['Profile']['city']))?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4 control-label">State</label>
                <div class="col-sm-8">
                    <?php
                    echo $this->Form->input(
                        'Profile.state',
                        array(
                            'type' => 'select',
                            'class' => 'form-control',
                            'label' => false,
                            'div' => false,
                            'required' => false,
                            'options' => $this->Utilities->getStateList(),
                            'value' => $userInformation['Profile']['state']
                        )
                    );
                    ?>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-4 control-label">Postal Code</label>
                <div class="col-sm-8">
                    <?php echo $this->Form->input('Profile.postal_code', array('type' => 'text', 'class' => 'form-control', 'required' => false, 'div' => false, 'label' => false, 'value' => $userInformation['Profile']['postal_code']))?>
                </div>
            </div>


            <div class="form-group">
                <label class="col-sm-4 control-label">Source</label>
                <div class="col-sm-8">
                    <?php echo $this->Form->input('Profile.is_custom_source', array('type' => 'hidden', 'value' => $customValue, 'class' => 'is_custom'))?>
                    <div class="default-source" <?php echo $defaultSource;?>>
                        <?php
                        echo $this->Form->input(
                            'Profile.source',
                            array(
                                'type' => 'select',
                                'class' => 'form-control',
                                'label' => false,
                                'div' => false,
                                'required' => false,
                                'options' => $sources,
                                'empty' => 'Select Source',
                                'value' => $userInformation['Profile']['source_id']
                            )
                        );
                        ?>
                    </div>
                    <div <?php echo $customSource;?> class="custom-source">
                        <?php echo $this->Form->input('Profile.custom_source', array('type' => 'text', 'class' => 'form-control', 'required' => false, 'div' => false, 'label' => false, 'value' => $cSourceValue))?>
                    </div>
                    <a class="d-btn pull-right custom-source-btn" <?php echo $customSourceBtn?>>custom source?</a>
                    <a class="d-btn pull-right default-source-btn" <?php echo $defaultSourceBtn?> >back to source?</a>
                </div>
            </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label">Password</label>
                    <div class="col-sm-8">
                        <?php echo $this->Form->input('User.password', array('type' => 'password', 'class' => 'form-control', 'required' => false, 'div' => false, 'label' => false, 'placeholder' => 'Password'))?>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-12">
                        <?php echo $this->Form->button('Edit Instructor', array('class' => 'btn btn-theme pull-right'))?>
                    </div>
                </div>
            <?php echo $this->Form->end();?>
        </div>
    </div>
</div>