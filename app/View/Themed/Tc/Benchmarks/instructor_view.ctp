<?php
/**
 * @var $this View
 */
?>
<h2 class="page-title">Benchmark Details</h2>
<hr class="shadow-line"/>


<div class="scoring-tab">
    <ul id="myTab" class="nav nav-tabs">
        <li class="active">
            <a href="#general-info" data-toggle="tab" >General Info</a>
        </li>
        <li class="">
            <?php echo $this->Html->link('Edit Benchmark', array('controller' => 'benchmarks', 'action' => 'edit', $benchmarkDetails['Benchmark']['uuid']));?>
        </li>
        <li class="">
            <a href="#position" data-toggle="tab">Setup Position</a>
        </li>
        <li class="">
            <a href="#lower-body-stability" data-toggle="tab">Lower Body Stability</a>
        </li>
        <li class="">
            <a href="#upper-body-stability" data-toggle="tab">Upper Body Stability</a>
        </li>
        <li class="">
            <a href="#scoring-area" data-toggle="tab">Scoring Area</a>
        </li>



    </ul>
    <div id="myTabContent" class="tab-content">
        <!--- General Information --->
        <div class="tab-pane fade active in" id="general-info">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-md-12">
                    <div class="d-box-mini">
                        <?php //var_dump($benchmarkDetails['User']);?>
                        <h4 class="d-title"><i class="fa fa-user info"></i> Student Details</h4>
                        <ul class="d-list">
                            <li>
                                <strong>Name: </strong>
                                <?php echo $this->Html->link($benchmarkDetails['User']['Profile']['name'], array('controller' => 'users', 'action' => 'details', $benchmarkDetails['User']['uuid']))?>
                            </li>
                            <li>
                                <strong>Email: </strong>
                                <?php echo $this->Html->link($benchmarkDetails['User']['username'], array('controller' => 'users', 'action' => 'details', $benchmarkDetails['User']['uuid']))?>
                            </li>

                            <?php if($benchmarkDetails['User']['Profile']['phone']):?>
                                <li>
                                    <strong>Phone Number: </strong>
                                    <?php echo $benchmarkDetails['User']['Profile']['phone'];?>
                                </li>
                            <?php endif;?>

                            <?php if($benchmarkDetails['User']['Profile']['street_1']):?>
                                <li>
                                    <strong>Address: </strong>
                                    <?php echo $benchmarkDetails['User']['Profile']['street_1'];?>
                                </li>
                            <?php endif;?>

                            <?php if($benchmarkDetails['User']['Profile']['postal_code']):?>
                                <li>
                                    <strong>Postal Code: </strong>
                                    <?php echo $benchmarkDetails['User']['Profile']['postal_code'];?>
                                </li>
                            <?php endif;?>

                            <?php if($benchmarkDetails['User']['Profile']['city']):?>
                                <li>
                                    <strong>City: </strong>
                                    <?php echo $benchmarkDetails['User']['Profile']['city'];?>
                                </li>
                            <?php endif;?>

                            <?php if($benchmarkDetails['User']['Profile']['state']):?>
                                <li>
                                    <strong>State: </strong>
                                    <?php echo $benchmarkDetails['User']['Profile']['state'];?>
                                </li>
                            <?php endif;?>

                        </ul>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-md-12">
                    <div class="d-box-mini">
                        <h4 class="d-title"><i class="fa fa-flask slate-blue"></i> Package Details</h4>
                        <ul class="d-list">
                            <li>
                                <strong>Package Name: </strong>
                                <?php echo $this->Html->link($benchmarkDetails['Package']['name'], array('controller' => 'packages', 'action' => 'view', $benchmarkDetails['Package']['uuid']))?>
                            </li>
                            <li>
                                <strong>Number of Lesson: </strong>
                                <?php echo $benchmarkDetails['Package']['lesson'];?>
                            </li>
                            <li>
                                <strong>Net Price: </strong>
                                <?php echo $this->Number->currency($benchmarkDetails['Package']['price']); ?>
                            </li>
                            <li>
                                <strong>Tax: </strong>
                                <?php echo $this->Number->currency($benchmarkDetails['Package']['tax']); ?>
                            </li>
                            <li>
                                <strong>Total Price: </strong>
                                <?php echo $this->Number->currency($benchmarkDetails['Package']['total']); ?>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-md-12">
                    <div class="d-box-mini">
                        <?php echo $this->Form->create('Profile', array('controller' => 'profiles', 'action' => 'editProfile/'.$benchmarkDetails['User']['uuid'], 'class' => '', 'method' => 'post'))?>

                        <div class="row">
                            <div class="col-lg-4">
                                <label>Golf History & Goals</label>
                                <?php
                                echo $this->Form->input('Profile.history_and_goal', array('class' => 'form-control', 'rows' => 6, 'label' => false, 'value' => $benchmarkDetails['User']['Profile']['history_and_goal']));
                                ?>
                            </div>
                            <div class="col-lg-4">
                                <label>What day and times are best for you?</label>
                                <?php
                                echo $this->Form->input('Profile.best_day_and_time', array('class' => 'form-control', 'rows' => 6, 'label' => false, 'value' => $benchmarkDetails['User']['Profile']['best_day_and_time']));
                                ?>
                            </div>
                            <div class="col-lg-4">
                                <label>How Frequently can you come in (in weeks)?</label>
                                <?php
                                echo $this->Form->input('Profile.frequently_come', array('class' => 'form-control', 'type' => 'text', 'label' => false, 'value' => $benchmarkDetails['User']['Profile']['frequently_come']));
                                ?>

                                <h4 class="page-title margin-top-25">Handicap</h4>.

                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Starting</label>
                                    <div class="col-sm-8">
                                        <?php echo $this->Form->input('Profile.handicap_starting', array('type' => 'text', 'class' => 'form-control', 'required' => false, 'div' => false, 'label' => false, 'value' => $benchmarkDetails['User']['Profile']['handicap_starting']))?>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Current</label>
                                    <div class="col-sm-8">
                                        <?php echo $this->Form->input('Profile.handicap_current', array('type' => 'text', 'class' => 'form-control', 'required' => false, 'div' => false, 'label' => false, 'value' => $benchmarkDetails['User']['Profile']['handicap_current']))?>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Change</label>
                                    <div class="col-sm-8">
                                        <?php echo $this->Form->input('Profile.handicap_change', array('type' => 'text', 'class' => 'form-control', 'required' => false, 'div' => false, 'label' => false, 'value' => $benchmarkDetails['User']['Profile']['handicap_change']))?>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-4 control-label"></label>
                                    <div class="col-sm-8">
                                        <button class="btn btn-theme pull-right">Edit Information</button>
                                    </div>
                                </div>



                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!--- General Information --->

        <!--- Scoring Area --->
        <div class="tab-pane fade" id="scoring-area">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-md-12">
                    <div class="d-box-md d-box-benchmark">
                        <h4 class="d-title">Scoring area of Training:</h4>
                        <ul class="d-list">
                            <li>
                                <strong>Putting:</strong>
                                <strong class="grading">
                                    <?php echo $benchmarkDetails['Benchmark']['scoring_putting'];?>
                                </strong>
                            </li>
                            <li>
                                <strong>Rotation Note:</strong>
                                <?php echo $benchmarkDetails['Benchmark']['scoring_putting_note'];?>
                            </li>
                            <li>
                                <strong>Chapping:</strong>
                                <strong class="grading">
                                    <?php echo $benchmarkDetails['Benchmark']['scoring_chipping'];?>
                                </strong>
                            </li>
                            <li>
                                <strong>Rotation Note:</strong>
                                <?php echo $benchmarkDetails['Benchmark']['scoring_chipping_note'];?>
                            </li>
                            <li>
                                <strong>Pitching:</strong>
                                <strong class="grading">
                                    <?php echo $benchmarkDetails['Benchmark']['scoring_pitching'];?>
                                </strong>
                            </li>
                            <li>
                                <strong>Rotation Note:</strong>
                                <?php echo $benchmarkDetails['Benchmark']['scoring_pitching_note'];?>
                            </li>
                            <li>
                                <strong>Distance Wedge:</strong>
                                <strong class="grading">
                                    <?php echo $benchmarkDetails['Benchmark']['scoring_distance_wedge'];?>
                                </strong>
                            </li>
                            <li>
                                <strong>Rotation Note:</strong>
                                <?php echo $benchmarkDetails['Benchmark']['scoring_distance_wedge_note'];?>
                            </li>
                            <li>
                                <strong>Sand Play Uneven lies:</strong>
                                <strong class="grading">
                                    <?php echo $benchmarkDetails['Benchmark']['scoring_sand_play'];?>
                                </strong>
                            </li>
                            <li>
                                <strong>Sand Play Uneven lies:</strong>
                                <?php echo $benchmarkDetails['Benchmark']['scoring_sand_play_note'];?>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-md-12">
                    <div class="d-box-md d-box-benchmark">
                        <h4 class="d-title">COURSE MANAGEMENT MENTAL GAME:</h4>
                        <ul class="d-list">
                            <li>
                                <strong>Playing Style:</strong>
                                <strong class="grading">
                                    <?php echo $benchmarkDetails['Benchmark']['course_management_paying_style'];?>
                                </strong>
                            </li>
                            <li>
                                <strong>Playing Style Note:</strong>
                                <?php echo $benchmarkDetails['Benchmark']['course_management_paying_style_note'];?>
                            </li>
                            <li>
                                <strong>Mind Set:</strong>
                                <strong class="grading">
                                    <?php echo $benchmarkDetails['Benchmark']['course_management_mind_set'];?>
                                </strong>
                            </li>
                            <li>
                                <strong>Mind Set Note:</strong>
                                <?php echo $benchmarkDetails['Benchmark']['course_management_mind_set_note'];?>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!--- Scoring Area --->

        <!--- Position Area --->
        <div class="tab-pane fade" id="position">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12">

                    <div class="d-box-md d-box-benchmark">
                        <h4 class="d-title">Posture</h4>
                        <ul class="d-list">
                            <li>
                                <strong>Knee Posture:</strong>
                                <strong class="grading">
                                    <?php echo $benchmarkDetails['Benchmark']['posture_knee'];?>
                                </strong>
                            </li>
                            <li>
                                <strong>Knee Posture Note:</strong>
                                <?php echo $benchmarkDetails['Benchmark']['posture_knee_note'];?>
                            </li>
                            <li>
                                <strong>Back Posture:</strong>
                                <strong class="grading">
                                    <?php echo $benchmarkDetails['Benchmark']['posture_back'];?>
                                </strong>
                            </li>
                            <li>
                                <strong>Back Posture Note:</strong>
                                <?php echo $benchmarkDetails['Benchmark']['posture_back_note'];?>
                            </li>
                        </ul>
                    </div>

                    <div class="d-box-md d-box-benchmark">
                        <h4 class="d-title">Stance Width</h4>
                        <ul class="d-list">
                            <li>
                                <strong>Foot Width:</strong>
                                <strong class="grading">
                                    <?php echo $benchmarkDetails['Benchmark']['stance_foot_width'];?>
                                </strong>
                            </li>
                            <li>
                                <strong>Foot Width Note:</strong>
                                <?php echo $benchmarkDetails['Benchmark']['stance_foot_width_note'];?>
                            </li>
                            <li>
                                <strong>Foot Direction:</strong>
                                <strong class="grading">
                                    <?php echo $benchmarkDetails['Benchmark']['stance_foot_direction'];?>
                                </strong>
                            </li>
                            <li>
                                <strong>Foot Direction Note:</strong>
                                <?php echo $benchmarkDetails['Benchmark']['stance_foot_direction_note'];?>
                            </li>
                        </ul>
                    </div>

                    <div class="d-box-md d-box-benchmark">
                        <h4 class="d-title">Arm Position</h4>
                        <ul class="d-list">
                            <li>
                                <strong>Right Arm:</strong>
                                <strong class="grading">
                                    <?php echo $benchmarkDetails['Benchmark']['arm_position_right'];?>
                                </strong>
                            </li>
                            <li>
                                <strong>Right Arm Note:</strong>
                                <?php echo $benchmarkDetails['Benchmark']['arm_position_right_note'];?>
                            </li>
                            <li>
                                <strong>Left Arm:</strong>
                                <strong class="grading">
                                    <?php echo $benchmarkDetails['Benchmark']['arm_position_left'];?>
                                </strong>
                            </li>
                            <li>
                                <strong>Left Arm Note:</strong>
                                <?php echo $benchmarkDetails['Benchmark']['arm_position_left_note'];?>
                            </li>
                        </ul>
                    </div>

                    <div class="d-box-md d-box-benchmark">
                        <h4 class="d-title">Grip</h4>
                        <ul class="d-list">
                            <li>
                                <strong>Right Hand Grip:</strong>
                                <strong class="grading">
                                    <?php echo $benchmarkDetails['Benchmark']['grip_right_hand'];?>
                                </strong>
                            </li>
                            <li>
                                <strong>Right Hand Grip Note:</strong>
                                <?php echo $benchmarkDetails['Benchmark']['grip_right_hand_note'];?>
                            </li>
                            <li>
                                <strong>Left Hand Grip:</strong>
                                <strong class="grading">
                                    <?php echo $benchmarkDetails['Benchmark']['grip_left_hand'];?>
                                </strong>
                            </li>
                            <li>
                                <strong>Left Hand Grip Note:</strong>
                                <?php echo $benchmarkDetails['Benchmark']['grip_left_hand_note'];?>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12">

                    <div class="d-box-md d-box-benchmark">
                        <h4 class="d-title">Alignment</h4>
                        <ul class="d-list">
                            <li>
                                <strong>Feet Alignment:</strong>
                                <strong class="grading">
                                    <?php echo $benchmarkDetails['Benchmark']['alignment_feet'];?>
                                </strong>
                            </li>
                            <li>
                                <strong>Feet Alignment Note:</strong>
                                <?php echo $benchmarkDetails['Benchmark']['alignment_feet_note'];?>
                            </li>
                            <li>
                                <strong>Knee Alignment:</strong>
                                <strong class="grading">
                                    <?php echo $benchmarkDetails['Benchmark']['alignment_knee'];?>
                                </strong>
                            </li>
                            <li>
                                <strong>Knee Alignment Note:</strong>
                                <?php echo $benchmarkDetails['Benchmark']['alignment_knee_note'];?>
                            </li>
                            <li>
                                <strong>Hip Alignment:</strong>
                                <strong class="grading">
                                    <?php echo $benchmarkDetails['Benchmark']['alignment_hip'];?>
                                </strong>
                            </li>
                            <li>
                                <strong>Hip Alignment Note:</strong>
                                <?php echo $benchmarkDetails['Benchmark']['alignment_hip_note'];?>
                            </li>
                            <li>
                                <strong>Shoulder Alignment:</strong>
                                <strong class="grading">
                                    <?php echo $benchmarkDetails['Benchmark']['alignment_shoulder'];?>
                                </strong>
                            </li>
                            <li>
                                <strong>Shoulder Alignment Note:</strong>
                                <?php echo $benchmarkDetails['Benchmark']['alignment_shoulder_note'];?>
                            </li>
                            <li>
                                <strong>Arm Alignment:</strong>
                                <strong class="grading">
                                    <?php echo $benchmarkDetails['Benchmark']['alignment_arm'];?>
                                </strong>
                            </li>
                            <li>
                                <strong>Arm Alignment Note:</strong>
                                <?php echo $benchmarkDetails['Benchmark']['alignment_arm_note'];?>
                            </li>
                            <li>
                                <strong>Club Face Alignment:</strong>
                                <strong class="grading">
                                    <?php echo $benchmarkDetails['Benchmark']['alignment_club_face'];?>
                                </strong>
                            </li>
                            <li>
                                <strong>Club Face Alignment Note:</strong>
                                <?php echo $benchmarkDetails['Benchmark']['alignment_club_face_note'];?>
                            </li>
                        </ul>
                    </div>

                    <div class="d-box-md d-box-benchmark">
                        <h4 class="d-title">Ball Position</h4>
                        <ul class="d-list">
                            <li>
                                <strong>Distance Away:</strong>
                                <strong class="grading">
                                    <?php echo $benchmarkDetails['Benchmark']['ball_position_distance_away'];?>
                                </strong>
                            </li>
                            <li>
                                <strong>Distance Away Note:</strong>
                                <?php echo $benchmarkDetails['Benchmark']['ball_position_distance_away_note'];?>
                            </li>
                            <li>
                                <strong>Forward and Back:</strong>
                                <strong class="grading">
                                    <?php echo $benchmarkDetails['Benchmark']['ball_position_forward_and_back'];?>
                                </strong>
                            </li>
                            <li>
                                <strong>Forward and Back Note:</strong>
                                <?php echo $benchmarkDetails['Benchmark']['ball_position_forward_and_back_note'];?>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!--- /Position Area --->

        <!--- Lower Body Stability Area --->
        <div class="tab-pane fade" id="lower-body-stability">
            <div class="row">
                <div class="col-lg-6 col-sm-6 col-sm-12">
                    <div class="d-box-md d-box-benchmark">
                        <h4 class="d-title">Lower Body Halfway Back</h4>
                        <ul class="d-list">
                            <li>
                                <strong>Pear:</strong>
                                <strong class="grading">
                                    <?php echo $benchmarkDetails['Benchmark']['lower_halfway_back_pear'];?>
                                </strong>
                            </li>
                            <li>
                                <strong>Pear Note:</strong>
                                <?php echo $benchmarkDetails['Benchmark']['lower_halfway_back_pear_note'];?>
                            </li>
                            <li>
                                <strong>Knee Flex:</strong>
                                <strong class="grading">
                                    <?php echo $benchmarkDetails['Benchmark']['lower_halfway_back_knee_flex'];?>
                                </strong>
                            </li>
                            <li>
                                <strong>Knee Flex Note:</strong>
                                <?php echo $benchmarkDetails['Benchmark']['lower_halfway_back_knee_flex_note'];?>
                            </li>
                            <li>
                                <strong>Hip Sway:</strong>
                                <strong class="grading">
                                    <?php echo $benchmarkDetails['Benchmark']['lower_halfway_back_hip_sway'];?>
                                </strong>
                            </li>
                            <li>
                                <strong>Hip Sway Note:</strong>
                                <?php echo $benchmarkDetails['Benchmark']['lower_halfway_back_hip_sway_note'];?>
                            </li>
                            <li>
                                <strong>Hip Pivot:</strong>
                                <strong class="grading">
                                    <?php echo $benchmarkDetails['Benchmark']['lower_halfway_back_hip_pivot'];?>
                                </strong>
                            </li>
                            <li>
                                <strong>Hip Pivot Note:</strong>
                                <?php echo $benchmarkDetails['Benchmark']['lower_halfway_back_hip_pivot_note'];?>
                            </li>
                        </ul>
                    </div>

                    <div class="d-box-md d-box-benchmark">
                        <h4 class="d-title">Lower Body  ¾ SWING:</h4>
                        <ul class="d-list">
                            <li>
                                <strong>Pear:</strong>
                                <strong class="grading">
                                    <?php echo $benchmarkDetails['Benchmark']['lower_swing_pear'];?>
                                </strong>
                            </li>
                            <li>
                                <strong>Pear Note:</strong>
                                <?php echo $benchmarkDetails['Benchmark']['lower_swing_pear_note'];?>
                            </li>
                            <li>
                                <strong>Knee Flex:</strong>
                                <strong class="grading">
                                    <?php echo $benchmarkDetails['Benchmark']['lower_swing_knee_flex'];?>
                                </strong>
                            </li>
                            <li>
                                <strong>Knee Flex Note:</strong>
                                <?php echo $benchmarkDetails['Benchmark']['lower_swing_knee_flex_note'];?>
                            </li>
                            <li>
                                <strong>Hip Sway:</strong>
                                <strong class="grading">
                                    <?php echo $benchmarkDetails['Benchmark']['lower_swing_hip_sway'];?>
                                </strong>
                            </li>
                            <li>
                                <strong>Hip Sway Note:</strong>
                                <?php echo $benchmarkDetails['Benchmark']['lower_swing_hip_sway_note'];?>
                            </li>
                            <li>
                                <strong>Hip Pivot:</strong>
                                <strong class="grading">
                                    <?php echo $benchmarkDetails['Benchmark']['lower_swing_hip_pivot'];?>
                                </strong>
                            </li>
                            <li>
                                <strong>Hip Pivot Note:</strong>
                                <?php echo $benchmarkDetails['Benchmark']['lower_swing_hip_pivot_note'];?>
                            </li>
                        </ul>
                    </div>

                    <div class="d-box-md d-box-benchmark">
                        <h4 class="d-title">Lower Body  At Top:</h4>
                        <ul class="d-list">
                            <li>
                                <strong>Pear:</strong>
                                <strong class="grading">
                                    <?php echo $benchmarkDetails['Benchmark']['lower_at_top_pear'];?>
                                </strong>
                            </li>
                            <li>
                                <strong>Pear Note:</strong>
                                <?php echo $benchmarkDetails['Benchmark']['lower_at_top_pear_note'];?>
                            </li>
                            <li>
                                <strong>Knee Flex:</strong>
                                <strong class="grading">
                                    <?php echo $benchmarkDetails['Benchmark']['lower_at_top_knee_flex'];?>
                                </strong>
                            </li>
                            <li>
                                <strong>Knee Flex Note:</strong>
                                <?php echo $benchmarkDetails['Benchmark']['lower_at_top_knee_flex_note'];?>
                            </li>
                            <li>
                                <strong>Hip Sway:</strong>
                                <strong class="grading">
                                    <?php echo $benchmarkDetails['Benchmark']['lower_at_top_hip_sway'];?>
                                </strong>
                            </li>
                            <li>
                                <strong>Hip Sway Note:</strong>
                                <?php echo $benchmarkDetails['Benchmark']['lower_at_top_hip_sway_note'];?>
                            </li>
                            <li>
                                <strong>Hip Pivot:</strong>
                                <strong class="grading">
                                    <?php echo $benchmarkDetails['Benchmark']['lower_at_top_hip_pivot'];?>
                                </strong>
                            </li>
                            <li>
                                <strong>Hip Pivot Note:</strong>
                                <?php echo $benchmarkDetails['Benchmark']['lower_at_top_hip_pivot_note'];?>
                            </li>
                        </ul>
                    </div>

                    <div class="d-box-md d-box-benchmark">
                        <h4 class="d-title">Lower Body  Downswing:</h4>
                        <ul class="d-list">
                            <li>
                                <strong>Pear:</strong>
                                <strong class="grading">
                                    <?php echo $benchmarkDetails['Benchmark']['lower_downswing_pear'];?>
                                </strong>
                            </li>
                            <li>
                                <strong>Pear Note:</strong>
                                <?php echo $benchmarkDetails['Benchmark']['lower_downswing_pear_note'];?>
                            </li>
                            <li>
                                <strong>Knee Flex:</strong>
                                <strong class="grading">
                                    <?php echo $benchmarkDetails['Benchmark']['lower_downswing_knee_flex'];?>
                                </strong>
                            </li>
                            <li>
                                <strong>Knee Flex Note:</strong>
                                <?php echo $benchmarkDetails['Benchmark']['lower_downswing_knee_flex_note'];?>
                            </li>
                            <li>
                                <strong>Hip Sway:</strong>
                                <strong class="grading">
                                    <?php echo $benchmarkDetails['Benchmark']['lower_downswing_hip_sway'];?>
                                </strong>
                            </li>
                            <li>
                                <strong>Hip Sway Note:</strong>
                                <?php echo $benchmarkDetails['Benchmark']['lower_downswing_hip_sway_note'];?>
                            </li>
                            <li>
                                <strong>Hip Pivot:</strong>
                                <strong class="grading">
                                    <?php echo $benchmarkDetails['Benchmark']['lower_downswing_hip_pivot'];?>
                                </strong>
                            </li>
                            <li>
                                <strong>Hip Pivot Note:</strong>
                                <?php echo $benchmarkDetails['Benchmark']['lower_downswing_hip_pivot_note'];?>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-6 col-sm-6 col-sm-12">
                    <div class="d-box-md d-box-benchmark">
                        <h4 class="d-title">Lower Body Impact:</h4>
                        <ul class="d-list">
                            <li>
                                <strong>Pear:</strong>
                                <strong class="grading">
                                    <?php echo $benchmarkDetails['Benchmark']['lower_impact_pear'];?>
                                </strong>
                            </li>
                            <li>
                                <strong>Pear Note:</strong>
                                <?php echo $benchmarkDetails['Benchmark']['lower_impact_pear_note'];?>
                            </li>
                            <li>
                                <strong>Knee Flex:</strong>
                                <strong class="grading">
                                    <?php echo $benchmarkDetails['Benchmark']['lower_impact_knee_flex'];?>
                                </strong>
                            </li>
                            <li>
                                <strong>Knee Flex Note:</strong>
                                <?php echo $benchmarkDetails['Benchmark']['lower_impact_knee_flex_note'];?>
                            </li>
                            <li>
                                <strong>Hip Sway:</strong>
                                <strong class="grading">
                                    <?php echo $benchmarkDetails['Benchmark']['lower_impact_hip_sway'];?>
                                </strong>
                            </li>
                            <li>
                                <strong>Hip Sway Note:</strong>
                                <?php echo $benchmarkDetails['Benchmark']['lower_impact_hip_sway_note'];?>
                            </li>
                            <li>
                                <strong>Hip Pivot:</strong>
                                <strong class="grading">
                                    <?php echo $benchmarkDetails['Benchmark']['lower_impact_hip_pivot'];?>
                                </strong>
                            </li>
                            <li>
                                <strong>Hip Pivot Note:</strong>
                                <?php echo $benchmarkDetails['Benchmark']['lower_impact_hip_pivot_note'];?>
                            </li>
                        </ul>
                    </div>

                    <div class="d-box-md d-box-benchmark">
                        <h4 class="d-title">Lower Body FOLLOW THROUGH:</h4>
                        <ul class="d-list">
                            <li>
                                <strong>Pear:</strong>
                                <strong class="grading">
                                    <?php echo $benchmarkDetails['Benchmark']['lower_through_pear'];?>
                                </strong>
                            </li>
                            <li>
                                <strong>Pear Note:</strong>
                                <?php echo $benchmarkDetails['Benchmark']['lower_through_pear_note'];?>
                            </li>
                            <li>
                                <strong>Knee Flex:</strong>
                                <strong class="grading">
                                    <?php echo $benchmarkDetails['Benchmark']['lower_through_knee_flex'];?>
                                </strong>
                            </li>
                            <li>
                                <strong>Knee Flex Note:</strong>
                                <?php echo $benchmarkDetails['Benchmark']['lower_through_knee_flex_note'];?>
                            </li>
                            <li>
                                <strong>Hip Sway:</strong>
                                <strong class="grading">
                                    <?php echo $benchmarkDetails['Benchmark']['lower_through_hip_sway'];?>
                                </strong>
                            </li>
                            <li>
                                <strong>Hip Sway Note:</strong>
                                <?php echo $benchmarkDetails['Benchmark']['lower_through_hip_sway_note'];?>
                            </li>
                            <li>
                                <strong>Hip Pivot:</strong>
                                <strong class="grading">
                                    <?php echo $benchmarkDetails['Benchmark']['lower_through_hip_pivot'];?>
                                </strong>
                            </li>
                            <li>
                                <strong>Hip Pivot Note:</strong>
                                <?php echo $benchmarkDetails['Benchmark']['lower_through_hip_pivot_note'];?>
                            </li>
                        </ul>
                    </div>

                    <div class="d-box-md d-box-benchmark">
                        <h4 class="d-title">Lower Body Finish</h4>
                        <ul class="d-list">
                            <li>
                                <strong>Pear:</strong>
                                <strong class="grading">
                                    <?php echo $benchmarkDetails['Benchmark']['lower_finish_pear'];?>
                                </strong>
                            </li>
                            <li>
                                <strong>Pear Note:</strong>
                                <?php echo $benchmarkDetails['Benchmark']['lower_finish_pear_note'];?>
                            </li>
                            <li>
                                <strong>Knee Flex:</strong>
                                <strong class="grading">
                                    <?php echo $benchmarkDetails['Benchmark']['lower_finish_knee_flex'];?>
                                </strong>
                            </li>
                            <li>
                                <strong>Knee Flex Note:</strong>
                                <?php echo $benchmarkDetails['Benchmark']['lower_finish_knee_flex_note'];?>
                            </li>
                            <li>
                                <strong>Hip Sway:</strong>
                                <strong class="grading">
                                    <?php echo $benchmarkDetails['Benchmark']['lower_finish_hip_sway'];?>
                                </strong>
                            </li>
                            <li>
                                <strong>Hip Sway Note:</strong>
                                <?php echo $benchmarkDetails['Benchmark']['lower_finish_hip_sway_note'];?>
                            </li>
                            <li>
                                <strong>Hip Pivot:</strong>
                                <strong class="grading">
                                    <?php echo $benchmarkDetails['Benchmark']['lower_finish_hip_pivot'];?>
                                </strong>
                            </li>
                            <li>
                                <strong>Hip Pivot Note:</strong>
                                <?php echo $benchmarkDetails['Benchmark']['lower_finish_hip_pivot_note'];?>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!--- /Lower Body Stability Area -->

        <!--- Upper Body Stability Area --->
        <div class="tab-pane fade" id="upper-body-stability">
            <div class="row">
                <div class="col-lg-6 col-sm-6 col-sm-12">
                    <div class="d-box-md d-box-benchmark">
                        <h4 class="d-title">Upper Body Halfway Back:</h4>
                        <ul class="d-list">
                            <li>
                                <strong>Rotation:</strong>
                                <strong class="grading">
                                    <?php echo $benchmarkDetails['Benchmark']['upper_halfway_back_rotation'];?>
                                </strong>
                            </li>
                            <li>
                                <strong>Rotation Note:</strong>
                                <?php echo $benchmarkDetails['Benchmark']['upper_halfway_back_rotation_note'];?>
                            </li>
                            <li>
                                <strong>Spine Tilt:</strong>
                                <strong class="grading">
                                    <?php echo $benchmarkDetails['Benchmark']['upper_halfway_back_spine'];?>
                                </strong>
                            </li>
                            <li>
                                <strong>Spine Tilt Note:</strong>
                                <?php echo $benchmarkDetails['Benchmark']['upper_halfway_back_spine_note'];?>
                            </li>
                            <li>
                                <strong>Head:</strong>
                                <strong class="grading">
                                    <?php echo $benchmarkDetails['Benchmark']['upper_halfway_back_head'];?>
                                </strong>
                            </li>
                            <li>
                                <strong>Head Note:</strong>
                                <?php echo $benchmarkDetails['Benchmark']['upper_halfway_back_head_note'];?>
                            </li>
                        </ul>
                    </div>

                    <div class="d-box-md d-box-benchmark">
                        <h4 class="d-title">UPPER BODY ¾ SWING:</h4>
                        <ul class="d-list">
                            <li>
                                <strong>Rotation:</strong>
                                <strong class="grading">
                                    <?php echo $benchmarkDetails['Benchmark']['upper_swing_rotation'];?>
                                </strong>
                            </li>
                            <li>
                                <strong>Rotation Note:</strong>
                                <?php echo $benchmarkDetails['Benchmark']['upper_swing_rotation_note'];?>
                            </li>
                            <li>
                                <strong>Spine Tilt:</strong>
                                <strong class="grading">
                                    <?php echo $benchmarkDetails['Benchmark']['upper_swing_spine'];?>
                                </strong>
                            </li>
                            <li>
                                <strong>Spine Tilt Note:</strong>
                                <?php echo $benchmarkDetails['Benchmark']['upper_swing_spine_note'];?>
                            </li>
                            <li>
                                <strong>Head:</strong>
                                <strong class="grading">
                                    <?php echo $benchmarkDetails['Benchmark']['upper_swing_head'];?>
                                </strong>
                            </li>
                            <li>
                                <strong>Head Note:</strong>
                                <?php echo $benchmarkDetails['Benchmark']['upper_swing_head_note'];?>
                            </li>
                        </ul>
                    </div>

                    <div class="d-box-md d-box-benchmark">
                        <h4 class="d-title">UPPER BODY AT TOP:</h4>
                        <ul class="d-list">
                            <li>
                                <strong>Rotation:</strong>
                                <strong class="grading">
                                    <?php echo $benchmarkDetails['Benchmark']['upper_at_top_rotation'];?>
                                </strong>
                            </li>
                            <li>
                                <strong>Rotation Note:</strong>
                                <?php echo $benchmarkDetails['Benchmark']['upper_at_top_rotation_note'];?>
                            </li>
                            <li>
                                <strong>Spine Tilt:</strong>
                                <strong class="grading">
                                    <?php echo $benchmarkDetails['Benchmark']['upper_at_top_spine'];?>
                                </strong>
                            </li>
                            <li>
                                <strong>Spine Tilt Note:</strong>
                                <?php echo $benchmarkDetails['Benchmark']['upper_at_top_spine_note'];?>
                            </li>
                            <li>
                                <strong>Head:</strong>
                                <strong class="grading">
                                    <?php echo $benchmarkDetails['Benchmark']['upper_at_top_head'];?>
                                </strong>
                            </li>
                            <li>
                                <strong>Head Note:</strong>
                                <?php echo $benchmarkDetails['Benchmark']['upper_at_top_head_note'];?>
                            </li>
                        </ul>
                    </div>

                    <div class="d-box-md d-box-benchmark">
                        <h4 class="d-title">UPPER BODY Downswing:</h4>
                        <ul class="d-list">
                            <li>
                                <strong>Rotation:</strong>
                                <strong class="grading">
                                    <?php echo $benchmarkDetails['Benchmark']['upper_downswing_rotation'];?>
                                </strong>
                            </li>
                            <li>
                                <strong>Rotation Note:</strong>
                                <?php echo $benchmarkDetails['Benchmark']['upper_downswing_rotation_note'];?>
                            </li>
                            <li>
                                <strong>Spine Tilt:</strong>
                                <strong class="grading">
                                    <?php echo $benchmarkDetails['Benchmark']['upper_downswing_spine'];?>
                                </strong>
                            </li>
                            <li>
                                <strong>Spine Tilt Note:</strong>
                                <?php echo $benchmarkDetails['Benchmark']['upper_downswing_spine_note'];?>
                            </li>
                            <li>
                                <strong>Head:</strong>
                                <strong class="grading">
                                    <?php echo $benchmarkDetails['Benchmark']['upper_downswing_head'];?>
                                </strong>
                            </li>
                            <li>
                                <strong>Head Note:</strong>
                                <?php echo $benchmarkDetails['Benchmark']['upper_downswing_head_note'];?>
                            </li>
                        </ul>
                    </div>

                    <div class="d-box-md d-box-benchmark">
                        <h4 class="d-title">UPPER BODY Impact:</h4>
                        <ul class="d-list">
                            <li>
                                <strong>Rotation:</strong>
                                <strong class="grading">
                                    <?php echo $benchmarkDetails['Benchmark']['upper_impact_rotation'];?>
                                </strong>
                            </li>
                            <li>
                                <strong>Rotation Note:</strong>
                                <?php echo $benchmarkDetails['Benchmark']['upper_impact_rotation_note'];?>
                            </li>
                            <li>
                                <strong>Spine Tilt:</strong>
                                <strong class="grading">
                                    <?php echo $benchmarkDetails['Benchmark']['upper_impact_spine'];?>
                                </strong>
                            </li>
                            <li>
                                <strong>Spine Tilt Note:</strong>
                                <?php echo $benchmarkDetails['Benchmark']['upper_impact_spine_note'];?>
                            </li>
                            <li>
                                <strong>Head:</strong>
                                <strong class="grading">
                                    <?php echo $benchmarkDetails['Benchmark']['upper_impact_head'];?>
                                </strong>
                            </li>
                            <li>
                                <strong>Head Note:</strong>
                                <?php echo $benchmarkDetails['Benchmark']['upper_impact_head_note'];?>
                            </li>
                        </ul>
                    </div>

                    <div class="d-box-md d-box-benchmark">
                        <h4 class="d-title">UPPER BODY FOLLOW THROUGH:</h4>
                        <ul class="d-list">
                            <li>
                                <strong>Rotation:</strong>
                                <strong class="grading">
                                    <?php echo $benchmarkDetails['Benchmark']['upper_through_rotation'];?>
                                </strong>
                            </li>
                            <li>
                                <strong>Rotation Note:</strong>
                                <?php echo $benchmarkDetails['Benchmark']['upper_through_rotation_note'];?>
                            </li>
                            <li>
                                <strong>Spine Tilt:</strong>
                                <strong class="grading">
                                    <?php echo $benchmarkDetails['Benchmark']['upper_through_spine'];?>
                                </strong>
                            </li>
                            <li>
                                <strong>Spine Tilt Note:</strong>
                                <?php echo $benchmarkDetails['Benchmark']['upper_through_spine_note'];?>
                            </li>
                            <li>
                                <strong>Head:</strong>
                                <strong class="grading">
                                    <?php echo $benchmarkDetails['Benchmark']['upper_through_head'];?>
                                </strong>
                            </li>
                            <li>
                                <strong>Head Note:</strong>
                                <?php echo $benchmarkDetails['Benchmark']['upper_through_head_note'];?>
                            </li>
                        </ul>
                    </div>

                    <div class="d-box-md d-box-benchmark">
                        <h4 class="d-title">UPPER BODY FOLLOW FINISH:</h4>
                        <ul class="d-list">
                            <li>
                                <strong>Rotation:</strong>
                                <strong class="grading">
                                    <?php echo $benchmarkDetails['Benchmark']['upper_finish_rotation'];?>
                                </strong>
                            </li>
                            <li>
                                <strong>Rotation Note:</strong>
                                <?php echo $benchmarkDetails['Benchmark']['upper_finish_rotation_note'];?>
                            </li>
                            <li>
                                <strong>Spine Tilt:</strong>
                                <strong class="grading">
                                    <?php echo $benchmarkDetails['Benchmark']['upper_finish_spine'];?>
                                </strong>
                            </li>
                            <li>
                                <strong>Spine Tilt Note:</strong>
                                <?php echo $benchmarkDetails['Benchmark']['upper_finish_spine_note'];?>
                            </li>
                            <li>
                                <strong>Head:</strong>
                                <strong class="grading">
                                    <?php echo $benchmarkDetails['Benchmark']['upper_finish_head'];?>
                                </strong>
                            </li>
                            <li>
                                <strong>Head Note:</strong>
                                <?php echo $benchmarkDetails['Benchmark']['upper_finish_head_note'];?>
                            </li>
                        </ul>
                    </div>

                    <div class="d-box-md d-box-benchmark">
                        <h4 class="d-title">CONNECTION HALFWAY BACK:</h4>
                        <ul class="d-list">
                            <li>
                                <strong>Triangle Shape:</strong>
                                <strong class="grading">
                                    <?php echo $benchmarkDetails['Benchmark']['upper_halfway_back_triangle'];?>
                                </strong>
                            </li>
                            <li>
                                <strong>Triangle Shape Note:</strong>
                                <?php echo $benchmarkDetails['Benchmark']['upper_halfway_back_triangle_note'];?>
                            </li>
                            <li>
                                <strong>Arm Position:</strong>
                                <strong class="grading">
                                    <?php echo $benchmarkDetails['Benchmark']['upper_halfway_back_arms'];?>
                                </strong>
                            </li>
                            <li>
                                <strong>Arm Position Note:</strong>
                                <?php echo $benchmarkDetails['Benchmark']['upper_halfway_back_arms_note'];?>
                            </li>
                        </ul>
                    </div>

                    <div class="d-box-md d-box-benchmark">
                        <h4 class="d-title">CONNECTION ¾ SWING:</h4>
                        <ul class="d-list">
                            <li>
                                <strong>Triangle Shape:</strong>
                                <strong class="grading">
                                    <?php echo $benchmarkDetails['Benchmark']['upper_swing_triangle'];?>
                                </strong>
                            </li>
                            <li>
                                <strong>Triangle Shape Note:</strong>
                                <?php echo $benchmarkDetails['Benchmark']['upper_swing_triangle_note'];?>
                            </li>
                            <li>
                                <strong>Arm Position:</strong>
                                <strong class="grading">
                                    <?php echo $benchmarkDetails['Benchmark']['upper_swing_arms'];?>
                                </strong>
                            </li>
                            <li>
                                <strong>Arm Position Note:</strong>
                                <?php echo $benchmarkDetails['Benchmark']['upper_swing_arms_note'];?>
                            </li>
                        </ul>
                    </div>
                    <div class="d-box-md d-box-benchmark">
                        <h4 class="d-title">CONNECTION At top:</h4>
                        <ul class="d-list">
                            <li>
                                <strong>Triangle Shape:</strong>
                                <strong class="grading">
                                    <?php echo $benchmarkDetails['Benchmark']['upper_at_top_triangle'];?>
                                </strong>
                            </li>
                            <li>
                                <strong>Triangle Shape Note:</strong>
                                <?php echo $benchmarkDetails['Benchmark']['upper_at_top_triangle_note'];?>
                            </li>
                            <li>
                                <strong>Arm Position:</strong>
                                <strong class="grading">
                                    <?php echo $benchmarkDetails['Benchmark']['upper_at_top_arms'];?>
                                </strong>
                            </li>
                            <li>
                                <strong>Arm Position Note:</strong>
                                <?php echo $benchmarkDetails['Benchmark']['upper_at_top_arms_note'];?>
                            </li>
                        </ul>
                    </div>

                    <div class="d-box-md d-box-benchmark">
                        <h4 class="d-title">CONNECTION Downswing:</h4>
                        <ul class="d-list">
                            <li>
                                <strong>Triangle Shape:</strong>
                                <strong class="grading">
                                    <?php echo $benchmarkDetails['Benchmark']['upper_downswing_triangle'];?>
                                </strong>
                            </li>
                            <li>
                                <strong>Triangle Shape Note:</strong>
                                <?php echo $benchmarkDetails['Benchmark']['upper_downswing_triangle_note'];?>
                            </li>
                            <li>
                                <strong>Arm Position:</strong>
                                <strong class="grading">
                                    <?php echo $benchmarkDetails['Benchmark']['upper_downswing_arms'];?>
                                </strong>
                            </li>
                            <li>
                                <strong>Arm Position Note:</strong>
                                <?php echo $benchmarkDetails['Benchmark']['upper_downswing_arms_note'];?>
                            </li>
                        </ul>
                    </div>

                    <div class="d-box-md d-box-benchmark">
                        <h4 class="d-title">CONNECTION Impact:</h4>
                        <ul class="d-list">
                            <li>
                                <strong>Triangle Shape:</strong>
                                <strong class="grading">
                                    <?php echo $benchmarkDetails['Benchmark']['upper_impact_triangle'];?>
                                </strong>
                            </li>
                            <li>
                                <strong>Triangle Shape Note:</strong>
                                <?php echo $benchmarkDetails['Benchmark']['upper_impact_triangle_note'];?>
                            </li>
                            <li>
                                <strong>Arm Position:</strong>
                                <strong class="grading">
                                    <?php echo $benchmarkDetails['Benchmark']['upper_impact_arms'];?>
                                </strong>
                            </li>
                            <li>
                                <strong>Arm Position Note:</strong>
                                <?php echo $benchmarkDetails['Benchmark']['upper_impact_arms_note'];?>
                            </li>
                        </ul>
                    </div>

                    <div class="d-box-md d-box-benchmark">
                        <h4 class="d-title">CONNECTION FOLLOW THROUGH:</h4>
                        <ul class="d-list">
                            <li>
                                <strong>Triangle Shape:</strong>
                                <strong class="grading">
                                    <?php echo $benchmarkDetails['Benchmark']['upper_through_triangle'];?>
                                </strong>
                            </li>
                            <li>
                                <strong>Triangle Shape Note:</strong>
                                <?php echo $benchmarkDetails['Benchmark']['upper_through_triangle_note'];?>
                            </li>
                            <li>
                                <strong>Arm Position:</strong>
                                <strong class="grading">
                                    <?php echo $benchmarkDetails['Benchmark']['upper_through_arms'];?>
                                </strong>
                            </li>
                            <li>
                                <strong>Arm Position Note:</strong>
                                <?php echo $benchmarkDetails['Benchmark']['upper_through_arms_note'];?>
                            </li>
                        </ul>
                    </div>

                    <div class="d-box-md d-box-benchmark">
                        <h4 class="d-title">CONNECTION FINISH:</h4>
                        <ul class="d-list">
                            <li>
                                <strong>Triangle Shape:</strong>
                                <strong class="grading">
                                    <?php echo $benchmarkDetails['Benchmark']['upper_finish_triangle'];?>
                                </strong>
                            </li>
                            <li>
                                <strong>Triangle Shape Note:</strong>
                                <?php echo $benchmarkDetails['Benchmark']['upper_finish_triangle_note'];?>
                            </li>
                            <li>
                                <strong>Arm Position:</strong>
                                <strong class="grading">
                                    <?php echo $benchmarkDetails['Benchmark']['upper_finish_arms'];?>
                                </strong>
                            </li>
                            <li>
                                <strong>Arm Position Note:</strong>
                                <?php echo $benchmarkDetails['Benchmark']['upper_finish_arms_note'];?>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-6 col-sm-6 col-sm-12">

                    <div class="d-box-md d-box-benchmark">
                        <h4 class="d-title">SWING PLANE HALFWAY BACK:</h4>
                        <ul class="d-list">
                            <li>
                                <strong>Club Shaft Position:</strong>
                                <strong class="grading">
                                    <?php echo $benchmarkDetails['Benchmark']['upper_halfway_back_club_shaft'];?>
                                </strong>
                            </li>
                            <li>
                                <strong>Club Shaft Position Note:</strong>
                                <?php echo $benchmarkDetails['Benchmark']['upper_halfway_back_club_shaft_note'];?>
                            </li>
                        </ul>
                    </div>

                    <div class="d-box-md d-box-benchmark">
                        <h4 class="d-title">SWING PLANE ¾ SWING:</h4>
                        <ul class="d-list">
                            <li>
                                <strong>Club Shaft Position:</strong>
                                <strong class="grading">
                                    <?php echo $benchmarkDetails['Benchmark']['upper_swing_club_shaft'];?>
                                </strong>
                            </li>
                            <li>
                                <strong>Club Shaft Position Note:</strong>
                                <?php echo $benchmarkDetails['Benchmark']['upper_swing_club_shaft_note'];?>
                            </li>
                        </ul>
                    </div>

                    <div class="d-box-md d-box-benchmark">
                        <h4 class="d-title">SWING PLANE AT TOP:</h4>
                        <ul class="d-list">
                            <li>
                                <strong>Club Shaft Position:</strong>
                                <strong class="grading">
                                    <?php echo $benchmarkDetails['Benchmark']['upper_at_top_club_shaft'];?>
                                </strong>
                            </li>
                            <li>
                                <strong>Club Shaft Position Note:</strong>
                                <?php echo $benchmarkDetails['Benchmark']['upper_at_top_club_shaft_note'];?>
                            </li>
                        </ul>
                    </div>

                    <div class="d-box-md d-box-benchmark">
                        <h4 class="d-title">SWING PLANE DOWNSWING:</h4>
                        <ul class="d-list">
                            <li>
                                <strong>Club Shaft Position:</strong>
                                <strong class="grading">
                                    <?php echo $benchmarkDetails['Benchmark']['upper_downswing_club_shaft'];?>
                                </strong>
                            </li>
                            <li>
                                <strong>Club Shaft Position Note:</strong>
                                <?php echo $benchmarkDetails['Benchmark']['upper_downswing_club_shaft_note'];?>
                            </li>
                        </ul>
                    </div>

                    <div class="d-box-md d-box-benchmark">
                        <h4 class="d-title">SWING PLANE IMPACT:</h4>
                        <ul class="d-list">
                            <li>
                                <strong>Club Shaft Position:</strong>
                                <strong class="grading">
                                    <?php echo $benchmarkDetails['Benchmark']['upper_impact_club_shaft'];?>
                                </strong>
                            </li>
                            <li>
                                <strong>Club Shaft Position Note:</strong>
                                <?php echo $benchmarkDetails['Benchmark']['upper_impact_club_shaft_note'];?>
                            </li>
                        </ul>
                    </div>

                    <div class="d-box-md d-box-benchmark">
                        <h4 class="d-title">SWING PLANE FOLLOW THROUGH:</h4>
                        <ul class="d-list">
                            <li>
                                <strong>Club Shaft Position:</strong>
                                <strong class="grading">
                                    <?php echo $benchmarkDetails['Benchmark']['upper_through_club_shaft'];?>
                                </strong>
                            </li>
                            <li>
                                <strong>Club Shaft Position Note:</strong>
                                <?php echo $benchmarkDetails['Benchmark']['upper_through_club_shaft_note'];?>
                            </li>
                        </ul>
                    </div>

                    <div class="d-box-md d-box-benchmark">
                        <h4 class="d-title">SWING PLANE FINISH:</h4>
                        <ul class="d-list">
                            <li>
                                <strong>Club Shaft Position:</strong>
                                <strong class="grading">
                                    <?php echo $benchmarkDetails['Benchmark']['upper_finish_club_shaft'];?>
                                </strong>
                            </li>
                            <li>
                                <strong>Club Shaft Position Note:</strong>
                                <?php echo $benchmarkDetails['Benchmark']['upper_finish_club_shaft_note'];?>
                            </li>
                        </ul>
                    </div>

                    <div class="d-box-md d-box-benchmark">
                        <h4 class="d-title">CLUB FACE HALFWAY BACK:</h4>
                        <ul class="d-list">
                            <li>
                                <strong>Club Face Position:</strong>
                                <strong class="grading">
                                    <?php echo $benchmarkDetails['Benchmark']['upper_halfway_back_club_face'];?>
                                </strong>
                            </li>
                            <li>
                                <strong>Club Face Position Note:</strong>
                                <?php echo $benchmarkDetails['Benchmark']['upper_halfway_back_club_face_note'];?>
                            </li>
                        </ul>
                    </div>

                    <div class="d-box-md d-box-benchmark">
                        <h4 class="d-title">CLUB FACE ¾ SWING:</h4>
                        <ul class="d-list">
                            <li>
                                <strong>Club Face Position:</strong>
                                <strong class="grading">
                                    <?php echo $benchmarkDetails['Benchmark']['upper_swing_club_face'];?>
                                </strong>
                            </li>
                            <li>
                                <strong>Club Face Position Note:</strong>
                                <?php echo $benchmarkDetails['Benchmark']['upper_swing_club_face_note'];?>
                            </li>
                        </ul>
                    </div>

                    <div class="d-box-md d-box-benchmark">
                        <h4 class="d-title">CLUB FACE AT TOP:</h4>
                        <ul class="d-list">
                            <li>
                                <strong>Club Face Position:</strong>
                                <strong class="grading">
                                    <?php echo $benchmarkDetails['Benchmark']['upper_at_top_club_face'];?>
                                </strong>
                            </li>
                            <li>
                                <strong>Club Face Position Note:</strong>
                                <?php echo $benchmarkDetails['Benchmark']['upper_at_top_club_face_note'];?>
                            </li>
                        </ul>
                    </div>

                    <div class="d-box-md d-box-benchmark">
                        <h4 class="d-title">CLUB FACE DOWNSWING:</h4>
                        <ul class="d-list">
                            <li>
                                <strong>Club Face Position:</strong>
                                <strong class="grading">
                                    <?php echo $benchmarkDetails['Benchmark']['upper_downswing_club_face'];?>
                                </strong>
                            </li>
                            <li>
                                <strong>Club Face Position Note:</strong>
                                <?php echo $benchmarkDetails['Benchmark']['upper_downswing_club_face_note'];?>
                            </li>
                        </ul>
                    </div>

                    <div class="d-box-md d-box-benchmark">
                        <h4 class="d-title">CLUB FACE IMPACT:</h4>
                        <ul class="d-list">
                            <li>
                                <strong>Club Face Position:</strong>
                                <strong class="grading">
                                    <?php echo $benchmarkDetails['Benchmark']['upper_impact_club_face'];?>
                                </strong>
                            </li>
                            <li>
                                <strong>Club Face Position Note:</strong>
                                <?php echo $benchmarkDetails['Benchmark']['upper_impact_club_face_note'];?>
                            </li>
                        </ul>
                    </div>

                    <div class="d-box-md d-box-benchmark">
                        <h4 class="d-title">CLUB FACE FOLLOW THROUGH:</h4>
                        <ul class="d-list">
                            <li>
                                <strong>Club Face Position:</strong>
                                <strong class="grading">
                                    <?php echo $benchmarkDetails['Benchmark']['upper_through_club_face'];?>
                                </strong>
                            </li>
                            <li>
                                <strong>Club Face Position Note:</strong>
                                <?php echo $benchmarkDetails['Benchmark']['upper_through_club_face_note'];?>
                            </li>
                        </ul>
                    </div>

                    <div class="d-box-md d-box-benchmark">
                        <h4 class="d-title">CLUB FACE FOLLOW FINISH:</h4>
                        <ul class="d-list">
                            <li>
                                <strong>Club Face Position:</strong>
                                <strong class="grading">
                                    <?php echo $benchmarkDetails['Benchmark']['upper_finish_club_face'];?>
                                </strong>
                            </li>
                            <li>
                                <strong>Club Face Position Note:</strong>
                                <?php echo $benchmarkDetails['Benchmark']['upper_finish_club_face_note'];?>
                            </li>
                        </ul>
                    </div>

                    <div class="d-box-md d-box-benchmark">
                        <h4 class="d-title">WRIST COCK ¾ SWING:</h4>
                        <ul class="d-list">
                            <li>
                                <strong>Wrist Cock:</strong>
                                <strong class="grading">
                                    <?php echo $benchmarkDetails['Benchmark']['wrist_cock_swing'];?>
                                </strong>
                            </li>
                            <li>
                                <strong>Wrist Cock Note:</strong>
                                <?php echo $benchmarkDetails['Benchmark']['wrist_cock_swing_note'];?>
                            </li>
                        </ul>
                    </div>

                    <div class="d-box-md d-box-benchmark">
                        <h4 class="d-title">WRIST COCK AT TOP:</h4>
                        <ul class="d-list">
                            <li>
                                <strong>Wrist Cock:</strong>
                                <strong class="grading">
                                    <?php echo $benchmarkDetails['Benchmark']['wrist_cock_at_top'];?>
                                </strong>
                            </li>
                            <li>
                                <strong>Wrist Cock Note:</strong>
                                <?php echo $benchmarkDetails['Benchmark']['wrist_cock_at_top_note'];?>
                            </li>
                        </ul>
                    </div>

                    <div class="d-box-md d-box-benchmark">
                        <h4 class="d-title">DOWNSWING SEQUENCE:</h4>
                        <ul class="d-list">
                            <li>
                                <strong>Upper Body Position:</strong>
                                <strong class="grading">
                                    <?php echo $benchmarkDetails['Benchmark']['sequence_downswing_upper_body'];?>
                                </strong>
                            </li>
                            <li>
                                <strong>Upper Body Position Note:</strong>
                                <?php echo $benchmarkDetails['Benchmark']['sequence_downswing_upper_body_note'];?>
                            </li>
                            <li>
                                <strong>Lower Body Position:</strong>
                                <strong class="grading">
                                    <?php echo $benchmarkDetails['Benchmark']['sequence_downswing_lower_body'];?>
                                </strong>
                            </li>
                            <li>
                                <strong>Lower Body Position Note:</strong>
                                <?php echo $benchmarkDetails['Benchmark']['sequence_downswing_lower_body_note'];?>
                            </li>
                        </ul>
                    </div>

                    <div class="d-box-md d-box-benchmark">
                        <h4 class="d-title">HANDS/CLUB POSITION IMPACT:</h4>
                        <ul class="d-list">
                            <li>
                                <strong>Hand Position Rotation:</strong>
                                <strong class="grading">
                                    <?php echo $benchmarkDetails['Benchmark']['club_position_impact_hand_rotation'];?>
                                </strong>
                            </li>
                            <li>
                                <strong>Hand Position Rotation Note:</strong>
                                <?php echo $benchmarkDetails['Benchmark']['club_position_impact_hand_rotation_note'];?>
                            </li>

                            <li>
                                <strong>Hand Position Location:</strong>
                                <strong class="grading">
                                    <?php echo $benchmarkDetails['Benchmark']['club_position_impact_hand_location'];?>
                                </strong>
                            </li>
                            <li>
                                <strong>Hand Position Location Note:</strong>
                                <?php echo $benchmarkDetails['Benchmark']['club_position_impact_hand_location_note'];?>
                            </li>

                            <li>
                                <strong>Tempo on Backswing:</strong>
                                <strong class="grading">
                                    <?php echo $benchmarkDetails['Benchmark']['club_position_impact_tempo_backswing'];?>
                                </strong>
                            </li>
                            <li>
                                <strong>Tempo on Backswing Note:</strong>
                                <?php echo $benchmarkDetails['Benchmark']['club_position_impact_tempo_backswing_note'];?>
                            </li>

                            <li>
                                <strong>Tempo on Downswing:</strong>
                                <strong class="grading">
                                    <?php echo $benchmarkDetails['Benchmark']['club_position_impact_tempo_downswing'];?>
                                </strong>
                            </li>
                            <li>
                                <strong>Tempo on Downswing Note:</strong>
                                <?php echo $benchmarkDetails['Benchmark']['club_position_impact_tempo_downswing_note'];?>
                            </li>

                            <li>
                                <strong>Lag:</strong>
                                <strong class="grading">
                                    <?php echo $benchmarkDetails['Benchmark']['club_position_impact_lag'];?>
                                </strong>
                            </li>
                            <li>
                                <strong>Lag Note:</strong>
                                <?php echo $benchmarkDetails['Benchmark']['club_position_impact_lag_note'];?>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!--- /Upper Body Stability Area --->
    </div>
</div>

<script>
    $(document).ready(function(){
        $('.grading').each(function(){
           var grade = $(this).text();

           if(grade <= 20){
               $(this).addClass('danger');
           }
           else if(grade > 20 && grade < 40){
               $(this).addClass('orange');
           }
           else if(grade >= 40 && grade < 60){
               $(this).addClass('info');
           }
           else if(grade >= 60 && grade < 80){
               $(this).addClass('yellowgreen');
           }
           else if(grade >= 80 && grade <= 100){
               $(this).addClass('green');
           }
            else{
               $(this).addClass('default');
           }

        });
    });
</script>