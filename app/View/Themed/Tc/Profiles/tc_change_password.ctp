<?php
/**
 * @var $this View
 */
?>
<h2 class="page-title">Change Password</h2>
<hr class="shadow-line"/>
<div class="row">
    <div class="col-md-8 col-md-offset-2 col-sm-12">
        <div class="box box-padding-adding radius-5 margin-top-25 margin-bottom-25">
                <?php echo $this->Form->create('Profile', array('controller' => 'profiles', 'action' => 'change_password', 'method' => 'post', 'class' => 'form-horizontal form-custom'));?>
                <div class="form-group">
                    <label class="col-sm-4 control-label">Old Password</label>
                    <div class="col-sm-8">
                        <?php
                        echo $this->Form->input('User.id', array('value' => $this->Session->read('Auth.User.id')));
                        ?>
                        <?php
                        echo $this->Form->input('User.oldPassword', array('type' => 'password', 'class' => 'form-control', 'placeholder' => 'Old Password', 'label' => false, 'error' => false, 'div' => false, 'required' => false));
                        ?>
                        <?php echo $this->Form->error('User.oldPassword');?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label">New Password</label>
                    <div class="col-sm-8">
                        <?php
                        echo $this->Form->input('User.password', array('type' => 'password', 'class' => 'form-control', 'placeholder' => 'New Password', 'label' => false, 'error' => false, 'div' => false, 'required' => false));
                        ?>
                        <?php echo $this->Form->error('User.password');?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label">Confirm New Password</label>
                    <div class="col-sm-8">
                        <?php
                        echo $this->Form->input('User.cPassword', array('type' => 'password', 'class' => 'form-control', 'placeholder' => 'Confirm Password', 'label' => false, 'error' => false, 'div' => false, 'required' => false));
                        ?>
                        <?php echo $this->Form->error('User.cPassword');?>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-8 col-sm-offset-4">
                        <button class="btn btn-theme pull-right">Change Password</button>
                    </div>
                </div>
            <?php echo $this->Form->end();?>
        </div>
    </div>
</div>