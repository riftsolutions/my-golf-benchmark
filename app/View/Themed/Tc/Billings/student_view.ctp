<?php
/**
 * @var $this View
 */
?>
<h2 class="page-title">Transaction Details</h2>
<hr class="shadow-line"/>
<div class="row">
    <div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-12">
        <div class="clearfix"></div>
        <div class="d-box-md margin-top-25">
            <h4 class="d-title"><i class="fa fa-money purple"></i> Transaction Details</h4>
            <ul class="d-list">
                <li>
                    <strong>Student Name:</strong>
                    <?php
                    echo $this->Html->link($transactionDetails[0]['studentName'], array('controller' => 'users', 'action' => 'details', $transactionDetails['User']['uuid']));
                    ?>
                </li>
                <li>
                    <strong>Email:</strong>
                    <?php
                    echo $transactionDetails['User']['username'];
                    ?>
                </li>
                <li>
                    <strong>Package Name:</strong>
                    <?php
                    echo $this->Html->link($transactionDetails['Package']['name'], array('controller' => 'packages', 'action' => 'view', $transactionDetails['Package']['uuid']));
                    ?>
                </li>
                <li>
                    <strong>Price:</strong>
                    <?php
                    echo $this->Number->currency($transactionDetails['Package']['price']);
                    ?>
                </li>
                <li>
                    <strong>Tax amount:</strong>
                    <?php
                    echo $this->Number->currency($transactionDetails['Package']['tax']);
                    ?>
                    <i>(<?php echo $transactionDetails['Package']['tax_percentage'];?>%)</i>
                </li>
                <li>
                    <strong>Total:</strong>
                    <?php
                    echo $this->Number->currency($transactionDetails['Package']['total']);
                    ?>
                </li>

                <li>
                    <strong>Transaction Amount:</strong>
                    <?php
                    echo $this->Number->currency($transactionDetails['Billing']['amount']);
                    ?>
                </li>

                <li>
                    <strong>Transaction Card Number:</strong>
                    <?php
                    echo $transactionDetails['Billing']['payment_account_number'];
                    ?>
                </li>

                <li>
                    <strong>Status:</strong>
                    <?php
                    if($transactionDetails['Billing']['payment_final_status'] == 1){
                        echo '<label class="label label-success">payment complete</label>';
                    }
                    elseif($transactionDetails['Billing']['payment_final_status'] == 2){
                        echo '<label class="label label-default">N/A</label>';
                    }
                    ?>
                </li>

                <li>
                    <strong>Instructor Name:</strong>
                    <?php
                    echo $this->Html->link($transactionDetails[0]['instructorName'], array('controller' => 'users', 'action' => 'details', $transactionDetails['InstructorUser']['uuid']));
                    ?>
                </li>
            </ul>
            <br/>
        </div>
    </div>
</div>