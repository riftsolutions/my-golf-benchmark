<?php
/**
 * @var $this View
 */
?>
<h2 class="page-title">Checkout</h2>
<hr class="shadow-line"/>
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="box box-padding-normal">
            <div class="row">
                <div class="col-lg-6  col-md-6 col-sm-12">
                    <div class="margin-top-25">
                        <ul class="data-list">
                            <li>
                                <strong>Total Bill:</strong><br>
                                <span>
                                    <?php echo $this->Number->currency($cost['total']);?>
                                </span>
                            </li>
                        </ul>
                        <ul class="data-list">
                            <li>
                                <strong>Payment To:</strong><br>
                            </li>
                            <li>(949) 554-9926</li>
                            <li>info@golfswingprescription.com</li>
                            <li>
                                26081 Merit Circle #103<br>
                                Laguna Hills, CA 92653
                            </li>
                        </ul>
                    </div>
                </div>
                <?php //var_dump($lessonDetails['CreatedBy']['first_name']);?>
                <div class="col-lg-6  col-md-6 col-sm-12">
                    <div class="margin-top-25">
                        <ul class="data-list" style="text-align: right">
                            <li>
                                <b>Instructor Information</b>
                            </li>
                            <li>
                                <?php echo $userInfo['Profile']['first_name'];?>
                            </li>
                            <li><?php echo $this->Session->read('Auth.User.username');?></li>
                            <li>
                                <?php echo $userInfo['Profile']['phone']; ?>
                            </li>
                            <li>
                                <?php
                                if($userInfo['Profile']['billing_street_1']){
                                    echo $userInfo['Profile']['billing_street_1'];
                                }
                                ?>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <table class="table margin-top-25">
                        <thead>
                        <tr>
                            <th>Instructor</th>
                            <th>Title</th>
                            <th><span class="pull-right">Sub total</span></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach($packages as $package):?>
                        <tr>
                            <td class="padding-left-0 padding-right-0">
                                <?php echo $this->Html->link($package['Profile']['first_name']. ' '. $package['Profile']['last_name'], array('controller' => 'users', 'action' => 'view', $package['User']['uuid']), array('class' => 'green'));?>
                            </td>
                            <td class="padding-left-0 padding-right-0">
                                <?php echo $this->Html->link($package['Package']['name'], array('controller' => 'package', 'action' => 'view', $package['Package']['uuid']))?>
                            </td>
                            <td class="padding-left-0 padding-right-0">
                                <span class="pull-right"><?php echo $this->Number->currency($package['Package']['price']);?></span>
                            </td>
                        </tr>
                        <?php endforeach;?>
                        </tbody>
                    </table>
                    <div class="subtoal">
                        <ul class="data-list">
                            <li>
                                <strong>Sub Total:</strong>
                                <?php echo $this->Number->currency($cost['subtotal']);?>
                            </li>
                            <li>
                                <strong>Tax:</strong>
                                <?php echo $this->Number->currency($cost['tax']);?>
                            </li>
                            <li>
                                <strong>Total</strong>
                                <?php echo $this->Number->currency($cost['total']);?>
                            </li>
                        </ul>
                    </div>
                    <?php
                    echo $this->Html->link(
                        'Process To Payment',
                        array(
                            'controller' => 'billings',
                            'action' => 'payment',
                        ),
                        array(
                            'class' => 'btn btn-theme',
                            'escape' => false
                        )
                    );
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
