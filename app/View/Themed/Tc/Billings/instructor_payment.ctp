<?php
/**
 * @var $this View
 */
?>
<style>
    .form-group {
        clear: both;
    }

    .d-box {
        min-height: 100px;
    }
</style>

<div id="studentID" style="display: none;">
    <?php $studentID = $this->Session->read('studentID'); echo $studentID; ?>
</div>

<?php
if($this->Session->read('cardValidationError') == true){
    $cardArea = 'style="display: visible;"';
    $oldCard = 'style="display: none;"';
    $newCard = 'style="display: visible;"';

    $payWithExistingBtn = 'style="display: visible;"';
    $payWithNewBtn = 'style="display: none;"';

}
else{
    $cardArea = 'style="display: none;"';
    $oldCard = 'style="display: none;"';
    $newCard = 'style="display: none;"';

    $payWithExistingBtn = 'style="display: none;"';
    $payWithNewBtn = 'style="display: none;"';
}

?>



<h2 class="page-title">Payment</h2>
<hr class="shadow-line"/>
<div class="row">
    <div class="col-lg-8 col-md-8">
        <div class="d-box">
            <h4 class="d-title"><i class="fa fa-check green"></i> Review</h4>
            <table class="table margin-top-25">
                <thead>
                <tr>
                    <th>Instructor</th>
                    <th>Title</th>
                    <th><span class="pull-right">Sub total</span></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($packages as $package):?>
                <tr>
                    <td class="padding-left-0 padding-right-0">
                        <?php echo $this->Html->link($package['Profile']['first_name']. ' '. $package['Profile']['last_name'], array('controller' => 'users', 'action' => 'view', $package['User']['uuid']), array('class' => 'green'));?>
                    </td>
                    <td class="padding-left-0 padding-right-0">
                        <?php echo $this->Html->link($package['Package']['name'], array('controller' => 'package', 'action' => 'view', $package['Package']['uuid']))?>
                    </td>
                    <td class="padding-left-0 padding-right-0">
                        <span class="pull-right"><?php echo $this->Number->currency($package['Package']['price']);?></span>
                    </td>
                </tr>
                <?php endforeach;?>
                </tbody>
            </table>
            <div class="subtoal">
                <ul class="data-list">
                    <li>
                        <strong>Sub Total:</strong>
                        <?php echo $this->Number->currency($cost['subtotal']);?>
                    </li>
                    <li>
                        <strong>Tax:</strong>
                        <?php echo $this->Number->currency($cost['tax']);?>
                    </li>
                    <li>
                        <strong>Total</strong>
                        <?php echo $this->Number->currency($cost['total']);?>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="col-lg-4 col-md-4">
        <div class="d-box">
            <h4 class="d-title padding-bottom-10">
                <i class="fa fa-money green"></i> Payment
            </h4>
            <hr class="shadow-list"/>
            <?php echo $this->Form->create('Billing', array('controller' => 'billings', 'action' => 'payment'));?>
            <div class="form-group">
                <label class="control-label">Select Student</label>
                <?php
                echo $this->Form->input(
                    'user_id',
                    array(
                        'type' => 'select',
                        'class' => 'form-control selectStudentForPurchase',
                        'label' => false,
                        'div' => false,
                        'required' => false,
                        'options' => $students,
                        'empty' => 'Select Student',
                        'value' => $studentID
                    )
                )
                ?>
            </div>
            <div class="card-area" <?php echo $cardArea;?>>

                <div class="old-card" <?php echo $oldCard;?>>
                    <div class="form-group">
                        <label class="control-label">Select Card</label>
                        <div></div>
                    </div>
                </div>
                <div class="new-card row" <?php echo $newCard;?> >

                    <div class="form-group">
                        <label class="col-sm-12 control-label">Card Name</label>
                        <div class="col-sm-12">
                            <?php echo $this->Form->input('Card.cardName', array('type' => 'text', 'label' => false, 'required' => false, 'class' => 'form-control', 'placeholder' => 'Name on Card'))?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-12 control-label">Card Number</label>
                        <div class="col-sm-12">
                            <?php echo $this->Form->input('Card.cardNumber', array('type' => 'text', 'label' => false, 'required' => false, 'class' => 'form-control', 'placeholder' => 'Card Number'))?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-12 control-label">Expiration Year</label>
                        <div class="col-sm-12">
                            <?php
                            echo $this->Form->input('Card.ExpireYear', array(
                                    'type' => 'select',
                                    'label' => false,
                                    'required' => false,
                                    'empty' => 'Select Year',
                                    'class' => 'form-control',
                                    'options' => array(
                                        '2015' => '2015',
                                        '2016' => '2016',
                                        '2017' => '2017',
                                        '2018' => '2018',
                                        '2019' => '2019',
                                        '2020' => '2020',
                                    )
                                ))
                            ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-12 control-label">Expiration Month</label>
                        <div class="col-sm-12">
                            <?php
                            echo $this->Form->input('Card.ExpireMonth', array(
                                    'type' => 'select',
                                    'label' => false,
                                    'required' => false,
                                    'empty' => 'Select Year',
                                    'class' => 'form-control',
                                    'options' => array(
                                        '01' => 'January (01)',
                                        '02' => 'February (02)',
                                        '03' => 'March (03)',
                                        '04' => 'April (04)',
                                        '05' => 'May (05)',
                                        '06' => 'June (06)',
                                        '07' => 'Jule (07)',
                                        '08' => 'August (08)',
                                        '09' => 'September (09)',
                                        '10' => 'October (10)',
                                        '11' => 'November (11)',
                                        '12' => 'December (12)',
                                    )
                                ))
                            ?>
                            <div><?php echo $this->Form->error('Card.Expiry');?></div>
                        </div>
                    </div>

                </div>

                <div>
                    <a class="d-btn pull-right"  id="new-card-btn" <?php echo $payWithNewBtn;?>>Pay by new card</a>
                    <a class="d-btn pull-right"  id="old-card-btn" <?php echo $payWithExistingBtn;?>>Pay with Existing</a>
                </div>

                <br/>
                <div class="form-group btn-area">
                    <button class="btn btn-theme">Paid</button>
                </div>
            </div>
            <?php echo $this->Form->end();?>
        </div>
    </div>
</div>


<script>
    $('.selectStudentForPurchase').click(function(){
        var studentID = $(this).val();
        $('.card-area').show();
        $('.card-area').addClass('animated fadeIn');

        $.ajax({
            url: '<?php echo Router::url('/', true);?>student/cards/getStudentCards',
            type: "POST",
            dataType: "json",
            data:  {studentID: studentID},
            success: function(data) {
                if(data != null){
                    console.log(data);
                    $('.btn-area').show();
                    var cards = '<select name="data[Billing][card_id]" class="form-control">';
                    if(data.package != 'error'){
                        $.each(data, function (key, value) {
                            cards += '<option value="' + key + '">' + value + '</option>';
                        });
                    }

                    $('.new-card').hide();
                    $('.old-card-btn').hide();

                    $('.old-card').show();
                    $('#new-card-btn').show();
                    $('.old-card').addClass('animated fadeIn');
                    $('#new-card-btn').addClass('animated fadeIn');

                    $('.card-area .old-card div div').html(cards);
                }
                else{

                    $('.old-card').hide();
                    $('#old-card-btn').hide();
                    $('#new-card-btn').hide();

                    $('.new-card').show();
                    manageFlashMessage('alert-danger', 'Card Empty or access denied')
                }
            }
        });
    });
</script>





<script>
    $(document).ready(function(){
        $('#new-card-btn').click(function(){
            $('#new-card-btn').hide();
            $('#old-card-btn').show();
            $('#old-card-btn').addClass('animated fadeIn');

            $('.old-card').hide();
            $('.new-card').show();
            $('.new-card').addClass('animated fadeIn');
        });

        $('#old-card-btn').click(function(){
            $('#old-card-btn').hide();
            $('#new-card-btn').show();
            $('#new-card-btn').addClass('animated fadeIn');

            $('.new-card').hide();
            $('.old-card').show();
            $('.old-card').addClass('animated fadeIn');
        });


        var studentID = $('#studentID').text();
        $.ajax({
            url: '<?php echo Router::url('/', true);?>student/cards/getStudentCards',
            type: "POST",
            dataType: "json",
            data:  {studentID: studentID},
            success: function(data) {
                if(data != null){
                    var cards = '<select name="data[Billing][card_id]" class="form-control">';
                    if(data.package != 'error'){
                        $.each(data, function (key, value) {
                            cards += '<option value="' + key + '">' + value + '</option>';
                        });
                    }
                    $('.new-card').hide();
                    $('.old-card-btn').hide();

                    $('.old-card').show();
                    $('#new-card-btn').show();

                    $('.card-area .old-card div div').html(cards);
                }
                else{
                    $('.old-card').hide();
                    $('#old-card-btn').hide();
                    $('#new-card-btn').hide();

                    $('.new-card').show();
                }

            }
        });
        console.log(studentID);
    });
</script>