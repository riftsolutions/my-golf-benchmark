<?php
/**
 * @var $this View
 */
?>
<h2 class="page-title">Add New Lead</h2>
<hr class="shadow-line"/>
<?php
if($this->Session->read('isCustomSource') == true){
    $defaultSource = 'style="display: none"';
    $customSource = 'style="display: visible"';

    $defaultSourceBtn = 'style="display: visible"';
    $customSourceBtn = 'style="display: none"';

    $customValue = 1;
}
else{
    $defaultSource = 'style="display: visible"';
    $customSource = 'style="display: none"';

    $defaultSourceBtn = 'style="display: none"';
    $customSourceBtn = 'style="display: visible"';
    $customValue = 0;
}
?>
<div class="row">
    <div class="col-md-8 col-md-offset-2 col-sm-12">
        <div class="box box-padding-adding radius-5 margin-top-25 margin-bottom-25">
            <?php echo $this->Form->create(
                'Contact',
                array('controller' => 'contacts', 'action' => 'add', 'class' => 'form-horizontal form-custom')
            ); ?>
            <div class="form-group">
                <label class="col-sm-4 control-label">First Name</label>

                <div class="col-sm-8">
                    <?php
                    echo $this->Form->input(
                        'Contact.first_name',
                        array(
                            'type' => 'text',
                            'class' => 'form-control',
                            'placeholder' => 'First Name',
                            'label' => false,
                            'required' => false,
                        )
                    );
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4 control-label">Last Name</label>

                <div class="col-sm-8">
                    <?php
                    echo $this->Form->input(
                        'Contact.last_name',
                        array(
                            'type' => 'text',
                            'class' => 'form-control',
                            'placeholder' => 'Last Name',
                            'label' => false,
                            'required' => false,
                        )
                    );
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4 control-label">Email</label>

                <div class="col-sm-8">
                    <?php
                    echo $this->Form->input(
                        'Contact.email',
                        array(
                            'type' => 'text',
                            'class' => 'form-control',
                            'placeholder' => 'Email Address',
                            'label' => false,
                            'required' => false,
                        )
                    );
                    ?>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-4 control-label">Assign to</label>
                <div class="col-sm-8">
                    <?php
                    echo $this->Form->input(
                        'Contact.user_id',
                        array(
                            'type' => 'select',
                            'class' => 'form-control appSource',
                            'label' => false,
                            'div' => false,
                            'required' => false,
                            'options' => $instructors,
                        )
                    );
                    ?>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-4 control-label">Phone</label>

                <div class="col-sm-8">
                    <?php
                    echo $this->Form->input(
                        'Contact.phone',
                        array(
                            'type' => 'text',
                            'class' => 'form-control',
                            'placeholder' => 'Phone Number',
                            'label' => false,
                            'required' => false,
                        )
                    );
                    ?>
                </div>
            </div>
            <!--<div class="form-group">
                <label class="col-sm-4 control-label">Account Type</label>

                <div class="col-sm-8">
                    <div class="user-role">
                        <div class="radio">
                            <label>
                                <input type="radio" name="data[Contact][account_type]" value="1" checked>
                                Student
                            </label>
                        </div>
                        <div class="radio disabled">
                            <label>
                                <input type="radio" name="data[Contact][account_type]" value="3">
                                Lead
                            </label>
                        </div>
                    </div>
                </div>
            </div>-->

            <input type="hidden" name="data[Contact][account_type]" value="3">

            <div class="form-group">
                <label class="col-sm-4 control-label">Source</label>
                <div class="col-sm-8">
                    <?php echo $this->Form->input('Contact.is_custom_source', array('type' => 'hidden', 'value' => $customValue, 'class' => 'is_custom'))?>
                    <div class="default-source" <?php echo $defaultSource;?>>
                        <?php
                        echo $this->Form->input(
                            'Contact.source',
                            array(
                                'type' => 'select',
                                'class' => 'form-control',
                                'label' => false,
                                'div' => false,
                                'required' => false,
                                'options' => $sources,
                                'empty' => 'Select Source'
                            )
                        );
                        ?>
                    </div>
                    <div <?php echo $customSource;?> class="custom-source">
                        <?php echo $this->Form->input('Contact.custom_source', array('type' => 'text', 'class' => 'form-control', 'required' => false, 'div' => false, 'label' => false, 'placeholder' => 'Type custom source'))?>
                    </div>
                    <a class="d-btn pull-right custom-source-btn" <?php echo $customSourceBtn?>>custom source?</a>
                    <a class="d-btn pull-right default-source-btn" <?php echo $defaultSourceBtn?> >back to source?</a>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-4 control-label">Follow Up</label>
                <div class="col-sm-6">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                        <input id="datepicker" name="data[Contact][follow_up_date]" type="text" class="form-control" placeholder="Follow Up">
                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                    </div>
                    <?php echo $this->Form->error('Contact.follow_up_date');?>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-4 control-label">Comment</label>

                <div class="col-sm-8">
                    <?php
                    echo $this->Form->input(
                        'Contact.comments',
                        array(
                            'type' => 'textarea',
                            'class' => 'form-control',
                            'placeholder' => 'Place your note here',
                            'label' => false,
                            'required' => false,
                        )
                    );
                    ?>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12">
                    <button class="btn btn-theme pull-right">Add New Lead</button>
                </div>
            </div>
            <?php echo $this->Form->end(); ?>
        </div>
    </div>
</div>