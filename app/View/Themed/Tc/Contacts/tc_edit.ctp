<?php
/**
 * @var $this View
 */
?>
<?php
if($contactDetails['Contact']['is_custom_source'] == 1 || $this->Session->read('isCustomSource') == true){
    $defaultSource = 'style="display: none"';
    $customSource = 'style="display: visible"';

    $defaultSourceBtn = 'style="display: visible"';
    $customSourceBtn = 'style="display: none"';

    $customValue = 1;
    $cSourceValue = $contactDetails['Contact']['source'];
}
else{
    $defaultSource = 'style="display: visible"';
    $customSource = 'style="display: none"';

    $defaultSourceBtn = 'style="display: none"';
    $customSourceBtn = 'style="display: visible"';
    $customValue = 0;
    $cSourceValue = '';
}
?>
<h2 class="page-title">Edit Lead</h2>
<hr class="shadow-line"/>
<div class="row">
    <div class="col-md-8 col-md-offset-2 col-sm-12">
        <div class="box box-padding-adding radius-5 margin-top-25 margin-bottom-25">
            <?php echo $this->Form->create(
                'Contact',
                array('controller' => 'contacts', 'action' => 'edit/'.$contactDetails['Contact']['id'], 'class' => 'form-horizontal form-custom')
            ); ?>
            <div class="form-group">
                <label class="col-sm-4 control-label">First Name</label>

                <div class="col-sm-8">
                    <?php
                    echo $this->Form->input(
                        'Contact.first_name',
                        array(
                            'type' => 'text',
                            'class' => 'form-control',
                            'value' => $contactDetails['Contact']['first_name'],
                            'label' => false,
                            'required' => false,
                        )
                    );
                    ?>
                    <?php echo $this->Form->error('Contact.first_name'); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4 control-label">Last Name</label>

                <div class="col-sm-8">
                    <?php
                    echo $this->Form->input(
                        'Contact.last_name',
                        array(
                            'type' => 'text',
                            'class' => 'form-control',
                            'value' => $contactDetails['Contact']['last_name'],
                            'label' => false,
                            'required' => false,
                        )
                    );
                    ?>
                    <?php echo $this->Form->error('Contact.last_name'); ?>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-4 control-label">Assign to</label>
                <div class="col-sm-8">
                    <?php
                    echo $this->Form->input(
                        'Contact.user_id',
                        array(
                            'type' => 'select',
                            'class' => 'form-control appSource',
                            'label' => false,
                            'div' => false,
                            'required' => false,
                            'options' => $instructors,
                            'value' => $contactDetails['Contact']['user_id']
                        )
                    );
                    ?>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-4 control-label">Email</label>

                <div class="col-sm-8">
                    <?php
                    echo $this->Form->input(
                        'Contact.email',
                        array(
                            'type' => 'text',
                            'class' => 'form-control',
                            'value' => $contactDetails['Contact']['email'],
                            'label' => false,
                            'required' => false,
                        )
                    );
                    ?>
                    <?php echo $this->Form->error('Contact.email'); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4 control-label">Phone</label>

                <div class="col-sm-8">
                    <?php
                    echo $this->Form->input(
                        'Contact.phone',
                        array(
                            'type' => 'text',
                            'class' => 'form-control',
                            'value' => $contactDetails['Contact']['phone'],
                            'label' => false,
                            'required' => false,
                        )
                    );
                    ?>
                    <?php echo $this->Form->error('Contact.phone'); ?>
                </div>
            </div>


            <div class="form-group">
                <label class="col-sm-4 control-label">Source</label>
                <div class="col-sm-8">
                    <?php echo $this->Form->input('Contact.is_custom_source', array('type' => 'hidden', 'value' => $customValue, 'class' => 'is_custom'))?>
                    <div class="default-source" <?php echo $defaultSource;?>>
                        <?php
                        echo $this->Form->input(
                            'Contact.source',
                            array(
                                'type' => 'select',
                                'class' => 'form-control',
                                'label' => false,
                                'div' => false,
                                'required' => false,
                                'options' => $sources,
                                'empty' => 'Select Source',
                                'value' => $contactDetails['Contact']['source_id']
                            )
                        );
                        ?>
                    </div>
                    <div <?php echo $customSource;?> class="custom-source">
                        <?php echo $this->Form->input('Contact.custom_source', array('type' => 'text', 'class' => 'form-control', 'required' => false, 'div' => false, 'label' => false, 'value' => $cSourceValue))?>
                    </div>
                    <a class="d-btn pull-right custom-source-btn" <?php echo $customSourceBtn?>>custom source?</a>
                    <a class="d-btn pull-right default-source-btn" <?php echo $defaultSourceBtn?> >back to source?</a>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-4 control-label">Follow Up</label>
                <div class="col-sm-6">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                        <input id="datepicker" name="data[Contact][follow_up_date]" type="text" class="form-control" value="<?php echo $contactDetails['Contact']['follow_up_date'];?>">
                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                    </div>
                    <?php echo $this->Form->error('Contact.follow_up_date');?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4 control-label">Comment</label>

                <div class="col-sm-8">
                    <?php
                    echo $this->Form->input(
                        'Contact.comments',
                        array(
                            'type' => 'textarea',
                            'class' => 'form-control',
                            'value' => $contactDetails['Contact']['comments'],
                            'label' => false,
                            'required' => false,
                        )
                    );
                    ?>
                    <?php echo $this->Form->error('Contact.phone'); ?>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12">
                    <button class="btn btn-theme pull-right">Edit Lead</button>
                </div>
            </div>
            <?php echo $this->Form->end(); ?>
        </div>
    </div>
</div>