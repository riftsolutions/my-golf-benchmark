<?php
/**
 * @var $this View
 */
?>
<h2 class="page-title">Lead List</h2>
<hr class="shadow-line"/>

<div class="page-option">
    <div class="pull-left page-option-left">
        <div class="dropdown pull-left">
            <button class="btn btn-info dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown">
                Quick Navigation
                <span class="caret"></span>
            </button>
            <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">

                <?php foreach ($overviews as $slug => $overview): ?>
                    <li role="presentation">
                        <?php echo $this->Html->link($overview['name'] . ' (' . $overview['count'] . ')', array('controller' => 'contacts', 'action' => 'list/' . $slug)); ?>
                    </li>
                    <li role="presentation" class="divider"></li>
                <?php endforeach; ?>
                <li role="presentation">
                    <?php echo $this->Html->link('One Star Rating (' . $ratingOverview[0][0]['oneStarRatingLead'] . ')', array('controller' => 'contacts', 'action' => 'list/rating_one')); ?>
                </li>
                <li role="presentation" class="divider"></li>
                <li role="presentation">
                    <?php echo $this->Html->link('Two Star Rating (' . $ratingOverview[0][0]['twoStarRatingLead'] . ')', array('controller' => 'contacts', 'action' => 'list/rating_two')); ?>
                </li>
                <li role="presentation" class="divider"></li>
                <li role="presentation">
                    <?php echo $this->Html->link('Three Star Rating (' . $ratingOverview[0][0]['threeStarRatingLead'] . ')', array('controller' => 'contacts', 'action' => 'list/rating_three')); ?>
                </li>
                <li role="presentation" class="divider"></li>
                <li role="presentation">
                    <?php echo $this->Html->link('Four Star Rating (' . $ratingOverview[0][0]['fourStarRatingLead'] . ')', array('controller' => 'contacts', 'action' => 'list/rating_four')); ?>
                </li>
                <li role="presentation" class="divider"></li>
                <li role="presentation">
                    <?php echo $this->Html->link('Five Star Rating (' . $ratingOverview[0][0]['fiveStarRatingLead'] . ')', array('controller' => 'contacts', 'action' => 'list/rating_five')); ?>
                </li>
                <li role="presentation" class="divider"></li>
                <li role="presentation">
                    <?php echo $this->Html->link('No Rating (' . $ratingOverview[0][0]['noRatingLead'] . ')', array('controller' => 'contacts', 'action' => 'list/no_rating')); ?>
                </li>
            </ul>
        </div>
    </div>
    <div class="pull-right page-option-right">

        <div class="btn-toolbar" role="toolbar" aria-label="...">
            <button type="button" class="btn btn-theme btn-lg btn-group" data-toggle="modal" data-target="#searchContactModal">
                Search Lead
            </button>

            <?php echo $this->Html->link('Export Lead', array('controller' => 'contacts', 'action' => 'export'), array('class' => 'btn btn-theme  btn-group')) ?>
        </div>

    </div>

</div>
<br>
<br>
<div class="clearfix"></div>

<?php if ($contactList): ?>
    <div class="table-responsive">
        <table class="table data-table table-bordered sort-table" colspecing=0>
            <thead>
            <tr>
                <th>
                    <?php echo $this->Paginator->sort('first_name', 'Name'); ?>
                </th>
                <th>
                    <?php echo $this->Paginator->sort('instructorName', 'Instructor Name'); ?>
                </th>

                <th>
                    <?php echo $this->Paginator->sort('email', 'Email/Phone'); ?>
                </th>
                <th>
                    <?php echo $this->Paginator->sort('rating', 'Rating'); ?>
                </th>
                <th>
                    <?php echo $this->Paginator->sort('Source.name'); ?>
                </th>
                <th>
                    Most Recent Note
                </th>
                <th>
                    Last Contact
                </th>
                <th>
                    FOLLOW UP DATE
                </th>
                <th style="width: 8%;">Action</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($contactList as $contact): ?>
                <tr>
                    <td>
                        <?php echo $this->Html->link($contact['Contact']['first_name'] . ' ' . $contact['Contact']['last_name'], array('controller' => 'contacts', 'action' => 'view', $contact['Contact']['id'])); ?>
                    </td>
                    <td><?php echo $contact[0]['instructorName']; ?></td>
                    <td>
                        <?php
                        echo '<a href="mailto:' . $contact['Contact']['email'] . '">' . $contact['Contact']['email'] . '</a>';
                        if ($contact['Contact']['phone']) {
                            echo '<br/><a href="tel:' . $contact['Contact']['phone'] . '"><i class="italic-sm">(phone: ' . $contact['Contact']['phone'] . ')</i></a>';
                        }
                        ?>
                    </td>
                    <td>
                        <select class="leadsRatings">
                            <option value="" selected="selected"><?php echo $contact['Contact']['id'] ?></option>
                            <?php
                            for ($counter = 1; $counter <= 5; $counter++) {
                                $select = '';
                                if ($counter == $contact['Contact']['rating']) {
                                    $select = 'selected';
                                }
                                echo '<option ' . $select . ' value="' . $counter . '">' . $contact['Contact']['id'] . '</option>';
                            }
                            ?>
                        </select>
                    </td>
                    <td>
                        <?php if ($contact['Source']['name']): ?>
                            <span class="text-success"><?php echo $contact['Source']['name']; ?></span>
                        <?php else: ?>
                            <span class="text-danger">Other</span>
                        <?php endif ?>
                    </td>
                    <td>
                        <?php
                        if (isset($contact['Note'][0]['note'])) {
                            echo $this->Html->link($this->Text->excerpt($contact['Note'][0]['note'], null, 20, ' <span class="green">...more</span>'),
                                array('controller' => 'contacts', 'action' => 'view/' . $contact['Note'][0]['contact_id'] . '#' . $contact['Note'][0]['id']), array('escape' => false));
                        } else {
                            echo 'N/A';
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if (isset($contact['Note'][0]['created'])) {
                            echo '<time>' . $this->Time->format('M d, Y', $contact['Note'][0]['created']);
                            echo ' <i class="italic-sm">(' . $this->Time->format('h:i A', $contact['Note'][0]['created']) . ') <i/></time>';
                        } else {
                            echo ' N/A';
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if (isset($contact['LastFollowupDate'][0]['note_date'])) {
                            echo '<time>' . $this->Time->format('M d, Y', $contact['LastFollowupDate'][0]['note_date']);
                            echo ' <i class="italic-sm">(' . $this->Time->format('h:i A', $contact['LastFollowupDate'][0]['note_date']) . ') <i/></time>';
                        } else {
                            echo ' N/A';
                        }
                        ?>
                    </td>
                    <td>
                        <div class="icons-area">
                            <?php echo $this->Html->link('<i class="fa fa-search-plus"></i>', array('controller' => 'contacts', 'action' => 'view', $contact['Contact']['id']),
                                array('escape' => false, 'class' => 'icon green')); ?>

                            <?php echo $this->Html->link('<i class="fa fa-pencil"></i>', array('controller' => 'contacts', 'action' => 'edit', $contact['Contact']['id']),
                                array('escape' => false, 'class' => 'icon primary')); ?>

                            <?php
                            echo $this->Form->postLink(
                                '<i class="glyphicon glyphicon-remove"></i>',
                                array(
                                    'controller' => 'contacts',
                                    'action' => 'delete',
                                    $contact['Contact']['id']
                                ),
                                array(
                                    'class' => 'icon danger',
                                    'escape' => false
                                ),
                                __('Are you sure you want to delete this lead?')
                            );
                            ?>
                        </div>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
        <?php echo $this->element('pagination'); ?>
    </div>

<?php else: ?>
    <h4 class="not-found">Sorry, lead not found</h4>
<?php endif; ?>

<!-- Modal For Displaying Schedule -->
<div class="modal event-modal fade" id="searchContactModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="border-bottom: 1px solid #e5e5e5;">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                </button>
                <h4 class="sm-title">Search Lead</h4>
            </div>
            <div class="modal-body">
                <!----- ADD NEW CONTACT NOTE ------>
                <div class="">
                    <form action="<?php echo Router::url('/', true); ?>tc/contacts/list" class="form-horizontal form-custom" method="get" accept-charset="utf-8">
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label>Instructor name:</label>
                                <select class="form-control form-inline" name="instructor[]" multiple>
                                    <option value="" selected="selected">Select Instructor</option>
                                    <?php foreach ($instructors as $instructor => $value): ?>
                                        <option  value="<?php echo $instructor; ?>">
                                            <?php echo $value; ?>
                                        </option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-sm-12">
                                <label>Lead name:</label>
                                <input name="contact" class="typeahead form-control" type="text" placeholder="Type Lead name">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12">
                                <label>Lead Email:</label>
                                <input name="email" class="form-control" type="text" placeholder="Type Lead Email">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12">
                                <label>Lead Phone Number:</label>
                                <input name="phone" class="form-control" type="text" placeholder="Type Lead Phone number">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12">
                                <label>Rating</label>
                                <select name="rating" class="form-control">
                                    <option value="" selected="selected">Select Rating</option>
                                    <?php
                                    for ($counter = 1; $counter <= 5; $counter++) {
                                        $select = '';
                                        echo '<option value="'.$counter.'">'.$counter.'</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12">
                                <label>Source</label>
                                <select name="source" class="form-control">
                                    <option value="" selected="selected">Select Source</option>
                                    <?php foreach ($sources as  $source): ?>
                                        <option value="<?php echo $source['Source']['id']; ?>"><?php echo $source['Source']['name']; ?></option>
                                    <?php endforeach;    ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-4">
                                <button class="btn btn-theme">Search</button>
                            </div>
                        </div>
                        <?php echo $this->Form->end(); ?>
                        <div class="clearfix"></div>
                </div>
                <!----- /ADD NEW CONTACT NOTE ------>
            </div>
        </div>
    </div>
</div>
<!-- /Modal For Displaying Schedule -->

<script type="text/javascript">
    $(function () {

        $('.leadsRatings').barrating('show', {
            initialRating: 0,
            theme: 'fontawesome-stars',
            onSelect: function (value, text) {
                $.ajax({
                    url: '<?php echo Router::url('/', true);?>instructor/contacts/set_rating',
                    type: "POST",
                    dataType: "json",
                    data: {contactID: text, rating: value},
                    success: function (response) {
                        if (response == 1) {
                            manageFlashMessage('alert-success', 'Your rating has been successfully complete');
                        }
                        else {
                            manageFlashMessage('alert-danger', 'Sorry, something went wrong');
                        }
                    },
                    error: function (xhr, status, error) {
                        console.log(error);
                    }
                });

            }
        });

    });
</script>

<?php echo $this->Html->script('typehead'); ?>
<script>
    // Instantiate the Bloodhound suggestion engine
    var contacts = new Bloodhound({
        datumTokenizer: function (datum) {
            return Bloodhound.tokenizers.whitespace(datum.value);
        },
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: {
            url: '<?php echo Router::url('/', true);?>tc/contacts/getContacts/contact/%QUERY',
            filter: function (contacts) {
                return $.map(contacts, function (contact) {
                    console.log(contact);
                    return {
                        value: contact['0']['contactName']
                    };
                });
            }
        }
    });

    contacts.initialize();
    // Instantiate the Typeahead UI
    $('.typeahead').typeahead(null, {
        name: 'states',
        displayKey: 'value',
        source: contacts.ttAdapter()
    });
</script>

