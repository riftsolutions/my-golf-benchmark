<?php
/**
 * @var $this View
 */
?>
<h2 class="page-title">Lead Details</h2>
<hr class="shadow-line"/>
<div class="row">
    <div class="col-lg-6 col-md-6 col-sm-12">
        <div class="d-box">
            <h4 class="d-title"><i class="fa fa-filter purple"></i> Contact Details</h4>
            <ul class="d-list">
                <li>
                    <strong>Contact Name:</strong>
                    <?php
                    echo $this->Html->link($contact['Contact']['first_name']. ' '. $contact['Contact']['last_name'], array('controller' => 'contacts', 'action' => 'view', $contact['Contact']['id']));
                    ?>
                </li>
                <li>
                    <strong>Email:</strong>
                    <?php
                    echo $contact['Contact']['email'];
                    ?>
                </li>
                <li>
                    <strong>Phone:</strong>
                    <?php
                    echo $contact['Contact']['phone']
                    ?>
                </li>
                <li>
                    <strong>Account Type:</strong>
                    <?php
                    if($contact['Contact']['account_type'] == 1){
                        echo '<label class="label label-info">student</label>';
                    }
                    elseif($contact['Contact']['account_type'] == 2){
                        echo '<label class="label label-warning">customer</label>';
                    }
                    elseif($contact['Contact']['account_type'] == 3){
                        echo '<label class="label label-success">lead/prospect</label>';
                    }
                    else{
                        echo '<label class="label label-default">N/A</label>';
                    }
                    ?>
                </li>

                <li>
                    <strong>Follow Date:</strong>
                    <?php
                    echo $contact['Contact']['follow_up_date'];
                    ?>
                </li>

                <li>
                    <strong>Source:</strong>
                    <?php
                    echo $contact['Contact']['source'];
                    ?>
                </li>
                </li>

                <li>
                    <strong>Comment:</strong>
                    <?php
                    echo $contact['Contact']['comments'];
                    ?>
                </li>
            </ul>
        </div>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-12">
        <div class="d-box">
            <h4 class="d-title"><i class="fa fa-file-text danger"></i> Quick Note</h4>
            <br/>
            <br/>
            <?php echo $this->Form->create(
                'Contact',
                array('controller' => 'contacts', 'action' => 'view/'.$contact['Contact']['id'], 'class' => 'form-horizontal form-custom')
            ); ?>
            <div class="form-group">
                <label class="col-sm-4 control-label">Date</label>
                <div class="col-sm-6">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                        <input id="datepicker" name="data[Note][note_date]" type="text" class="form-control" placeholder="Date">
                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                    </div>
                    <?php echo $this->Form->error('Note.note_date');?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4 control-label">Note</label>

                <div class="col-sm-8">
                    <?php
                    echo $this->Form->input(
                        'Note.note',
                        array(
                            'type' => 'textarea',
                            'class' => 'form-control',
                            'placeholder' => 'Place your note here',
                            'label' => false,
                            'required' => false,
                        )
                    );
                    ?>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-4 col-sm-offset-4">
                    <button class="btn btn-theme">Save Note</button>
                </div>
            </div>
            <?php echo $this->Form->end(); ?>
        </div>
    </div>

    <?php if($notes):?>
    <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="d-box-md margin-top-25">
            <h4 class="d-title"><i class="fa fa-file-text purple"></i> Note</h4>
            <ul class="d-list">
                <?php foreach($notes as $note):?>
                <li id="<?php echo $note['Note']['id'];?>">
                    <strong><?php echo $this->Time->format('d M, Y', $note['Note']['note_date'])?></strong>
                    <br/>
                    <a name="<?php echo $note['Note']['id'];?>"><?php echo $note['Note']['note'];?></a>
                </li>
                <?php endforeach;?>
            </ul>
        </div>
    </div>
    <?php endif;?>
</div>


<script>
    $(document).ready(function () {
        $('#datepicker').datepicker()

        var fullUrl = window.location.href.split('#');
        var noteID = fullUrl[1];
        $('#'+noteID).addClass('current-note');
    });
</script>