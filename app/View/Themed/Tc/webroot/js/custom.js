/****** Dynamically Set The height of sidebar ******************/
$(document).ready(function() {
    var contentAreaHeight = $("#content-area").innerHeight();
    var windowsHeight = window.innerHeight;

    if(contentAreaHeight > windowsHeight){
        sidebarHeight = contentAreaHeight;
    }
    else{
        sidebarHeight = windowsHeight;
    }

    $(".sidebar").css("height", sidebarHeight + "px");
});
/****** Dynamically Set The height of sidebar ******************/


$(document).ready(function() {
    $('[data-toggle=offcanvas]').click(function() {
        $('.row-offcanvas').toggleClass('active');
    });
});





$(function () {
    $('#menu').metisMenu({
        toggle: false // disable the auto collapse. Default: true.
    });
});

;(function ($, window, document, undefined) {

    var pluginName = "metisMenu",
        defaults = {
            toggle: true
        };

    function Plugin(element, options) {
        this.element = element;
        this.settings = $.extend({}, defaults, options);
        this._defaults = defaults;
        this._name = pluginName;
        this.init();
    }

    Plugin.prototype = {
        init: function () {

            var $this = $(this.element),
                $toggle = this.settings.toggle;

            $this.find('li.active').has('ul').children('ul').addClass('collapse in');
            $this.find('li').not('.active').has('ul').children('ul').addClass('collapse');

            $this.find('li').has('ul').children('a').on('click', function (e) {
                e.preventDefault();

                $(this).parent('li').toggleClass('active').children('ul').collapse('toggle');

                if ($toggle) {
                    $(this).parent('li').siblings().removeClass('active').children('ul.in').collapse('hide');
                }
            });
        }
    };

    $.fn[ pluginName ] = function (options) {
        return this.each(function () {
            if (!$.data(this, "plugin_" + pluginName)) {
                $.data(this, "plugin_" + pluginName, new Plugin(this, options));
            }
        });
    };

})(jQuery, window, document);

$(window).bind("load", function() {
    var footer = $("footer");
    var pos = footer.position();
    var height = $(window).height();
    height = height - pos.top;
    height = height - footer.height();
    if (height > 0) {
        footer.css({'margin-top' : height+'px'});
    }
});


/*---- ADD Attachment -----*/
$(document).ready(function () {
    var counter = 1;
    $("#addAttachment").on("click", function () {
        $("#addAllCatBrands").slideUp();
        counter++;
        var newRow = $("<tr>");
        var cols = "";
        cols += '<td>' +
            '<input class="form-control" name="data[Attachment][][attachment]" id="phone_number" type="file">' +
            '</td>';
        cols += '<td style="text-align: right; height: 55px;"><a class="deleteRow danger"><i class="glyphicon glyphicon-remove"></i></td>';
        newRow.append(cols);
        $("table.addAttachment").append(newRow);
    });
    $("table.addAttachment").on("click", "a.deleteRow", function (event) {
        $(this).closest("tr").remove();
        counter--;
        if (counter < 2) {
            $("#addAllCatBrands").slideDown();
        }
    });
});
/*---- ADD Attachment -----*/


function manageFlashMessage(alertType, message)
{
    $('#message-box').show();
    $('#message-box').addClass(alertType);
    $('#message-box').addClass('animated slideInDown');
    $('#message').html(message);
    removeMgsArea();
}
function removeMgsArea()
{
    window.setTimeout(function ()
        {
            if($( "#message-box" ).hasClass( "alert-danger" )){
                $('#message-box').removeClass("alert-danger" );
            }
            if($( "#message-box" ).hasClass( "alert-success" )){
                $('#message-box').removeClass("alert-success" );
            }
            $("#message-box").hide();
        },
        2500
    );
}

$(document).ready(function () {
    $('.datepicker').datepicker();
    $('#datepicker').datepicker();
    $('#datepicker1').datepicker();
    $('#datepicker2').datepicker();
    $('#datepicker3').datepicker();
    $('#datepicker4').datepicker();

    $('#timepicker').datepicker();
});



$('.custom-source-btn').click(function(){

    $('.default-source').hide();
    $('.custom-source-btn').hide();
    $('.is_custom').val(1);

    $('.default-source-btn').show();
    $('.default-source-btn').addClass('animated fadeInDown');
    $('.custom-source').show();
    $('.custom-source').addClass('animated fadeInDown');
});

$('.default-source-btn').click(function(){

    $('.default-source-btn').hide();
    $('.custom-source').hide();
    $('.is_custom').val(0);

    $('.default-source').show();
    $('.default-source').addClass('animated fadeInUp');
    $('.custom-source-btn').show();
    $('.custom-source-btn').addClass('animated fadeInUp');

});