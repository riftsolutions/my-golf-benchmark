$(document).ready(function() {
    $('#username').blur(function (){
        $.post(
            '/golf-swing-prescription/users/test',
            {field: $('#username').attr('id'), value: $('#username').val()},
            handleEmailValidation
        );
    });

    function handleEmailValidation(error){
        if($('#emailNotEmpty').length == 0){
            $('#username').before($('<div id="emailNotEmpty" class="error-msg">'+ error +'</div>').fadeIn(500));
        }
        else{
            $('#emailNotEmpty').remove()
        }
    }
});