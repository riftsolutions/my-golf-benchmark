<?php
/**
 * @var $this View
 */
?>
<h2 class="page-title">Appointment Details</h2>
<hr class="shadow-line"/>
<div class="pull-right">
    <?php echo $this->Utilities->appointmentStatusButton($appointmentDetails['Appointment']['status']);?>
</div>
<div class="clearfix"></div>

<div class="row margin-top-25">
    <div class="col-lg-7 col-md-7 col-sm-12">
        <div class="clearfix"></div>
        <div class="d-box-md">
            <h4 class="d-title"><i class="fa fa-filter purple"></i> Appointment Details</h4>
            <ul class="d-list">
                <li>
                    <strong>Student Name:</strong>
                    <?php
                    echo $this->Html->link($appointmentDetails['Profile']['first_name']. ' '. $appointmentDetails['Profile']['last_name'], array('controller' => 'users', 'action' => 'details', $appointmentDetails['User']['uuid']));
                    ?>
                </li>
                <li>
                    <strong>Email:</strong>
                    <?php
                    echo $appointmentDetails['User']['username'];
                    ?>
                </li>
                <li>
                    <strong>Phone:</strong>
                    <?php
                    echo $appointmentDetails['Profile']['phone'];
                    ?>
                </li>
                <li>
                    <strong>City:</strong>
                    <?php
                    echo $appointmentDetails['Profile']['first_name'];
                    ?>
                </li>
                <li>
                    <strong>State:</strong>
                    <?php
                    echo $appointmentDetails['Profile']['state'];
                    ?>
                </li>
                <li>
                    <strong>Postal Code:</strong>
                    <?php
                    echo $appointmentDetails['Profile']['postal_code'];
                    ?>
                </li>
                <li>
                    <strong>Appointment Date:</strong>
                    <?php
                    echo $this->Time->format('M d, Y', $appointmentDetails['Appointment']['start']);
                    ?>
                </li>
                <li>
                    <strong>Appointment Time:</strong>
                    <?php
                    echo $this->Time->format('h:m a', $appointmentDetails['Appointment']['start']). ' - '. $this->Time->format('h:m a', $appointmentDetails['Appointment']['end']);
                    ?>
                </li>
                <li>
                    <strong>Package:</strong>
                    <?php
                    echo $this->Html->link($appointmentDetails['Package']['name'], array('controller' => 'packages', 'action' => 'view', $appointmentDetails['Package']['uuid']))
                    ?>
                </li>
                <li>
                    <strong>Lesson No:</strong>
                    <?php
                    echo $appointmentDetails['Appointment']['lesson_no'];
                    ?>
                </li>
            </ul>

            <br/>
            <?php

            if($appointmentDetails['Appointment']['status'] == 1){
                echo $this->Form->postLink(
                    'Confirm Appointment',
                    array(
                        'controller' => 'appointments',
                        'action' => 'appointmentActions',
                        $appointmentDetails['Appointment']['id'],
                        2
                    ),
                    array(
                        'class' => 'btn btn-theme-square',
                        'escape' => false
                    ),
                    __('Are you sure you want to confirm this appointment')
                );

                echo $this->Form->postLink(
                    'Cancel Appointment',
                    array(
                        'controller' => 'appointments',
                        'action' => 'appointmentActions',
                        $appointmentDetails['Appointment']['id'],
                        3
                    ),
                    array(
                        'class' => 'btn btn-black margin-left-10',
                        'escape' => false
                    ),
                    __('Are you sure you want to cancelled this appointment')
                );
            }
            if($appointmentDetails['Appointment']['status'] == 2){
                echo $this->Html->link('Benchmark Now', array('controller' => 'benchmarks', 'action' => 'create', $appointmentDetails['Appointment']['uuid']), array('class' => 'btn btn-theme-square'));
            }

            if($appointmentDetails['Benchmark']['uuid']){
                echo $this->Html->link('View benchmark', array('controller' => 'benchmarks', 'action' => 'view', $appointmentDetails['Benchmark']['uuid']), array('class' => 'btn btn-theme-square'));
            }
            ?>

        </div>
    </div>
    <div class="col-lg-5 col-md-5 col-sm-12">
        <!----- WAITING LIST ------>
        <div class="d-box">
            <h4 class="d-title padding-bottom-10">
                <i class="fa fa-clock-o default"></i> Waiting List
                <?php
                if($waitingList){
                    echo $this->html->link('See Full List', array('controller' => 'appointments', 'action' => 'waiting'), array('escape' => false, 'class' => 'full-list pull-right'));
                }
                ?>
            </h4>

            <?php if($waitingList):?>
                <table class="table">
                    <thead>
                    <th>Student Name</th>
                    <th>Package Name</th>
                    <th style="text-align: right;">Date</th>
                    </thead>
                    <tbody>
                    <?php foreach($waitingList as $waiting):?>
                        <tr>
                            <td><?php echo $this->Html->link($waiting['Profile']['first_name'].' '. $waiting['Profile']['last_name'], array('controller' => 'users', 'action' => 'details', $waiting['User']['uuid']), array('class' => 'green'));?></td>
                            <td>
                                <?php echo $this->Html->link($waiting['Package']['name'], array('controller' => 'packages', 'action' => 'view', $waiting['Package']['uuid']));?>
                                <i class="italic-sm">(lesson no: <?php echo $waiting['Waiting']['lesson_no']?>)</i>
                            </td>
                            <td style="text-align: right;">
                                <?php echo $this->Time->format('M d, Y', $waiting['Waiting']['start']);?>
                                <span class="italic-sm">( <?php echo $this->Time->format('h:m a', $waiting['Waiting']['start'])?> - <?php echo $this->Time->format('h:m a', $waiting['Waiting']['end'])?>)</span>
                            </td>
                        </tr>
                    <?php endforeach;?>

                    </tbody>
                </table>

            <?php else:?>
                <?php echo $this->element('not-found')?>
            <?php endif;?>

            <div class="clearfix"></div>
        </div>
        <!----- /WAITING LIST ------>
    </div>
</div>

