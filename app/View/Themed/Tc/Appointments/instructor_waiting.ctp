<?php
/**
 * @var $this View
 */
?>
    <h2 class="page-title"><?php echo $title;?></h2>
    <hr class="shadow-line"/>
    <div class="dropdown margin-bottom-5">
        <button class="btn btn-info dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown">
            Quick Navigation
            <span class="caret"></span>
        </button>
        <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
            <li role="presentation">
                <?php echo $this->Html->link('Waiting', array('controller' => 'appointments', 'action' => 'waiting/waiting'));?>
            </li>
            <li role="presentation" class="divider"></li>
            <li role="presentation">
                <?php echo $this->Html->link('Expired', array('controller' => 'appointments', 'action' => 'waiting/expired'));?>
            </li>
            <li role="presentation" class="divider"></li>
            <li role="presentation">
                <?php echo $this->Html->link('Cancelled', array('controller' => 'appointments', 'action' => 'waiting/cancelled'));?>
            </li>
        </ul>
    </div>
<?php if($appointments):?>
    <div class="table-responsive">
        <table class="table data-table table-bordered sort-table">
            <thead>
            <tr>
                <th><?php echo $this->Paginator->sort('Instructor.first_name', 'Student Name'); ?></th>
                <th><?php echo $this->Paginator->sort('Package.name', 'Package Name'); ?></th>
                <th><?php echo $this->Paginator->sort('Waiting.lesson_no', 'Lesson No'); ?></th>
                <th><?php echo $this->Paginator->sort('Waiting.start', 'Appointment Data and Time'); ?></th>
                <th><?php echo $this->Paginator->sort('Waiting.status', 'Status'); ?></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($appointments as $appointment):?>
                <tr>
                    <td>
                        <?php echo $this->Html->link($appointment['Instructor']['first_name']. ' '. $appointment['Instructor']['last_name'], array('controller' => 'users', 'action' => 'details', $appointment['InstructorUser']['uuid']));?>
                    </td>
                    <td>
                        <?php echo $this->Html->link($appointment['Package']['name'], array('controller' => 'packages', 'action' => 'view', $appointment['Package']['uuid']));?>
                    </td>
                    <td>
                        <?php echo $appointment['Waiting']['lesson_no']; ?>
                    </td>
                    <td>
                        <?php echo $this->Time->format('M d, Y', $appointment['Waiting']['start']);?>
                        <span class="italic-sm">( <?php echo $this->Time->format('h:m a', $appointment['Waiting']['start'])?> - <?php echo $this->Time->format('h:m a', $appointment['Waiting']['end'])?>)</span>
                    </td>
                    <td>
                        <?php
                        if($appointment['Waiting']['status'] == 1){
                            echo '<label class="label label-warning">waiting</label>';
                        }
                        elseif($appointment['Waiting']['status'] == 2){
                            echo '<label class="label label-info">awaiting for confirmation</label>';
                        }
                        elseif($appointment['Waiting']['status'] == 3){
                            echo '<label class="label label-success">converted into appointment</label>';
                        }
                        elseif($appointment['Waiting']['status'] == 4){
                            echo '<label class="label label-danger">expired</label>';
                        }
                        elseif($appointment['Waiting']['status'] == 5){
                            echo '<label class="label label-danger">cancelled</label>';
                        }
                        elseif($appointment['Waiting']['status'] == 6){
                            echo '<label class="label label-danger">invalid</label>';
                        }
                        else{
                            echo '<label class="label label-danger">N/A</label>';
                        }
                        ?>
                    </td>
                </tr>
            <?php endforeach;?>
            </tbody>
        </table>
        <?php echo $this->element('pagination')?>
    </div>
<?php else:?>
    <h4 class="not-found"><?php echo $notFount; ?></h4>
<?php endif;?>