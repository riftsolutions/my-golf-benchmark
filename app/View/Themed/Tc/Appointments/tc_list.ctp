<?php
/**
 * @var $this View
 */
?>
    <h2 class="page-title"><?php echo $title; ?></h2>
    <hr class="shadow-line"/>
    <div class="page-option">
        <div class="pull-left page-option-left dropdown">
            <button class="btn btn-info dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown">
                Quick Navigation
                <span class="caret"></span>
            </button>
            <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                <li role="presentation">
                    <?php echo $this->Html->link('All Appointment (' . $appointmentOverview[0][0]['totalAppointment'] . ')', array('controller' => 'appointments', 'action' => 'list')); ?>
                </li>
                <li role="presentation" class="divider"></li>
                <li role="presentation">
                    <?php echo $this->Html->link('Rescheduled Appointment (' . $appointmentOverview[0][0]['countRescheduledAppointment'] . ')', array('controller' => 'appointments', 'action' => 'list', 'reschedule')); ?>
                </li>
                <li role="presentation" class="divider"></li>
                <li role="presentation">
                    <?php echo $this->Html->link('Pending Appointment (' . $appointmentOverview[0][0]['countPendingAppointment'] . ')', array('controller' => 'appointments', 'action' => 'list', 'pending')); ?>
                </li>
                <li role="presentation" class="divider"></li>
                <li role="presentation">
                    <?php echo $this->Html->link('Upcoming Appointment (' . $appointmentOverview[0][0]['countConfirmAppointment'] . ')', array('controller' => 'appointments', 'action' => 'list', 'upcoming')); ?>
                </li>
                <li role="presentation" class="divider"></li>
                <li role="presentation">
                    <?php echo $this->Html->link('Cancelled Appointment (' . $appointmentOverview[0][0]['countCancelledAppointment'] . ')', array('controller' => 'appointments', 'action' => 'list', 'cancelled')); ?>
                </li>
                <li role="presentation" class="divider"></li>
                <li role="presentation">
                    <?php echo $this->Html->link('Completed Appointment (' . $appointmentOverview[0][0]['countCompletedAppointment'] . ')', array('controller' => 'appointments', 'action' => 'list', 'completed')); ?>
                </li>
                <li role="presentation" class="divider"></li>
                <li role="presentation">
                    <?php echo $this->Html->link('Incomplete Appointment (' . $appointmentOverview[0][0]['countExpiredAppointment'] . ')', array('controller' => 'appointments', 'action' => 'list', 'expired')); ?>
                </li>
            </ul>
        </div>
        <div class="pull-right page-option-right">
            <div class="btn-toolbar" role="toolbar" aria-label="...">
                <button type="button" class="btn btn-theme btn-lg margin-left-15" data-toggle="modal" data-target="#searchAppointmentModal">
                    Search
                </button>
            </div>
        </div>
    </div>
    <br>
    <br>
<?php if ($appointments): ?>
    <div class="table-responsive">
        <table class="table data-table table-bordered sort-table">
            <thead>
            <tr>
                <th><?php echo $this->Paginator->sort('Profile.first_name. Profile.last_name', 'Student Name'); ?></th>
                <th><?php echo $this->Paginator->sort('0.InstructorName', 'Instructor'); ?></th>
                <th><?php echo $this->Paginator->sort('Package.name', 'Package Name'); ?></th>
                <th><?php echo $this->Paginator->sort('Appointment.lesson_no', 'Lesson No'); ?></th>
                <th><?php echo $this->Paginator->sort('Appointment.start', 'Appointment Data and Time'); ?></th>
                <th><?php echo $this->Paginator->sort('Appointment.status', 'Status'); ?></th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($appointments as $appointment): ?>
                <tr>
                    <td>
                        <?php echo $this->Html->link($appointment['Profile']['first_name'] . ' ' . $appointment['Profile']['last_name'],
                            array('controller' => 'users', 'action' => 'details', $appointment['Profile']['user_id']), array('class' => 'green')); ?>
                    </td>
                    <td>
                        <?php echo $this->Html->link($appointment['0']['instructorName'], array('controller' => 'users', 'action' => 'details', $appointment['InstructorUser']['uuid']), array('class' => 'green')); ?>
                    </td>
                    <td>
                        <?php echo $this->Html->link($appointment['Package']['name'], array('controller' => 'packages', 'action' => 'view', $appointment['Package']['uuid'])); ?>
                    </td>
                    <td>
                        <?php echo $appointment['Appointment']['lesson_no']; ?>
                    </td>
                    <td>
                        <?php echo $this->Time->format('M d, Y', $appointment['Appointment']['start']); ?>
                        <span class="italic-sm">( <?php echo $this->Time->format('h:m a', $appointment['Appointment']['start']) ?> - <?php echo $this->Time->format('h:m a', $appointment['Appointment']['end']) ?>)</span>
                    </td>
                    <td>
                        <?php
                        if ($appointment['Appointment']['status'] == 1) {
                            echo '<label class="label label-gray">pending</label>';
                        } elseif ($appointment['Appointment']['status'] == 2) {
                            echo '<label class="label label-success">confirm/upcoming</label>';
                        } elseif ($appointment['Appointment']['status'] == 3) {
                            echo '<label class="label label-danger">cancelled</label>';
                        } elseif ($appointment['Appointment']['status'] == 4) {
                            echo '<label class="label label-info">Complete</label>';
                        } elseif ($appointment['Appointment']['status'] == 5) {
                            echo '<label class="label label-warning">Incomplete</label>';
                        } else {
                            echo '<label class="label label-danger">N/A</label>';
                        }

                        if ($appointment['Appointment']['is_benchmarked'] == 1 && ($appointment['Appointment']['status'] == 4)) {
                            echo '<label class="label label-default margin-left-5">benchmark complete</label>';
                        }

                        if (($appointment['Appointment']['is_reschedule'] == 1)) {
                            echo '<label class="label label-default margin-left-5">reschedule</label>';
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        echo $this->Html->link('Details', array('controller' => 'appointments', 'action' => 'view', $appointment['Appointment']['id']), array('class' => 'd-btn'));
                        ?>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
        <?php echo $this->element('pagination') ?>
    </div>
<?php else: ?>
    <h4 class="not-found">Sorry, appointment list empty</h4>
<?php endif; ?>


<!-- Modal For Displaying Schedule -->
<div class="modal event-modal fade" id="searchAppointmentModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="border-bottom: 1px solid #e5e5e5;">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                </button>
                <h4 class="sm-title">Search Appointments</h4>
            </div>
            <div class="modal-body">
                <!----- ADD NEW CONTACT NOTE ------>
                <div class="">
                    <?php echo $this->Form->create(
                        'Appointment',
                        array('type' => 'GET', 'controller' => 'appointments', 'action' => 'tc_list', 'class' => 'form-horizontal form-custom')
                    ); ?>

                    <div class="form-group">
                        <div class="col-sm-12">
                            <label>Instructor name:</label>
                            <select class="form-control form-inline" name="instructor[]" multiple>
                                <option value="" selected="selected">Select Instructor</option>
                                <?php foreach ($instructors as $instructor => $value): ?>
                                    <option  value="<?php echo $instructor; ?>">
                                        <?php echo $value; ?>
                                    </option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-12">
                            <label>Student name:</label>
                            <input name="student" class="typeahead form-control" type="text" placeholder="Type student name">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label>Package:</label>
                            <input name="package" class="packageTypeahead form-control" type="text" placeholder="Type package name">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-12"><label>Date Range</label></div>
                        <div class="clearfix"></div>
                        <div class="col-sm-6">
                            <div class="input-group">
                                <input id="datepicker" name="appointment_starting_date" type="text" class="form-control" placeholder="Appointment Starting">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="input-group">
                                <input id="datepicker2" name="appointment_ending_date" type="text" class="form-control" placeholder="Appointment Ending">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-12"><label>Time Range</label></div>
                        <div class="col-lg-6">
                            <div class="input-group">
                                <select name="appointment_starting_time" class="form-control" id="GroupLessonStart">
                                    <?php echo $this->Utilities->getFromHours('Starting Time'); ?>
                                </select>
                                <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="input-group">
                                <select name="appointment_ending_time" class="form-control" id="GroupLessonEnd">
                                    <?php echo $this->Utilities->getFromHours('Ending Time'); ?>
                                </select>
                                <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label>Appointment Status:</label>
                            <select name="status" class="form-control">
                                <option>Choose Status</option>
                                <option value="pending">Pending</option>
                                <option value="upcoming">Upcoming</option>
                                <option value="cancelled">Cancelled</option>
                                <option value="completed">Completed</option>
                                <option value="expired">Expired</option>
                                <option value="reschedule">Reschedule</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-4">
                            <button class="btn btn-theme">Search</button>
                        </div>
                    </div>
                    <?php echo $this->Form->end(); ?>
                    <div class="clearfix"></div>
                </div>
                <!----- /ADD NEW CONTACT NOTE ------>
            </div>
        </div>
    </div>
</div>
<!-- /Modal For Displaying Schedule -->
<?php echo $this->Html->script('typehead'); ?>
<script>
    // Instantiate the Bloodhound suggestion engine
    var contacts = new Bloodhound({
        datumTokenizer: function (datum) {
            return Bloodhound.tokenizers.whitespace(datum.value);
        },
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: {
            url: '<?php echo Router::url('/', true);?>tc/contacts/getContacts/student/%QUERY',
            filter: function (contacts) {
                return $.map(contacts, function (contact) {
                    console.log(contact);
                    return {
                        value: contact['0']['contactName']
                    };
                });
            }
        }
    });

    contacts.initialize();
    // Instantiate the Typeahead UI
    $('.typeahead').typeahead(null, {
        name: 'states',
        displayKey: 'value',
        source: contacts.ttAdapter()
    });


    // Instantiate the Bloodhound suggestion engine
    var packages = new Bloodhound({
        datumTokenizer: function (datum) {
            return Bloodhound.tokenizers.whitespace(datum.value);
        },
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: {
            url: '<?php echo Router::url('/', true);?>instructor/packages/getPackages/%QUERY',
            filter: function (packages) {
                return $.map(packages, function (package) {
                    return {
                        value: package['Package']['name']
                    };
                });
            }
        }
    });

    console.log(packages);
    packages.initialize();
    $('.packageTypeahead').typeahead(null, {
        name: 'states',
        displayKey: 'value',
        source: packages.ttAdapter()
    });

</script>
