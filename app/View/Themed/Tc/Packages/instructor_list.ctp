<?php
/**
 * @var $this View
 */
?>
<h2 class="page-title">List of Package</h2>
<hr class="shadow-line"/>

<div class="dropdown pull-left">
    <button class="btn btn-info dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown">
        Quick Navigation
        <span class="caret"></span>
    </button>
    <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
        <li role="presentation">
            <?php echo $this->Html->link('Default Package (' .$packageOverview[0][0]['defaultPackage']. ')', array('controller' => 'packages', 'action' => 'list/default'));?>
        </li>
        <li role="presentation" class="divider"></li>
        <li role="presentation">
            <?php echo $this->Html->link('Your Created Package (' .($packageOverview[0][0]['totalPackage'] - $packageOverview[0][0]['defaultPackage']). ')', array('controller' => 'packages', 'action' => 'list/users'));?>
        </li>
        <li role="presentation" class="divider"></li>
        <li role="presentation">
            <?php echo $this->Html->link('All Package (' .$packageOverview[0][0]['totalPackage']. ')', array('controller' => 'packages', 'action' => 'list'));?>
        </li>
    </ul>
</div>

<div class="pull-right">
    <?php echo $this->Html->link('Create New Package', array('controller' => 'packages', 'action' => 'create'), array('class' => 'btn btn-theme'))?>

    <?php echo $this->Html->link('Package List', array('controller' => 'packages', 'action' => 'list'), array('class' => 'btn btn-theme'))?>
</div>
<div class="clearfix"></div>
<br/>
<div class="table-responsive">
    <?php if ($packages): ?>
        <table class="table data-table table-bordered sort-table" colspecing=0>
            <thead>
            <tr>
                <th><?php echo $this->Paginator->sort('Package.name', 'Name of Package'); ?></th>
                <th><?php echo $this->Paginator->sort('Package.price', 'New Price'); ?></th>
                <th><?php echo $this->Paginator->sort('Package.tax', 'Tax'); ?></th>
                <th><?php echo $this->Paginator->sort('Package.total', 'Total'); ?></th>
                <th><?php echo $this->Paginator->sort('Package.lesson', 'Number of Lesson'); ?></th>
                <th><?php echo $this->Paginator->sort('Package.expiration_days', 'Expiration Days'); ?></th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($packages as $package):?>
                <tr>
                    <td>
                        <?php echo $this->Html->link($package['Package']['name'], array('controller' => 'packages', 'action' => 'view', $package['Package']['uuid']), array('class' => 'green'));?>

                        <?php if($package['Package']['is_default']):?>
                            <label class="label label-info">default</label>
                        <?php endif;?>
                    </td>
                    <td>
                        <?php echo $this->Number->currency($package['Package']['price']);?>
                    </td>
                    <td>
                        <?php echo $this->Number->currency($package['Package']['tax']);?>
                        <i class="italic-md u-l">(Tax <?php echo $package['Package']['tax_percentage']?>%)</i>
                    </td>
                    <td>
                        <?php echo $this->Number->currency($package['Package']['total']);?>
                    </td>
                    <td>
                        <?php echo $package['Package']['lesson'];?>
                    </td>
                    <td>
                        <?php echo $package['Package']['expiration_days'];?>
                    </td>
                    <td>
                        <?php echo $this->Html->link('View', array('controller' => 'packages', 'action' => 'view', $package['Package']['uuid']), array('class' => 'btn d-btn'));?>

                        <?php echo $this->Html->link('Edit', array('controller' => 'packages', 'action' => 'edit', $package['Package']['uuid']), array('class' => 'btn d-btn'));?>

                        <?php echo $this->Html->link('Assign to Student', array('controller' => 'packages', 'action' => 'assign', $package['Package']['uuid']), array('class' => 'btn d-btn'));?>

                        <?php
                        echo $this->Form->postLink(
                            'Delete',
                            array(
                                'controller' => 'packages',
                                'action' => 'delete',
                                $package['Package']['uuid']
                            ),
                            array(
                                'class' => 'btn d-btn',
                                'escape' => false
                            ),
                            __('Are you sure you want to delete this package')
                        );
                        ?>

                    </td>
                </tr>
            <?php endforeach;?>
            </tbody>
        </table>
        <?php echo $this->element('pagination');?>
    <?php else: ?>
        <h2 class="not-found">Sorry, package not found</h2>
        <hr class="shadow-line"/>
    <?php endif; ?>
</div>
<div class="clearfix"></div>