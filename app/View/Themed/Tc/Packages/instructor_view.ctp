<?php
/**
 * @var $this View
 */
?>
<h2 class="page-title">Package Details</h2>
<hr class="shadow-line"/>
<div class="pull-right">
    <?php echo $this->Html->link('Create New Package', array('controller' => 'packages', 'action' => 'create'), array('class' => 'btn btn-theme'))?>

    <?php echo $this->Html->link('Assign Package', array('controller' => 'packages', 'action' => 'assign', $package['Package']['uuid']), array('class' => 'btn btn-theme'))?>

    <?php echo $this->Html->link('Package List', array('controller' => 'packages', 'action' => 'list'), array('class' => 'btn btn-theme'))?>
</div>
<div class="clearfix"></div>
<div class="row">
    <div class="col-md-8 col-md-offset-2 col-sm-12">
        <div class="box box-padding-adding radius-5 margin-top-25 margin-bottom-25">
            <?php echo $this->Html->link('<i class="fa fa-pencil"></i> Update Package', array('controller' => 'packages', 'action' => 'edit', $package['Package']['uuid']), array('escape' => false, 'class' => 'fly-edit-common'));?>
            <h2 class="page-title"><?php echo $package['Package']['name']?></h2>
            <ul class="d-list">
                <li>
                    <?php echo $package['Package']['description']?>
                </li>
                <li><strong>Number of lesson:</strong> <?php echo $package['Package']['lesson']?></li>
                <li>
                    <strong>Price:</strong> <?php echo $this->Number->currency($package['Package']['total']);?>
                    <i class="italic-sm">(Including <?php echo $package['Package']['tax_percentage'];?>% tax, tax amount <?php echo $this->Number->currency($package['Package']['tax'])?>)</i>
                </li>
                <li>
                    <strong>Expiration Days:</strong> <?php echo $package['Package']['expiration_days'];?> Days
                    <i class="italic-sm">(Expiration starts counting from the time of purchase)</i>
                </li>
            </ul>

        </div>
    </div>

</div>