<?php
/**
 * @var $this View
 */
?>
<h2 class="page-title">Create Package</h2>
<hr class="shadow-line"/>
<div class="pull-right">
    <?php echo $this->Html->link('Create New Package', array('controller' => 'packages', 'action' => 'create'), array('class' => 'btn btn-theme'))?>

    <?php echo $this->Html->link('Package List', array('controller' => 'packages', 'action' => 'list'), array('class' => 'btn btn-theme'))?>
</div>
<div class="clearfix"></div>

<div class="row">
    <div class="col-md-8 col-md-offset-2 col-sm-12">
        <div class="box box-padding-adding radius-5 margin-top-25 margin-bottom-25">
            <?php echo $this->Form->create(
                'Package',
                array(
                    'controller' => 'packages',
                    'action' => 'create',
                    'type' => 'file',
                    'method' => 'post',
                    'class' => 'form-horizontal form-custom'
                )
            ); ?>

            <div class="form-group">
                <label class="col-sm-12">Package Name</label>

                <div class="col-sm-9">
                    <?php echo $this->Form->input(
                        'Package.name',
                        array(
                            'type' => 'text',
                            'label' => false,
                            'class' => 'form-control',
                            'placeholder' => 'Package Name',
                            'required' => false
                        )
                    ); ?>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-12">Description</label>

                <div class="col-sm-12">
                    <script src="//tinymce.cachefly.net/4.1/tinymce.min.js"></script>
                    <script>tinymce.init({selector:'textarea', menubar : false, height:270});</script>
                    <textarea name="data[Package][description]"></textarea>
                    <?php echo $this->Form->error('Package.description');?>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-12">Number of Lesson</label>

                <div class="col-sm-4">
                    <?php
                    echo $this->Form->input(
                        'Package.lesson',
                        array(
                            'type' => 'text',
                            'class' => 'form-control',
                            'label' => false,
                            'div' => false,
                            'placeholder' => 'Number of lesson',
                            'required' => false,
                        )
                    )
                    ?>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-12">Price</label>

                <div class="col-sm-4">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-money"></i></span>
                        <input name="data[Package][price]" type="text" class="form-control" placeholder="Price">
                    </div>
                    <?php echo $this->Form->error('Package.price'); ?>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-12">Tax Percentage</label>

                <div class="col-sm-4">
                    <div class="input-group">
                        <span class="input-group-addon">%</span>
                        <input name="data[Package][tax_percentage]" type="text" class="form-control" placeholder="Tax Percentage">
                    </div>
                    <?php echo $this->Form->error('Package.tax_percentage'); ?>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-12">Expiration Days <i class="italic-sm">
                        (Expiration starts counting from the time of purchase)
                    </i></label>

                <div class="col-sm-3">
                    <div class="input-group">
                        <input name="data[Package][expiration_days]" type="text" class="form-control" placeholder="Expiration Days">
                        <span class="input-group-addon">Days</span>
                    </div>
                    <?php echo $this->Form->error('Package.expiration_days'); ?>
                </div>
            </div>

            <div class="form-group">
                <div class="col-lg-6">
                    <div class="checkbox">
                        <label>
                            <input name="data[Package][status]" type="checkbox" value="0"> Invisible to student?
                        </label>
                    </div>
                </div>
            </div>



            <div class="form-group">
                <div class="col-sm-12">
                    <br/>
                    <button class="btn btn-theme">Create Package</button>
                </div>
            </div>

            <?php echo $this->Form->end(); ?>
        </div>
    </div>
</div>