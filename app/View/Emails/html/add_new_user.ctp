<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Golf Swing Prescription</title>
</head>
<body style="margin:0px;font-family: Arial, Helvetica, sans-serif;font-family: Calibri;background-color: ##212121;">
<table cellpadding="0" cellspacing="0"
       style="border-collapse: collapse; width: 650px;height: auto;margin: 15px auto;background: #FFF;  border: 1px solid #DDD;border-top: 5px solid #6f8e40;border-bottom: 5px solid #6f8e40;">
    <!--HEADER-->
    <tr>
        <td style="width: 100%;height: auto;padding-top: 15px;text-align: center;background: #FFF;border-bottom: 1px dashed #DDD;">
            <img src="http://golfswingprescription.com.php54-5.ord1-1.websitetestlink.com/theme/Admin/img/logo-green-small.png" alt="Golf Swing Prescription">
            <br/>
            <br/>
        </td>
    </tr>
    <!--/HEADER-->
    <!--CONTENT-->
    <tr>
        <td valign="top" style="width: 100%;padding: 20px 50px; ">
            <h2 style="color: #6f8e40;font-weight: normal;font-size: 25px; text-align: center; font-style: italic;font-family: Georgia, 'Times New Roman', Times, serif;">
                New student has been added
            </h2>
            <table style="width: 100%;">
                <tr>
                    <td style="text-align: left; font-size: 12px; font-style: italic; color: #777;font-family: Georgia, 'Times New Roman', Times, serif;">
                        <h2 style="border-bottom: 1px solid #DDD; padding-bottom: 5px; margin-bottom: 15px; ">User Details:</h2>
                        <p><strong>Student Name:</strong> <?php echo $var['Profile']['name']?></p>
                        <p><strong>Email:</strong> <?php echo $var['User']['username']?></p>

                        <?php if($var['Profile']['street_1']):?>
                            <p><strong>Street 1:</strong> <?php echo $var['Profile']['street_1']?></p>
                        <?php endif;?>

                        <?php if($var['Profile']['street_2']):?>
                            <p><strong>Street 2:</strong> <?php echo $var['Profile']['street_2']?></p>
                        <?php endif;?>

                        <?php if($var['Profile']['phone']):?>
                            <p><strong>Phone:</strong> <?php echo $var['Profile']['phone']?></p>
                        <?php endif;?>

                        <?php if($var['Profile']['city']):?>
                            <p><strong>City:</strong> <?php echo $var['Profile']['city']?></p>
                        <?php endif;?>

                        <?php if($var['Profile']['postal_code']):?>
                            <p><strong>Postal Code:</strong> <?php echo $var['Profile']['postal_code']?></p>
                        <?php endif;?>

                        <?php if($var['Profile']['state']):?>
                            <p><strong>State:</strong> <?php echo $var['Profile']['state']?></p>
                        <?php endif;?>

                        <?php if($var['Profile']['source']):?>
                            <p><strong>Source:</strong> <?php echo $var['Profile']['source']?></p>
                        <?php endif;?>

                        <br/>
                        <br/>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <!--/CONTENT-->
    <!--FOOTER-->
    <tr>
        <td style="width: 100%;padding: 0px 50px;height: 80px;border-top: 1px dashed #DDD;text-align: center;">
            <p style="color: #777;line-height: 25px;font-size: 16px;"><span>All rights reserved.</span></p>
        </td>
    </tr>
    <!--/FOOTER-->
</table>
</body>
</html>