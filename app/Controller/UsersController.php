<?php
App::uses('AppController', 'Controller');
App::uses('DashboardsController' , 'Controller');

/**
 * Class AppointmentsController
 *
 * @property Appointment $Appointment..
 * @property Attachment @Attachment
 * @property Benchmark $Benchmark
 * @property Billing $Billing
 * @property BillingsPackage @BillingsPackage
 * @property BlockTime @BlockTime
 * @property Card $Card
 * @property Config $Config
 * @property Contact $Contact
 * @property CountLesson $CountLesson
 * @property EmailsEvent $EmailsEvent
 * @property EmailsEventsSetting $EmailsEventsSetting
 * @property Event $Event
 * @property GroupLesson $GroupLesson
 * @property GroupLessonsStudents $GroupLessonsStudents
 * @property Note @Note
 * @property Package $Packagetc_
 * @property PackagesUser $PackagesUser
 * @property PackagesOrder $PackagesOrder
 * @property Profile $Profile
 * @property Source $Source
 * @property TcsUser $TcsUser
 * @property User $User
 * @property Waiting $Waiting
 *
 * @property PaginatorComponent $Paginator
 * @property EmailComponent $Email
 * @property UtilitiesComponent $Utilities
 * @property AuthorizeNetComponent $AuthorizeNet
 * @property DataTableComponent $DataTable
 * @property ImgComponent $Img
 * @property AppsComponent $Apps
 *
 */

use Facebook\FacebookSession;
use Facebook\FacebookRedirectLoginHelper;
use Facebook\FacebookRequest;
use Facebook\FacebookResponse;
use Facebook\FacebookSDKException;
use Facebook\FacebookRequestException;
use Facebook\FacebookAuthorizationException;
use Facebook\GraphObject;
use Facebook\Entities\AccessToken;
use Facebook\HttpClients\FacebookCurlHttpClient;
use Facebook\HttpClients\FacebookHttpable;

class UsersController extends AppController
{
    /**
     * The name of this controller.
     *
     * @var string
     */
    public $name = 'Users';

    private $twiConsumerKey =  'dG7STOacX3i0Rp7YLHHpKnUcg';

    private $twiConsumerSecKey =  'k2dABvUMMY42KHH7eEyVaMXyu7Vr3m2fsgftqzW5wu9APzM05h';

    /**
     * Execute these line of codes before do anything in this controller.
     */
    /*public function beforeFilter()
    {
        $this->Auth->allowedActions = array('signup', 'forgot_password', 'reset_password');
        if($this->request['action'] != 'logout' && $this->Session->read('Auth.User')){
            $this->redirectUserByRole();
        }
    }*/

    public $components = array('Twitteroauth.Twitter', 'Auth');

    /**
     * User index page will redirect login page.
     */
    public function index()
    {
        $this->redirect(array('controller' => 'users', 'action' => 'login'));
    }

    /**
     * This is the basic signup method for this application. Here user will create their account.
     * After creating account user can login into the system and then use whole the features of this application.
     */
    public function signup()
    {
        $this->set('title_for_layout', 'Create Account - '.$this->Utilities->applicationName);
        if ($this->request->is('post')) {
            $this->request->data['User']['uuid'] = String::uuid();

            $this->User->set($this->request->data);
            $this->Profile->set($this->request->data);
            $isUserValidated = $this->User->signupValidate();
            $isProfileValidated = $this->Profile->profileValidationForSignup();

            if ($isUserValidated && $isProfileValidated) {
                if($this->Session->write('userInfo', $this->request->data))
                {
                    if ($this->request->data['User']['plan']) {
                        $this->redirect(array('controller' => 'users', 'action' => 'payment'));
                    } else {
                        $this->redirect(array('controller' => 'users', 'action' => 'plan_select'));
                    }
                }
            }
        }
    }
    
    public function plan_select() {
        $this->set('title_for_layout', 'Select Plan - '.$this->Utilities->applicationName);
        if ($this->request->is('post')) {
            $data = $this->request->data;
            $user = $this->Session->read('userInfo');
            $user['User']['plan'] = $data['User']['plan'];
            
            $this->Session->write('userInfo', $user);
            $this->redirect(array('controller' => 'users', 'action' => 'payment'));
        }
    }

    public function payment()
    {
        $this->set('title_for_layout', 'Payment - '.$this->Utilities->applicationName);
        $user = $this->Session->read('userInfo');

        if(empty($user)){
            $this->Session->setFlash('Sorry, please fill up these info before payment', 'flash_error');
            $this->redirect(array('controller' => 'users', 'action' => 'signup'));
        }

        if ($this->request->is('post')) {
            $data = $this->request->data;
            $stripeToken = $data['stripe']['token'];
            $email = $user['User']['username'];
            $name = $user['Profile']['first_name']. ' '. $user['Profile']['last_name'];
            $plan = $user['User']['plan'];
            
            if (strpos($plan, 'single') === 0) {
                // instructor
                $user['User']['role'] = 3;
            } else {
                // training center
                $user['User']['role'] = 4;
            }
            
            $stripeCustomer = array(
                'source' => $stripeToken,
                'plan' => $plan,
                'email' => $email,
                'description' => $name
            );
            try {
                $result = \Stripe\Customer::create($stripeCustomer);
            } catch (Exception $e) {
                $result = null;
            }
            if ($result && $result->id) {
                $user['User']['stripe_customer'] = $result->id;
                
                $this->User->saveAll($user);
                $userID = $this->User->getLastInsertID();
                
                $this->Session->setFlash('Your account has been created!', 'flash_success');

                if($email){
                    $this->Email->sendEmail(
                        'mandrill',
                        'golf-swing-prescription',
                        $email,
                        'Signup Confirmation - Golf Swing Prescription',
                        null
                    );
                }

                if($this->Session->read('is_social_login')){

                    $insertedUser = $this->User->find('first', array(
                            'recursive' => 0,
                            'conditions' => array(
                                'User.id' => $userID,
                            )
                        )
                    );

                    if($insertedUser && sizeof($insertedUser['User']) > 0 && sizeof($insertedUser['Profile']) > 0){
                        $profile = $insertedUser['Profile'];
                        unset($insertedUser['Profile']);
                        $insertedUser['User']['Profile'] = $profile;
                        $this->Session->delete('is_social_login');
                        $this->customLogin($insertedUser['User']);
                    }
                    else{
                        $this->Session->setFlash("Sorry, something went wrong", 'flash_error');
                    }
                }
            } else {
                $this->Session->setFlash("Sorry, something went wrong", 'flash_error');
            }

            $this->Session->delete('userInfo');
            $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
    }

    /**
     * This is the login of this application. This is only way to enter into the system and then use.
     */
    public function login()
    {
        $this->set('title_for_layout', 'Login - '.$this->Utilities->applicationName);
        $getConnect = new TwitterOAuth($this->twiConsumerKey, $this->twiConsumerSecKey);
        $tmpCredential = $getConnect->getRequestToken(Router::url('/users/login/', true));
        if(!isset($_REQUEST['oauth_verifier'])){
            $this->Session->write('tmpToken', $tmpCredential['oauth_token']);
            $this->Session->write('tmpTokenSec', $tmpCredential['oauth_token_secret']);
        }
        $twitterLoginURL = $getConnect->getAuthorizeURL($tmpCredential);
        $this->set('twitterLoginURL', $twitterLoginURL);
        $this->facebookLogin();

        if ($this->request->is('post')) {
            $this->User->set($this->request->data);
            if ($this->User->loginValidate()) {
                if ($this->Auth->login()) {
                    $this->updateLoginTime();
                    $this->redirectUserByRole();
                }
            }
            $this->Session->setFlash('Sorry, invalid username of password!', 'flash_error');
        }
        elseif(isset($_REQUEST['oauth_verifier'])){
            $getConnectAgain = new TwitterOAuth($this->twiConsumerKey, $this->twiConsumerSecKey, $this->Session->read('tmpToken'), $this->Session->read('tmpTokenSec'));
            $twitterAuth = $getConnectAgain->getAccessToken($_REQUEST['oauth_verifier']);

            $twitterProfile = $this->getTwitterProfile($twitterAuth['oauth_token'], $twitterAuth['oauth_token_secret'], $twitterAuth['screen_name'], 1);
            $this->twitterLogin($twitterAuth, $twitterProfile);
        }
    }



    /**
     * If the will forgot password then then can reset their password.
     * Here they will get a link to reset password,
     */
    public function forgot_password()
    {
        $this->set('title_for_layout', 'Forgot Password - '.$this->Utilities->applicationName);
        if ($this->request->is('post')) {
            $this->User->set($this->request->data);
            if ($this->User->forgotPassValidate()) {
                $userInfo = $this->User->findByUsername($this->request->data['User']['username']);
                if ($userInfo) {
                    $randNum = $this->genRandomNum($userInfo['User']['username']);
                    $this->User->id = $userInfo['User']['id'];
                    if ($this->User->saveField('reset_val_for_password', $randNum)) {
                        $resetURL = Router::url('/', true) . 'users/reset_password/' . $randNum;
                        if ($this->Email->sendEmail(
                            'mandrill',
                            'forgot_password',
                            $userInfo['User']['username'],
                            'Reset Password - Golf Swing Prescription',
                            $resetURL
                        )
                        ) {
                            $this->Session->setFlash('A link has been sent to your email to change your password', 'flash_success');
                        } else {
                            $this->Session->setFlash('Sorry, something went wrong', 'flash_error');
                        }
                    }
                } else {
                    $this->Session->setFlash('Sorry, this email is not been registered!', 'flash_error');
                }
            }
        }
    }

    /**
     * User can reset their password after clicking the link the got from the forgot password.
     * After changing password they will get a confirmation email of changing password.
     * After changing the password the reset password will be deleted so the reset password link will be invalided.
     *
     * @param null $value
     * @throws BadRequestException
     * @throws ForbiddenException
     */
    public function reset_password($value = null)
    {
        if ($value) {
            $this->set('title_for_layout', 'Reset Password - '.$this->Utilities->applicationName);
            $this->set('resetValue', $value);
            $userInfo = $this->User->findByreset_val_for_password($value);
            if ($userInfo) {
                if ($this->request->is('post')) {
                    $this->User->set($this->request->data);
                    if ($this->User->resetPassValidate()) {
                        $this->User->id = $userInfo['User']['id'];
                        if ($this->User->save($this->request->data)) {
                            $this->Session->setFlash('Your password has been changed successfully!', 'flash_success');
                            $this->sendEmailNotification('when_reset_password', array('userID' => $userInfo['User']['id']));
                            $this->Email->sendEmail(
                                'mandrill',
                                'change_password',
                                $userInfo['User']['username'],
                                'Password Changed - Golf Swing Prescription',
                                null
                            );
                            $this->User->id = $userInfo['User']['id'];
                            $this->User->saveField('pass_reset_value', null);
                            $this->redirect(array('controller' => 'users', 'action' => 'login'));
                        }
                        else {
                            $this->Session->setFlash('Sorry, something went wrong!', 'flash_error');
                        }
                    }
                }
            } else {
                throw new ForbiddenException('Bad Request');
            }
        } else {
            throw new BadRequestException('Bad Request');
        }
    }

    /**
     * This is the confirmation verification method
     * from email who confirm his waiting appointment
     *
     */
    public function waiting_confirm($responseCode)
    {
        if ($responseCode) {
            $waitingQueue = $this->WaitingQueue->find('first', array(
                'conditions' => array(
                    'response_code' => $responseCode,
                    'validity_period >=' => date('Y-m-d H:i:s'),
                    'status' => 1
                ),
            ));

            if (sizeof($waitingQueue) > 0) {
                $this->WaitingQueue->id = $waitingQueue['WaitingQueue']['id'];
                $this->WaitingQueue->saveField('status', 4);
                $this->Session->setFlash('Appointment has been confirmed', 'flash_success');

                $anotherUsers = $this->WaitingQueue->find('all', array(
                    'conditions' => array(
                        'appointment_id' => $waitingQueue['appointment_id'],
                        'status' => 1
                    ),
                ));

                if (sizeof($anotherUsers) > 0) {
                    foreach ($anotherUsers as $anotherUser) {
                        $this->WaitingQueue->id = $anotherUser['WaitingQueue']['id'];
                        $this->WaitingQueue->saveField('status', 3);
                    }
                }

            } else {
                $this->Session->setFlash('Sorry, something went wrong. Appointment not confirmed', 'flash_error');
            }
            $this->redirect(array('controller' => 'dashboards', 'action' => 'student_index'));

        } else {
            throw new ForbiddenException('Bad Request');
        }
    }

    /**
     * Here user can end their session, in other words we can say
     * they can leave (logout) form the application.
     */
    public function logout()
    {
        $this->Session->write('isAppointmentExpiredChecked', null);
        $this->Session->write('isPackageExpiredChecked', null);
        $this->Session->write('isSettedEmailNotification', null);
        $this->Session->write('isSlippingAwayChecked', null);
        $this->Session->write('packageIds', null);
        if ($this->Auth->logout()) {
            $this->Session->setFlash('Good Bye!', 'flash_success');
            $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }
    }

    /**
     *Generating a unique random number.
     *
     * @param string $arg
     * @return string
     */
    public function genRandomNum($arg)
    {
        $randNum1 = rand(1, 9999999);
        $randNum2 = rand(999999, 9999999999999);
        return md5(md5($arg) . rand($randNum1, $randNum1) . $randNum1 . $randNum2);
    }


    /**************************************************************************************************************/
    /********************************************        Admin Panel          *************************************/
    /**************************************************************************************************************/

    /**
     * Admin login page will redirect into login page
     */
    public function admin_login()
    {
        $this->redirect(array('controller' => 'users', 'action' => 'login', 'admin' => false));
    }

    /**************************************************************************************************************/
    /******************************************         Training Center Panel         *************************************/
    /**************************************************************************************************************/

    public function tc_add()
    {
        $this->set('title_for_layout', 'Add New Student - '.$this->Utilities->applicationName);
        $this->set('sources', $this->Source->getSourceList());
        if ($this->request->is('post')) {

            $data = $this->request->data;
            if($data['Profile']['is_custom_source'] == 1 && $data['Profile']['custom_source']){
                $data['Profile']['source'] = $data['Profile']['custom_source'];
                $data['Profile']['source_id'] = null;
                $isCustomSource = true;
            }
            else{
                $isCustomSource = false;
                $sourceName = $this->Source->getSourceByID($data['Profile']['source']);
                $data['Profile']['source_id'] = $data['Profile']['source'];
                $data['Profile']['source'] = $sourceName;
            }

            $data['User']['uuid'] = String::uuid() ;
            $data['User']['role'] = 3;
            if ($this->User->saveAll($data)) {
                $data['TcsUser']['tc_id'] = $this->userID;
                $data['TcsUser']['user_id'] = $this->User->getLastInsertID();
                $data['TcsUser']['status'] = 1;
                $this->TcsUser->save($data['TcsUser']);
                $isCustomSource = false;
                $this->Session->write('isCustomSource', $isCustomSource);
                $this->Session->setFlash('New instructor has been successfully created', 'flash_success');
                $this->redirect(array('controller' => 'users', 'action' => 'list'));
            }
            else {
                $this->Session->write('isCustomSource', $isCustomSource);
                $this->Session->setFlash('Sorry, something went wrong', 'flash_error');
            }
        }
    }

    public function tc_instructor($status = null)
    {
        $this->set('title_for_layout', 'List of Instructor - '.$this->Utilities->applicationName);
        $conditions = array('TcsUser.tc_id' => $this->userID);
        $subConditions = $this->Utilities->setUserConditions($status);
        $conditions = array_merge($conditions, $subConditions);
        if($status == 'slipping-away'){

        }

        $this->Paginator->settings = array(
            'fields' => array(
                'TcsUser.*',
                'User.*',
                'Profile.*'
            ),
            'conditions' => $conditions,
            'limit' => $this->limit,
            'recursive' => -1,
            'order' => 'User.id DESC',
            'joins' => array(
                array(
                    'table' => 'profiles',
                    'type' => 'LEFT',
                    'alias' => 'Profile',
                    'conditions' => array('Profile.user_id = TcsUser.user_id')
                ),
                array(
                    'table' => 'users',
                    'type' => 'LEFT',
                    'alias' => 'User',
                    'conditions' => array('User.id = TcsUser.user_id')
                )
            )
        );

        $userList = $this->Paginator->paginate('TcsUser');
        $this->set('userList', $userList);
    }

    /**
     * This is the student view of tc panel
     * @param $userUUID
     */
    protected  function student_detail_tc_panel($userUUID){
        $this->set('element', 'user_details/student_details');
        $this->instructor_details($userUUID);
    }

    /**
     * This is the instructor view of tc panel
     * @param $userUUID
     */
    protected  function instructor_detail_tc_panel($userUUID){

         $userID = $this->User->getUserIdByUuid($userUUID);



        $this->set('element', 'user_details/instructor_details');

        $this->set('title_for_layout', 'Instructor Details - '.$this->Utilities->applicationName);
        $contactOption = array('conditions' => array('Contact.user_id' => $userID));

        $this->set('contactsOverview', $this->Contact->getContactOverview($contactOption));
        $this->set('countStudent', $this->Profile->getStudentForInstructor('count', $userID));
        $this->set(
            'countPOS',
            $this->PackagesUser->getPOS(
                array('query' => 'count', 'conditions' => array('PackagesUser.instructor_id' => $userID))
            )
        );
        $this->set(
            'countBenchmark',
            $this->Benchmark->getBenchmarks(
                array('query' => 'count', 'conditions' => array('Benchmark.created_by' => $userID))
            )
        );



        $this->set(
            'upComingAppointments',
            $this->Appointment->getAppointments(
                array(
                    'conditions' => array('Appointment.instructor_id' => $userID, 'Appointment.status' => 2),
                    'limit' => 5
                )
            )
        );

        $userInfo = $this->User->getUser($userID);
        $this->set('userInformation', $userInfo);

        $recentBillingCond = array('Profile.instructor_id' => $userID);
        $recentBilling = $this->Billing->geBilling(
            array(
                'conditions' => $recentBillingCond,
                'limit' => 5
            )

        );
        $this->set('recentBilling', $recentBilling);

        $conditions = array('Profile.instructor_id' => $userID, 'User.status !=' => 9);

       $students =  $this->User->find('all' , array(
            'conditions' => $conditions,
            'limit' => 5,
            'recursive' => 1,
            'order' => 'User.id DESC',
        ));
        $this->set('userList' , $students);

    }


    public function tc_details($userUUID)
    {
        $userRole = $this->User->getUserRoleByUuid($userUUID);
        $element = null;
         if($userRole == 2) {
             return $this->student_detail_tc_panel($userUUID);
         }elseif($userRole == 3){
             return $this->instructor_detail_tc_panel($userUUID);
         }else{
             $this->redirect($this->referer());
             return false;
         }

        $this->set('element' , $element);
    }

    public function tc_edit($userUUID)
    {
        $this->set('title_for_layout', 'Edit Instructor - '.$this->Utilities->applicationName);
        $this->set('sources', $this->Source->getSourceList());
        $userInfo = $this->fetchUserByUUID($userUUID);

        if ($this->request->is('post')) {

            if($this->request->data['Profile']['is_custom_source'] == 1 && $this->request->data['Profile']['custom_source']){
                $this->request->data['Profile']['source'] = $this->request->data['Profile']['custom_source'];
                $this->request->data['Profile']['source_id'] = null;
                $isCustomSource = true;
            }
            else{
                $isCustomSource = false;
                $sourceName = $this->Source->getSourceByID($this->request->data['Profile']['source']);
                $this->request->data['Profile']['source_id'] = $this->request->data['Profile']['source'];
                $this->request->data['Profile']['source'] = $sourceName;
            }

            $this->User->id = $userInfo['User']['id'];
            if ($this->User->save($this->request->data)) {
                $this->Profile->id = $userInfo['Profile']['id'];
                $this->Profile->save($this->request->data);
                $isCustomSource = false;
                $this->Session->write('isCustomSource', $isCustomSource);
                $this->Session->setFlash('Instructor information has been updated', 'flash_success');
                $this->redirect(array('controller' => 'users', 'action' => 'details', $userInfo['User']['uuid']));
            } else {
                $this->Session->write('isCustomSource', $isCustomSource);
                $this->Session->setFlash('Sorry, something went wrong', 'flash_error');
            }
        }
    }

    public function tc_add_student()
    {
        $this->set('title_for_layout', 'Add New Student - '.$this->Utilities->applicationName);
        $this->set('sources', $this->Source->getSourceList());
        $this->set('instructors', $this->TcsUser->getInstructorIdsByTCID($this->userID));
        if ($this->request->is('post')) {

            $data = $this->request->data;
            if($data['Profile']['is_custom_source'] == 1 && $data['Profile']['custom_source']){
                $data['Profile']['source'] = $data['Profile']['custom_source'];
                $data['Profile']['source_id'] = null;
                $isCustomSource = true;
            }
            else{
                $isCustomSource = false;
                $sourceName = $this->Source->getSourceByID($data['Profile']['source']);
                $data['Profile']['source_id'] = $data['Profile']['source'];
                $data['Profile']['source'] = $sourceName;
            }

            $data['User']['uuid'] = String::uuid() ;
            $data['User']['role'] = 2;
            if ($this->User->saveAll($data)) {
                $isCustomSource = false;
                $this->Session->write('isCustomSource', $isCustomSource);
                $this->Session->setFlash('New student has been successfully created', 'flash_success');
                $this->redirect(array('controller' => 'users', 'action' => 'student_list'));
            }
            else {
                $this->Session->write('isCustomSource', $isCustomSource);
                $this->Session->setFlash('Sorry, something went wrong', 'flash_error');
            }
        }
    }

    public function tc_edit_student($userUUID)
    {
        $this->set('title_for_layout', 'Edit Instructor - '.$this->Utilities->applicationName);
        $this->set('sources', $this->Source->getSourceList());
        $this->set('instructors', $this->TcsUser->getInstructorIdsByTCID($this->userID));
        $userInfo = $this->fetchUserByUUID($userUUID);

        if ($this->request->is('post')) {

            if($this->request->data['Profile']['is_custom_source'] == 1 && $this->request->data['Profile']['custom_source']){
                $this->request->data['Profile']['source'] = $this->request->data['Profile']['custom_source'];
                $this->request->data['Profile']['source_id'] = null;
                $isCustomSource = true;
            }
            else{
                $isCustomSource = false;
                $sourceName = $this->Source->getSourceByID($this->request->data['Profile']['source']);
                $this->request->data['Profile']['source_id'] = $this->request->data['Profile']['source'];
                $this->request->data['Profile']['source'] = $sourceName;
            }

            $this->User->id = $userInfo['User']['id'];
            if ($this->User->save($this->request->data)) {
                $this->Profile->id = $userInfo['Profile']['id'];
                $this->Profile->save($this->request->data);
                $isCustomSource = false;
                $this->Session->write('isCustomSource', $isCustomSource);
                $this->Session->setFlash('Instructor information has been updated', 'flash_success');
                $this->redirect(array('controller' => 'users', 'action' => 'details', $userInfo['User']['uuid']));
            } else {
                $this->Session->write('isCustomSource', $isCustomSource);
                $this->Session->setFlash('Sorry, something went wrong', 'flash_error');
            }
        }
    }


    /**************************************************************************************************************/
    /******************************************         Student Panel         *************************************/
    /**************************************************************************************************************/

    /**
     * Student login page will redirect into login page
     */
    public function student_login()
    {
        $this->redirect(array('controller' => 'users', 'action' => 'login', 'student' => false));
    }

    public function tc_login() {
        $this->redirect(array('controller' => 'users', 'action' => 'login', 'student' => false));
    }


    public function student_details($userUUID = null)
    {
        $this->set('title_for_layout', 'Instructor Details - '.$this->Utilities->applicationName);
        $userInfo = $this->fetchUserByUUID($userUUID);
        $this->set(
            'benchmarks',
            $this->Benchmark->getBenchmark('all', array('created_for' => $userInfo['User']['id']))
        );
    }
    /**************************************************************************************************************/
    /******************************************       Instructor Panel         ************************************/
    /**************************************************************************************************************/

    /**
     * Instructor login page will redirect into login page
     */
    public function instructor_login()
    {
        $this->redirect(array('controller' => 'users', 'action' => 'login', 'instructor' => false));
    }

    /**
     * Here instructor can add student.
     */
    public function instructor_add()
    {
        $this->set('title_for_layout', 'Add New Student - '.$this->Utilities->applicationName);
        $this->set('sources', $this->Source->getSourceList());
        if ($this->request->is('post')) {
            $this->addUser($this->request->data, 2);
        }
    }

    public function instructor_index() {
        $this->redirect(array('controller' => 'users', 'action' => 'student'));
    }

    /**
     * @param null $status
     *
     * Here instructor can see the student list
     */

    public function instructor_student($status = null)
    {
        $this->set('title_for_layout', 'List of Student - '.$this->Utilities->applicationName);
        $conditions = array('Profile.instructor_id' => $this->userID, 'User.status !=' => 9);
        $subConditions = $this->Utilities->setUserConditions($status);
        $conditions = array_merge($conditions, $subConditions);

        if($status == 'slipping_away'){
            $conditions = array_merge($conditions, array('User.is_slipping_away' => 1));
            $this->set('title', 'SLIPPING AWAY - Who did\'nt take any lesson in last 14 day\'s');
        }

        if($status == 'never_login'){
            $conditions = array_merge($conditions, array('User.last_login' => NULL));
            $this->set('title', 'Lever Login - Who never login into application');
        }
        if($status == 'never_lesson_purchased'){
            $conditions = array_merge($conditions, array('CountLesson.lesson_left' => NULL));
            $this->set('title', 'Never Lesson Purchased - Who never purchased any lesson');
        }
        if($status == 'no_lesson_left'){
            $noLessonLeftConditions['OR'] = array(
                array('CountLesson.lesson_left' => array('', 0)),
                array('CountLesson.lesson_left' => NULL)
            );
            $conditions = array_merge($conditions, $noLessonLeftConditions);
            $this->set('title', 'No Lesson Left - Who have no lesson left');
        }
        if($status == 'lesson_left'){
            $conditions = array_merge($conditions, array('CountLesson.lesson_left >' => 0));
            $this->set('title', 'Lesson Left - Who have more than one lesson left');
        }
        if($status == 'never_schedule'){
            $conditions = array_merge($conditions, array('CountLesson.purchased_lesson_count = CountLesson.lesson_left'));
            $this->set('title', 'Never Scheduled - Who never set any schedule');
        }

        if($status == 'rating_one')
        {
            $conditions = array_merge($conditions, array('User.rating' => 1));
            $this->set('title', 'One star rating student list');
        }

        if($status == 'rating_two')
        {
            $conditions = array_merge($conditions, array('User.rating' => 2));
            $this->set('title', 'Two star rating student list');
        }

        if($status == 'rating_three')
        {
            $conditions = array_merge($conditions, array('User.rating' => 3));
            $this->set('title', 'Three star rating student list');
        }

        if($status == 'rating_four')
        {
            $conditions = array_merge($conditions, array('User.rating' => 4));
            $this->set('title', 'Four star rating student list');
        }

        if($status == 'rating_five')
        {
            $conditions = array_merge($conditions, array('User.rating' => 5));
            $this->set('title', 'Five star rating student list');
        }

        if($status == 'no_rating')
        {
            $noRatingConditions['OR'] = array(
                array('User.rating' => array('', 0)),
                array('User.rating' => NULL)
            );
            $conditions = array_merge($conditions, $noRatingConditions);
            $this->set('title', 'No rating lead list');
        }

        $this->User->bindModel(
            array(
                'hasMany'=>
                    array(
                        'LastFollowupDate' => array(
                            'className' => 'Note',
                            'foreignKey' => 'user_id',
                            'order' => 'LastFollowupDate.note_date DESC',
                        )
                    )
            ),
            false
        );

        $this->Paginator->settings = array(
            'conditions' => $conditions,
            'fields' => array('User.id', 'User.username', 'User.uuid', 'User.is_slipping_away', 'User.status', 'User.rating', 'User.last_login',  'Profile.first_name', 'Profile.last_name', 'Profile.phone', '', 'CountLesson.lesson_left', 'CountLesson.appointed_lesson_count'),
            'limit' => $this->limit,
            'recursive' => -1,
            'contain' => array(
                'Note' => array(
                    'fields' => array('Note.id', 'Note.note', 'Note.created'),
                    'limit' => 1,
                    'order' => 'Note.created DESC',
                ),
                'LastFollowupDate' => array(
                    'fields' => array('LastFollowupDate.id', 'LastFollowupDate.note_date'),
                    'limit' => 1,
                    'order' => 'LastFollowupDate.note_date DESC',
                    'conditions' => array('LastFollowupDate.note_date <' => date('Y-m-d H:i:s'))
                )
            ),
            'order' => 'User.id DESC',
            'joins' => array(
                array(
                    'table' => 'count_lessons',
                    'type' => 'LEFT',
                    'alias' => 'CountLesson',
                    'conditions' => array('CountLesson.user_id = User.id')
                ),
                array(
                    'table' => 'profiles',
                    'type' => 'LEFT',
                    'alias' => 'Profile',
                    'conditions' => array('Profile.user_id = User.id')
                ),
            )
        );

        $userOverview = $this->User->getUserOverview(array('conditions' => array('Profile.instructor_id' => $this->userID)));
        $users = $this->Paginator->paginate('User');
        $this->set('userOverview', $userOverview);
        $this->set('userList', $users);
    }
    public function tc_student($status = null)
    {
        $this->set('title_for_layout', 'List of Student - '.$this->Utilities->applicationName);
        $instructorIds = $this->TcsUser->getIntructorIdsForTC($this->userID);
        $conditions = array(
            'User.role' => 2,
            array('Profile.instructor_id' => $instructorIds),
            'User.status !=' => 9
        );
        $subConditions = $this->Utilities->setUserConditions($status);
        $conditions = array_merge($conditions, $subConditions);

        if($status == 'slipping_away'){
            $conditions = array_merge($conditions, array('User.is_slipping_away' => 1));
            $this->set('title', 'SLIPPING AWAY - Who did\'nt take any lesson in last 14 day\'s');
        }

        if($status == 'never_login'){
            $conditions = array_merge($conditions, array('User.last_login' => NULL));
            $this->set('title', 'Lever Login - Who never login into application');
        }
        if($status == 'never_lesson_purchased'){
            $conditions = array_merge($conditions, array('CountLesson.lesson_left' => NULL));
            $this->set('title', 'Never Lesson Purchased - Who never purchased any lesson');
        }
        if($status == 'no_lesson_left'){
            $noLessonLeftConditions['OR'] = array(
                array('CountLesson.lesson_left' => array('', 0)),
                array('CountLesson.lesson_left' => NULL)
            );
            $conditions = array_merge($conditions, $noLessonLeftConditions);
            $this->set('title', 'No Lesson Left - Who have no lesson left');
        }
        if($status == 'lesson_left'){
            $conditions = array_merge($conditions, array('CountLesson.lesson_left >' => 0));
            $this->set('title', 'Lesson Left - Who have more than one lesson left');
        }
        if($status == 'never_schedule'){
            $conditions = array_merge($conditions, array('CountLesson.purchased_lesson_count = CountLesson.lesson_left'));
            $this->set('title', 'Never Scheduled - Who never set any schedule');
        }

        if($status == 'rating_one')
        {
            $conditions = array_merge($conditions, array('User.rating' => 1));
            $this->set('title', 'One star rating student list');
        }

        if($status == 'rating_two')
        {
            $conditions = array_merge($conditions, array('User.rating' => 2));
            $this->set('title', 'Two star rating student list');
        }

        if($status == 'rating_three')
        {
            $conditions = array_merge($conditions, array('User.rating' => 3));
            $this->set('title', 'Three star rating student list');
        }

        if($status == 'rating_four')
        {
            $conditions = array_merge($conditions, array('User.rating' => 4));
            $this->set('title', 'Four star rating student list');
        }

        if($status == 'rating_five')
        {
            $conditions = array_merge($conditions, array('User.rating' => 5));
            $this->set('title', 'Five star rating student list');
        }

        if($status == 'no_rating')
        {
            $noRatingConditions['OR'] = array(
                array('User.rating' => array('', 0)),
                array('User.rating' => NULL)
            );
            $conditions = array_merge($conditions, $noRatingConditions);
            $this->set('title', 'No rating lead list');
        }

        if (isset($this->request->query['instructor']) && $this->request->query['instructor']) {
            $instructorID = $this->request->query['instructor'];
            if($instructorID[0] == ''){
                unset($instructorID[0]);
            }
            if (!empty($instructorID)) {
                $conditions = array_merge($conditions, array('Profile.instructor_id' => $instructorID));
            }
        }

        if (isset($this->request->query['student']) && $this->request->query['student'] && $this->request->query['student'] != '') {
            $student = explode('-', $this->request->query['student']);
            $studentID = $this->User->getUserByEMail(trim($student[1]));
            if ($studentID) {
                $conditions = array_merge($conditions, array('Profile.user_id' => $studentID));
            }
        }

        if (isset($this->request->query['email']) && $this->request->query['email'] && $this->request->query['email'] != '') {
            $email = $this->request->query['email'];
            if (isset($email) && $email) {
                $conditions = array_merge($conditions, array('User.username  LIKE' => '%'.$email.'%'));
            }
        }

        if (isset($this->request->query['phone']) && $this->request->query['phone'] && $this->request->query['phone'] != '') {
            $phone = $this->request->query['phone'];
            if (isset($phone) && $phone) {
                $conditions = array_merge($conditions, array('Profile.phone  LIKE' => '%'.$phone.'%'));
            }
        }

        if (isset($this->request->query['rating']) && $this->request->query['rating'] && $this->request->query['rating'] != '') {
            $rating = $this->request->query['rating'];
            if (isset($rating) && $rating) {
                $conditions = array_merge($conditions, array('User.rating' => $rating));
            }
        }

        if (isset($this->request->query['status']) && $this->request->query['status'] && $this->request->query['status'] != '') {
            $status = $this->request->query['status'];
            if (isset($status) && $status) {
                $conditions = array_merge($conditions, array('User.status' => $status));
            }
        }

        if (isset($this->request->query['slipping_away']) && $this->request->query['slipping_away'] && $this->request->query['slipping_away'] != '') {
            $slippingAway = $this->request->query['slipping_away'];
            if (isset($slippingAway) && $slippingAway) {
                $conditions = array_merge($conditions, array('User.is_slipping_away' => $slippingAway));
            }
        }

        $this->User->bindModel(
            array(
                'hasMany'=>
                    array(
                        'LastFollowupDate' => array(
                            'className' => 'Note',
                            'foreignKey' => 'user_id',
                            'order' => 'LastFollowupDate.note_date DESC',
                        )
                    )
            ),
            false
        );

        $this->Paginator->settings = array(
            'conditions' => $conditions,
            'fields' => array('User.id', 'User.username', 'User.uuid', 'User.is_slipping_away', 'User.status', 'User.rating', 'User.last_login',  'Profile.first_name', 'Profile.last_name', 'Profile.phone', '', 'CountLesson.lesson_left', 'CountLesson.appointed_lesson_count' ,
                'CONCAT(Instructor.first_name, " ", Instructor.last_name) as instructorName',

                ),
            'limit' => $this->limit,
            'recursive' => -1,
            'contain' => array(
                'Note' => array(
                    'fields' => array('Note.id', 'Note.note', 'Note.created'),
                    'limit' => 1,
                    'order' => 'Note.created DESC',
                ),
                'LastFollowupDate' => array(
                    'fields' => array('LastFollowupDate.id', 'LastFollowupDate.note_date'),
                    'limit' => 1,
                    'order' => 'LastFollowupDate.note_date DESC',
                    'conditions' => array('LastFollowupDate.note_date <' => date('Y-m-d H:i:s'))
                )
            ),
            'order' => 'User.id DESC',
            'joins' => array(
                array(
                    'table' => 'count_lessons',
                    'type' => 'LEFT',
                    'alias' => 'CountLesson',
                    'conditions' => array('CountLesson.user_id = User.id')
                ),
                array(
                    'table' => 'profiles',
                    'type' => 'LEFT',
                    'alias' => 'Profile',
                    'conditions' => array('Profile.user_id = User.id')
                ),
                array(
                    'table' => 'profiles',
                    'type' => 'LEFT',
                    'alias' => 'Instructor',
                    'conditions' => array('Instructor.user_id = Profile.instructor_id')
                )

            )
        );

        $userOverview = $this->User->getUserOverview(array('conditions' => array('Profile.instructor_id' => $instructorIds, 'User.role' => 2)));
        $users = $this->Paginator->paginate('User');
        $this->set('userOverview', $userOverview);
        $this->set('userList', $users);
        $getTcInstructors = $this->TcsUser->getInstructorIdsByTCID($this->userID);
        $this->set('instructors', $getTcInstructors);

    }

    /**
     * Search student from there.
     */
    public function instructor_search()
    {
        $this->set('title_for_layout', 'Search User - '.$this->Utilities->applicationName);
        $searchTerm = $this->request->query['user'];
        $this->searchUser($searchTerm);
    }

    /**
     * @param $userUUID
     *
     * Here instructor will see details of student.
     */
    public function instructor_details($userUUID)
    {
        $this->set('title_for_layout', 'Student Details - '.$this->Utilities->applicationName);

        /**
         * Only For TC users
         */
        $instructorIds = $this->TcsUser->getIntructorIdsForTC($this->userID);
        $instructorDetails = $this->User->find('all' , array(
            'fields' => array(
                'CONCAT(Instructor.first_name, " ", Instructor.last_name) as instructorName',
                'User.id'
            ),
            'conditions' => array('User.id' => $instructorIds),
            'joins' =>array(
                array(
                    'table' => 'profiles',
                    'type' => 'LEFT',
                    'alias' => 'Instructor',
                    'conditions' => array('Instructor.user_id = User.id')
                )

            )
        ));

        $this->set('instructors' , $instructorDetails);


        $userID = $this->User->getUserIdByUuid($userUUID);


        if(!$userID)
        {
            $this->Session->setFlash('Sorry, something went wrong', 'flash_error');
            $this->redirect(array('controller' => 'users', 'action' => 'list'));
        }
        $userInfo = $this->User->getStudentDetails($userID);
        $totalLesson = $this->PackagesUser->countUsersPurchasedLesson($userID);
        $totalPackage = $this->PackagesUser->countPackagesByUserID($userID);
        $totalWaiting = $this->Waiting->countWaitingByUserID($userID);
        $upcomingAppointment = $this->Appointment->countUpcomingAppointmentByUserID($userID);
        $totalBenchmark = $this->Benchmark->countBenchmarkByUserID($userID);
        $totalSpendAmount = $this->Billing->getAmountByUserID($userID);

        $overview = array(
            'totalLesson' => $totalLesson,
            'totalPackage' => $totalPackage,
            'totalWaiting' => $totalWaiting,
            'upcomingAppointment' => $upcomingAppointment,
            'totalBenchmark' => $totalBenchmark,
            'totalSpendAmount' => $totalSpendAmount,
        );





        $this->set('notes', $this->Note->getUserNotes($userInfo['User']['id']));
        $this->set('userInformation', $userInfo);
        $this->set('overview', $overview);

        if ($this->request->is('post')) {
            $this->request->data['Note']['user_id'] = $userInfo['User']['id'];
            $this->request->data['Note']['created_by'] = $this->userID;
            $this->request->data['Note']['note_date'] = date('Y-m-d H:i:s', strtotime($this->request->data['Note']['note_date']));

            if ($this->Note->save($this->request->data)) {
                $this->Session->setFlash('Note has been saved', 'flash_success');
                $this->redirect($this->referer());
            } else {
                $this->Session->setFlash('Sorry, something went wrong', 'flash_error');
            }

        }
    }

    public function instructor_changeStatus($userUUID, $status)
    {
        $userID = $this->User->getUserIdByUuid($userUUID);
        if($this->changeStatus($userID, $status)){
            if($status == 9){
                $this->Session->setFlash('User has been removed from application', 'flash_success');
            }
            elseif($status == 1){
                $this->Session->setFlash('User is now active user', 'flash_success');
            }
        }
        $this->redirect($this->referer());
        $this->redirect($this->referer());
    }

    public function tc_unassigned($userUUID)
    {
        $userID = $this->User->getUserIdByUuid($userUUID);
        if ($this->unassignedStudent($userID)) {
            $this->Session->setFlash('Successfully unassigned', 'flash_error');
        } else {
            $this->Session->setFlash('Sorry, cannot unassigned', 'flash_success');
        }

        $this->redirect($this->referer());
    }

    public function unassignedStudent($userID)
    {
        $profile = $this->Profile->find('first' , array(
            'conditions' =>array('user_id' => $userID),
            'fields' => array('id')
        ));
        $this->Profile->id= $profile['Profile']['id'];

        if($this->Profile->saveField('instructor_id', null)){
            return true;
        }
        return false;
    }
    public function tc_change_assign($userUUID){
        $userID = $this->User->getUserIdByUuid($userUUID);
        $instructorId = $this->request->data('instructor_change');
        $instructor = $this->Profile->getProfile('first', array('user_id' => $instructorId));

        $profile = $this->Profile->getProfile('first', array('user_id' => $userID));
        $this->Profile->id= $profile['Profile']['id'];

        if($this->Profile->saveField('instructor_id', $instructorId)){
            $this->Session->setFlash($instructor['Profile']['name']. ' has been assigned to '. $profile['Profile']['name'], 'flash_success');
        }
        else{
            $this->Session->setFlash('Sorry, something went wrong', 'flash_success');

        }
        $this->redirect($this->referer());

    }

    /**
     * @param $userUUID
     *
     * Here instructor will update/edit the students profile.
     */
    public function instructor_edit($userUUID)
    {
        $this->set('title_for_layout', 'Edit Student - '.$this->Utilities->applicationName);
        $this->set('sources', $this->Source->getSourceList());
        $userInfo = $this->fetchUserByUUID($userUUID);

        if ($this->request->is('post')) {
            $this->Profile->set($this->request->data);
            $this->User->set($this->request->data);

            $isUserValidated = $this->User->editProfileValidation();
            $isProfileValidated = $this->Profile->editProfileValidation();

            if($isUserValidated && $isProfileValidated){
                if($this->request->data['Profile']['is_custom_source'] == 1 && $this->request->data['Profile']['custom_source']){
                    $this->request->data['Profile']['source'] = $this->request->data['Profile']['custom_source'];
                    $this->request->data['Profile']['source_id'] = null;
                    $isCustomSource = true;
                }
                else{
                    $isCustomSource = false;
                    $sourceName = $this->Source->getSourceByID($this->request->data['Profile']['source']);
                    $this->request->data['Profile']['source_id'] = $this->request->data['Profile']['source'];
                    $this->request->data['Profile']['source'] = $sourceName;
                }

                $this->User->id = $userInfo['User']['id'];
                if ($this->User->save($this->request->data)) {
                    $this->Profile->id = $userInfo['Profile']['id'];
                    $this->Profile->save($this->request->data);
                    $isCustomSource = false;
                    $this->Session->write('isCustomSource', $isCustomSource);
                    $this->Session->setFlash('Student information has been updated', 'flash_success');
                    $this->redirect(array('controller' => 'users', 'action' => 'details', $userInfo['User']['uuid']));
                } else {
                    $this->Session->write('isCustomSource', $isCustomSource);
                    $this->Session->setFlash('Sorry, something went wrong', 'flash_error');
                }
            }
            $this->Session->setFlash('Sorry, something went wrong', 'flash_error');
        }
    }

    public function instructor_note()
    {
        $this->set('title_for_layout', 'Note List - '.$this->Utilities->applicationName);
        $conditions = array('Note.created_by' => $this->userID, 'Note.contact_id' => null);

        $this->Paginator->settings = array(
            'conditions' => $conditions,
            'recursive' => -1,
            'limit' => $this->limit,
            'order' => 'Note.created DESC',
            'fields' => array('Note.*', 'Profile.*', 'User.*'),
            'joins' => array(
                array(
                    'table' => 'profiles',
                    'type' => 'LEFT',
                    'alias' => 'Profile',
                    'conditions' => array('Profile.user_id = Note.user_id')
                ),
                array(
                    'table' => 'users',
                    'type' => 'LEFT',
                    'alias' => 'User',
                    'conditions' => array('User.id = Note.user_id')
                )
            )
        );

        $notes = $this->Paginator->paginate('Note');
        $this->set('notes', $notes);
    }

    public function instructor_export()
    {
        $this->exportUser($this->userID);
    }

    public function instructor_import()
    {
        $this->set('title_for_layout', 'Import New User - '.$this->Utilities->applicationName);
        if ($this->request->is('post')) {
            $this->User->set($this->request->data);
            if ($this->User->importFormValidation()) {
                $document = $this->request->data['User']['file']['tmp_name'];
                $usersList = $this->Csv->import($document);
                $users = $this->Apps->arrangeUserList($usersList, $this->userID);
                if (Cache::write('users', $users)) {
                    $this->redirect(array('action' => 'preview'));
                }
            }
            else{
                $this->Session->setFlash('Sorry, something went wrong', 'flash_error');
            }
        }
    }

    public function tc_export()
    {
        $this->exportUser($this->userID);
    }


    public function instructor_preview()
    {
        $this->set('title_for_layout', 'Preview of User - '.$this->Utilities->applicationName);
        $users = Cache::read('users');
        $this->set('userList', $users);
        $duplicates = $this->Apps->searchArrayValue($users, 'duplicate', true);
        $this->set('duplicate', sizeof($duplicates));
        if ($this->request->is('post')) {
            $this->importUser($users);
        }
    }

    public function instructor_set_rating()
    {
        $this->setRating();
    }

    /**************************************************************************************************************/
    /******************************************       Custom Function          ************************************/
    /**************************************************************************************************************/


    /**
     * Export contact list
     */
    protected function exportUser($userID)
    {

        if($userID)
        {

            if($this->userRole == 4){
                $this->userID = $instructorIDs = $this->TcsUser->getIntructorIdsForTC($this->userID);
            }

            $conditions = array('Profile.instructor_id' => $this->userID);
        }
        else{
            $conditions = array();
        }


        $results = $this->User->find(
            'all',
            array(
                'conditions' => $conditions,
                'fields' => array('User.username', 'User.status', 'User.last_login',  'User.role', 'Profile.first_name', 'Profile.last_name', 'Profile.phone', 'Profile.fax', 'Profile.primary_email', 'Profile.secondary_email', 'Profile.city', 'Profile.state', 'Profile.postal_code', 'Profile.street_1', 'Profile.street_2', 'CountLesson.purchased_lesson_count', 'CountLesson.appointed_lesson_count', 'CountLesson.lesson_left'),
                'limit' => $this->limit,
                'recursive' => -1,
                'order' => 'User.id DESC',
                'joins' => array(
                    array(
                        'table' => 'count_lessons',
                        'type' => 'LEFT',
                        'alias' => 'CountLesson',
                        'conditions' => array('CountLesson.user_id = User.id')
                    ),
                    array(
                        'table' => 'profiles',
                        'type' => 'LEFT',
                        'alias' => 'Profile',
                        'conditions' => array('Profile.user_id = User.id')
                    )
                )
            )
        );
        if (!$results) {
            $this->Session->setFlash("Sorry, you don't have enough data to export", 'flash_error');
            $this->redirect($this->referer());
        }

        $excludePaths = array('User.id');
        $customHeaders = array(
            'User.first_name' => 'First Name',
            'Profile.last_name' => 'Last Name',
            'User.username' => 'Email',
            'Profile.phone' => 'Phone Number',
            'Profile.fax' => 'fax',
            'Profile.primary_email' => 'primary_email',
            'Profile.secondary_email' => 'secondary_email',
            'Profile.city' => 'city',
            'Profile.state' => 'state',
            'Profile.postal_code' => 'postal_code',
            'Profile.street_1' => 'street_1',
            'Profile.street_2' => 'street_2',
            'CountLesson.purchased_lesson_count' => 'Purchased Lesson',
            'CountLesson.appointed_lesson_count' => 'Appointed Lesson',
            'CountLesson.lesson_left' => 'Lesson Left',
        );

        $this->response->download('user_list.csv');
        $this->CsvView->quickExport($results, $excludePaths, $customHeaders);
    }

    protected function importUser($users)
    {
        $users = $this->Apps->searchArrayValue($users, 'duplicate', false);
        /*
         * @TODO need to save all users.
         */
    }

    /**
     * @param $data
     * @param $userRole
     *
     * Adding the users. Create new user for the application.
     */
    protected function addUser($data, $userRole)
    {
        $data['User']['uuid'] = String::uuid() ;
        $data['User']['role'] = $userRole;
        $data['Profile']['instructor_id'] = $this->userID;

        if($data['Profile']['is_custom_source'] == 1 && $data['Profile']['custom_source']){
            $data['Profile']['source'] = $data['Profile']['custom_source'];
            $data['Profile']['source_id'] = null;
            $isCustomSource = true;
        }
        else{
            $isCustomSource = false;
            $sourceName = $this->Source->getSourceByID($data['Profile']['source']);
            $data['Profile']['source_id'] = $data['Profile']['source'];
            $data['Profile']['source'] = $sourceName;
        }

        $this->User->create();
        
        if ($userRole == 2) {
            // students should generate stripe customer at this point.
            
            $email = $data['User']['username'];
            $name = 'Student - '. $data['Profile']['first_name']. ' '. $data['Profile']['last_name'];
            
            $stripeCustomer = array(
                'email' => $email,
                'description' => $name
            );
            try {
                $result = \Stripe\Customer::create($stripeCustomer);
            } catch (Exception $e) {
                $result = null;
            }
            if ($result && $result->id) {
                $data['User']['stripe_customer'] = $result->id;
            }
        }
        
        if ($this->User->saveAll($data)) {
            $isCustomSource = false;
            $this->Session->write('isCustomSource', $isCustomSource);
            $this->Session->setFlash('New user has been successfully created', 'flash_success');
            $this->sendEmailNotification('when_add_student', array('userID' => $this->userID, 'new_user' => $this->User->getLastInsertID()));
            $this->redirect(array('controller' => 'users', 'action' => 'list'));
        }
        else {
            $this->Session->write('isCustomSource', $isCustomSource);
            $this->Session->setFlash('Sorry, something went wrong', 'flash_error');
        }
    }

    /**
     * This function will update the last login time.
     */
    public function updateLoginTime()
    {
        $loginTime = date('Y-m-d H:i:s');
        $this->User->id = $this->Session->read('Auth.User.id');
        $this->User->saveField('last_login', $loginTime);
    }

    /**
     * @param $userUUID
     * @return array|bool
     * @throws NotFoundException
     *
     * Fetch student details from here
     */
    protected function fetchUserByUUID($userUUID)
    {
        $userInfo = $this->User->getUser($userUUID);
        if (empty($userInfo)) {
            throw new NotFoundException;
        }
        $this->set('userInformation', $userInfo);
        return $userInfo;
    }

    /**
     * @param $searchTerm
     *
     * Search student from here
     */
    protected function searchUser($searchTerm)
    {
        if ($searchTerm) {
            $this->Paginator->settings = array(
                'fields' => array('User.id', 'User.uuid', 'User.username', 'User.role', 'User.status', 'User.created', 'User.last_login', 'Profile.first_name', 'Profile.last_name', 'Profile.phone'),
                'conditions' => array(
                    'Profile.instructor_id' => $this->userID,
                    'OR' => array(
                        'User.username LIKE' => '%' . $searchTerm . '%',
                        'Profile.first_name LIKE' => '%' . $searchTerm . '%',
                        'Profile.last_name LIKE' => '%' . $searchTerm . '%',
                        'Profile.phone LIKE' => '%' . $searchTerm . '%',
                    ),

                ),
                'joins' => array(
                    array(
                        'table' => 'profiles',
                        'type' => 'LEFT',
                        'alias' => 'Profile',
                        'conditions' => array('Profile.user_id = User.id')
                    )
                ),

                'limit' => $this->limit,
                'order' => array('User.id' => 'DESC')
            );

            $this->set('users', $this->Paginator->paginate('User'));
        } else {
            $this->Session->setFlash("Sorry, you didn't search anything", 'flash_error');
        }
    }

    public function changeStatus($userID, $status)
    {
        $this->User->id = $userID;
        if($this->User->saveField('status', $status)){
            return true;
        }
        return false;
    }


    public function twitterLogin($twitterAuth, $twitterProfile)
    {
        if($twitterProfile)
        {
            $firstName = '';
            $lastName = '';
            $name = explode(' ', $twitterProfile[0]->user->name);
            if(isset($name[0])){
                $firstName = $name[0];
            }
            if(isset($name[1])){
                $lastName = $name[1];
            }
        }

        $profilePic = 'https://twitter.com/'.$twitterAuth['screen_name'].'/profile_image?size=original';
        $twUserData = array(
            'User' => array(
                'uuid' => String::uuid(),
                'role' => 2,
                'password' => 'golf-swing-prescription',
                'twitter_oauth_token' => $twitterAuth['oauth_token'],
                'twitter_oauth_token_secret' => $twitterAuth['oauth_token_secret'],
                'twitter_user_id' => $twitterAuth['user_id'],
                'social_pic' => $profilePic,
                'twitter_screen_name' => $twitterAuth['screen_name'],
            ),
            'Profile' => array(
                'first_name' => $firstName,
                'last_name' => $lastName,
            )
        );

        $isAlreadySignUp = $this->User->find('first', array('recursive' => 0, 'conditions' => array('twitter_user_id' => $twUserData['User']['twitter_user_id'])));
        if(empty($isAlreadySignUp))
        {
            $this->Session->write('userInfo', $twUserData);
            $this->Session->write('is_social_login', true);
            $this->redirect(array('controller' => 'users', 'action' => 'payment'));
            //$this->User->saveAll($twUserData);
        }
        else{
            $this->User->id = (int) $isAlreadySignUp['User']['id'];
            $this->User->saveField('social_pic', $profilePic);
        }

        $user = $this->User->find('first', array('recursive' => 0, 'conditions' => array('twitter_user_id' => $twUserData['User']['twitter_user_id'])));
        if($user && sizeof($user['User']) > 0 && sizeof($user['Profile']) > 0){
            $profile = $user['Profile'];
            unset($user['Profile']);
            $user['User']['Profile'] = $profile;
            $this->customLogin($user['User']);
        }
        else{
            $this->Session->setFlash("Sorry, something went wrong", 'flash_error');
        }
    }

    protected function getTwitterProfile($oToken, $oTokenSec, $screenName, $count)
    {
        $newConnect = new TwitterOAuth($this->twiConsumerKey, $this->twiConsumerSecKey, $oToken, $oTokenSec);
        $url = "https://api.twitter.com/1.1/statuses/user_timeline.json?screen_name={$screenName}&count={$count}";
        $twitterProfile = $newConnect->get($url);
        return $twitterProfile;
    }

    /**
     *
     */
    public function facebookLogin()
    {

        require APP.'Vendor/facebook-php-sdk-v4-4.0-dev/autoload.php';
        FacebookSession::setDefaultApplication( $this->facebookApiID, $this->facebookApsSecretKey);
        $url = Router::url('/', true).'users/login';

        $helper = new FacebookRedirectLoginHelper($url);
        try {
            $session = $helper->getSessionFromRedirect();
        }
        catch( Exception $ex ) {
            $this->Session->setFlash("Sorry, something went wrong", 'flash_error');
            $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }

        if ( isset( $session ) ) {
            $request = new FacebookRequest( $session, 'GET', '/me' );
            $response = $request->execute();

            $graphObject = $response->getGraphObject();
            $fbid = $graphObject->getProperty('id');              // To Get Facebook ID
            $fbfullname = $graphObject->getProperty('name'); // To Get Facebook full name

            $firstName = '';
            $lastName = '';
            $name = explode(' ', $fbfullname);
            if(isset($name[0])){
                $firstName = $name[0];
            }
            if(isset($name[1])){
                $lastName = $name[1];
            }

            $profilePic = 'https://graph.facebook.com/'.$fbid.'/picture?type=large';

            $fbUserData = array(
                'User' => array(
                    'uuid' => String::uuid(),
                    'facebook_user_id' => $fbid,
                    'social_pic' => $profilePic,
                    'password' => 'golf-swing-prescription',
                ),
                'Profile' => array(
                    'first_name' => $firstName,
                    'last_name' => $lastName,
                )
            );

            $isAlreadySignUp = $this->User->find('first', array('recursive' => 0, 'conditions' => array('facebook_user_id' => $fbUserData['User']['facebook_user_id'])));

            if(empty($isAlreadySignUp))
            {
                $this->Session->write('userInfo', $fbUserData);
                $this->Session->write('is_social_login', true);
                $this->redirect(array('controller' => 'users', 'action' => 'payment'));
                //$this->User->saveAll($fbUserData);
            }
            else{
                $this->User->id = (int) $isAlreadySignUp['User']['id'];
                $this->User->saveField('social_pic', $profilePic);
            }


            $user = $this->User->find('first', array('recursive' => 0, 'conditions' => array('facebook_user_id' => $fbUserData['User']['facebook_user_id'])));

            if($user && sizeof($user['User']) > 0 && sizeof($user['Profile']) > 0){
                $profile = $user['Profile'];
                unset($user['Profile']);
                $user['User']['Profile'] = $profile;
                $this->customLogin($user['User']);
            }
            else{
                $this->Session->setFlash("Sorry, something went wrong", 'flash_error');
            }

        }
        else {
            $fbPermissions = '';

            $loginUrl = $helper->getLoginUrl(array('scope' => $fbPermissions));
            $this->set('loginUrl', $loginUrl);
        }
    }

    public function customLogin($user){
        if($user && sizeof($user) > 0){
            if ($this->Auth->login($user)) {
                $this->updateLoginTime();
                $this->redirect(array('controller' => 'dashboards', 'action' => 'index'));
            }
            else {
                $this->redirect(array('controller' => 'users', 'action' => 'login'));
            }
        }
        else{
            $this->Session->setFlash("Sorry, something went wrong", 'flash_error');
        }
    }

    private function setRating()
    {
        $this->autoLayout = false;
        $this->autoRender = false;
        $this->response->type('application/javascript');

        $userID = (int) $this->request->data['userID'];
        $rating = (int) $this->request->data['rating'];

        $this->User->id = $userID;
        if($this->User->saveField('rating', $rating))
        {
            $this->response->body(json_encode(1));
        }
        else{
            $this->response->body(json_encode(0));
        }
    }

    public function instructor_getUserIDByEmail()
    {
        $this->autoLayout = false;
        $this->autoRender = false;
        $this->response->type('application/javascript');

        $email = $this->request->data['email'];
        $studentID = $this->User->getUserByEMail($email);
        $this->response->body(json_encode($studentID));
    }

    /**
     * @param $studentID
     */
    public function instructor_userUserByID($studentID)
    {
        $this->autoLayout = false;
        $this->autoRender = false;
        $this->response->type('application/javascript');

        $user = $this->User->find(
            'first',
            array(
                'fields' => array('User.username'),
                'conditions' => array('User.id' => $studentID),
                'contain' => array('Profile' => array('fields' => array('Profile.first_name', 'Profile.last_name')))
            )
        );

        if ($user) {
            $this->response->body(json_encode($user));
        }
        else {
            $this->response->body(json_encode(null));
        }
    }

}