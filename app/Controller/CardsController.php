<?php
App::uses('AppController', 'Controller');
/**
 * Class AppointmentsController
 *
 * @property Appointment $Appointment..
 * @property Attachment @Attachment
 * @property Benchmark $Benchmark
 * @property Billing $Billing
 * @property BillingsPackage @BillingsPackage
 * @property BlockTime @BlockTime
 * @property Card $Card
 * @property Config $Config
 * @property Contact $Contact
 * @property CountLesson $CountLesson
 * @property EmailsEvent $EmailsEvent
 * @property EmailsEventsSetting $EmailsEventsSetting
 * @property Event $Event
 * @property GroupLesson $GroupLesson
 * @property GroupLessonsStudents $GroupLessonsStudents
 * @property Note @Note
 * @property Package $Package
 * @property PackagesUser $PackagesUser
 * @property PackagesOrder $PackagesOrder
 * @property Profile $Profile
 * @property Source $Source
 * @property TcsUser $TcsUser
 * @property User $User
 * @property Waiting $Waiting
 *
 * @property PaginatorComponent $Paginator
 * @property EmailComponent $Email
 * @property UtilitiesComponent $Utilities
 * @property AuthorizeNetComponent $AuthorizeNet
 * @property DataTableComponent $DataTable
 * @property ImgComponent $Img
 * @property AppsComponent $Apps
 *
 */

class CardsController extends AppController
{
    /**
     * The name of this controller.
     *
     * @var string
     */
    public $name = 'Cards';

    /**************************************************************************************************************/
    /********************************************        Admin Panel          *************************************/
    /**************************************************************************************************************/

    /**
     *
     */
    public function admin_index()
    {

    }
    /**************************************************************************************************************/
    /******************************************       Instructor Panel         ************************************/
    /**************************************************************************************************************/

    /**
     *
     */
    public function instructor_index()
    {

    }

    /**************************************************************************************************************/
    /******************************************         Student Panel         *************************************/
    /**************************************************************************************************************/

    /**
     *
     */
    public function student_index()
    {
        $this->set('title_for_layout', 'My Card - '.$this->Utilities->applicationName);
        
        $user = $this->User->getUser($this->userID);
        $cards = \Stripe\Customer::retrieve($user['User']['stripe_customer'])->sources->all(array('object' => 'card'));
        $this->set('cards', $cards['data']);
    }

    /**
     *
     */
    public function student_add()
    {
        $this->set('title_for_layout', 'Add New Card - '.$this->Utilities->applicationName);

        if ($this->request->is('post')) {
            $data = $this->request->data;
            $stripeToken = $data['stripe']['token'];
            
            $user = $this->User->getUser($this->userID);
            $customer = \Stripe\Customer::retrieve($user['User']['stripe_customer']);
            
            $customer->sources->create(array("source" => $stripeToken));
            
            $this->redirect(array('controller' => 'cards', 'action' => 'index'));
        }
    }

    /**
     * @param $uuid
     */
    public function student_delete($uuid)
    {
        $this->deleteCard($uuid);
    }



    /**************************************************************************************************************/
    /******************************************       Custom Function          ************************************/
    /**************************************************************************************************************/

    /**
     * @param $cardName
     * @param $cardNumber
     * @param $expiryYear
     * @param $expiryMonth
     * @return array
     */
    public function createAuthorizeAccount($cardName, $cardNumber, $expiryYear, $expiryMonth)
    {
        $identifier = 'xxxx xxxx xxxx '.substr($cardNumber, -4);
        $userInfo = $this->Profile->getProfile('first', array('user_id' => $this->userID));
        $expireDate = $expiryYear . '-' . $expiryMonth;

        $basicInfo = array(
            'description' => '',
            'name' => $userInfo['Profile']['name'],
            'email' => $userInfo['User']['username'],
        );
        $cardInfo = array(
            'cardNumber' => $cardNumber,
            'exp_date' => $expireDate,
        );
        $shippingInfo = array(
            'firstName' => $userInfo['Profile']['first_name'],
            'lastName' => $userInfo['Profile']['last_name'],
            'companyName' => 'Golf Swing Prescription',
            'address' => $userInfo['Profile']['billing_street_1'],
            'city' => $userInfo['Profile']['billing_city'],
            'state' =>$userInfo['Profile']['billing_state'],
            'zip' => $userInfo['Profile']['billing_postal_code'],
            'country' => $userInfo['Profile']['billing_country'],
            'phoneNumber' => $userInfo['Profile']['phone'],
            'faxNumber' => $userInfo['Profile']['fax'],
        );

        $authorizedCustomerID = 0;
        $authorizedPaymentProfileID = 0;
        $authorizedShippingID = 0;
        $createAuthorisedAcc = $this->AuthorizeNet->createCustomerProfileCIM($basicInfo);
        $authorizedCustomerID = (int)$createAuthorisedAcc['id'];
        $createAuthorisedPaymentAcc = $this->AuthorizeNet->createCustomerPaymentProfileCIM(
            $authorizedCustomerID,
            $cardInfo
        );
        if (isset($createAuthorisedPaymentAcc['id'])) {
            $authorizedPaymentProfileID = (int)$createAuthorisedPaymentAcc['id'];
        }
        $createAuthorisedShippingAcc = $this->AuthorizeNet->createCustomerShippingAddressCIM(
            $authorizedCustomerID,
            $shippingInfo
        );
        if (isset($createAuthorisedShippingAcc['id'])) {
            $authorizedShippingID = (int)$createAuthorisedShippingAcc['id'];
        }

        $cardInfo = array(
            'uuid' => String::uuid() ,
            'user_id' => $this->userID,
            'name' => $cardName,
            'identifier' => $identifier,
            'is_visible' => 1,
            'authorise_dot_net_customer_profile_id' => $authorizedCustomerID,
            'authorise_dot_net_payment_profile_id' => $authorizedPaymentProfileID,
            'authorise_dot_net_shipping_id' => $authorizedShippingID,
        );


        return $cardInfo;
    }

    /**
     * @param $cardUUID
     * @throws NotFoundException
     */
    protected function deleteCard($cardUUID)
    {
        $user = $this->User->getUser($this->userID);
        $customer = \Stripe\Customer::retrieve($user['User']['stripe_customer']);
        
        $customer->sources->retrieve($cardUUID)->delete();

        $this->redirect($this->referer());
    }

    /**
     * @param $data
     * @return bool
     */
    public function updateBillingInfo($data)
    {
        $this->Profile->id = $data['Profile']['id'];
        $profileData['Profile']['billing_street_1'] = $data['Card']['billing_street_1'];
        $profileData['Profile']['billing_street_2'] = $data['Card']['billing_street_2'];
        $profileData['Profile']['billing_city'] = $data['Card']['billing_city'];
        $profileData['Profile']['billing_state'] = $data['Card']['billing_state'];
        $profileData['Profile']['billing_postal_code'] = $data['Card']['billing_postal_code'];
        $profileData['Profile']['billing_country'] = $data['Card']['billing_country'];
        if($this->Profile->save($profileData)){
            return true;
        }
        return false;
    }

    /**
     *
     */
    public function student_getStudentCards()
    {
        $this->autoLayout = false;
        $this->autoRender = false;
        $this->response->type('application/javascript');
        $studentID = $this->request->query['studentID'];

        $user = $this->User->getUser($studentID);
        
        $stripe_customer = $user['User']['stripe_customer'];
        
        if (empty($stripe_customer)) {
            return $this->response->body(json_encode(array()));
        }
        
        $cards = \Stripe\Customer::retrieve($stripe_customer)->sources->all(array('object' => 'card'));
        if ($cards){
            $this->response->body(json_encode($cards['data']));
        } else {
            $this->response->body(json_encode(array()));
        }
    }
}

