<?php
App::uses('AppController', 'Controller');
/**
 * Class AppointmentsController
 *
 * @property Appointment $Appointment..
 * @property Attachment @Attachment
 * @property Benchmark $Benchmark
 * @property Billing $Billing
 * @property BillingsPackage @BillingsPackage
 * @property BlockTime @BlockTime
 * @property Card $Card
 * @property Config $Config
 * @property Contact $Contact
 * @property CountLesson $CountLesson
 * @property EmailsEvent $EmailsEvent
 * @property EmailsEventsSetting $EmailsEventsSetting
 * @property Event $Event
 * @property GroupLesson $GroupLesson
 * @property GroupLessonsStudents $GroupLessonsStudents
 * @property Note @Note
 * @property Package $Package
 * @property PackagesUser $PackagesUser
 * @property PackagesOrder $PackagesOrder
 * @property Profile $Profile
 * @property Source $Source
 * @property TcsUser $TcsUser
 * @property User $User
 * @property Waiting $Waiting
 *
 * @property PaginatorComponent $Paginator
 * @property EmailComponent $Email
 * @property UtilitiesComponent $Utilities
 * @property AuthorizeNetComponent $AuthorizeNet
 * @property DataTableComponent $DataTable
 * @property ImgComponent $Img
 * @property AppsComponent $Apps
 *
 */

class ContactsController extends AppController
{
    /**
     * The name of this controller.
     *
     * @var string
     */
    public $name = 'Contacts';


    /**************************************************************************************************************/
    /********************************************        Admin Panel          *************************************/
    /**************************************************************************************************************/

    /**
     * Admin index page will redirect into the list page.
     */
    public function admin_index()
    {
        $this->redirect(array('controller' => 'contacts', 'action' => 'list'));
    }

    /**
     * Showing contact list for admin.
     */
    public function admin_list($sort = null)
    {
        $this->set('title_for_layout', 'Contact List - '.$this->Utilities->applicationName);
        $this->contactList($sort);
    }

    /**
     * Add new contact page for admin.
     */
    public function admin_add()
    {
        $this->set('title_for_layout', 'Add New Contact - '.$this->Utilities->applicationName);
        if ($this->request->is('post')) {
            $this->addContact();
        }
    }

    /**
     * Here admin can edit leads information.
     *
     * @param null $contactID
     * @throws NotFoundException
     */
    public function admin_edit($contactID = null)
    {
        $this->set('title_for_layout', 'Edit Contact - '.$this->Utilities->applicationName);
        $this->editContact($contactID);
    }

    /**
     * @param null $contactID
     * @throws NotFoundException
     *
     * Here admin can delete any lead.
     */
    public function admin_delete($contactID = null)
    {
        $this->deleteContact($contactID);
    }

    /**
     * Import contact list for the admin.
     */
    public function admin_import()
    {
        $this->set('title_for_layout', 'Import New Contact - '.$this->Utilities->applicationName);
        if ($this->request->is('post')) {
            $this->importContact();
        }
    }

    /**
     * Export contact list for the admin
     */
    public function admin_export()
    {
        $this->exportContact();
    }

    /**
     * contact preview page for the admin
     */
    public function admin_preview()
    {
        $this->importPreview();
    }

    /**************************************************************************************************************/
    /******************************************       Instructor Panel         ************************************/
    /**************************************************************************************************************/

    /**
     * Instructor index page will redirect into the list page.
     */
    public function instructor_index()
    {
        $this->redirect(array('controller' => 'contacts', 'action' => 'list'));
    }

    /**
     * Showing contact list for instructor.
     */
    public function instructor_list($sort = null)
    {
        $this->set('title_for_layout', 'Contact List - '.$this->Utilities->applicationName);
        $this->contactList($sort);
    }

    /**
     * Add new contact page for instructor.
     */
    public function instructor_add()
    {
        $this->set('title_for_layout', 'Add New Contact - '.$this->Utilities->applicationName);
        $this->set('sources', $this->Source->getSourceList());
        if ($this->request->is('post')) {
            $this->addContact();
        }
    }

    /**
     * Here instructor can edit leads information.
     *
     * @param null $contactID
     * @throws NotFoundException
     */
    public function instructor_edit($contactID = null)
    {
        $this->set('title_for_layout', 'Edit Contact - '.$this->Utilities->applicationName);
        $this->set('sources', $this->Source->getSourceList());
        $this->editContact($contactID);
    }

    /**
     * @param null $contactID
     * @throws NotFoundException
     *
     * Here instructor can delete any lead.
     */
    public function instructor_delete($contactID = null)
    {
        $this->deleteContact($contactID);
    }

    /**
     * Import contact list for the instructor.
     */
    public function instructor_import()
    {
        $this->set('title_for_layout', 'Import New Contact - '.$this->Utilities->applicationName);
        if ($this->request->is('post')) {
            $this->importContact();
        }
    }

    /**
     * Export contact list for the instructor
     */
    public function instructor_export()
    {
        $this->exportContact();
    }

    /**
     * contact preview page for the instructor
     */
    public function instructor_preview()
    {
        $this->set('title_for_layout', 'Preview of Contact - '.$this->Utilities->applicationName);
        $this->importPreview();
    }

    /**
     *
     */
    public function instructor_search()
    {
        $this->set('title_for_layout', 'Search Contact - '.$this->Utilities->applicationName);
        $this->set('allContact', $this->Contact->countContact($this->userID));
        $this->set('studentContact', $this->Contact->countContact($this->userID, array('Contact.account_type' => 1)));
        $this->set('customerContact', $this->Contact->countContact($this->userID, array('Contact.account_type' => 2)));
        $this->set('leadContact', $this->Contact->countContact($this->userID, array('Contact.account_type' => 3)));
        $searchTerm = $this->request->query['contact'];
        $this->searchContact($searchTerm);
    }

    public function tc_search()
    {
        $instructorIDs = $this->TcsUser->getIntructorIdsForTC($this->userID);
        $this->set('title_for_layout', 'Search Contact - '.$this->Utilities->applicationName);
        $this->set('allContact', $this->Contact->countContact($instructorIDs));
        $this->set('studentContact', $this->Contact->countContact($instructorIDs, array('Contact.account_type' => 1)));
        $this->set('customerContact', $this->Contact->countContact($instructorIDs, array('Contact.account_type' => 2)));
        $this->set('leadContact', $this->Contact->countContact($instructorIDs, array('Contact.account_type' => 3)));
        $searchTerm = $this->request->query['contact'];
        //var_dump($searchTerm); die();
        $this->searchContact($searchTerm);
    }

    /**
     * Export contact list for the instructor
     */
    public function tc_export()
    {
        $this->exportContact();
    }

    /**
     *
     */
    public function instructor_note()
    {
        $this->set('title_for_layout', 'Take Note - '.$this->Utilities->applicationName);
        if($this->request->is('post')){
            $contactID = null;
            $studentID = null;


            $contactPersonInfo = explode(' - ', $this->request->data['Note']['contactPerson']);

            if($this->request->data['noteFor'] == 'contact'){
                $contactID = $this->Contact->getContactByEMail($contactPersonInfo[1]);
            }

            if($this->request->data['noteFor'] == 'student'){
                $studentID = $this->User->getUserByEMail($contactPersonInfo[1]);
            }


            if($contactID || $studentID){
                $this->request->data['Note']['user_id'] = $studentID;
                $this->request->data['Note']['contact_id'] = $contactID;
                $this->request->data['Note']['created_by'] = $this->userID;
                if(isset($this->request->data['Note']['note_date'])){
                    $this->request->data['Note']['note_date'] = date('Y-m-d H:i:s', strtotime($this->request->data['Note']['note_date']));
                }
                else{
                    unset($this->request->data['Note']['note_date']);
                }

                if ($this->Note->save($this->request->data)) {
                    $this->Session->setFlash('Note has been saved', 'flash_success');
                }
                else
                {
                    $this->Session->setFlash('Sorry, something went wrong', 'flash_error');
                }
            }
            else {
                $this->Session->setFlash('Sorry, something went wrong', 'flash_error');
            }
            $this->redirect($this->referer());
        }
    }

    /**
     * @param null $sort
     */
    public function instructor_note_list($sort = null)
    {
        $search = false;
        $this->set('title_for_layout', 'List of Notes - '.$this->Utilities->applicationName);
        $conditions = array(
            'OR' => array(
                'Profile.instructor_id' => $this->userID,
                'Contact.user_id' => $this->userID
            )
        );
        $title = 'Follow Up Notes';
        $now = date('Y-m-d H:i:s');

        if(isset($this->request->query['status']) && $this->request->query['status'] && $this->request->query['status'] != 'Choose Status')
        {
            $sort = $this->request->query['status'];
            $search = true;
        }

        if($sort == 'leads'){
            $conditions = array_merge($conditions, array('Note.contact_id !=' => null));
            $title = 'Lead\'s follow up notes';
        }
        elseif($sort == 'users'){
            $conditions = array_merge($conditions, array('Note.user_id !=' => null));
            $title = 'User\'s follow up notes';
        }
        elseif($sort == 'no_rating'){
            $noRatingConditions['OR'] = array(
                array('Note.rating' => array('', 0)),
                array('Note.rating' => NULL)
            );
            $conditions = array_merge($conditions, $noRatingConditions);
            $title = 'No rating follow up notes';
        }
        elseif($sort == 'rating_one'){
            $conditions = array_merge($conditions, array('Note.rating' => 1));
            $title = 'One star rating follow up notes';
        }
        elseif($sort == 'rating_two'){
            $conditions = array_merge($conditions, array('Note.rating' => 2));
            $title = 'Two star rating follow up notes';
        }
        elseif($sort == 'rating_three'){
            $conditions = array_merge($conditions, array('Note.rating' => 3));
            $title = 'Three star rating follow up notes';
        }
        elseif($sort == 'rating_four'){
            $conditions = array_merge($conditions, array('Note.rating' => 4));
            $title = 'Four star rating follow up notes';
        }
        elseif($sort == 'rating_five'){
            $conditions = array_merge($conditions, array('Note.rating' => 5));
            $title = 'Five star rating follow up notes';
        }
        elseif($sort == 'rating_five'){
            $conditions = array_merge($conditions, array('Note.rating' => 5));
            $title = 'Five star rating follow up notes';
        }
        elseif($sort == 'upcoming'){
            $conditions = array_merge($conditions, array('Note.note_date >' => $now, 'Note.status' => 1));
            $title = 'Upcoming follow up notes';
        }
        elseif($sort == 'expired'){
            $conditions = array_merge($conditions, array('Note.note_date <' => $now, 'Note.status' => 1));
            $title = 'Expired follow up notes';
        }
        elseif($sort == 'followed'){
            $conditions = array_merge($conditions, array('Note.status' => 2));
            $title = 'Followed follow up notes';
        }
        elseif($sort == 'unfollowed'){
            $conditions = array_merge($conditions, array('Note.status' => 3));
            $title = 'Unfollowed follow up notes';
        }

        if(isset($this->request->query['noteFor']) && $this->request->query['noteFor'] && $this->request->query['name']){
            if($this->request->query['noteFor'] == 'contact'){
                $contact = explode('-', $this->request->query['name']);
                $contactID = (int) $this->Contact->getContactByEMail(trim($contact[1]));
                $conditions = array_merge($conditions, array('Note.contact_id' => $contactID));
            }
            else{
                $studentInfo = explode('-', $this->request->query['name']);
                $studentID = $this->User->getUserByEMail(trim($studentInfo[1]));
                $conditions = array_merge($conditions, array('Note.user_id' => $studentID));
            }
            $search = true;
        }

        if(isset($this->request->query['starting_date']) && $this->request->query['starting_date'] && $this->request->query['starting_date'] != '')
        {
            $startingDate = $this->request->query['starting_date'];
            $conditions = array_merge($conditions, array(
                    'Note.note_date >=' => date('Y-m-d H:i:s', strtotime($startingDate)),
                )
            );
            $search = true;
        }

        if(isset($this->request->query['ending_date']) && $this->request->query['ending_date'] && $this->request->query['ending_date'] != '')
        {
            $endingDate = $this->request->query['ending_date'];
            $conditions = array_merge($conditions, array(
                    'Note.note_date <=' => date('Y-m-d H:i:s', strtotime($endingDate)),
                )
            );
            $search = true;
        }

        if(isset($this->request->query['rating']) && $this->request->query['rating'])
        {
            $rating = $this->request->query['rating'];
            $conditions = array_merge($conditions, array(
                    'Note.rating' => $rating
                )
            );
            $search = true;
        }

        if(isset($this->request->query['note']) && $this->request->query['note'])
        {
            $note = $this->request->query['note'];
            $conditions = array_merge($conditions, array(
                    'Note.note LIKE' => '%'.$note.'%',
                )
            );
            $search = true;
        }

        $this->Paginator->settings = array(
            'conditions' => $conditions,
            'recursive' => -1,
            'limit' => $this->limit,
            'order' => 'Note.rating DESC',
            'fields' => array('Profile.first_name', 'Profile.last_name', 'Profile.phone', 'User.id', 'User.uuid', 'User.username', 'Note.id', 'Note.note', 'Note.rating', 'Note.note_date', 'Note.status', 'Contact.id', 'Contact.first_name', 'Contact.last_name', 'Contact.phone', 'Contact.email'),
            'joins' => array(
                array(
                    'table' => 'contacts',
                    'type' => 'LEFT',
                    'alias' => 'Contact',
                    'conditions' => array('Contact.id = Note.contact_id')
                ),
                array(
                    'table' => 'profiles',
                    'type' => 'LEFT',
                    'alias' => 'Profile',
                    'conditions' => array('Profile.user_id = Note.user_id')
                ),
                array(
                    'table' => 'users',
                    'type' => 'LEFT',
                    'alias' => 'User',
                    'conditions' => array('User.id = Note.user_id')
                )
            )
        );

        if($search)
        {
            $title = 'Search Result';
        }

        $notes = $this->Paginator->paginate('Note');
        $overview = $this->Note->getNoteOverview();
        $this->set('notes', $notes);
        $this->set('title', $title);
        $this->set('overview', $overview);
    }

    /**
     * @param $type
     * @param null $query
     */
    public function instructor_getContacts($type, $query = null)
    {
        $this->autoLayout = false;
        $this->autoRender = false;
        $this->response->type('application/javascript');

        if($type == 'student'){
            $result = $contacts = $this->Profile->getUserListTypeHead($this->userID, $query);
        }
        elseif($type == 'contact'){
            $result = $contacts = $this->Contact->getContactList($this->userID, $query);
        }
        $this->response->body(json_encode($result));
    }

    public function tc_getContacts($type, $query = null)
    {
        $this->autoLayout = false;
        $this->autoRender = false;
        $this->response->type('application/javascript');

        if($type == 'contact'){
            $userID = $this->TcsUser->getIntructorIdsForTC($this->userID);
            $result = $contacts = $this->Profile->getUserListTypeHeadForTC($userID, $query);
        }
        elseif($type == 'student'){
            $userID = $this->TcsUser->getIntructorIdsForTC($this->userID);
            $result = $contacts = $this->Profile->getStudentListTypeHeadForTC($userID, $query);
        }


        $this->response->body(json_encode($result));
    }



    public function instructor_getStudent($instructorID, $query = null)
    {
        $this->autoLayout = false;
        $this->autoRender = false;
        $this->response->type('application/javascript');

        $result = $contacts = $this->Profile->getUserListTypeHead($instructorID, $query);
        $this->response->body(json_encode($result));
    }

    public function tc_getStudent($instructorID, $query = null)
    {
        $this->autoLayout = false;
        $this->autoRender = false;
        $this->response->type('application/javascript');

        $result = $contacts = $this->Profile->getUserListTypeHeadForTC($instructorID, $query);
        $this->response->body(json_encode($result));
    }

    /**
     *
     */
    public function instructor_set_rating()
    {
        $this->setRating();
    }
    /**************************************************************************************************************/
    /******************************************         TC Panel         *************************************/
    /**************************************************************************************************************/

    /**
     * Instructor index page will redirect into the list page.
     */
    public function tc_index()
    {
        $this->redirect(array('controller' => 'contacts', 'action' => 'list'));
    }

    /**
     * Showing contact list for instructor.
     */
    public function tc_list($sort = null)
    {
        $this->set('title_for_layout', 'Contact List - '.$this->Utilities->applicationName);
        $this->contactList($sort);
    }
    /**
     * @param $contactID
     * @throws NotFoundException
     */
    public function tc_view($contactID)
    {
        $this->set('title_for_layout', 'Contact Details - '.$this->Utilities->applicationName);
        $this->Contact->id = $contactID;
        if (!$this->Contact->exists()) {
            throw new NotFoundException('Invalid Contact');
        }

        $this->set('contact', $this->Contact->getContactByID($contactID));
        $this->set('notes', $this->Note->getContactNotes($contactID));

        if ($this->request->is('post')) {
            $this->request->data['Note']['contact_id'] = $contactID;
            $this->request->data['Note']['created_by'] = $this->userID;
            $this->request->data['Note']['note_date'] = date('Y-m-d H:i:s', strtotime($this->request->data['Note']['note_date']));

            if ($this->Note->save($this->request->data)) {
                $this->Session->setFlash('Note has been saved', 'flash_success');
                $this->redirect(array('controller' => 'contacts', 'action' => 'view', $contactID));
            } else {
                $this->Session->setFlash('Sorry, something went wrong', 'flash_error');
            }

        }
    }

    /**
     * Add new contact page for instructor.
     */
    public function tc_add()
    {
        $this->set('title_for_layout', 'Add New Contact - '.$this->Utilities->applicationName);
        $this->set('sources', $this->Source->getSourceList());
        $this->set('instructors', $this->TcsUser->getInstructorIdsByTCID($this->userID));
        if ($this->request->is('post')) {

           // $this->request->data['Contact']['user_id'] = $this->request->data['Contact']['user_id'];

            if($this->request->data['Contact']['is_custom_source'] == 1 && $this->request->data['Contact']['custom_source']){
                $this->request->data['Contact']['source'] = $this->request->data['Contact']['custom_source'];
                $this->request->data['Contact']['source_id'] = null;
                $isCustomSource = true;
            }
            else{
                $isCustomSource = false;
                $sourceName = $this->Source->getSourceByID($this->request->data['Contact']['source']);
                $this->request->data['Contact']['source_id'] = $this->request->data['Contact']['source'];
                $this->request->data['Contact']['source'] = $sourceName;
            }

            $this->Contact->create();
            if ($this->Contact->save($this->request->data)) {
                $isCustomSource = false;
                $this->Session->write('isCustomSource', $isCustomSource);
                $this->Session->setFlash('New contact has been saved', 'flash_success');
                $this->redirect(array('controller' => 'contacts', 'action' => 'list'));
            } else {
                $this->Session->write('isCustomSource', $isCustomSource);
                $this->Session->setFlash('Sorry, something went wrong', 'flash_error');
            }


            //$this->addContact();
        }
    }

    /**
     * Here instructor can edit leads information.
     *
     * @param null $contactID
     * @throws NotFoundException
     */
    public function tc_edit($contactID = null)
    {
        $this->set('title_for_layout', 'Edit Contact - '.$this->Utilities->applicationName);
        $this->set('sources', $this->Source->getSourceList());
        $this->set('instructors', $this->TcsUser->getInstructorIdsByTCID($this->userID));
        $this->editContact($contactID);
    }

    /**
     * @param null $contactID
     * @throws NotFoundException
     *
     * Here instructor can delete any lead.
     */
    public function tc_delete($contactID = null)
    {
        $this->deleteContact($contactID);
    }


    /**************************************************************************************************************/
    /******************************************       Custom Function          ************************************/
    /**************************************************************************************************************/

    /**
     * @param $lug
     */
    protected function contactList($slug)
    {
        $title = 'LEADS LIST (Source: All)';
        $getTcInstructors = $this->TcsUser->getInstructorIdsByTCID($this->userID);
        $this->set('instructors', $getTcInstructors);

        if($this->userRole == 4){
            $this->userID = $instructorIDs = $this->TcsUser->getIntructorIdsForTC($this->userID);
        }
        $conditions = array('Contact.user_id' => $this->userID, 'Contact.status !=' => 9);


        if (isset($this->request->query['instructor']) && $this->request->query['instructor']) {
            $instructorID = $this->request->query['instructor'];
            if($instructorID[0] == ''){
                unset($instructorID[0]);
            }
            if (!empty($instructorID)) {
                $conditions = array_merge($conditions, array('Contact.user_id' => $instructorID));
            }
        }

        /*
         * Contact Search
         */
        if (isset($this->request->query['contact']) && $this->request->query['contact'] && $this->request->query['contact'] != '') {
            $contactInfo = explode('-', $this->request->query['contact']);
            $contacttID = $this->Contact->getContactByEMail(trim($contactInfo[1]));
            if ($contacttID) {
                $conditions = array_merge($conditions, array('Contact.id' => $contacttID));
            }
        }

        if (isset($this->request->query['email']) && $this->request->query['email'] && $this->request->query['email'] != '') {
            $email = $this->request->query['email'];
            if (isset($email) && $email) {
                $conditions = array_merge($conditions, array('Contact.email  LIKE' => '%'.$email.'%'));
            }
        }

        if (isset($this->request->query['phone']) && $this->request->query['phone'] && $this->request->query['phone'] != '') {
            $phone = $this->request->query['phone'];
            if (isset($phone) && $phone) {
                $conditions = array_merge($conditions, array('Contact.phone  LIKE' => '%'.$phone.'%'));
            }
        }

        if (isset($this->request->query['rating']) && $this->request->query['rating'] && $this->request->query['rating'] != '') {
            $rating = $this->request->query['rating'];
            if (isset($rating) && $rating) {
                $conditions = array_merge($conditions, array('Contact.rating' => $rating));
            }
        }

        if (isset($this->request->query['source']) && $this->request->query['source'] && $this->request->query['source'] != '') {
            $source = $this->request->query['source'];
            if (isset($source) && $source) {
                $conditions = array_merge($conditions, array('Contact.source_id' => $source));
            }
        }

        if($slug)
        {
            if($slug == 'other'){
                $conditions = array_merge($conditions, array('Contact.source_id' => null));
                $title = 'List of Lead (Source: Others)';
            }
            elseif($slug == 'all'){
                $conditions = $conditions;
                $title = 'List of Lead (Source: All)';
            }
            elseif($slug == 'rating_one')
            {
                $conditions = array_merge($conditions, array('Contact.rating' => 1));
                $title = 'One star rating lead list';
            }
            elseif($slug == 'rating_two')
            {
                $conditions = array_merge($conditions, array('Contact.rating' => 2));
                $title = 'Two star rating lead list';
            }
            elseif($slug == 'rating_three')
            {
                $conditions = array_merge($conditions, array('Contact.rating' => 3));
                $title = 'Three star rating lead list';
            }
            elseif($slug == 'rating_four')
            {
                $conditions = array_merge($conditions, array('Contact.rating' => 4));
                $title = 'Four star rating lead list';
            }
            elseif($slug == 'rating_five')
            {
                $conditions = array_merge($conditions, array('Contact.rating' => 5));
                $title = 'Five star rating lead list';
            }
            elseif($slug == 'no_rating')
            {
                $noRatingConditions['OR'] = array(
                    array('Contact.rating' => array('', 0)),
                    array('Contact.rating' => NULL)
                );
                $conditions = array_merge($conditions, $noRatingConditions);
                $title = 'No rating lead list';
            }
            else{
                $conditions = array_merge($conditions, array('Source.slug' => $slug));
                $title = 'List of Lead (Source: '.$sourceName = $this->Source->getSourceBySlug($slug).')';
            }
        }
        $this->set('allContact', $this->Contact->countContact($this->userID));
        $this->set('studentContact', $this->Contact->countContact($this->userID, array('Contact.account_type' => 1)));
        $this->set('customerContact', $this->Contact->countContact($this->userID, array('Contact.account_type' => 2)));
        $this->set('leadContact', $this->Contact->countContact($this->userID, array('Contact.account_type' => 3)));
        //$searchTerm = $this->request->query['contact'];
        //$this->searchContact($searchTerm);


        $this->Contact->bindModel(
            array(
                'hasMany'=>
                    array(
                        'LastFollowupDate' => array(
                            'className' => 'Note',
                            'foreignKey' => 'contact_id',
                            'order' => 'LastFollowupDate.note_date DESC',
                        )
                )
            ),
            false
        );

        $this->Paginator->settings = array(
            'conditions' => $conditions,
            'fields' =>
                array(
                    'Contact.id',
                    'Contact.first_name',
                    'Contact.last_name',
                    'Contact.account_type',
                    'Contact.email',
                    'Contact.phone',
                    'Contact.status',
                    'Contact.member_type',
                    'Contact.created',
                    'Contact.rating',
                    'Source.name',
                    'CONCAT(Instructor.first_name, " ", Instructor.last_name) as instructorName',

                ),
            'limit' => $this->limit,
            'order' => 'Contact.id DESC',
            'group' => 'Contact.id',
            'contain' => array(
                'User',
                'Note' => array(
                    'fields' => array('Note.id', 'Note.note', 'Note.created'),
                    'limit' => 1,
                    'order' => 'Note.created DESC',
                    /*'conditions' => array('Note.created <' => date('Y-m-d H:i:s'))*/
                ),
                'LastFollowupDate' => array(
                    'fields' => array('LastFollowupDate.id', 'LastFollowupDate.note_date'),
                    'limit' => 1,
                    'order' => 'LastFollowupDate.note_date DESC',
                    'conditions' => array('LastFollowupDate.note_date <' => date('Y-m-d H:i:s'))
                )
            ),
            'joins' => array(
                array(
                    'table' => 'sources',
                    'type' => 'LEFT',
                    'alias' => 'Source',
                    'conditions' => array('Source.id = Contact.source_id')
                ),
                array(
                    'table' => 'profiles',
                    'type' => 'LEFT',
                    'alias' => 'Instructor',
                    'conditions' => array('Instructor.user_id = Contact.user_id')
                ),

            )
        );

        $sources = $this->Source->getSourceList($slug = true);
        $overview = $this->Contact->getContactOverviewBySource($sources, $this->userID);
        $ratingOverview = $this->Contact->getContactOverviewByRating($this->userID);
        $leads = $this->Paginator->paginate('Contact');

        $this->set('sources', $sources);
        $this->set('overviews', $overview);
        $this->set('ratingOverview', $ratingOverview);
        $this->set('contactList', $leads);
        $this->set('title', $title);
    }

    /**
     * @param $slug
     */

    /**
     * Adding new contact
     */
    protected function addContact()
    {
        $this->request->data['Contact']['user_id'] = $this->userID;

        if($this->request->data['Contact']['is_custom_source'] == 1 && $this->request->data['Contact']['custom_source']){
            $this->request->data['Contact']['source'] = $this->request->data['Contact']['custom_source'];
            $this->request->data['Contact']['source_id'] = null;
            $isCustomSource = true;
        }
        else{
            $isCustomSource = false;
            $sourceName = $this->Source->getSourceByID($this->request->data['Contact']['source']);
            $this->request->data['Contact']['source_id'] = $this->request->data['Contact']['source'];
            $this->request->data['Contact']['source'] = $sourceName;
        }

        $this->Contact->create();
        if ($this->Contact->save($this->request->data)) {
            $isCustomSource = false;
            $this->Session->write('isCustomSource', $isCustomSource);
            $this->Session->setFlash('New contact has been saved', 'flash_success');
            $this->redirect(array('controller' => 'contacts', 'action' => 'list'));
        } else {
            $this->Session->write('isCustomSource', $isCustomSource);
            $this->Session->setFlash('Sorry, something went wrong', 'flash_error');
        }
    }

    /**
     * @param $contactID
     * @throws NotFoundException
     *
     * Edit/Modify contact
     */
    protected function editContact($contactID)
    {
        $this->Contact->id = $contactID;
        if (!$this->Contact->exists()) {
            throw new NotFoundException('Invalid Contact');
        }

        $this->set('contactDetails', $this->Contact->findById($contactID));

        if ($this->request->is('post')) {
            $this->Contact->id = $contactID;

            if($this->request->data['Contact']['is_custom_source'] == 1 && $this->request->data['Contact']['custom_source']){
                $this->request->data['Contact']['source'] = $this->request->data['Contact']['custom_source'];
                $this->request->data['Contact']['source_id'] = null;
                $isCustomSource = true;
            }
            else{
                $isCustomSource = false;
                $sourceName = $this->Source->getSourceByID($this->request->data['Contact']['source']);
                $this->request->data['Contact']['source_id'] = $this->request->data['Contact']['source'];
                $this->request->data['Contact']['source'] = $sourceName;
            }


            if ($this->Contact->save($this->request->data)) {
                $isCustomSource = false;
                $this->Session->write('isCustomSource', $isCustomSource);
                $this->Session->setFlash('New contact has been updated', 'flash_success');
                $this->redirect(array('controller' => 'contacts', 'action' => 'list'));
            } else {
                $this->Session->write('isCustomSource', $isCustomSource);
                $this->Session->setFlash('Sorry, something went wrong', 'flash_error');
            }
        }
    }

    /**
     * @param $contactID
     * @throws NotFoundException
     */
    public function instructor_view($contactID)
    {
        $this->set('title_for_layout', 'Contact Details - '.$this->Utilities->applicationName);
        $this->Contact->id = $contactID;
        if (!$this->Contact->exists()) {
            throw new NotFoundException('Invalid Contact');
        }

        $this->set('contact', $this->Contact->getContactByID($contactID));
        $this->set('notes', $this->Note->getContactNotes($contactID));

        if ($this->request->is('post')) {
            $this->request->data['Note']['contact_id'] = $contactID;
            $this->request->data['Note']['created_by'] = $this->userID;
            $this->request->data['Note']['note_date'] = date('Y-m-d H:i:s', strtotime($this->request->data['Note']['note_date']));

            if ($this->Note->save($this->request->data)) {
                $this->Session->setFlash('Note has been saved', 'flash_success');
                $this->redirect(array('controller' => 'contacts', 'action' => 'view', $contactID));
            } else {
                $this->Session->setFlash('Sorry, something went wrong', 'flash_error');
            }

        }
    }

    /**
     * @param $contactID
     */
    public function instructor_restore($contactID)
    {
        if($this->restore($contactID)){
            $this->Session->setFlash('This contact restored successfully', 'flash_success');
        }
        else{
            $this->Session->setFlash('Sorry, something went wrong', 'flash_error');
        }
        $this->redirect($this->referer());
    }

    /**
     * @param $contactID
     * @throws NotFoundException
     *
     * Delete contact
     */
    protected function deleteContact($contactID)
    {
        $this->Contact->id = $contactID;
        if (!$this->Contact->exists()) {
            throw new NotFoundException('Invalid Contact');
        }
        $this->request->onlyAllow('post', 'delete');

        if ($this->Contact->saveField('status', 9)) {
            $this->Session->setFlash('Contact has been deleted!', 'flash_error');
        } else {
            $this->Session->setFlash('Sorry, something went wrong!', 'flash_error');
        }
        $this->redirect($this->referer());
    }

    /**
     * @param $contactID
     * @return bool
     */
    protected function restore($contactID)
    {
        $this->Contact->id = $contactID;
        if ($this->Contact->saveField('status', 1)) {
            return true;
        }
        return false;
    }

    /**
     * Import contact as a csv file.
     */
    protected function importContact()
    {
        $this->Contact->set($this->request->data);
        if ($this->Contact->importFormValidation()) {
            $document = $this->request->data['Contact']['file']['tmp_name'];
            $contactList = $this->Csv->import($document);
            $contacts = $this->arrangeContactList($contactList, $this->request->data['Contact']['account_type']);
            if (Cache::write('contactList', $contacts)) {
                $this->redirect(array('action' => 'preview'));
            }
        }
    }

    /**
     * Preview of imported contact.
     */
    protected function importPreview()
    {
        $this->set('title_for_layout', 'Preview of contacts - '.$this->Utilities->applicationName);
        $contacts = Cache::read('contactList');
        $this->set('contacts', $contacts);

        if ($contacts == null) {
            $this->redirect(array('action' => 'list'));
        }

        if ($this->request->is('post')) {
            if ($this->Contact->saveAll($contacts, array('validate' => false))) {
                $this->Session->setFlash('Your contact has been imported successfully', 'flash_success');
                Cache::write('contactList', null);
                $this->redirect(array('action' => 'list'));
            } else {
                $this->Session->setFlash('Sorry, something went wrong', 'flash_error');
            }
        }
    }

    /**
     * Export contact list
     */
    protected function exportContact()
    {
        if($this->userRole == 4){
            $this->userID = $instructorIDs = $this->TcsUser->getIntructorIdsForTC($this->userID);
        }

        $results = $this->Contact->find(
            'all',
            array(
                'conditions' => array('user_id' => $this->userID)
            )
        );

        if (!$results) {
            $this->Session->setFlash("Sorry, you don't have enough data to export", 'flash_error');
            $this->redirect($this->referer());
        }

        $excludePaths = array('Contact.id', 'Contact.user_id', 'Contact.modified');

        $customHeaders = array(
            'Contact.first_name' => 'FirstName',
            'Contact.last_name' => 'LastName',
            'Contact.email' => 'EmailAddress',
            'Contact.phone' => 'PhoneNumber',
            'Contact.account_type' => 'AccountType',
            'Contact.member_type' => 'MemberType',
            'Contact.status' => 'Status',
            'Contact.source' => 'Referral Source',
            'Contact.follow_up_date' => 'Follow Up Date',
            'Contact.created' => 'LastContactDate',
            'Contact.comments' => 'Comment'
        );

        $this->response->download('contact_list.csv');
        $this->CsvView->quickExport($results, $excludePaths, $customHeaders);
    }


    /**
     * @param $contactList
     * @param $type
     * @return array
     *
     * Manage the imported data into well format.
     */
    protected function arrangeContactList($contactList, $type)
    {
        foreach ($contactList as $contact) {
            if (isset($contact['Contact']['FirstName'])) {
                $firstName = $contact['Contact']['FirstName'];
            } else {
                $firstName = null;
            }

            if (isset($contact['Contact']['LastName'])) {
                $lastName = $contact['Contact']['LastName'];
            } else {
                $lastName = null;
            }

            if (isset($contact['Contact']['EmailAddress'])) {
                $email = $contact['Contact']['EmailAddress'];
            } elseif (isset($contact['Contact']['Username'])) {
                $email = $contact['Contact']['Username'];
            } else {
                $email = null;
            }

            if (isset($contact['Contact']['PhoneNumber'])) {
                $phone = $contact['Contact']['PhoneNumber'];
            } else {
                $phone = null;
            }

            if (isset($contact['Contact']['LastNote'])) {
                $note = $contact['Contact']['LastNote'];
            } else {
                $note = null;
            }

            if (isset($contact['Contact']['AccountType'])) {
                $accountType = $contact['Contact']['AccountType'];
            } else {
                if (!empty($type)) {
                    $accountType = $type;
                } else {
                    $accountType = 0;
                }
            }

            if (isset($contact['Contact']['MemberType'])) {
                $memberType = $contact['Contact']['MemberType'];
            } else {
                $memberType = 1;
            }

            if (isset($contact['Contact']['Status'])) {
                $status = $contact['Contact']['Status'];
            } else {
                $status = 1;
            }

            if (isset($contact['Contact']['CreatedDate'])) {
                $created = date('Y-m-d H:i:s', strtotime($contact['Contact']['CreatedDate']));
            } elseif (isset($contact['Contact']['LastContactDate'])) {
                $created = date('Y-m-d H:i:s', strtotime($contact['Contact']['LastContactDate']));
            } else {
                $created = null;
            }

            $contacts[]['Contact'] = array(
                'user_id' => $this->userID,
                'account_type' => $accountType,
                'member_type' => $memberType,
                'status' => $status,
                'first_name' => $firstName,
                'last_name' => $lastName,
                'email' => $email,
                'phone' => $phone,
                'note' => $note,
                'created' => $created,
            );


        }

        return $contacts;
    }

    /**
     * @param $contactList
     * @return array
     *
     * Manage data for export.
     */
    protected function manageExportDate($contactList)
    {
        foreach ($contactList as $contact) {

            $contacts[][] = array(
                'FirstName' => $contact['Contact']['first_name'],
                'LastName' => $contact['Contact']['last_name'],
                'EmailAddress' => $contact['Contact']['email'],
                'PhoneNumber' => $contact['Contact']['phone'],
                'CreatedDate' => $contact['Contact']['created']
            );
        }

        return $contacts;
    }

    /**
     * @param $searchTerm
     */
    protected function searchContact($searchTerm)
    {
        if($this->userRole == 4){
            $this->userID = $instructorIDs = $this->TcsUser->getIntructorIdsForTC($this->userID);
        }

        if ($searchTerm) {
            $this->Paginator->settings = array(
                'conditions' => array(
                    'Contact.user_id' => $this->userID,
                    'OR' => array(
                        'Contact.id' => $searchTerm,
                        'Contact.first_name LIKE' => '%' . $searchTerm . '%',
                        'Contact.last_name LIKE' => '%' . $searchTerm . '%',
                        'Contact.email LIKE' => '%' . $searchTerm . '%',
                        'Contact.phone LIKE' => '%' . $searchTerm . '%',
                    )
                ),
                'limit' => $this->limit,
                'order' => array('Contact.id' => 'DESC')
            );

            $this->set('contactList', $this->Paginator->paginate('Contact'));
        } else {
            $this->Session->setFlash("Sorry, you didn't search anything", 'flash_error');
        }
    }

    /**
     * @param $noteID
     * @param $status
     */
    public function instructor_followedNotes($noteID, $status)
    {
        $this->Note->id = $noteID;
        if($this->Note->saveField('status', $status)){
            if($status == 2){
                $this->Session->setFlash('Note has been followed up', 'flash_success');
            }
            elseif($status == 3){
                $this->Session->setFlash('Note has been unfollowed up', 'flash_error');
            }
        }
        $this->redirect($this->referer());
    }

    /**
     *
     */
    public function instructor_setRating()
    {
        $this->autoLayout = false;
        $this->autoRender = false;
        $this->response->type('application/javascript');

        $noteID = (int) $this->request->data['noteID'];
        $rating = (int) $this->request->data['rating'];

        $this->Note->id = $noteID;
        if($this->Note->saveField('rating', $rating)){
            $this->response->body(json_encode(1));
        }
        else{
            $this->response->body(json_encode(0));
        }
    }

    /**
     *
     */
    private function setRating()
    {
        $this->autoLayout = false;
        $this->autoRender = false;
        $this->response->type('application/javascript');

        $contactID = (int) $this->request->data['contactID'];
        $rating = (int) $this->request->data['rating'];

        $this->Contact->id = $contactID;
        if($this->Contact->saveField('rating', $rating))
        {
            $this->response->body(json_encode(1));
        }
        else{
            $this->response->body(json_encode(0));
        }
    }
}
