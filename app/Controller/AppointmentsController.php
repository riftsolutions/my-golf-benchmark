<?php
App::uses('AppController', 'Controller');
/**
 * Class AppointmentsController
 *
 * @property Appointment $Appointment..
 * @property Attachment @Attachment
 * @property Benchmark $Benchmark
 * @property Billing $Billing
 * @property BillingsPackage @BillingsPackage
 * @property BlockTime @BlockTime
 * @property Card $Card
 * @property Config $Config
 * @property Contact $Contact
 * @property CountLesson $CountLesson
 * @property EmailsEvent $EmailsEvent
 * @property EmailsEventsSetting $EmailsEventsSetting
 * @property Event $Event
 * @property GroupLesson $GroupLesson
 * @property GroupLessonsStudents $GroupLessonsStudents
 * @property Note @Note
 * @property Package $Package
 * @property PackagesUser $PackagesUser
 * @property PackagesOrder $PackagesOrder
 * @property Profile $Profile
 * @property Source $Source
 * @property TcsUser $TcsUser
 * @property User $User
 * @property Waiting $Waiting
 *
 * @property PaginatorComponent $Paginator
 * @property EmailComponent $Email
 * @property UtilitiesComponent $Utilities
 * @property AuthorizeNetComponent $AuthorizeNet
 * @property DataTableComponent $DataTable
 * @property ImgComponent $Img
 * @property AppsComponent $Apps
 *
 */

class AppointmentsController extends AppController
{
    /**
     * The name of this controller.
     *
     * @var string
     */
    public $name = 'Appointments';

    /**************************************************************************************************************/
    /********************************************        Admin Panel          *************************************/
    /**************************************************************************************************************/

    /**
     *
     */
    public function admin_index()
    {
        $this->layout = 'admin';
    }

    /**************************************************************************************************************/
    /******************************************       Instructor Panel         ************************************/
    /**************************************************************************************************************/

    /**
     * Instructor index page of appointment
     */
    public function instructor_index()
    {
        $this->redirect(array('controller' => 'appointments', 'action' => 'list'));
    }

    /**
     * @param null $sortBy
     *
     * Appointment list page of instructor
     */
    public function instructor_list($sortBy = null)
    {
        $this->set('title_for_layout', 'My Schedule List - '.$this->Utilities->applicationName);
        $conditions = array('Appointment.instructor_id' => $this->userID);
        $title  = 'My Appointment List';
        $sid = null;

        if(isset($this->request->query['status']) && $this->request->query['status'] && $this->request->query['status'] != '')
        {
            $sortBy = $this->request->query['status'];
        }

        if($sortBy == 'pending'){
            $conditions = array_merge($conditions, array('Appointment.status' => 1));
            $title  = 'Pending Appointment List';
        }
        elseif($sortBy == 'upcoming'){
            $conditions = array_merge($conditions, array('Appointment.status' => 2));
            $title  = 'Upcoming Appointment List';
        }
        elseif($sortBy == 'cancelled'){
            $conditions = array_merge($conditions, array('Appointment.status' => 3));
            $title  = 'Cancelled Appointment List';
        }
        elseif($sortBy == 'completed'){
            $conditions = array_merge($conditions, array('Appointment.status' => 4));
            $title  = 'Complete Appointment List';
        }
        elseif($sortBy == 'expired'){
            $conditions = array_merge($conditions, array('Appointment.status' => 5));
            $title  = 'Expired Appointment List';
        }
        elseif($sortBy == 'reschedule'){
            $conditions = array_merge($conditions, array('Appointment.is_reschedule' => 1));
            $title  = 'Rescheduled Appointment List';
        }


        if(isset($this->request->query['sid']) && $this->request->query['sid'] && $this->request->query['sid'] != '')
        {
            $student = $this->User->getUserBasicUuid($this->request->query['sid']);
            $conditions = array_merge($conditions, array('Appointment.appointment_by' => $student['User']['id']));
            $title = 'Appointment list of '.$student['Profile']['name'].' ('. $student['User']['username'].')';
            $sid = $this->request->query['sid'];
        }


        if(isset($this->request->query['student']) && $this->request->query['student'] && $this->request->query['student'] != '')
        {
            $studentInfo = explode('-', $this->request->query['student']);
            $studentID = $this->User->getUserByEMail(trim($studentInfo[1]));
            if($studentID){
                $conditions = array_merge($conditions, array('Appointment.appointment_by' => $studentID));
            }
            $search = true;
        }


        if(isset($this->request->query['package']) && $this->request->query['package'] && $this->request->query['package'] != '')
        {
            $packageName = $this->request->query['package'];
            $packageID = $this->Package->getPackageIDByName($packageName);
            if($packageID)
            {
                $conditions = array_merge($conditions, array('Appointment.package_id' => $packageID));
            }
            $search = true;
        }


        if(isset($this->request->query['appointment_starting_date']) && $this->request->query['appointment_starting_date'] && $this->request->query['appointment_starting_date'] != '')
        {
            $startingDate = $this->request->query['appointment_starting_date'];
            $conditions = array_merge($conditions, array(
                    'YEAR(Appointment.start) >=' => date('Y', strtotime($startingDate)),
                    'MONTH(Appointment.start) >=' => date('m', strtotime($startingDate)),
                    'DAY(Appointment.start) >=' => date('d', strtotime($startingDate)),
                )
            );
            $search = true;
        }

        if(isset($this->request->query['appointment_ending_date']) && $this->request->query['appointment_ending_date'] && $this->request->query['appointment_ending_date'] != '')
        {
            $endingDate = $this->request->query['appointment_ending_date'];
            $conditions = array_merge($conditions, array(
                    'YEAR(Appointment.start) <=' => date('Y', strtotime($endingDate)),
                    'MONTH(Appointment.start) <=' => date('m', strtotime($endingDate)),
                    'DAY(Appointment.start) <=' => date('d', strtotime($endingDate)),
                )
            );
            $search = true;
        }

        if(isset($this->request->query['appointment_starting_time']) && $this->request->query['appointment_starting_time'] && $this->request->query['appointment_starting_time'] != '')
        {
            $startingTime = $this->request->query['appointment_starting_time'];
            $conditions = array_merge($conditions, array(
                    'HOUR(Appointment.start) >=' => date('H', strtotime($startingTime)),
                    'MINUTE(Appointment.start) >=' => date('i', strtotime($startingTime)),
                )
            );
            $search = true;
        }

        if(isset($this->request->query['appointment_ending_time']) && $this->request->query['appointment_ending_time'] && $this->request->query['appointment_ending_time'] != '')
        {
            $endingTime = $this->request->query['appointment_ending_time'];
            $conditions = array_merge($conditions, array(
                    'HOUR(Appointment.start) <=' => date('H', strtotime($endingTime)),
                    'MINUTE(Appointment.start) <=' => date('i', strtotime($endingTime)),
                )
            );
            $search = true;
        }

        $this->Paginator->settings = array(
            'conditions' => $conditions,
            'recursive' => -1,
            'limit' => $this->limit,
            'fields' => array(
                'Appointment.id',
                'Appointment.uuid',
                'Appointment.start',
                'Appointment.end',
                'Appointment.lesson_no',
                'Appointment.package_id',
                'Appointment.is_benchmarked',
                'Appointment.status',
                'Appointment.is_reschedule',
                'Package.name',
                'Package.uuid',
                'Profile.user_id',
                'Profile.first_name',
                'Profile.last_name',
                'Profile.phone',
                'Profile.city',
                'Profile.state',
                'Profile.profile_pic_url',
                'User.username',
                'Benchmark.uuid',
            ),
            'joins' => array(
                array(
                    'table' => 'packages',
                    'type' => 'LEFT',
                    'alias' => 'Package',
                    'conditions' => array('Package.id = Appointment.package_id')
                ),
                array(
                    'table' => 'profiles',
                    'type' => 'LEFT',
                    'alias' => 'Profile',
                    'conditions' => array('Profile.user_id = Appointment.appointment_by')
                ),
                array(
                    'table' => 'users',
                    'type' => 'LEFT',
                    'alias' => 'User',
                    'conditions' => array('User.id = Appointment.appointment_by')
                ),
                array(
                    'table' => 'benchmarks',
                    'type' => 'LEFT',
                    'alias' => 'Benchmark',
                    'conditions' => array('Benchmark.appointment_id = Appointment.id')
                ),
            ),
            'order' => array('Appointment.id' => 'DESC')
        );

        if(isset($search) && $search)
        {
            $title = 'Search Result';
        }

        $appointment = $this->Paginator->paginate('Appointment');
        $this->set('appointments',  $appointment);
        $this->set('title',  $title);
        $this->set('sid',  $sid);

        $appointmentOverview = $this->Appointment->getAppointmentOverview(array('conditions' => array('Appointment.instructor_id' => $this->userID)));
        $this->set('appointmentOverview', $appointmentOverview);
    }
    /**
     * Waiting list of appointment
     */
    public function instructor_waiting($sortBY = null)
    {
        $this->set('title_for_layout', 'My Waiting List - '.$this->Utilities->applicationName);
        $this->set('title', 'My Waiting List');
        $this->set('notFount', 'Sorry, Waiting list empty');
        $sid = null;

        $conditions = array('Waiting.instructor_id' => $this->userID);
        if($sortBY == 'waiting'){
            $conditions = array_merge($conditions, array('Waiting.status' => 1));
        }
        if($sortBY == 'expired'){
            $conditions = array_merge($conditions, array('Waiting.status' => 4));
            $this->set('title', 'Expired Waiting List');
        }
        if($sortBY == 'cancelled'){
            $conditions = array_merge($conditions, array('Waiting.status' => 5));
            $this->set('title', 'Cancelled Waiting List');
        }
        if($sortBY == 'notification'){
            $conditions = array_merge($conditions, array('Waiting.status' => 2));
            $this->set('title', 'My Notifications');
            $this->set('notFount', 'Sorry, there is no new notification');
        }
        if($sortBY == 'confirm'){
            $conditions = array_merge($conditions, array('Waiting.status' => 3));
        }

        if(isset($this->request->query['sid']) && $this->request->query['sid'] && $this->request->query['sid'] != '')
        {
            $student = $this->User->getUserBasicUuid($this->request->query['sid']);
            $conditions = array_merge($conditions, array('Waiting.appointment_by' => $student['User']['id']));
            $this->set('title', 'Waiting list of '.$student['Profile']['name'].' ('. $student['User']['username'].')');
            $sid = $this->request->query['sid'];
        }

        if(isset($this->request->query['student']) && $this->request->query['student'] && $this->request->query['student'] != '')
        {
            $studentInfo = explode('-', $this->request->query['student']);
            $studentID = $this->User->getUserByEMail(trim($studentInfo[1]));
            if($studentID){
                $conditions = array_merge($conditions, array('Waiting.appointment_by' => $studentID));
            }
            $search = true;
        }


        if(isset($this->request->query['package']) && $this->request->query['package'] && $this->request->query['package'] != '')
        {
            $packageName = $this->request->query['package'];
            $packageID = $this->Package->getPackageIDByName($packageName);
            if($packageID)
            {
                $conditions = array_merge($conditions, array('Waiting.package_id' => $packageID));
            }
            $search = true;
        }


        if(isset($this->request->query['waiting_starting_date']) && $this->request->query['waiting_starting_date'] && $this->request->query['waiting_starting_date'] != '')
        {
            $startingDate = $this->request->query['waiting_starting_date'];
            $conditions = array_merge($conditions, array(
                    'YEAR(Waiting.start) >=' => date('Y', strtotime($startingDate)),
                    'MONTH(Waiting.start) >=' => date('m', strtotime($startingDate)),
                    'DAY(Waiting.start) >=' => date('d', strtotime($startingDate)),
                )
            );
            $search = true;
        }

        if(isset($this->request->query['waiting_ending_date']) && $this->request->query['waiting_ending_date'] && $this->request->query['waiting_ending_date'] != '')
        {
            $endingDate = $this->request->query['waiting_ending_date'];
            $conditions = array_merge($conditions, array(
                    'YEAR(Waiting.start) <=' => date('Y', strtotime($endingDate)),
                    'MONTH(Waiting.start) <=' => date('m', strtotime($endingDate)),
                    'DAY(Waiting.start) <=' => date('d', strtotime($endingDate)),
                )
            );
            $search = true;
        }

        if(isset($this->request->query['waiting_starting_time']) && $this->request->query['waiting_starting_time'] && $this->request->query['waiting_starting_time'] != '')
        {
            $startingTime = $this->request->query['waiting_starting_time'];
            $conditions = array_merge($conditions, array(
                    'HOUR(Waiting.start) >=' => date('H', strtotime($startingTime)),
                    'MINUTE(Waiting.start) >=' => date('i', strtotime($startingTime)),
                )
            );
            $search = true;
        }

        if(isset($this->request->query['waiting_ending_time']) && $this->request->query['waiting_ending_time'] && $this->request->query['waiting_ending_time'] != '')
        {
            $endingTime = $this->request->query['waiting_ending_time'];
            $conditions = array_merge($conditions, array(
                    'HOUR(Waiting.start) <=' => date('H', strtotime($endingTime)),
                    'MINUTE(Waiting.start) <=' => date('i', strtotime($endingTime)),
                )
            );
            $search = true;
        }

        $this->Paginator->settings = array(
            'conditions' => $conditions,
            'fields' => array(
                'Waiting.id',
                'Waiting.uuid',
                'Waiting.appointment_id',
                'Waiting.instructor_id',
                'Waiting.lesson_no',
                'Waiting.start',
                'Waiting.status',
                'Waiting.end',
                'Package.name',
                'Package.uuid',
                'Instructor.user_id',
                'Instructor.first_name',
                'Instructor.last_name',
                'InstructorUser.username',
                'InstructorUser.uuid',
            ),
            'limit' => $this->limit,
            'joins' => array(
                array(
                    'table' => 'packages',
                    'type' => 'LEFT',
                    'alias' => 'Package',
                    'conditions' => array('Package.id = Waiting.package_id')
                ),
                array(
                    'table' => 'profiles',
                    'type' => 'LEFT',
                    'alias' => 'Instructor',
                    'conditions' => array('Instructor.user_id = Waiting.appointment_by')
                ),
                array(
                    'table' => 'users',
                    'type' => 'LEFT',
                    'alias' => 'InstructorUser',
                    'conditions' => array('InstructorUser.id = Waiting.appointment_by')
                ),
            ),
            'order' => array('Waiting.id' => 'DESC')
        );

        $appointmentList = $this->Paginator->paginate('Waiting');


        $this->set('appointments',  $appointmentList);
        $this->set('sid', $sid);
    }

    /**
     * @param null $sortBy
     *
     * Appointment list page of TC
     */
    public function tc_list($sortBy = null)
    {
        $this->set('title_for_layout', 'Schedule List - '.$this->Utilities->applicationName);
        $instructorIDs = array_keys($this->TcsUser->getInstructorIdsByTCID($this->userID) ?: array());

        $conditions = array('Appointment.instructor_id' => $instructorIDs);
        $title  = 'My Appointment List';
        $sid = null;

        if(isset($this->request->query['status']) && $this->request->query['status'] && $this->request->query['status'] != '')
        {
            $sortBy = $this->request->query['status'];
        }

        if($sortBy == 'pending'){
            $conditions = array_merge($conditions, array('Appointment.status' => 1));
            $title  = 'Pending Appointment List';
        }
        elseif($sortBy == 'upcoming'){
            $conditions = array_merge($conditions, array('Appointment.status' => 2));
            $title  = 'Upcoming Appointment List';
        }
        elseif($sortBy == 'cancelled'){
            $conditions = array_merge($conditions, array('Appointment.status' => 3));
            $title  = 'Cancelled Appointment List';
        }
        elseif($sortBy == 'completed'){
            $conditions = array_merge($conditions, array('Appointment.status' => 4));
            $title  = 'Complete Appointment List';
        }
        elseif($sortBy == 'expired'){
            $conditions = array_merge($conditions, array('Appointment.status' => 5));
            $title  = 'Expired Appointment List';
        }
        elseif($sortBy == 'reschedule'){
            $conditions = array_merge($conditions, array('Appointment.is_reschedule' => 1));
            $title  = 'Rescheduled Appointment List';
        }


        if(isset($this->request->query['sid']) && $this->request->query['sid'] && $this->request->query['sid'] != '')
        {
            $student = $this->User->getUserBasicUuid($this->request->query['sid']);
            $conditions = array_merge($conditions, array('Appointment.appointment_by' => $student['User']['id']));
            $title = 'Appointment list of '.$student['Profile']['name'].' ('. $student['User']['username'].')';
            $sid = $this->request->query['sid'];
        }



        if (isset($this->request->query['instructor']) && $this->request->query['instructor']) {
            $instructorID = $this->request->query['instructor'];
            if($instructorID[0] == ''){
                unset($instructorID[0]);
            }
            if (!empty($instructorID)) {
                $conditions = array_merge($conditions, array('Appointment.instructor_id' => $instructorID));
            }
        }

        if(isset($this->request->query['student']) && $this->request->query['student'] && $this->request->query['student'] != '')
        {
            $studentInfo = explode('-', $this->request->query['student']);
            $studentID = $this->User->getUserByEMail(trim($studentInfo[1]));
            if($studentID){
                $conditions = array_merge($conditions, array('Appointment.appointment_by' => $studentID));
            }
            $search = true;
        }


        if(isset($this->request->query['package']) && $this->request->query['package'] && $this->request->query['package'] != '')
        {
            $packageName = $this->request->query['package'];
            $packageID = $this->Package->getPackageIDByName($packageName);
            if($packageID)
            {
                $conditions = array_merge($conditions, array('Appointment.package_id' => $packageID));
            }
            $search = true;
        }


        if(isset($this->request->query['appointment_starting_date']) && $this->request->query['appointment_starting_date'] && $this->request->query['appointment_starting_date'] != '')
        {
            $startingDate = $this->request->query['appointment_starting_date'];
            $conditions = array_merge($conditions, array(
                    'YEAR(Appointment.start) >=' => date('Y', strtotime($startingDate)),
                    'MONTH(Appointment.start) >=' => date('m', strtotime($startingDate)),
                    'DAY(Appointment.start) >=' => date('d', strtotime($startingDate)),
                )
            );
            $search = true;
        }

        if(isset($this->request->query['appointment_ending_date']) && $this->request->query['appointment_ending_date'] && $this->request->query['appointment_ending_date'] != '')
        {
            $endingDate = $this->request->query['appointment_ending_date'];
            $conditions = array_merge($conditions, array(
                    'YEAR(Appointment.start) <=' => date('Y', strtotime($endingDate)),
                    'MONTH(Appointment.start) <=' => date('m', strtotime($endingDate)),
                    'DAY(Appointment.start) <=' => date('d', strtotime($endingDate)),
                )
            );
            $search = true;
        }

        if(isset($this->request->query['appointment_starting_time']) && $this->request->query['appointment_starting_time'] && $this->request->query['appointment_starting_time'] != '')
        {
            $startingTime = $this->request->query['appointment_starting_time'];
            $conditions = array_merge($conditions, array(
                    'HOUR(Appointment.start) >=' => date('H', strtotime($startingTime)),
                    'MINUTE(Appointment.start) >=' => date('i', strtotime($startingTime)),
                )
            );
            $search = true;
        }

        if(isset($this->request->query['appointment_ending_time']) && $this->request->query['appointment_ending_time'] && $this->request->query['appointment_ending_time'] != '')
        {
            $endingTime = $this->request->query['appointment_ending_time'];
            $conditions = array_merge($conditions, array(
                    'HOUR(Appointment.start) <=' => date('H', strtotime($endingTime)),
                    'MINUTE(Appointment.start) <=' => date('i', strtotime($endingTime)),
                )
            );
            $search = true;
        }

        $this->Paginator->settings = array(
            'conditions' => $conditions,
            'recursive' => -1,
            'limit' => $this->limit,
            'fields' => array(
                'Appointment.id',
                'Appointment.uuid',
                'Appointment.start',
                'Appointment.end',
                'Appointment.lesson_no',
                'Appointment.package_id',
                'Appointment.is_benchmarked',
                'Appointment.status',
                'Appointment.is_reschedule',
                'Package.name',
                'Package.uuid',
                'Profile.user_id',
                'Profile.first_name',
                'Profile.last_name',
                'Profile.phone',
                'Profile.city',
                'Profile.state',
                'Profile.profile_pic_url',
                'User.username',
                'Benchmark.uuid',
                'InstructorUser.uuid',
                'InstructorUser.username',
                'CONCAT(Instructor.first_name, " ", Instructor.last_name) as instructorName',


            ),
            'joins' => array(
                array(
                    'table' => 'packages',
                    'type' => 'LEFT',
                    'alias' => 'Package',
                    'conditions' => array('Package.id = Appointment.package_id')
                ),
                array(
                    'table' => 'profiles',
                    'type' => 'LEFT',
                    'alias' => 'Profile',
                    'conditions' => array('Profile.user_id = Appointment.appointment_by')
                ),
                array(
                    'table' => 'users',
                    'type' => 'LEFT',
                    'alias' => 'User',
                    'conditions' => array('User.id = Appointment.appointment_by')
                ),
                array(
                    'table' => 'profiles',
                    'type' => 'LEFT',
                    'alias' => 'Instructor',
                    'conditions' => array('Instructor.user_id = Appointment.instructor_id')
                ),
                array(
                    'table' => 'users',
                    'type' => 'LEFT',
                    'alias' => 'InstructorUser',
                    'conditions' => array('InstructorUser.id = Appointment.instructor_id')
                ),

                array(
                    'table' => 'benchmarks',
                    'type' => 'LEFT',
                    'alias' => 'Benchmark',
                    'conditions' => array('Benchmark.appointment_id = Appointment.id')
                ),
            ),
            'order' => array('Appointment.id' => 'DESC')
        );

        if(isset($search) && $search)
        {
            $title = 'Search Result';
        }

        $appointment = $this->Paginator->paginate('Appointment');
        $this->set('appointments',  $appointment);
        $this->set('title',  $title);
        $this->set('sid',  $sid);

        $appointmentOverview = $this->Appointment->getAppointmentOverview(array('conditions' => array('Appointment.instructor_id' => $instructorIDs)));
        $this->set('appointmentOverview', $appointmentOverview);

        $getTcInstructors = $this->TcsUser->getInstructorIdsByTCID($this->userID);
        $this->set('instructors', $getTcInstructors);
    }

    /**
     * @param null $id
     * Appointment view page of TC
     */
    public function tc_view($id = null)
    {
        $this->set('title_for_layout', 'Appointment Details - '.$this->Utilities->applicationName);
        $appointmentDetails = $this->Appointment->find(
            'first',
            array(
                'conditions' => array(
                    'Appointment.id' => $id
                ),
                'recursive' => -1,
                'fields' => array(
                    'Appointment.id',
                    'Appointment.uuid',
                    'Appointment.start',
                    'Appointment.end',
                    'Appointment.lesson_no',
                    'Appointment.package_id',
                    'Appointment.is_benchmarked',
                    'Appointment.status',
                    'Package.name',
                    'Package.uuid',
                    'Profile.user_id',
                    'Profile.first_name',
                    'Profile.last_name',
                    'Profile.phone',
                    'Profile.city',
                    'Profile.state',
                    'Profile.postal_code',
                    'Profile.profile_pic_url',
                    'User.username',
                    'User.uuid',
                    'Benchmark.uuid',
                ),
                'joins' => array(
                    array(
                        'table' => 'packages',
                        'type' => 'LEFT',
                        'alias' => 'Package',
                        'conditions' => array('Package.id = Appointment.package_id')
                    ),
                    array(
                        'table' => 'profiles',
                        'type' => 'LEFT',
                        'alias' => 'Profile',
                        'conditions' => array('Profile.user_id = Appointment.appointment_by')
                    ),
                    array(
                        'table' => 'users',
                        'type' => 'LEFT',
                        'alias' => 'User',
                        'conditions' => array('User.id = Appointment.appointment_by')
                    ),
                    array(
                        'table' => 'benchmarks',
                        'type' => 'LEFT',
                        'alias' => 'Benchmark',
                        'conditions' => array('Benchmark.appointment_id = Appointment.id')
                    ),
                ),
            )
        );
        if(empty($appointmentDetails))
        {
            throw new BadRequestException;
        }
        $this->set('appointmentDetails', $appointmentDetails);
        $this->set('waitingList', $this->Waiting->getWaitingList(array('conditions' => array('Waiting.appointment_id' => $id), 'limit' => false)));
    }

    /**
     * @param $waitingUUID
     */
    public function student_notification_details($waitingUUID)
    {
        $id =  $this->Waiting->getWaitingIDByUuid($waitingUUID);
        $waiting =  $this->Waiting->getWaitingByID($id);
        $this->set('title_for_layout', 'Waiting Details - '.$this->Utilities->applicationName);
        $this->set('waiting', $waiting);
    }

    /**
     * @param $waitingUUID
     */
    public function student_confirmAppointment($waitingUUID)
    {
        $id =  $this->Waiting->getWaitingIDByUuid($waitingUUID);
        if($this->confirmAppointment($id)){
            $this->Session->setFlash('Appointment has been confirmed', 'flash_success');
            $this->redirect(array('controller' =>'appointments', 'action' => 'list', 'upcoming'));
        }
        else{
            $this->Session->setFlash('Sorry, something went wrong', 'flash_success');
            $this->redirect($this->referer());
        }
    }

    /**
     * @param null $id
     * @throws BadRequestException
     */
    public function instructor_view($id = null)
    {
        $this->set('title_for_layout', 'Appointment Details - '.$this->Utilities->applicationName);
        $appointmentDetails = $this->Appointment->find(
            'first',
            array(
                'conditions' => array(
                    'Appointment.id' => $id
                ),
                'recursive' => -1,
                'fields' => array(
                    'Appointment.id',
                    'Appointment.uuid',
                    'Appointment.start',
                    'Appointment.end',
                    'Appointment.lesson_no',
                    'Appointment.package_id',
                    'Appointment.is_benchmarked',
                    'Appointment.status',
                    'Package.name',
                    'Package.uuid',
                    'Profile.user_id',
                    'Profile.first_name',
                    'Profile.last_name',
                    'Profile.phone',
                    'Profile.city',
                    'Profile.state',
                    'Profile.postal_code',
                    'Profile.profile_pic_url',
                    'User.username',
                    'User.uuid',
                    'Benchmark.uuid',
                ),
                'joins' => array(
                    array(
                        'table' => 'packages',
                        'type' => 'LEFT',
                        'alias' => 'Package',
                        'conditions' => array('Package.id = Appointment.package_id')
                    ),
                    array(
                        'table' => 'profiles',
                        'type' => 'LEFT',
                        'alias' => 'Profile',
                        'conditions' => array('Profile.user_id = Appointment.appointment_by')
                    ),
                    array(
                        'table' => 'users',
                        'type' => 'LEFT',
                        'alias' => 'User',
                        'conditions' => array('User.id = Appointment.appointment_by')
                    ),
                    array(
                        'table' => 'benchmarks',
                        'type' => 'LEFT',
                        'alias' => 'Benchmark',
                        'conditions' => array('Benchmark.appointment_id = Appointment.id')
                    ),
                ),
            )
        );
        if(empty($appointmentDetails))
        {
            throw new BadRequestException;
        }
        $this->set('appointmentDetails', $appointmentDetails);
        $this->set('waitingList', $this->Waiting->getWaitingList(array('conditions' => array('Waiting.appointment_id' => $id), 'limit' => false)));
    }

    /**
     * @param $appointmentID
     * @throws NotFoundException
     */
    public function instructor_complete($appointmentID)
    {
        $this->Appointment->id = $appointmentID;
        if (!$this->Appointment->exists()) {
            throw new NotFoundException('Invalid Appointment');
        }
        $this->request->onlyAllow('post', 'delete');
        $this->Appointment->id = $appointmentID;
        if ($this->Appointment->saveField('status', 2)) {
            $this->Session->setFlash('Appointment has been completed', 'flash_success');
        }
        else{
            $this->Session->setFlash('Sorry, something went wrong!', 'flash_error');
        }
        $this->redirect($this->referer());
    }

    /**
     * @param $appointmentID
     * @param $status
     */
    public function instructor_appointmentActions($appointmentID, $status)
    {
        if($this->request->is('post')){
            $this->Appointment->id = $appointmentID;
            if($this->Appointment->saveField('status', $status)){
                $this->Appointment->saveField('operated_by', $this->userID);
                if($status == 2){
                    $this->Session->setFlash("Appointment has been confirmed", 'flash_success');
                    $this->sendEmailNotification('when_lesson_confirm', array('appointmentID' => $appointmentID));
                }
                elseif($status == 3){
                    $this->notifiedToWaitingList($appointmentID);
                    $this->Session->setFlash("Appointment has been cancelled", 'flash_success');
                    $this->sendEmailNotification('when_lesson_cancelled', array('appointmentID' => $appointmentID));
                }
                elseif($status == 4){
                    $this->Session->setFlash("Appointment has been completed", 'flash_success');
                }
            }
            else{
                $this->Session->setFlash("Sorry, something went wrong", 'flash_error');
            }
            $this->redirect($this->referer());
        }
    }

    /**
     * @param $appointmentID
     * @param $status
     */
    public function student_appointmentActions($appointmentID, $status)
    {
        if($this->request->is('post')){
            $this->Appointment->id = $appointmentID;
            if($this->Appointment->saveField('status', $status)){
                $this->Appointment->saveField('operated_by', $this->userID);
                if($status == 2){
                    $this->Session->setFlash("Appointment has been confirmed", 'flash_success');
                    $this->sendEmailNotification('when_lesson_confirm', array('appointmentID' => $appointmentID));
                }
                elseif($status == 3){
                    $this->notifiedToWaitingList($appointmentID);
                    $this->Session->setFlash("Appointment has been cancelled", 'flash_success');
                    $this->sendEmailNotification('when_lesson_cancelled', array('appointmentID' => $appointmentID));
                }
                elseif($status == 4){
                    $this->Session->setFlash("Appointment has been completed", 'flash_success');
                }
            }
            else{
                $this->Session->setFlash("Sorry, something went wrong", 'flash_error');
            }
            $this->redirect($this->referer());
        }
    }


    /**************************************************************************************************************/
    /******************************************         Student Panel          ************************************/
    /**************************************************************************************************************/

    /**
     *
     */
    public function student_index()
    {
        $this->redirect(array('controller' => 'appointments', 'action' => 'list'));
    }

    /**
     * @param null $sortBy
     */
    public function student_list($sortBy = null)
    {
        $this->set('title_for_layout', 'My Schedule List - '.$this->Utilities->applicationName);
        $title  = 'My Appointment List';
        $conditions = array('Appointment.appointment_by' => $this->userID);

        if($sortBy == 'pending'){
            $conditions = array_merge($conditions, array('Appointment.status' => 1));
            $title  = 'Pending Appointment List';
        }
        elseif($sortBy == 'upcoming'){
            $conditions = array_merge($conditions, array('Appointment.status' => 2));
            $title  = 'Upcoming Appointment List';
        }
        elseif($sortBy == 'cancelled'){
            $conditions = array_merge($conditions, array('Appointment.status' => 3));
            $title  = 'Cancelled Appointment List';
        }
        elseif($sortBy == 'completed'){
            $conditions = array_merge($conditions, array('Appointment.status' => 4));
            $title  = 'Completed Appointment List';
        }
        elseif($sortBy == 'expired'){
            $conditions = array_merge($conditions, array('Appointment.status' => 5));
            $title  = 'Expired Appointment List';
        }
        elseif($sortBy == 'reschedule'){
            $conditions = array_merge($conditions, array('Appointment.is_reschedule' => 1));
            $title  = 'Rescheduled Appointment List';
        }


        $this->Paginator->settings = array(
            'conditions' => $conditions,
            'recursive' => -1,
            'limit' => $this->limit,
            'fields' => array(
                'Appointment.id',
                'Appointment.uuid',
                'Appointment.start',
                'Appointment.end',
                'Appointment.lesson_no',
                'Appointment.package_id',
                'Appointment.is_benchmarked',
                'Appointment.is_reschedule',
                'Appointment.status',
                'Package.name',
                'Package.uuid',
                'Instructor.user_id',
                'Instructor.first_name',
                'Instructor.last_name',
                'InstructorUser.username',
                'InstructorUser.uuid',
                'Benchmark.uuid',
            ),
            'joins' => array(
                array(
                    'table' => 'packages',
                    'type' => 'LEFT',
                    'alias' => 'Package',
                    'conditions' => array('Package.id = Appointment.package_id')
                ),
                array(
                    'table' => 'profiles',
                    'type' => 'LEFT',
                    'alias' => 'Instructor',
                    'conditions' => array('Instructor.user_id = Appointment.instructor_id')
                ),
                array(
                    'table' => 'users',
                    'type' => 'LEFT',
                    'alias' => 'InstructorUser',
                    'conditions' => array('InstructorUser.id = Appointment.instructor_id')
                ),
                array(
                    'table' => 'benchmarks',
                    'type' => 'LEFT',
                    'alias' => 'Benchmark',
                    'conditions' => array('Benchmark.appointment_id = Appointment.id')
                ),
            ),
            'order' => array('Appointment.id' => 'DESC')
        );
        $this->set('appointments',  $this->Paginator->paginate('Appointment'));

        $this->set('appointmentOverview', $this->Appointment->getAppointmentOverview(array('conditions' => array('Appointment.appointment_by' => $this->userID))));
        $this->set('title', $title);
    }

    /**
     * @param null $sortBY
     */
    public function student_waiting($sortBY = null)
    {
        $this->set('title_for_layout', 'My Waiting List - '.$this->Utilities->applicationName);
        $this->set('title', 'My Waiting List');
        $this->set('notFount', 'Sorry, Waiting list empty');

        $conditions = array('Waiting.appointment_by' => $this->userID);
        if($sortBY == 'waiting'){
            $conditions = array_merge($conditions, array('Waiting.status' => 1, 'Waiting.status !=' => 2));
        }
        if($sortBY == 'expired'){
            $conditions = array_merge($conditions, array('Waiting.status' => 4, 'Waiting.status !=' => 2));
            $this->set('title', 'Expired Waiting List');
        }
        if($sortBY == 'cancelled'){
            $conditions = array_merge($conditions, array('Waiting.status' => 5, 'Waiting.status !=' => 2));
            $this->set('title', 'Cancelled Waiting List');
        }
        if($sortBY == 'notification'){
            $conditions = array_merge($conditions, array('Waiting.status' => 2));
            $this->set('title', 'My Notifications');
            $this->set('notFount', 'Sorry, there is no new notification');
        }
        if($sortBY == 'confirm'){
            $conditions = array_merge($conditions, array('Waiting.status' => 3, 'Waiting.status !=' => 2));
            $this->set('title', 'Confirmed Waiting List');
        }

        $this->Paginator->settings = array(
            'conditions' => $conditions,
            'fields' => array(
                'Waiting.id',
                'Waiting.uuid',
                'Waiting.appointment_id',
                'Waiting.instructor_id',
                'Waiting.lesson_no',
                'Waiting.start',
                'Waiting.status',
                'Waiting.end',
                'Package.name',
                'Package.uuid',
                'Instructor.user_id',
                'Instructor.first_name',
                'Instructor.last_name',
                'InstructorUser.username',
                'InstructorUser.uuid',
            ),
            'limit' => $this->limit,
            'joins' => array(
                array(
                    'table' => 'packages',
                    'type' => 'LEFT',
                    'alias' => 'Package',
                    'conditions' => array('Package.id = Waiting.package_id')
                ),
                array(
                    'table' => 'profiles',
                    'type' => 'LEFT',
                    'alias' => 'Instructor',
                    'conditions' => array('Instructor.user_id = Waiting.instructor_id')
                ),
                array(
                    'table' => 'users',
                    'type' => 'LEFT',
                    'alias' => 'InstructorUser',
                    'conditions' => array('InstructorUser.id = Waiting.instructor_id')
                ),
            ),
            'order' => array('Waiting.id' => 'DESC')
        );
        $appointmentList = $this->Paginator->paginate('Waiting');
        $this->set('appointments',  $appointmentList);
    }

    /**
     * @param null $id
     * @throws BadRequestException
     */
    public function student_view($id = null)
    {
        $this->set('title_for_layout', 'Appointment Details - '.$this->Utilities->applicationName);
        $appointmentDetails = $this->Appointment->find(
            'first',
            array(
                'conditions' => array(
                    'Appointment.id' => $id
                ),
                'recursive' => -1,
                'fields' => array(
                    'Appointment.id',
                    'Appointment.uuid',
                    'Appointment.start',
                    'Appointment.end',
                    'Appointment.lesson_no',
                    'Appointment.package_id',
                    'Appointment.is_benchmarked',
                    'Appointment.created',
                    'Appointment.status',
                    'Package.name',
                    'Package.uuid',
                    'Profile.user_id',
                    'Profile.first_name',
                    'Profile.last_name',
                    'Profile.phone',
                    'Profile.city',
                    'Profile.state',
                    'Profile.postal_code',
                    'Profile.profile_pic_url',
                    'User.username',
                    'User.uuid',
                    'Instructor.first_name',
                    'Instructor.last_name',
                    'InstructorUser.uuid',
                    'InstructorUser.username',
                    'Benchmark.uuid',
                ),
                'joins' => array(
                    array(
                        'table' => 'packages',
                        'type' => 'LEFT',
                        'alias' => 'Package',
                        'conditions' => array('Package.id = Appointment.package_id')
                    ),
                    array(
                        'table' => 'profiles',
                        'type' => 'LEFT',
                        'alias' => 'Profile',
                        'conditions' => array('Profile.user_id = Appointment.appointment_by')
                    ),
                    array(
                        'table' => 'users',
                        'type' => 'LEFT',
                        'alias' => 'User',
                        'conditions' => array('User.id = Appointment.appointment_by')
                    ),
                    array(
                        'table' => 'profiles',
                        'type' => 'LEFT',
                        'alias' => 'Instructor',
                        'conditions' => array('Instructor.user_id = Appointment.instructor_id')
                    ),
                    array(
                        'table' => 'users',
                        'type' => 'LEFT',
                        'alias' => 'InstructorUser',
                        'conditions' => array('InstructorUser.id = Appointment.instructor_id')
                    ),
                    array(
                        'table' => 'benchmarks',
                        'type' => 'LEFT',
                        'alias' => 'Benchmark',
                        'conditions' => array('Benchmark.appointment_id = Appointment.id')
                    ),
                ),
            )
        );

        if(empty($appointmentDetails))
        {
            throw new BadRequestException;
        }
        $this->set('appointmentDetails', $appointmentDetails);
    }

    /**
     * @param $appointmentID
     * @throws NotFoundException
     *
     * completed appointment of student
     */
    public function student_complete($appointmentID)
    {
        $this->Appointment->id = $appointmentID;
        if (!$this->Appointment->exists()) {
            throw new NotFoundException('Invalid Appointment');
        }
        $this->request->onlyAllow('post', 'delete');
        $this->Appointment->id = $appointmentID;
        if ($this->Appointment->saveField('status', 2)) {
            $this->Session->setFlash('Appointment has been completed', 'flash_success');
        }
        else{
            $this->Session->setFlash('Sorry, something went wrong!', 'flash_error');
        }
        $this->redirect($this->referer());
    }
    /**************************************************************************************************************/
    /******************************************       Custom Functions         ************************************/
    /**************************************************************************************************************/

    /**
     * @param $appointmentID
     */
    public function notifiedToWaitingList($appointmentID)
    {

        $users = $this->Waiting->getWaitingUsers($appointmentID);
        foreach($users as $key => $value){

            $this->Waiting->id = $key;
            $this->Waiting->saveField('is_send_notification', 1);
            $this->Waiting->saveField('status', 2);


            $notificationCode = String::uuid() ;
            $resetURL = Router::url('/', true) . 'appointment/confirm/' . $notificationCode;
            if ($this->Email->sendEmail('mandrill', 'confirm_appointment', $value, 'Confirm your appointment - Golf Swing Prescription', $resetURL)){
                $this->Waiting->id = $key;
                $this->Waiting->saveField('notification_code', $notificationCode);
            }
        }
    }

    /**
     * @param $waitingID
     * @return bool
     */
    public function confirmAppointment($waitingID)
    {

        $waiting =  $this->Waiting->getWaitingByID($waitingID);
        $users = $this->Waiting->getWaitingUsers($waiting['Waiting']['appointment_id']);

        foreach($users as $key => $value){
            $this->Waiting->id = $key;
            $this->Waiting->saveField('status', 6);
        }

        $data = array(
            'uuid' => String::uuid() ,
            'appointment_by' => $waiting['Waiting']['appointment_by'],
            'instructor_id' => $waiting['Waiting']['instructor_id'],
            'package_id' => $waiting['Waiting']['package_id'],
            'lesson_no' => $waiting['Waiting']['lesson_no'],
            'start' => $waiting['Waiting']['start'],
            'end' => $waiting['Waiting']['end'],
            'operated_by' => $waiting['Waiting']['appointment_by'],
            'is_converted' => 1,
            'status' => 2,
        );


        if($this->Appointment->save($data)){
            $this->Waiting->id = $waitingID;
            $this->Waiting->saveField('status', 3);
            return true;
        }

        return false;
    }

    /**
     * @param $notificationCode
     */
    public function confirm($notificationCode)
    {
        $id =  $this->Waiting->getWaitingIDByNotificationCode($notificationCode);
        $waiting =  $this->Waiting->getWaitingByID($id);

        if($waiting['Waiting']['status'] != 3){
            if($id){
                if($this->confirmAppointment($id)){
                    $this->Session->setFlash('Appointment has been confirmed', 'flash_success');
                }
                else{
                    $this->Session->setFlash('Sorry, something went wrong', 'flash_error');
                }
            }
            else{
                $this->Session->setFlash('Sorry, something went wrong', 'flash_error');
            }
        }
        else{
            $this->Session->setFlash('Sorry, this link has been invalid', 'flash_error');
        }
        $this->redirect(array('controller' => 'users', 'action' => 'login'));
    }

    /**
     *
     */
    public function instructor_getAppointmentByID()
    {
        $this->autoLayout = false;
        $this->autoRender = false;
        $this->response->type('application/javascript');

        $appointmentID = $this->request->data['appointmentID'];

        $appointment = $this->Appointment->find('first', array('conditions' => array('Appointment.id' => $appointmentID)));

        if ($appointment) {
            $this->response->body(json_encode($appointment['Appointment']));
        }
        else {
            $this->response->body(json_encode(null));
        }
    }
}
