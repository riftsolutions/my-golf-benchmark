<?php
App::uses('AppController', 'Controller');
/**
 * Class AppointmentsController
 *
 * @property Appointment $Appointment..
 * @property Attachment @Attachment
 * @property Benchmark $Benchmark
 * @property Billing $Billing
 * @property BillingsPackage @BillingsPackage
 * @property BlockTime @BlockTime
 * @property Card $Card
 * @property Config $Config
 * @property Contact $Contact
 * @property CountLesson $CountLesson
 * @property EmailsEvent $EmailsEvent
 * @property EmailsEventsSetting $EmailsEventsSetting
 * @property Event $Event
 * @property GroupLesson $GroupLesson
 * @property GroupLessonsStudents $GroupLessonsStudents
 * @property Note @Note
 * @property Package $Package
 * @property PackagesUser $PackagesUser
 * @property PackagesOrder $PackagesOrder
 * @property Profile $Profile
 * @property Source $Source
 * @property TcsUser $TcsUser
 * @property User $User
 * @property Waiting $Waiting
 *
 * @property PaginatorComponent $Paginator
 * @property EmailComponent $Email
 * @property UtilitiesComponent $Utilities
 * @property AuthorizeNetComponent $AuthorizeNet
 * @property DataTableComponent $DataTable
 * @property ImgComponent $Img
 * @property AppsComponent $Apps
 *
 */

class BillingsController extends AppController
{
    /**
     * The name of this controller.
     *
     * @var string
     */
    public $name = 'Billings';

    public function beforeFilter()
    {
        parent::beforeFilter();
    }

    /**************************************************************************************************************/
    /********************************************        Admin Panel          *************************************/
    /**************************************************************************************************************/

    /**
     *
     */
    public function admin_index()
    {

    }

    /**************************************************************************************************************/
    /******************************************       Instructor Panel         ************************************/
    /**************************************************************************************************************/

    /**
     *
     */
    public function instructor_index()
    {

    }

    /**************************************************************************************************************/
    /******************************************         Student Panel         *************************************/
    /**************************************************************************************************************/

    /**
     *
     */
    public function student_index()
    {

    }

    /**
     * @param $packageUuid
     * @throws BadRequestException
     */
    public function student_checkout($packageUuid = null)
    {
        $this->set('title_for_layout', 'Checkout - '.$this->Utilities->applicationName);
        if(isset($packageUuid)){
            $packageID = $this->Package->getPackageIDByUuid($packageUuid);
            $packageDetails = $this->Package->getPackageByID($packageID);

            if(empty($packageDetails))
            {
                throw new BadRequestException;
            }
            $this->set('package', $packageDetails);
        }
        else{
            $packagesIds = $this->Session->read('packageIds');
            if(empty($packagesIds)){
                $this->Session->setFlash('Please select packages for purchases', 'flash_error');
                $this->redirect(array('controller' => 'packages', 'action' => 'pos'));
            }
            $packages = $this->Package->getPackages($packagesIds);
            $invoiceID = $this->Billing->getBillingByID();
            $this->set('packages', $packages);
            $this->set('cost', $this->Utilities->calculatePrice($packages));
            $this->set('invoiceID', $invoiceID);
            $this->set('instructorInfo', $this->User->getUser($this->Session->read('Auth.User.Profile.instructor_id'), 0));
        }
    }

    /**
     * @param null $packageUuid
     */
    public function student_payment($packageUuid = null)
    {
        $this->set('title_for_layout', 'Payment - '.$this->Utilities->applicationName);
        $packagesIds = $this->Session->read('packageIds');
        $packages = $this->Package->getPackages($packagesIds);
        $packageCost = $this->Utilities->calculatePrice($packages);
        $this->set('packages', $packages);
        $this->set('cost', $packageCost);
        $this->set('cards', $this->Card->getCardList($this->userID));
        $this->set('instructorInfo', $this->User->getUser($this->Session->read('Auth.User.Profile.instructor_id'), 0));

        if($this->request->is('post')){
            $data = $this->request->data;

            $cardID = 'Select Card';
            if(isset($data['existingCard'])){
                $cardID = $data['existingCard'];
            }

            if($cardID != 'Select Card'){
                $card = $this->Card->getCard($data['existingCard'], $this->userID);
                if(empty($card)){
                    $this->Session->setFlash('Sorry, something went wrong', 'flash_error');
                    $this->redirect($this->referer());
                }

                $authorizedNetCustomerID = $card['Card']['authorise_dot_net_customer_profile_id'];
                $authorizedNetPaymentProfileID = $card['Card']['authorise_dot_net_payment_profile_id'];
                $authorizedNetShippingID = $card['Card']['authorise_dot_net_shipping_id'];
                $paymentCardNo = $cardID;
            }
            else{
                if($data['Card']['ExpireYear'] && $data['Card']['ExpireMonth']){
                    $expireDate = $data['Card']['ExpireYear'] . '-' . $data['Card']['ExpireMonth'];
                }
                else{
                    $expireDate = null;
                }
                $data['Card']['Expiry'] = $expireDate;
                $data['Card']['cardID'] = $cardID;
                $this->Card->set($data);
                if($this->Card->cardValidation()){

                    $cardName = $data['Card']['cardName'];
                    $cardNumber = $data['Card']['cardNumber'];
                    $expiryYear = $data['Card']['ExpireYear'];
                    $expiryMonth = $data['Card']['ExpireMonth'];

                    $identifier = 'xxxx xxxx xxxx '.substr($cardNumber, -4);
                    $userInfo = $this->Profile->getProfile('first', array('user_id' => $this->userID));
                    $expireDate = $expiryYear . '-' . $expiryMonth;

                    $basicInfo = array(
                        'description' => '',
                        'name' => $userInfo['Profile']['name'],
                        'email' => $userInfo['User']['username'],
                    );
                    $cardInfo = array(
                        'cardNumber' => $cardNumber,
                        'exp_date' => $expireDate,
                    );
                    $shippingInfo = array(
                        'firstName' => $userInfo['Profile']['first_name'],
                        'lastName' => $userInfo['Profile']['last_name'],
                        'companyName' => 'Golf Swing Prescription',
                        'address' => $userInfo['Profile']['billing_street_1'],
                        'city' => $userInfo['Profile']['billing_city'],
                        'state' =>$userInfo['Profile']['billing_state'],
                        'zip' => $userInfo['Profile']['billing_postal_code'],
                        'country' => $userInfo['Profile']['billing_country'],
                        'phoneNumber' => $userInfo['Profile']['phone'],
                        'faxNumber' => $userInfo['Profile']['fax'],
                    );

                    $authorizedCustomerID = 0;
                    $authorizedPaymentProfileID = 0;
                    $authorizedShippingID = 0;
                    $createAuthorisedAcc = $this->AuthorizeNet->createCustomerProfileCIM($basicInfo);
                    $authorizedCustomerID = (int)$createAuthorisedAcc['id'];
                    $createAuthorisedPaymentAcc = $this->AuthorizeNet->createCustomerPaymentProfileCIM(
                        $authorizedCustomerID,
                        $cardInfo
                    );
                    if (isset($createAuthorisedPaymentAcc['id'])) {
                        $authorizedPaymentProfileID = (int)$createAuthorisedPaymentAcc['id'];
                    }
                    $createAuthorisedShippingAcc = $this->AuthorizeNet->createCustomerShippingAddressCIM(
                        $authorizedCustomerID,
                        $shippingInfo
                    );
                    if (isset($createAuthorisedShippingAcc['id'])) {
                        $authorizedShippingID = (int)$createAuthorisedShippingAcc['id'];
                    }

                    $cardInfo = array(
                        'uuid' => String::uuid() ,
                        'user_id' => $this->userID,
                        'name' => $cardName,
                        'identifier' => $identifier,
                        'is_visible' => 1,
                        'authorise_dot_net_customer_profile_id' => $authorizedCustomerID,
                        'authorise_dot_net_payment_profile_id' => $authorizedPaymentProfileID,
                        'authorise_dot_net_shipping_id' => $authorizedShippingID,
                    );

                    if($this->Card->save($cardInfo)){
                        $authorizedNetCustomerID = $cardInfo['authorise_dot_net_customer_profile_id'];
                        $authorizedNetPaymentProfileID = $cardInfo['authorise_dot_net_payment_profile_id'];
                        $authorizedNetShippingID = $cardInfo['authorise_dot_net_shipping_id'];
                        $paymentCardNo = $this->Card->getLastInsertID();
                    }
                    $this->Session->setFlash('Sorry, something went wrong', 'flash_error');
                }
                else{
                    if($data['Card']['cardName'] && $data['Card']['cardNumber'] && $data['Card']['ExpireYear'] && $data['Card']['ExpireMonth']){
                        $this->Session->write('cardValidationError', true);
                    }
                }
            }

            if(isset($authorizedNetCustomerID) && isset($authorizedNetCustomerID) && isset($authorizedNetCustomerID))
            {
                $transactionResponse = $this->AuthorizeNet->createTransactionCIM($packageCost['total'], $authorizedNetCustomerID, $authorizedNetPaymentProfileID, $authorizedNetShippingID);
            }
            else{
                $this->Session->setFlash('Sorry, something went wrong', 'flash_error');
            }



            if(isset($transactionResponse) && isset($transactionResponse['payment_final_status'])){

                if($transactionResponse['payment_final_status'] == 1){

                    /*if(isset($packageDetails['PackagesUser']['id'])){
                        $packagesUserID = $packageDetails['PackagesUser']['id'];
                        $this->PackagesUser->id = $packageDetails['PackagesUser']['id'];
                        $this->PackagesUser->saveField('payment_status', 1);
                    }
                    else{
                        $userInfo = $this->Profile->getProfile('first', array('user_id' => $this->userID));
                        $instructorID = null;
                        if(isset($userInfo['Profile']['instructor_id'])){
                            $instructorID = $userInfo['Profile']['instructor_id'];
                        }

                        $expirationDays = $packageDetails['Package']['expiration_days'];
                        $date = strtotime("+{$expirationDays} day");
                        $expirationDate = date('Y-m-d H:i:s', $date);

                        $packageUsers = array(
                            'instructor_id' => $instructorID,
                            'created_for' => $this->userID,
                            'package_id' => $packageID,
                            'total_lesson' => $packageDetails['Package']['lesson'],
                            'available' => $packageDetails['Package']['lesson'],
                            'expiration_date' => $expirationDate,
                            'payment_status' => 1
                        );
                        $this->PackagesUser->create();
                        $this->PackagesUser->save($packageUsers);
                        $packagesUserID = $this->PackagesUser->getLastInsertID();
                    }*/

                    $countTotalLesson = 0;
                    foreach($packages as $package){
                        $expirationDays = $package['Package']['expiration_days'];
                        $date = strtotime("+{$expirationDays} day");
                        $expirationDate = date('Y-m-d H:i:s', $date);

                        $packageUsers = array(
                            'instructor_id' => $this->Session->read('Auth.User.Profile.instructor_id'),
                            'created_for' => $this->userID,
                            'package_id' => $package['Package']['id'],
                            'total_lesson' => $package['Package']['lesson'],
                            'available' => $package['Package']['lesson'],
                            'expiration_date' => $expirationDate,
                            'payment_status' => 1
                        );
                        $countTotalLesson = $countTotalLesson + $package['Package']['lesson'];
                        $this->PackagesUser->create();
                        $this->PackagesUser->save($packageUsers);

                        $this->CountLesson->countUsersLesson($this->userID, $countTotalLesson);
                    }

                }
                $transactionResponse = array_merge(
                    $transactionResponse,
                    array(
                        'user_id' => $this->userID,
                        'uuid' => String::uuid() ,
                        'subtotal' => $packageCost['subtotal'],
                        'tax' => $packageCost['tax'],
                        'amount' => $packageCost['total'],
                    )
                );

                $transactionResponse['card_id'] = $paymentCardNo;
                $this->Billing->create();
                $this->Billing->save($transactionResponse);
                if(isset($transactionResponse['payment_response_code']))
                {
                    $billingID = $this->Billing->getLastInsertID();
                    foreach($packages as $package){
                        $billingPackages[] = array(
                            'billing_id' => $billingID,
                            'package_id' => $package['Package']['id'],
                        );
                    }
                    $this->BillingsPackage->saveAll($billingPackages);

                    $this->Session->write('packageIds', null);
                    $this->Session->setFlash($transactionResponse['payment_response_reason_text'], 'flash_success');
                    $this->redirect(array('controller' => 'reports', 'action' => 'my_package'));
                    //$this->redirect(array('controller' => 'reports', 'action' => 'my_package_view', $packageDetails['Package']['uuid']));
                }
                else{
                    $this->Session->setFlash($transactionResponse['message'], 'flash_error');
                }
            }
            else{
                $this->Session->setFlash('Sorry, something went wrong', 'flash_error');
            }
        }

    }

    /**
     * @param null $packageUuid
     * @throws BadRequestException
     */
    public function instructor_checkout($packageUuid = null)
    {
        $this->set('title_for_layout', 'Checkout - '.$this->Utilities->applicationName);
        if(isset($packageUuid)){
            $packageID = $this->Package->getPackageIDByUuid($packageUuid);
            $packageDetails = $this->Package->getPackageByID($packageID);

            if(empty($packageDetails))
            {
                throw new BadRequestException;
            }
            $this->set('package', $packageDetails);
        }
        else{

            $packagesIds = $this->Session->read('packageIds');
            if(empty($packagesIds)){

                $this->Session->setFlash('Please select packages for purchases', 'flash_error');
                $this->redirect(array('controller' => 'packages', 'action' => 'pos'));
            }

            $packages = $this->Package->getPackages($packagesIds);
            if(empty($packages)){
                $this->Session->setFlash('Please select packages for purchases', 'flash_error');
                $this->redirect(array('controller' => 'packages', 'action' => 'pos'));
            }

            $invoiceID = $this->Billing->getBillingByID();
            $this->set('packages', $packages);
            $this->set('cost', $this->Utilities->calculatePrice($packages));
            $this->set('invoiceID', $invoiceID);
        }
    }

    /**
     *
     */
    public function instructor_payment()
    {
        $this->set('title_for_layout', 'Payment - '.$this->Utilities->applicationName);
        $packagesIds = $this->Session->read('packageIds');
        if(empty($packagesIds)){
            $this->Session->setFlash('Please select packages for purchases', 'flash_error');
            $this->redirect(array('controller' => 'packages', 'action' => 'pos'));
        }

        $students = $this->Profile->getStudentForInstructor('list', $this->userID);
        $packages = $this->Package->getPackages($packagesIds);
        $packageCost = $this->Utilities->calculatePrice($packages);
        $this->set('packages', $packages);
        $this->set('cost', $packageCost);
        $this->set('students', $students);

        $authorizedNetCustomerID = null;
        $authorizedNetPaymentProfileID = null;
        $authorizedNetShippingID = null;

        if($this->request->is('post')){
            $data = $this->request->data;
            
            $studentID = $data['Billing']['user_id'];
            $cardID = $data['Billing']['card_id'];
            
            $student = $this->User->getUser($studentID);
            $studentStripeCustomer = $student['User']['stripe_customer'];
            
            $myStripeAccountID = $this->Session->read('Auth.User.stripe_account_id');
            
            $packageNamesArray = array();
            foreach($packages as $package){
                array_push($packageNamesArray, $package['Package']['name']);
            }
            $packageNames = implode(', ', $packageNamesArray);
            
            \Stripe\Charge::create(array(
                "amount" => $packageCost['total'] * 100, // dollars to cents
                "currency" => "usd",
                "customer" => $studentStripeCustomer, // the student's stripe customer (stripe card)
                "destination" => $myStripeAccountID,
                "description" => $packageNames
            ));
            
            $countTotalLesson = 0;
            foreach($packages as $package){
                $expirationDays = $package['Package']['expiration_days'];
                $date = strtotime("+{$expirationDays} day");
                $expirationDate = date('Y-m-d H:i:s', $date);

                $packageUsers = array(
                    'instructor_id' => $this->userID,
                    'created_for' => $studentID,
                    'package_id' => $package['Package']['id'],
                    'total_lesson' => $package['Package']['lesson'],
                    'available' => $package['Package']['lesson'],
                    'expiration_date' => $expirationDate,
                    'payment_status' => 1
                );
                $countTotalLesson = $countTotalLesson + $package['Package']['lesson'];
                $this->PackagesUser->create();
                $this->PackagesUser->save($packageUsers);
            }
            $this->CountLesson->countUsersLesson($studentID, $countTotalLesson);
            
            $this->redirect(array('controller' => 'reports', 'action' => 'my_package'));
            
        }
   
    }

    /**
     *
     */
    public function instructor_refund(){
        $this->set('title_for_layout', 'Refund Money - '.$this->Utilities->applicationName);
    }

    /**
     *
     */
    public function instructor_getTransactionDetails(){
        $this->autoLayout = false;
        $this->autoRender = false;
        $this->response->type('application/javascript');

        $transactionID = $this->request['data']['transactionID'];
        $transactionIDetails = $this->AuthorizeNet->transactionDetails($transactionID);

        if($transactionIDetails){
            $this->response->body(json_encode($transactionIDetails));
        }
        else{
            $this->response->body(json_encode('error'));
        }
    }

    /**
     * @throws ForbiddenException
     */
    public function instructor_refundPayment(){
        if($this->request->is('post')){
            $transID = $this->request->data['transId'];
            $amount = (float) $this->request->data['amount'];

            /*$transactionIDetails = $this->Billing->getBillingByTransactionID($transID);
            $cardInfo = $this->Card->getCardByID($transactionIDetails['Billing']['card_id']);


            $transactionResponse = $this->AuthorizeNet->refundTransaction($amount, $transID, $cardInfo['Card']['authorise_dot_net_customer_profile_id'], $cardInfo['Card']['authorise_dot_net_payment_profile_id']);
            $transactionResponse['user_id'] = $cardInfo['Card']['user_id'];
            $transactionResponse['card_id'] = $cardInfo['Card']['id'];*/

           //var_dump($transactionResponse); die();

           /* $this->Billing->create();
            if($this->Billing->save($transactionResponse)){
                $this->Session->setFlash('Refund process is successfully started', 'flash_success');
            }
            else{
                $this->Session->setFlash('Sorry, something went wrong', 'flash_danger');
            }*/

            $this->Session->setFlash('Refund process is successfully started', 'flash_success');
            $this->redirect($this->referer());
        }
        else{
            throw new ForbiddenException;
        }
    }


    /**************************************************************************************************************/
    /******************************************       Custom Function          ************************************/
    /**************************************************************************************************************/
}
