<?php
App::uses('AppController', 'Controller');
/**
 * Class AppointmentsController
 *
 * @property Appointment $Appointment..
 * @property Attachment @Attachment
 * @property Benchmark $Benchmark
 * @property Billing $Billing
 * @property BillingsPackage @BillingsPackage
 * @property BlockTime @BlockTime
 * @property Card $Card
 * @property Config $Config
 * @property Contact $Contact
 * @property CountLesson $CountLesson
 * @property EmailsEvent $EmailsEvent
 * @property EmailsEventsSetting $EmailsEventsSetting
 * @property Event $Event
 * @property GroupLesson $GroupLesson
 * @property GroupLessonsStudents $GroupLessonsStudents
 * @property Note @Note
 * @property Package $Package
 * @property PackagesUser $PackagesUser
 * @property PackagesOrder $PackagesOrder
 * @property Profile $Profile
 * @property Source $Source
 * @property TcsUser $TcsUser
 * @property User $User
 * @property Waiting $Waiting
 *
 * @property PaginatorComponent $Paginator
 * @property EmailComponent $Email
 * @property UtilitiesComponent $Utilities
 * @property AuthorizeNetComponent $AuthorizeNet
 * @property DataTableComponent $DataTable
 * @property ImgComponent $Img
 * @property AppsComponent $Apps
 *
 */

class BenchmarksController extends AppController
{

    public $name = 'Benchmarks';

    /**
     * Index page of benchmark for the instructor.
     */
    public function instructor_index()
    {
        $this->redirect(array('controller' => 'benchmarks', 'action' => 'list'));
    }


    public function instructor_new()
    {
        
    }



    /**
     * Instructor list page for the benchmark
     */
    public function instructor_history()
    {
        $this->set('title_for_layout', 'List of Benchmark - '.$this->Utilities->applicationName);
        $conditions = array('Benchmark.created_by' => $this->userID);
        $title = 'Benchmark List';
        $fields = array(
            'Benchmark.*',
            'Package.id',
            'Package.uuid',
            'Package.name',
            'Package.price',
            'User.uuid',
            'Profile.first_name',
            'Profile.last_name',
        );

        if(isset($this->request->query['benchmark'])){
            $searchTerm = $this->request->query['benchmark'];
            $searchConditions = array(
                'OR' => array(
                    'Benchmark.id' => $searchTerm,
                    'Benchmark.appointment_id' => $searchTerm,
                    'Benchmark.package_id' => $searchTerm,
                    'Package.name LIKE' => '%' . $searchTerm . '%',
                )
            );
            $conditions = array_merge($conditions, $searchConditions);
            $title = 'Benchmark for "'. $searchTerm.'"';
        }

        $this->Paginator->settings = array(
            'conditions' => $conditions,
            'fields' => $fields,
            'order' => 'Benchmark.id DESC',
            'limit' => $this->limit,
            'recursive' => -1,
            'joins' => array(
                array(
                    'table' => 'packages',
                    'type' => 'LEFT',
                    'alias' => 'Package',
                    'conditions' => array('Package.id = Benchmark.package_id')
                ),
                array(
                    'table' => 'users',
                    'type' => 'LEFT',
                    'alias' => 'User',
                    'conditions' => array('User.id = Benchmark.created_for')
                ),
                array(
                    'table' => 'profiles',
                    'type' => 'LEFT',
                    'alias' => 'Profile',
                    'conditions' => array('Profile.user_id = Benchmark.created_for')
                )
            ),

        );
        $this->set('benchmarkList', $this->Paginator->paginate('Benchmark'));
        $this->set('title', $title);
    }

    /**
     * @param null $benchmarkUUID
     *
     * benchmark page of benchmark for the instructor.
     */
    public function instructor_view($benchmarkUUID = null)
    {
        $this->set('title_for_layout', 'Details of Benchmark - '.$this->Utilities->applicationName);
        $this->fetchBenchmark($benchmarkUUID);
    }

    /**************************************************************************************************************/
    /******************************************         Student Panel         *************************************/
    /**************************************************************************************************************/

    /**
     * Index page of benchmark for instructor
     */
    public function student_index()
    {
        $this->redirect(array('controller' => 'benchmarks', 'action' => 'list'));
    }

    /**
     * Benchmark list for the instructor.
     */
    public function student_list($packageUUID = array())
    {
        $packageDetails = $this->Package->getPackageByUuid($packageUUID);
        if($packageDetails){
            $packageID = $packageDetails['Package']['id'];
        }

        $conditions = array('Benchmark.created_for' => $this->userID);
        $title = 'Benchmark List';

        if(isset($packageUUID) && $packageDetails){
            $conditions = array_merge($conditions, array('Benchmark.package_id' => $packageID));
        }
        if($packageDetails){
            $title = 'Benchmark List of '.$packageDetails['Package']['name'];
        }

        if(isset($this->request->query['benchmark'])){
            $searchTerm = $this->request->query['benchmark'];
            $searchConditions = array(
                'OR' => array(
                    'Benchmark.id' => $searchTerm,
                    'Benchmark.appointment_id' => $searchTerm,
                    'Benchmark.package_id' => $searchTerm,
                    'Package.name LIKE' => '%' . $searchTerm . '%',
                )
            );
            $conditions = array_merge($conditions, $searchConditions);
            $title = 'Benchmark for "'. $searchTerm.'"';
        }

        $this->set('title_for_layout', 'List of Benchmark - '.$this->Utilities->applicationName);
        $fields = array(
            'Benchmark.*',
            'Package.id',
            'Package.uuid',
            'Package.name',
            'Package.price',
            'User.uuid',
            'Profile.first_name',
            'Profile.last_name',
        );
        $this->Paginator->settings = array(
            'conditions' => $conditions,
            'fields' => $fields,
            'order' => 'Benchmark.id DESC',
            'limit' => $this->limit,
            'recursive' => -1,
            'joins' => array(
                array(
                    'table' => 'packages',
                    'type' => 'LEFT',
                    'alias' => 'Package',
                    'conditions' => array('Package.id = Benchmark.package_id')
                ),
                array(
                    'table' => 'users',
                    'type' => 'LEFT',
                    'alias' => 'User',
                    'conditions' => array('User.id = Benchmark.created_for')
                ),
                array(
                    'table' => 'profiles',
                    'type' => 'LEFT',
                    'alias' => 'Profile',
                    'conditions' => array('Profile.user_id = Benchmark.created_for')
                )
            ),

        );
        $this->set('benchmarkList', $this->Paginator->paginate('Benchmark'));
        $this->set('title', $title);
    }

    /**
     * @param null $benchmarkUUID
     *
     * View page of benchmark for the instructor
     */
    public function student_view($benchmarkUUID = null)
    {
        $this->set('title_for_layout', 'Details of Benchmark - '.$this->Utilities->applicationName);
        $this->fetchBenchmark($benchmarkUUID);
    }


    /**
     * @param $benchmarkUUID
     * @return mixed
     * @throws BadRequestException
     *
     * Getting information for specific benchmark.
     */
    protected function fetchBenchmark($benchmarkUUID)
    {
        $benchmarkDetails = $this->Benchmark->find(
            'first',
            array(
                'conditions' => array(
                    'Benchmark.uuid' => $benchmarkUUID),
                'recursive' => 2
            )
        );

        if(empty($benchmarkUUID)){
            throw new BadRequestException;
        }

        $this->set('benchmarkDetails', $benchmarkDetails);
        return $benchmarkDetails;
    }

} 