<?php
App::uses('AppController', 'Controller');
/**
 * Class AppointmentsController
 *
 * @property Appointment $Appointment..
 * @property Attachment @Attachment
 * @property Benchmark $Benchmark
 * @property Billing $Billing
 * @property BillingsPackage @BillingsPackage
 * @property BlockTime @BlockTime
 * @property Card $Card
 * @property Config $Config
 * @property Contact $Contact
 * @property CountLesson $CountLesson
 * @property EmailsEvent $EmailsEvent
 * @property EmailsEventsSetting $EmailsEventsSetting
 * @property Event $Event
 * @property GroupLesson $GroupLesson
 * @property GroupLessonsStudents $GroupLessonsStudents
 * @property Note @Note
 * @property Package $Package
 * @property PackagesUser $PackagesUser
 * @property PackagesOrder $PackagesOrder
 * @property Profile $Profile
 * @property Source $Source
 * @property TcsUser $TcsUser
 * @property User $User
 * @property Waiting $Waiting
 *
 * @property PaginatorComponent $Paginator
 * @property EmailComponent $Email
 * @property UtilitiesComponent $Utilities
 * @property AuthorizeNetComponent $AuthorizeNet
 * @property DataTableComponent $DataTable
 * @property ImgComponent $Img
 * @property AppsComponent $Apps
 *
 */

class TrashController extends AppController
{
    /**
     * The name of this controller.
     *
     * @var string
     */
    public $name = 'Trash';

    public function beforeFilter()
    {
        parent::beforeFilter();
    }

    /**************************************************************************************************************/
    /********************************************        Admin Panel          *************************************/
    /**************************************************************************************************************/

    /**
     *
     */
    public function admin_index()
    {

    }

    /**************************************************************************************************************/
    /******************************************       Instructor Panel         ************************************/
    /**************************************************************************************************************/

    /**
     *
     */
    public function instructor_index()
    {

    }

    /**
     *
     */
    public function instructor_user()
    {
        $this->set('title_for_layout', 'List of Student - '.$this->Utilities->applicationName);
        $conditions = array('Profile.instructor_id' => $this->userID, 'User.status' => 9);

        $this->Paginator->settings = array(
            'conditions' => $conditions,
            'limit' => $this->limit,
            'recursive' => 1,
            'order' => 'User.id DESC'
        );

        $this->set('userList', $this->Paginator->paginate('User'));
    }

    /**
     *
     */
    public function instructor_contact()
    {
        $this->set('title_for_layout', 'List of Lead - '.$this->Utilities->applicationName);
        $conditions = array('Contact.user_id' => $this->userID, 'Contact.status' => 9);
        $this->Paginator->settings = array(
            'conditions' => $conditions,
            'limit' => $this->limit,
            'order' => 'Contact.id DESC'
        );

        $this->set('contactList', $this->Paginator->paginate('Contact'));
    }

    /**************************************************************************************************************/
    /******************************************         Student Panel         *************************************/
    /**************************************************************************************************************/

    /**
     *
     */
    public function student_index()
    {

    }

    /**************************************************************************************************************/
    /******************************************       Custom Function          ************************************/
    /**************************************************************************************************************/
}
