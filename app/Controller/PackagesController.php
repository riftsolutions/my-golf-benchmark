<?php
App::uses('AppController', 'Controller');
/**
 * Class AppointmentsController
 *
 * @property Appointment $Appointment..
 * @property Attachment @Attachment
 * @property Benchmark $Benchmark
 * @property Billing $Billing
 * @property BillingsPackage @BillingsPackage
 * @property BlockTime @BlockTime
 * @property Card $Card
 * @property Config $Config
 * @property Contact $Contact
 * @property CountLesson $CountLesson
 * @property EmailsEvent $EmailsEvent
 * @property EmailsEventsSetting $EmailsEventsSetting
 * @property Event $Event
 * @property GroupLesson $GroupLesson
 * @property GroupLessonsStudents $GroupLessonsStudents
 * @property Note @Note
 * @property Package $Package
 * @property PackagesUser $PackagesUser
 * @property PackagesOrder $PackagesOrder
 * @property Profile $Profile
 * @property Source $Source
 * @property TcsUser $TcsUser
 * @property User $User
 * @property Waiting $Waiting
 *
 * @property PaginatorComponent $Paginator
 * @property EmailComponent $Email
 * @property UtilitiesComponent $Utilities
 * @property AuthorizeNetComponent $AuthorizeNet
 * @property DataTableComponent $DataTable
 * @property ImgComponent $Img
 * @property AppsComponent $Apps
 *
 */

class PackagesController extends AppController
{
    /**
     * The name of this controller.
     *
     * @var string
     */
    public $name = 'Packages';

    /**************************************************************************************************************/
    /******************************************       Admin Panel         ************************************/
    /**************************************************************************************************************/


    /**************************************************************************************************************/
    /********************************************        Instructor Panel          *************************************/
    /**************************************************************************************************************/

    /**
     * Index page of package for instructor
     */
    public function instructor_index()
    {
        $this->redirect(array('controller' => 'packages', 'action' => 'list'));
    }

    /**
     * Package list page for instructor.
     */
    public function instructor_list($sortBy = null)
    {
        $title = 'List of items';
        $sid = null;

        $condition = array(
            'OR' => array(
                'Package.created_by' => $this->userID,
                'Package.is_default' => 1
            )
        );

        if($sortBy == 'users'){
            $condition = array(
                'Package.created_by' => $this->userID
            );
            $title = 'List of My Created Item';
        }

        if($sortBy == 'default'){
            $condition = array(
                'Package.is_default' => 1
            );
            $title = 'List of Default Item';
        }

        if($sortBy == 'visible'){
            $condition = array(
                'OR' => array(
                    'Package.created_by' => $this->userID,
                    'Package.is_default' => 1
                ),
                'Package.status' => 1
            );
            $title = 'List of Visible Item';
        }

        if($sortBy == 'invisible'){
            $condition = array(
                'OR' => array(
                    'Package.created_by' => $this->userID,
                    'Package.is_default' => 1
                ),
                'Package.status' => 0
            );
            $title = 'List of Invisible Item';
        }

        if(isset($this->request->query['sid']) && $this->request->query['sid'] && $this->request->query['sid'] != '')
        {
            $student = $this->User->getUserBasicUuid($this->request->query['sid']);
            $condition = array_merge($condition, array('PackagesUser.created_for' => $student['User']['id']));
            $title = 'Package purchased by '.$student['Profile']['name'].' ('. $student['User']['username'].')';
            $sid = $this->request->query['sid'];
        }
        /**
         * For Modal Search
         */

        if (isset($this->request->query['item_name']) && $this->request->query['item_name'] && $this->request->query['item_name'] != '') {
            $condition = array_merge($condition,
                array('Package.name Like' => '%' . $this->request->query['item_name'] . '%'));
        }

        if (isset($this->request->query['amount']) && $this->request->query['amount'] && $this->request->query['amount'] != 'Choose Price') {
            $amount = $this->request->query['amount'];
            $condition = array_merge($condition, array('Package.price >=' => $amount));
        }

        if (isset($this->request->query['starting_amount']) && $this->request->query['starting_amount']) {
            $startingAmount = $this->request->query['starting_amount'];
            $condition = array_merge($condition, array('Package.price >=' => $startingAmount));
        }

        if (isset($this->request->query['ending_amount']) && $this->request->query['ending_amount']) {
            $endingAmount = $this->request->query['ending_amount'];
            $condition = array_merge($condition, array('Package.price <=' => $endingAmount));
        }

        if (isset($this->request->query['type']) ) {
            if ($this->request->query['type'] != '') {
                $type = (int)$this->request->query['type'];
                $condition = array_merge($condition, array('Package.is_default' => $type));
            }
        }


        if (isset($this->request->query['status']) ) {
            $status = $this->request->query['status'];
            if ($status != '') {
                $status = (int)$this->request->query['status'];
                $condition = array_merge($condition, array('Package.status' => $status));

            }
        }

        $this->Paginator->settings = array(
            'conditions' => $condition,
            'limit' => $this->limit,
            'order' => array('Package.id' => 'DESC'),
            'recursive' => -1,
            'joins' => array(
                array(
                    'table' => 'packages_users',
                    'type' => 'LEFT',
                    'alias' => 'PackagesUser',
                    'conditions' => array('Package.id = PackagesUser.package_id')
                ),
            ),
        );


        if(isset($student))
        {
            $packageOverview = $this->Package->Overview($this->userID, array('conditions' => array('PackagesUser.created_for' => $student['User']['id'])));
        }
        else{
            $packageOverview = $this->Package->getPackageOverview($this->userID);
        }

        $packages = $this->Paginator->paginate('Package');
        $this->set('packages', $packages);
        $this->set('title', $title);
        $this->set('sid', $sid);
        $this->set('packageOverview', $packageOverview);
        $this->set('title_for_layout', $title.' - '.$this->Utilities->applicationName);
    }

    /**
     * Create package page.
     */
    public function instructor_create()
    {
        $this->set('title_for_layout', 'Create Item - '.$this->Utilities->applicationName);
        if($this->request->is('post')){
            $this->request->data['Package']['uuid'] = String::uuid() ;
            $this->request->data['Package']['created_by'] = $this->userID;

            $taxPercentage = $this->request->data['Package']['tax_percentage'];
            $price = $this->request->data['Package']['price'];
            $taxAmount = ($taxPercentage / 100) * $price;
            $total = $price + $taxAmount;

            $this->request->data['Package']['total'] = $total;
            $this->request->data['Package']['tax'] = $taxAmount;
            $this->request->data['Package']['is_default'] = 0;

            if($this->Package->save($this->request->data)){
                $this->Session->setFlash('Item has been created successfully', 'flash_success');
                $this->redirect(array('controller' => 'packages', 'action' => 'list'));
            }
            else{
                $this->Session->setFlash('Sorry, something went wrong', 'flash_error');
            }
        }
    }

    /**
     * @param null $packageUuid
     * @param null $userID
     *
     * Instructor can assign package to student
     */

    public function instructor_assign($packageUuid = null, $userID = null)
    {
        $this->set('title_for_layout', 'Assign Item - '.$this->Utilities->applicationName);
        $studentDetails = $this->Profile->getStudentForInstructor('list', $this->userID);
        $packageList = $this->Package->getPackageList($this->userID);
        $packageDetails = $this->Package->getPackageByUuid($packageUuid);
        if(empty($studentDetails) || empty($packageList)){
            $this->Session->setFlash("Sorry, you haven't any student assign item", 'flash_error');
            $this->redirect($this->referer());
        }

        if($packageDetails)
        {
            $this->set('packageID', $packageDetails['Package']['id']);
        }
        else{
            $this->set('packageID', null);
        }

        if($userID)
        {
            $this->set('userID', $userID);
        }
        else{
            $this->set('userID', null);
        }

        $this->set('studentList', $studentDetails);
        $this->set('packages', $packageList);

        if($this->request->is('post')){
            $this->PackagesUser->create();

            $isAlreadyAssigned = $this->PackagesUser->find(
                'first', array(
                    'conditions' => array(
                        'PackagesUser.package_id' => $this->request->data['PackagesUser']['package_id'],
                        'PackagesUser.created_for' => $this->request->data['PackagesUser']['created_for'])));

            if($isAlreadyAssigned)
            {
                $this->PackagesUser->id = $isAlreadyAssigned['PackagesUser']['id'];
            }
            else{
                $currentLessonNumber = $this->Package->getPackageLesson($this->request->data['PackagesUser']['package_id']);
                $this->request->data['PackagesUser']['available'] = $currentLessonNumber;
                $this->request->data['PackagesUser']['instructor_id'] = $this->userID;
                $this->PackagesUser->create();
            }


            if($this->PackagesUser->save($this->request->data))
            {
                if($packageUuid && $userID)
                {
                    $this->Session->setFlash("Item has been reassigned to student successfully", 'flash_success');
                    $this->redirect(array('controller' => 'packages', 'action' => 'current'));
                }
                else{
                    $this->Session->setFlash("Item has been assigned to student successfully", 'flash_success');
                    $this->redirect(array('controller' => 'packages', 'action' => 'pos'));
                }
            }
        }
    }

    /**
     * @param $packageUuid
     * @throws BadRequestException
     *
     * Edit package page
     */
    public function instructor_edit($packageUuid)
    {
        $this->set('title_for_layout', 'Edit Item - '.$this->Utilities->applicationName);
        $packageDetails = $this->Package->getPackageByUuid($packageUuid);
        $this->set('package', $packageDetails);

        if(empty($packageDetails))
        {
            throw new BadRequestException;
        }

        if($this->request->is('post')){

            if(!isset($this->request->data['Package']['status'])){
                $this->request->data['Package']['status'] = 1;
            }

            $taxPercentage = $this->request->data['Package']['tax_percentage'];
            $price = $this->request->data['Package']['price'];
            $taxAmount = ($taxPercentage / 100) * $price;
            $total = $price + $taxAmount;

            $this->request->data['Package']['total'] = $total;
            $this->request->data['Package']['tax'] = $taxAmount;
            $this->request->data['Package']['tax_percentage'] = (float) $taxPercentage;
            $this->request->data['Package']['is_default'] = 0;

            $this->Package->id = $packageDetails['Package']['id'];
            if($this->Package->save($this->request->data)){
                $this->Session->setFlash('Item has been updated successfully', 'flash_success');
                $this->redirect(array('controller' => 'packages', 'action' => 'list'));
            }
            else{
                $this->Session->setFlash('Sorry, something went wrong', 'flash_error');
            }
        }

    }

    /**
     * @param $packageUuid
     * @throws BadRequestException
     *
     * Package view page
     */
    public function instructor_view($packageUuid)
    {
        $this->set('title_for_layout', 'Item Details - '.$this->Utilities->applicationName);
        $packageDetails = $this->Package->getPackageByUuid($packageUuid);
        $this->set('package', $packageDetails);

        if(empty($packageDetails))
        {
            throw new BadRequestException;
        }
    }

    /**
     * @param $packageUuid
     * @throws NotFoundException
     *
     * Package delete page
     */
    public function instructor_delete($packageUuid)
    {
        $packageDetails = $this->Package->getPackageByUuid($packageUuid);

        $this->Package->id = $packageDetails['Package']['id'];
        if (!$this->Package->exists()) {
            throw new NotFoundException('Invalid Package');
        }

        $this->request->onlyAllow('post', 'delete');
        if ($this->Package->delete($packageDetails['Package']['id'], true)) {
            $this->Session->setFlash('Item has been deleted!', 'flash_success');
        }
        else{
            $this->Session->setFlash('Sorry, something went wrong!', 'flash_error');
        }
        $this->redirect($this->referer());
    }

    /**
     *
     */
    public function instructor_pos()
    {
        $this->set('title_for_layout', 'Point of Sales - '.$this->Utilities->applicationName);
        $packagesIds = array_keys($this->Package->getPackageList($this->userID));

        $isOrderExists = $this->PackagesOrder->isOrderExists($this->userID);
        $isOrderExistsCount = $this->PackagesOrder->isOrderExistsCount($this->userID);

        if($isOrderExists == false && !empty($packagesIds)){
            $order = 1;
            foreach($packagesIds as $key => $value){
                $data[] = array(
                    'user_id' => $this->userID,
                    'package_id' => $value,
                    'order' => $order,
                );
                $order++;
            }
            $this->PackagesOrder->saveAll($data);
        }
        elseif(sizeof($packagesIds) > $isOrderExistsCount){
            $this->PackagesOrder->addPackage((int)end($packagesIds), $this->userID, $isOrderExistsCount);
        }

        $this->Paginator->settings = array(
            'conditions' => array('PackagesOrder.user_id' => $this->userID, 'Package.status' => 1),
            'fields' => array(
                'Package.*',
                'PackagesOrder.id',
                'PackagesOrder.order',
            ),
            'limit' => $this->limit,
            'order' => array('PackagesOrder.order' => 'ASC'),
            'recursive' => -1,
            'joins' => array(
                array(
                    'table' => 'packages',
                    'type' => 'LEFT',
                    'alias' => 'Package',
                    'conditions' => array('Package.id = PackagesOrder.package_id')
                ),
            ),
        );

        $packages = $this->Paginator->paginate('PackagesOrder');
        $this->set('packages', $packages);
        $this->set('selectedPackagesIds', $this->Session->read('packageIds'));

        $currentPageNo = 0;
        if(isset($this->request->params['named']['page'])){
            $currentPageNo = $this->request->params['named']['page'];
        }
        $this->set('currentPageNo', $currentPageNo);
    }


    /**
     * @param $packageUuid
     * @throws BadRequestException
     */
    public function instructor_pos_view($packageUuid)
    {
        $this->set('title_for_layout', 'Point of Sales Details - '.$this->Utilities->applicationName);
        $packageDetails = $this->Package->getPackageByUuid($packageUuid);
        $this->set('package', $packageDetails);
        $this->set('selectedPackagesIds', $this->Session->read('packageIds'));

        if(empty($packageDetails))
        {
            throw new BadRequestException;
        }
    }
    /**************************************************************************************************************/
    /******************************************         Student Panel         *************************************/
    /**************************************************************************************************************/

    /**
     *
     */
    public function student_pos()
    {
        $this->set('title_for_layout', 'List of Item - '.$this->Utilities->applicationName);
        $userInfo = $this->Profile->getProfile('first', array('user_id' => $this->userID));
        $previousPackageIDs = $this->PackagesUser->getUsersPackageIds($this->userID);
        $instructorID = null;
        if(isset($userInfo['Profile']['instructor_id'])){
            $instructorID = $userInfo['Profile']['instructor_id'];
        }

        $this->Paginator->settings = array(
            'conditions' => array(
                'OR' => array(
                    'Package.created_by' => $instructorID,
                    'Package.is_default' => 1
                ),
                /*'Package.id !=' => $previousPackageIDs,*/
                'Package.status' => 1,
                'PackagesOrder.user_id' => $instructorID
            ),
            'fields' => array(
                'Package.*',
                'PackagesOrder.id',
                'PackagesOrder.order',
            ),
            'limit' => $this->limit,
            'order' => array('PackagesOrder.order' => 'ASC'),
            'recursive' => -1,
            'joins' => array(
                array(
                    'table' => 'packages',
                    'type' => 'LEFT',
                    'alias' => 'Package',
                    'conditions' => array('Package.id = PackagesOrder.package_id')
                ),
            ),
        );

        $packages = $this->Paginator->paginate('PackagesOrder');
        $this->set('packages', $packages);
        $this->set('selectedPackagesIds', $this->Session->read('packageIds'));
    }

    /**
     * @param $packageUuid
     * @throws BadRequestException
     */
    public function student_view($packageUuid)
    {
        $this->set('title_for_layout', 'Item Details - '.$this->Utilities->applicationName);
        $packageDetails = $this->Package->getPackageByUuid($packageUuid);
        $this->set('package', $packageDetails);
        $this->set('selectedPackagesIds', $this->Session->read('packageIds'));

        if(empty($packageDetails))
        {
            throw new BadRequestException;
        }
    }
    /**************************************************************************************************************/
    /******************************************       Custom Function          ************************************/
    /**************************************************************************************************************/

    public function instructor_usersPackages($studentID = null)
    {
        $this->autoLayout = false;
        $this->autoRender = false;
        $this->response->type('application/javascript');
        $usersCancelledAppointments = null;
        $usersPackages = null;
        $packages = $this->PackagesUser->getUsersPackageList($studentID);
        $appointments = $this->Appointment->getCancelledAppointment($studentID);
        $payAtFacilities = $this->Package->getPayAtFacility($studentID);
        $lessonLeft = $this->CountLesson->getUserLessonLeft($studentID);
        $packages = array_merge($packages, $payAtFacilities/*, array('booked' => 'Just Want to Booked')*/);

        if($appointments){
            $usersCancelledAppointments = $appointments;
        }

        if($packages){
            $usersPackages = $packages;
        }

        $result = array(
            'package' => $usersPackages,
            'appointments' => $usersCancelledAppointments,
            'lessonLeft' => $lessonLeft,
        );
        if($result){
            $this->response->body(json_encode($result));
        }
        else{
            $this->response->body(json_encode('error'));
        }
    }

    /**
     * @param null $packageUUID
     * @param null $action
     */
    public function student_choosePackage($packageUUID = null, $action = null)
    {
        $this->choosePackage($packageUUID, $action);
    }

    /**
     * @param null $packageUUID
     * @param null $action
     */
    public function instructor_choosePackage($packageUUID = null, $action = null)
    {
        $this->choosePackage($packageUUID, $action);
    }

    /**
     *
     */
    public function student_countPackage()
    {
        $this->autoLayout = false;
        $this->autoRender = false;
        $this->response->type('application/javascript');

        $this->response->body(json_encode(sizeof($this->Session->read('packageIds'))));
    }

    public function instructor_countPackage()
    {
        $this->autoLayout = false;
        $this->autoRender = false;
        $this->response->type('application/javascript');

        $this->response->body(json_encode(sizeof($this->Session->read('packageIds'))));
    }

    public function instructor_remove_items() {

        if($this->Session->write('packageIds', null)){
            $this->Session->setFlash('All packages has been removed', 'flash_error');
        }
        else{
            $this->Session->setFlash('Sorry, something went wrong');
        }
        $this->redirect($this->referer());
    }

    public function student_remove_items() {

        if($this->Session->write('packageIds', null)){
            $this->Session->setFlash('All package has been removed', 'flash_error');
        }
        else{
            $this->Session->setFlash('Sorry, something went wrong');
        }
        $this->redirect($this->referer());
    }

    /**
     * @param $packageID
     * @param $action
     * @return bool
     */
    private function writePackageIdsToSession($packageID, $action)
    {
        if($action == 0){
            if ($this->Session->read('packageIds')) {
                $oldIds = $this->Session->read('packageIds');
                $Ids = array_values($oldIds);
                $newPackageIds = $Ids;
                $newPackageIds[sizeof($Ids)] = $packageID;
            }
            else{
                $newPackageIds[0] = $packageID;
            }
            if($this->Session->write('packageIds', $newPackageIds)){
                return true;
            }
            return false;
        }

        if($action == 1){
            $selectedPackage = array_values($this->Session->read('packageIds'));
            $newList = array();
            $counter = 0;
            while(sizeof($selectedPackage) > $counter){
                if($selectedPackage[$counter] != $packageID){
                    $newList[] = $selectedPackage[$counter];
                }
                $counter++;
            }

            if($this->Session->write('packageIds', $newList)){
                return true;
            }
            return false;
        }
    }

    /**
     * @param $packageUUID
     * @param $action
     */
    private function choosePackage($packageUUID, $action)
    {
        if($this->request->is('ajax')){
            $this->autoLayout = false;
            $this->autoRender = false;
            $this->response->type('application/javascript');

            $packageInfo = $this->request->data['packageInfo'];
            $packageDetails = $this->Package->getPackageByUuid($packageInfo['packageUUID']);
            $packageID = $packageDetails['Package']['id'];
            $action = $packageInfo['action'];
            $this->writePackageIdsToSession($packageID, $action);

            if($action == 0){
                $this->response->body(json_encode(1));
            }

            if($action == 1){
                $this->response->body(json_encode(0));
            }

        }
        else{
            $packageDetails = $this->Package->getPackageByUuid($packageUUID);
            $packageID = $packageDetails['Package']['id'];
            $this->writePackageIdsToSession($packageID, $action);

            if($action == 0){
                $this->Session->setFlash('Item has been selected for purchases', 'flash_success');
            }

            if($action == 1){
                $this->Session->setFlash('Item has been removed form selected package list', 'flash_error');
            }

            $this->redirect($this->referer());

        }
    }

    /**
     *
     */
    public function instructor_arrangePOSOrder()
    {
        $this->autoLayout = false;
        $this->autoRender = false;
        $this->response->type('application/javascript');
        $ordersList = explode('&table[]=', $this->request->data['orders']);

        $ordersList[0] = str_replace('table[]=', '', $ordersList[0]);
        $counter = 0;
        $saveCount = 0;
        $intOrder = 1;
        foreach($ordersList as $key => $value){
            $data = explode('_', $value);
            $packageOrderID = (int) $data[1];
            $pageNo = (int) $data[0];

            if($pageNo >= 2){
                $pageNo = $pageNo - 1;
            }

            $order = ($pageNo * $this->limit) + $intOrder;
            $this->PackagesOrder->id = $packageOrderID;
            if($this->PackagesOrder->saveField('order', $order)){
                $saveCount++;
            }
            $counter ++;
            $intOrder ++;

            $orderArray[] = $order;
        }

        if($counter == $saveCount)
        {
            $this->response->body(json_encode('done'));
        }
        else{
            $this->response->body(json_encode('error'));
        }
    }

    /**
     * @param null $query
     */
    public function instructor_getPackages($query = null)
    {
        $this->autoLayout = false;
        $this->autoRender = false;
        $this->response->type('application/javascript');

        $conditions = array(
            'OR' => array(
                'Package.created_by' => 1,
                'Package.is_default' => 1
            ),
            'AND' => array(
                'OR' => array(
                    'Package.name LIKE' => '%'.$query.'%',
                    'Package.description LIKE' => '%'.$query.'%',
                    'Package.price LIKE' => '%'.$query.'%',
                )
            )
        );

        $fields = array(
            'Package.id', 'Package.name',
        );

        $options = array(
            'conditions' => $conditions,
            'fields' => $fields,
            'recursive' => -1
        );

        $result = $this->Package->find('all', $options);
        $this->response->body(json_encode($result));
    }

}
