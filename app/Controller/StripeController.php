<?php
App::uses('AppController', 'Controller');

class StripeController extends AppController
{
    
    public $name = 'StripeCallback';
    
    public function index() {
        
        
        if (isset($this->request->query['code'])) {
            $code = $this->request->query['code'];
            $token_request_body = array(
                'grant_type' => 'authorization_code',
                'client_id' => STRIPE_CLIENT_ID,
                'code' => $code,
                'client_secret' => STRIPE_SECRET
            );

            $req = curl_init(STRIPE_TOKEN_URI);
            curl_setopt($req, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($req, CURLOPT_POST, true );
            curl_setopt($req, CURLOPT_POSTFIELDS, http_build_query($token_request_body));

            // TODO: Additional error handling
            $respCode = curl_getinfo($req, CURLINFO_HTTP_CODE);
            $resp = json_decode(curl_exec($req), true);
            curl_close($req);

            if (isset($resp['access_token'])) {
                $access_token = $resp['access_token'];
                $refresh_token = $resp['refresh_token'];
                $stripe_user_id = $resp['stripe_user_id'];
                
                $user = $this->User->getUser($this->Session->read('Auth.User.uuid'));
                $user['User']['stripe_account_id'] = $stripe_user_id;
                $user['User']['stripe_account_access_token'] = $access_token;
                $user['User']['stripe_account_refresh_token'] = $refresh_token;
                
                var_dump($user);
                
                $this->User->save($user);
                
                $this->Session->write('Auth', $user); //save the updated user into the session.
                
                $this->redirectUserByRole();
            }
            
            return;
        }
        
        echo 'No stripe token returned';
        
        //
    }
}