<?php
App::uses('AppController', 'Controller');
App::uses('GoogleCharts', 'GoogleCharts.Lib');

/**
 * Class AppointmentsController
 *
 * @property Appointment $Appointment..
 * @property Attachment @Attachment
 * @property Benchmark $Benchmark
 * @property Billing $Billing
 * @property BillingsPackage @BillingsPackage
 * @property BlockTime @BlockTime
 * @property Card $Card
 * @property Config $Config
 * @property Contact $Contact
 * @property CountLesson $CountLesson
 * @property EmailsEvent $EmailsEvent
 * @property EmailsEventsSetting $EmailsEventsSetting
 * @property Event $Event
 * @property GroupLesson $GroupLesson
 * @property GroupLessonsStudents $GroupLessonsStudents
 * @property Note @Note
 * @property Package $Package
 * @property PackagesUser $PackagesUser
 * @property PackagesOrder $PackagesOrder
 * @property Profile $Profile
 * @property Source $Source
 * @property TcsUser $TcsUser
 * @property User $User
 * @property Waiting $Waiting
 *
 * @property PaginatorComponent $Paginator
 * @property EmailComponent $Email
 * @property UtilitiesComponent $Utilities
 * @property AuthorizeNetComponent $AuthorizeNet
 * @property DataTableComponent $DataTable
 * @property ImgComponent $Img
 * @property AppsComponent $Apps
 *
 */

class DashboardsController extends AppController
{
    /**
     * The name of this controller.
     *
     * @var string
     */
    public $name = 'Dashboards';


    /**************************************************************************************************************/
    /********************************************        Admin Panel          *************************************/
    /**************************************************************************************************************/

    /**
     * This is the index page of dashboard for admin panel. which will mostly using to showing the overview
     * of the current activities of this application.
     */
    public function admin_index()
    {

    }


    /**************************************************************************************************************/
    /******************************************       Instructor Panel         ************************************/
    /**************************************************************************************************************/

    /**
     * This is the index page of dashboard for instructor. which will mostly using to showing the overview
     * of the current activities of this application.
     */
    public function instructor_index()
    {
        $this->set('title_for_layout', 'My Dashboard - '.$this->Utilities->applicationName);
        $contactOption = array('conditions' => array('Contact.user_id' => $this->userID));

        $this->set('contactsOverview', $this->Contact->getContactOverview($contactOption));
        $this->set('countStudent', $this->Profile->getStudentForInstructor('count', $this->userID));

        $this->set(
            'countPOS',
            $this->PackagesUser->getPOS(
                array('query' => 'count', 'conditions' => array('PackagesUser.instructor_id' => $this->userID))
            )
        );
        $this->set(
            'countBenchmark',
            $this->Benchmark->getBenchmarks(
                array('query' => 'count', 'conditions' => array('Benchmark.created_by' => $this->userID))
            )
        );

        $this->bestStudentChart(array('conditions' => array('Profile.instructor_id' => $this->userID)));
        $this->bestPackagesChart(array('conditions' => array('PackagesUser.instructor_id' => $this->userID)));

        $this->set(
            'upComingAppointments',
            $this->Appointment->getAppointments(
                array(
                    'conditions' => array('Appointment.instructor_id' => $this->userID, 'Appointment.status' => 2),
                    'limit' => 5
                )
            )
        );

        $from = date("Y-m-d H:i:s", strtotime('monday this week'));
        $to = date("Y-m-d H:i:s", strtotime('sunday this week'));
        $cancelledAppointments = $this->Appointment->getAppointments(
            array(
                'conditions' => array('Appointment.instructor_id' => $this->userID, 'Appointment.status' => 3, 'Appointment.start >' => $from, 'Appointment.start <' => $to),
                'limit' => 5
            )
        );
        $this->set('cancelledAppointments', $cancelledAppointments);


        $followUpConditions = array('limit' => 5, 'conditions' => array('DAY(Note.note_date) >' => date('d'), 'created_by' => $this->userID));
        $followUp = $this->Note->getFollowUp($followUpConditions);
        $this->set('followUp', $followUp);


        $slippingAwayConditions = array('limit' => 5, 'conditions' => array('User.is_slipping_away' => 1, 'Profile.instructor_id ' => $this->userID));
        $slippingAway = $this->User->getSlippingAway($slippingAwayConditions);
        $this->set('slippingAway', $slippingAway);

        $waitingListConditions = array('limit' => 5, 'conditions' => array('Waiting.instructor_id ' => $this->userID));
        $waitingList = $this->Waiting->getWaitingList($waitingListConditions);
        $this->set('waitingList', $waitingList);

        $studentIds = $this->Profile->getStudentIdsByInstructorID($this->userID);
        $this->set(
            'recentTransaction',
            $this->Billing->geBilling(array('conditions' => array('Billing.user_id' => $studentIds), 'limit' => 5))
        );
    }


    /**************************************************************************************************************/
    /******************************************       Training Center Panel         ************************************/
    /**************************************************************************************************************/
    /**
     * This is the index page of dashboard for TC. which will mostly using to showing the overview
     * of the current activities of this application.
     */

    public function tc_index()
    {
        $instructorIDs = $this->TcsUser->getIntructorIdsForTC($this->userID);

        $this->set('title_for_layout', 'My Dashboard - '.$this->Utilities->applicationName);



        $contactOption = array('conditions' => array('Contact.user_id' => $instructorIDs));

        $this->set('contactsOverview', $this->Contact->getContactOverview($contactOption));
        /**
         * For Total instructor
         */
        $this->set(
            'totalInstructor',
            sizeof($this->TcsUser->getInstructorIdsByTCID($this->userID))
        );
        /**
         * For total student
         */
        $this->set(
            'totalStudent',
            $this->Profile->getStudentForInstructor('count', $instructorIDs)
        );

        /**
         * For upcoming Appointments
         */
        $this->set(
            'upComingAppointments',
            $this->Appointment->getAppointments(
                array(
                    'conditions' => array('Appointment.instructor_id' => $instructorIDs, 'Appointment.status' => 2),
                    'limit' => 5
                )
            ));
        /**
         * For Upcoming follow up
         */
        $followUpConditions = array('limit' => 5, 'conditions' => array('DAY(Note.note_date) >' => date('d'), 'created_by'=>$instructorIDs));
        $followUp = $this->Note->getFollowUp($followUpConditions);
        $this->set('followUp', $followUp);
        /**
         * For student slipping away
         */
        $slippingAwayConditions = array('limit' => 5, 'conditions' => array('User.is_slipping_away' => 1, 'Profile.instructor_id'=>$instructorIDs));
        $slippingAway = $this->User->getSlippingAway($slippingAwayConditions);
        $this->set('slippingAway', $slippingAway);

        /**
         * For waiting list
         */
        $waitingListConditions = array('limit' => 5, 'conditions' => array('Waiting.instructor_id'=>$instructorIDs));
        $waitingList = $this->Waiting->getWaitingList($waitingListConditions);
        $this->set('waitingList', $waitingList);
        /**
         * For recent transaction
         */
        $studentIds = $this->Profile->getStudentIdsByInstructorID($instructorIDs);
        $this->set(
            'recentTransaction',
            $this->Billing->geBilling(array('conditions' => array('Billing.user_id' => $studentIds), 'limit' => 5))
        );
        /**
         * For cancelled appointment
         */
        $from = date("Y-m-d H:i:s", strtotime('monday this week'));
        $to = date("Y-m-d H:i:s", strtotime('sunday this week'));
        $cancelledAppointments = $this->Appointment->getAppointments(
            array(
                'conditions' => array('Appointment.instructor_id' => $instructorIDs, 'Appointment.status' => 3, 'Appointment.start >' => $from, 'Appointment.start <' => $to),
                'limit' => 5
            )
        );
        $this->set('cancelledAppointments', $cancelledAppointments);
        /**
         * Best charts
         */

        $this->bestStudentChart(array('conditions' => array('Profile.instructor_id' => $instructorIDs)));
        $this->bestPackagesChart(array('conditions' => array('PackagesUser.instructor_id' => $instructorIDs)));

    }


    /**************************************************************************************************************/
    /******************************************         Student Panel         *************************************/
    /**************************************************************************************************************/

    /**
     * This is the index page of dashboard for student panel. which will mostly using to showing the overview
     * of the current activities of this application.
     */
    public function student_index()
    {
        $this->set('title_for_layout', 'My Dashboard - '.$this->Utilities->applicationName);
        $this->set(
            'purchaseOverview',
            $this->PackagesUser->getPurchaseAmount(
                array('conditions' => array('PackagesUser.created_for' => $this->userID))
            )
        );

        $this->set(
            'countRunningPOS',
            $this->PackagesUser->getPOS(
                array(
                    'query' => 'count',
                    'conditions' => array('PackagesUser.created_for' => $this->userID, 'PackagesUser.status' => 1)
                )
            )
        );

        $this->set(
            'totalPurchasePackages',
            $this->PackagesUser->getPOS(
                array('query' => 'count', 'conditions' => array('PackagesUser.created_for' => $this->userID))
            )
        );

        $this->set(
            'countBenchmark',
            $this->Benchmark->getBenchmarks(
                array('query' => 'count', 'conditions' => array('Benchmark.created_for' => $this->userID))
            )
        );

        $this->set(
            'upComingAppointments',
            $this->Appointment->getAppointments(
                array(
                    'conditions' => array('Appointment.appointment_by' => $this->userID, 'Appointment.status' => 2),
                    'limit' => 5
                )
            )
        );

        $this->set(
            'recentTransaction',
            $this->Billing->geBilling(array('conditions' => array('Billing.user_id' => $this->userID), 'limit' => 5))
        );

        $this->set(
            'recentBenchmarks',
            $this->Benchmark->geBenchmark(
                array('conditions' => array('Benchmark.created_for' => $this->userID), 'limit' => 5)
            )
        );


        $feelingsCount = $this->Feedback->find('count' , array(
            'conditions' => array(
                'user_id' => $this->userID
            )
        ));
        $this->set('feelingsCount' , $feelingsCount);
    }

    public function userFeedback(){

        if ($this->request->is('post')) {

            $data = $this->request->data;
            if($data['Feedback']['rating'] == ''){
                $data['Feedback']['rating'] = 0;
            }

            if(isset($data['Feedback']['no_thanks'])){
                $data['Feedback']['feedback'] = 'This user not interest to give his feedback';
                $data['Feedback']['is_feedback'] = 0;
                $data['Feedback']['rating'] = 0;
            }

            if ($this->Feedback->save($data)) {
                if(isset($data['Feedback']['no_thanks'])) {
                  //  $this->Session->setFlash('Ok , No problem', 'flash_success');
                }else{
                    $this->Session->setFlash('Thank you for your feedback', 'flash_success');
                }
            } else {
                $this->Session->setFlash('Sorry, something went wrong', 'flash_error');
            }
            $this->redirect($this->referer());
        }

    }

    /**************************************************************************************************************/
    /******************************************       Custom Function          ************************************/
    /**************************************************************************************************************/

    /**
     * @param array $options
     */
    public function pieChart($options = array())
    {
        $contacts = $this->Contact->getContactOverview($options);
        $chart = new GoogleCharts();
        $chart->type("PieChart");
        $chart->div('chart_div');
        $chart->options(
            array(
                'series' => array(0 => array('color' => 'lightslategray')),
                'title' => ' ',
                'pieHole' => 0.4,
                'width' => 'auto',
                'fontSize' => 10,
                'height' => 270,
                'hAxis' => array('title' => 'Contact'),
                'vAxis' => array('title' => 'Type')
            )
        );
        $chart->columns(
            array(
                'contact' => array(
                    'type' => 'string',
                    'label' => 'Contact Type'
                ),
                'Percent' => array(
                    'type' => 'number',
                    'label' => 'Percent'
                )
            )
        );

        $chart->addRow(
            array(
                'contact' => 'Student' . ' (' . $contacts[0][0]['student'] . ')',
                'Percent' => $contacts[0][0]['student']
            )
        );
        $chart->addRow(
            array(
                'contact' => 'Customer' . ' (' . $contacts[0][0]['customer'] . ')',
                'Percent' => $contacts[0][0]['customer']
            )
        );
        $chart->addRow(
            array(
                'contact' => 'Lead/Prospect' . ' (' . $contacts[0][0]['lead'] . ')',
                'Percent' => $contacts[0][0]['lead']
            )
        );

        if($contacts[0][0]['totalContact'] <= 0){
            $chart = null;
        }

        $this->set(compact('chart'));
    }

    /**
     * @param $options
     */
    public function bestStudentChart($options)
    {
        $bestStudents = $this->PackagesUser->bestStudent($options);

        $bestStudentsChart = new GoogleCharts();
        $bestStudentsChart->type("ColumnChart");
        $bestStudentsChart->div('best_student');
        $bestStudentsChart->options(
            array(
                'series' => array(0 => array('color' => '#526075')),
                'title' => ' ',
                'width' => 'auto',
                'height' => 400,
                'hAxis' => array('title' => 'Students Name'),
                'vAxis' => array('title' => 'Package Amount')
            )
        );
        $bestStudentsChart->columns(
            array(
                'name' => array(
                    'type' => 'string',
                    'label' => 'Student Name'
                ),
                'package' => array(
                    'type' => 'number',
                    'prefix' => '$',
                    'label' => 'Total Sales'
                )
            )
        );

        if($bestStudents){
            foreach ($bestStudents as $student) {
                $bestStudentsChart->addRow(
                    array(
                        'name' => $student[0]['studentName'] . '(' . $student[0]['PackagePurchases'] . ' package)',
                        'package' => $student[0]['PackagePrice']
                    )
                );
            }
        }
        else{
            $bestStudentsChart = null;
        }


        $this->set(compact('bestStudentsChart'));
    }

    /**
     * @param $options
     */
    public function bestPackagesChart($options)
    {
        $bestPackages = $this->PackagesUser->bestPackages($options);
        $bestPackagesChart = new GoogleCharts();
        $bestPackagesChart->type("ColumnChart");
        $bestPackagesChart->div('best_packages');
        $bestPackagesChart->options(
            array(
                'series' => array(0 => array('color' => '#526075')),
                'title' => ' ',
                'width' => 'auto',
                'height' => 400,
                'hAxis' => array('title' => 'Name of Packages'),
                'vAxis' => array('title' => 'Total Purchases')
            )
        );
        $bestPackagesChart->columns(
            array(
                'name' => array(
                    'type' => 'string',
                    'label' => 'Package Name'
                ),
                'purchases' => array(
                    'type' => 'number',
                    'prefix' => '$',
                    'label' => 'Purchases'
                )
            )
        );

        if($bestPackages){
            foreach ($bestPackages as $package) {
                $bestPackagesChart->addRow(
                    array(
                        'name' => $package['Package']['name'],
                        'purchases' => $package[0]['PackagePurchases'],
                    )
                );
            }
        }
        else{
            $bestPackagesChart = null;
        }

        $this->set(compact('bestPackagesChart'));
    }
}
