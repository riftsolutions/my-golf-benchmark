<?php
App::uses('AppController', 'Controller');

/**
 * Class AppointmentsController
 *
 * @property Appointment $Appointment..
 * @property Attachment @Attachment
 * @property Benchmark $Benchmark
 * @property Billing $Billing
 * @property BillingsPackage @BillingsPackage
 * @property BlockTime @BlockTime
 * @property Card $Card
 * @property Config $Config
 * @property Contact $Contact
 * @property CountLesson $CountLesson
 * @property EmailsEvent $EmailsEvent
 * @property EmailsEventsSetting $EmailsEventsSetting
 * @property Event $Event
 * @property GroupLesson $GroupLesson
 * @property GroupLessonsStudents $GroupLessonsStudents
 * @property Note @Note
 * @property Package $Package
 * @property PackagesUser $PackagesUser
 * @property PackagesOrder $PackagesOrder
 * @property Profile $Profile
 * @property Source $Source
 * @property TcsUser $TcsUser
 * @property User $User
 * @property Waiting $Waiting
 *
 * @property PaginatorComponent $Paginator
 * @property EmailComponent $Email
 * @property UtilitiesComponent $Utilities
 * @property AuthorizeNetComponent $AuthorizeNet
 * @property DataTableComponent $DataTable
 * @property ImgComponent $Img
 * @property AppsComponent $Apps
 *
 */
class LessonsController extends AppController
{
    /**
     * The name of this controller.
     *
     * @var string
     */
    public $name = 'Lessons';

    /**************************************************************************************************************/
    /******************************************       Instructor Panel         ************************************/
    /**************************************************************************************************************/

    /**
     *
     */
    public function instructor_index()
    {
        $this->redirect(array('controller' => 'lessons', 'action' => 'group'));
    }

    /**
     *
     */
    public function instructor_group()
    {
        $conditions = array('GroupLesson.user_id' => $this->userID);
        $this->Paginator->settings = array(
            'conditions' => $conditions,
            'recursive' => -1,
            'limit' => $this->limit,
            'order' => 'GroupLesson.created DESC',
            'fields' => array('GroupLesson.id, GroupLesson.title,  GroupLesson.description, GroupLesson.price, GroupLesson.start, GroupLesson.end, GroupLesson.student_limit, GroupLesson.filled_up_student, GroupLesson.status'),
        );

        $lessons = $this->Paginator->paginate('GroupLesson');
        $this->set('lessons', $lessons);
        $this->set('title_for_layout', 'My Group Lesson - ' . $this->Utilities->applicationName);
    }

    public function tc_index()
    {
        $this->redirect(array('controller' => 'lessons', 'action' => 'group'));
    }

    /**
     *
     */
    public function tc_group()
    {
        $instructorID = $this->TcsUser->getIntructorIdsForTC($this->userID);
        $conditions = array('GroupLesson.user_id' => $instructorID);

        $instructorDetails = $this->User->find('all' , array(
            'fields' => array(
                'CONCAT(Instructor.first_name, " ", Instructor.last_name) as instructorName',
                'User.id'
            ),
            'conditions' => array('User.id' => $instructorID),
            'joins' =>array(
                array(
                    'table' => 'profiles',
                    'type' => 'LEFT',
                    'alias' => 'Instructor',
                    'conditions' => array('Instructor.user_id = User.id')
                )

            )
        ));

        if (isset($this->request->query['instructor']) && $this->request->query['instructor']) {
            $instructorID = $this->request->query['instructor'];
            if($instructorID[0] == ''){
                unset($instructorID[0]);
            }
            if (!empty($instructorID)) {
                $conditions = array_merge($conditions, array('GroupLesson.user_id' => $instructorID));
            }
        }

        if (isset($this->request->query['lesson_name']) && $this->request->query['lesson_name'] && $this->request->query['lesson_name'] != '') {
            $lessonName = $this->request->query['lesson_name'];
            if (isset($lessonName) && $lessonName) {
                $conditions = array_merge($conditions, array('GroupLesson.title Like' => '%'.$lessonName.'%'));
            }
        }

        if (isset($this->request->query['price']) && $this->request->query['price'] && $this->request->query['price'] != 'Choose Price') {
            $amount = $this->request->query['price'];
            $conditions = array_merge($conditions, array('GroupLesson.price  >=' => $amount));
        }

        if (isset($this->request->query['starting_amount']) && $this->request->query['starting_amount']) {
            $startingAmount = $this->request->query['starting_amount'];
            $conditions = array_merge($conditions, array('GroupLesson.price >=' => $startingAmount));
        }

        if (isset($this->request->query['ending_amount']) && $this->request->query['ending_amount']) {
            $endingAmount = $this->request->query['ending_amount'];
            $conditions = array_merge($conditions, array('GroupLesson.price <=' => $endingAmount));
        }

        if (isset($this->request->query['date']) && $this->request->query['date'] && $this->request->query['date'] != 'Choose Date') {
            $data = $this->request->query['date'];
            if ($data == 'last_month') {
                $month_ini = new DateTime("first day of last month");
                $month_end = new DateTime("last day of last month");
                $dataFrom = $month_ini->format('Y-m-d') . ' 00:00:00';
                $dataTo = $month_end->format('Y-m-d') . ' 23:59:59';
            }

            if ($data == 'current_month') {
                $dataFrom = date('Y-m-01 00:00:00', strtotime('this month'));
                $dataTo = date('Y-m-t 12:59:59', strtotime('this month'));
            }

            if ($data == 'last_3_month') {
                $dataFrom = date("Y-m-d", strtotime("-3 Months")) . ' 00:00:00';;
                $dataTo = date("Y-m-d H:i:s");
            }

            if ($data == 'last_6_month') {
                $dataFrom = date("Y-m-d", strtotime("-6 Months")) . ' 00:00:00';;
                $dataTo = date("Y-m-d H:i:s");
            }

            if ($data == 'last_12_month') {
                $dataFrom = date("Y-m-d", strtotime("-12 Months")) . ' 00:00:00';;
                $dataTo = date("Y-m-d H:i:s");
            }

            $conditions = array_merge($conditions,
                array('GroupLesson.start >=' => $dataFrom, 'GroupLesson.start <=' => $dataTo));
        }

        if (isset($this->request->query['starting']) && $this->request->query['starting']) {
            $starting = date("Y-m-d H:i:s", strtotime($this->request->query['starting']));
            $conditions = array_merge($conditions, array('GroupLesson.start >=' => $starting));
        }

        if (isset($this->request->query['ending']) && $this->request->query['ending']) {
            $ending = date("Y-m-d H:i:s", strtotime($this->request->query['ending']));
            $conditions = array_merge($conditions, array('GroupLesson.end <=' => $ending));
        }


        if (isset($this->request->query['student_limit']) && $this->request->query['student_limit'] && $this->request->query['student_limit'] != 'Choose Price') {
            $studentLimit = $this->request->query['student_limit'];
            $conditions = array_merge($conditions, array('GroupLesson.student_limit  >=' => $studentLimit));
        }

        //var_dump($conditions); die();
        $this->Paginator->settings = array(
            'conditions' => $conditions,
            'recursive' => -1,
            'limit' => $this->limit,
            'order' => 'GroupLesson.created DESC',
            'fields' => array(
                'GroupLesson.id,
                GroupLesson.title,
                GroupLesson.description,
                GroupLesson.price,
                GroupLesson.start,
                GroupLesson.end,
                GroupLesson.student_limit,
                GroupLesson.filled_up_student,
                GroupLesson.status',
                'CONCAT(Instructor.first_name, " ", Instructor.last_name) as instructorName',
                'InstructorUser.uuid'

            ),
            'joins' => array(
                array(
                    'table' => 'profiles',
                    'type' => 'LEFT',
                    'alias' => 'Instructor',
                    'conditions' => array('Instructor.user_id = GroupLesson.user_id'),
                ),
                array(
                    'table' => 'users',
                    'type' => 'LEFT',
                    'alias' => 'InstructorUser',
                    'conditions' => array('InstructorUser.id = GroupLesson.user_id'),
                )
            )

        );

        $lessons = $this->Paginator->paginate('GroupLesson');
        $this->set('lessons', $lessons);
        $this->set('instructors' , $instructorDetails);
        $this->set('title_for_layout', 'My Group Lesson - ' . $this->Utilities->applicationName);
    }

    /**
     * @param $groupLessonID
     */
    public function instructor_details($groupLessonID)
    {
        $result = $this->GroupLesson->find('first',
            array(
                'conditions' => array('GroupLesson.id' => $groupLessonID),
                'recursive' => -1,
                'contain' => array(
                    'GroupLessonsStudents' => array(
                        'fields' => array('id', 'user_id', 'payment_status'),
                        'User' => array(
                            'fields' => array('username', 'id', 'uuid'),
                            'Profile' => array(
                                'fields' => array('first_name', 'last_name', 'profile_pic_url', 'phone')
                            )
                        ),
                    )
                ),

            )
        );
        $this->set('groupLessonDetails', $result);
        $this->set('title_for_layout', 'Group Lesson Details - ' . $this->Utilities->applicationName);
    }

    public function tc_details($groupLessonID)
    {
        $result = $this->GroupLesson->find('first',
            array(
                'fields' => array(
                    'CONCAT(Instructor.first_name, " ", Instructor.last_name) as instructorName',
                    'GroupLesson.title',
                    'GroupLesson.description',
                    'GroupLesson.start',
                    'GroupLesson.end',
                    'GroupLesson.price',
                    'GroupLesson.student_limit',
                    'GroupLesson.filled_up_student',
                    'GroupLesson.status',
                    'InstructorUser.uuid'
                ),
                'conditions' => array('GroupLesson.id' => $groupLessonID),
                'recursive' => -1,
                'contain' => array(
                    'GroupLessonsStudents' => array(
                        'fields' => array('id', 'user_id', 'payment_status'),
                        'User' => array(
                            'fields' => array('username', 'id', 'uuid'),
                            'Profile' => array(
                                'fields' => array('first_name', 'last_name', 'profile_pic_url', 'phone')
                            )
                        )
                    )
                ),
                'joins' => array(
                    array(
                        'table' => 'profiles',
                        'type' => 'LEFT',
                        'alias' => 'Instructor',
                        'conditions' => array('Instructor.user_id = GroupLesson.user_id'),
                    ),
                    array(
                        'table' => 'users',
                        'type' => 'LEFT',
                        'alias' => 'InstructorUser',
                        'conditions' => array('InstructorUser.id = GroupLesson.user_id'),
                    )
                )
            ));
        $this->set('groupLessonDetails', $result);
        $this->set('title_for_layout', 'Group Lesson Details - ' . $this->Utilities->applicationName);
    }

    public function instructor_payment($groupLessonID, $status)
    {
        $this->GroupLessonsStudents->id = $groupLessonID;
        if ($this->GroupLessonsStudents->saveField('payment_status', $status)) {
            if ($status == 1) {
                $this->Session->setFlash('This student marked as paid successfully', 'flash_success');
            } elseif ($status == 2) {
                $this->Session->setFlash('This student marked as unpaid', 'flash_error');
            }
            $this->redirect($this->referer());
        }
    }
    /**************************************************************************************************************/
    /******************************************       Student Panel         ************************************/
    /**************************************************************************************************************/

    /**
     *
     */
    public function student_index()
    {
        $this->redirect(array('controller' => 'lessons', 'action' => 'group'));
    }

    /**
     *
     */
    public function student_group()
    {
        $conditions = array('GroupLessonsStudents.user_id' => $this->userID);
        $this->Paginator->settings = array(
            'conditions' => $conditions,
            'recursive' => -1,
            'limit' => $this->limit,
            'order' => 'GroupLesson.created DESC',
            'fields' => array('GroupLesson.id, GroupLesson.title,  GroupLesson.description, GroupLesson.price, GroupLesson.start, GroupLesson.end, GroupLesson.student_limit, GroupLesson.filled_up_student, GroupLesson.status'),
            'joins' => array(
                array(
                    'table' => 'group_lessons_students',
                    'type' => 'LEFT',
                    'alias' => 'GroupLessonsStudents',
                    'conditions' => array('GroupLessonsStudents.group_lesson_id = GroupLesson.id')
                ),
            )
        );

        $lessons = $this->Paginator->paginate('GroupLesson');
        $this->set('lessons', $lessons);
        $this->set('title_for_layout', 'My Group Lesson - ' . $this->Utilities->applicationName);
    }

    /**
     * @param $groupLessonID
     */
    public function student_details($groupLessonID)
    {
        $result = $this->GroupLesson->find('first',
            array(
                'conditions' => array('GroupLesson.id' => $groupLessonID),
                'recursive' => -1
            )
        );
        $this->set('groupLessonDetails', $result);
        $this->set('title_for_layout', 'Group Lesson Details - ' . $this->Utilities->applicationName);
    }

    /**
     *
     */
    public function instructor_getGLByID()
    {
        $this->autoLayout = false;
        $this->autoRender = false;
        $this->response->type('application/javascript');

        $groupID = $this->request->data['groupID'];

        $gl = $this->GroupLesson->find('first', array('conditions' => array('GroupLesson.id' => $groupID)));

        if ($gl) {
            $this->response->body(json_encode($gl['GroupLesson']));
        } else {
            $this->response->body(json_encode(null));
        }
    }
}
