<?php
App::uses('AppController', 'Controller');
/**
 * Class AppointmentsController
 *
 * @property Appointment $Appointment..
 * @property Attachment @Attachment
 * @property Benchmark $Benchmark
 * @property Billing $Billing
 * @property BillingsPackage @BillingsPackage
 * @property BlockTime @BlockTime
 * @property Card $Card
 * @property Config $Config
 * @property Contact $Contact
 * @property CountLesson $CountLesson
 * @property EmailsEvent $EmailsEvent
 * @property EmailsEventsSetting $EmailsEventsSetting
 * @property Event $Event
 * @property GroupLesson $GroupLesson
 * @property GroupLessonsStudents $GroupLessonsStudents
 * @property Note @Note
 * @property Package $Package
 * @property PackagesUser $PackagesUser
 * @property PackagesOrder $PackagesOrder
 * @property Profile $Profile
 * @property Source $Source
 * @property TcsUser $TcsUser
 * @property User $User
 * @property Waiting $Waiting
 *
 * @property PaginatorComponent $Paginator
 * @property EmailComponent $Email
 * @property UtilitiesComponent $Utilities
 * @property AuthorizeNetComponent $AuthorizeNet
 * @property DataTableComponent $DataTable
 * @property ImgComponent $Img
 * @property AppsComponent $Apps
 *
 */

class ProfilesController extends AppController
{
    /**
     * The name of Profile Controller.
     *
     * @var string
     */
    public $name = 'Profiles';


    /**************************************************************************************************************/
    /********************************************        Admin Panel          *************************************/
    /**************************************************************************************************************/

    /**
     * Profile page for the admin user.
     */
    public function admin_index()
    {
        $this->set('title_for_layout', 'My Profile - '.$this->Utilities->applicationName);
    }

    /**
     * Update profile page for the admin
     */
    public function admin_update()
    {
        $this->updateProfile();
    }

    /**
     * Profile settings page for the admin.
     */
    public function admin_settings()
    {

    }

    /**
     * Change profile photo for the admin.
     */
    public function admin_change_photo()
    {
        $this->changeProfilePhoto();
    }

    /**
     * Change password for the admin
     */
    public function admin_change_password()
    {
        $this->changePassword();
    }

    /**************************************************************************************************************/
    /******************************************       Instructor Panel         ************************************/
    /**************************************************************************************************************/

    /**
     * Profile page for the instructor.
     */
    public function instructor_index()
    {
        $this->set('title_for_layout', 'My Profile - '.$this->Utilities->applicationName);
    }

    /**
     * Update profile page for the student
     */
    public function instructor_update()
    {
        $this->updateProfile();
    }

    /**
     * Profile settings page for the instructor.
     */
    public function instructor_settings()
    {
        $this->set('title_for_layout', 'Profile Settings - '.$this->Utilities->applicationName);
    }

    /**
     * Change profile photo for the instructor.
     */
    public function instructor_change_photo()
    {
        $this->changeProfilePhoto();
    }

    /**
     * Change password for the instructor
     */
    public function instructor_change_password()
    {
        $this->changePassword();
    }
    
    public function instructor_unlink_stripe() {
        if($this->request->is('post')){
            $user = $this->User->getUser($this->userID);
            $myStripeAccountID = $user['User']['stripe_account_id'];
            
            if ($myStripeAccountID) {
                try {
                    $account = \Stripe\Account::retrieve($myStripeAccountID);
                    $account->delete();
                } catch (Exception $e) {
                    
                }
                
                $user['User']['stripe_account_id'] = null;
                $user['User']['stripe_account_access_token'] = null;
                $user['User']['stripe_account_refresh_token'] = null;
                
                $this->User->save($user);
                $this->Session->write('Auth', $user);
            }
        }
        $this->redirect($this->referer());
    }

    /**************************************************************************************************************/
    /******************************************       Training Center Panel         ************************************/
    /**************************************************************************************************************/

    /**
     * Profile page for the instructor.
     */
    public function tc_index()
    {
        $this->set('title_for_layout', 'My Profile - '.$this->Utilities->applicationName);
    }

    /**
     * Update profile page for the student
     */
    public function tc_update()
    {
        $this->updateProfile();
    }

    /**
     * Profile settings page for the instructor.
     */
    public function tc_settings()
    {
        $this->set('title_for_layout', 'Profile Settings - '.$this->Utilities->applicationName);
    }

    /**
     * Change profile photo for the instructor.
     */
    public function tc_change_photo()
    {
        $this->changeProfilePhoto();
    }

    /**
     * Change password for the instructor
     */
    public function tc_change_password()
    {
        $this->changePassword();
    }

    /**************************************************************************************************************/
    /******************************************         Student Panel         *************************************/
    /**************************************************************************************************************/

    /**
     * Profile page for the student.
     */
    public function student_index()
    {
        $this->set('title_for_layout', 'My Profile - '.$this->Utilities->applicationName);
    }

    /**
     * Update profile page for the student.
     */
    public function student_update()
    {
        $this->updateProfile();
    }

    /**
     * Profile setting page for the student.
     */
    public function student_settings()
    {
        $this->set('title_for_layout', 'Profile Settings - '.$this->Utilities->applicationName);
        $events = $this->EmailsEventsSetting->getEmailSettingByUserID($this->userID);
        $this->set('events', $events);
    }

    /**
     * Change profile picture for the student.
     */
    public function student_change_photo()
    {
        $this->changeProfilePhoto();
    }

    /**
     * Change password for the student.
     */
    public function student_change_password()
    {
        $this->changePassword();
    }

    /**************************************************************************************************************/
    /******************************************       Custom Function          ************************************/
    /**************************************************************************************************************/

    /**
     * This function for update/edit profile information od login user.
     */
    private function updateProfile()
    {
        $this->set('title_for_layout', 'Update Profile - '.$this->Utilities->applicationName);
        $userInfo = $this->Profile->getProfile('first', array('user_id' => $this->userID));
        $this->set('userInfo', $userInfo);
        if($this->request->is('post')){
            $this->Profile->id = $userInfo['Profile']['id'];
            if($this->Profile->save($this->request->data)){
                $this->Session->setFlash('Your profile has been updated', 'flash_success');
                $this->redirect(array('controller' => 'profiles', 'action' => 'index'));
            }
            else{
                $this->Session->setFlash('Sorry, something went wrong', 'flash_error');
            }
        }
    }

    /**
     * This function for change the password of lgoin user.
     */
    private function changePassword()
    {
        $this->set('title_for_layout', 'Change Password - '.$this->Utilities->applicationName);
        if($this->request->is('post')){
            $this->User->set($this->request->data);
            if($this->User->changePassValidate()){
                if($this->User->changePassword($this->userID, $this->request->data['User']['password'])){
                    $this->Session->setFlash('Your password has been changed', 'flash_success');
                }
            }
            else{
                $this->Session->setFlash('Sorry, something went wrong', 'flash_error');
            }
        }
    }

    /**
     * This function fot change the profile photo of login user.
     */
    private function changeProfilePhoto()
    {
        $this->set('title_for_layout', 'Change Profile Photo - '.$this->Utilities->applicationName);

        if($this->request->is('post')){
            $this->Profile->set($this->request->data);

            $isValid = $this->Profile->validateProfilePhoto();
            $errors = $this->Profile->validationErrors;

            if($isValid){
                $isUploaded = $this->uploadImage($this->Session->read('Auth.User.uuid'), $this->data['Profile']['profile_pic_url'], 'img/profiles');
                if($isUploaded){
                    $this->Profile->id = $this->Session->read('Auth.User.Profile.id');
                    if($this->Profile->saveField('profile_pic_url', $isUploaded)){
                        $this->Session->write('Auth.User.Profile.profile_pic_url', $isUploaded);
                        $this->Session->setFlash('Your profile picture has been updated', 'flash_success');
                    }
                }
            }
            else{
                if(isset($errors['profile_pic_url'][0]) && $errors['profile_pic_url'][0])
                {
                    $this->Session->setFlash($errors['profile_pic_url'][0], 'flash_error');
                }
                else{
                    $this->Session->setFlash('Sorry, something went wrong', 'flash_error');
                }
            }
        }

        $this->redirect($this->referer());
    }

    /**
     * @param $lessonUUID
     * @return mixed
     * @throws NotFoundException
     */
    public function fetchLessonByUUID($lessonUUID)
    {
        $lessonDetails = $this->Lesson->findByuuid($lessonUUID);
        if(empty($lessonDetails)){
            throw new NotFoundException;
        }
        $this->set('lessonDetails', $lessonDetails);
        return $lessonDetails;
    }

    /**
     * @param $userUuid
     */
    public function instructor_editProfile($userUuid)
    {
        if($this->request->is('post')){
            $userID = $this->User->getUserIdByUuid($userUuid);
            $this->Profile->id = $userID;
            if($this->Profile->save($this->request->data)){
                $this->Session->setFlash('Information has been updated', 'flash_success');
            }
            else{
                $this->Session->setFlash('Sorry, something went wrong', 'flash_error');
            }
        }
        $this->redirect($this->referer());
    }

    /**
     * @param $userUuid
     */
    public function student_editProfile($userUuid)
    {
        if($this->request->is('post')){
            $userID = $this->User->getUserIdByUuid($userUuid);
            $this->Profile->id = $userID;
            if($this->Profile->save($this->request->data)){
                $this->Session->setFlash('Information has been updated', 'flash_success');
            }
            else{
                $this->Session->setFlash('Sorry, something went wrong', 'flash_error');
            }
        }
        $this->redirect($this->referer());
    }
}
