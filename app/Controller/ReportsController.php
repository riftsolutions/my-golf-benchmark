<?php
App::uses('AppController', 'Controller');
App::uses('GoogleCharts', 'GoogleCharts.Lib');

/**
 * Class AppointmentsController
 *
 * @property Appointment $Appointment..
 * @property Attachment @Attachment
 * @property Benchmark $Benchmark
 * @property Billing $Billing
 * @property BillingsPackage @BillingsPackage
 * @property BlockTime @BlockTime
 * @property Card $Card
 * @property Config $Config
 * @property Contact $Contact
 * @property CountLesson $CountLesson
 * @property EmailsEvent $EmailsEvent
 * @property EmailsEventsSetting $EmailsEventsSetting
 * @property Event $Event
 * @property GroupLesson $GroupLesson
 * @property GroupLessonsStudents $GroupLessonsStudents
 * @property Note @Note
 * @property Package $Package
 * @property PackagesUser $PackagesUser
 * @property PackagesOrder $PackagesOrder
 * @property Profile $Profile
 * @property Source $Source
 * @property TcsUser $TcsUser
 * @property User $User
 * @property Waiting $Waiting
 *
 * @property PaginatorComponent $Paginator
 * @property EmailComponent $Email
 * @property UtilitiesComponent $Utilities
 * @property AuthorizeNetComponent $AuthorizeNet
 * @property DataTableComponent $DataTable
 * @property ImgComponent $Img
 * @property AppsComponent $Apps
 *
 */
class ReportsController extends AppController
{
    /**
     * The name of this controller.
     *
     * @var string
     */
    public $name = 'Reports';

    /**************************************************************************************************************/
    /********************************************        Admin Panel          *************************************/
    /**************************************************************************************************************/

    /**
     *
     */
    public function admin_index()
    {

    }

    /**************************************************************************************************************/
    /******************************************       Instructor Panel         ************************************/
    /**************************************************************************************************************/

    /**
     *
     */
    public function instructor_index()
    {
        $this->set('title_for_layout', 'My Report - ' . $this->Utilities->applicationName);
    }

    /**
     *
     */
    public function instructor_student()
    {
        $this->set('title_for_layout', 'Student Report - ' . $this->Utilities->applicationName);
        $conditions = array(
            'Profile.instructor_id' => $this->userID
        );

        $fields = array(
            'User.id',
            'User.uuid',
            'User.username',
            'Profile.name',
            'Profile.phone',
            'CONCAT(Instructor.first_name, " ", Instructor.last_name) as instructorName',
            'InstructorUser.uuid',
            'InstructorUser.username',
            'COUNT(PackagesUser.id) as packagesPurchased',
            'SUM(PackagesUser.available) as availableLesson',
            'SUM(PackagesUser.total_lesson) as totalLesson',
            'COUNT(Benchmark.id) as countCompletedLesson',

        );

        $joins = array(
            array(
                'table' => 'users',
                'type' => 'LEFT',
                'alias' => 'User',
                'conditions' => array('User.id = Profile.user_id')
            ),
            array(
                'table' => 'profiles',
                'type' => 'LEFT',
                'alias' => 'Instructor',
                'conditions' => array('Instructor.user_id = Profile.instructor_id')
            ),
            array(
                'table' => 'users',
                'type' => 'LEFT',
                'alias' => 'InstructorUser',
                'conditions' => array('InstructorUser.id = Profile.instructor_id')
            ),
            array(
                'table' => 'benchmarks',
                'type' => 'LEFT',
                'alias' => 'Benchmark',
                'conditions' => array('Benchmark.created_for = Profile.user_id')
            ),
            array(
                'table' => 'packages_users',
                'type' => 'LEFT',
                'alias' => 'PackagesUser',
                'conditions' => array('PackagesUser.created_for = Profile.user_id')
            ),
        );

        $options = array(
            'conditions' => $conditions,
            'fields' => $fields,
            'limit' => $this->limit,
            'recursive' => -1,
            'joins' => $joins,
            'group' => 'Profile.user_id'
        );


        $this->Paginator->settings = $options;
        $users = $this->Paginator->paginate('Profile');
        $this->set('users', $users);
    }

    /**
     *
     */
    public function instructor_contact()
    {
        $this->set('title_for_layout', 'Contact Report - ' . $this->Utilities->applicationName);

        $conditions = array(
            'Contact.user_id' => $this->userID
        );

        if (isset($this->request->query['fromDate']) && isset($this->request->query['toDate'])) {
            if ($this->request->query['fromDate'] && $this->request->query['toDate']) {
                $fromDate = date('Y-m-d H:i:s', strtotime($this->request->query['fromDate']));
                $toDate = date('Y-m-d H:i:s', strtotime($this->request->query['toDate']));
                $rangeConditions = array(
                    'Contact.created >' => $fromDate,
                    'Contact.created <' => $toDate
                );
                $conditions = array_merge($conditions, $rangeConditions);
            } else {
                $conditions = array_merge($conditions, array('DAY(Contact.created)' => date('d')));
            }

        }

        $fields = array(
            'Contact.id',
            'Contact.name',
            'Contact.email',
            'Contact.phone',
            'Contact.account_type',
            'Contact.status',
            'Contact.member_type',
            'Contact.comments',
        );

        $joins = array();

        $options = array(
            'conditions' => $conditions,
            'fields' => $fields,
            'recursive' => -1,
            'joins' => $joins,
            'contain' => array(
                'Note' => array(
                    'order' => 'Note.id DESC',
                )
            ),
        );

        $this->Paginator->settings = $options;
        $contacts = $this->Paginator->paginate('Contact');
        $this->set('contacts', $contacts);
        $this->set('conditions', $conditions);
    }

    public function instructor_sources()
    {
        $this->set('title_for_layout', 'Referral Sources - ' . $this->Utilities->applicationName);
        $this->referralSourceOfUser();
        $this->referralSourceOfContact($options = array('conditions' => array('Contact.user_id' => $this->userID)));
    }


    /**
     *
     */
    public function instructor_transaction()
    {
        $title = 'Transaction List';
        $this->set('title_for_layout', 'Transaction List - ' . $this->Utilities->applicationName);
        $studentIds = $this->Profile->getStudentIdsByInstructorID($this->userID);
        $conditions = array(
            'Billing.user_id' => $studentIds
        );


        if (isset($this->request->query['student']) && $this->request->query['student'] && $this->request->query['student'] != '') {
            $studentInfo = explode('-', $this->request->query['student']);
            $studentID = $this->User->getUserByEMail(trim($studentInfo[1]));
            if ($studentID) {
                $conditions = array_merge($conditions, array('Billing.user_id' => $studentID));
            }
            $search = true;
        }

        if (isset($this->request->query['date']) && $this->request->query['date'] && $this->request->query['date'] != 'Choose Date') {
            $data = $this->request->query['date'];
            if ($data == 'last_month') {
                $month_ini = new DateTime("first day of last month");
                $month_end = new DateTime("last day of last month");
                $dataFrom = $month_ini->format('Y-m-d') . ' 00:00:00';
                $dataTo = $month_end->format('Y-m-d') . ' 23:59:59';
            }

            if ($data == 'current_month') {
                $dataFrom = date('Y-m-01 00:00:00', strtotime('this month'));
                $dataTo = date('Y-m-t 12:59:59', strtotime('this month'));
            }

            if ($data == 'l_3_month') {
                $dataFrom = date("Y-m-d", strtotime("-3 Months")) . ' 00:00:00';;
                $dataTo = date("Y-m-d H:i:s");
            }

            if ($data == 'last_6_month') {
                $dataFrom = date("Y-m-d", strtotime("-6 Months")) . ' 00:00:00';;
                $dataTo = date("Y-m-d H:i:s");
            }

            if ($data == 'last_12_month') {
                $dataFrom = date("Y-m-d", strtotime("-12 Months")) . ' 00:00:00';;
                $dataTo = date("Y-m-d H:i:s");
            }

            $conditions = array_merge($conditions,
                array('Billing.created >=' => $dataFrom, 'Billing.created <=' => $dataTo));
        }

        if (isset($this->request->query['starting']) && $this->request->query['starting']) {
            $starting = date("Y-m-d H:i:s", strtotime($this->request->query['starting']));
            $conditions = array_merge($conditions, array('Billing.created >=' => $starting));
        }

        if (isset($this->request->query['ending']) && $this->request->query['ending']) {
            $ending = date("Y-m-d H:i:s", strtotime($this->request->query['ending']));
            $conditions = array_merge($conditions, array('Billing.created <=' => $ending));
        }

        if (isset($this->request->query['amount']) && $this->request->query['amount'] && $this->request->query['amount'] != 'Choose Amount') {
            $amount = $this->request->query['amount'];
            $conditions = array_merge($conditions, array('Billing.amount >=' => $amount));
        }

        if (isset($this->request->query['starting_amount']) && $this->request->query['starting_amount']) {
            $startingAmount = $this->request->query['starting_amount'];
            $conditions = array_merge($conditions, array('Billing.amount >=' => $startingAmount));
        }

        if (isset($this->request->query['ending_amount']) && $this->request->query['ending_amount']) {
            $endingAmount = $this->request->query['ending_amount'];
            $conditions = array_merge($conditions, array('Billing.amount <=' => $endingAmount));
        }

        if ($this->request->query('transaction')) {
            $transaction = $this->request->query('transaction');
            $conditions = array_merge($conditions,
                array(
                    'OR' => array(
                        'Billing.payment_transaction_id' => $transaction,
                    )
                )
            );
        }

        $this->Paginator->settings = array(
            'conditions' => $conditions,
            'limit' => $this->limit,
            'order' => 'Billing.id DESC',
            'fields' => array(
                'Billing.uuid',
                'Billing.amount',
                'Billing.user_id',
                'Billing.payment_account_number',
                'Billing.payment_transaction_type',
                'Billing.payment_transaction_id',
                'Billing.created',
                'CONCAT(Profile.first_name, " ", Profile.last_name) as studentName',
                'User.uuid',
            ),
            'joins' => array(
                array(
                    'table' => 'profiles',
                    'type' => 'LEFT',
                    'alias' => 'Profile',
                    'conditions' => array('Profile.user_id = Billing.user_id')
                ),
                array(
                    'table' => 'users',
                    'type' => 'LEFT',
                    'alias' => 'User',
                    'conditions' => array('User.id = Billing.user_id')
                ),
            )
        );

        if (isset($search) && $search) {
            $title = 'Search  Result';
        }
        $transactions = $this->Paginator->paginate('Billing');
        $this->set('transactions', $transactions);
        $this->set('title', $title);
        $this->set('conditions', $conditions);
    }

    /**
     * @param $billingUuid
     * @throws BadRequestException
     */
    public function instructor_transaction_view($billingUuid)
    {
        $this->set('title_for_layout', 'Transaction Details - ' . $this->Utilities->applicationName);
        $billingID = $this->Billing->getBillingIDByUuid($billingUuid);
        $userID = $this->Billing->getUserIDByBillingID($billingID);

        $transactionID = $this->Billing->getBillingByID($billingID);
        $items = $this->BillingsPackage->getItemsByBillingID($billingID);
        if (empty($transactionID)) {
            throw new BadRequestException;
        }

        $this->set('transactionID', $transactionID);
        $this->set('cost', $this->Utilities->calculatePrice($items));
        $this->set('student', $this->User->getUser($userID, 0));
        $this->set('items', $items);
    }

    /**
     *
     */
    public function instructor_export_transaction()
    {
        $conditions = array();
        if (isset($this->request->query['conditions']) && $this->request->query['conditions']) {
            $conditions = unserialize($this->request->query['conditions']);
        }

        $transactions = $this->Billing->find(
            'all',
            array(
                'conditions' => $conditions,
                'order' => 'Billing.id DESC',
                'fields' => array(
                    'Billing.uuid',
                    'Billing.amount',
                    'Billing.user_id',
                    'Billing.payment_account_number',
                    'Billing.payment_transaction_type',
                    'Billing.payment_transaction_id',
                    'Billing.created',
                    'CONCAT(Profile.first_name, " ", Profile.last_name) as studentName',
                    'User.uuid',
                ),
                'joins' => array(
                    array(
                        'table' => 'profiles',
                        'type' => 'LEFT',
                        'alias' => 'Profile',
                        'conditions' => array('Profile.user_id = Billing.user_id')
                    ),
                    array(
                        'table' => 'users',
                        'type' => 'LEFT',
                        'alias' => 'User',
                        'conditions' => array('User.id = Billing.user_id')
                    ),
                )
            )
        );

        App::uses('CakeNumber', 'Utility');
        App::uses('CakeTime', 'Utility');
        $transactionsList = array();

        foreach ($transactions as $transaction) {
            if ($transaction['Billing']['payment_transaction_type'] == 'auth_capture') {
                $type = 'purchased items';
            } elseif ($transaction['Billing']['payment_transaction_type'] == 'refund') {
                $type = 'refunded';
            } else {
                $type = 'N/A';
            }

            $transactionsList[]['Billing'] = array(
                'Student Name' => $transaction[0]['studentName'],
                'Transaction ID' => $transaction['Billing']['payment_transaction_id'],
                'Transaction Amount' => CakeNumber::currency($transaction['Billing']['amount']),
                'Transaction Type' => $type,
                'Card' => $transaction['Billing']['payment_account_number'],
                'Date' => CakeTime::format('d M, Y h:i A', $transaction['Billing']['created']),
            );
        }

        $excludePaths = array();
        $customHeaders = array(
            'Billing.Student Name' => 'Student Name',
            'Billing.Transaction ID' => 'Transaction ID',
            'Billing.Transaction Amount' => 'Transaction Amount',
            'Billing.Transaction Type' => 'Transaction Type',
            'Billing.Card' => 'Card',
            'Billing.Date' => 'Date',
        );

        $this->response->download('transaction_list.csv');
        $this->CsvView->quickExport($transactionsList, $excludePaths, $customHeaders);
    }
    /**************************************************************************************************************/
    /******************************************         Student Panel         *************************************/
    /**************************************************************************************************************/

    /**
     *
     */
    public function student_index()
    {

    }

    /**
     * @param null $sortBy
     */
    public function student_my_package($sortBy = null)
    {
        $this->set('title_for_layout', 'My Item List - ' . $this->Utilities->applicationName);
        $title = 'My Item List';
        $condition = array('PackagesUser.created_for' => $this->userID);

        if ($sortBy == 'running') {
            $condition = array_merge($condition, array('PackagesUser.status' => 1));
            $title = 'Running Package List';
        } elseif ($sortBy == 'completed') {
            $condition = array_merge($condition, array('PackagesUser.status' => 2));
            $title = 'Complete Package List';
        } elseif ($sortBy == 'expired') {
            $condition = array_merge($condition, array('PackagesUser.status' => 3));
            $title = 'Expired Package List';
        } elseif ($sortBy == 'unpaid') {
            $condition = array_merge($condition, array('PackagesUser.payment_status' => 0));
            $title = 'Unpaid Package List';
        } elseif ($sortBy == 'paid') {
            $condition = array_merge($condition, array('PackagesUser.payment_status' => 1));
            $title = 'Paid Package List';
        }

        $this->Paginator->settings = array(
            'conditions' => $condition,
            'limit' => $this->limit,
            'order' => array('PackagesUser.created' => 'DESC'),
            'recursive' => 1,
            'fields' => array(
                'CONCAT(Profile.first_name, " ", Profile.last_name) as name',
                'User.uuid',
                'User.id',
                'Package.name',
                'Package.uuid',
                'Package.price',
                'Package.tax',
                'Package.total',
                'Package.tax_percentage',
                'PackagesUser.*',
            ),
            'joins' => array(
                array(
                    'table' => 'profiles',
                    'type' => 'LEFT',
                    'alias' => 'Profile',
                    'conditions' => array('Profile.user_id = PackagesUser.created_for')
                )
            ),

        );

        $this->set('title', $title);
        $this->set('packages', $this->Paginator->paginate('PackagesUser'));
        $this->set('posOverview',
            $this->PackagesUser->getPOSOverview(array('conditions' => array('PackagesUser.created_for' => $this->userID))));
    }

    /**
     * @param $ID
     * @throws BadRequestException
     */
    public function student_my_package_view($ID)
    {
        $this->set('title_for_layout', 'Item Details - ' . $this->Utilities->applicationName);
        $packageDetails = $this->PackagesUser->getPackageUser($ID);

        if (empty($packageDetails)) {
            throw new BadRequestException;
        }

        $this->set('package', $packageDetails);
    }

    /**
     *
     */
    public function student_transaction()
    {
        $this->set('title_for_layout', 'Transaction List - ' . $this->Utilities->applicationName);
        $this->Paginator->settings = array(
            'conditions' => array(
                'Billing.user_id' => $this->userID
            ),
            'limit' => $this->limit,
            'order' => 'Billing.id DESC',
            'fields' => array(
                'Billing.uuid',
                'Billing.amount',
                'Billing.user_id',
                'Billing.payment_account_number',
                'Billing.payment_transaction_type',
                'Billing.created',
                'CONCAT(Profile.first_name, " ", Profile.last_name) as studentName',
                'User.uuid',
            ),
            'joins' => array(
                array(
                    'table' => 'profiles',
                    'type' => 'LEFT',
                    'alias' => 'Profile',
                    'conditions' => array('Profile.user_id = Billing.user_id')
                ),
                array(
                    'table' => 'users',
                    'type' => 'LEFT',
                    'alias' => 'User',
                    'conditions' => array('User.id = Billing.user_id')
                ),
            )
        );
        $transactions = $this->Paginator->paginate('Billing');
        $this->set('transactions', $transactions);
    }

    /**
     * @param $billingUuid
     * @throws BadRequestException
     */
    public function student_transaction_view($billingUuid)
    {
        $this->set('title_for_layout', 'Transaction Details - ' . $this->Utilities->applicationName);
        $billingID = $this->Billing->getBillingIDByUuid($billingUuid);
        $transactionDetails = $this->Billing->getBillingByID($billingID);
        $items = $this->BillingsPackage->getItemsByBillingID($billingID);
        if (empty($transactionDetails)) {
            throw new BadRequestException;
        }

        $this->set('transactionDetails', $transactionDetails);
        $this->set('items', $items);
    }



    /**
     *
     */
    public function tc_user($slug = null)
    {
        $this->set('title_for_layout', 'User Report - ' . $this->Utilities->applicationName);
        $conditions = array('User.role' => array(2, 3));
        $title = 'User Report';
        if(isset($slug)){
            if ($slug == 'student') {
                $conditions = array_merge($conditions, array('User.role' => 2));
                $title = 'Student Report';
            }elseif ($slug == 'instructor') {
                $conditions = array_merge($conditions, array('User.role' => 3));
                $title = 'Instructor Report';
            }
        }

        $fields = array(
            'User.id',
            'User.uuid',
            'User.username',
            'User.role',
            'Profile.name',
            'Profile.phone',
            'Profile.city',
            'CONCAT(Instructor.first_name, " ", Instructor.last_name) as instructorName',
            'InstructorUser.uuid',
            'InstructorUser.username',
            'COUNT(PackagesUser.id) as packagesPurchased',
            'SUM(PackagesUser.available) as availableLesson',
            'SUM(PackagesUser.total_lesson) as totalLesson',
            'COUNT(Benchmark.id) as countCompletedLesson',

        );

        $joins = array(
            array(
                'table' => 'users',
                'type' => 'LEFT',
                'alias' => 'User',
                'conditions' => array('User.id = Profile.user_id')
            ),
            array(
                'table' => 'profiles',
                'type' => 'LEFT',
                'alias' => 'Instructor',
                'conditions' => array('Instructor.user_id = Profile.instructor_id')
            ),
            array(
                'table' => 'users',
                'type' => 'LEFT',
                'alias' => 'InstructorUser',
                'conditions' => array('InstructorUser.id = Profile.instructor_id')
            ),
            array(
                'table' => 'benchmarks',
                'type' => 'LEFT',
                'alias' => 'Benchmark',
                'conditions' => array('Benchmark.created_for = Profile.user_id')
            ),
            array(
                'table' => 'packages_users',
                'type' => 'LEFT',
                'alias' => 'PackagesUser',
                'conditions' => array('PackagesUser.created_for = Profile.user_id')
            ),
        );

        $options = array(
            'conditions' => $conditions,
            'fields' => $fields,
            'limit' => $this->limit,
            'recursive' => -1,
            'joins' => $joins,
            'group' => 'Profile.user_id'
        );


        $this->Paginator->settings = $options;
        $users = $this->Paginator->paginate('Profile');
        $this->set('users', $users);
        $this->set('title' , $title);
    }

    /**
     *
     */
    public function tc_lead()
    {
        $this->set('title_for_layout', 'Lead Report - ' . $this->Utilities->applicationName);
        $instructorIDs = $this->TcsUser->getIntructorIdsForTC($this->userID);
        $conditions = array(
            'Contact.user_id' => $instructorIDs
        );

        if (isset($this->request->query['instructor']) && $this->request->query['instructor']) {
            $instructorID = $this->request->query['instructor'];
            if($instructorID[0] == ''){
                unset($instructorID[0]);
            }
            if (!empty($instructorID)) {
                $conditions = array_merge($conditions, array('Contact.user_id' => $instructorID));
            }
        }

        /*
         * Contact Search
         */
        if (isset($this->request->query['contact']) && $this->request->query['contact'] && $this->request->query['contact'] != '') {
            $contactInfo = explode('-', $this->request->query['contact']);
            $contacttID = $this->Contact->getContactByEMail(trim($contactInfo[1]));
            if ($contacttID) {
                $conditions = array_merge($conditions, array('Contact.id' => $contacttID));
            }
        }

        if (isset($this->request->query['email']) && $this->request->query['email'] && $this->request->query['email'] != '') {
            $email = $this->request->query['email'];
            if (isset($email) && $email) {
                $conditions = array_merge($conditions, array('Contact.email  LIKE' => '%'.$email.'%'));
            }
        }

        if (isset($this->request->query['phone']) && $this->request->query['phone'] && $this->request->query['phone'] != '') {
            $phone = $this->request->query['phone'];
            if (isset($phone) && $phone) {
                $conditions = array_merge($conditions, array('Contact.phone  LIKE' => '%'.$phone.'%'));
            }
        }

        if (isset($this->request->query['date']) && $this->request->query['date'] && $this->request->query['date'] != 'Choose Date') {
            $data = $this->request->query['date'];
            if ($data == 'last_month') {
                $month_ini = new DateTime("first day of last month");
                $month_end = new DateTime("last day of last month");
                $dataFrom = $month_ini->format('Y-m-d') . ' 00:00:00';
                $dataTo = $month_end->format('Y-m-d') . ' 23:59:59';
            }

            if ($data == 'current_month') {
                $dataFrom = date('Y-m-01 00:00:00', strtotime('this month'));
                $dataTo = date('Y-m-t 12:59:59', strtotime('this month'));
            }

            if ($data == 'l_3_month') {
                $dataFrom = date("Y-m-d", strtotime("-3 Months")) . ' 00:00:00';;
                $dataTo = date("Y-m-d H:i:s");
            }

            if ($data == 'last_6_month') {
                $dataFrom = date("Y-m-d", strtotime("-6 Months")) . ' 00:00:00';;
                $dataTo = date("Y-m-d H:i:s");
            }

            if ($data == 'last_12_month') {
                $dataFrom = date("Y-m-d", strtotime("-12 Months")) . ' 00:00:00';;
                $dataTo = date("Y-m-d H:i:s");
            }

            $conditions = array_merge($conditions,
                array('Contact.created >=' => $dataFrom, 'Contact.created <=' => $dataTo));
        }


        if (isset($this->request->query['fromDate']) && isset($this->request->query['toDate'])) {
            if ($this->request->query['fromDate'] && $this->request->query['toDate']) {
                $fromDate = date('Y-m-d H:i:s', strtotime($this->request->query['fromDate']));
                $toDate = date('Y-m-d H:i:s', strtotime($this->request->query['toDate']));
                $rangeConditions = array(
                    'Contact.created >' => $fromDate,
                    'Contact.created <' => $toDate
                );
                $conditions = array_merge($conditions, $rangeConditions);
            } else {
                //$conditions = array_merge($conditions, array('DAY(Contact.created)' => date('d')));
            }

        }


        $fields = array(
            'Contact.id',
            'Contact.name',
            'Contact.email',
            'Contact.phone',
            'Contact.account_type',
            'Contact.status',
            'Contact.member_type',
            'Contact.comments',
            'Contact.created',
            'Contact.user_id',
        );

        $options = array(
            'conditions' => $conditions,
            'fields' => $fields,
            'recursive' => -1,
            'contain' => array(
                'Note' => array(
                    'order' => 'Note.id DESC',
                ),
                'User' => array(
                    'fields' => array('id' , 'username'),
                    'Profile' => array(
                        'fields' => array('first_name' , 'last_name'),
                    )
                )
            ),
        );

        $this->Paginator->settings = $options;
        $contacts = $this->Paginator->paginate('Contact');

        $getTcInstructors = $this->TcsUser->getInstructorIdsByTCID($this->userID);
        $this->set('instructors', $getTcInstructors);

        $this->set('contacts', $contacts);
        $this->set('conditions', $conditions);
    }
    /**
     * @param $contactID
     * @throws NotFoundException
     */
    public function tc_view($contactID)
    {
        $this->set('title_for_layout', 'Contact Details - '.$this->Utilities->applicationName);
        $this->Contact->id = $contactID;
        if (!$this->Contact->exists()) {
            throw new NotFoundException('Invalid Contact');
        }

        $this->set('contact', $this->Contact->getContactByID($contactID));
        $this->set('notes', $this->Note->getContactNotes($contactID));

        if ($this->request->is('post')) {
            $this->request->data['Note']['contact_id'] = $contactID;
            $this->request->data['Note']['created_by'] = $this->userID;
            $this->request->data['Note']['note_date'] = date('Y-m-d H:i:s', strtotime($this->request->data['Note']['note_date']));

            if ($this->Note->save($this->request->data)) {
                $this->Session->setFlash('Note has been saved', 'flash_success');
                $this->redirect(array('controller' => 'contacts', 'action' => 'view', $contactID));
            } else {
                $this->Session->setFlash('Sorry, something went wrong', 'flash_error');
            }

        }
    }

    public function tc_sources()
    {
        $instructorIDs = $this->TcsUser->getIntructorIdsForTC($this->userID);
        $this->set('title_for_layout', 'Referral Sources - ' . $this->Utilities->applicationName);
        $this->referralSourceOfUser();
        $this->referralSourceOfContact($options = array('conditions' => array('Contact.user_id' => $instructorIDs)));
    }


    /**
     *
     */
    public function tc_transaction()
    {
        $title = 'Transaction List';
        $instructorIDs = $this->TcsUser->getIntructorIdsForTC($this->userID);
        $this->set('title_for_layout', 'Transaction List - ' . $this->Utilities->applicationName);

        $studentIds = $this->Profile->getStudentIdsByInstructorID($instructorIDs);
        $conditions = array(
            'Billing.user_id' => $studentIds
        );

        if (isset($this->request->query['instructor']) && $this->request->query['instructor']) {
            $instructorID = $this->request->query['instructor'];
            if($instructorID[0] == ''){
                unset($instructorID[0]);
            }
            if (!empty($instructorID)) {
                $conditions = array_merge($conditions, array('Profile.instructor_id' => $instructorID));
            }
        }


        if (isset($this->request->query['student']) && $this->request->query['student'] && $this->request->query['student'] != '') {
            $studentInfo = explode('-', $this->request->query['student']);
            $studentID = $this->User->getUserByEMail(trim($studentInfo[1]));
            if ($studentID) {
                $conditions = array_merge($conditions, array('Billing.user_id' => $studentID));
            }
            $search = true;
        }

        if (isset($this->request->query['date']) && $this->request->query['date'] && $this->request->query['date'] != 'Choose Date') {
            $data = $this->request->query['date'];
            if ($data == 'last_month') {
                $month_ini = new DateTime("first day of last month");
                $month_end = new DateTime("last day of last month");
                $dataFrom = $month_ini->format('Y-m-d') . ' 00:00:00';
                $dataTo = $month_end->format('Y-m-d') . ' 23:59:59';
            }

            if ($data == 'current_month') {
                $dataFrom = date('Y-m-01 00:00:00', strtotime('this month'));
                $dataTo = date('Y-m-t 12:59:59', strtotime('this month'));
            }

            if ($data == 'l_3_month') {
                $dataFrom = date("Y-m-d", strtotime("-3 Months")) . ' 00:00:00';;
                $dataTo = date("Y-m-d H:i:s");
            }

            if ($data == 'last_6_month') {
                $dataFrom = date("Y-m-d", strtotime("-6 Months")) . ' 00:00:00';;
                $dataTo = date("Y-m-d H:i:s");
            }

            if ($data == 'last_12_month') {
                $dataFrom = date("Y-m-d", strtotime("-12 Months")) . ' 00:00:00';;
                $dataTo = date("Y-m-d H:i:s");
            }

            $conditions = array_merge($conditions,
                array('Billing.created >=' => $dataFrom, 'Billing.created <=' => $dataTo));
        }

        if (isset($this->request->query['starting']) && $this->request->query['starting']) {
            $starting = date("Y-m-d H:i:s", strtotime($this->request->query['starting']));
            $conditions = array_merge($conditions, array('Billing.created >=' => $starting));
        }

        if (isset($this->request->query['ending']) && $this->request->query['ending']) {
            $ending = date("Y-m-d H:i:s", strtotime($this->request->query['ending']));
            $conditions = array_merge($conditions, array('Billing.created <=' => $ending));
        }

        if (isset($this->request->query['amount']) && $this->request->query['amount'] && $this->request->query['amount'] != 'Choose Amount') {
            $amount = $this->request->query['amount'];
            $conditions = array_merge($conditions, array('Billing.amount >=' => $amount));
        }

        if (isset($this->request->query['starting_amount']) && $this->request->query['starting_amount']) {
            $startingAmount = $this->request->query['starting_amount'];
            $conditions = array_merge($conditions, array('Billing.amount >=' => $startingAmount));
        }

        if (isset($this->request->query['ending_amount']) && $this->request->query['ending_amount']) {
            $endingAmount = $this->request->query['ending_amount'];
            $conditions = array_merge($conditions, array('Billing.amount <=' => $endingAmount));
        }

        if ($this->request->query('transaction')) {
            $transaction = $this->request->query('transaction');
            $conditions = array_merge($conditions,
                array(
                    'OR' => array(
                        'Billing.payment_transaction_id' => $transaction,
                    )
                )
            );
        }

        $this->Paginator->settings = array(
            'conditions' => $conditions,
            'limit' => $this->limit,
            'order' => 'Billing.id DESC',
            'fields' => array(
                'Billing.uuid',
                'Billing.amount',
                'Billing.user_id',
                'Billing.payment_account_number',
                'Billing.payment_transaction_type',
                'Billing.payment_transaction_id',
                'Billing.created',
                'CONCAT(Profile.first_name, " ", Profile.last_name) as studentName',
                'User.uuid',
                'CONCAT(Instructor.first_name, " ", Instructor.last_name) as instructorName',

            ),
            'joins' => array(
                array(
                    'table' => 'profiles',
                    'type' => 'LEFT',
                    'alias' => 'Profile',
                    'conditions' => array('Profile.user_id = Billing.user_id')
                ),
                array(
                    'table' => 'users',
                    'type' => 'LEFT',
                    'alias' => 'User',
                    'conditions' => array('User.id = Billing.user_id')
                ),
                array(
                    'table' => 'profiles',
                    'type' => 'LEFT',
                    'alias' => 'Instructor',
                    'conditions' => array('Instructor.user_id = Profile.instructor_id')
                ),

            )
        );

        if (isset($search) && $search) {
            $title = 'Search  Result';
        }
        $transactions = $this->Paginator->paginate('Billing');
        $this->set('transactions', $transactions);
        $this->set('title', $title);
        $this->set('conditions', $conditions);

        $getTcInstructors = $this->TcsUser->getInstructorIdsByTCID($this->userID);
        $this->set('instructors', $getTcInstructors);

    }



    /**
     * @param $billingUuid
     * @throws BadRequestException
     */
    public function tc_transaction_view($billingUuid)
    {
        $this->set('title_for_layout', 'Transaction Details - ' . $this->Utilities->applicationName);
        $billingID = $this->Billing->getBillingIDByUuid($billingUuid);
        $userID = $this->Billing->getUserIDByBillingID($billingID);

        $transactionID = $this->Billing->getBillingByID($billingID);
        $items = $this->BillingsPackage->getItemsByBillingID($billingID);
        if (empty($transactionID)) {
            throw new BadRequestException;
        }

        $this->set('transactionID', $transactionID);
        $this->set('cost', $this->Utilities->calculatePrice($items));
        $this->set('student', $this->User->getUser($userID, 0));
        $this->set('items', $items);
    }

    /**
     *
     */
    public function tc_export_transaction()
    {
        $conditions = array();
        if (isset($this->request->query['conditions']) && $this->request->query['conditions']) {
            $conditions = unserialize($this->request->query['conditions']);
        }

        $transactions = $this->Billing->find(
            'all',
            array(
                'conditions' => $conditions,
                'order' => 'Billing.id DESC',
                'fields' => array(
                    'Billing.uuid',
                    'Billing.amount',
                    'Billing.user_id',
                    'Billing.payment_account_number',
                    'Billing.payment_transaction_type',
                    'Billing.payment_transaction_id',
                    'Billing.created',
                    'CONCAT(Profile.first_name, " ", Profile.last_name) as studentName',
                    'User.uuid',
                ),
                'joins' => array(
                    array(
                        'table' => 'profiles',
                        'type' => 'LEFT',
                        'alias' => 'Profile',
                        'conditions' => array('Profile.user_id = Billing.user_id')
                    ),
                    array(
                        'table' => 'users',
                        'type' => 'LEFT',
                        'alias' => 'User',
                        'conditions' => array('User.id = Billing.user_id')
                    ),
                )
            )
        );

        App::uses('CakeNumber', 'Utility');
        App::uses('CakeTime', 'Utility');
        $transactionsList = array();

        foreach ($transactions as $transaction) {
            if ($transaction['Billing']['payment_transaction_type'] == 'auth_capture') {
                $type = 'purchased items';
            } elseif ($transaction['Billing']['payment_transaction_type'] == 'refund') {
                $type = 'refunded';
            } else {
                $type = 'N/A';
            }

            $transactionsList[]['Billing'] = array(
                'Student Name' => $transaction[0]['studentName'],
                'Transaction ID' => $transaction['Billing']['payment_transaction_id'],
                'Transaction Amount' => CakeNumber::currency($transaction['Billing']['amount']),
                'Transaction Type' => $type,
                'Card' => $transaction['Billing']['payment_account_number'],
                'Date' => CakeTime::format('d M, Y h:i A', $transaction['Billing']['created']),
            );
        }

        $excludePaths = array();
        $customHeaders = array(
            'Billing.Student Name' => 'Student Name',
            'Billing.Transaction ID' => 'Transaction ID',
            'Billing.Transaction Amount' => 'Transaction Amount',
            'Billing.Transaction Type' => 'Transaction Type',
            'Billing.Card' => 'Card',
            'Billing.Date' => 'Date',
        );

        $this->response->download('transaction_list.csv');
        $this->CsvView->quickExport($transactionsList, $excludePaths, $customHeaders);
    }
    /**************************************************************************************************************/
    /******************************************       Custom Function          ************************************/
    /**************************************************************************************************************/

    /**
     * @param null $sortBy
     */
    public function instructor_my_package($sortBy = null)
    {
        $this->set('title_for_layout', 'My Item List - ' . $this->Utilities->applicationName);
        $title = 'My Item List';
        $condition = array('PackagesUser.instructor_id' => $this->userID);

        if ($sortBy == 'running') {
            $condition = array_merge($condition, array('PackagesUser.status' => 1));
            $title = 'Running Package List';
        } elseif ($sortBy == 'completed') {
            $condition = array_merge($condition, array('PackagesUser.status' => 2));
            $title = 'Complete Package List';
        } elseif ($sortBy == 'expired') {
            $condition = array_merge($condition, array('PackagesUser.status' => 3));
            $title = 'Expired Package List';
        } elseif ($sortBy == 'unpaid') {
            $condition = array_merge($condition, array('PackagesUser.payment_status' => 0));
            $title = 'Unpaid Package List';
        } elseif ($sortBy == 'paid') {
            $condition = array_merge($condition, array('PackagesUser.payment_status' => 1));
            $title = 'Paid Package List';
        }
        /**
         * For Modal Search
         */

        if (isset($this->request->query['student']) && $this->request->query['student'] && $this->request->query['student'] != '') {
            $studentInfo = explode('-', $this->request->query['student']);
            $studentID = $this->User->getUserByEMail(trim($studentInfo[1]));
            if ($studentID) {
                $condition = array_merge($condition, array('PackagesUser.created_for' => $studentID));
            }
        }
        if (isset($this->request->query['item_name']) && $this->request->query['item_name'] && $this->request->query['item_name'] != '') {
            $condition = array_merge($condition,
                array('Package.name Like' => '%' . $this->request->query['item_name'] . '%'));
        }

        if (isset($this->request->query['amount']) && $this->request->query['amount'] && $this->request->query['amount'] != 'Choose Price') {
            $amount = $this->request->query['amount'];
            $condition = array_merge($condition, array('Package.price >=' => $amount));
        }

        if (isset($this->request->query['starting_amount']) && $this->request->query['starting_amount']) {
            $startingAmount = $this->request->query['starting_amount'];
            $condition = array_merge($condition, array('Package.price >=' => $startingAmount));
        }

        if (isset($this->request->query['ending_amount']) && $this->request->query['ending_amount']) {
            $endingAmount = $this->request->query['ending_amount'];
            $condition = array_merge($condition, array('Package.price <=' => $endingAmount));
        }

        if (isset($this->request->query['date']) && $this->request->query['date'] && $this->request->query['date'] != 'Choose Date') {
            $data = $this->request->query['date'];


            if ($data == 'current_month') {
                $dataTo = date('Y-m-01 00:00:00', strtotime('this month'));
                $dataFrom = date('Y-m-t 12:59:59', strtotime('this month'));
            }

            if ($data == 'next_3_month') {
                $dataTo = date("Y-m-d", strtotime("+3 Months")) . ' 00:00:00';;
                $dataFrom = date("Y-m-d H:i:s");
            }

            if ($data == 'next_6_month') {
                $dataTo = date("Y-m-d", strtotime("+6 Months")) . ' 00:00:00';;
                $dataFrom = date("Y-m-d H:i:s");
            }

            if ($data == 'next_12_month') {
                $dataTo = date("Y-m-d", strtotime("+12 Months")) . ' 00:00:00';;
                $dataFrom = date("Y-m-d H:i:s");
            }


            $condition = array_merge($condition,
                array('PackagesUser.expiration_date >=' => $dataFrom, 'PackagesUser.expiration_date <=' => $dataTo));
        }

        if (isset($this->request->query['starting']) && $this->request->query['starting']) {
            $starting = date("Y-m-d H:i:s", strtotime($this->request->query['starting']));
            $conditions = array_merge($condition, array('PackagesUser.expiration_date >=' => $starting));
        }

        if (isset($this->request->query['ending']) && $this->request->query['ending']) {
            $ending = date("Y-m-d H:i:s", strtotime($this->request->query['ending']));
            $condition = array_merge($conditions, array('PackagesUser.expiration_date <=' => $ending));
        }

        if (isset($this->request->query['payment_status']) ) {
            $status = $this->request->query['payment_status'];
            if ($status != '') {
                $status = (int)$this->request->query['payment_status'];
                $condition = array_merge($condition, array('PackagesUser.payment_status' => $status));

            }
        }


        $this->Paginator->settings = array(
            'conditions' => $condition,
            'limit' => $this->limit,
            'order' => array('PackagesUser.created' => 'DESC'),
            'recursive' => 1,
            'fields' => array(
                'CONCAT(Profile.first_name, " ", Profile.last_name) as name',
                'User.uuid',
                'User.id',
                'Package.name',
                'Package.uuid',
                'Package.price',
                'Package.tax',
                'Package.total',
                'Package.tax_percentage',
                'PackagesUser.*',
            ),
            'joins' => array(
                array(
                    'table' => 'profiles',
                    'type' => 'LEFT',
                    'alias' => 'Profile',
                    'conditions' => array('Profile.user_id = PackagesUser.created_for')
                )
            ),

        );

        $this->set('title', $title);
        $this->set('packages', $this->Paginator->paginate('PackagesUser'));
        $this->set('posOverview',
            $this->PackagesUser->getPOSOverview(array('conditions' => array('PackagesUser.instructor_id' => $this->userID))));
    }

    /**
     * @param $ID
     * @throws BadRequestException
     */
    public function instructor_my_package_view($ID)
    {
        $this->set('title_for_layout', 'Item Details - ' . $this->Utilities->applicationName);
        $packageDetails = $this->PackagesUser->getPackageUser($ID);

        if (empty($packageDetails)) {
            throw new BadRequestException;
        }

        $this->set('package', $packageDetails);
    }

    public function instructor_feedback(){
        $this->set('title_for_layout', 'Student Feedback - ' . $this->Utilities->applicationName);

        $conditions = array();
        $fields = array(
            'Profile.first_name',
            'Profile.last_name',
            'User.id',
            'User.uuid',
            'Feedback.id',
            'Feedback.feedback',
            'Feedback.is_feedback',
            'Feedback.rating',
            'Feedback.status',
            'Feedback.created'
        );
        $joins = array(
            array(
                'table' => 'users',
                'type' => 'LEFT',
                'alias' => 'User',
                'conditions' => array('User.id = Feedback.user_id')
            ),
            array(
                'table' => 'profiles',
                'type' => 'LEFT',
                'alias' => 'Profile',
                'conditions' => array('Profile.user_id = User.id')
            ),
        );
        $options = array(
            'conditions' => $conditions,
            'fields' => $fields,
            'limit' => $this->limit,
            'joins' => $joins,
            'order' => array('User.id' => 'desc')
        );


        $this->Paginator->settings = $options;
        $feedback = $this->Paginator->paginate('Feedback');
        $this->set('feedbacks', $feedback);
    }

    public function instructor_feedback_view($id){
        $this->set('title_for_layout', 'Student Feedback Details - ' . $this->Utilities->applicationName);

        $conditions = array('Feedback.id' => $id);
        $fields = array(
            'Profile.first_name',
            'Profile.last_name',
            'User.id',
            'User.uuid',
            'Feedback.id',
            'Feedback.feedback',
            'Feedback.is_feedback',
            'Feedback.rating',
            'Feedback.status',
            'Feedback.created'
        );
        $joins = array(
            array(
                'table' => 'users',
                'type' => 'LEFT',
                'alias' => 'User',
                'conditions' => array('User.id = Feedback.user_id')
            ),
            array(
                'table' => 'profiles',
                'type' => 'LEFT',
                'alias' => 'Profile',
                'conditions' => array('Profile.user_id = User.id')
            ),
        );
        $options = array(
            'conditions' => $conditions,
            'fields' => $fields,
            'limit' => $this->limit,
            'joins' => $joins,
            'order' => array('User.id' => 'desc')
        );

        $feedback = $this->Feedback->find('first' , $options);
        $this->set('feedback', $feedback);
    }

    public function instructor_export_contact()
    {
        if (isset($this->request->query['conditions'])) {
            $conditions = unserialize($this->request->query['conditions']);
            $contacts = $this->Contact->find(
                'all',
                array(
                    'conditions' => $conditions,
                    'recursive' => -1
                )
            );

            if (!$contacts) {
                $this->Session->setFlash("Sorry, you don't have enough data to export", 'flash_error');
                $this->redirect($this->referer());
            }

            $excludePaths = array('Contact.id', 'Contact.user_id', 'Contact.modified');
            $customHeaders = array(
                'Contact.first_name' => 'FirstName',
                'Contact.last_name' => 'LastName',
                'Contact.email' => 'EmailAddress',
                'Contact.phone' => 'PhoneNumber',
                'Contact.account_type' => 'AccountType',
                'Contact.member_type' => 'MemberType',
                'Contact.status' => 'Status',
                'Contact.source' => 'Referral Source',
                'Contact.follow_up_date' => 'Follow Up Date',
                'Contact.created' => 'LastContactDate',
                'Contact.comments' => 'LastNote'
            );

            $this->response->download('contact_report.csv');
            $this->CsvView->quickExport($contacts, $excludePaths, $customHeaders);
        }
    }


    /**
     * @param $options
     */
    public function referralSourceOfUser($options = array())
    {
        $options['sources'] = $this->Source->getSourceList();
        $sources = $this->User->getSourcesForPie($options);
        $studentReferralSource = new GoogleCharts();
        $studentReferralSource->type("PieChart");
        $studentReferralSource->div('student_referral_sources');
        $studentReferralSource->options(
            array(
                'series' => array(0 => array('color' => 'lightslategray')),
                'title' => ' ',
                'pieHole' => 0.4,
                'width' => 'auto',
                'fontSize' => 10,
                'height' => 500,
            )
        );
        $studentReferralSource->columns(
            array(
                'contact' => array(
                    'type' => 'string',
                    'label' => 'Contact Type'
                ),
                'Percent' => array(
                    'type' => 'number',
                    'label' => 'Percent'
                )
            )
        );

        foreach ($sources[0][0] as $key => $value) {
            $studentReferralSource->addRow(
                array(
                    'contact' => $key . '( ' . $value . ' )',
                    'Percent' => $value
                )
            );
        }
        $this->set(compact('studentReferralSource'));
    }


    /**
     * @param $options
     */
    public function referralSourceOfContact($options = array())
    {
        $options['sources'] = $this->Source->getSourceList();
        $sources = $this->Contact->getSourcesForPie($options);
        //var_dump($sources); die();
        $contactReferralSource = new GoogleCharts();
        $contactReferralSource->type("PieChart");
        $contactReferralSource->div('contact_referral_sources');
        $contactReferralSource->options(
            array(
                'series' => array(0 => array('color' => 'lightslategray')),
                'title' => ' ',
                'pieHole' => 0.4,
                'width' => 'auto',
                'fontSize' => 10,
                'height' => 500,
            )
        );
        $contactReferralSource->columns(
            array(
                'contact' => array(
                    'type' => 'string',
                    'label' => 'Contact Type'
                ),
                'Percent' => array(
                    'type' => 'number',
                    'label' => 'Percent'
                )
            )
        );

        foreach ($sources[0][0] as $key => $value) {
            $contactReferralSource->addRow(
                array(
                    'contact' => $key . '( ' . $value . ' )',
                    'Percent' => $value
                )
            );
        }
        $this->set(compact('contactReferralSource'));
    }


    /**
     * @param $options
     */
    public function appointmentOverview($options = array())
    {
        //$options['sources'] = $this->Source->getSourceList();
        $sources = $this->Appointment->getAppointmentOverview();
        $appointmentOverview = new GoogleCharts();
        $appointmentOverview->type("PieChart");
        $appointmentOverview->div('student_referral_sources');
        $appointmentOverview->options(
            array(
                'series' => array(0 => array('color' => 'lightslategray')),
                'title' => ' ',
                'pieHole' => 0.4,
                'width' => 'auto',
                'fontSize' => 10,
                'height' => 500,
            )
        );
        $appointmentOverview->columns(
            array(
                'appointment' => array(
                    'type' => 'string',
                    'label' => 'Contact Type'
                ),
                'percent' => array(
                    'type' => 'number',
                    'label' => 'Percent'
                )
            )
        );

        foreach ($sources[0][0] as $key => $value) {
            $appointmentOverview->addRow(
                array(
                    'appointment' => $key . '( ' . $value . ' )',
                    'percent' => $value
                )
            );
        }
        $this->set(compact('appointmentOverview'));
    }


    public function instructor_appointment()
    {
        $this->set('title_for_layout', 'Appointment Report - ' . $this->Utilities->applicationName);

        $this->appointmentOverview();
    }
}
