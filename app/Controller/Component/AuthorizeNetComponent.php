<?php

define("AUTHORIZENET_API_LOGIN_ID", '7nUQt7V8k');
define("AUTHORIZENET_TRANSACTION_KEY", '8sFWMQ58y7t65xdJ');
define("AUTHORIZENET_SANDBOX", true);

App::import('Vendor', 'authorizenet', array('file' => 'authorizenet'.DS.'authorizenet'.DS.'autoload.php'));

class AuthorizeNetComponent extends Component
{
    public $name = 'AuthorizeNet';

    public function createCustomerProfileCIM($profile = array())
    {
        $request = new AuthorizeNetCIM;
        $customerProfile = new AuthorizeNetCustomer();

        $customerProfile->description = $profile['description'];
        $customerProfile->merchantCustomerId = time() . rand(1, 10);
        $customerProfile->email = $profile['email'];

        $response = $request->createCustomerProfile($customerProfile, true);

        if ($response->isOk()) {
            $result['id'] = $response->getCustomerProfileId();
            $result['status'] = 1;
            return $result;

        } else {
            $result['message'] = $response->getMessageText();
            $result['status'] = 0;
            return $result;
        }
    }

    /**
     * Update customer profile by existing profile id.
     *
     * @param string $customerProfileId
     * @param array $profile
     * @return  array (status 0 for getting error)
     */

    public function updateCustomerProfileCIM($customerProfileId, $profile = array())
    {
        $request = new AuthorizeNetCIM();
        $customerProfile = new AuthorizeNetCustomer();

        $customerProfile->description = $profile['description'];
        $customerProfile->email = $profile['email'];

        $response = $request->updateCustomerProfile($customerProfileId, $customerProfile);

        if ($response->isOk()) {
            $result['message'] = $response->getMessageText();
            $result['status'] = 1;
            return $result;

        } else {
            $result['message'] = $response->getMessageText();
            $result['status'] = 0;
            return $result;
        }
    }

    /**
     * Delete a customer profile by customer id.
     *
     * @param $customerProfileId
     * @return mixed array (status 0 return exception)
     */

    public function deleteCustomerProfileCIM($customerProfileId)
    {
        $request = new AuthorizeNetCIM;
        $response = $request->deleteCustomerProfile($customerProfileId);
        if ($response->isOk()) {
            $result['message'] = $response->getMessageText();
            $result['status'] = 1;
            return $result;

        } else {
            $result['message'] = $response->getMessageText();
            $result['status'] = 0;
            return $result;
        }
    }


    /**
     * Create a Payment profile of a existing customer profile.
     *
     * @param $customerProfileId (customer profile must be exist.)
     * @param array $card (elements name: customerType = individual or business, cardNumber, exp_date )
     * @return mixed array (status 0 to get exception)
     */
    public function createCustomerPaymentProfileCIM($customerProfileId, $card = array())
    {
        $request = new AuthorizeNetCIM;
        $customerProfile = new AuthorizeNetCustomer();
        $paymentProfile = new AuthorizeNetPaymentProfile();

//        $paymentProfile->customerType                       = $card['customerType'];
        $paymentProfile->customerType = 'individual';
        $paymentProfile->payment->creditCard->cardNumber = $card['cardNumber'];
        $paymentProfile->payment->creditCard->expirationDate = $card['exp_date'];
        $customerProfile->paymentProfiles[] = $paymentProfile;

        $response = $request->createCustomerPaymentProfile($customerProfileId, $paymentProfile);

        if ($response->isOk()) {
            $result['id'] = $response->getPaymentProfileId();
            $result['status'] = 1;
            return $result;

        } else {
            $result['message'] = $response->getMessageText();
            $result['status'] = 0;
            return $result;
        }
    }

    /**
     * Update payment profile of a existing customer
     *
     * @param $customerProfileId
     * @param $paymentProfileId
     * @param array $card (elements:cardNumber, exp_date )
     * @return mixed array (status 0 return exception)
     */

    public function updateCustomerPaymentProfileCIM($customerProfileId, $paymentProfileId, $card = array())
    {
        $request = new AuthorizeNetCIM;
        $paymentProfile = new AuthorizeNetPaymentProfile();

        $paymentProfile->payment->creditCard->cardNumber = $card['cardNumber'];
        $paymentProfile->payment->creditCard->expirationDate = $card['exp_date'];
        $response = $request->updateCustomerPaymentProfile($customerProfileId, $paymentProfileId, $paymentProfile);

        if ($response->isOk()) {
            $result['message'] = $response->getMessageText();
            $result['status'] = 1;
            return $result;

        } else {
            $result['message'] = $response->getMessageText();
            $result['status'] = 0;
            return $result;
        }
    }


    /**
     * Delete payment profile
     *
     * @param $customerProfileId
     * @param $paymentProfileId
     * @return mixed array (status 0 return exception)
     */
    public function deleteCustomerPaymentProfileCIM($customerProfileId, $paymentProfileId)
    {
        $request = new AuthorizeNetCIM;
        $response = $request->deleteCustomerPaymentProfile($customerProfileId, $paymentProfileId);
        if ($response->isOk()) {
            $result['message'] = $response->getMessageText();
            $result['status'] = 1;
            return $result;

        } else {
            $result['message'] = $response->getMessageText();
            $result['status'] = 0;
            return $result;
        }
    }

    /**
     * Create a shipping address of an existing customer profile
     *
     * @param $customerProfileID
     * @param array $shippingProfile (elements: firstName, lastName, companyName, address, city, state, zip,country, phoneNumber, faxNumber )
     * @return array (status 0 return an exception)
     */
    public function createCustomerShippingAddressCIM($customerProfileID, $shippingProfile = array())
    {
        if (empty($shippingProfile))
            return 'Shipping profile can not be empty';

        if (empty($customerProfileID))
            return 'Customer profile must be created';

        $request = new AuthorizeNetCIM();
        $address = new AuthorizeNetAddress();

        $address->firstName = $shippingProfile['firstName'];
        $address->lastName = $shippingProfile['lastName'];
        $address->company = $shippingProfile['companyName'];
        $address->address = $shippingProfile['address'];
        $address->city = $shippingProfile['city'];
        $address->state = $shippingProfile['state'];
        $address->zip = $shippingProfile['zip'];
        $address->country = $shippingProfile['country'];
        $address->phoneNumber = $shippingProfile['phoneNumber'];
        $address->faxNumber = $shippingProfile['faxNumber'];

        $response = $request->createCustomerShippingAddress($customerProfileID, $address);

        if ($response->isOk()) {
            $result['id'] = $response->getCustomerAddressId();
            $result['status'] = 1;
            return $result;

        } else {
            $result['message'] = $response->getMessageText();
            $result['status'] = 0;
            return $result;
        }

    }

    /**
     * Update and existing customers shipping address
     *
     * @param $customerProfileId
     * @param $customerShippingAddressId
     * @param array $shippingProfile (elements: firstName, lastName, companyName, address, city, state, zip,country, phoneNumber, faxNumber )
     * @return mixed array (status 0 return exception)
     */

    public function updateCustomerShippingAddressCIM($customerProfileId, $customerShippingAddressId, $shippingProfile = array())
    {
        $request = new AuthorizeNetCIM();
        $address = new AuthorizeNetAddress();

        $address->firstName = $shippingProfile['firstName'];
        $address->lastName = $shippingProfile['lastName'];
        $address->company = $shippingProfile['companyName'];
        $address->address = $shippingProfile['address'];
        $address->city = $shippingProfile['city'];
        $address->state = $shippingProfile['state'];
        $address->zip = $shippingProfile['zip'];
        $address->country = $shippingProfile['country'];
        $address->phoneNumber = $shippingProfile['phoneNumber'];
        $address->faxNumber = $shippingProfile['faxNumber'];

        $response = $request->updateCustomerShippingAddress($customerProfileId, $customerShippingAddressId, $address);

        if ($response->isOk()) {
            $result['message'] = $response->getMessageText();
            $result['status'] = 1;
            return $result;

        } else {
            $result['message'] = $response->getMessageText();
            $result['status'] = 0;
            return $result;
        }

    }

    /**
     * Delete customer shipping address
     *
     * @param $customerProfileId
     * @param $customerAddressId
     * @return mixed array (status 0 making an exception)
     */

    public function deleteCustomerShippingAddressCIM($customerProfileId, $customerAddressId)
    {
        $request = new AuthorizeNetCIM;
        $response = $request->deleteCustomerShippingAddress($customerProfileId, $customerAddressId);
        if ($response->isOk()) {
            $result['message'] = $response->getMessageText();
            $result['status'] = 1;
            return $result;

        } else {
            $result['message'] = $response->getMessageText();
            $result['status'] = 0;
            return $result;
        }
    }

    /**
     * Create a simple transaction of an existing customer without products details
     *
     * Customer profile, payment profile and shipping address must be created.
     * If transaction successfully done return array of transaction related information.
     * If failed to transaction return array with exception message and status 0
     *
     * @param $amount total amount of products
     * @param $customerProfileId
     * @param $customerPaymentId
     * @param $shippingAddressId
     * @return array
     *
     */
    public function createTransactionCIM($amount, $customerProfileId, $customerPaymentId, $shippingAddressId)
    {
        $data = array();
        if (empty($amount)) {
            $data['status'] = 0;
            $data['message'] = 'Amount can not be empty.';
            return $data;

        } elseif (!is_int($customerProfileId) & !is_numeric($customerProfileId) | empty($customerProfileId)) {
            $data['status'] = 0;
            $data['message'] = 'May be customer information invalid.';
            return $data;

        } elseif (!is_int($shippingAddressId) & !is_numeric($shippingAddressId) | empty($shippingAddressId)) {
            $data['status'] = 0;
            $data['message'] = 'May be shipping information invalid.';
            return $data;

        } elseif (!is_int($customerPaymentId) & !is_numeric($customerPaymentId) | empty($customerPaymentId)) {
            $data['status'] = 0;
            $data['message'] = 'May be card number or expired date invalid.';
            return $data;
        }

        $request = new AuthorizeNetCIM();
        $transactionType = 'AuthCapture';
        $transaction = array(
            'amount' => $amount,
            'customerProfileId' => $customerProfileId,
            'customerPaymentProfileId' => $customerPaymentId,
            'customerShippingAddressId' => $shippingAddressId
        );

        $response = $request->createCustomerProfileTransaction($transactionType, $transaction);
        if ($response->isOk()) {
            $responseData = $response->getTransactionResponse();

            if ($responseData->approved) {
                $data['payment_final_status'] = 1;
                $data['status'] = 1;

            } elseif ($responseData->declined) {
                $data['payment_final_status'] = 2;
                $data['status'] = 2;

            } elseif ($responseData->error) {
                $data['payment_final_status'] = 3;
                $data['status'] = 3;

            } elseif ($responseData->approved) {
                $data['payment_final_status'] = 4;
                $data['status'] = 4;
            }
            $data['payment_response_code'] = $responseData->response_code;
            $data['payment_response_reason_code'] = $responseData->response_reason_code;
            $data['payment_response_reason_subcode'] = $responseData->response_subcode;
            $data['payment_response_reason_text'] = $responseData->response_reason_text;
            $data['payment_avs_response'] = $responseData->avs_response;
            $data['payment_authorization_code'] = $responseData->authorization_code;
            $data['payment_invoice_no'] = $responseData->invoice_number;
            $data['payment_transaction_id'] = $responseData->transaction_id;
            $data['payment_transaction_type'] = $responseData->transaction_type;
            $data['payment_account_number'] = $responseData->account_number;
            $data['payment_card_type'] = $responseData->card_type;
            $data['payment_response_text_full'] = $responseData->response;

            return $data;

        } else {
            $data['message'] = $response->getMessageText();
            $data['status'] = 0;
            return $data;

        }
    }

    /**
     * @param $amount
     * @param $transId
     * @param $customerProfileId
     * @param $customerPaymentId
     * @return mixed
     */
    public function refundTransaction($amount, $transId, $customerProfileId, $customerPaymentId)
    {

        $request = new AuthorizeNetCIM();
        $transactionType = 'Refund';
        $transaction = array(
            'amount' => $amount,
            'customerProfileId' => $customerProfileId,
            'customerPaymentProfileId' => $customerPaymentId,
            'transId' => $transId,
        );
        $response = $request->createCustomerProfileTransaction($transactionType, $transaction);

        if ($response->isOk()) {
            $responseData = $response->getTransactionResponse();

            if ($responseData->approved) {
                $data['payment_final_status'] = 1;
                $data['status'] = 1;

            } elseif ($responseData->declined) {
                $data['payment_final_status'] = 2;
                $data['status'] = 2;

            } elseif ($responseData->error) {
                $data['payment_final_status'] = 3;
                $data['status'] = 3;

            } elseif ($responseData->approved) {
                $data['payment_final_status'] = 4;
                $data['status'] = 4;
            }
            $data['payment_response_code'] = $responseData->response_code;
            $data['payment_response_reason_code'] = $responseData->response_reason_code;
            $data['payment_response_reason_subcode'] = $responseData->response_subcode;
            $data['payment_response_reason_text'] = $responseData->response_reason_text;
            $data['payment_avs_response'] = $responseData->avs_response;
            $data['payment_authorization_code'] = $responseData->authorization_code;
            $data['payment_invoice_no'] = $responseData->invoice_number;
            $data['payment_transaction_id'] = $responseData->transaction_id;
            $data['payment_transaction_type'] = $responseData->transaction_type;
            $data['payment_account_number'] = $responseData->account_number;
            $data['payment_card_type'] = $responseData->card_type;
            $data['payment_response_text_full'] = $responseData->response;

            return $data;

        } else {
            $data['message'] = $response->getMessageText();
            $data['status'] = 0;
            return $data;

        }
    }


    /**
     *
     * Create a authCapture transaction of an existing customer with products details
     *
     * Customer profile, payment profile and shipping address must be created.
     * If transaction successfully done return array of transaction related information.
     * If failed to transaction return array with exception message and status 0
     *
     * $product = array(
     *      array(
     *          'id'         => '123',
     *          'name'       => 'Facebook status robot',
     *          'description'=> 'Post your wall in your given time',
     *          'quantity'   => '1',
     *          'unitPrice'  => '120',
     *          'taxable'    => 'true'
     *      ),
     *      array(
     *          'id'         => '3423',
     *          'name'       => 'Facebook like robot',
     *          'description'=> 'Like a page in a given time',
     *          'quantity'   => '1',
     *          'unitPrice'  => '200',
     *          'taxable'    => 'false'
     *      )
     * );
     *
     * @param $amount total amount of all products
     * @param array $products
     * @param $customerProfileId
     * @param $paymentProfileId
     * @param $shippingAddressId
     * @return array (status 0 is exception)
     */
    public function createAuthCaptureTransactionCIM($products = array(), $customerProfileId, $paymentProfileId, $shippingAddressId)
    {
        $data = array();
        $amount = 0;

        if (!is_int($customerProfileId) & !is_numeric($customerProfileId) | empty($customerProfileId)) {
            $data['status'] = 0;
            $data['message'] = 'May be customer information invalid.';
            return $data;

        } elseif (!is_int($shippingAddressId) & !is_numeric($shippingAddressId) | empty($shippingAddressId)) {
            $data['status'] = 0;
            $data['message'] = 'May be shipping information invalid.';
            return $data;

        } elseif (!is_int($paymentProfileId) & !is_numeric($paymentProfileId) | empty($paymentProfileId)) {
            $data['status'] = 0;
            $data['message'] = 'May be card number or expired date invalid.';
            return $data;
        }

        $transaction = new AuthorizeNetTransaction;
        $transaction->customerProfileId = $customerProfileId;
        $transaction->customerPaymentProfileId = $paymentProfileId;
        $transaction->customerShippingAddressId = $shippingAddressId;

        $itemId = 1;

        foreach ($products as $product) {
            $lineItem = new AuthorizeNetLineItem;
            $lineItem->itemId = $itemId;
            $lineItem->name = $product['name'];
            $lineItem->description = $product['description'];
            $lineItem->quantity = $product['quantity'];
            $lineItem->unitPrice = $product['unitPrice'];
            $lineItem->taxable = $product['taxable'];

            $transaction->lineItems[] = $lineItem;
            $amount = $amount + $product['unitPrice'];
            $itemId++;
        }
        $transaction->amount = $amount;

        $request = new AuthorizeNetCIM();
        $response = $request->createCustomerProfileTransaction("AuthCapture", $transaction);

        if ($response->isOk()) {
            $responseData = $response->getTransactionResponse();

            if ($responseData->approved) {
                $data['payment_final_status'] = 1;
                $data['status'] = 1;

            } elseif ($responseData->declined) {
                $data['payment_final_status'] = 2;
                $data['status'] = 2;

            } elseif ($responseData->error) {
                $data['payment_final_status'] = 3;
                $data['status'] = 3;

            } elseif ($responseData->approved) {
                $data['payment_final_status'] = 4;
                $data['status'] = 4;
            }

            $data['payment_response_code'] = $responseData->response_code;
            $data['payment_response_reason_code'] = $responseData->response_reason_code;
            $data['payment_response_reason_subcode'] = $responseData->response_subcode;
            $data['payment_response_reason_text'] = $responseData->response_reason_text;
            $data['payment_avs_response'] = $responseData->avs_response;
            $data['payment_authorization_code'] = $responseData->authorization_code;
            $data['payment_invoice_no'] = $responseData->invoice_number;
            $data['payment_transaction_id'] = $responseData->transaction_id;
            $data['payment_transaction_type'] = $responseData->transaction_type;
            $data['payment_account_number'] = $responseData->account_number;
            $data['payment_card_type'] = $responseData->card_type;
            $data['payment_response_text_full'] = $responseData->response;

            return $data;

        } else {
            $data['message'] = $response->getMessageText();
            $data['status'] = 0;
            return $data;
        }
    }

    /**
     * Get customer,payment and shipping profile information
     *
     * @param $customerProfileID
     * @param $shippingAddressID
     * @param $paymentProfileID
     * @return AuthorizeNetCIM_Response|string
     */
    public function getFullProfileCIM($customerProfileID, $shippingAddressID, $paymentProfileID)
    {
        if (!is_int($customerProfileID) & !is_numeric($customerProfileID) | empty($customerProfileID))
            return 'May be customer information invalid';

        if (!is_int($shippingAddressID) & !is_numeric($shippingAddressID) | empty($shippingAddressID))
            return 'May be shipping information invalid';

        if (!is_int($paymentProfileID) & !is_numeric($paymentProfileID) | empty($paymentProfileID))
            return 'May be card number or expired date invalid';

        $request = new AuthorizeNetCIM();
        $profile['customer'] = $request->getCustomerProfile($customerProfileID);
        $profile['shipping'] = $request->getCustomerShippingAddress($customerProfileID, $shippingAddressID);
        $profile['payment'] = $request->getCustomerPaymentProfile($customerProfileID, $paymentProfileID);

        return $profile['customer'];

    }


    /**
     * Create a recursively payment
     *
     * @param array $user
     * @return mixed
     */
    public function recursivePaymentARB($user = array())
    {
        $subscription = new AuthorizeNet_Subscription;
        $subscription->name = $user['productName'];
        $subscription->intervalLength = $user['interval'];
        $subscription->intervalUnit = $user['intervalUnit'];
        $subscription->startDate = $user['startDate'];
        $subscription->totalOccurrences = $user['totalOccurrences'];
        $subscription->amount = $user['amount'];
        $subscription->creditCardCardNumber = $user['creditCardCardNumber'];
        $subscription->creditCardExpirationDate = $user['creditCardExpirationDate'];
        $subscription->creditCardCardCode = $user['creditCardCardCode'];
        $subscription->billToFirstName = $user['billToFirstName'];
        $subscription->billToLastName = $user['billToLastName'];

        $request = new AuthorizeNetARB();
        $response = $request->createSubscription($subscription);
        if ($response->isOk()) {
            $result['id'] = $response->getSubscriptionId();
            $result['status'] = 1;
            return $result;

        } else {
            $result['message'] = $response->getMessageText();
            $result['status'] = 0;
            return $result;
        }
    }


    public function sampleTransaction()
    {

        $sale = new AuthorizeNetAIM();
        $sale->amount = "5.99";
        $sale->card_num = '6011000000000012';
        $sale->exp_date = '04/15';
        $response = $sale->authorizeAndCapture();
        if ($response->approved) {
            return $transaction_id = $response->transaction_id;
        }
    }

    public function receivePayment($order = array(), $shippingInfo = array(), $billingInfo = array(), $options = array(), $fullResponse = false)
    {
        //var_dump(class_exists('AuthorizeNetAIM')); die();
        $authorize = new AuthorizeNetAIM();

        //Add order details
        $authorize->setField('po_num', $order['id']); //order number

        //Add Billing address
        $authorize->invoice_num = $options['invoice_prefix'] . $order['id']; //package name and id

        $authorize->amount = $order['amount'];

        //Credit card information
        $authorize->card_num = $billingInfo['card_number'];
        $authorize->exp_date = $billingInfo['card_expired'];

        //Customer Billing information
        $authorize->setField('cust_id', $billingInfo['customer_id']);
        $authorize->setField('first_name', $billingInfo['first_name']);
        $authorize->setField('last_name', $billingInfo['last_name']);
        $authorize->setField('address', $billingInfo['address']);
        $authorize->setField('city', $billingInfo['city']);
        $authorize->setField('state', $billingInfo['state']);
        $authorize->setField('zip', $billingInfo['zip']);

        //Shipping info
        $authorize->setField('ship_to_first_name', $billingInfo['first_name']);
        $authorize->setField('ship_to_last_name', $billingInfo['last_name']);
        $authorize->setField('ship_to_address', $shippingInfo['address']);
        $authorize->setField('ship_to_city', $shippingInfo['city']);
        $authorize->setField('ship_to_state', $shippingInfo['state']);
        $authorize->setField('ship_to_zip', $shippingInfo['zip']);
        $authorize->setField('ship_to_country', $shippingInfo['country']);

        // Authorize Only:
        $response['authorize'] = $authorize->authorizeOnly();

        if ($response['authorize']->approved) {
            $response['auth_code'] = $response['authorize']->transaction_id;

            // Now capture:
            $capture = new AuthorizeNetAIM;
            $response['capture_response'] = $capture->priorAuthCapture($response['auth_code']);

            // Now void:
            $void = new AuthorizeNetAIM;
            $response['void_response'] = $void->void($response['capture_response']->transaction_id);
            $response['data'] = array(
                'payment_response_code' => $response['void_response']->response_code,
                'payment_response_reason_code' => $response['void_response']->response_reason_code,
                'payment_response_reason_text' => $response['void_response']->response_reason_text,
                'payment_avs_response' => $response['void_response']->avs_response,
                'payment_authorization_code' => $response['void_response']->authorization_code,
                'payment_invoice_no' => $response['void_response']->invoice_number,
                'payment_transaction_id' => $response['void_response']->transaction_id,
                'payment_transaction_type' => $response['void_response']->transaction_type,
                'payment_account_number' => $response['void_response']->account_number,
                'payment_card_type' => $response['void_response']->card_type,
                'payment_response_text_full' => $response['void_response']->response,
                'payment_final_status' => 1, //Payment received
                'status' => 1, //Order activated
            );
        } else if ($response['authorize']->declined) {
            $response['custom'] = 'declined';
            $response['data'] = array(
                'payment_response_code' => $response['authorize']->response_code,
                'payment_response_reason_code' => $response['authorize']->response_reason_code,
                'payment_response_reason_text' => $response['authorize']->response_reason_text,
                'payment_avs_response' => $response['authorize']->avs_response,
                'payment_authorization_code' => $response['authorize']->authorization_code,
                'payment_invoice_no' => $response['authorize']->invoice_number,
                'payment_transaction_id' => $response['authorize']->transaction_id,
                'payment_transaction_type' => $response['authorize']->transaction_type,
                'payment_account_number' => $response['authorize']->account_number,
                'payment_card_type' => $response['authorize']->card_type,
                'payment_response_text_full' => $response['authorize']->response,
                'payment_final_status' => 2, //Payment declined
                'status' => 2, //Order status Payment declined
            );
        } else if ($response['authorize']->error) {
            $response['custom'] = 'error';
            $response['data'] = array(
                'payment_response_code' => $response['authorize']->response_code,
                'payment_response_reason_code' => $response['authorize']->response_reason_code,
                'payment_response_reason_text' => $response['authorize']->response_reason_text,
                'payment_avs_response' => $response['authorize']->avs_response,
                'payment_authorization_code' => $response['authorize']->authorization_code,
                'payment_invoice_no' => $response['authorize']->invoice_number,
                'payment_transaction_id' => $response['authorize']->transaction_id,
                'payment_transaction_type' => $response['authorize']->transaction_type,
                'payment_account_number' => $response['authorize']->account_number,
                'payment_card_type' => $response['authorize']->card_type,
                'payment_response_text_full' => $response['authorize']->response,
                'payment_final_status' => 3, //Payment error
                'status' => 3, //Order status Payment Error
            );
        } else if ($response['authorize']->held) {
            $response['custom'] = 'held';
            $response['data'] = array(
                'payment_response_code' => $response['authorize']->response_code,
                'payment_response_reason_code' => $response['authorize']->response_reason_code,
                'payment_response_reason_text' => $response['authorize']->response_reason_text,
                'payment_avs_response' => $response['authorize']->avs_response,
                'payment_authorization_code' => $response['authorize']->authorization_code,
                'payment_invoice_no' => $response['authorize']->invoice_number,
                'payment_transaction_id' => $response['authorize']->transaction_id,
                'payment_transaction_type' => $response['authorize']->transaction_type,
                'payment_account_number' => $response['authorize']->account_number,
                'payment_card_type' => $response['authorize']->card_type,
                'payment_response_text_full' => $response['authorize']->response,
                'payment_final_status' => 4, //Payment held
                'status' => 4, //Order status Payment held
            );
        }

        if ($fullResponse === true)
            return $response;

        return $response['data'];
    }

    public function transactionDetails($transactionId)
    {
        $request = new AuthorizeNetTD();
        $response = $request->getTransactionDetails($transactionId);

        if($response->isOk()){
            return $this->oneDimensionArray($this->objectToArray($response->xml->transaction));
        }
        return false;
    }

    /**
     * @param array $array
     * @return array
     */
    public function oneDimensionArray($array = array())
    {
        foreach($array as $key => $value){
            if(is_array($array[$key])){
                foreach($array[$key] as $newKey => $newValue){
                    $data[$key.'_'.$newKey] = $newValue;
                    if(is_array($data[$key.'_'.$newKey])){
                        foreach($data[$key.'_'.$newKey] as $newKey2 => $newValue2){
                            $data[$key.'_'.$newKey.'_'.$newKey2] = $newValue2;
                        }
                        unset($data[$key.'_'.$newKey]);
                    }
                }
                unset($array[$key]);
            }else{
                $data[$key] = $value;
            }
        }
        return $data;
    }

    public function objectToArray($obj)
    {
        if(is_object($obj)) $obj = (array) $obj;
        if(is_array($obj)) {
            $new = array();
            foreach($obj as $key => $val) {
                $new[$key] = self::objectToArray($val);
            }
        }
        else $new = $obj;
        return $new;
    }

}