<?php
class UtilitiesComponent extends Component
{

    /**
     * @var string
     *
     * Name of Utilities Components
     */
    public $name = 'Utilities';

    public $applicationName = 'Golf Swing Prescription';

    /**
     * @param $attachments
     * @return array
     *
     * This function will arrange the attachment into and  array. If attachment empty the return empty array.
     */
    public function manageEmptyAttachment($attachments)
    {
        $isAttachment = array();

        foreach($attachments as $attachment){
            if($attachment['attachment']['name']){
                $isAttachment[] = $attachment;
            }
        }

        return $isAttachment;
    }

    /**
     * @param $status
     * @param $Topic
     * @return string
     *
     * Set flash message according the given status.
     */
    public function getMsgOnChangeStatus($status, $Topic)
    {
        if($Topic == 'Lesson'){
            switch ($status){
                case 1:{
                    return 'Lesson is now become new lesson list';
                    break;
                }
                case 2:{
                    return 'Lesson is now being waiting list';
                    break;
                }
                case 3:{
                    return 'Lesson is now being upcoming list';
                    break;
                }
                case 4:{
                    return 'Lesson has been invalid list';
                    break;
                }
                case 5:{
                    return 'Lesson has been expired list';
                    break;
                }
                case 6:{
                    return 'Lesson has been completed list';
                    break;
                }
                default:
                    return 'Lesson has been updated list';
                    break;
            }
        }
    }

    /**
     * @param $status
     * @return array
     *
     * Setting condition based on different conditions. And return array of that condition.
     */
    public static function setLessonConditions($status)
    {
        $condition = array();
        if($status == 'all'){
            $condition = array();
        }
        elseif($status == 'new'){
            $condition = array('Lesson.status' => 1);
        }
        elseif($status == 'waiting'){
            $condition = array('Lesson.status' => 2);
        }
        elseif($status == 'upcoming'){
            $condition = array('Lesson.status' => 3);
        }
        elseif($status == 'invalid'){
            $condition = array('Lesson.status' => 4);
        }
        elseif($status == 'expired'){
            $condition = array('Lesson.status' => 5);
        }
        elseif($status == 'completed'){
            $condition = array('Lesson.status' => 6);
        }
        elseif($status == 'unpaid'){
            $condition = array('Lesson.payment' => 1);
        }
        elseif($status == 'paid'){
            $condition = array('Lesson.payment' => 2);
        }
        elseif($status == 'requested'){
            $condition = array('Lesson.is_request_for_change_time' => 1);
        }
        elseif($status == 'accepted'){
            $condition = array('Lesson.is_accept_or_deny_time_request' => 1);
        }
        elseif($status == 'declined'){
            $condition = array('Lesson.is_accept_or_deny_time_request' => 2);
        }
        elseif($status == 'benchmark'){
            $condition = array('Lesson.is_benchmark' => 1);
        }

        return $condition;
    }

    /**
     * @param $status
     * @return array
     *
     * Setting condition based on different status. And return array of that condition.
     */
    public static function setUserConditions($status = null)
    {
        $condition = array();
        if($status == 'all'){
            $condition = array();
        }
        elseif($status == 'active'){
            $condition = array('User.status' => 1);
        }
        elseif($status == 'inactive'){
            $condition = array('User.status' => 2);
        }
        elseif($status == 'blocked'){
            $condition = array('User.status' => 3);
        }
        elseif($status == 'banned'){
            $condition = array('User.status' => 4);
        }

        return $condition;
    }

    /**
     * @param $start
     * @param $end
     * @return array
     *
     * Arrange time slot.
     */
    public function arrangeTimeSlot($start, $end)
    {
        $startValue = array_values($start);
        $endValue = array_values($end);
        $counter = 0;
        $newData = array();

        if(sizeof($startValue) > 0){

            while($counter <= sizeof($startValue))
            {
                if(sizeof($newData) == 0){
                    $newData[$counter] = array(
                        'id' => '11',
                        'type' => 'time_slot',
                        'start' => '1990-01-01 00:00:00',
                        'end' => $startValue[$counter]
                    );
                }
                else{
                    if(isset($startValue[$counter])){
                        if(isset($startValue[$counter + 1]))
                        {
                            $newData[$counter+1] = array(
                                'id' => '11',
                                'type' => 'time_slot',
                                'start' => $endValue[$counter],
                                'end' => $startValue[$counter + 1]
                            );
                        }
                        else{
                            $newData[$counter+1] = array(
                                'id' => '11',
                                'type' => 'time_slot',
                                'start' => $endValue[$counter],
                                'end' => '2100-01-01 00:00:00'
                            );
                        }
                    }
                    $counter ++;
                }

            }
        }
        else{
            $newData[$counter] = array(
                'id' => '11',
                'type' => 'time_slot',
                'start' => '1990-01-01 00:00:00',
                'end' => '2100-01-01 00:00:00'
            );
        }

        return $newData;
    }

    /**
     * @param $data
     * @param $userID
     * @return array
     *
     * Arranging the schedule slot.
     */
    public function arrangeScheduleTimes($data, $userID)
    {
        $scheduleDate = $data['scheduleDate'];
        $start = $data['Schedule']['start'];
        $end = $data['Schedule']['end'];
        $counter = 0;
        $countSchedule = 0;
        $singleDaySchedule = array();
        $wholeMonthSchedule = array();

        while(sizeof($start) > $counter){
            if($start[$counter] != 'From' && $end[$counter] != 'To'){
                $starting = date('Y-m-d H:i:s', strtotime($scheduleDate.' '.$start[$counter]));
                $ending = date('Y-m-d H:i:s', strtotime($scheduleDate.' '.$end[$counter]));

                if(strtotime($scheduleDate.' '.$start[$counter]) < strtotime($scheduleDate.' '.$end[$counter])){
                    $singleDaySchedule[$countSchedule] = array(
                        'created_by' => $userID,
                        'start' => $starting,
                        'end' => $ending
                    );
                    $countSchedule ++;
                }
            }
            $counter++;
        }

        if($data['isRepeat'] == true){
            $dayCount = 0;
            while(sizeof($singleDaySchedule) > $dayCount){

                $currentMonth = date('m', strtotime($singleDaySchedule[$dayCount]['start']));
                $nextMonth = $currentMonth;
                $startMonth = $singleDaySchedule[$dayCount]['start'];
                $endMonth = $singleDaySchedule[$dayCount]['end'];

                while($currentMonth == $nextMonth){

                    $counter ++;
                    $startMonth = date('Y-m-d H:i:s', strtotime($startMonth. ' + 7 days'));
                    $endMonth = date('Y-m-d H:i:s', strtotime($endMonth. ' + 7 days'));
                    $nextMonth = date('m', strtotime($startMonth));

                    if($currentMonth == $nextMonth){
                        if(strtotime($startMonth) < strtotime($endMonth)){
                            $wholeMonthSchedule[$counter] = array(
                                'created_by' => $userID,
                                'start' =>  date('Y-m-d H:i:s', strtotime($startMonth)),
                                'end' =>  date('Y-m-d H:i:s', strtotime($endMonth))
                            );
                        }
                    }

                }
                $dayCount++;
            }
        }
        return array_merge($singleDaySchedule, $wholeMonthSchedule);
    }

    /**
     * @return mixed
     *
     */
    public function getBestInstructorList()
    {
        $userModel = ClassRegistry::init('User');
        $lessonModel = ClassRegistry::init('Lesson');

        $data = $lessonModel->find('all',
            array(
                'fields'=>array('Lesson.id', 'COUNT(`Lesson`.`id`) AS totalLesson'),
                'group'=>array('Lesson.created_by'),
                'recursive' => -1
            ))
        ;

        return $data;
    }

    /**
     * @return mixed
     */
    public function getBestStudent()
    {
        $lessonModel = ClassRegistry::init('Lesson');
        $data = $lessonModel->find('all',
            array(
                'fields' => array('Lesson.id', 'COUNT(`Lesson`.`id`) AS totalLesson'),
                'group' => array('Lesson.created_by')
            )
        );

        return $data;
    }

    /**
     *
     */
    public function bestBenchmarkList()
    {
        $benchmarkModel = ClassRegistry::init('Benchmark');
        $data = $benchmarkModel->find('all',
            array()
        );
    }

    /**
     * @param $packages
     * @return array
     */
    public function calculatePrice($packages){
        $subtotal = 0.00;
        $tax = 0.00;
        foreach($packages as $package){
            $subtotal = $package['Package']['price'] + $subtotal;
            $tax = $package['Package']['tax'] + $tax;
        }

        $total = $tax + $subtotal;
        $cost = array(
            'subtotal' => $subtotal,
            'tax' => $tax,
            'total' => $total,
        );
        return $cost;
    }

    /**
     * @param $gl
     * @return bool
     */
    public function isGLExpired($gl){
        if(strtotime($gl['GroupLesson']['start']) < strtotime(date('Y-m-d H:i:s'))){
            return true;
        }
        return false;
    }

    /**
     * @param $gl
     * @return bool
     */
    public function isGLFilledUp($gl){
        if($gl['GroupLesson']['student_limit'] <= $gl['GroupLesson']['filled_up_student']){
            return true;
        }
        return false;
    }

    /**
     * @param $gl
     * @return bool
     */
    public function isGLInvalid($gl){
        if($gl['GroupLesson']['status'] != 1){
            return true;
        }
        return false;
    }


} 