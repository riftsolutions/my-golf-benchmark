<?php
App::uses('CakeEmail', 'Network/Email');
class EmailComponent extends Component
{

    public $name = 'Email';

    /**
     * @param string $transporter
     * @param string $template
     * @param $to
     * @param null $subject
     * @param null $link
     * @return bool
     *
     * This is the mani/core function to send email. This function is set up all the email configuration.
     * So this function is be able to use from anywhere of this application.
     */
    public function sendEmail(
        $transporter = 'mandrill',
        $template = 'golf-swing-prescription',
        $to,
        $subject = null,
        $link = null
    ) {
        if ($subject == null) {
            $subject = 'Golf Swing Prescription Official Email';
        }
        $cakeEmail = new CakeEmail($transporter);
        $cakeEmail->template($template)
            ->emailFormat('html')
            ->subject($subject)
            ->to($to)
            ->from('support@foutsventures.com', 'Golf Swing Prescription Account')
            ->replyTo('no-reply@foutsventures.com')
            ->viewVars(
                array(
                    'link' => $link
                )
            );
        if ($cakeEmail->send()) {
            return true;
        }
        return false;
    }

    /**
     * @param string $transporter
     * @param string $template
     * @param $to
     * @param null $subject
     * @param null $var
     * @return bool
     */
    public function notificationEmail($transporter = 'mandrill', $template = 'golf-swing-prescription', $to, $subject = null, $var = null) {
        if ($subject == null) {
            $subject = 'Golf Swing Prescription Official Email';
        }
        $cakeEmail = new CakeEmail($transporter);
        $cakeEmail->template($template)
            ->emailFormat('html')
            ->subject($subject)
            ->to($to)
            ->from('support@foutsventures.com', 'Golf Swing Prescription Account')
            ->replyTo('no-reply@foutsventures.com')
            ->viewVars(
                array(
                    'var' => $var
                )
            );
        if ($cakeEmail->send()) {
            return true;
        }
        return false;
    }

} 