<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 9/10/15
 * Time: 3:12 AM
 */

class AppsComponent extends Component
{
    public $name = 'Apps';

    /**
     * @param $users
     * @param $instructorID
     * @return array
     */
    public function arrangeUserList($users, $instructorID)
    {
        foreach($users as $user)
        {
            $emails[] = $user['User']['Email'];
        }

        $userModel = ClassRegistry::init('User');
        $exiestingUser = $userModel->checkExiestByEmails($emails);

        foreach($users as $user)
        {
            $username = $user['User']['Email'];
            $duplicate = false;
            if(in_array($username, $exiestingUser))
            {
                $duplicate = true;
            }

            $userList[] = array(
                'User' => array(
                    'username' => $username,
                    'status' => $user['User']['User Status'],
                    'role' => $user['User']['User Role'],
                    'last_login' => $user['User']['User Last Login'],
                    'password' => '55e77dfb-4a34-420d-93b0-33bf7f000101' // Password = 123456
                ),
                'Profile' => array(
                    'first_name' => $user['User']['Profile First Name'],
                    'last_name' => $user['User']['Last Name'],
                    'instructor_id' => $instructorID,
                    'phone' => $user['User']['Phone Number'],
                    'fax' => $user['User']['fax'],
                    'primary_email' => $user['User']['primary_email'],
                    'secondary_email' => $user['User']['secondary_email'],
                    'city' => $user['User']['city'],
                    'state' => $user['User']['state'],
                    'postal_code' => $user['User']['postal_code'],
                    'street_1' => $user['User']['street_1'],
                    'street_2' => $user['User']['street_2'],
                ),
                'CountLesson' => array(
                    'purchased_lesson_count' => $user['User']['Purchased Lesson'],
                    'appointed_lesson_count' => $user['User']['Appointed Lesson'],
                    'lesson_left' => $user['User']['Lesson Left'],
                ),
                'duplicate' => $duplicate
            );
        }
        return $userList;
    }

    /**
     * @param $array
     * @param $key
     * @param $value
     * @return array
     */
    public  function searchArrayValue($array, $key, $value)
    {
        $results = array();
        if (is_array($array)) {
            if (isset($array[$key]) && $array[$key] == $value) {
                $results[] = $array;
            }
            foreach ($array as $subArray) {
                $results = array_merge($results, $this->searchArrayValue($subArray, $key, $value));
            }
        }
        return $results;
    }
} 