<?php
App::uses('AppController', 'Controller');
/**
 * Class AppointmentsController
 *
 * @property Appointment $Appointment..
 * @property Attachment @Attachment
 * @property Benchmark $Benchmark
 * @property Billing $Billing
 * @property BillingsPackage @BillingsPackage
 * @property BlockTime @BlockTime
 * @property Card $Card
 * @property Config $Config
 * @property Contact $Contact
 * @property CountLesson $CountLesson
 * @property EmailsEvent $EmailsEvent
 * @property EmailsEventsSetting $EmailsEventsSetting
 * @property Event $Event
 * @property GroupLesson $GroupLesson
 * @property GroupLessonsStudents $GroupLessonsStudents
 * @property Note @Note
 * @property Package $Package
 * @property PackagesUser $PackagesUser
 * @property PackagesOrder $PackagesOrder
 * @property Profile $Profile
 * @property Source $Source
 * @property TcsUser $TcsUser
 * @property User $User
 * @property Waiting $Waiting
 *
 * @property PaginatorComponent $Paginator
 * @property EmailComponent $Email
 * @property UtilitiesComponent $Utilities
 * @property AuthorizeNetComponent $AuthorizeNet
 * @property DataTableComponent $DataTable
 * @property ImgComponent $Img
 * @property AppsComponent $Apps
 *
 */

class NotificationsController extends AppController
{
    /**
     * The name of this controller.
     *
     * @var string
     */
    public $name = 'Notifications';

    /**************************************************************************************************************/
    /********************************************        Admin Panel          *************************************/
    /**************************************************************************************************************/

    /**
     *
     */
    public function admin_index()
    {

    }

    /**************************************************************************************************************/
    /******************************************       Instructor Panel         ************************************/
    /**************************************************************************************************************/

    /**
     *
     */
    public function instructor_email()
    {
        $this->set('title_for_layout', 'Email Notification - '.$this->Utilities->applicationName);
        $events = $this->EmailsEventsSetting->getEmailSettingByUserID($this->userID);
        $this->set('events', $events);
    }

    /**
     *
     */
    public function instructor_saveNotifications()
    {
        $this->saveNotification();
    }

    /**************************************************************************************************************/
    /******************************************         Student Panel         *************************************/
    /**************************************************************************************************************/

    /**
     *
     */
    public function student_index()
    {

    }

    /**
     *
     */
    public function student_saveNotifications()
    {
        $this->saveNotification();
    }

    /**************************************************************************************************************/
    /******************************************       Custom Function          ************************************/
    /**************************************************************************************************************/

    /**
     *
     */
    private function saveNotification()
    {
        $this->autoLayout = false;
        $this->autoRender = false;
        $this->response->type('application/javascript');

        $notificationSettingID = $this->request->data['id'];
        $data = $this->EmailsEventsSetting->getEmailSettingByID($notificationSettingID);

        if($this->request->data['value'] == 1){
            $value = true;
        }
        else{
            $value = false;
        }


        if($data['EmailsEvent']['is_mandatory'] == false){
            $this->EmailsEventsSetting->id = $notificationSettingID;
            if($this->EmailsEventsSetting->saveField('value', $value)){
                $this->response->body(json_encode(1));
            }
            else{
                $this->response->body(json_encode(0));
            }
        }
        else{
            $this->response->body(json_encode('mandatory'));
        }

    }

    /**
     * @param $notification
     * @return bool
     *
     * This function is storing the email notifications setting.
     */
    private function saveNotificationSettings($notification)
    {
        $whenAddStudent = 0;
        if(isset($notification['when_add_student'])){
            $whenAddStudent = 1;
        }

        $whenLessonConfirm = 0;
        if(isset($notification['when_lesson_confirm'])){
            $whenLessonConfirm = 1;
        }

        $whenLessonCancelled = 0;
        if(isset($notification['when_lesson_cancelled'])){
            $whenLessonCancelled = 1;
        }

        $whenLessonRescheduled = 0;
        if(isset($notification['when_lesson_rescheduled'])){
            $whenLessonRescheduled = 1;
        }

        $whenAfterBenchmark = 0;
        if(isset($notification['when_after_benchmark'])){
            $whenAfterBenchmark = 1;
        }

        $whenOneHourRemaining = 0;
        if(isset($notification['when_one_hour_remaining'])){
            $whenOneHourRemaining = 1;
        }

        $whenForgotPassword = 1;
        $whenResetPassword = 1;

        $notificationList = array(
            'when_add_student' => $whenAddStudent,
            'when_lesson_confirm' => $whenLessonConfirm,
            'when_lesson_cancelled' => $whenLessonCancelled,
            'when_lesson_rescheduled' => $whenLessonRescheduled,
            'when_after_benchmark' => $whenAfterBenchmark,
            'when_one_hour_remaining' => $whenOneHourRemaining,
            'when_forgot_password' => $whenForgotPassword,
            'when_reset_password' => $whenResetPassword,
        );

        $saveCounter = 0;
        foreach($notificationList as $key => $value){
            if($this->storeKeyValue($key, $value, $this->userID)){
                $saveCounter = $saveCounter + 1;
            }
        }

        if($saveCounter == sizeof($notificationList)){
            return true;
        }
        return false;
    }

    /**
     * @param $key
     * @param $value
     * @param $userID
     * @param array $option
     * @return bool
     *
     * This function will save the config as key/value style
     */
    private function storeKeyValue($key, $value, $userID, $option = array())
    {
        $data = array(
            'user_id' => $userID,
            'key' => $key,
            'value' => $value,
        );

        $isExiest = $this->Config->getConfig($key, $userID);
        if($isExiest){
            $this->Config->id = $isExiest['id'];
        }
        else{
            $this->Config->create();
        }

        if($this->Config->save($data)){
            $this->storeConfigIntoSession($key, $value);
            return true;
        }
        return false;
    }

    /**
     * @param $key
     * @param $value
     *
     * Configuration is storing into session.
     */
    private function storeConfigIntoSession($key, $value)
    {
        $this->Session->write('Configuration.'.$key, $value);
    }
}
