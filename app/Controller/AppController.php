<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 *
 * Class AppController
 *
 * @property Appointment $Appointment..
 * @property Attachment @Attachment
 * @property Benchmark $Benchmark
 * @property Billing $Billing
 * @property Card $Card
 * @property Config $Config
 * @property Contact $Contact
 * @property Event $Event
 * @property Note @Note
 * @property Package $Package
 * @property Profile $Profile
 * @property PackagesUser $PackagesUser
 * @property PackagesOrder $PackagesOrder
 * @property User $User
 * @property Waiting $Waiting
 *
 * @property PaginatorComponent $Paginator
 * @property EmailComponent $Email
 * @property UtilitiesComponent $Utilities
 * @property AuthorizeNetComponent $AuthorizeNet
 * @property DataTableComponent $DataTable
 * @property ImgComponent $Img
 *
 */

App::uses('Controller', 'Controller');


/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

    /**
     * The name of this controller.
     *
     * @var string
     */
    public $name = 'App';

    /*
     * facebook ApiID = 1645244829055897
     * facebook Api SecretKey = 7d0ec4407bb39dbb21167ea5269af7f8
     */

    public $facebookApiID = '1661354900778223';

    public $facebookApsSecretKey = '246e9e91fce3bcd8959577eaba3f43cf';

    public $costForCreateAccount = 10.00;


    /**
     * These application is using these component list. Someone is built in with cakePHP someone is custom.
     *
     * @var array
     */
    public $components = array('DebugKit.Toolbar', 'Auth', 'Session', 'Cookie', 'RequestHandler', 'Email', 'Utilities', 'Img', 'Paginator', 'DataTable', 'AuthorizeNet', 'CSV.Csv', 'CsvView.CsvView', 'Apps');

    /**
     * These application is using these helpers list. Someone is built in with cakePHP someone is custom.
     *
     * @var array
     */
    public $helpers = array('Html', 'Form', 'Js', 'Text', 'Time', 'Utilities', 'DataTable', 'GoogleCharts.GoogleCharts');

    /**
     * This application is using these model for handle it's activities.
     *
     * @var array
     */
    public $uses = array('User', 'Profile', 'Contact', 'Lesson', 'Attachment', 'Schedule', 'Benchmark', 'Event', 'Waiting', 'Schedule', 'Appointment', 'UsersLessons', 'Package', 'PackagesUser', 'Config', 'Card','Billing', 'Note', 'BillingsPackage', 'PackagesOrder', 'Source', 'EmailsEvent', 'EmailsEventsSetting','TcsUser', 'BillingsPackage', 'GroupLesson', 'CountLesson', 'BlockTime', 'GroupLessonsStudents', 'BillingsPeriod', 'RangeWaiting', 'WaitingQueue' , 'TcsUser' , 'Feedback');

    /**
     * @var
     *
     * public variable for store userID.
     */
    public $userID;
    public $userRole;
    /**
     * @var int
     */
    public $lessonLimit = 10;

    public $limit = 25;

    public $Facebook;


    /**
     * Do/Execute these activities before do anything of this application for each action for each controller.
     */
    public function beforeFilter()
    {
        $this->userID = (int)$this->Session->read('Auth.User.id');
        $this->userRole = (int)$this->Session->read('Auth.User.role');
        $this->set('userID', (int)$this->Session->read('Auth.User.id'));
        $this->Auth->allowedActions = array(
            'signup',
            'payment',
			'plan_select',
            'forgot_password',
            'reset_password',
            'getNotAppointedEvents',
            'createEvents',
            'getContacts',
            'confirm' ,
            'waiting_confirm'
        );

        if($this->userID){
            if($this->Session->check('isPackageExpiredChecked') == false){
                $this->checkExpiredPackage();
            }

            if($this->Session->check('isAppointmentExpiredChecked') == false){
                $this->checkExpiredAppointment();
            }

            if($this->Session->check('isSlippingAwayChecked') == false){
                $students = array_keys($this->Profile->getStudentForInstructor('list', $this->userID));
                if($students)
                {
                    $this->managerSlippingAway($students);
                }
            }

            if($this->Session->check('isSettedEmailNotification') == false){
                $this->isSettedEmailNotifications();
            }


            if($this->Session->read('Auth.User.role') == 2)
            {
                $this->set('hasBenchmark', $this->Benchmark->countBenchmark($this->userID));
                $this->set('countNotifications', $this->Waiting->countWaitingNotification(array('conditions' => array('Waiting.appointment_by' => $this->userID, 'Waiting.status' => 2))));
            }
        }
    }

    /**
     * Do/Execute these activities before render any view.
     */
    public function beforeRender()
    {
        $this->themeSwitcher();
        $this->layoutSwitcher();
        $userInfo = $this->Profile->getProfile('first', array('user_id' => $this->userID));
        $this->set('userInfo', $userInfo);
    }

    /**
     * Do/Execute these lines of codes after render a view.
     */
    public function afterFilter()
    {
        $this->manageRoutes();
    }

    /**
     * Check the prefix in URL, if the prefix is mission/empty then user will redirected to their panel
     * according to user role.
     */
    public function manageRoutes()
    {
        if(!isset($this->request->params['prefix'])){
            if($this->request->params['controller'] != 'calendars' && $this->request->params['controller'] != 'stripe'){
                $this->redirectUserByRole();
            }
        }
    }



    /**
     * Redirect user to their dashboard according to their role.
     *
     * If users role is 1 (admin) then they will redirect into admin dashboard
     * If users role is 2 (student) then they will redirect into student dashboard
     * If users role is 3 (instructor) then they will redirect into instructor dashboard
     */
    public function redirectUserByRole()
    {
        $userRole = $this->Session->read('Auth.User.role');
        if($userRole == 1){
            $this->redirect(array('controller' => 'dashboards', 'action' => 'index', 'admin' => true));
        }
        elseif($userRole == 2){
            $this->redirect(array('controller' => 'dashboards', 'action' => 'index', 'student' => true));
        }
        elseif($userRole == 3){
            $this->redirect(array('controller' => 'dashboards', 'action' => 'index', 'instructor' => true));
        }
        elseif($userRole == 4){
            $this->redirect(array('controller' => 'dashboards', 'action' => 'index', 'tc' => true));
        }
    }

    /**
     * Application will switch them according to user role.
     *
     * If users role is 1 (admin) then application the will be 'Admin'
     * If users role is 2 (student) then application the will be 'Student'
     * If users role is 3 (instructor) then application the will be 'Instructor'
     */
    public function themeSwitcher()
    {
        if(isset($this->request->params['prefix'])){
            if ($this->request->params['prefix'] == 'admin') {
                $this->theme = 'Admin';
            }
            elseif ($this->request->params['prefix'] == 'student') {
                $this->theme = 'Student';
            }
            elseif ($this->request->params['prefix'] == 'instructor') {
                $this->theme = 'Instructor';
            }
            elseif ($this->request->params['prefix'] == 'tc') {
                $this->theme = 'Tc';
            }
            else {

            }
        }
        else{
            $this->theme = 'Admin';
        }
    }

    /**
     * Application will switch theme layout according to many situation.
     *
     */
    public function layoutSwitcher()
    {
        if ($this->request->params['action'] == 'login' ||$this->request->params['action'] == 'payment' || $this->request->params['action'] == 'signup' || $this->request->params['action'] == 'plan_select' || $this->request->params['action'] == 'forgot_password' || $this->request->params['action'] == 'reset_password') {
            $this->layout = 'access-point-layout';

        } else {
            $this->layout = 'application-layout';
        }
    }

    /**
     * @param $filename
     * @param $document
     * @param $path
     * @return bool|string
     *
     * This function will store the document to given path,
     */
    public function uploadImage($filename, $document, $path)
    {
        $ext = $this->Img->ext($document['name']);
        $origFile = $filename. '.'. $ext;
        $targetDir = WWW_ROOT . $path;
        $upload = $this->Img->upload($document['tmp_name'], $targetDir, $origFile);
        if ($upload == 'Success') {
            return $origFile;
        }
        return false;
    }

    /**
     * @param $document
     * @param $path
     * @return bool
     */
    public function uploadDocument($document, $path)
    {
        $filename = $document['tmp_name'];
        $targetDirectory = $path.DS.$document['name'];
        if(move_uploaded_file($filename, $targetDirectory)){
            return true;
        }
        return false;
    }

    /**
     * This function checking the expired packages for student and make them into expired packages.
     */
    public function checkExpiredPackage()
    {
        $today = (array) new DateTime("now");
        $userRole = $this->Session->read('Auth.User.role');
        $conditions = array('PackagesUser.expiration_date <' => $today['date'], 'PackagesUser.status !=' => 3);

        if($userRole == 2){
            $conditions = array_merge($conditions, array('PackagesUser.created_for' => $this->userID));
        }

        if($userRole == 3){
            $conditions = array_merge($conditions, array('PackagesUser.instructor_id' => $this->userID));
        }

        $ids = $this->PackagesUser->find('list', array('conditions' => $conditions));

        foreach($ids as $key => $value){
            $this->PackagesUser->id = $key;
            $this->PackagesUser->saveField('status', 3);
        }

        $this->Session->write('isPackageExpiredChecked', true);
    }

    /**
     * This function will checking the expired appointment and make them into expired.
     */
    public function checkExpiredAppointment()
    {
        $today = (array) new DateTime("now");
        $conditions = array('Appointment.end <' => $today['date'], 'Appointment.status !=' => 5);
        $ids = $this->Appointment->find('list', array('conditions' => $conditions));

        foreach($ids as $key => $value){
            $this->Appointment->id = $key;
            $this->Appointment->saveField('status', 5);
        }
        $this->Session->write('isAppointmentExpiredChecked', true);
    }

    /**
     * @param $users
     *
     * This function is manage tje slipping away student's
     */
    public function managerSlippingAway($users)
    {
        foreach($users as $key => $userID)
        {
            $appointment = $this->Appointment->find(
                'first',
                array(
                    'recursive' => -1,
                    'conditions' => array(
                        'Appointment.appointment_by' => $userID
                    ),
                    'order' => 'Appointment.id DESC',
                )
            );

            $slippingAwayTime = strtotime(date("Y-m-d H:i:s", strtotime("-2 week")));
            if($appointment){
                if($slippingAwayTime > strtotime($appointment['Appointment']['start'])){
                    $this->User->changeSlippingAway($userID, 1);
                }
                else{
                    $this->User->changeSlippingAway($userID, 0);
                }
            }
            else{
                $this->User->changeSlippingAway($userID, 1);
            }
        }
        $this->Session->write('isSlippingAwayChecked', true);
    }

    /**
     * This function is checking user either set his email notification or not. If not then it will stored by default
     * setting to that user.
     */
    public function isSettedEmailNotifications()
    {
        $userRole = $this->Session->read('Auth.User.role');
        if($userRole == 3){

            $result = $this->EmailsEventsSetting->getEmailSettingByUserID($this->userID);
            if($result == null){
                $events = $this->EmailsEvent->getAllEvent();
                foreach($events as $event){
                    $emailSettingValues[] = array(
                        'emails_event_id' => $event['EmailsEvent']['id'],
                        'user_id' => $this->userID,
                        'value' => $event['EmailsEvent']['value'],
                        'enable_for_student' => $event['EmailsEvent']['enable_for_student']
                    );
                }

                $this->EmailsEventsSetting->saveAll($emailSettingValues);
            }
            else{
                $this->Session->write('isSettedEmailNotification', true);
            }
        }
        elseif($userRole == 2){
            $result = $this->EmailsEventsSetting->getEmailSettingByUserID($this->userID);
            if($result == null){
                $events = $this->EmailsEventsSetting->getEmailSettingByUserID($this->Session->read('Auth.User.Profile.instructor_id'), $enable = true);
                if($events){
                    foreach($events as $event){
                        $emailSettingValues[] = array(
                            'emails_event_id' => $event['EmailsEventsSetting']['emails_event_id'],
                            'user_id' => $this->userID,
                            'value' => $event['EmailsEventsSetting']['value'],
                            'enable_for_student' => $event['EmailsEventsSetting']['enable_for_student']
                        );
                    }
                }
                else{
                    $events = $this->EmailsEvent->getAllEvent();
                    foreach($events as $event){
                        if($event['EmailsEvent']['enable_for_student'] == true){
                            $emailSettingValues[] = array(
                                'emails_event_id' => $event['EmailsEvent']['id'],
                                'user_id' => $this->userID,
                                'value' => $event['EmailsEvent']['value'],
                                'enable_for_student' => $event['EmailsEvent']['enable_for_student']
                            );
                        }
                    }
                }

                $this->EmailsEventsSetting->saveAll($emailSettingValues);
            }
            else{
                $this->Session->write('isSettedEmailNotification', true);
            }
        }
    }

    /**
     * @param $emailEvent
     * @param array $options
     * @return bool|null
     *
     * Sending email notification to user based on situation.
     */
    public function sendEmailNotification($emailEvent, $options = array())
    {
        switch($emailEvent){
            case 'when_add_student':{

                if(isset($options['userID'])){
                    $userIds = $options['userID'];
                    $addedUserID = $options['new_user'];
                    $user = $this->User->getUser($addedUserID);
                    $emails = $this->EmailsEventsSetting->getEmailsByCheckingNotificationSettings($emailEvent, $userIds);
                    if($emails){
                        $counter = 0;
                        foreach($emails as $key => $value){
                            if($this->Email->notificationEmail('mandrill', 'add_new_user', $value, 'New Student has been added', $user)){
                                $counter++;
                            }
                        }
                        if(sizeof($emailEvent) == $counter){
                            return true;
                        }
                    }
                    return false;
                }
                break;
            }
            case 'when_lesson_confirm':{
                if(isset($options['appointmentID'])){
                    $appointmentID = $options['appointmentID'];
                    $appointmentDetails = $this->Appointment->getAppointmentByIDForNotification($appointmentID);
                    $ids[] = $appointmentDetails['Appointment']['appointment_by'];
                    $ids[] = $appointmentDetails['Appointment']['instructor_id'];
                    $emails = $this->EmailsEventsSetting->getEmailsByCheckingNotificationSettings($emailEvent, $ids);
                    if($emails){
                        $counter = 0;
                        foreach($emails as $key => $value){
                            if($this->Email->notificationEmail('mandrill', 'appointment_confirm', $value, 'Appointment has been confirmed', $appointmentDetails)){
                                $counter++;
                            }
                        }

                        if(sizeof($emailEvent) == $counter){
                            return true;
                        }
                    }

                    return false;
                }
                break;
            }
            case 'when_lesson_cancelled':{
                if(isset($options['appointmentID'])){
                    $appointmentID = $options['appointmentID'];
                    $appointmentDetails = $this->Appointment->getAppointmentByIDForNotification($appointmentID);
                    $ids[] = $appointmentDetails['Appointment']['appointment_by'];
                    $ids[] = $appointmentDetails['Appointment']['instructor_id'];
                    $emails = $this->EmailsEventsSetting->getEmailsByCheckingNotificationSettings($emailEvent, $ids);
                    if($emails){
                        $counter = 0;
                        foreach($emails as $key => $value){
                            if($this->Email->notificationEmail('mandrill', 'appointment_cancel', $value, 'Appointment has been cancelled', $appointmentDetails)){
                                $counter++;
                            }
                        }

                        if(sizeof($emailEvent) == $counter){
                            return true;
                        }
                    }
                    return false;
                }
                break;
            }
            case 'when_lesson_rescheduled':{

                if(isset($options['appointmentID'])){
                    $appointmentID = $options['appointmentID'];
                    $appointmentDetails = $this->Appointment->getAppointmentByIDForNotification($appointmentID);
                    $ids[] = $appointmentDetails['Appointment']['appointment_by'];
                    $ids[] = $appointmentDetails['Appointment']['instructor_id'];
                    $emails = $this->EmailsEventsSetting->getEmailsByCheckingNotificationSettings($emailEvent, $ids);
                    if($emails){
                        $counter = 0;
                        foreach($emails as $key => $value){
                            if($this->Email->notificationEmail('mandrill', 'appointment_reschedule', $value, 'Appointment has been rescheduled', $appointmentDetails)){
                                $counter++;
                            }
                        }

                        if(sizeof($emailEvent) == $counter){
                            return true;
                        }
                    }
                    return false;
                }
                break;
            }
            case 'when_after_benchmark':{
                if(isset($options['benchmarkID'])){
                    $benchmarkID = $options['benchmarkID'];
                    $benchmarkDetails = $this->Benchmark->geBenchmarkByID($benchmarkID);
                    $ids[] = $benchmarkDetails['Benchmark']['created_by'];
                    $ids[] = $benchmarkDetails['Benchmark']['created_for'];
                    $emails = $this->EmailsEventsSetting->getEmailsByCheckingNotificationSettings($emailEvent, $ids);
                    if($emails){
                        $counter = 0;
                        foreach($emails as $key => $value){
                            if($this->Email->notificationEmail('mandrill', 'after_benchmark', $value, 'Benchmark has been completed', $benchmarkDetails)){
                                $counter++;
                            }
                        }

                        if(sizeof($emailEvent) == $counter){
                            return true;
                        }
                    }
                    return false;
                }
                break;
            }
            case 'when_one_hour_remaining':{
                break;
            }
            case 'when_reset_password':{
                if(isset($options['userID'])){
                    $userIds = $options['userID'];
                    $emails = $this->EmailsEventsSetting->getEmailsByCheckingNotificationSettings($emailEvent, $userIds);
                    if($emails){
                        $counter = 0;
                        foreach($emails as $key => $value){
                            if($this->Email->notificationEmail('mandrill', 'change_password', $value, 'Password has been Changed', null)){
                                $counter++;
                            }
                        }
                        if(sizeof($emailEvent) == $counter){
                            return true;
                        }
                    }
                    return false;
                }
                break;
            }
            default:
                return null;
        }
    }
    /**
     * @param $appointmentID
     * This method insert waiting information in waiting queue
     * based on appointment id
     */
    public function waitingQueueWhenAppointmentCancelled($appointmentID){
        $this->autoLayout = false;
        $this->autoRender = false;

        $waitingLists = $this->Waiting->find('all', array(
            'conditions' => array(
                'appointment_id' => $appointmentID
            ),
            'order' => array('Waiting.id ASC'),
        ));
        //var_dump($waitingLists); die();
        $checkWaitingList = sizeof($waitingLists);
        if($checkWaitingList > 0) {
            $min = 5;
            foreach ($waitingLists as $waitingList):
                $valMin = $min + 5;
                $db = $this->WaitingQueue->getDataSource();
                $this->WaitingQueue->create();
                $this->WaitingQueue->set('appointment_id', $waitingList['Waiting']['appointment_id']);
                $this->WaitingQueue->set('waiting_id', $waitingList['Waiting']['id']);
                $this->WaitingQueue->set('user_id', $waitingList['Waiting']['appointment_by']);
                $this->WaitingQueue->set('queue_time', $db->expression('DATE_ADD(NOW(), INTERVAL ' . $min . ' MINUTE)'));
                $this->WaitingQueue->set('validity_period', $db->expression('DATE_ADD(NOW(), INTERVAL ' . $valMin . ' MINUTE)'));
                $this->WaitingQueue->set('response_code', substr(md5(uniqid(mt_rand(), true)), 0, 8));
                $this->WaitingQueue->save();
                $min = $min + 5;
            endforeach;
        }else{
            return false;
        }
    }

}