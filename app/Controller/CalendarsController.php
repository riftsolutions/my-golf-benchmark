<?php
App::uses('AppController', 'Controller');

/**
 * Class AppointmentsController
 *
 * @property Appointment $Appointment..
 * @property Attachment @Attachment
 * @property Benchmark $Benchmark
 * @property Billing $Billing
 * @property BillingsPackage @BillingsPackage
 * @property BlockTime @BlockTime
 * @property Card $Card
 * @property Config $Config
 * @property Contact $Contact
 * @property CountLesson $CountLesson
 * @property EmailsEvent $EmailsEvent
 * @property EmailsEventsSetting $EmailsEventsSetting
 * @property Event $Event
 * @property GroupLesson $GroupLesson
 * @property GroupLessonsStudents $GroupLessonsStudents
 * @property Note @Note
 * @property Package $Package
 * @property PackagesUser $PackagesUser
 * @property PackagesOrder $PackagesOrder
 * @property Profile $Profile
 * @property Source $Source
 * @property TcsUser $TcsUser
 * @property User $User
 * @property Waiting $Waiting
 * @property Waiting $RangeWaiting
 * @property PaginatorComponent $Paginator
 * @property EmailComponent $Email
 * @property UtilitiesComponent $Utilities
 * @property AuthorizeNetComponent $AuthorizeNet
 * @property DataTableComponent $DataTable
 * @property ImgComponent $Img
 * @property AppsComponent $Apps
 *
 */
class CalendarsController extends AppController
{

    public $name = 'Calendars';

    /**************************************************************************************************************/
    /********************************************        Admin Panel          *************************************/
    /**************************************************************************************************************/

    /**************************************************************************************************************/
    /******************************************       Instructor Panel         ************************************/
    /**************************************************************************************************************/

    /**
     * Index page of Calendar for the instructor.
     */
    public function instructor_index()
    {
        $this->set('title_for_layout', 'My Calendar - ' . $this->Utilities->applicationName);
        $this->set('instructors', $this->User->getUserByRole('all', 3));


        $from = date('Y-m-d', strtotime('first day of this month'));
        $to = date('Y-m-d', strtotime('last day of this month'));
        $conditions = array('Event.created_by' => $this->userID, 'Event.start >' => $from, 'Event.start <' => $to);
        $events = $this->Event->find('all', array('conditions' => $conditions, 'recursive' => -1));
        $this->set('events', $events);
    }

    public function instructor_legend()
    {
        $this->set('title_for_layout', 'legend - ' . $this->Utilities->applicationName);
    }

    public function instructor_new()
    {
        $this->set('title_for_layout', 'My Calendar - ' . $this->Utilities->applicationName);
        $this->set('instructors', $this->User->getUserByRole('all', 3));
    }

    public function instructor_student()
    {
        $this->set('title_for_layout', 'My Calendar - ' . $this->Utilities->applicationName);
    }

    /**************************************************************************************************************/
    /******************************************         Student Panel         *************************************/
    /**************************************************************************************************************/

    /**
     * Index page of Calendar for instructor
     */
    public function student_index()
    {
        $this->set('title_for_layout', 'My Calendar - ' . $this->Utilities->applicationName);
        $packages = $this->PackagesUser->getUsersPackageList($this->userID);
        $appointments = $this->Appointment->getCancelledAppointment($this->userID);

        $payAtFacilities = $this->Package->getPayAtFacility($this->userID);
        $packages = array_merge($packages, $payAtFacilities/*, array('booked' => 'Just Want to Booked')*/);
        $this->set('packages', $packages);
        $this->set('appointments', $appointments);
    }

    public function student_legend()
    {
        $this->set('title_for_layout', 'legend - ' . $this->Utilities->applicationName);
    }

    /**************************************************************************************************************/
    /******************************************         TC Panel         *************************************/
    /**************************************************************************************************************/

    /**
     * Index page of Calendar for the student.
     */
    public function tc_index()
    {
        $this->set('title_for_layout', 'My Calendar - ' . $this->Utilities->applicationName);

        $getTcInstructors = $this->TcsUser->getInstructorIdsByTCID($this->userID);
        $this->set('instructors', $getTcInstructors);

        $from = date('Y-m-d', strtotime('first day of this month'));
        $to = date('Y-m-d', strtotime('last day of this month'));
        $conditions = array('Event.created_by' => $this->userID, 'Event.start >' => $from, 'Event.start <' => $to);
        $events = $this->Event->find('all', array('conditions' => $conditions, 'recursive' => -1));
        $this->set('events', $events);

    }

    public function tc_legend()
    {
        $this->set('title_for_layout', 'legend - ' . $this->Utilities->applicationName);
    }

    public function getTCEvents()
    {
        $instructorsIds = $this->request->data['instructors'];
        $start = date('Y-m-d H:i:s', strtotime($this->request->data('start')));
        $end = date('Y-m-d H:i:s', strtotime($this->request->data('end')));
        $appointments = $this->Appointment->getAppointmentOfTC($instructorsIds, $start, $end);

        $groupLessons = $this->GroupLesson->getGroupLesson(array('start' => $start, 'end' => $end,
            'conditions' => array('GroupLesson.user_id' => $instructorsIds)
        ));

        $blockTimes = $this->BlockTime->getBlockTime(array(
            'start' => $start,
            'end' => $end,
            'conditions' => array('BlockTime.user_id' => $instructorsIds)
        ));

        $result = array_merge($appointments, $groupLessons, $blockTimes);

        $this->autoLayout = false;
        $this->autoRender = false;
        $this->response->type('application/javascript');

        if($appointments && $instructorsIds)
        {
            $this->response->body(json_encode($result));
        }
        else{
            $this->response->body(json_encode(null));
        }

    }

    /**************************************************************************************************************/
    /******************************************       Custom Function          ************************************/
    /**************************************************************************************************************/

    /******************* Getting events for the Calendar for the instructor ***********************/

    /**
     *
     */
    public function createEvents()
    {
        $this->autoLayout = false;
        $this->autoRender = false;
        $this->response->type('application/javascript');

        if ($this->request->data['title']) {
            $eventInfo = array(
                'uuid' => String::uuid(),
                'created_by' => $this->userID,
                'title' => $this->request->data['title'],
                'description' => $this->request->data['description'],
                'start' => date('Y-m-d H:i:s', strtotime($this->request->data['start'])),
                'end' => date('Y-m-d H:i:s', strtotime($this->request->data['end'])),
                'allDay' => $this->request->data['allDay'],
            );
            if ($this->Event->save($eventInfo)) {
                $this->response->body(json_encode('done'));
            } else {
                $this->response->body(json_encode('problem'));
            }
        } else {
            $this->response->body(json_encode('empty-title'));
        }
    }

    /**
     * Edit specific events.
     */
    public function editEvents()
    {
        $this->autoLayout = false;
        $this->autoRender = false;
        $this->response->type('application/javascript');

        $eventInfo = array(
            'title' => $this->request->data['title'],
            'start' => date('Y-m-d H:i:s', strtotime($this->request->data['start'])),
            'end' => date('Y-m-d H:i:s', strtotime($this->request->data['end'])),
        );
        $this->Event->id = (int)$this->request->data['id'];
        if ($this->Event->save($eventInfo)) {
            $this->response->body(json_encode('done'));
        } else {
            $this->response->body(json_encode('problem'));
        }
    }

    /**
     * @param $instructorID
     */
    public function getEventByInstructor($instructorID)
    {
        $this->autoLayout = false;
        $this->autoRender = false;
        $this->response->type('application/javascript');
        $result = $this->Event->getEvents('all', array('conditions' => array('Event.created_by' => $instructorID)));
        $this->response->body(json_encode($result));
    }

    public function getAllEventsOfInstructor($instructorID)
    {
        $start = date('Y-m-d H:i:s', strtotime($this->request->query('start')));
        $end = date('Y-m-d H:i:s', strtotime($this->request->query('end')));

        $this->autoLayout = false;
        $this->autoRender = false;
        $this->response->type('application/javascript');
        $events = $this->Event->getEventsForCalendar(array(
            'start' => $start,
            'end' => $end,
            'conditions' => array('Event.created_by' => $instructorID)
        ));
        $groupLessons = $this->GroupLesson->getGroupLesson(array(
            'start' => $start,
            'end' => $end,
            'conditions' => array('GroupLesson.user_id' => $instructorID)
        ));
        $blockTimes = $this->BlockTime->getBlockTime(array(
            'start' => $start,
            'end' => $end,
            'conditions' => array('BlockTime.user_id' => $instructorID)
        ));
        $appointments = $this->Appointment->getAppointmentOfInstructor($instructorID, $start, $end);

        $result = array_merge($events, $groupLessons, $appointments, $blockTimes);
        $this->response->body(json_encode($result));
    }

    public function getAllEventsOfStudent()
    {
        $this->autoLayout = false;
        $this->autoRender = false;
        $this->response->type('application/javascript');

        $startingTime = date('Y-m-d H:i:s', strtotime($this->request->data['start']));
        $endingTime = date('Y-m-d H:i:s', strtotime($this->request->data['end']));


        $instructorID = $this->Session->read('Auth.User.Profile.instructor_id');
        $start = $this->Event->find(
            'list',
            array(
                'conditions' => array('Event.created_by' => $instructorID),
                'fields' => array('Event.start'),
                'order' => array('Event.start' => 'ASC')
            )
        );
        $end = $this->Event->find(
            'list',
            array(
                'conditions' => array('Event.created_by' => $instructorID),
                'fields' => array('Event.end'),
                'order' => array('Event.end' => 'ASC')
            )
        );

        $groupLessons = $this->GroupLesson->getGroupLesson(array(
            'start' => $startingTime,
            'end' => $endingTime,
            'conditions' => array('GroupLesson.user_id' => $instructorID)
        ));

        $blockTimes = $this->BlockTime->getBlockTime(array('conditions' => array('BlockTime.user_id' => $instructorID)));

        $times = $this->Utilities->arrangeTimeSlot($start, $end);
        $events = $this->Appointment->myAppointments($this->userID);
        $otherStudentEvents = $this->Appointment->othersAppointments($this->userID, $instructorID);
        $result = array_merge($times, $events, $otherStudentEvents, $blockTimes, $groupLessons);
        $this->response->body(json_encode($result));
    }

    /**
     * @param $eventID
     *
     * Getting event by ID
     */
    public function getEventByID($eventID)
    {
        $this->autoLayout = false;
        $this->autoRender = false;
        $this->response->type('application/javascript');
        $result = $this->Event->getEvents('first', array('eventID' => $eventID));
        $this->response->body(json_encode($result));
    }

    /**
     *
     */
    public function getStudentForReassignAppointments()
    {
        $this->autoLayout = 'ajax';
        $this->autoRender = false;
        $this->response->type('application/javascript');

        $studentIds = array();
        if ($this->request->data['studentID']) {
            $studentIds = array($this->request->data['studentID']);
        }
        $result = $this->Profile->getStudentWithoutThisIds($this->userID, $studentIds);
        $this->response->body(json_encode($result));

    }

    /**
     *
     */
    public function reassign()
    {

        $studentID = (int)$this->request->data['student_id'];
        $packageID = (int)$this->request->data['package_id'];
        $appointmentID = (int)$this->request->data['appointmentID'];
        $lesson = (int)$this->request->data['lesson_will_deduct_for_reassign'];

        $this->Appointment->id = $appointmentID;
        if ($this->Appointment->saveField('appointment_by', $studentID)) {
            $this->Appointment->saveField('package_id', $packageID);
            $this->Appointment->saveField('is_reassigned', 1);
            $currentLessonNo = $this->PackagesUser->getCurrentLessonOfPackage($packageID, $studentID) + 1;
            $this->Appointment->saveField('lesson_no', $currentLessonNo);

            $isPayAtPackage = $this->Package->isPayAtPackage($packageID);
            if ($isPayAtPackage) {
                $this->Appointment->saveField('pay_at_facility', 1);
            } else {
                $this->Appointment->saveField('pay_at_facility', null);
                $this->Appointment->saveField('lesson_sum', $lesson);
                $result = $this->CountLesson->updateUsersLesson($studentID, false, $lesson);
                $this->PackagesUser->updateUsersPackageLesson($packageID, $studentID, $lesson, true);
            }

            if ($this->Appointment->saveField('status', 2)) {
                $this->Session->setFlash('Appointment has been reassigned', 'flash_success');
            } else {
                $this->Session->setFlash('Sorry, something went wrong', 'flash_success');
            }
        } else {
            $this->Session->setFlash('Sorry, something went wrong', 'flash_success');
        }

        $this->redirect($this->referer());
    }

    /**
     * Getting students
     */
    public function getStudents($instructorID = null)
    {
        if (empty($instructorID)) {
            $instructorID = $this->userID;
        }

        $this->autoLayout = false;
        $this->autoRender = false;
        $this->response->type('application/javascript');
        $students = $this->Profile->getStudentForInstructor('all', $instructorID);
        $this->response->body(json_encode($students));
    }

    /**
     * @param $instructorID
     */
    public function getEventOfStudentsByInstructorID($instructorID)
    {
        $this->autoLayout = false;
        $this->autoRender = false;
        $this->response->type('application/javascript');
        $result = $this->Appointment->getAppointmentOfInstructor($instructorID);
        $this->response->body(json_encode($result));
    }

    /**
     * @param $instructorID
     */
    public function getGroupLesson($instructorID)
    {
        $this->autoLayout = false;
        $this->autoRender = false;
        $this->response->type('application/javascript');
        $result = $this->GroupLesson->find('all', array('conditions' => array('GroupLesson.user_id' => $instructorID)));
        $this->response->body(json_encode($result));
    }

    /**
     * Arranging Time slot for the student calendar.This time slot is generating based on instructor given availability.
     */
    public function arrangeTimeSlot()
    {
        $this->autoLayout = false;
        $this->autoRender = false;
        $this->response->type('application/javascript');

        $instructorID = $this->Session->read('Auth.User.Profile.instructor_id');

        $start = $this->Event->find(
            'list',
            array(
                'conditions' => array('Event.created_by' => $instructorID),
                'fields' => array('Event.start'),
                'order' => array('Event.start' => 'ASC')
            )
        );
        $end = $this->Event->find(
            'list',
            array(
                'conditions' => array('Event.created_by' => $instructorID),
                'fields' => array('Event.end'),
                'order' => array('Event.end' => 'ASC')
            )
        );

        $events = $this->Utilities->arrangeTimeSlot($start, $end);
        $this->response->body(json_encode($events));
    }

    /**
     * Getting student's appointment times. These appointment are created my themself.
     */
    public function getMyAppointments()
    {
        $this->autoLayout = false;
        $this->autoRender = false;
        $this->response->type('application/javascript');
        $events = $this->Appointment->myAppointments($this->userID);
        $this->response->body(json_encode($events));
    }

    /**
     * Getting the appointments list which is created by other student of any specific instructor. Except the loggged in student.
     */
    public function getOthersAppointments()
    {
        $this->autoLayout = false;
        $this->autoRender = false;
        $this->response->type('application/javascript');
        $instructorID = $this->Session->read('Auth.User.Profile.instructor_id');
        $events = $this->Appointment->othersAppointments($this->userID, $instructorID);
        $this->response->body(json_encode($events));
    }

    /**
     * @param $userID
     *
     * This is the function for getting userInformation based on userID
     */
    public function getUserByID($userID)
    {
        $this->autoLayout = false;
        $this->autoRender = false;
        $this->response->type('application/javascript');

        $userInfo = $this->User->getUser($userID);

        $this->response->body(json_encode($userInfo));
    }

    /**
     * @param $eventID
     *
     * Here instructor can remove his schedule time.
     */
    public function removeEvent($eventID)
    {
        $this->autoLayout = false;
        $this->autoRender = false;
        $this->response->type('application/javascript');

        if ($this->Event->delete($eventID)) {
            $this->response->body(json_encode('done'));
        } else {
            $this->response->body(json_encode('problem'));
        }
    }

    /**
     * Here a student can set their appointment with their instructor.
     */
    public function setAppointment()
    {
        $this->autoLayout = false;
        $this->autoRender = false;
        $this->response->type('application/javascript');

        $today = strtotime(date('Y-m-d H:i:s'));
        $startData = strtotime($this->request->data['start']);
        $startingTime = date('Y-m-d H:i:s', strtotime($this->request->data['start']));
        $endingTime = date('Y-m-d H:i:s', strtotime($this->request->data['end']));


        $packageID = null;
        $appointmentID = null;

        if (isset($this->request->data['packageID'])) {
            $packageID = $this->request->data['packageID'];
        }

        if (isset($this->request->data['appointmentID'])) {
            $appointmentID = (int)$this->request->data['appointmentID'];
        }

        if ($packageID == null) {
            $appointment = $this->Appointment->getAppointmentByID($appointmentID);
            $eventInfo = array(
                'uuid' => $appointment['uuid'],
                'appointment_by' => $appointment['appointment_by'],
                'instructor_id' => $appointment['instructor_id'],
                'start' => $startingTime,
                'end' => $endingTime,
                'lesson_sum' => 1,
                'package_id' => $appointment['package_id'],
                'operated_by' => $this->userID,
                'is_reschedule' => 1,
                'status' => 2,
            );
        } else {

            $isPayAtPackage = $this->Package->isPayAtPackage($packageID);
            if ($isPayAtPackage) {

                $eventInfo = array(
                    'uuid' => String::uuid(),
                    'appointment_by' => $this->userID,
                    'instructor_id' => $this->Session->read('Auth.User.Profile.instructor_id'),
                    'start' => $startingTime,
                    'end' => $endingTime,
                    'package_id' => $packageID,
                    'status' => 2,
                    'pay_at_facility' => 1,
                );

            } else {

                $eventInfo = array(
                    'uuid' => String::uuid(),
                    'appointment_by' => $this->userID,
                    'instructor_id' => $this->Session->read('Auth.User.Profile.instructor_id'),
                    'start' => $startingTime,
                    'end' => $endingTime,
                    'lesson_sum' => 1,
                    'package_id' => $packageID,
                    'status' => 2,
                    'pay_at_facility' => 0,
                );
            }
        }

        if ($today < $startData) {
            $this->Appointment->create();
            if ($this->Appointment->save($eventInfo)) {

                $appointmentID = $this->Appointment->getLastInsertId();

                //var_dump($appointmentID); die();

                $this->Appointment->setWaitingFromRangeTime($startingTime, $endingTime, $this->userID, $appointmentID);

                $this->Appointment->getUpdateStudentsNextLesson($this->userID);

                $this->User->changeSlippingAway($this->userID, 0);
                $this->Appointment->getUpdateStudentsNextLesson($this->userID);

                if ($appointmentID != null) {
                    $rescheduleAppointmentID = $this->Appointment->getLastInsertId();
                    $this->sendEmailNotification(
                        'when_lesson_rescheduled',
                        array('appointmentID' => $rescheduleAppointmentID)
                    );

                    $this->Appointment->id = $appointmentID;
                    $this->Appointment->saveField('is_already_rescheduled', 1);
                    $this->Session->setFlash('Appointment has been rescheduled successfully', 'flash_success');
                } else {
                    if (empty($isPayAtPackage) && isset($isPayAtPackage)) {
                        $this->PackagesUser->updateUsersPackageLesson($packageID, $this->userID, 1, true);
                        $this->CountLesson->updateUsersLesson($this->userID, false, 1);
                    }
                    $this->Session->setFlash('Appointment has been complete', 'flash_success');
                }

            } else {
                $this->Session->setFlash('Sorry, something went wrong', 'flash_success');
            }
        } else {
            $this->Session->setFlash('Invalid time', 'flash_success');
        }

        $this->redirect(array('controller' => 'calendars', 'action' => 'index', 'student' => true));
    }

    /**
     * Here a student can set their appointment with their instructor.
     */
    public function setWaitingList()
    {
        $this->autoLayout = false;
        $this->autoRender = false;
        $this->response->type('application/javascript');

        $previousAppointmentID = $this->request->data['previousAppointmentID'];
        $givenPackageID = $this->request->data['packageID'];
        $previousAppointmentInfo = $this->Appointment->getAppointmentByID($previousAppointmentID);


        if (isset($this->request->data['appointmentID']) && $this->request->data['appointmentID']) {
            $appointment = $this->Appointment->getAppointmentByID($this->request->data['appointmentID']);
            $lessonNo = $appointment['lesson_no'];
            $packageID = $appointment['package_id'];
        }

        if ($givenPackageID) {
            $packageUser = $this->PackagesUser->getRunningLesson($givenPackageID, $this->userID);
            $lessonNo = $packageUser['count_appointment'] + 1;
            $packageID = $givenPackageID;
        }


        $waiting = array(
            'uuid' => String::uuid(),
            'appointment_id' => $previousAppointmentInfo['id'],
            'instructor_id' => $previousAppointmentInfo['instructor_id'],
            'appointment_by' => $this->userID,
            'package_id' => $packageID,
            'lesson_no' => $lessonNo,
            'start' => $previousAppointmentInfo['start'],
            'end' => $previousAppointmentInfo['end'],
        );

        $this->Waiting->create();
        if ($this->Waiting->save($waiting)) {
            $this->Session->setFlash('You successfully joined to waiting list', 'flash_success');
        } else {
            $this->Session->setFlash('Sorry, something went wrong', 'flash_error');
        }

        $this->redirect($this->referer());
    }


    /**
     * Here a student can set their appointment Range Waiting List.
     */
    public function setRangeWaitingList()
    {
        $data = $this->request->data;
        $start = $data['fromDate'] . '' . $data['waiting_starting_time'];
        $end = $data['toDate'] . ' ' . $data['waiting_ending_time'];
        $startTime = date("Y-m-d H:i:s", strtotime($start));
        $endTime = date("Y-m-d H:i:s", strtotime($end));



        if (isset($data['studentID'])) {

            $student = explode(' - ', $data['studentID']);

            $studentID = $this->User->getUserByEMail($student[1]);

            $userID = $studentID;
            $instructorID = $this->userID;

        } else {

            $userID = $this->userID;
            $instructorID = $instructorID = $this->Session->read('Auth.User.Profile.instructor_id');

        }
        $waiting = $this->Waiting->find('list',

            array(
                'fields' => array(
                    'Waiting.appointment_id',
                ),
                'conditions' => array(
                    'Waiting.appointment_by =' => $userID
                )
            )
        );
        if (sizeof($waiting) > 0) {
            $otherStudentEventsQ = $this->Appointment->otherAppointmentIDs($userID, $instructorID, $startTime,
                $endTime);
            $otherStudentEvents = array_diff($otherStudentEventsQ, $waiting);
        } else {
            $otherStudentEvents = $this->Appointment->otherAppointmentIDs($userID, $instructorID, $startTime, $endTime);
        }

        if (sizeof($otherStudentEvents) > 0) {
            if (isset($data['studentID'])) {

                $rangeWaiting = array(
                    'user_id' => $userID,
                    'start_time' => $startTime,
                    'end_time' => $endTime,
                    'instructor_id' => $instructorID
                );

            } else {

                $rangeWaiting = array(
                    'user_id' => $userID,
                    'start_time' => $startTime,
                    'end_time' => $endTime,
                );

            }

            $this->RangeWaiting->create();
            $this->RangeWaiting->save($rangeWaiting);
            $rangeWaitingID = $this->RangeWaiting->getLastInsertId();
            foreach ($otherStudentEvents as $key => $otherStudentEvent) {
                $appointmentID = $otherStudentEvent;

                $this->autoLayout = false;
                $this->autoRender = false;
                $this->response->type('application/javascript');
                if (isset($appointmentID) && $appointmentID) {
                    $appointment = $this->Appointment->getAppointmentByID($appointmentID);
                    $lessonNo = $appointment['lesson_no'];
                    $packageID = $appointment['package_id'];
                }

                $waiting = array(
                    'uuid' => String::uuid(),
                    'appointment_id' => $appointmentID,
                    'instructor_id' => $instructorID,
                    'appointment_by' => $userID,
                    'package_id' => $packageID,
                    'lesson_no' => $lessonNo,
                    'start' => $appointment['start'],
                    'end' => $appointment['end'],
                    'range_waiting_id' => $rangeWaitingID
                );

                $this->Waiting->create();
                $this->Waiting->save($waiting);
            }
            $this->Session->setFlash('You successfully joined to waiting list', 'flash_success');
        } else {
            $this->Session->setFlash('Sorry , There are no appointments within that time', 'flash_error');

        }
        $this->redirect($this->referer());
    }

    /**
     * Instructor can set his appointment time with his student.
     */
    public function setAppointmentByInstructor()
    {
        $today = date('Y-m-d H:i:s');
        $studentID = explode(' - ', $this->request->data['studentID']);
        $studentID = (int)$this->User->getUserByEMail($studentID[1]);
        $appointmentTitle = $this->request->data['appointment_title'];

        $instructorID = $this->userID;
        if(isset($this->request->data['instructor_id']) && $this->request->data['instructor_id'])
        {
            $instructorID = (int) $this->request->data['instructor_id'];
        }

        $beforeAppointmentID = null;
        $beforePackageID = null;
        $beforeEditLessonWillDeduct = null;
        $beforeEditAppointmentBy = null;
        $beforeEditPayItFacility = null;

        if (isset($this->request->data['beforeAppointmentID'])) {
            $beforeAppointmentID = (int)$this->request->data['beforeAppointmentID'];
        }

        if (isset($this->request->data['beforePackageID'])) {
            $beforePackageID = (int)$this->request->data['beforePackageID'];
        }

        if (isset($this->request->data['beforeEditLessonWillDeduct'])) {
            $beforeEditLessonWillDeduct = (int)$this->request->data['beforeEditLessonWillDeduct'];
        }


        if (isset($this->request->data['beforeEditPayItFacility']) && $this->request->data['beforeEditPayItFacility'] == 'true') {
            $beforeEditPayItFacility = true;
        }

        if (isset($this->request->data['beforeEditAppointmentBy'])) {
            $beforeEditAppointmentBy = (int)$this->request->data['beforeEditAppointmentBy'];
        }

        $note = $this->request->data['appointmentNote'];

        $appointmentDate = $this->request->data['setAppointmentDate'];
        if(isset($this->request->data['setAppointmentDate']))
        {
            $appointmentDate = $this->request->data['custom_date'];
        }

        $startingTime = date('Y-m-d H:i:s', strtotime($appointmentDate . '' . $this->request->data['start']));
        $endingTime = date('Y-m-d H:i:s', strtotime($appointmentDate . '' . $this->request->data['end']));
        $packageID = $this->request->data['packageID'];
        $appointmentID = null;

        $lessonWillDeduct = 1;
        if (isset($this->request->data['lessonWillDeduct'])) {
            $lessonWillDeduct = (int)$this->request->data['lessonWillDeduct'];
        }

        if (isset($this->request->data['appointmentID'])) {
            $appointmentID = $this->request->data['appointmentID'];
        }
        $currentLessonNo = $this->PackagesUser->getCurrentLessonOfPackage($packageID, $studentID) + $lessonWillDeduct;

        if(strtotime($startingTime) > strtotime($endingTime))
        {
            $swap = $endingTime;
            $endingTime = $startingTime;
            $startingTime = $swap;
        }


        if ($packageID == null) {
            $appointment = $this->Appointment->getAppointmentByID($appointmentID);
            $eventInfo = array(
                'uuid' => $appointment['uuid'],
                'appointment_by' => $appointment['appointment_by'],
                'instructor_id' => $appointment['instructor_id'],
                'title' => $appointmentTitle,
                'start' => $startingTime,
                'end' => $endingTime,
                'note' => $note,
                'lesson_no' => $appointment['lesson_no'],
                'lesson_sum' => $lessonWillDeduct,
                'package_id' => $appointment['package_id'],
                'operated_by' => $instructorID,
                'is_reschedule' => 1,
                'status' => 2,
                'pay_at_facility' => 0,
            );
        } else {
            $isPayAtPackage = $this->Package->isPayAtPackage($packageID);
            if ($isPayAtPackage) {
                $eventInfo = array(
                    'uuid' => String::uuid(),
                    'appointment_by' => $studentID,
                    'instructor_id' => $instructorID,
                    'title' => $appointmentTitle,
                    'start' => $startingTime,
                    'end' => $endingTime,
                    'note' => $note,
                    'lesson_no' => null,
                    'package_id' => $packageID,
                    'lesson_sum' => $lessonWillDeduct,
                    'status' => 2,
                    'pay_at_facility' => 1,
                );
            } else {
                $eventInfo = array(
                    'uuid' => String::uuid(),
                    'appointment_by' => $studentID,
                    'instructor_id' => $instructorID,
                    'title' => $appointmentTitle,
                    'start' => $startingTime,
                    'end' => $endingTime,
                    'note' => $note,
                    'lesson_no' => $currentLessonNo,
                    'lesson_sum' => $lessonWillDeduct,
                    'package_id' => $packageID,
                    'status' => 2,
                    'pay_at_facility' => 0,
                );
            }
        }
        $checkTime = true;/*$this->Appointment->checkAvailabltimeForAppointment($startingTime, $this->userID);*/




        if ($startingTime && $endingTime) {
            if (((int)($checkTime[0][0]['total'] == 0))) {
                if ($beforeAppointmentID) {
                    $this->Appointment->id = $beforeAppointmentID;
                    $eventInfo['status'] = 3;
                } else {
                    $this->Appointment->create();
                }

                if ($studentID && $this->Appointment->save($eventInfo)) {
                    $this->User->changeSlippingAway($studentID, 0);
                    $this->Appointment->getUpdateStudentsNextLesson($studentID);

                    if ($beforeEditPayItFacility == null) {
                        $this->PackagesUser->updateUsersPackageLesson($beforePackageID, $beforeEditAppointmentBy,
                            $beforeEditLessonWillDeduct);
                        $this->CountLesson->updateUsersLesson($beforeEditAppointmentBy, true,
                            $beforeEditLessonWillDeduct);
                    }

                    if ($appointmentID != null && $appointmentID != 'Choose Lesson') {
                        $rescheduleAppointmentID = $this->Appointment->getLastInsertId();
                        $this->sendEmailNotification('when_lesson_rescheduled',
                            array('appointmentID' => $rescheduleAppointmentID));
                        $this->Appointment->id = $appointmentID;
                        $this->Appointment->saveField('is_already_rescheduled', 1);
                        $this->Session->setFlash('Appointment has been reschedule successfully', 'flash_success');
                    } else {
                        if (empty($isPayAtPackage) && isset($isPayAtPackage)) {
                            $this->PackagesUser->updateUsersPackageLesson($packageID, $studentID, $lessonWillDeduct,
                                true);
                            $this->CountLesson->updateUsersLesson($studentID, false, $lessonWillDeduct);
                            $this->Session->setFlash('Appointment has been created successfully', 'flash_success');
                        }
                        $this->Session->setFlash('Appointment has been created successfully', 'flash_success');
                    }

                    if ($beforeAppointmentID) {
                        $this->Session->setFlash('Appointment has been modified successfully', 'flash_success');
                    }
                } else {
                    $this->Session->setFlash('Sorry, something went wrong', 'flash_error');
                }
            } else {
                $this->Session->setFlash('Sorry, Appointment exist in this time', 'flash_error');
            }
        } else {
            $this->Session->setFlash('Sorry, invalid time', 'flash_error');
        }

        $this->redirect($this->referer());
    }

    /**
     * @param $appointmentID
     * @param $status
     */
    public
    function appointmentActions(
        $appointmentID,
        $status
    ) {
        $appointmentDetails = $this->Appointment->getAppointmentByID($appointmentID);
        $today = new DateTime("now");
        $appointedTime = new DateTime($appointmentDetails['created']);
        $interval = date_diff($today, $appointedTime);
        $HoursDifferent = $interval->h;

        if ($HoursDifferent < 24) {

            $this->Appointment->id = $appointmentID;
            if ($this->Appointment->saveField('status', $status)) {
                $this->Appointment->saveField('operated_by', $this->userID);

                if ($appointmentDetails['pay_at_facility'] == null) {
                    $this->PackagesUser->updateUsersPackageLesson($appointmentDetails['package_id'],
                        $appointmentDetails['appointment_by'], (int)$appointmentDetails['lesson_sum'], false);
                    $this->CountLesson->updateUsersLesson($appointmentDetails['appointment_by'], true,
                        (int)$appointmentDetails['lesson_sum']);
                }

                /*if ($status == 2) {
                    $this->sendEmailNotification('when_lesson_confirm', array('appointmentID' => $appointmentID));
                }
                elseif ($status == 3) {
                    App::uses('AppointmentsController', 'Controller');
                    $appointmentsController = new AppointmentsController;
                    $appointmentsController->notifiedToWaitingList($appointmentID);
                    $this->sendEmailNotification('when_lesson_cancelled', array('appointmentID' => $appointmentID));
                }*/

                if ($status == 3) {
                    $this->sendEmailNotification('when_lesson_cancelled', array('appointmentID' => $appointmentID));
                    $this->Session->setFlash('Appointment has been cancelled', 'flash_error');
                } else {
                    $this->Session->setFlash('Appointment has been modified', 'flash_error');
                }
            } else {
                $this->Session->setFlash('Sorry, something wend wrong', 'flash_error');
            }
        } else {
            $this->Session->setFlash('Appointment has been expired', 'flash_error');
        }

        $this->redirect(array('controller' => 'calendars', 'action' => 'index', 'instructor' => true));
    }


    /**
     * Manage available time slot.
     */
    public function available()
    {
        if ($this->request->is('post')) {

            $instructorID = $this->userID;
            if (isset($this->request->data['tc_instructor_id'])) {
                $instructorID = $this->request->data['tc_instructor_id'];
            }

            $currentDate = date('Y-m-d H:i:s');
            $from = date('Y-m-d', strtotime('first day of this month'));
            $to = date('Y-m-d', strtotime('last day of this month'));
            $conditions = array(
                'Event.created_by' => $instructorID,
                'Event.start >' => $from,
                'Event.start <' => $to,
                'Event.start > ' => $currentDate
            );
            $events = $this->Event->find('list',
                array('conditions' => $conditions, 'recursive' => -1, 'fields' => array('Event.id')));

            if ($events) {
                $this->Event->deleteAll($events);
            }

            $scheduleData = $this->request->data;
            $mondayThisWeek = date("Y-m-d", strtotime("monday this week"));
            $tuesdayThisWeek = date("Y-m-d", strtotime("tuesday this week"));
            $wednesdayThisWeek = date("Y-m-d", strtotime("wednesday this week"));
            $thursdayThisWeek = date("Y-m-d", strtotime("thursday this week"));
            $fridayThisWeek = date("Y-m-d", strtotime("friday this week"));
            $saturdayThisWeek = date("Y-m-d", strtotime("saturday this week"));
            $sundayThisWeek = date("Y-m-d", strtotime("sunday this week"));

            $mondayResult = $this->arrangeSingleDaySchedule($mondayThisWeek, $scheduleData['monday'],
                $instructorID);
            $tuesdayResult = $this->arrangeSingleDaySchedule($tuesdayThisWeek, $scheduleData['tuesday'],
                $instructorID);
            $wednesdayResult = $this->arrangeSingleDaySchedule($wednesdayThisWeek, $scheduleData['wednesday'],
                $instructorID);
            $thursdayResult = $this->arrangeSingleDaySchedule($thursdayThisWeek, $scheduleData['thursday'],
                $instructorID);
            $fridayResult = $this->arrangeSingleDaySchedule($fridayThisWeek, $scheduleData['friday'],
                $instructorID);
            $saturdayResult = $this->arrangeSingleDaySchedule($saturdayThisWeek, $scheduleData['saturday'],
                $instructorID);
            $sundayResult = $this->arrangeSingleDaySchedule($sundayThisWeek, $scheduleData['sunday'],
                $instructorID);
            $result = array_merge($mondayResult, $tuesdayResult, $wednesdayResult, $thursdayResult, $fridayResult,
                $saturdayResult, $sundayResult);

            if ($this->Event->saveAll($result)) {
                $this->Session->setFlash('Schedule has been created', 'flash_success');
                $this->Session->write('instructorID', $instructorID);
            } else {
                $this->Session->setFlash('Sorry, something went wrong', 'flash_error');
            }
            $this->redirect($this->referer());
        }
    }

    /**
     * @param $date
     * @param $schedule
     * @param $instructorID
     * @return array|bool
     *
     * Arrange single day'schedule.
     */
    public function arrangeSingleDaySchedule(
        $date,
        $schedule,
        $instructorID
    ) {
        $isRepeat = false;
        if (isset($schedule['select'])) {
            $isRepeat = true;
        }
        $arranging = array(
            'scheduleDate' => $date,
            'Schedule' => array(
                'start' => $schedule['start'],
                'end' => $schedule['end']
            ),
            'isRepeat' => $isRepeat
        );

        $result = $this->Utilities->arrangeScheduleTimes($arranging, $instructorID);
        if (is_array($result)) {
            return $result;
        }

        return false;
    }

    /**
     *
     */
    public function save_group_lesson()
    {
        $instructorID = $this->userID;
        if(isset($this->request->data['instructor_id']) && $this->request->data['instructor_id'])
        {
            $instructorID = (int) $this->request->data['instructor_id'];
        }

        $startingTime = date(
            'Y-m-d H:i:s',
            strtotime($this->request->data['GroupLesson']['date'] . " " . $this->request->data['GroupLesson']['starting_time'])
        );

        $endingTime = date(
            'Y-m-d H:i:s',
            strtotime($this->request->data['GroupLesson']['date'] . " " . $this->request->data['GroupLesson']['ending_time'])
        );

        $data = array(
            'user_id' => $instructorID,
            'start' => $startingTime,
            'end' => $endingTime,
            'student_limit' => $this->request->data['GroupLesson']['student_limit'],
            'title' => $this->request->data['GroupLesson']['title'],
            'description' => $this->request->data['GroupLesson']['description'],
            'price' => $this->request->data['GroupLesson']['price'],
        );

        if (isset($this->request->data['GroupLesson']['id']) && $this->request->data['GroupLesson']['id']) {
            $this->GroupLesson->id = (int)$this->request->data['GroupLesson']['id'];
        }

        if ($this->GroupLesson->save($data)) {

            if (isset($this->request->data['GroupLesson']['id']) && $this->request->data['GroupLesson']['id']) {
                $this->Session->setFlash('Group lesson has been updated', 'flash_success');
            } else {
                $this->Session->setFlash('Group lesson has been created', 'flash_success');
            }

        } else {
            $this->Session->setFlash('Sorry, something went wrong', 'flash_error');
        }

        $this->redirect($this->referer());
    }

    /**
     *
     */
    public function block_time()
    {
        $instructorID = $this->userID;
        if(isset($this->request->data['instructor_id']) && $this->request->data['instructor_id'])
        {
            $instructorID = (int) $this->request->data['instructor_id'];
        }

        $startingTime = date(
            'Y-m-d H:i:s',
            strtotime(
                $this->request->data['BlockTime']['date'] . " " . $this->request->data['BlockTime']['starting_time']
            )
        );

        $endingTime = date(
            'Y-m-d H:i:s',
            strtotime(
                $this->request->data['BlockTime']['date'] . " " . $this->request->data['BlockTime']['ending_time']
            )
        );

        $data = array(
            'user_id' => $instructorID,
            'start' => $startingTime,
            'end' => $endingTime,
            'title' => $this->request->data['BlockTime']['title'],
            'message' => $this->request->data['BlockTime']['message'],
        );

        if ($this->BlockTime->save($data)) {
            $this->Session->setFlash('Your block time has been saved successfully', 'flash_success');
        } else {
            $this->Session->setFlash('Sorry, something went wrong', 'flash_error');
        }

        $this->redirect($this->referer());
    }

    /**
     *
     */
    public function student_join_group_lesson($groupID, $userID)
    {
        $data = array(
            'group_lesson_id' => $groupID,
            'user_id' => $userID,
        );

        $this->GroupLessonsStudents->create();
        if ($this->GroupLessonsStudents->save($data)) {
            $this->GroupLesson->updateGroupLessonStudent($groupID);
            $this->Session->setFlash('Thanks for joining in group lesson', 'flash_success');
        } else {
            $this->Session->setFlash('Sorry, something went wrong', 'flash_error');
        }

        $this->redirect($this->referer());
    }

    /**
     *
     */
    public function student_leave_group_lesson($groupID, $userID)
    {
        $ID = $this->GroupLessonsStudents->getGroupLessonStudentID($groupID, $userID);
        $this->GroupLessonsStudents->id = $ID;

        if ($ID) {
            if ($this->GroupLessonsStudents->delete()) {
                $this->GroupLesson->updateGroupLessonStudent($groupID, false);
                $this->Session->setFlash('Successfully leaved from group lesson', 'flash_error');
                $this->redirect($this->referer());
            }
        }

        $this->Session->setFlash('Sorry, something went wrong', 'flash_error');
        $this->redirect($this->referer());
    }


    public function checkGroupLessonBtn($groupID, $studentID)
    {
        if ($this->request->is('ajax')) {
            $this->autoLayout = false;
            $this->autoRender = false;
            $this->response->type('application/javascript');

            $response = $this->GroupLessonsStudents->checkStudentIDInGroupLesson($groupID, $studentID);
            $gl = $this->GroupLesson->find('first', array('conditions' => array('GroupLesson.id' => $groupID)));

            if ($response == false) {
                $btn = 'join';
            } else {
                $btn = 'leave';
            }

            if ($this->Utilities->isGLInvalid($gl)) {
                $btn = 'none';
            }

            if ($this->Utilities->isGLExpired($gl)) {
                $btn = 'none';
            }

            if ($this->Utilities->isGLFilledUp($gl) && $btn != 'leave') {
                $btn = 'none';
            }

            $this->response->body(json_encode($btn));
        }
    }


    public function checkWaitingBtn($appointmentID, $studentID)
    {
        if ($this->request->is('ajax')) {
            $this->autoLayout = false;
            $this->autoRender = false;
            $this->response->type('application/javascript');

            $response = $this->Waiting->find('first', array(
                'conditions' => array(
                    'Waiting.appointment_id' => $appointmentID,
                    'Waiting.appointment_by' => $studentID
                )
            ));

            if ($response == false) {
                $btn = 'join';
            } else {
                $btn = 'leave';
            }

            $this->response->body(json_encode($btn));
        }
    }

    public function student_leave_waiting_list($appointmentID, $studentID) {
        $response = $this->Waiting->find('first', array(
            'conditions' => array(
                'Waiting.appointment_id' => $appointmentID,
                'Waiting.appointment_by' => $studentID
            )
        ));

        $ID = $response['Waiting']['id'];
        $this->Waiting->id = $ID;

        if ($ID) {
            if ($this->Waiting->delete()) {
                $this->Session->setFlash('Successfully leaved from waiting list', 'flash_error');
                $this->redirect($this->referer());
            }
        }

        $this->Session->setFlash('Sorry, something went wrong', 'flash_error');
        $this->redirect($this->referer());
    }


    public function delete_group_lesson($gpID)
    {
        if($this->GroupLesson->delete($gpID, true)){
            $this->Session->setFlash('Group lesson has been removed successfully', 'flash_error');
        }
        else{
            $this->Session->setFlash('Sorry, something went wrong', 'flash_error');
        }
        $this->redirect($this->referer());
    }


    public function delete_block_time($btID)
    {
        if($this->BlockTime->delete($btID, true)){
            $this->Session->setFlash('Block has been removed successfully', 'flash_error');
        }
        else{
            $this->Session->setFlash('Sorry, something went wrong', 'flash_error');
        }
        $this->redirect($this->referer());
    }

}

