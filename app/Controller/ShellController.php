<?php
App::uses('AppController', 'Controller');

/**
 * Class AppointmentsController
 *
 * @property Appointment $Appointment..
 * @property Attachment @Attachment
 * @property Benchmark $Benchmark
 * @property Billing $Billing
 * @property BillingsPackage @BillingsPackage
 * @property BlockTime @BlockTime
 * @property Card $Card
 * @property Config $Config
 * @property Contact $Contact
 * @property CountLesson $CountLesson
 * @property EmailsEvent $EmailsEvent
 * @property EmailsEventsSetting $EmailsEventsSetting
 * @property Event $Event
 * @property GroupLesson $GroupLesson
 * @property GroupLessonsStudents $GroupLessonsStudents
 * @property Note @Note
 * @property Package $Package
 * @property PackagesUser $PackagesUser
 * @property PackagesOrder $PackagesOrder
 * @property Profile $Profile
 * @property Source $Source
 * @property TcsUser $TcsUser
 * @property User $User
 * @property Waiting $Waiting
 *
 * @property PaginatorComponent $Paginator
 * @property EmailComponent $Email
 * @property UtilitiesComponent $Utilities
 * @property AuthorizeNetComponent $AuthorizeNet
 * @property DataTableComponent $DataTable
 * @property ImgComponent $Img
 * @property AppsComponent $Apps
 *
 */

use Facebook\FacebookSession;
use Facebook\FacebookRedirectLoginHelper;
use Facebook\FacebookRequest;
use Facebook\FacebookResponse;
use Facebook\FacebookSDKException;
use Facebook\FacebookRequestException;
use Facebook\FacebookAuthorizationException;
use Facebook\GraphObject;
use Facebook\Entities\AccessToken;
use Facebook\HttpClients\FacebookCurlHttpClient;
use Facebook\HttpClients\FacebookHttpable;

App::uses('CakeEmail', 'Network/Email');

class ShellController extends AppController
{
    /**
     * The name of this controller.
     *
     * @var string
     */
    public $name = 'Shell';

    public $components = array('Email');

    public function beforeFilter()
    {
        $this->Auth->allow();
    }

    public function paymentReminder()
    {

        $this->autoLayout = false;
        $this->autoRender = false;

        $users = $this->BillingsPeriod->getNotifyUsers();
        if ($users && sizeof($users) > 0) {
            foreach ($users as $key => $value) {
                $subject = 'Billing Statement Available (live)';
                $cakeEmail = new CakeEmail('mandrill');
                $cakeEmail->template('billing_notification')
                    ->emailFormat('html')
                    ->subject($subject)
                    ->to($value['User']['username'])
                    ->from('support@foutsventures.com', 'Golf Swing Prescription Account')
                    ->replyTo('no-reply@foutsventures.com')
                    ->viewVars(
                        array(
                            'var' => $value
                        )
                    );

                if ($cakeEmail->send()) {

                }

                $this->BillingsPeriod->makeNotified($value['BillingsPeriod']['id']);
                $this->BillingsPeriod->changePaymentStatus($value['BillingsPeriod']['id'], 8);
            }
        }

    }

    public function makePayment()
    {

        $this->autoLayout = false;
        $this->autoRender = false;
        $users = $this->BillingsPeriod->getPaymentUsers();

        if ($users && sizeof($users) > 0) {
            foreach ($users as $key => $value) {
                $payment = $this->payment($value['User']['id'], $value['BillingsPeriod']['amount'], 1);
                if ($payment) {
                    $this->BillingsPeriod->changePaymentStatus($value['BillingsPeriod']['id'], 5);
                    $this->BillingsPeriod->changeStatus($value['BillingsPeriod']['id'], 2);

                    $nextPayment = date('Y-m-d H:i:s',
                        strtotime('+1 mins', strtotime($value['BillingsPeriod']['payment_date'])));
                    $this->BillingsPeriod->addBillingPeriod($value['User']['id'], 10, $nextPayment);

                    $subject = 'Bill Received (live)';
                    $cakeEmail = new CakeEmail('mandrill');
                    $cakeEmail->template('recurring_billing_confirm')
                        ->emailFormat('html')
                        ->subject($subject)
                        ->to($value['User']['username'])
                        ->from('support@foutsventures.com', 'Golf Swing Prescription Account')
                        ->replyTo('no-reply@foutsventures.com')
                        ->viewVars(
                            array(
                                'var' => $value,
                                'card' => $payment
                            )
                        );
                    if ($cakeEmail->send()) {

                    }
                } else {
                    $this->BillingsPeriod->changePaymentStatus($value['BillingsPeriod']['id'], 1);
                    /**
                     * @TODO need to user's account locked.
                     */
                }
            }
        }
    }


    public function payment($userID, $amount, $recurringBilling = null)
    {
        $this->autoLayout = false;
        $this->autoRender = false;

        $cards = $this->Card->getCardList($userID);
        foreach ($cards as $card) {
            $authorizedNetCustomerID = $card['Card']['authorise_dot_net_customer_profile_id'];
            $authorizedNetPaymentProfileID = $card['Card']['authorise_dot_net_payment_profile_id'];
            $authorizedNetShippingID = $card['Card']['authorise_dot_net_shipping_id'];

            $transactionResponse = $this->AuthorizeNet->createTransactionCIM($amount, $authorizedNetCustomerID,
                $authorizedNetPaymentProfileID, $authorizedNetShippingID);
            if (isset($transactionResponse) && isset($transactionResponse['payment_final_status']) && $transactionResponse['payment_final_status'] == 1) {
                $transactionResponse = array_merge(
                    $transactionResponse,
                    array(
                        'user_id' => $userID,
                        'card_id' => $card['Card']['id'],
                        'uuid' => String::uuid(),
                        'subtotal' => $amount,
                        'tax' => 00,
                        'amount' => $amount,
                    )
                );

                if ($recurringBilling) {
                    $transactionResponse['recurring_billing'] = 1;
                }

                $this->Billing->create();
                $this->Billing->save($transactionResponse);

                return $card;
            }
        }

        return false;
    }

    public function sendWaitingQueueEmail()
    {
        $this->autoLayout = false;
        $this->autoRender = false;

        $runningQueues = $this->WaitingQueue->checkRunningQueue();
        //var_dump($runningQueues); die();
        if (sizeof($runningQueues) > 0) {
            foreach ($runningQueues as $runningQueue) {

                $subject = 'Waiting Queue Confirmation';
                $cakeEmail = new CakeEmail('mandrill');
                $cakeEmail->template('waiting_confirm')
                    ->emailFormat('html')
                    ->subject($subject)
                    ->to($runningQueue['User']['username'])
                    ->from('support@foutsventures.com', 'Golf Swing Prescription Account')
                    ->replyTo('no-reply@foutsventures.com')
                    ->viewVars(
                        array(
                            'var' => $runningQueue
                        )
                    );

                if ($cakeEmail->send()) {
                    $this->WaitingQueue->id = $runningQueue['WaitingQueue']['id'];
                    $this->WaitingQueue->saveField('status', 1);
                }
            }
        } else {
            return false;
        }
    }

    /**
     * This function change status to Not Reponse
     * when current is greater than vaildity pediod
     * time
     * @return bool
     */
    public function changeNotResponedWhenTimeOver()
    {
        $this->autoLayout = false;
        $this->autoRender = false;

        $checkTimeOverWaitingQueue = $this->WaitingQueue->find('all', array(
            'conditions' => array(
                'validity_period < ' => date('Y-m-d H:i:s'),
                'status' => 1
            ),
        ));

        if (sizeof($checkTimeOverWaitingQueue) > 0) {
            foreach ($checkTimeOverWaitingQueue as $waitingQueue):
                $this->WaitingQueue->id = $waitingQueue['WaitingQueue']['id'];
                $this->WaitingQueue->saveField('status', 2);
            endforeach;
        } else {
            return false;
        }
    }
}