<?php
App::uses('AppModel', 'Model');
class PackagesOrder extends AppModel
{
    /**
     * @var string
     *
     * Name of this model.
     */
    public $name = 'PackagesOrder';

    public $belongsTo = array(
        'Package' => array(
            'className' => 'Package',
            'foreignKey' => 'package_id'
        )
    );

    /**
     * @param $packageID
     * @param $userID
     * @param $order
     * @return bool
     */
    public function addPackage($packageID, $userID, $order)
    {
        $data = array(
            'user_id' => $userID,
            'package_id' => $packageID,
            'order' => $order,
        );

        if ($this->save($data)) {
            return true;
        }
        return false;
    }

    /**
     * @param $userID
     * @return bool
     */
    public function isOrderExists($userID)
    {
        $isExists = $this->find('first', array('conditions' => array('PackagesOrder.user_id' => $userID)));
        if ($isExists) {
            return true;
        }
        return false;
    }

    /**
     * @param $userID
     * @return array|int
     */
    public function isOrderExistsCount($userID)
    {
        $isExists = $this->find('count', array('conditions' => array('PackagesOrder.user_id' => $userID)));
        if ($isExists) {
            return $isExists;
        }
        return 0;
    }

    /**
     * @param $userID
     * @param $packageID
     * @return array|int
     */
    public function getOrderPackage($userID, $packageID)
    {
        $result = $this->find('first', array('conditions' => array('PackagesOrder.user_id' => $userID, 'PackagesOrder.package_id' => $packageID)));
        if ($result) {
            return $result;
        }
        return 0;
    }

}


