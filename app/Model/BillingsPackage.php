<?php
App::uses('AppModel', 'Model');
class BillingsPackage extends AppModel
{

    public $name = 'BillingsPackage';

    public function getItemsByBillingID($billingID){
        $result = $this->find(
            'all',
            array(
                'conditions' => array('BillingsPackage.billing_id' => $billingID),
                'fields' => array(
                    'BillingsPackage.*',
                    'Package.*'
                ),
                'joins' => array(
                    array(
                        'table' => 'packages',
                        'type' => 'LEFT',
                        'alias' => 'Package',
                        'conditions' => array('Package.id = BillingsPackage.package_id')
                    )
                )
            )
        );
        if($result){
            return $result;
        }
        return false;
    }
}
