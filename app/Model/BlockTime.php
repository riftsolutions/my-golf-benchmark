<?php
App::uses('AppModel', 'Model');
class BlockTime extends AppModel
{
    /**
     * @var string
     *
     * Name of this BlockTime Model
     */

    public $name = 'BlockTime';

    public $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id'
        )
    );

    public function getBlockTime($options = array())
    {
        $queryConditions = array();

        if (isset($options['conditions'])) {
            if (is_array($options['conditions'])) {
                $queryConditions = array_merge($queryConditions, $options['conditions']);
            }
        }

        if (isset($options['start']) && isset($options['end'])) {
            if (is_array($options['conditions'])) {
                $queryConditions = array_merge($queryConditions,
                    array(
                        'BlockTime.start >=' => $options['start'],
                        'BlockTime.end <=' => $options['end']
                    )
                );
            }
        }

        $blockTimes = $this->find(
            'all',
            array(
                'conditions' => $queryConditions,
                'fields' => array('id', 'start', 'end', 'title', 'message', 'user_id', 'CONCAT(Profile.first_name, " ", Profile.last_name) as instructorName'),
                'limit' => false,
                'recursive' => -1,
                'order' => 'BlockTime.id DESC',
                'joins' => array(
                    array(
                        'table' => 'profiles',
                        'type' => 'LEFT',
                        'alias' => 'Profile',
                        'conditions' => array('Profile.user_id = BlockTime.user_id')
                    ),
                ),
            )
        );

        $data = array();
        if ($blockTimes) {
            foreach ($blockTimes as $time) {
                $data[] = array(
                    'instructor_name' => $time[0]['instructorName'],
                    'id' => $time['BlockTime']['id'],
                    'start' => $time['BlockTime']['start'],
                    'end' => $time['BlockTime']['end'],
                    'title' => $time['BlockTime']['title'],
                    'message' => $time['BlockTime']['message'],
                    'type' => 'block_time',
                    'resources' => [ltrim($time['BlockTime']['user_id'])]
                );
            }
        }
        return $data;
    }
}
