<?php
App::uses('AppModel', 'Model');
class Contact extends AppModel{

    /**
     * Name of this Contact Model.
     *
     * @var string
     */
    public $name = 'Contact';

    public $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id'
        )
    );

    public $hasMany = array(
        'Note' => array(
            'className' => 'Note',
            'foreignKey' => 'contact_id',
        )
    );

    public $virtualFields = array(
        'name' => 'CONCAT(Contact.first_name, " ", Contact.last_name)',
    );

    /**
     * Public validation for the contact model.
     *
     * @var array
     */
    public $validate = array(
        'first_name' => array(
            'notEmpty' => array(
                'rule' => 'notEmpty',
                'message' => 'First name must be required!'
            ),
            'rule1' => array(
                'rule'    => array('between', 2, 20),
                'message' => 'First Name must be between 2 to 20 characters!'
            ),
            'custom' => array(
                'rule' => array('custom','/^[A-Z. a-z]{1,21}$/'),
                'message' => 'Number/Special character and space are not allowed, only allow(A-Z, a-z, .)!',
            )
        ),
        'last_name' => array(
            'notEmpty' => array(
                'rule' => 'notEmpty',
                'message' => 'Last name must be required!'
            ),
            'rule1' => array(
                'rule'    => array('between', 2, 20),
                'message' => 'Last Name must be between 2 to 20 characters!'
            ),
            'custom' => array(
                'rule' => array('custom','/^[A-Z. a-z]{1,21}$/'),
                'message' => 'Number/Special character and space are not allowed, only allow(A-Z, a-z, .)!',
            )
        ),

        'email' => array(
            'notEmpty' => array(
                'rule' => 'notEmpty',
                'message' => 'Username must be required'
            ),
            'email' => array(
                'rule' => 'email',
                'message' => 'Please provide valid email'
            ),
            'unique' => array(
                'rule' => 'isUnique',
                'message' => 'Sorry, this email has been already taken',
                'on' => 'create',
            )
        ),

        'phone' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'message' => 'Phone Number must be required!',
                'allowEmpty' => true
            ),
            'rule2' => array(
                'rule' => array('phone', '/^[(]{0,1}[0-9]{3}[)]{0,1}[-\s.]{0,1}[0-9]{3}[-\s.]{0,1}[0-9]{4}$/'),
                'message' => 'Phone number in us standard!',
            ),
        ),

        'source' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'message' => 'Source must be required!',
            )
        ),

        'follow_up_date' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'message' => 'Follow up must be required!',
            )
        ),
    );

    /**
     * @param string $needed
     * @param null $contactID
     * @param bool $limit
     * @param string $orderBy
     * @param int $recursive
     * @param array $conditions
     * @return array
     *
     * Getting contact information and this function will return various information according the given parameter.
     */
    public function getContact($needed = 'all', $contactID = null, $limit = false, $orderBy = 'DESC', $recursive = 1, $conditions = array())
    {
        $newConditions = array();
        $newConditions = array_merge($newConditions, $conditions);
        if($contactID){
            $newConditions = array_merge($newConditions, array('Contact.id' => $contactID));
        }

        $result = $this->find(
            $needed,
            array(
                'conditions' => $newConditions,
                'limit' => $limit,
                'recursive' => $recursive,
                'order' => 'Contact.id '. $orderBy,
            )
        );

        if($result){
            return $result;
        }
        return $result;
    }

    /**
     * Validation set for the user login.
     * @return bool
     */
    public function importFormValidation()
    {
        $validateLogin = array(
            'file' => array(
                'isFile' => array(
                    'rule' => array('isFile'),
                    'message' => 'Please provide an csv file',
                ),
                'size' => array(
                    'rule' => array('size', 24, 10240000),
                    'message' => 'file size should be between 1KB - 10MB'
                ),
                'Extension' => array(
                    'rule' => array(
                        'extension',
                        array('csv')
                    ),
                    'message' => "Please supply a valid csv format file"
                ),
            ),
            'type' => array(
                'rule' => 'notEmpty',
                'message' => 'Password must be required'
            )
        );

        $this->validate = $validateLogin;
        return $this->validates();
    }

    /**
     * @param $data
     * @return bool
     *
     * Checking the given document is image or not.
     */
    public function isFile($data) {
        if(!empty($data['file']['name']))
        {
            return true;
        }
        return false;
    }

    /**
     * @param $data
     * @param $min
     * @param $max
     * @return bool
     *
     * Checking the size of given document.
     */
    public function size($data, $min, $max) {
        $size =$data['file']['size'] ;
        if($size > $min && $size < $max)
        {
            return true;
        }
        return false;
    }

    /**
     * @param null $userID
     * @param array $options
     * @return array
     */
    public function countContact($userID = null, $options = array())
    {
        $conditions = array('Contact.user_id' => $userID);
        $conditions = array_merge($conditions, $options);
        $result = $this->find('count', array('conditions' => $conditions));
        return $result;
    }

    /**
     * @param array $options
     * @return array
     */
    public function getContactOverview($options = array())
    {
        $conditions = array();
        if(isset($options['conditions'])){
            $conditions = array_merge($conditions, $options['conditions']);
        }

        $fields = array(
            'SUM(CASE WHEN Contact.account_type = 1 THEN 1 ELSE 0 END) AS student',
            'SUM(CASE WHEN Contact.account_type = 2 THEN 1 ELSE 0 END) AS customer',
            'SUM(CASE WHEN Contact.account_type = 3 THEN 1 ELSE 0 END) AS lead',
            'COUNT(Contact.id) as totalContact'
        );

        $options = array(
            'conditions' => $conditions,
            'fields' => $fields,
            'recursive' => -1
        );

        $result = $this->find('all', $options);
        return $result;
    }

    /**
     * @param $sources
     * @param $userID
     * @return array
     */
    public function getContactOverviewBySource($sources, $userID = false)
    {
        $leadWithSource = 0;

        if ($userID) {
            $data['all']['name'] = 'All Source';
            $data['all']['count'] = $this->find('count', array('conditions' => array('user_id' => $userID)));
            foreach ($sources as $source) {
                $count = $this->find(
                    'count',
                    array('conditions' => array('user_id' => $userID, 'Contact.source_id' => $source['Source']['id']))
                );
                $data[$source['Source']['slug']]['name'] = $source['Source']['name'];
                $data[$source['Source']['slug']]['count'] = $count;
                $leadWithSource = $leadWithSource + $count;
            }
        } else {
            $data['all']['name'] = 'All Source';
            $data['all']['count'] = $this->find('count');
            foreach ($sources as $source) {
                $count = $this->find(
                    'count',
                    array('conditions' => array('Contact.source_id' => $source['Source']['id']))
                );
                $data[$source['Source']['slug']]['name'] = $source['Source']['name'];
                $data[$source['Source']['slug']]['count'] = $count;
                $leadWithSource = $leadWithSource + $count;
            }
        }

        $data['other']['name'] = 'Other Source';
        $data['other']['count'] = $data['all']['count'] - $leadWithSource;

        return $data;
    }

    public function getContactOverviewByRating($userID = false, $options = array())
    {
        $conditions = array();

        if($userID)
        {
            $conditions = array_merge($conditions, array('Contact.user_id' => $userID));
        }

        if(isset($options['condition']))
        {
            $conditions = array_merge($conditions, $options['condition']);
        }

        $overview = $this->find('all', array(
            'fields' => array(
                'COUNT(Contact.id) AS total',
                'SUM(CASE WHEN Contact.rating = 0 THEN 1 ELSE 0 END) AS noRatingLead',
                'SUM(CASE WHEN Contact.rating = 1 THEN 1 ELSE 0 END) AS oneStarRatingLead',
                'SUM(CASE WHEN Contact.rating = 2 THEN 1 ELSE 0 END) AS twoStarRatingLead',
                'SUM(CASE WHEN Contact.rating = 3 THEN 1 ELSE 0 END) AS threeStarRatingLead',
                'SUM(CASE WHEN Contact.rating = 4 THEN 1 ELSE 0 END) AS fourStarRatingLead',
                'SUM(CASE WHEN Contact.rating = 5 THEN 1 ELSE 0 END) AS fiveStarRatingLead',
            ),
            'conditions' => $conditions,
            'recursive' => -1
        ));

        return $overview;
    }

    /**
     * @param $userID
     * @param $query
     * @return array
     */
    public function getContactList($userID, $query)
    {
        $conditions = array(
            'Contact.user_id' => $userID,
            'OR' => array(
                'Contact.first_name LIKE' => '%'.$query.'%',
                'Contact.last_name LIKE' => '%'.$query.'%',
                'Contact.email LIKE' => '%'.$query.'%',
                'Contact.phone LIKE' => '%'.$query.'%',
            )
        );

        $fields = array(
            'CONCAT(Contact.first_name, " ", Contact.last_name, " - ", Contact.email) as contactName',
        );

        $options = array(
            'conditions' => $conditions,
            'fields' => $fields,
            'recursive' => -1
        );

        $result = $this->find('all', $options);
        return $result;
    }

    public function getContactByID($contactID)
    {
        $result = $this->find(
            'first',
            array(
                'conditions' =>
                    array(
                        'Contact.id' => $contactID
                    ),
                'recursive' => -1
            )
        );

        return $result;
    }

    public function getContactByEMail($contactEmail)
    {
        $result = $this->find(
            'first',
            array(
                'conditions' =>
                    array(
                        'Contact.email' => $contactEmail
                    ),
                'recursive' => -1
            )
        );

        if($result){
            return $result['Contact']['id'];
        }

        return false;
    }

    public function getSourcesForPie($options = array())
    {
        $conditions = array();
        if(isset($options['conditions'])){
            $conditions = array_merge($conditions, $options['conditions']);
        }

        foreach($options['sources'] as $key => $value){
            $fields[] = 'SUM(CASE WHEN Contact.source = "'.$value.'" THEN 1 ELSE 0 END) AS "'.$value.'"';
        }
        array_push($fields, 'SUM(CASE WHEN Contact.is_custom_source = 1 THEN 1 ELSE 0 END) AS customSource');


        $options = array(
            'conditions' => $conditions,
            'fields' => $fields,
            'recursive' => -1,
        );

        $result = $this->find('all', $options);
        return $result;
    }
}