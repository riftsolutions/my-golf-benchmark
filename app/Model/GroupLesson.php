<?php
App::uses('AppModel', 'Model');
class GroupLesson extends AppModel
{
    /**
     * @var string
     *
     * Name of this GroupLesson Model
     */
    public $name = 'GroupLesson';

    public $hasMany = array(
        'GroupLessonsStudents' => array(
            'className' => 'GroupLessonsStudents',
            'foreignKey' => 'group_lesson_id',
        )
    );

    /**
     * @param array $options
     * @return array
     */
    public function getGroupLesson($options = array())
    {
        $queryConditions = array();

        if (isset($options['conditions'])) {
            if (is_array($options['conditions'])) {
                $queryConditions = array_merge($queryConditions, $options['conditions']);
            }
        }

        if (isset($options['start']) && isset($options['end'])) {
            if (is_array($options['conditions'])) {
                $queryConditions = array_merge($queryConditions,
                    array(
                        'GroupLesson.start >=' => $options['start'],
                        'GroupLesson.end <=' => $options['end']
                    )
                );
            }
        }

        $groupLessons = $this->find(
            'all',
            array(
                'conditions' => $queryConditions,
                'fields' => array('id', 'user_id', 'title', 'start', 'end', 'student_limit', 'filled_up_student', 'description', 'price', 'CONCAT(Profile.first_name, " ", Profile.last_name) as instructorName'),
                'limit' => false,
                'recursive' => -1,
                'order' => 'GroupLesson.id DESC',
                'joins' => array(
                    array(
                        'table' => 'profiles',
                        'type' => 'LEFT',
                        'alias' => 'Profile',
                        'conditions' => array('Profile.user_id = GroupLesson.user_id')
                    ),
                ),
            )
        );

        $data = array();
        if ($groupLessons) {
            foreach ($groupLessons as $lesson) {
                $data[] = array(
                    'instructor_name' => $lesson[0]['instructorName'],
                    'id' => $lesson['GroupLesson']['id'],
                    'title' => $lesson['GroupLesson']['title'],
                    'start' => $lesson['GroupLesson']['start'],
                    'end' => $lesson['GroupLesson']['end'],
                    'student_limit' => $lesson['GroupLesson']['student_limit'],
                    'filled_up_student' => $lesson['GroupLesson']['filled_up_student'],
                    'description' => $lesson['GroupLesson']['description'],
                    'price' => $lesson['GroupLesson']['price'],
                    'type' => 'group_lesson',
                    'editable' => true,
                    'resources' => [ltrim($lesson['GroupLesson']['user_id'])]
                );
            }
        }
        return $data;
    }

    public function updateGroupLessonStudent($groupID, $join = true)
    {
        $groupLesson = $this->find('first', array('conditions' => array('GroupLesson.id' => $groupID)));

        if($join)
        {
            $filledUpStudent = (int)$groupLesson['GroupLesson']['filled_up_student'] + 1;
        }
        else{
            $filledUpStudent = (int)$groupLesson['GroupLesson']['filled_up_student'] - 1;
        }

        $studentLimit = (int)$groupLesson['GroupLesson']['student_limit'];

        $this->id = (int )$groupLesson['GroupLesson']['id'];
        $this->saveField('filled_up_student', $filledUpStudent);

        /*if ($filledUpStudent >= $studentLimit) {
            $this->saveField('status', 2);
        }
        else{
            $this->saveField('status', 1);
        }*/
    }
}
