<?php
App::uses('AppModel', 'Model');
class PackagesUser extends AppModel
{
    /**
     * @var string
     *
     * Name of this model.
     */
    public $name = 'PackagesUser';

    public $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'created_for',
            'counterCache' => true
        ),
        'Package' => array(
            'className' => 'Package',
            'foreignKey' => 'package_id',
        )
    );


    public $validate = array(

        'created_for' => array(
            'rule1' => array(
                'rule' => array('notEmpty'),
                'message' => 'Please select the student',
            )
        ),

        'package_id' => array(
            'rule1' => array(
                'rule' => array('notEmpty'),
                'message' => 'Please select the package',
            )
        ),
    );

    /**
     * @param $id
     * @return array
     */
    public function getPackageUser($id)
    {
        $result = $this->find(
            'first',
            array(
                'conditions' => array(
                        'PackagesUser.id' => $id
                ),
                'fields' => array(
                    'Package.id',
                    'Package.uuid',
                    'Package.name',
                    'Package.created_by',
                    'Package.description',
                    'Package.lesson',
                    'Package.price',
                    'Package.tax_percentage',
                    'Package.tax',
                    'Package.total',
                    'Package.status',
                    'User.id',
                    'User.uuid',
                    'User.username',
                    'Profile.first_name',
                    'Profile.last_name',
                    'Profile.phone',
                    'Profile.billing_street_1',
                    'Instructor.first_name',
                    'Instructor.last_name',
                    'InstructorUsers.uuid',
                    'PackagesUser.*',
                ),
                'joins' => array(
                    array(
                        'table' => 'packages',
                        'type' => 'LEFT',
                        'alias' => 'Package',
                        'conditions' => array('Package.id = PackagesUser.package_id')
                    ),
                    array(
                        'table' => 'profiles',
                        'type' => 'LEFT',
                        'alias' => 'Profile',
                        'conditions' => array('Profile.user_id = PackagesUser.created_for')
                    ),
                    array(
                        'table' => 'profiles',
                        'type' => 'LEFT',
                        'alias' => 'Instructor',
                        'conditions' => array('Instructor.user_id = PackagesUser.instructor_id')
                    ),
                    array(
                        'table' => 'users',
                        'type' => 'LEFT',
                        'alias' => 'User',
                        'conditions' => array('Profile.user_id = PackagesUser.created_for')
                    ),
                    array(
                        'table' => 'users',
                        'type' => 'LEFT',
                        'alias' => 'InstructorUsers',
                        'conditions' => array('InstructorUsers.id = PackagesUser.instructor_id')
                    )
                ),
            )
        );
        return $result;
    }

    /**
     * @param $packageID
     * @param $userID
     * @return bool
     */
    public function getRunningLesson($packageID, $userID)
    {
        $result = $this->find(
            'first',
            array(
                'conditions' =>
                array(
                    'PackagesUser.package_id' => $packageID,
                    'PackagesUser.created_for' => $userID,
                    'PackagesUser.payment_status' => 1,
                    'PackagesUser.status' => 1
                ),
                'recursive' => -1
            )
        );

        if($result)
        {
            return $result['PackagesUser'];
        }
        return false;
    }

    /**
     * @param $packageID
     * @param $userID
     * @return array|bool
     */
    public function getPackagesWithout($packageID, $userID)
    {
        $result = $this->find(
            'all',
            array(
                'conditions' =>
                array(
                    'PackagesUser.package_id !=' => $packageID,
                    'PackagesUser.created_for' => $userID,
                ),
                'recursive' => -1,
                'order' => 'PackagesUser.id ASC'
            )
        );

        if($result)
        {
            return $result;
        }
        return false;
    }

    /**
     * @param $options
     * @return array
     */
    public function getPOS($options = array())
    {
        $conditions = array();
        $queryType = 'all';
        $limit = false;

        if(isset($options['conditions'])){
            $conditions = array_merge($conditions, $options['conditions']);
        }

        if(isset($options['query'])){
            $queryType = $options['query'];
        }

        if(isset($options['limit'])){
            $limit = $options['limit'];
        }

        $fields = array(

        );

        $options = array(
            'conditions' => $conditions,
            'fields' => $fields,
            'recursive' => -1,
            'limit' => $limit,
            'order' => 'PackagesUser.id DESC'
        );

        $result = $this->find($queryType, $options);
        return $result;
    }

    public function getPOSInfo($options = array())
    {
        $conditions = array();
        $queryType = 'all';
        $limit = false;

        if(isset($options['conditions'])){
            $conditions = array_merge($conditions, $options['conditions']);
        }

        if(isset($options['query'])){
            $queryType = $options['query'];
        }

        if(isset($options['limit'])){
            $limit = $options['limit'];
        }

        $fields = array(
            'CONCAT(Profile.first_name, " ", Profile.last_name) as studentName',
            'Package.name',
            'User.uuid',
            'Package.uuid',
            'Package.price',
            'Package.tax',
            'Package.total',
            'Package.tax_percentage',
            'PackagesUser.available',
            'PackagesUser.payment_status',
            'PackagesUser.created',
        );

        $options = array(
            'conditions' => $conditions,
            'fields' => $fields,
            'recursive' => -1,
            'limit' => $limit,
            'order' => 'PackagesUser.id DESC',
            'joins' => array(
                array(
                    'table' => 'users',
                    'type' => 'LEFT',
                    'alias' => 'User',
                    'conditions' => array('User.id = PackagesUser.created_for')
                ),
                array(
                    'table' => 'profiles',
                    'type' => 'LEFT',
                    'alias' => 'Profile',
                    'conditions' => array('Profile.user_id = PackagesUser.created_for')
                ),
                array(
                    'table' => 'packages',
                    'type' => 'LEFT',
                    'alias' => 'Package',
                    'conditions' => array('Package.id = PackagesUser.package_id')
                )
            )
        );

        $result = $this->find($queryType, $options);
        return $result;
    }

    public function getPOSOverview($options = array())
    {
        $conditions = array();

        if(isset($options['conditions'])){
            $conditions = array_merge($conditions, $options['conditions']);
        }

        $fields = array(
            'SUM(CASE WHEN PackagesUser.status = 1 THEN 1 ELSE 0 END) AS countRunningPOS',
            'SUM(CASE WHEN PackagesUser.status = 2 THEN 1 ELSE 0 END) AS countCompletedPOS',
            'SUM(CASE WHEN PackagesUser.status = 3 THEN 1 ELSE 0 END) AS countExpiredPOS',
            'SUM(CASE WHEN PackagesUser.payment_status = 0 THEN 1 ELSE 0 END) AS unpaidPOS',
            'SUM(CASE WHEN PackagesUser.payment_status = 1 THEN 1 ELSE 0 END) AS paidPOS',
            'COUNT(PackagesUser.id) as totalPOS'
        );

        $options = array(
            'conditions' => $conditions,
            'fields' => $fields,
            'recursive' => -1,
        );

        $result = $this->find('all', $options);
        return $result;
    }

    /**
     * @param $options
     * @return array
     */
    public function bestStudent($options)
    {
        $conditions = array();
        $queryType = 'all';
        $limit = 5;

        if(isset($options['conditions'])){
            $conditions = array_merge($conditions, $options['conditions']);
        }

        if(isset($options['query'])){
            $queryType = $options['query'];
        }

        if(isset($options['limit'])){
            $limit = $options['limit'];
        }

        $fields = array(
            'CONCAT(Profile.first_name, " ", Profile.last_name) as studentName',
            'COUNT(PackagesUser.created_for) as PackagePurchases',
            'SUM(Package.total) as PackagePrice'
        );

        $options = array(
            'conditions' => $conditions,
            'fields' => $fields,
            'recursive' => -1,
            'limit' => $limit,
            'group' => 'PackagesUser.created_for',
            'order' => 'PackagePrice DESC',
            'joins' => array(
                array(
                    'table' => 'profiles',
                    'type' => 'LEFT',
                    'alias' => 'Profile',
                    'conditions' => array('Profile.user_id = PackagesUser.created_for')
                ),
                array(
                    'table' => 'packages',
                    'type' => 'LEFT',
                    'alias' => 'Package',
                    'conditions' => array('Package.id = PackagesUser.package_id')
                ),
            )
        );

        $result = $this->find($queryType, $options);
        return $result;
    }

    /**
     * @param $options
     * @return array
     *
     */
    public function bestPackages($options)
    {
        $conditions = array();
        $queryType = 'all';
        $limit = 5;

        if(isset($options['conditions'])){
            $conditions = array_merge($conditions, $options['conditions']);
        }

        if(isset($options['query'])){
            $queryType = $options['query'];
        }

        if(isset($options['limit'])){
            $limit = $options['limit'];
        }

        $fields = array(
            'COUNT(PackagesUser.package_id) as PackagePurchases',
            'SUM(Package.total) as PackagePrice',
            'Package.name'
        );

        $options = array(
            'conditions' => $conditions,
            'fields' => $fields,
            'recursive' => -1,
            'limit' => $limit,
            'group' => 'PackagesUser.package_id',
            'order' => 'PackagePurchases DESC',
            'joins' => array(
                array(
                    'table' => 'profiles',
                    'type' => 'LEFT',
                    'alias' => 'Profile',
                    'conditions' => array('Profile.user_id = PackagesUser.created_for')
                ),
                array(
                    'table' => 'packages',
                    'type' => 'LEFT',
                    'alias' => 'Package',
                    'conditions' => array('Package.id = PackagesUser.package_id')
                ),
            )
        );

        $result = $this->find($queryType, $options);
        return $result;
    }

    public function getUsersPackageIds($userID)
    {
        $result = $this->find('list', array(
                'conditions' => array(
                    'PackagesUser.created_for' => $userID,
                    'PackagesUser.status !=' => 1

                ),
                'fields' => array('PackagesUser.id', 'PackagesUser.package_id')
            )
        );

        return $result;
    }

    public function getUsersPackageList($userID)
    {
        $result = $this->find('all', array(
                'conditions' => array(
                    'PackagesUser.created_for' => $userID,
                    'PackagesUser.status' => 1,
                    'PackagesUser.available >=' => 0,
                    'PackagesUser.payment_status ' => 1,
                    'PackagesUser.total_lesson !=' => 0,

                ),

                'fields' =>
                    array(
                        'PackagesUser.id',
                        'PackagesUser.package_id',
                        'PackagesUser.available',
                        'PackagesUser.count_appointment',
                        'PackagesUser.total_lesson',
                        'Package.name',
                        'Package.lesson'
                    ),
                'joins' => array(
                    array(
                        'table' => 'packages',
                        'type' => 'LEFT',
                        'alias' => 'Package',
                        'conditions' => array('Package.id = PackagesUser.package_id')
                    ),
                )

            )
        );

        $data = array();
        foreach($result as $package){
            if($package['PackagesUser']['total_lesson'] > $package['PackagesUser']['count_appointment']){
                $data[$package['PackagesUser']['package_id'] ] = $package['Package']['name'];
            }
        }

        return $data;
    }

    public function getPurchaseAmount($options = array())
    {
        $conditions = array();

        if(isset($options['conditions'])){
            $conditions = array_merge($conditions, $options['conditions']);
        }

        $fields = array(
            'COUNT(PackagesUser.package_id) as PackagePurchases',
            'SUM(Package.total) as packagePrice',
            'SUM(Package.tax) as taxAmount',
            'SUM(Billing.amount) as totalPaid',
        );

        $options = array(
            'conditions' => $conditions,
            'fields' => $fields,
            'recursive' => -1,
            'joins' => array(
                array(
                    'table' => 'packages',
                    'type' => 'LEFT',
                    'alias' => 'Package',
                    'conditions' => array('Package.id = PackagesUser.package_id')
                ),
                array(
                    'table' => 'billings',
                    'type' => 'LEFT',
                    'alias' => 'Billing',
                    'conditions' => array('Billing.user_id = PackagesUser.created_for')
                ),
            )
        );



        $result = $this->find('all', $options);
        return $result;
    }


    /**
     * @param $userID
     * @return array
     */
    public function countUsersPurchasedLesson($userID)
    {
        $result = $this->find(
            'all',
            array(
                'conditions' => array(
                    'PackagesUser.created_for' => $userID
                ),
                'recursive' => -1,
                'fields' => array(
                    'PackagesUser.id',
                    'SUM(PackagesUser.total_lesson) as totalLesson',
                ),
            )
        );

        if($result[0]){
            return (int) $result[0][0]['totalLesson'];
        }
        return 0;
    }

    public function countPackagesByUserID($userID)
    {
        $result = $this->find(
            'count',
            array(
                'conditions' => array(
                    'PackagesUser.created_for' => $userID
                ),
                'recursive' => -1,
            )
        );

        if($result){
            return $result;
        }
        return 0;
    }

    public function updateUsersPackageLesson($packageID, $studentID, $lessonCount = 1, $added = false)
    {
        $packageUser = $this->getRunningLesson($packageID, $studentID);
        $isLessonExceed = false;
        $lessonSaveInUsersPackage = $lessonCount;

        if($packageUser['available'] <  $lessonCount)
        {
            $isLessonExceed = true;
            $exceedLesson = $lessonCount - $packageUser['available'];
            $lessonSaveInUsersPackage = $lessonCount - $exceedLesson;
        }




        if($packageUser){

            if($added == true)
            {
                $currentLessonNo = $packageUser['count_appointment'] + $lessonSaveInUsersPackage;
                $setAvailableLesson = $packageUser['available'] - $lessonSaveInUsersPackage;
            }
            else{
                $currentLessonNo = $packageUser['count_appointment'] - $lessonSaveInUsersPackage;
                $setAvailableLesson = $packageUser['available'] + $lessonSaveInUsersPackage;
            }


            $this->id = $packageUser['id'];
            $this->saveField('count_appointment', $currentLessonNo);
            $this->saveField('available', $setAvailableLesson);
        }

        if($isLessonExceed)
        {
            $this->manageExceedLesson($isLessonExceed, $studentID, $exceedLesson, $packageUser['id']);
        }
    }

    public function getCurrentLessonOfPackage($packageID, $userID)
    {
        $result = $this->find(
            'first',
            array(
                'conditions' =>
                    array(
                        'PackagesUser.package_id' => $packageID,
                        'PackagesUser.created_for' => $userID,
                        'PackagesUser.payment_status' => 1,
                        'PackagesUser.status' => 1
                    ),
                'recursive' => -1
            )
        );

        if($result)
        {
            return $result['PackagesUser']['count_appointment'];
        }
        return 0;
    }

    public function manageExceedLesson($currentPackageID, $userID, $exceedLesson, $usersPackageID)
    {
        $packages = $this->getPackagesWithout($currentPackageID, $userID);

        foreach($packages as $package)
        {
            if($exceedLesson < 1){
                exit;
            }


            $lessonNeedToSave =  $exceedLesson;
            if($package['PackagesUser']['available'] <  $exceedLesson)
            {
                $extra = $exceedLesson - $package['PackagesUser']['available'];
                $lessonNeedToSave = $exceedLesson - $extra;
            }
            $currentLessonNo = $package['PackagesUser']['count_appointment'] + $lessonNeedToSave;
            $setAvailableLesson = $package['PackagesUser']['available'] - $lessonNeedToSave;
            $this->id = (int) $package['PackagesUser']['id'];
            $this->saveField('count_appointment', $currentLessonNo);
            $this->saveField('available', $setAvailableLesson);

            $exceedLesson = $exceedLesson - $lessonNeedToSave;
        }

        if($exceedLesson > 0){
            $this->id = $usersPackageID;
            $this->saveField('lesson_exceed', $exceedLesson);
            $exceedLesson = 0;
        }
    }


}


