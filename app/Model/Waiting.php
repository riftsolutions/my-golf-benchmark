<?php
App::uses('AppModel', 'Model');
class Waiting extends AppModel
{
    /**
     * @var string
     *
     * Name of this Billing Model
     */
    public $name = 'Waiting';

    public $belongsTo = array(
        'Package' => array(
            'className' => 'Package',
            'foreignKey' => 'package_id'
        )
    );

    public function checkExistence($eventID, $userID)
    {
        $result = $this->find('first', array('conditions' => array('Waiting.event_id' => $eventID, 'Waiting.waiting_by' => $userID)));

        if($result){
            return true;
        }
        return false;
    }

    public function getWaitingList($options = array())
    {
        $limit = 5;
        $conditions = array();

        if(isset($options['conditions'])){
            $conditions = array_merge($conditions, $options['conditions']);
        }

        if(isset($options['limit'])){
            $limit = $options['limit'];
        }

        $fields = array(
            'User.uuid',
            'User.username',
            'Profile.first_name',
            'Profile.last_name',
            'Waiting.lesson_no',
            'Waiting.start',
            'Waiting.end',
            'Package.name',
            'Package.uuid',
            'CONCAT(Instructor.first_name, " ", Instructor.last_name) as instructorName',
            'InstructorUser.uuid'

        );

        $options = array(
            'conditions' => $conditions,
            'fields' => $fields,
            'recursive' => -1,
            'limit' => $limit,
            'order' => 'Waiting.start ASC',
            'joins' => array(
                array(
                    'table' => 'profiles',
                    'type' => 'LEFT',
                    'alias' => 'Profile',
                    'conditions' => array('Profile.user_id = Waiting.appointment_by')
                ),

                array(
                    'table' => 'users',
                    'type' => 'LEFT',
                    'alias' => 'User',
                    'conditions' => array('User.id = Waiting.appointment_by')
                ),

                array(
                    'table' => 'packages',
                    'type' => 'LEFT',
                    'alias' => 'Package',
                    'conditions' => array('Package.id = Waiting.package_id')
                ),
                array(
                    'table' => 'profiles',
                    'type' => 'LEFT',
                    'alias' => 'Instructor',
                    'conditions' => array('Instructor.user_id = Profile.instructor_id')
                ),
                array(
                    'table' => 'users',
                    'type' => 'LEFT',
                    'alias' => 'InstructorUser',
                    'conditions' => array('InstructorUser.id = Profile.instructor_id')
                ),

            )
        );

        $result = $this->find('all', $options);
        return $result;
    }

    public function getWaitingUsers($appointmentID){
        $users = $this->find(
            'list',
            array(
                'conditions' => array('Waiting.appointment_id' => $appointmentID),
                'fields' => array('User.username'),
                'joins' => array(
                    array(
                        'table' => 'users',
                        'type' => 'LEFT',
                        'alias' => 'User',
                        'conditions' => array('User.id = Waiting.appointment_by')
                    ),
                )
            )
        );

        return $users;
    }


    public function countWaitingNotification($options = array()){

        $conditions = array();
        if(isset($options['conditions'])){
            $conditions = array_merge($conditions, $options['conditions']);
        }

        $users = $this->find(
            'count',
            array(
                'conditions' => $conditions,
            )
        );

        return $users;
    }


    public function getWaitingByID($id)
    {
        $result = $this->find(
            'first',
            array(
                'fields' => array(
                    'Waiting.*',
                    'Package.*',
                    'Instructor.*',
                    'InstructorUser.*',
                ),
                'recursive' => -1,
                'conditions' => array(
                    'Waiting.id' => $id
                ),
                'joins' => array(
                    array(
                        'table' => 'packages',
                        'type' => 'LEFT',
                        'alias' => 'Package',
                        'conditions' => array('Package.id = Waiting.package_id')
                    ),
                    array(
                        'table' => 'profiles',
                        'type' => 'LEFT',
                        'alias' => 'Instructor',
                        'conditions' => array('Instructor.user_id = Waiting.appointment_by')
                    ),
                    array(
                        'table' => 'users',
                        'type' => 'LEFT',
                        'alias' => 'InstructorUser',
                        'conditions' => array('InstructorUser.id = Waiting.appointment_by')
                    ),
                ),
            )
        );

        if($result)
        {
            return $result;
        }
        return null;
    }

    public function getWaitingIDByUuid($uuid)
    {
        $result = $this->find(
            'first',
            array(
                'recursive' => -1,
                'conditions' => array(
                    'Waiting.uuid' => $uuid
                )
            )
        );

        if($result)
        {
            return $result['Waiting']['id'];
        }
        return null;
    }

    public function getWaitingIDByNotificationCode($code)
    {
        $result = $this->find(
            'first',
            array(
                'recursive' => -1,
                'conditions' => array(
                    'Waiting.notification_code' => $code
                )
            )
        );

        if($result)
        {
            return $result['Waiting']['id'];
        }
        return null;
    }

    public function countWaitingByUserID($userID)
    {
        $result = $this->find(
            'count',
            array(
                'conditions' => array(
                    'Waiting.appointment_by' => $userID
                ),
                'recursive' => -1,
            )
        );

        if($result){
            return $result;
        }
        return 0;
    }
}
