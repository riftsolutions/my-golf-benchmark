<?php
App::uses('AppModel', 'Model');

/**
 * RangeWaiting Model
 *
 * @property User $User
 * @property Waiting $Waiting
 */
class RangeWaiting extends AppModel
{


    //The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * belongsTo associations
     *
     * @var array
     */
//array('not' => array('RangeWaiting.instructor_id' => null))
    public $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),

    );

    /**
     * hasMany associations
     *
     * @var array
     */
    public $hasMany = array(
        'Waiting' => array(
            'className' => 'Waiting',
            'foreignKey' => 'range_waiting_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );

    /**
     * Here When a user want to set an appointment
     * Then it will return the existing waithingList
     */
    public function getRangeWaithingListByTime($startTime , $endTime, $userID)
    {
        $result = $this->find(
            'all', array(
                'conditions' => array(
                    'RangeWaiting.start_time <=' => $startTime,
                    'RangeWaiting.end_time >=' => $endTime,
                    'RangeWaiting.user_id !=' => $userID

                ),
        ));


        return $result;
    }
}
