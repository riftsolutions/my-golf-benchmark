<?php
App::uses('AppModel', 'Model');
class TcsUser extends AppModel
{

    public $name = 'TcsUser';

    public function getInstructorIdsByTCID($tcId)
    {
        $result = $this->find('all', array(
                'fields' => array(
                    'TcsUser.user_id',
                    'CONCAT(Profile.first_name, " ", Profile.last_name) as name'
                ),
                'conditions' => array('TcsUser.tc_id' => $tcId),
                'joins' => array(
                    array(
                        'table' => 'profiles',
                        'type' => 'LEFT',
                        'alias' => 'Profile',
                        'conditions' => array('Profile.user_id = TcsUser.user_id')
                    ),
                ),
                'order' => 'TcsUser.user_id ASC'
            )
        );
       // var_dump($result); die();


        if($result){
            $ids = array();
            foreach($result as $id){
                $ids[$id['TcsUser']['user_id']] = $id[0]['name'];
            }
            return $ids;
        }
        return false;
    }

    public function totalStudentByTc(){

    }

    public function getAppointmentOverview($options = array())
    {
        $conditions = array();

        if (isset($options['conditions'])) {
            $conditions = array_merge($conditions, $options['conditions']);
        }

        $fields = array(
            'SUM(CASE WHEN Appointment.status = 1 THEN 1 ELSE 0 END) AS countConfirmAppointment',
            'SUM(CASE WHEN Appointment.status = 2 THEN 1 ELSE 0 END) AS  countConfirmAppointment',
            'SUM(CASE WHEN Appointment.status = 3 THEN 1 ELSE 0 END) AS countCancelledAppointment',
            'SUM(CASE WHEN Appointment.status = 4 THEN 1 ELSE 0 END) AS countCompletedAppointment',
            'SUM(CASE WHEN Appointment.status = 5 THEN 1 ELSE 0 END) AS  countExpiredAppointment',
            'SUM(CASE WHEN Appointment.is_reschedule = 1 THEN 1 ELSE 0 END)  countRescheduledAppointment',

            // 'COUNT(Appointment.id) as total_appointment'
        );

        $options = array(
            'conditions' => $conditions,
            'fields' => $fields,
            'recursive' => -1,
        );

        $result = $this->find('all', $options);

        return $result;
    }
    /**
     * @param $tcId
     * @return array
     * This function will get instructor ids for and tc user
     */
    public function getIntructorIdsForTC($tcId){

        $instructorIDs = array_keys($this->getInstructorIdsByTCID($tcId));
        return $instructorIDs;
    }

}
