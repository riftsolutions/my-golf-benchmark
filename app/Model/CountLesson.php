<?php
App::uses('AppModel', 'Model');
class CountLesson extends AppModel
{
    /**
     * @var string
     *
     * Name of this CountLesson Model
     */
    public $name = 'CountLesson';

    public $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id'
        )
    );

    /**
     * @param $userID
     * @param bool $substruct
     * @return bool
     */
    public function updateUsersLesson($userID, $substruct = false, $lesson = 1)
    {
        $result = $this->find('first', array('conditions' => array('CountLesson.user_id' => $userID)));
        if ($result) {

            $appointedLessonCount = $result['CountLesson']['appointed_lesson_count'] + $lesson;
            if ($substruct) {
                $appointedLessonCount = $result['CountLesson']['appointed_lesson_count'] - $lesson;
            }

            $this->id = $result['CountLesson']['id'];
            if ($this->saveField('appointed_lesson_count', $appointedLessonCount)) {
                return $this->updateLessonLeft($userID);
            }

            return false;
        }
        return false;
    }

    /**
     * @param $userID
     * @param $countPurchasedLesson
     * @return bool
     */
    public function countUsersLesson($userID, $countPurchasedLesson)
    {
        $result = $this->find('first', array('conditions' => array('CountLesson.user_id' => $userID)));

        if(empty($result)){
            $data = array(
                'user_id' => $userID,
                'purchased_lesson_count' => $countPurchasedLesson,
                'appointed_lesson_count' => 0,
            );
            $this->create();
            $this->save($data);
        }
        else{
            $data = array(
                'user_id' => $userID,
                'purchased_lesson_count' => $result['CountLesson']['purchased_lesson_count'] + $countPurchasedLesson,
            );
            $this->id = $result['CountLesson']['id'];
            $this->save($data);
        }
        return $this->updateLessonLeft($userID);
    }

    /**
     * @param $userID
     * @return bool
     */
    public function updateLessonLeft($userID){
        $result = $this->find('first', array('conditions' => array('CountLesson.user_id' => $userID)));
        $lessonLeft = (int) $result['CountLesson']['purchased_lesson_count'] - (int) $result['CountLesson']['appointed_lesson_count'];
        $this->id = (int )$result['CountLesson']['id'];
        if($this->saveField('lesson_left', $lessonLeft)){
            return true;
        }
        return false;
    }

    /**
     * @param $userID
     * @return int
     */
    public function getUserLessonLeft($userID)
    {
        $result = $this->find('first', array('conditions' => array('CountLesson.user_id' => $userID)));

        if($result)
        {
            return $result['CountLesson']['lesson_left'];
        }
        return 0;
    }
}
