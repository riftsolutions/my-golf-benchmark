<?php
App::uses('AppModel', 'Model');
class Attachment extends AppModel
{
    /**
     * @var string
     *
     * Name of this Attachment Model
     */
    public $name = 'Attachment';

    /**
     * @var array
     *
     * Relation between attachment and other's model.
     */
    public $belongsTo = array(
        'Package' => array(
            'className' => 'Package',
            'foreignKey' => 'package_id',
        ),
    );

    public $actsAs = array(
        'Upload.Upload' => array(
            'attachment' => array(
                'fields' => array(
                    'dir' => 'attachment_dir'
                ),
                'thumbnailSizes' => array(
                    'medium' => '800x800',
                    'small' => '200x192',
                    'thumb' => '80x80'
                )
            ),
        )
    );
}
