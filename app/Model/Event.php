<?php
App::uses('AppModel', 'Model');
class Event extends AppModel
{
    /**
     * @var string
     *
     * Name of this Billing Model
     */
    public $name = 'Event';

    public $hasMany = array(
        'Waiting' => array(
            'className' => 'Waiting',
            'foreignKey' => 'event_id'
        )
    );

    public $belongsTo = array(
        'Profile' => array(
            'className' => 'Profile',
            'foreignKey' => 'created_by'
        ),
    );

    /**
     * @param string $needed
     * @param array $options
     * @return array
     *
     * Getting event information based on some conditions.
     */
    public function getEvents($needed = 'all', $options = array())
    {
        $limit = false;
        $orderBy = 'DESC';
        $recursive = -1;
        $queryConditions = array();

        if(isset($options['is_appointed'])){
            $queryConditions = array_merge($queryConditions, array('Event.is_appointed' => $options['is_appointed']));
        }

        if(isset($options['eventID'])){
            $queryConditions = array_merge($queryConditions, array('Event.id' => $options['eventID']));
        }

        if(isset($options['conditions'])){
            if(is_array($options['conditions'])){
                $queryConditions = array_merge($queryConditions, $options['conditions']);
            }
        }

        $result = $this->find(
            $needed,
            array(
                'conditions' => $queryConditions,
                'limit' => $limit,
                'recursive' => $recursive,
                'order' => 'Event.id '. $orderBy,
            )
        );

        if($result){
            return $result;
        }
        return $result;
    }


    /**
     * @param array $options
     * @return array
     *
     * This function will retrieve instructor's event list.
     */
    public function getEventsForCalendar($options = array())
    {
        $limit = false;
        $orderBy = 'DESC';
        $recursive = -1;
        $queryConditions = array();

        if (isset($options['conditions'])) {
            if (is_array($options['conditions'])) {
                $queryConditions = array_merge($queryConditions, $options['conditions']);
            }
        }

        if (isset($options['start']) && isset($options['end'])) {
            if (is_array($options['conditions'])) {
                $queryConditions = array_merge($queryConditions,
                    array(
                        'Event.start >=' => $options['start'],
                        'Event.end <=' => $options['end']
                    )
                );
            }
        }


        $events = $this->find(
            'all',
            array(
                'conditions' => $queryConditions,
                'fields' => array('id', 'start', 'end'),
                'limit' => $limit,
                'recursive' => $recursive,
                'order' => 'Event.id ' . $orderBy,
            )
        );

        $data = array();
        if ($events) {
            foreach ($events as $event) {
                $data[] = array(
                    'id' => $event['Event']['id'],
                    'start' => $event['Event']['start'],
                    'end' => $event['Event']['end'],
                    'type' => 'event'
                );
            }
        }
        return $data;
    }

    /**
     * @param $instructorID
     * @return array
     *
     * Get schedule of instructor by instructor id
     */
    public function getSchedule($instructorID)
    {
        $result = $this->find(
            'all',
            array(
                'conditions' => array('created_by' => $instructorID),
                'recursive' => -1,
                'order' => 'Event.id ASC',
            )
        );

        if($result){
            return $result;
        }
        return $result;
    }
}
