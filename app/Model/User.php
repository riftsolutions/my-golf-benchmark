<?php
App::uses('AppModel', 'Model');
class User extends AppModel
{
    /**
     * @var string
     *
     * Name of this model.
     */
    public $name = 'User';

    /**
     * Encrypting password before it store into the database.
     *
     * @param array $options
     * @return bool
     */
    public function beforeSave($options = array())
    {
        if(!empty($this->data['User']['password'])) {
            $this->data['User']['password'] = AuthComponent::password($this->data['User']['password']);
        } else {
            unset($this->data['User']['password']);
        }
        return true;

    }

    /**
     * Relation between User and Profile Model.
     * And this relation should be 'One to One'
     * this means one user only have one profile.
     * @var array
     */
    public $hasOne = array(
        'Profile' => array(
            'className' => 'Profile',
            'foreignKey' => 'user_id'
        ),
        'CountLesson' => array(
            'className' => 'CountLesson',
            'foreignKey' => 'user_id'
        )
    );

    /**
     * @var array
     *
     * Has Many relation between User and Lesson, Benchmark table.
     */
    public $hasMany = array(
        /*'Lesson' => array(
            'className' => 'Lesson',
            'foreignKey' => 'created_for'
        ),*/
        'Benchmark' => array(
            'className' => 'Benchmark',
            'foreignKey' => 'created_for'
        ),
        'Event' => array(
            'className' => 'Event',
            'foreignKey' => 'created_by'
        ),
        'Appointment' => array(
            'className' => 'Appointment',
            'foreignKey' => 'appointment_by'
        ),
        'Waiting' => array(
            'className' => 'Waiting',
            'foreignKey' => 'appointment_by'
        ),
        'Contact' => array(
            'className' => 'Contact',
            'foreignKey' => 'user_id'
        ),
        'PackagesUser' => array(
            'className' => 'PackagesUser',
            'foreignKey' => 'created_for'
        ),
        'Note' => array(
            'className' => 'Note',
            'foreignKey' => 'user_id',
        ),
        'BlockTime' => array(
            'className' => 'BlockTime',
            'foreignKey' => 'user_id',
        ),
        'GroupLessonsStudents' => array(
            'className' => 'GroupLessonsStudents',
            'foreignKey' => 'user_id',
        )
    );

    public $hasAndBelongsToMany = array(
        'Package' => array(
            'className' => 'Package',
            'joinTable' => 'packages_users',
            'foreignKey' => 'created_for',
            'associationForeignKey' => 'package_id'
        ),
    );

    /**
     * Validated Username,Password,Confirm Password.
     * This validation is done by default CakePHP validation rules.
     * @var array
     */
    public $validate = array(
        'username' => array(
            'notempty' => array(
                'rule' => 'notempty',
                'message' => 'Email must be required'
            ),
            'email' => array(
                'rule' => 'email',
                'message' => 'Please provide valid email'
            ),
            'unique' => array(
                'on' => 'create',
                'rule' => 'isUnique',
                'message' => 'Sorry, this email has been already taken'
            )
        ),
        'password' => array(
            'notempty' => array(
                'on' => 'create',
                'rule' => 'notempty',
                'message' => 'Password must be required'
            ),
            'size' => array(
                'on' => 'create',
                'rule' => array('between', 6, 25),
                'message' => 'Password should be at least 6 characters long'
            )
        ),
        'cPassword' => array(
            'notempty' => array(
                'on' => 'create',
                'rule' => 'notempty',
                'message' => 'Confirm password must be required'
            ),
            'size' => array(
                'on' => 'create',
                'rule' => array('between', 6, 25),
                'message' => 'Confirm Password should be at least 6 characters long'
            ),
            array(
                'on' => 'create',
                'rule' => array('CheckPass'),
                'message' => 'Password and Confirm Password not Match'
            )
        )
    );

    /**
     * Validation set for the user signup.
     * @return bool
     */
    public function signupValidate()
    {
        $validations = array(
            'username' => array(
                'notempty' => array(
                    'rule' => 'notempty',
                    'message' => 'Email must be required'
                ),
                'email' => array(
                    'rule' => 'email',
                    'message' => 'Please provide valid email'
                ),
                'unique' => array(
                    'on' => 'create',
                    'rule' => 'isUnique',
                    'message' => 'Sorry, this email has been alreadyxxxxxxx taken'
                )
            ),
            'password' => array(
                'notempty' => array(
                    'on' => 'create',
                    'rule' => 'notempty',
                    'message' => 'Password must be required'
                ),
                'size' => array(
                    'on' => 'create',
                    'rule' => array('between', 6, 25),
                    'message' => 'Password should be at least 6 characters long'
                )
            ),
            'cPassword' => array(
                'notempty' => array(
                    'on' => 'create',
                    'rule' => 'notempty',
                    'message' => 'Confirm password must be required'
                ),
                'size' => array(
                    'on' => 'create',
                    'rule' => array('between', 6, 25),
                    'message' => 'Confirm Password should be at least 6 characters long'
                ),
                array(
                    'on' => 'create',
                    'rule' => array('CheckPass'),
                    'message' => 'Password and Confirm Password not Match'
                )
            )
        );

        $this->validate = $validations;
        return $this->validates();
    }

    /**
     * Validation set for the user login.
     * @return bool
     */
    public function loginValidate()
    {
        $validateLogin = array(
            'username' => array(
                'notempty' => array(
                    'rule' => 'notEmpty',
                    'message' => 'Username must be required'
                ),
                'email' => array(
                    'rule' => 'email',
                    'message' => 'Please provide valid email'
                ),
            ),
            'password' => array(
                'rule' => 'notEmpty',
                'message' => 'Password must be required'
            )
        );

        $this->validate = $validateLogin;
        return $this->validates();
    }

    /**
     * Validation set for the forgot password.
     * @return bool
     */
    public function forgotPassValidate()
    {
        $validateForgotPass = array(
            'username' => array(
                'notempty' => array(
                    'rule' => 'notempty',
                    'message' => 'Email must be required'
                ),
                'email' => array(
                    'rule' => 'email',
                    'message' => 'Please provide valid email'
                ),
            ),
        );
        $this->validate = $validateForgotPass;
        return $this->validates();
    }


    /**
     * @return bool
     * @description This is the validation set for the reset Password.
     */
    public function resetPassValidate()
    {
        $validateResetPass = array(
            'password' => array(
                'notempty' => array(
                    'rule' => 'notempty',
                    'message' => 'Password must be required'
                ),
                'size' => array(
                    'rule' => array('between', 6, 25),
                    'message' => 'Password should be at least 6 characters long'
                )
            ),
            'cPassword' => array(
                'notempty' => array(
                    'rule' => 'notempty',
                    'message' => 'Confirm password must be required'
                ),
                array(
                    'rule' => array('CheckPass'),
                    'message' => 'Password and Confirm Password not Match'
                ),
                'size' => array(
                    'rule' => array('between', 6, 25),
                    'message' => 'Confirm Password should be at least 6 characters long'
                )
            ),
        );

        $this->validate = $validateResetPass;
        return $this->validates();
    }

    /**
     * Validation set for the user login.
     * @return bool
     */
    public function editProfileValidation()
    {
        $validateLogin = array(
            'username' => array(
                'notempty' => array(
                    'rule' => 'notempty',
                    'message' => 'Email must be required'
                ),
                'email' => array(
                    'rule' => 'email',
                    'message' => 'Please provide valid email'
                ),
                array(
                    'rule' => array('isUniqueOnProfileEdit'),
                    'message' => 'Sorry, this email has been already taken'
                )
            ),
            'password' => array(
                'notempty' => array(
                    'rule' => 'notempty',
                    'message' => 'Password must be required',
                    'allowEmpty' => true
                ),
                'size' => array(
                    'rule' => array('between', 6, 25),
                    'message' => 'Password should be at least 6 characters long'
                )
            ),
            'cPassword' => array(
                array(
                    'rule' => array('isPasswordGiven'),
                    'message' => 'Confirm password must be required'
                ),
                array(
                    'rule' => array('isMatchWithPassword'),
                    'message' => 'Password and Confirm Password not Match'
                ),
            ),
        );

        $this->validate = $validateLogin;
        return $this->validates();
    }

    /**
     * @return bool
     *
     * Validating the changing password section.
     */
    public function changePassValidate()
    {
        $validateChangePassword = array(
            'oldPassword' => array(
                'notempty' => array(
                    'rule' => 'notempty',
                    'message' => 'Old Password must be required'
                ),
                array(
                    'rule' => array('matchingCurrentPassword'),
                    'message' => 'Current Password Not Match'
                ),
            ),
            'password' => array(
                'notempty' => array(
                    'rule' => 'notempty',
                    'message' => 'Password must be required'
                ),
                'size' => array(
                    'rule' => array('between', 6, 25),
                    'message' => 'Password should be at least 6 characters long'
                )
            ),
            'cPassword' => array(
                'notempty' => array(
                    'rule' => 'notempty',
                    'message' => 'Confirm password must be required'
                ),
                array(
                    'rule' => array('CheckPass'),
                    'message' => 'Password and Confirm Password not Match'
                ),
                'size' => array(
                    'rule' => array('between', 6, 25),
                    'message' => 'Confirm Password should be at least 6 characters long'
                )
            ),
        );

        $this->validate = $validateChangePassword;
        return $this->validates();
    }

    /**
     * @param $data
     * @return bool
     *
     * This function will matching the given password with user's current password (User Table Password Field)
     */
    public function matchingCurrentPassword($data)
    {
        $oldPassword = AuthComponent::password($data['oldPassword']);
        $userInfo = $this->findById($this->data['User']['id']);
        if ($oldPassword == $userInfo['User']['password']) {
            return true;
        }
        else {
            return false;
        }

    }

    /**
     * Matching the password and confirm password.
     * @return bool
     */
    public function CheckPass()
    {
        return $this->data['User']['password'] === $this->data['User']['cPassword'];
    }

    /**
     * Matching the password and confirm password.
     * @return bool
     */
    public function isMatchWithPassword()
    {
        if($this->data['User']['password']){
            return $this->data['User']['password'] === $this->data['User']['cPassword'];
        }
        return true;
    }

    /**
     * Matching the password and confirm password.
     * @return bool
     */
    public function isPasswordGiven()
    {
        if($this->data['User']['password']){
            if($this->data['User']['cPassword']){
                return true;
            }
            else{
                return false;
            }
        }
        return true;
    }

    /**
     * Matching the password and confirm password.
     * @return bool
     */
    public function isUniqueOnProfileEdit()
    {
        $email = $this->data['User']['username'];
        $userID = $this->data['User']['id'];

        $user = $this->find('first', array('conditions' => array('User.id' => $userID)));
        if($user['User']['username'] == $email){
            return true;
        }
        else{
            $isExiest = $this->find('first', array('conditions' => array('User.username' => $email)));
            if($isExiest){
                return false;
            }
            return true;
        }
    }

    /**
     * @param $userID
     * @param $newPassword
     * @return bool
     *
     * This function will change the user's password.,
     */
    public function changePassword($userID, $newPassword)
    {
        $this->id = $userID;
        if($this->saveField('password', $newPassword)){
            return true;
        }
        return false;
    }

    /**
     * @param $ID
     * @param int $recursive
     * @return array|bool
     *
     * This function will check the  user based on user ID or user UUID.
     */
    public function getUser($ID, $recursive = 1)
    {
        $result = $this->find(
            'first',
            array(
                'conditions' =>
                    array(
                        'OR' => array(
                            'User.id' => $ID,
                            'User.uuid' => $ID
                        )
                    ),
                'recursive' => $recursive,
                'contain' => array('Profile', 'CountLesson', 'Benchmark', 'Appointment', 'PackagesUser', 'Waiting')
            ));
        if($result){
            return $result;
        }
        return false;
    }

    public function getStudentDetails($ID)
    {
        $result = $this->find(
            'first',
            array(
                'conditions' =>
                    array(
                        'OR' => array(
                            'User.id' => $ID,
                            'User.uuid' => $ID
                        )
                    ),
                'recursive' => 1,
                'contain' => array(
                    'Profile',
                    'CountLesson' => array(
                        'fields' => array('purchased_lesson_count', 'appointed_lesson_count', 'lesson_left')
                    ),
                    'Appointment' => array(
                        'fields' => array('id', 'uuid', 'start', 'end', 'package_id', 'lesson_no', 'pay_at_facility'),
                        'order' => 'Appointment.start DESC',
                        'limit' => 5,
                        'Package' => array(
                            'fields' => array('name', 'uuid', 'total')
                        )
                    ),
                    'PackagesUser' => array(
                        'fields' => array('package_id', 'status', 'total_lesson'),
                        'order' => 'PackagesUser.created DESC',
                        'limit' => 5,
                        'Package' => array(
                            'fields' => array('name', 'uuid', 'total')
                        )
                    ),
                    'Waiting' => array(
                        'fields' => array('id', 'uuid', 'start', 'end', 'package_id'),
                        'order' => 'Waiting.start DESC',
                        'limit' => 5,
                        'Package' => array(
                            'fields' => array('name', 'uuid', 'total')
                        )
                    ),
                    'Benchmark' => array(
                        'conditions' => array('Benchmark.created_for ' => $ID),
                        'fields' => array('id', 'uuid',  'package_id', 'lesson_no', 'status', 'appointment_id'),
                        'order' => 'Benchmark.created DESC',
                        'Package' => array(
                            'fields' => array('name', 'uuid', 'total')
                        ),
                        'limit' => 5,
                    ),

                    /*'WaitingRange' => array(
                        'conditions' => array('WaitingRange.user_id ' => $ID),
                        //'fields' => array('id', 'uuid',  'package_id', 'lesson_no', 'status', 'appointment_id'),
                        'order' => 'WaitingRange.created DESC',
                        'limit' => 1,
                    ),*/
                )
            )
        );

        return $result;
    }

    /**
     * @param string $needed
     * @param $role
     * @return array
     *
     * This function will return a list of user based on user role.
     */
    public function getUserByRole($needed = 'all', $role)
    {
        $result = $this->find($needed, array('conditions' => array('User.role' => $role), 'contain' => array('Profile')));
        return $result;
    }


    /**
     * @param $uuid
     * @return null
     */
    public function getUserIdByUuid($uuid)
    {
        $result = $this->find(
            'first',
            array(
                'recursive' => -1,
                'conditions' => array(
                    'User.uuid' => $uuid
                )
            )
        );

        if($result)
        {
            return $result['User']['id'];
        }
        return null;
    }
    public function getUuidByUserId($id)
    {
        $result = $this->find(
            'first',
            array(
                'recursive' => -1,
                'conditions' => array(
                    'User.id' => $id
                )
            )
        );

        if($result)
        {
            return $result['User']['uuid'];
        }
        return null;
    }


    /**
     * @param $uuid
     * @return null
     */
    public function getUserRoleByUuid($uuid)
    {
        $result = $this->find(
            'first',
            array(
                'recursive' => -1,
                'conditions' => array(
                    'User.uuid' => $uuid
                )
            )
        );

        if($result)
        {
            return $result['User']['role'];
        }
        return null;
    }


    /**
     * @param $uuid
     * @return null
     */
    public function getUserBasicUuid($uuid)
    {
        $result = $this->find(
            'first',
            array(
                'recursive' => -1,
                'conditions' => array(
                    'User.uuid' => $uuid
                ),
                'contain' => array('Profile' => array('fields' => array('name'))),
            )
        );

        if($result)
        {
            return $result;
        }
        return null;
    }

    public function getSlippingAway($options = array())
    {
        $limit = false;
        $conditions = array();

        if(isset($options['conditions'])){
            $conditions = array_merge($conditions, $options['conditions']);
        }

        if(isset($options['limit'])){
            $limit = $options['limit'];
        }

        $fields = array(
            'User.uuid',
            'User.username',
            'User.last_login',
            'Profile.first_name',
            'Profile.last_name',
            'InstructorUser.uuid',
            'CONCAT(Instructor.first_name, " ", Instructor.last_name) as instructorName',
        );

        $options = array(
            'conditions' => $conditions,
            'fields' => $fields,
            'recursive' => -1,
            'limit' => $limit,
            'order' => 'User.id DESC',
            'joins' => array(
                array(
                    'table' => 'profiles',
                    'type' => 'LEFT',
                    'alias' => 'Profile',
                    'conditions' => array('Profile.user_id = User.id')
                ),
                array(
                    'table' => 'profiles',
                    'type' => 'LEFT',
                    'alias' => 'Instructor',
                    'conditions' => array('Instructor.user_id = Profile.instructor_id')
                ),
                array(
                    'table' => 'users',
                    'type' => 'LEFT',
                    'alias' => 'InstructorUser',
                    'conditions' => array('InstructorUser.id = Profile.user_id'),
                    'fields' => array('uuid')
                ),

            )
        );

        $result = $this->find('all', $options);
        return $result;
    }

    public function getUserByEMail($email)
    {
        $result = $this->find(
            'first',
            array(
                'conditions' =>
                    array(
                        'User.username' => $email
                    ),
                'recursive' => -1
            )
        );

        if($result){
            return (int) $result['User']['id'];
        }

        return false;
    }

    public function getSourcesForPie($options = array())
    {
        $conditions = array();
        if(isset($options['conditions'])){
            $conditions = array_merge($conditions, $options['conditions']);
        }

        foreach($options['sources'] as $key => $value){
            $fields[] = 'SUM(CASE WHEN Profile.source = "'.$value.'" THEN 1 ELSE 0 END) AS "'.$value.'"';
        }
        //array_push($fields, 'COUNT(Profile.id) AS totalSource');
        array_push($fields, 'SUM(CASE WHEN Profile.is_custom_source = 1 THEN 1 ELSE 0 END) AS customSource');

        $options = array(
            'conditions' => $conditions,
            'fields' => $fields,
            'recursive' => -1,
            'joins' => array(
                array(
                    'table' => 'profiles',
                    'type' => 'LEFT',
                    'alias' => 'Profile',
                    'conditions' => array('Profile.user_id = User.id')
                )
            )
        );

        $result = $this->find('all', $options);
        return $result;
    }

    public function changeSlippingAway($userID, $value){
        $this->id = $userID;
        if($this->saveField('is_slipping_away', $value)){
            return true;
        }
        return false;
    }

    public function getUserOverview($options = array())
    {
        $conditions = array();
        if(isset($options['conditions'])){
            $conditions = array_merge($conditions, $options['conditions']);
        }

        $fields = array(
            'SUM(CASE WHEN User.is_slipping_away = 1 THEN 1 ELSE 0 END) AS slippingAway',
            'SUM(CASE WHEN User.last_login IS NULL THEN 1 ELSE 0 END) AS neverLoggedIn',
            'SUM(CASE WHEN CountLesson.lesson_left IS NULL THEN 1 ELSE 0 END) AS neverLessonPurchased',
            'SUM(CASE WHEN CountLesson.purchased_lesson_count = CountLesson.lesson_left THEN 1 ELSE 0 END) AS neverSchedule',
            'SUM(CASE WHEN CountLesson.lesson_left > 0 THEN 1 ELSE 0 END) AS lessonLeft',
            'SUM(CASE WHEN CountLesson.lesson_left = 0 OR CountLesson.lesson_left IS NULL THEN 1 ELSE 0 END) AS noLessonLeft',
            'COUNT(User.id) as totalUser',
            'SUM(CASE WHEN User.rating = 0 THEN 1 ELSE 0 END) AS noRatingLead',
            'SUM(CASE WHEN User.rating = 1 THEN 1 ELSE 0 END) AS oneStarRatingLead',
            'SUM(CASE WHEN User.rating = 2 THEN 1 ELSE 0 END) AS twoStarRatingLead',
            'SUM(CASE WHEN User.rating = 3 THEN 1 ELSE 0 END) AS threeStarRatingLead',
            'SUM(CASE WHEN User.rating = 4 THEN 1 ELSE 0 END) AS fourStarRatingLead',
            'SUM(CASE WHEN User.rating = 5 THEN 1 ELSE 0 END) AS fiveStarRatingLead',
        );

        $options = array(
            'conditions' => $conditions,
            'fields' => $fields,
            'recursive' => -1,
            'joins' => array(
                array(
                    'table' => 'profiles',
                    'type' => 'LEFT',
                    'alias' => 'Profile',
                    'conditions' => array('Profile.user_id = User.id')
                ),
                array(
                    'table' => 'count_lessons',
                    'type' => 'LEFT',
                    'alias' => 'CountLesson',
                    'conditions' => array('CountLesson.user_id = User.id')
                )
            ),
        );
        $result = $this->find('all', $options);
        return $result;
    }


    /**
     * Validation set for the user login.
     * @return bool
     */
    public function importFormValidation()
    {
        $validateLogin = array(
            'file' => array(
                'isFile' => array(
                    'rule' => array('isFile'),
                    'message' => 'Please provide an csv file',
                ),
                'size' => array(
                    'rule' => array('size', 24, 10240000),
                    'message' => 'file size should be between 1KB - 10MB'
                ),
                'Extension' => array(
                    'rule' => array(
                        'extension',
                        array('csv')
                    ),
                    'message' => "Please supply a valid csv format file"
                ),
            )
        );

        $this->validate = $validateLogin;
        return $this->validates();
    }

    /**
     * @param $data
     * @return bool
     *
     * Checking the given document is image or not.
     */
    public function isFile($data) {
        if(!empty($data['file']['name']))
        {
            return true;
        }
        return false;
    }

    /**
     * @param $data
     * @param $min
     * @param $max
     * @return bool
     *
     * Checking the size of given document.
     */
    public function size($data, $min, $max) {
        $size =$data['file']['size'] ;
        if($size > $min && $size < $max)
        {
            return true;
        }
        return false;
    }

    public function checkExiestByEmails($emails)
    {
        $results = $this->find(
            'all',
            array(
                'recursive' => -1,
                'fields' => array('User.username'),
                'conditions' => array('User.username' => $emails)
            )
        );

        foreach($results as $user)
        {
            $users[] = $user['User']['username'];
        }

        return $users;
    }
    public function getStudentForTC($tcID){
        $result = $this->find('all', array(
                'fields' => array(
                    'TcsUser.user_id',
                    'CONCAT(Profile.first_name, " ", Profile.last_name) as name'
                ),
                'conditions' => array('TcsUser.tc_id' => $tcID),
                'joins' => array(
                    array(
                        'table' => 'profiles',
                        'type' => 'LEFT',
                        'alias' => 'Profile',
                        'conditions' => array('Profile.user_id = TcsUser.user_id')
                    ),
                ),
                'order' => 'TcsUser.user_id ASC'
            )
        );
        // var_dump($result); die();


        if($result){
            $ids = array();
            foreach($result as $id){
                $ids[$id['TcsUser']['user_id']] = $id[0]['name'];
            }
            return $ids;
        }
        return false;

    }

}
