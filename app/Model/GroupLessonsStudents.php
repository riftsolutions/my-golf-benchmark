<?php
App::uses('AppModel', 'Model');
class GroupLessonsStudents extends AppModel
{
    /**
     * @var string
     *
     * Name of this Billing Model
     */
    public $name = 'GroupLessonsStudents';

    public $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id'
        ),
        'GroupLesson' => array(
            'className' => 'GroupLesson',
            'foreignKey' => 'group_lesson_id'
        )
    );

    public function checkStudentIDInGroupLesson($groupID, $userID)
    {
        $result = $this->find('first', array('conditions' => array('GroupLessonsStudents.group_lesson_id' => $groupID, 'GroupLessonsStudents.user_id' => $userID)));

        if($result){
            return true;
        }
        return false;
    }

    public function getGroupLessonStudentID($groupID, $userID)
    {
        $result = $this->find('first', array('conditions' => array('GroupLessonsStudents.group_lesson_id' => $groupID, 'GroupLessonsStudents.user_id' => $userID)));

        if($result){
            return (int) $result['GroupLessonsStudents']['id'];
        }
        return false;
    }
}
