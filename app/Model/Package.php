<?php
App::uses('AppModel', 'Model');
class Package extends AppModel
{
    /**
     * @var string
     *
     * Name of This Package Model
     */
    public $name = 'Package';

    public $hasAndBelongsToMany = array(
        'User' => array(
            'className' => 'User',
            'joinTable' => 'packages_users',
            'foreignKey' => 'package_id',
            'associationForeignKey' => 'created_for'
        ),
    );

    public $hasMany = array(
        'PackagesOrder' => array(
            'className' => 'PackagesOrder',
            'foreignKey' => 'package_id'
        ),
    );


    public $validate = array(

        'name' => array(
            'rule1' => array(
                'rule' => array('notEmpty'),
                'message' => 'Name must be required',
            )
        ),

        'lesson' => array(
            'rule1' => array(
                'rule' => array('notEmpty'),
                'message' => 'Lesson number must be required',
            ),
            'rule2' => array(
                'rule' => array('numeric'),
                'message' => 'Invalid number, please supply valid number of lesson',
            )
        ),

        'description' => array(
            'rule1' => array(
                'rule' => array('notEmpty'),
                'message' => 'Description must be required',
            )
        ),

        'price' => array(
            'rule1' => array(
                'rule' => array('notEmpty'),
                'message' => 'Price must be required',
            ),

            'salary' => array(
                'rule'    => array('money', 'left'),
                'message' => 'Please supply a valid monetary amount.'
            )
        ),

        'tax' => array(
            'rule1' => array(
                'rule' => array('notEmpty'),
                'message' => 'Price must be required',
                'allowEmpty' => true
            ),

            'salary' => array(
                'rule'    => array('money', 'left'),
                'message' => 'Please supply a valid monetary amount.'
            )
        ),

        'lesson_id' => array(
            array(
                'rule' => array('checkLessonIDs'),
                'message' => 'Please select the lesson'
            )
        ),

        'expiration_days' => array(
            'rule1' => array(
                'rule' => array('notEmpty'),
                'message' => 'Expiration day must be required',
            ),
        ),
    );

    /**
     * @return bool
     *
     */
    public function checkLessonIDs()
    {
        $lesson = $this->data['Package']['lesson_id'];
        if(is_array($lesson)){
            return true;
        }

        return false;
    }

    /**
     * @param $userID
     * @return array
     *
     */
    public function getPackageList($userID)
    {
        $result = $this->find('list',
            array(
                'conditions' =>
                    array(
                        'Package.status' => 1,
                        'Package.pay_at_facility' => 0,
                        'OR' => array(
                            'Package.created_by' => $userID,
                            'Package.is_default' => 1,
                        )
                    ),
                'fields' => array('Package.id', 'Package.name')
            )
        );

        return $result;
    }


    /**
     * @return bool
     */
    public function assignPackageToStudentValidation()
    {
        $validation = array(
            'created_for' => array(
                'rule1' => array(
                    'rule' => array('notEmpty'),
                    'message' => 'Please select the student',
                )
            ),

            'package_id' => array(
                'rule1' => array(
                    'rule' => array('notEmpty'),
                    'message' => 'Please select the package',
                )
            ),
        );

        $this->validate = $validation;
        return $this->validates();
    }

    /**
     * @param $name
     * @return array
     */
    public function getPackageIDByName($name)
    {
        $result = $this->find(
            'first',
            array(
                'recursive' => -1,
                'conditions' => array(
                    'Package.name' => $name
                )
            )
        );

        if($result)
        {
            return (int) $result['Package']['id'];
        }
        return null;
    }

    /**
     * @param $uuid
     * @return array
     */
    public function getPackageByUuid($uuid)
    {
        $result = $this->find(
            'first',
            array(
                'recursive' => -1,
                'conditions' => array(
                    'Package.uuid' => $uuid
                )
            )
        );

        return $result;
    }

    public function getPackageByID($ID)
    {
        $result = $this->find(
            'first',
            array(
                'recursive' => -1,
                'conditions' => array(
                    'Package.id' => $ID
                ),
                'fields' => array(
                    'Package.*',
                    'Profile.first_name',
                    'Profile.last_name',
                    'User.uuid',
                ),
                'joins' => array(
                    array(
                        'table' => 'profiles',
                        'type' => 'LEFT',
                        'alias' => 'Profile',
                        'conditions' => array('Profile.user_id = Package.created_by')
                    ),
                    array(
                        'table' => 'users',
                        'type' => 'LEFT',
                        'alias' => 'User',
                        'conditions' => array('User.id = Package.created_by')
                    ),
                ),
            )
        );

        return $result;
    }

    public function getPackages($IDs)
    {
        $result = $this->find(
            'all',
            array(
                'recursive' => -1,
                'conditions' => array(
                    'Package.id' => $IDs
                ),
                'fields' => array(
                    'Package.*',
                    'Profile.first_name',
                    'Profile.last_name',
                    'User.id',
                    'User.uuid',
                ),
                'joins' => array(
                    array(
                        'table' => 'profiles',
                        'type' => 'LEFT',
                        'alias' => 'Profile',
                        'conditions' => array('Profile.user_id = Package.created_by')
                    ),
                    array(
                        'table' => 'users',
                        'type' => 'LEFT',
                        'alias' => 'User',
                        'conditions' => array('User.id = Package.created_by')
                    ),
                ),
            )
        );

        return $result;
    }

    /**
     * @param $uuid
     * @return null
     */
    public function getPackageIDByUuid($uuid)
    {
        $result = $this->find(
            'first',
            array(
                'recursive' => -1,
                'conditions' => array(
                    'Package.uuid' => $uuid
                )
            )
        );

        if($result)
        {
            return $result['Package']['id'];
        }
        return null;
    }

    /**
     * @param $id
     * @return null
     */
    public function getPackageLesson($id)
    {
        $result = $this->find(
            'first',
            array(
                'recursive' => -1,
                'conditions' => array(
                    'Package.id' => $id
                ),
            )
        );

        if($result)
        {
            return $result['Package']['lesson'];
        }
        return null;
    }

    /**
     * @param array $options
     * @return array
     */

    public function getPackageOverview($userID, $options = array())
    {
        $conditions = array('Package.is_default');

        if(isset($userID)){
            $conditions =
                array(
                    'Package.pay_at_facility' => 0,
                    'OR' => array(
                        'Package.is_default',
                        'Package.created_by' => $userID
                    )
                )
            ;
        }

        if(isset($options['conditions'])){
            $conditions = array_merge($conditions, $options['conditions']);
        }

        $fields = array(
            'SUM(CASE WHEN Package.is_default = 1 THEN 1 ELSE 0 END) AS defaultPackage',
            'SUM(CASE WHEN Package.is_default = 0 THEN 1 ELSE 0 END) AS customPackage',
            'SUM(CASE WHEN Package.status = 1 THEN 1 ELSE 0 END) AS visiblePackage',
            'SUM(CASE WHEN Package.status = 0 THEN 1 ELSE 0 END) AS invisiblePackage',
            'COUNT(Package.id) as totalPackage'
        );

        $options = array(
            'conditions' => $conditions,
            'fields' => $fields,
            'recursive' => -1,
            'joins' => array(
                array(
                    'table' => 'packages_users',
                    'type' => 'LEFT',
                    'alias' => 'PackagesUser',
                    'conditions' => array('Package.id = PackagesUser.package_id')
                ),
            ),
        );

        $result = $this->find('all', $options);
        return $result;
    }


    /**
     * @param $studentID
     * @return array
     */
    public function getPayAtFacility($studentID)
    {


        $result = $this->find('list', array('conditions' => array('Package.pay_at_facility' => 1)));
        return $result;
    }


    /**
     * @param $packageID
     * @return array
     */
    public function isPayAtPackage($packageID)
    {


        $result = $this->find(
            'first',
            array(
                'recursive' => -1,
                'fields' => array('Package.pay_at_facility'),
                'conditions' =>
                    array(
                        'Package.id' => $packageID,
                        'Package.pay_at_facility' => 1
                    )
            )
        );

        if($result){
            return true;
        }
        return false;
    }

}
