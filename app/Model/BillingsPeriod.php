<?php

/**
 * Created by PhpStorm.
 * User: root
 * Date: 12/8/15
 * Time: 1:50 AM
 */
class BillingsPeriod extends AppModel
{
    public $name = 'BillingsPeriod';

    public function addBillingPeriod($userID, $amount, $date = null)
    {
        $paymentDate = date('Y-m-d H:i:s');
        if ($date) {
            $paymentDate = date('Y-m-d H:i:s', strtotime($date));
        }
        $notifyingDate = date('Y-m-d H:i:s', strtotime('-1 mins', strtotime($paymentDate)));

        $date = array(
            'user_id' => $userID,
            'amount' => (float)$amount,
            'payment_date' => $paymentDate,
            'notifying_date' => $notifyingDate,
        );

        $this->create();
        if ($this->save($date)) {
            return true;
        }
        return false;
    }

    public function getNotifyUsers()
    {
        $now = date('Y-m-d H:i:s');
        $around = date('Y-m-d H:i:s', strtotime('+10 min', strtotime($now)));

        $users = $this->find('all', array(
            'fields' => array(
                'BillingsPeriod.id',
                'BillingsPeriod.amount',
                'BillingsPeriod.payment_date',
                'BillingsPeriod.notifying_date',
                'User.id',
                'User.username',
                'CONCAT(Profile.first_name, " ", Profile.last_name) as name'
            ),
            'conditions' => array(
                'BillingsPeriod.notifying_date >=' => $now,
                'BillingsPeriod.notifying_date <=' => $around,
                'BillingsPeriod.get_notified' => 0,
                'BillingsPeriod.status' => 1,
                'BillingsPeriod.payment_status' => 0,
            ),
            'joins' => array(
                array(
                    'table' => 'users',
                    'type' => 'LEFT',
                    'alias' => 'User',
                    'conditions' => array('User.id = BillingsPeriod.user_id')
                ),
                array(
                    'table' => 'profiles',
                    'type' => 'LEFT',
                    'alias' => 'Profile',
                    'conditions' => array('Profile.user_id = BillingsPeriod.user_id')
                ),
            ),
        ));

        return $users;
    }

    public function getPaymentUsers()
    {
        $now = date('Y-m-d H:i:s');
        $around = date('Y-m-d H:i:s', strtotime('+10 min', strtotime($now)));

        $users = $this->find('all', array(
            'fields' => array(
                'BillingsPeriod.id',
                'BillingsPeriod.amount',
                'BillingsPeriod.payment_date',
                'BillingsPeriod.notifying_date',
                'User.id',
                'User.username',
                'CONCAT(Profile.first_name, " ", Profile.last_name) as name'
            ),
            'conditions' => array(
                'BillingsPeriod.payment_date >=' => $now,
                'BillingsPeriod.payment_date <=' => $around,
                'BillingsPeriod.get_notified' => 1,
                'BillingsPeriod.payment_status' => 8,
            ),
            'joins' => array(
                array(
                    'table' => 'users',
                    'type' => 'LEFT',
                    'alias' => 'User',
                    'conditions' => array('User.id = BillingsPeriod.user_id')
                ),
                array(
                    'table' => 'profiles',
                    'type' => 'LEFT',
                    'alias' => 'Profile',
                    'conditions' => array('Profile.user_id = BillingsPeriod.user_id')
                ),
            ),
        ));

        return $users;
    }

    public function makeNotified($billingPeriodID)
    {
        $this->id = $billingPeriodID;
        return $this->saveField('get_notified', 1);
    }

    public function changePaymentStatus($billingPeriodID, $status = 9)
    {
        $this->id = $billingPeriodID;
        return $this->saveField('payment_status', $status);
    }

    public function changeStatus($billingPeriodID, $status = 2)
    {
        $this->id = $billingPeriodID;
        return $this->saveField('status', $status);
    }


}