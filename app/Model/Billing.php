<?php
App::uses('AppModel', 'Model');
class Billing extends AppModel
{
    /**
     * @var string
     *
     * Name of this Billing Model
     */
    public $name = 'Billing';

    public function geBilling($options = array())
    {
        $conditions = array();
        $queryType = 'all';
        $limit = 5;

        if(isset($options['conditions'])){
            $conditions = array_merge($conditions, $options['conditions']);
        }


        if(isset($options['query'])){
            $queryType = $options['query'];
        }

        if(isset($options['limit'])){
            $limit = $options['limit'];
        }

        $fields = array(
            'Billing.uuid',
            'Billing.amount',
            'Billing.user_id',
/*            'Billing.packages_users_id',*/
            'Billing.payment_account_number',
            'Billing.created',
            'Profile.first_name',
            'Profile.last_name',
            'User.uuid',
            'CONCAT(Instructor.first_name, " ", Instructor.last_name) as instructorName',
            'InstructorInfo.uuid'

        );

        $options = array(
            'conditions' => $conditions,
            'fields' => $fields,
            'recursive' => -1,
            'limit' => $limit,
            'order' => 'Billing.created DESC',
            'joins' => array(
                array(
                    'table' => 'profiles',
                    'type' => 'LEFT',
                    'alias' => 'Profile',
                    'conditions' => array('Profile.user_id = Billing.user_id')
                ),
                array(
                    'table' => 'users',
                    'type' => 'LEFT',
                    'alias' => 'User',
                    'conditions' => array('User.id = Billing.user_id')
                ),
                array(
                    'table' => 'billings_packages',
                    'type' => 'LEFT',
                    'alias' => 'BillingsPackage',
                    'conditions' => array('BillingsPackage.id = Billing.id')
                ),
                array(
                    'table' => 'profiles',
                    'type' => 'LEFT',
                    'alias' => 'Instructor',
                    'conditions' => array('Instructor.user_id = Profile.instructor_id')
                ),
                array(
                    'table' => 'users',
                    'type' => 'LEFT',
                    'alias' => 'InstructorInfo',
                    'conditions' => array('InstructorInfo.id = Profile.instructor_id'),
                    'fields' => array('uuid')
                ),

            )
        );

        $result = $this->find($queryType, $options);
        return $result;
    }

    public function getBillingIDByUuid($uuid)
    {
        $result = $this->find(
            'first',
            array(
                'recursive' => -1,
                'conditions' => array(
                    'Billing.uuid' => $uuid
                )
            )
        );

        if($result)
        {
            return $result['Billing']['id'];
        }
        return null;
    }

    public function getUserIDByBillingID($billingID)
    {
        $result = $this->find(
            'first',
            array(
                'recursive' => -1,
                'conditions' => array(
                    'Billing.id' => $billingID
                )
            )
        );

        if($result)
        {
            return $result['Billing']['user_id'];
        }
        return null;
    }

    public function getBillingByID()
    {
        $result = $this->find(
            'first',
            array(
                'recursive' => -1,
                'order' => 'Billing.id DESC'
            )
        );

        if($result){
            return (int) $result['Billing']['id'];
        }
        return 1;
    }

    public function getBillingByTransactionID($ID)
    {
        $result = $this->find(
            'first',
            array(
                'recursive' => -1,
                'conditions' => array(
                    'Billing.payment_transaction_id' => $ID
                ),
                'fields' => array(
                    'Billing.uuid',
                    'Billing.tax',
                    'Billing.subtotal',
                    'Billing.amount',
                    'Billing.user_id',
                    'Billing.card_id',
                    'Billing.payment_transaction_id',
                    'payment_transaction_type',
                    'Billing.payment_account_number',
                    'Billing.payment_final_status',
                    'Billing.created',
                    'CONCAT(Profile.first_name, " ", Profile.last_name) as studentName',
                    'User.uuid',
                    'User.username',
                ),
                'joins' => array(
                    array(
                        'table' => 'profiles',
                        'type' => 'LEFT',
                        'alias' => 'Profile',
                        'conditions' => array('Profile.user_id = Billing.user_id')
                    ),
                    array(
                        'table' => 'users',
                        'type' => 'LEFT',
                        'alias' => 'User',
                        'conditions' => array('User.id = Billing.user_id')
                    )
                )
            )
        );

        return $result;
    }

    public function getLastBillingID($ID)
    {
        $result = $this->find(
            'first',
            array(
                'recursive' => -1,
                'conditions' => array(
                    'Billing.payment_transaction_id' => $ID
                ),
                'fields' => array(
                    'Billing.uuid',
                    'Billing.tax',
                    'Billing.subtotal',
                    'Billing.amount',
                    'Billing.user_id',
                    'Billing.card_id',
                    'Billing.payment_transaction_id',
                    'payment_transaction_type',
                    'Billing.payment_account_number',
                    'Billing.payment_final_status',
                    'Billing.created',
                    'CONCAT(Profile.first_name, " ", Profile.last_name) as studentName',
                    'User.uuid',
                    'User.username',
                ),
                'joins' => array(
                    array(
                        'table' => 'profiles',
                        'type' => 'LEFT',
                        'alias' => 'Profile',
                        'conditions' => array('Profile.user_id = Billing.user_id')
                    ),
                    array(
                        'table' => 'users',
                        'type' => 'LEFT',
                        'alias' => 'User',
                        'conditions' => array('User.id = Billing.user_id')
                    )
                )
            )
        );

        return $result;
    }

    public function getAmountByUserID($userID)
    {
        $result = $this->find(
            'all',
            array(
                'conditions' => array(
                    'Billing.user_id' => $userID
                ),
                'recursive' => -1,
                'fields' => array(
                    'Billing.id',
                    'SUM(Billing.amount) as amount',
                ),
            )
        );

        if($result[0]){
            return (int) $result[0][0]['amount'];
        }
        return 0;
    }
}
