<?php
App::uses('AppModel', 'Model');

/**
 * Card Model
 */
class Card extends AppModel
{

    //The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * belongsTo associations
     *
     * @var array
     */
    public $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );


    public function cardValidation()
    {
        $cardValidate = array(
            'cardID' => array(
                array(
                    'rule' => array('isSelectCard'),
                    'message' => 'Please select the card',
                )
            ),
            'cardName' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'required' => false,
                    'message' => 'Card name must be required',
                ),
            ),

            'cardNumber' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'required' => false,
                    'message' => 'Card number must be required',
                ),
                'cardNumber' => array(
                    'rule' => array('cc', 'all', false, null),
                    'message' => 'Card number invalid.'
                ),
            ),

            'ExpireYear' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'required' => false,
                    'message' => 'Expire year must be required',
                )
            ),

            'ExpireMonth' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'required' => false,
                    'message' => 'Expire month must be required',
                )
            ),

            'Expiry' => array(
                array(
                    'rule' => array('getTimeDiff'),
                    'message' => 'Expiry date invalid!',
                    'allowEmpty' => true
                )
            ),
            'billing_street_1' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => 'Billing street 1 must be required!',
                ),
            ),

            'billing_street_2' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => 'Billing street 2 must be required!',
                ),
            ),

            'billing_city' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => 'Billing city must be required!',
                ),
            ),

            'billing_postal_code' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => 'Billing postal code must be required!',
                ),
                'zipcode' => array(
                    'rule' => array('postal', null, 'us'),
                    'message' => 'Invalid Zip Code! zip code must be in us standard'
                )
            ),

            'billing_state' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => 'Billing state must be required!',
                ),
            ),

            'billing_country' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => 'Billing country must be required!',
                ),
            ),
        );
        $this->validate = $cardValidate;
        return $this->validates();
    }

    public function getTimeDiff(){
        $datetime1 = strtotime(date('Y-m-d H:i:s'));
        $datetime2 = strtotime($this->data['Card']['Expiry']);
        if($datetime2 > $datetime1){
            return true;
        }
        return false;
    }

    public function isSelectCard(){
        if($this->data['Card']['ExpireMonth'] || $this->data['Card']['cardNumber'] || $this->data['Card']['ExpireYear']){
            return true;
        }
        elseif($this->data['Card']['cardID'] == 'Select Card'){
            return false;
        }
        return true;
    }

    public function getCardIDByUuid($uuid){
        $card = $this->find('first', array('controller' => array('Card.uuid' => $uuid)));
        return $card['Card']['id'];
    }

    public function getCardList($userID)
    {
        $result = $this->find(
            'all',
            array('conditions' =>
                array(
                'Card.user_id' => $userID
                )
            )
        );

        return $result;
    }

    public function getCard($cardID, $userID)
    {
        return $this->find('first', array('conditions' => array('Card.id' => $cardID, 'Card.user_id' => $userID)));
    }

    public function getCardByID($cardID)
    {
        return $this->find('first', array('conditions' => array('Card.id' => $cardID), 'recursive' => -1));
    }

    public function getCardByUserID($userID)
    {
        return $this->find('list', array('conditions' => array('Card.access_to_use_instructor' => 1, 'Card.user_id' => $userID)));
    }
}