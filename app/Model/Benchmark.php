<?php
App::uses('AppModel', 'Model');
class Benchmark extends AppModel
{
    /**
     * @var string
     *
     * Name of this Benchmark Model
     */
    public $name = 'Benchmark';

    public $belongsTo = array(
        'Package' => array(
            'className' => 'Package',
            'foreignKey' => 'package_id',
        ),
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'created_for',
        )
    );

    /**
     * @param $uuid
     * @return array
     */
    public function getBenchmarkByUuid($uuid)
    {
        return $this->find('first', array('conditions' => array('Benchmark.uuid' => $uuid), 'recursive' => -1));
    }

    /**
     * @param $uuid
     * @return array
     */
    public function countBenchmark($userID)
    {
        return $this->find('count', array('conditions' => array('Benchmark.created_for' => $userID)));
    }


    /**
     * @param string $needed
     * @param array $options
     * @return array
     *
     * Getting benchmark based on several conditions
     */
    public function getBenchmark($needed = 'all', $options = array())
    {
        $limit = 10;
        $fields = array();
        $orderBy = 'DESC';
        $recursive = 1;
        $queryConditions = array();

        if(isset($options['created_for'])){
            $queryConditions = array_merge($queryConditions, array('Benchmark.created_for' => $options['created_for']));
        }

        if(isset($options['created_by'])){
            $queryConditions = array_merge($queryConditions, array('Benchmark.created_by' => $options['created_by']));
        }

        if(isset($options['limit'])){
            $limit = $options['limit'];
        }

        if(isset($options['recursive'])){
            $recursive = $options['recursive'];
        }

        if(isset($options['order'])){
            $orderBy = $options['order'];
        }

        if(isset($options['conditions'])){
            if(is_array($options['conditions'])){
                $queryConditions = array_merge($queryConditions, $options['conditions']);
            }
        }

        $result = $this->find(
            $needed,
            array(
                'conditions' => $queryConditions,
                'limit' => $limit,
                'fields' => $fields,
                'recursive' => $recursive,
                'order' => 'Benchmark.id '. $orderBy,
            )
        );

        if($result){
            return $result;
        }
        return $result;
    }

    /**
     * @param $options
     * @return array
     *
     */
    public function getBenchmarks($options)
    {
        $conditions = array();
        $queryType = 'all';

        if(isset($options['conditions'])){
            $conditions = array_merge($conditions, $options['conditions']);
        }

        if(isset($options['query'])){
            $queryType = $options['query'];
        }

        $fields = array(

        );

        $options = array(
            'conditions' => $conditions,
            'fields' => $fields,
            'recursive' => -1
        );

        $result = $this->find($queryType, $options);
        return $result;
    }

    public function geBenchmark($options = array())
    {
        $conditions = array();
        $queryType = 'all';
        $limit = 5;

        if(isset($options['conditions'])){
            $conditions = array_merge($conditions, $options['conditions']);
        }

        if(isset($options['query'])){
            $queryType = $options['query'];
        }

        if(isset($options['limit'])){
            $limit = $options['limit'];
        }

        $fields = array(
            'Benchmark.uuid',
            'Benchmark.package_id',
            'Benchmark.created_by',
            'Benchmark.created',
            'CONCAT(Profile.first_name, " ", Profile.last_name) as instructorName',
            'Package.name',
            'User.uuid',
            'Package.uuid',
            'Package.price',
            'Package.tax',
            'Package.total',
        );

        $options = array(
            'conditions' => $conditions,
            'fields' => $fields,
            'recursive' => -1,
            'limit' => $limit,
            'order' => 'Benchmark.created DESC',
            'joins' => array(
                array(
                    'table' => 'profiles',
                    'type' => 'LEFT',
                    'alias' => 'Profile',
                    'conditions' => array('Profile.user_id = Benchmark.created_by')
                ),
                array(
                    'table' => 'users',
                    'type' => 'LEFT',
                    'alias' => 'User',
                    'conditions' => array('User.id = Benchmark.created_by')
                ),
                array(
                    'table' => 'packages',
                    'type' => 'LEFT',
                    'alias' => 'Package',
                    'conditions' => array('Package.id = Benchmark.package_id')
                ),
            )
        );

        $result = $this->find($queryType, $options);
        return $result;
    }


    public function geBenchmarkByID($id)
    {
        $conditions = array('Benchmark.id' => $id);
        $fields = array(
            'Benchmark.uuid',
            'Benchmark.lesson_no',
            'Benchmark.package_id',
            'Benchmark.created_by',
            'Benchmark.created_for',
            'Benchmark.created',
            'CONCAT(Profile.first_name, " ", Profile.last_name) as studentName',
            'User.uuid',
            'User.username',
            'CONCAT(Instructor.first_name, " ", Instructor.last_name) as instructorName',
            'InstructorUser.uuid',
            'InstructorUser.username',
            'Package.name',
            'Package.uuid',
            'Package.price',
            'Package.tax',
            'Package.total',
        );

        $options = array(
            'conditions' => $conditions,
            'fields' => $fields,
            'recursive' => -1,
            'joins' => array(
                array(
                    'table' => 'profiles',
                    'type' => 'LEFT',
                    'alias' => 'Profile',
                    'conditions' => array('Profile.user_id = Benchmark.created_for')
                ),
                array(
                    'table' => 'users',
                    'type' => 'LEFT',
                    'alias' => 'User',
                    'conditions' => array('User.id = Benchmark.created_for')
                ),
                array(
                    'table' => 'profiles',
                    'type' => 'LEFT',
                    'alias' => 'Instructor',
                    'conditions' => array('Instructor.user_id = Benchmark.created_by')
                ),
                array(
                    'table' => 'users',
                    'type' => 'LEFT',
                    'alias' => 'InstructorUser',
                    'conditions' => array('InstructorUser.id = Benchmark.created_by')
                ),
                array(
                    'table' => 'packages',
                    'type' => 'LEFT',
                    'alias' => 'Package',
                    'conditions' => array('Package.id = Benchmark.package_id')
                ),
            )
        );

        $result = $this->find('first', $options);
        return $result;
    }

    public function countBenchmarkByUserID($userID)
    {
        $result = $this->find(
            'count',
            array(
                'conditions' => array(
                    'Benchmark.created_for' => $userID
                ),
                'recursive' => -1,
            )
        );

        if($result){
            return $result;
        }
        return 0;
    }
}
