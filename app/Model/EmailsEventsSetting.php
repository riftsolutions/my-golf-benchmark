<?php
App::uses('AppModel', 'Model');
class EmailsEventsSetting extends AppModel
{
    /**
     * @var string
     *
     * Name of this Billing Model
     */
    public $name = 'EmailsEventsSetting';

    public function getEmailSettingByUserID($userID, $enable = false)
    {
        $conditions = array('EmailsEventsSetting.user_id' => $userID);
        if($enable == true){
            $conditions = array('EmailsEventsSetting.user_id' => $userID, 'EmailsEventsSetting.enable_for_student' => true);
        }

        $result = $this->find(
            'all', array(
                'fields' => array('EmailsEvent.*', 'EmailsEventsSetting.*'),
                'conditions' => $conditions,
                'joins' => array(
                    array(
                        'table' => 'emails_events',
                        'type' => 'LEFT',
                        'alias' => 'EmailsEvent',
                        'conditions' => array('EmailsEvent.id = EmailsEventsSetting.emails_event_id')
                    )
                )
            )
        );
        if ($result) {
            return $result;
        }
        return null;
    }

    public function getEmailSettingByID($id)
    {
        $conditions = array('EmailsEventsSetting.id' => $id);
        $result = $this->find(
            'first',
            array(
                'fields' => array('EmailsEvent.*', 'EmailsEventsSetting.*'),
                'conditions' => $conditions,
                'joins' => array(
                    array(
                        'table' => 'emails_events',
                        'type' => 'LEFT',
                        'alias' => 'EmailsEvent',
                        'conditions' => array('EmailsEvent.id = EmailsEventsSetting.emails_event_id')
                    )
                )
            )
        );
        if ($result) {
            return $result;
        }
        return null;
    }

    public function getEmailsByCheckingNotificationSettings($notification, $userIds)
    {
        $emails = $this->find(
            'list',
            array(
                'fields' => array('User.username'),
                'conditions' => array(
                    'EmailsEvent.field' => $notification,
                    'EmailsEventsSetting.user_id' => $userIds,
                    'EmailsEventsSetting.value' => true,
                ),
                'joins' => array(
                    array(
                        'table' => 'users',
                        'type' => 'LEFT',
                        'alias' => 'User',
                        'conditions' => array('User.id = EmailsEventsSetting.user_id')
                    ),
                    array(
                        'table' => 'emails_events',
                        'type' => 'LEFT',
                        'alias' => 'EmailsEvent',
                        'conditions' => array('EmailsEvent.id = EmailsEventsSetting.emails_event_id')
                    )
                )
            )
        );

        if ($emails) {
            return array_values($emails);
        }
        return null;
    }
}
