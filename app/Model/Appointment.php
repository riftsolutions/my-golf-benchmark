<?php
App::uses('AppModel', 'Model');

class Appointment extends AppModel
{
    /**
     * @var string
     *
     * Name of this Billing Model
     */
    public $name = 'Appointment';

    public $belongsTo = array(
        'Profile' => array(
            'className' => 'Profile',
            'foreignKey' => 'appointment_by'
        ),
        'Instructor' => array(
            'className' => 'Profile',
            'foreignKey' => 'instructor_id'
        ),
        'Package' => array(
            'className' => 'Package',
            'foreignKey' => 'package_id'
        )
    );

    /**
     * @param $instructorID
     * @param $start
     * @param $end
     * @return array
     *
     * Getting appointment of instructor.
     */
    public function getAppointmentOfInstructor($instructorID, $start, $end)
    {
        $results = $this->find(
            'all',
            array(
                'conditions' =>
                    array(
                        'Appointment.instructor_id' => $instructorID,
                        'Appointment.start >=' => $start,
                        'Appointment.end <=' => $end
                    ),
                'fields' => array(
                    'Appointment.id',
                    'Appointment.title',
                    'Appointment.start',
                    'Appointment.end',
                    'Appointment.lesson_no',
                    'Appointment.lesson_sum',
                    'Appointment.status',
                    'Appointment.is_reschedule',
                    'Appointment.is_reassigned',
                    'Appointment.pay_at_facility',
                    'Appointment.next_lesson_start',
                    'Appointment.next_lesson_end',
                    'Appointment.just_booked',
                    'Appointment.note',
                    'Package.name',
                    'Package.id',
                    'Package.total',
                    'Profile.user_id',
                    'Profile.first_name',
                    'Profile.last_name',
                    'CONCAT(Profile.first_name, " ", Profile.last_name) as studentName',
                    'Profile.phone',
                    'Profile.instructor_id',
                    'User.id',
                    'User.uuid',
                    'User.username',
                    'CountLesson.purchased_lesson_count',
                    'CountLesson.appointed_lesson_count',
                    'COUNT(Waiting.id) as count_waiting'
                ),
                'recursive' => -1,
                'joins' => array(
                    array(
                        'table' => 'packages',
                        'type' => 'LEFT',
                        'alias' => 'Package',
                        'conditions' => array('Package.id = Appointment.package_id')
                    ),
                    array(
                        'table' => 'profiles',
                        'type' => 'LEFT',
                        'alias' => 'Profile',
                        'conditions' => array('Profile.user_id = Appointment.appointment_by')
                    ),
                    array(
                        'table' => 'users',
                        'type' => 'LEFT',
                        'alias' => 'User',
                        'conditions' => array('User.id = Appointment.appointment_by')
                    ),
                    array(
                        'table' => 'waitings',
                        'type' => 'LEFT',
                        'alias' => 'Waiting',
                        'conditions' => array('Waiting.appointment_id = Appointment.id')
                    ),
                    array(
                        'table' => 'count_lessons',
                        'type' => 'LEFT',
                        'alias' => 'CountLesson',
                        'conditions' => array('CountLesson.user_id = Appointment.appointment_by')
                    ),
                ),
                'group' => 'Appointment.id'
            )
        );

        $data = array();

        foreach ($results as $result) {
            $isPaid = false;
            if ($result['Package']['total'] > 0) {
                $isPaid = true;
            }

            $isPayAtFacility = false;
            if ($result['Appointment']['pay_at_facility'] == 1) {
                $isPayAtFacility = true;
            }

            $justBooked = false;
            if ($result['Appointment']['just_booked'] == 1) {
                $justBooked = true;
            }

            $data[] = array(
                'id' => $result['Appointment']['id'],
                'title' => $result['Appointment']['title'],
                'name' => $result[0]['studentName'],
                'start' => $result['Appointment']['start'],
                'end' => $result['Appointment']['end'],
                'appointment_status' => $result['Appointment']['status'],
                'is_reschedule' => $result['Appointment']['is_reschedule'],
                'is_reassigned' => $result['Appointment']['is_reassigned'],
                'package' => $result['Package']['name'],
                'student_profile' => $result['Profile'],
                'uuid' => $result['User']['uuid'],
                'username' => $result['User']['username'],
                'student_id' => $result['User']['id'],
                'lesson_sum' => $result['Appointment']['lesson_sum'],
                'lesson_left' => $result['CountLesson']['purchased_lesson_count'] - $result['CountLesson']['appointed_lesson_count'],
                'scheduled' => $result['CountLesson']['appointed_lesson_count'],
                'total' => $result['CountLesson']['purchased_lesson_count'],
                'type' => 'appointment',
                'is_paid_lesson' => $isPaid,
                'is_pay_at_facility' => $isPayAtFacility,
                'just_booked' => $justBooked,
                'next_lesson_start' => $result['Appointment']['next_lesson_start'],
                'next_lesson_end' => $result['Appointment']['next_lesson_end'],
                'note' => $result['Appointment']['note'],
                'count_waiting' => $result[0]['count_waiting']
            );
        }

        if ($results) {
            return $data;
        }

        return array();
    }


    /**
     * @param $instructorIDs
     * @param $start
     * @param $end
     * @return array
     */
    public function getAppointmentOfTC($instructorIDs, $start, $end)
    {
        $appointments = array();
        foreach($instructorIDs as $key => $value)
        {
            $appointments = array_merge($appointments, $this->getTCAppoitments($value, $start, $end));
        }

        return $appointments;
    }

    /**
     * @param $instructorID
     * @param $start
     * @param $end
     * @return array
     */
    protected function getTCAppoitments($instructorID, $start, $end)
    {
        $results = $this->find(
            'all',
            array(
                'conditions' =>
                    array(
                        'Appointment.instructor_id' => $instructorID,
                        /*'Appointment.start >=' => $start,
                        'Appointment.end <=' => $end*/
                    ),
                'fields' => array(
                    'Appointment.id',
                    'Appointment.title',
                    'Appointment.start',
                    'Appointment.end',
                    'Appointment.lesson_no',
                    'Appointment.lesson_sum',
                    'Appointment.status',
                    'Appointment.is_reschedule',
                    'Appointment.is_reassigned',
                    'Appointment.pay_at_facility',
                    'Appointment.next_lesson_start',
                    'Appointment.next_lesson_end',
                    'Appointment.just_booked',
                    'Appointment.note',
                    'Package.name',
                    'Package.id',
                    'Package.total',
                    'Profile.user_id',
                    'Profile.first_name',
                    'Profile.last_name',
                    'CONCAT(Profile.first_name, " ", Profile.last_name) as studentName',
                    'Profile.phone',
                    'Profile.instructor_id',
                    'User.id',
                    'User.uuid',
                    'User.username',
                    'CONCAT(InstructorProfile.first_name, " ", InstructorProfile.last_name) as instructorName',
                    'InstructorUser.id',
                    'InstructorUser.uuid',
                    'InstructorUser.username',
                    'CountLesson.purchased_lesson_count',
                    'CountLesson.appointed_lesson_count',
                    'COUNT(Waiting.id) as count_waiting'
                ),
                'recursive' => -1,
                'joins' => array(
                    array(
                        'table' => 'packages',
                        'type' => 'LEFT',
                        'alias' => 'Package',
                        'conditions' => array('Package.id = Appointment.package_id')
                    ),
                    array(
                        'table' => 'profiles',
                        'type' => 'LEFT',
                        'alias' => 'Profile',
                        'conditions' => array('Profile.user_id = Appointment.appointment_by')
                    ),
                    array(
                        'table' => 'users',
                        'type' => 'LEFT',
                        'alias' => 'User',
                        'conditions' => array('User.id = Appointment.appointment_by')
                    ),
                    array(
                        'table' => 'profiles',
                        'type' => 'LEFT',
                        'alias' => 'InstructorProfile',
                        'conditions' => array('InstructorProfile.user_id = Appointment.instructor_id')
                    ),
                    array(
                        'table' => 'users',
                        'type' => 'LEFT',
                        'alias' => 'InstructorUser',
                        'conditions' => array('InstructorUser.id = Appointment.instructor_id')
                    ),
                    array(
                        'table' => 'waitings',
                        'type' => 'LEFT',
                        'alias' => 'Waiting',
                        'conditions' => array('Waiting.appointment_id = Appointment.id')
                    ),
                    array(
                        'table' => 'count_lessons',
                        'type' => 'LEFT',
                        'alias' => 'CountLesson',
                        'conditions' => array('CountLesson.user_id = Appointment.appointment_by')
                    ),
                ),
                'group' => 'Appointment.id'
            )
        );

        $data = array();

        foreach ($results as $result) {
            $isPaid = false;
            if ($result['Package']['total'] > 0) {
                $isPaid = true;
            }

            $isPayAtFacility = false;
            if ($result['Appointment']['pay_at_facility'] == 1) {
                $isPayAtFacility = true;
            }

            $justBooked = false;
            if ($result['Appointment']['just_booked'] == 1) {
                $justBooked = true;
            }

            $data[] = array(
                'id' => $result['Appointment']['id'],
                'title' => $result['Appointment']['title'],
                'name' => $result[0]['studentName'],
                'start' => $result['Appointment']['start'],
                'end' => $result['Appointment']['end'],
                'appointment_status' => $result['Appointment']['status'],
                'is_reschedule' => $result['Appointment']['is_reschedule'],
                'is_reassigned' => $result['Appointment']['is_reassigned'],
                'package' => $result['Package']['name'],
                'student_profile' => $result['Profile'],
                'uuid' => $result['User']['uuid'],
                'username' => $result['User']['username'],
                'student_id' => $result['User']['id'],
                'lesson_sum' => $result['Appointment']['lesson_sum'],
                'lesson_left' => $result['CountLesson']['purchased_lesson_count'] - $result['CountLesson']['appointed_lesson_count'],
                'scheduled' => $result['CountLesson']['appointed_lesson_count'],
                'total' => $result['CountLesson']['purchased_lesson_count'],
                'type' => 'appointment',
                'is_paid_lesson' => $isPaid,
                'is_pay_at_facility' => $isPayAtFacility,
                'just_booked' => $justBooked,
                'next_lesson_start' => $result['Appointment']['next_lesson_start'],
                'next_lesson_end' => $result['Appointment']['next_lesson_end'],
                'note' => $result['Appointment']['note'],
                'count_waiting' => $result[0]['count_waiting'],
                'instructor' => $result['InstructorUser'],
                'instructorName' => $result[0]['instructorName'],
                'resources' => [ltrim($result['InstructorUser']['id'])]
            );
        }

        if ($results) {
            return $data;
        }

        return array();
    }

    public function getAppointmentByID($id)
    {
        $result = $this->find(
            'first',
            array(
                'recursive' => -1,
                'conditions' => array(
                    'Appointment.id' => $id
                )
            )
        );

        if ($result) {
            return $result['Appointment'];
        }

        return null;
    }


    public function getAppointmentByIDForNotification($id)
    {
        $result = $this->find(
            'first',
            array(
                'fields' => array(
                    'Appointment.*',
                    'Package.*',
                    'Profile.*',
                    'User.*',
                    'Instructor.*',
                    'InstructorUser.*'
                ),
                'recursive' => -1,
                'conditions' => array(
                    'Appointment.id' => $id
                ),
                'joins' => array(
                    array(
                        'table' => 'packages',
                        'type' => 'LEFT',
                        'alias' => 'Package',
                        'conditions' => array('Package.id = Appointment.package_id')
                    ),
                    array(
                        'table' => 'profiles',
                        'type' => 'LEFT',
                        'alias' => 'Profile',
                        'conditions' => array('Profile.user_id = Appointment.appointment_by')
                    ),
                    array(
                        'table' => 'users',
                        'type' => 'LEFT',
                        'alias' => 'User',
                        'conditions' => array('User.id = Appointment.appointment_by')
                    ),
                    array(
                        'table' => 'profiles',
                        'type' => 'LEFT',
                        'alias' => 'Instructor',
                        'conditions' => array('Instructor.user_id = Appointment.instructor_id')
                    ),
                    array(
                        'table' => 'users',
                        'type' => 'LEFT',
                        'alias' => 'InstructorUser',
                        'conditions' => array('Instructor.id = Appointment.instructor_id')
                    ),
                ),
            )
        );

        if ($result) {
            return $result;
        }

        return null;
    }

    public function getAppointmentIDByUuid($uuid)
    {
        $result = $this->find(
            'first',
            array(
                'recursive' => -1,
                'conditions' => array(
                    'Appointment.uuid' => $uuid
                )
            )
        );

        if ($result) {
            return $result['Appointment']['id'];
        }

        return null;
    }

    public function getAppointmentInfoByUuid($uuid)
    {
        $result = $this->find(
            'first',
            array(
                'recursive' => -1,
                'conditions' => array(
                    'Appointment.uuid' => $uuid
                )
            )
        );

        if ($result) {
            return $result['Appointment'];
        }

        return null;
    }

    /**
     * @param $userID
     * @return array
     *
     * Getting appointment of student. This function will return an array with appointment list.
     */
    public function myAppointments($userID)
    {
        $results = $this->find(
            'all',
            array(
                'conditions' =>
                    array(
                        'Appointment.appointment_by' => $userID,
                    ),
                'fields' => array(
                    'Appointment.id',
                    'Appointment.title',
                    'Appointment.start',
                    'Appointment.end',
                    'Appointment.lesson_no',
                    'Appointment.status',
                    'Appointment.is_reschedule',
                    'Appointment.is_reassigned',
                    'Appointment.appointment_by',
                    'Appointment.lesson_no',
                    'Appointment.pay_at_facility',
                    'Appointment.next_lesson_start',
                    'Appointment.next_lesson_end',
                    'Appointment.just_booked',
                    'Appointment.note',
                    'Package.name',
                    'Package.id',
                    'Package.total',
                    'Profile.user_id',
                    'Profile.first_name',
                    'Profile.last_name',
                    'CONCAT(Profile.first_name, " ", Profile.last_name) as studentName',
                    'Profile.phone',
                    'Profile.city',
                    'Profile.state',
                    'Profile.postal_code',
                    'Profile.street_1',
                    'Profile.instructor_id',
                    'User.id',
                    'User.username',
                    'CountLesson.purchased_lesson_count',
                    'CountLesson.appointed_lesson_count',
                    'COUNT(Waiting.id) as count_waiting'
                ),
                'recursive' => -1,
                'joins' => array(
                    array(
                        'table' => 'packages',
                        'type' => 'LEFT',
                        'alias' => 'Package',
                        'conditions' => array('Package.id = Appointment.package_id')
                    ),
                    array(
                        'table' => 'profiles',
                        'type' => 'LEFT',
                        'alias' => 'Profile',
                        'conditions' => array('Profile.user_id = Appointment.appointment_by')
                    ),
                    array(
                        'table' => 'users',
                        'type' => 'LEFT',
                        'alias' => 'User',
                        'conditions' => array('User.id = Appointment.appointment_by')
                    ),
                    array(
                        'table' => 'waitings',
                        'type' => 'LEFT',
                        'alias' => 'Waiting',
                        'conditions' => array('Waiting.appointment_id = Appointment.id')
                    ),
                    array(
                        'table' => 'count_lessons',
                        'type' => 'LEFT',
                        'alias' => 'CountLesson',
                        'conditions' => array('CountLesson.user_id = Appointment.appointment_by')
                    )
                ),
                'group' => 'Appointment.id'
            )
        );

        $appointments = array();
        foreach ($results as $result) {
            $isPaid = false;
            if ($result['Package']['total'] > 0) {
                $isPaid = true;
            }

            $isPayAtFacility = false;
            if ($result['Appointment']['pay_at_facility'] == 1) {
                $isPayAtFacility = true;
            }

            $justBooked = false;
            if ($result['Appointment']['just_booked'] == 1) {
                $justBooked = true;
            }

            $appointments[] = array(
                'id' => $result['Appointment']['id'],
                'title' => $result['Appointment']['title'],
                'name' => $result[0]['studentName'],
                'start' => $result['Appointment']['start'],
                'end' => $result['Appointment']['end'],
                'appointment_by' => $result['Appointment']['appointment_by'],
                'lesson_no' => $result['Appointment']['lesson_no'],
                'appointment_status' => $result['Appointment']['status'],
                'is_reschedule' => $result['Appointment']['is_reschedule'],
                'is_reassigned' => $result['Appointment']['is_reassigned'],
                'package' => $result['Package']['name'],
                'student_profile' => $result['Profile'],
                'username' => $result['User']['username'],
                'student_id' => $result['User']['id'],
                'lesson_left' => $result['CountLesson']['purchased_lesson_count'] - $result['CountLesson']['appointed_lesson_count'],
                'scheduled' => $result['CountLesson']['appointed_lesson_count'],
                'total' => $result['CountLesson']['purchased_lesson_count'],
                'type' => 'appointment',
                'owner' => 'my',
                'is_paid_lesson' => $isPaid,
                'is_pay_at_facility' => $isPayAtFacility,
                'just_booked' => $justBooked,
                'next_lesson_start' => $result['Appointment']['next_lesson_start'],
                'next_lesson_end' => $result['Appointment']['next_lesson_end'],
                'note' => $result['Appointment']['note'],
                'count_waiting' => $result[0]['count_waiting']
            );


        }

        return $appointments;
    }

    /**
     * @param $userID
     * @param $instructorID
     * @return array
     *
     * Getting others appointments
     */
    public function othersAppointments($userID, $instructorID)
    {
        $results = $this->find(
            'all',
            array(
                'conditions' =>
                    array(
                        'Appointment.instructor_id' => $instructorID,
                        'Appointment.appointment_by !=' => $userID
                    ),
                'fields' => array(
                    'Appointment.id',
                    'Appointment.title',
                    'Appointment.start',
                    'Appointment.end',
                    'Appointment.appointment_by',
                    'Appointment.lesson_no',
                    'Appointment.status',
                    'Appointment.is_reschedule',
                    'Appointment.is_reassigned',
                    'Appointment.pay_at_facility',
                    'Appointment.next_lesson_start',
                    'Appointment.next_lesson_end',
                    'Appointment.just_booked',
                    'Appointment.note',
                    'Package.name',
                    'Package.id',
                    'Package.total',
                    'Profile.user_id',
                    'Profile.first_name',
                    'Profile.last_name',
                    'CONCAT(Profile.first_name, " ", Profile.last_name) as studentName',
                    'Profile.phone',
                    'Profile.city',
                    'Profile.state',
                    'Profile.postal_code',
                    'Profile.street_1',
                    'Profile.instructor_id',
                    'User.id',
                    'User.username',
                    'CountLesson.purchased_lesson_count',
                    'CountLesson.appointed_lesson_count',
                    'COUNT(Waiting.id) as count_waiting'
                ),
                'recursive' => -1,
                'joins' => array(
                    array(
                        'table' => 'packages',
                        'type' => 'LEFT',
                        'alias' => 'Package',
                        'conditions' => array('Package.id = Appointment.package_id')
                    ),
                    array(
                        'table' => 'profiles',
                        'type' => 'LEFT',
                        'alias' => 'Profile',
                        'conditions' => array('Profile.user_id = Appointment.appointment_by')
                    ),
                    array(
                        'table' => 'users',
                        'type' => 'LEFT',
                        'alias' => 'User',
                        'conditions' => array('User.id = Appointment.appointment_by')
                    ),
                    array(
                        'table' => 'waitings',
                        'type' => 'LEFT',
                        'alias' => 'Waiting',
                        'conditions' => array('Waiting.appointment_id = Appointment.id')
                    ),
                    array(
                        'table' => 'count_lessons',
                        'type' => 'LEFT',
                        'alias' => 'CountLesson',
                        'conditions' => array('CountLesson.user_id = Appointment.appointment_by')
                    )
                ),
                'group' => 'Appointment.id'
            )
        );

        $appointments = array();
        foreach ($results as $result) {
            $isPaid = false;
            if ($result['Package']['total'] > 0) {
                $isPaid = true;
            }

            $isPayAtFacility = false;
            if ($result['Appointment']['pay_at_facility'] == 1) {
                $isPayAtFacility = true;
            }

            $justBooked = false;
            if ($result['Appointment']['just_booked'] == 1) {
                $justBooked = true;
            }

            $appointments[] = array(
                'id' => $result['Appointment']['id'],
                'title' => $result['Appointment']['title'],
                'name' => $result[0]['studentName'],
                'start' => $result['Appointment']['start'],
                'end' => $result['Appointment']['end'],
                'appointment_by' => $result['Appointment']['appointment_by'],
                'lesson_no' => $result['Appointment']['lesson_no'],
                'appointment_status' => $result['Appointment']['status'],
                'is_reschedule' => $result['Appointment']['is_reschedule'],
                'is_reassigned' => $result['Appointment']['is_reassigned'],
                'package' => $result['Package']['name'],
                'student_profile' => $result['Profile'],
                'username' => $result['User']['username'],
                'student_id' => $result['User']['id'],
                'lesson_left' => $result['CountLesson']['purchased_lesson_count'] - $result['CountLesson']['appointed_lesson_count'],
                'type' => 'appointment',
                'owner' => 'other',
                'is_paid_lesson' => $isPaid,
                'is_pay_at_facility' => $isPayAtFacility,
                'just_booked' => $justBooked,
                'next_lesson_start' => $result['Appointment']['next_lesson_start'],
                'next_lesson_end' => $result['Appointment']['next_lesson_end'],
                'note' => $result['Appointment']['note'],
                'count_waiting' => $result[0]['count_waiting']
            );
        }

        return $appointments;
    }

    /**
     * @param $userID
     * @param $instructorID
     * @return array
     */
    public function otherAppointmentIDs($userID, $instructorID, $date_start, $date_end)
    {
        $results = $this->find(
            'list',
            array(
                'conditions' =>
                    array(
                        'Appointment.instructor_id' => $instructorID,
                        'Appointment.appointment_by !=' => $userID,
                        'Appointment.start BETWEEN ? AND ?' => array($date_start, $date_end),
                    ),
                'fields' => array(
                    'Appointment.id',
                ),
                'recursive' => -1,
                'joins' => array(
                    array(
                        'table' => 'waitings',
                        'type' => 'LEFT',
                        'alias' => 'Waiting',
                        'conditions' => array(
                            'Waiting.appointment_id = Appointment.id',
                            'Waiting.appointment_by !=' => $userID
                        )
                    )
                ),
                'group' => 'Appointment.id'

            ));
        //$log = $this->getDataSource()->getLog(false, false);
        //debug($log); die();
        return $results;
    }

    /**
     * @param string $status
     * @param null $createdBy
     * @param null $createdFor
     * @return array
     *
     * Count appointment based on several condition.
     */
    public function countAppointments($status = 'all', $createdBy = null, $createdFor = null)
    {
        $conditions = array();

        if ($status == 'pending') {
            $conditions = array('Appointment.status' => 1);
        } elseif ($status == 'upcoming') {
            $conditions = array('Appointment.status' => 2);
        } elseif ($status == 'cancelled') {
            $conditions = array('Appointment.status' => 3);
        } elseif ($status == 'completed') {
            $conditions = array('Appointment.status' => 4);
        }

        if ($createdBy) {
            $conditions = array_merge($conditions, array('Appointment.instructor_id' => $createdBy));
        } elseif ($createdFor) {
            $conditions = array_merge($conditions, array('Appointment.appointment_by' => $createdFor));
        }

        $result = $this->find('count', array('conditions' => $conditions));

        return $result;
    }

    public function getAppointments($options = array())
    {
        $conditions = array();
        $queryType = 'all';
        $limit = 5;

        if (isset($options['conditions'])) {
            $conditions = array_merge($conditions, $options['conditions']);
        }

        if (isset($options['query'])) {
            $queryType = $options['query'];
        }

        if (isset($options['limit'])) {
            $limit = $options['limit'];
        }

        $fields = array(
            'Appointment.*',
            'User.uuid',
            'InstructorUser.uuid',
            'CONCAT(Profile.first_name, " ", Profile.last_name) as studentName',
            'CONCAT(Instructor.first_name, " ", Instructor.last_name) as instructorName',
        );

        $options = array(
            'conditions' => $conditions,
            'fields' => $fields,
            'recursive' => -1,
            'limit' => $limit,
            'order' => 'Appointment.start ASC',
            'joins' => array(
                array(
                    'table' => 'profiles',
                    'type' => 'LEFT',
                    'alias' => 'Profile',
                    'conditions' => array('Profile.user_id = Appointment.appointment_by')
                ),
                array(
                    'table' => 'users',
                    'type' => 'LEFT',
                    'alias' => 'User',
                    'conditions' => array('User.id = Appointment.appointment_by')
                ),
                array(
                    'table' => 'profiles',
                    'type' => 'LEFT',
                    'alias' => 'Instructor',
                    'conditions' => array('Instructor.user_id = Appointment.instructor_id')
                ),
                array(
                    'table' => 'users',
                    'type' => 'LEFT',
                    'alias' => 'InstructorUser',
                    'conditions' => array('InstructorUser.id = Appointment.instructor_id')
                ),
            )
        );

        $result = $this->find($queryType, $options);

        return $result;
    }


    public function getAppointmentOverview($options = array())
    {
        $conditions = array();

        if (isset($options['conditions'])) {
            $conditions = array_merge($conditions, $options['conditions']);
        }

        $fields = array(
            'SUM(CASE WHEN Appointment.status = 1 THEN 1 ELSE 0 END) AS countPendingAppointment',
            'SUM(CASE WHEN Appointment.status = 2 THEN 1 ELSE 0 END) AS  countConfirmAppointment',
            'SUM(CASE WHEN Appointment.status = 3 THEN 1 ELSE 0 END) AS countCancelledAppointment',
            'SUM(CASE WHEN Appointment.status = 4 THEN 1 ELSE 0 END) AS countCompletedAppointment',
            'SUM(CASE WHEN Appointment.status = 5 THEN 1 ELSE 0 END) AS  countExpiredAppointment',
            'SUM(CASE WHEN Appointment.is_reschedule = 1 THEN 1 ELSE 0 END)  countRescheduledAppointment',
            'COUNT(Appointment.id) as totalAppointment'
        );

        $options = array(
            'conditions' => $conditions,
            'fields' => $fields,
            'recursive' => -1,
        );

        $result = $this->find('all', $options);

        return $result;
    }

    public function getCancelledAppointment($userID)
    {
        $conditions = array(
            'Appointment.appointment_by' => $userID,
            'Appointment.status' => 3,
            'Appointment.is_already_rescheduled' => 0,
            'Appointment.pay_at_facility' => 0,
            'Appointment.just_booked' => null,
            /*'PackagesUser.status' => 1*/
        );

        if (isset($options['conditions'])) {
            $conditions = array_merge($conditions, $options['conditions']);
        }

        $fields = array(
            'Appointment.id',
            'Appointment.lesson_no',
            'Package.name'
        );

        $options = array(
            'conditions' => $conditions,
            'fields' => $fields,
            'recursive' => -1,
            'joins' => array(
                array(
                    'table' => 'packages_users',
                    'type' => 'LEFT',
                    'alias' => 'PackagesUser',
                    'conditions' => array('PackagesUser.package_id = Appointment.package_id')
                ),
                array(
                    'table' => 'packages',
                    'type' => 'LEFT',
                    'alias' => 'Package',
                    'conditions' => array('Package.id = Appointment.package_id')
                )
            )
        );

        $result = $this->find('all', $options);


        $data = array();
        foreach ($result as $appointment) {
            $data[$appointment['Appointment']['id']] = $appointment['Package']['name'] . '(lesson number #' . $appointment['Appointment']['lesson_no'] . ')';
        }

        return $data;
    }

    /**
     * @param $userID
     * @param array $options
     * @return array
     */
    public function getStudentsLastAppointmentID($userID, $options = array())
    {
        $conditions = array(
            'Appointment.appointment_by' => $userID,
        );

        if (isset($options['conditions'])) {
            $conditions = array_merge($conditions, $options['conditions']);
        }

        $fields = array(
            'Appointment.id',
            'Appointment.start',
        );

        $options = array(
            'conditions' => $conditions,
            'fields' => $fields,
            'recursive' => -1,
            'order' => 'Appointment.start DESC',
        );

        $result = $this->find('first', $options);

        if ($result) {
            return $result['Appointment']['id'];
        }

        return null;
    }

    /**
     * @param $userID
     * @param array $options
     * @return array
     */
    public function getUpdateStudentsNextLesson($userID, $options = array())
    {
        $conditions = array(
            'Appointment.appointment_by' => $userID,
        );

        if (isset($options['conditions'])) {
            $conditions = array_merge($conditions, $options['conditions']);
        }

        $fields = array(
            'Appointment.id',
            'Appointment.start',
            'Appointment.end',
        );

        $options = array(
            'conditions' => $conditions,
            'fields' => $fields,
            'recursive' => -1,
            'order' => 'Appointment.start ASC',
        );

        $appointments = $this->find('all', $options);

        if ($appointments) {
            $appointments = array_values($appointments);
        }


        $counter = 0;
        while (sizeof($appointments) > $counter) {

            $appointmentID = $appointments[$counter]['Appointment']['id'];
            $nextAppointmentStart = null;
            $nextAppointmentEnd = null;

            if (isset($appointments[$counter + 1])) {
                $nextAppointmentStart = $appointments[$counter + 1]['Appointment']['start'];
                $nextAppointmentEnd = $appointments[$counter + 1]['Appointment']['end'];
            }

            $this->id = $appointmentID;
            $this->saveField('next_lesson_start', $nextAppointmentStart);
            $this->saveField('next_lesson_end', $nextAppointmentEnd);

            $counter++;
        }

        return $appointments;
    }

    public function countAppointmentByUserID($userID)
    {
        $result = $this->find(
            'count',
            array(
                'conditions' => array(
                    'Appointment.appointment_by' => $userID
                ),
                'recursive' => -1,
            )
        );

        if ($result) {
            return $result;
        }

        return 0;
    }

    public function countUpcomingAppointmentByUserID($userID)
    {
        $result = $this->find(
            'count',
            array(
                'conditions' => array(
                    'Appointment.appointment_by' => $userID,
                    'Appointment.status' => 2
                ),
                'recursive' => -1,
            )
        );

        if ($result) {
            return $result;
        }

        return 0;
    }

    /**
     * @param $startingTime
     * @param $endingTime
     * @param $userID
     * @param $appointmentID
     */
    public function  setWaitingFromRangeTime($startingTime, $endingTime, $userID , $appointmentID) {
        $rangeWaitingModel = ClassRegistry::init('RangeWaiting');
        $waitingModel = ClassRegistry::init('Waiting');
        $appointment = $this->getAppointmentByID($appointmentID);
        //var_dump($appointment); die();
        $lessonNo = $appointment['lesson_no'];
        $packageID = $appointment['package_id'];

        $previousRangeWaiting = $rangeWaitingModel->getRangeWaithingListByTime($startingTime, $endingTime,
            $userID);
        if (sizeof($previousRangeWaiting) > 0) {
            foreach ($previousRangeWaiting as $rangeWaiting):
                $waiting = array(
                    'uuid' => String::uuid(),
                    'appointment_id' => $appointmentID,
                    'instructor_id' => $this->Session->read('Auth.User.Profile.instructor_id'),
                    'appointment_by' => $rangeWaiting['RangeWaiting']['user_id'],
                    'package_id' => $packageID,
                    'lesson_no' => $lessonNo,
                    'start' => $startingTime,
                    'end' => $endingTime,
                    'range_waiting_id' => $rangeWaiting['RangeWaiting']['id']
                );
                $waitingModel->create();
                $waitingModel->save($waiting);
            endforeach;
        }
    }

    public function checkAvailabltimeForAppointment($time , $instructor_id)
    {
       return $this->query("
                  select count(*) as total from appointments
                  where instructor_id = $instructor_id and (start <= end and '".$time."' between start and end) or
                (end < start and '".$time."' not between end and start)
                ");
    }

}
