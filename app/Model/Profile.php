<?php
App::uses('AppModel', 'Model');
class Profile extends AppModel{

    /**
     * Name of this Model
     *
     * @var string
     */
    public $name = 'Profile';

    /**
     * Relation between profile nad user table (profile belongsTo user).
     *
     * @var array
     */
    public $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id'
        )
    );

    /**
     * @var array
     *
     * Has many relation between Lesson and Profile.
     */
    public $hasMany = array(

    );

    /**
     * @var array
     *
     * First and Last name will retrieved by concat as 'NAME'
     */
    public $virtualFields = array(
        'name' => 'CONCAT(Profile.first_name, " ", Profile.last_name)',
    );

    /**
     * Public validation for the profile model.
     *
     * @var array
     */
    public $validate = array(
        'first_name' => array(
            'notEmpty' => array(
                'rule' => 'notEmpty',
                'message' => 'First name must be required!'
            ),
            'rule1' => array(
                'rule'    => array('between', 2, 20),
                'message' => 'First Name must be between 2 to 20 characters!'
            ),
            'custom' => array(
                'rule' => array('custom','/^[A-Z. a-z]{1,21}$/'),
                'message' => 'Number/Special character and space are not allowed, only allow(A-Z, a-z, .)!',
            )
        ),

        'last_name' => array(
            'notEmpty' => array(
                'rule' => 'notEmpty',
                'message' => 'Last name must be required!'
            ),
            'rule1' => array(
                'rule'    => array('between', 2, 20),
                'message' => 'Last name must be between 2 to 20 characters!'
            ),
            'custom' => array(
                'rule' => array('custom','/^[A-Z. a-z]{1,21}$/'),
                'message' => 'Number/Special character and space are not allowed, only allow(A-Z, a-z, .)!',
            )
        ),

        'phone' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'message' => 'Phone number must be required!',
                'allowEmpty' => true
            ),
            'rule2' => array(
                'rule' => array('phone', '/^[(]{0,1}[0-9]{3}[)]{0,1}[-\s.]{0,1}[0-9]{3}[-\s.]{0,1}[0-9]{4}$/'),
                'message' => 'Phone number in us standard!',
            ),
        ),

        'street_1' => array(
            'notEmpty' => array(
                'rule' => 'notEmpty',
                'message' => 'Street 1 must be required!',
                'allowEmpty' => true
            ),
        ),

        'street_2' => array(
            'notEmpty' => array(
                'rule' => 'notEmpty',
                'message' => 'Street 2 must be required!',
                'allowEmpty' => true
            ),
        ),

        'City' => array(
            'notEmpty' => array(
                'rule' => 'notEmpty',
                'message' => 'City must be required!',
                'allowEmpty' => true
            ),
        ),

        'postal_code' => array(
            'notEmpty' => array(
                'rule' => 'notEmpty',
                'message' => 'postal code must be required!',
                'allowEmpty' => true
            ),
            'zipcode' => array(
                'rule' => array('postal', null, 'us'),
                'message' => 'Invalid postal code! postal code must be in us standard'
            )
        ),

        'state' => array(
            'notEmpty' => array(
                'rule' => 'notEmpty',
                'message' => 'State must be required!',
                'allowEmpty' => true
            ),
        ),

        'biography' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'message' => 'Biography must be required!',
                'allowEmpty' => true
            ),
        ),

        'source' => array(
            'rule1' => array(
                'rule' => 'notEmpty',
                'message' => 'Source must be required!',
            ),
        ),

        'billing_street_1' => array(
            'notEmpty' => array(
                'rule' => 'notEmpty',
                'message' => 'Billing street 1 must be required!',
                'allowEmpty' => true
            ),
        ),

        'billing_street_2' => array(
            'notEmpty' => array(
                'rule' => 'notEmpty',
                'message' => 'Billing street 2 must be required!',
                'allowEmpty' => true
            ),
        ),

        'billing_city' => array(
            'notEmpty' => array(
                'rule' => 'notEmpty',
                'message' => 'Billing city must be required!',
                'allowEmpty' => true
            ),
        ),

        'billing_postal_doe' => array(
            'notEmpty' => array(
                'rule' => 'notEmpty',
                'message' => 'Billing postal code must be required!',
                'allowEmpty' => true
            ),
            'zipcode' => array(
                'rule' => array('postal', null, 'us'),
                'message' => 'Invalid Zip Code! zip code must be in us standard'
            )
        ),

        'billing_state' => array(
            'notEmpty' => array(
                'rule' => 'notEmpty',
                'message' => 'Billing state must be required!',
                'allowEmpty' => true
            ),
        ),
    );

    /**
     * @return bool
     */
    public function profileValidationForSignup()
    {
        $validations = array(
            'first_name' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => 'First name must be required!'
                ),
                'rule1' => array(
                    'rule'    => array('between', 2, 20),
                    'message' => 'First Name must be between 2 to 20 characters!'
                ),
                'custom' => array(
                    'rule' => array('custom','/^[A-Z. a-z]{1,21}$/'),
                    'message' => 'Number/Special character and space are not allowed, only allow(A-Z, a-z, .)!',
                )
            ),

            'last_name' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => 'Last name must be required!'
                ),
                'rule1' => array(
                    'rule'    => array('between', 2, 20),
                    'message' => 'Last name must be between 2 to 20 characters!'
                ),
                'custom' => array(
                    'rule' => array('custom','/^[A-Z. a-z]{1,21}$/'),
                    'message' => 'Number/Special character and space are not allowed, only allow(A-Z, a-z, .)!',
                )
            ),
        );

        $this->validate = $validations;
        return $this->validates();
    }


    /**
     * @return bool
     */
    public function editProfileValidation()
    {
        $validations = array(
            'first_name' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => 'First name must be required!'
                ),
                'rule1' => array(
                    'rule'    => array('between', 2, 20),
                    'message' => 'First Name must be between 2 to 20 characters!'
                ),
                'custom' => array(
                    'rule' => array('custom','/^[A-Z. a-z]{1,21}$/'),
                    'message' => 'Number/Special character and space are not allowed, only allow(A-Z, a-z, .)!',
                )
            ),

            'last_name' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => 'Last name must be required!'
                ),
                'rule1' => array(
                    'rule'    => array('between', 2, 20),
                    'message' => 'Last name must be between 2 to 20 characters!'
                ),
                'custom' => array(
                    'rule' => array('custom','/^[A-Z. a-z]{1,21}$/'),
                    'message' => 'Number/Special character and space are not allowed, only allow(A-Z, a-z, .)!',
                )
            ),

            'phone' => array(
                'rule1' => array(
                    'rule' => 'notEmpty',
                    'message' => 'Phone number must be required!',
                    'allowEmpty' => true
                ),
                'rule2' => array(
                    'rule' => array('phone', '/^[(]{0,1}[0-9]{3}[)]{0,1}[-\s.]{0,1}[0-9]{3}[-\s.]{0,1}[0-9]{4}$/'),
                    'message' => 'Phone number in us standard!',
                ),
            ),

            'street_1' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => 'Street 1 must be required!',
                    'allowEmpty' => true
                ),
            ),

            'street_2' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => 'Street 2 must be required!',
                    'allowEmpty' => true
                ),
            ),

            'City' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => 'City must be required!',
                    'allowEmpty' => true
                ),
            ),

            'postal_code' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => 'postal code must be required!',
                    'allowEmpty' => true
                ),
                'zipcode' => array(
                    'rule' => array('postal', null, 'us'),
                    'message' => 'Invalid postal code! postal code must be in us standard'
                )
            ),

            'state' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => 'State must be required!',
                    'allowEmpty' => true
                ),
            ),

            'biography' => array(
                'rule1' => array(
                    'rule' => 'notEmpty',
                    'message' => 'Biography must be required!',
                    'allowEmpty' => true
                ),
            ),

            'source' => array(
                'rule1' => array(
                    'rule' => 'notEmpty',
                    'message' => 'Source must be required!',
                ),
            ),

            'billing_street_1' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => 'Billing street 1 must be required!',
                    'allowEmpty' => true
                ),
            ),

            'billing_street_2' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => 'Billing street 2 must be required!',
                    'allowEmpty' => true
                ),
            ),

            'billing_city' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => 'Billing city must be required!',
                    'allowEmpty' => true
                ),
            ),

            'billing_postal_doe' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => 'Billing postal code must be required!',
                    'allowEmpty' => true
                ),
                'zipcode' => array(
                    'rule' => array('postal', null, 'us'),
                    'message' => 'Invalid Zip Code! zip code must be in us standard'
                )
            ),

            'billing_state' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => 'Billing state must be required!',
                    'allowEmpty' => true
                ),
            ),
        );

        $this->validate = $validations;
        return $this->validates();
    }

    /**
     * @return bool
     *
     * Validating profile photo.
     */
    public function validateProfilePhoto()
    {
        $validateFields = array(
            'profile_pic_url' => array(
                'isImage' => array(
                    'rule' => array('isImage'),
                    'message' => 'Please provide an image!',
                ),
                /*'dimension' => array(
                    'rule' => array('dimension', 400, 400),
                    'message' => 'Your image dimensions are incorrect: please provide 400X400 image'
                ),*/
                'size' => array(
                    'rule' => array('size', 10240, 2004800),
                    'message' => 'Image size should be between 10kb - 200kb'
                ),
                'rule3' => array(
                    'rule' => array(
                        'extension',
                        array('gif', 'jpeg', 'png', 'jpg')
                    ),
                    'message' => "Please supply a valid image (allow gif, jpeg, png and jpg format)"
                ),
                'rule4' => array(
                    'rule' => array('fileSize', '<=', '1MB'),
                    'message' => 'Image must be less than 1MB'
                ),
            ),
        );
        $this->validate = $validateFields;
        return $this->validates();
    }

    /**
     * @param $data
     * @param null $width
     * @param null $height
     * @return bool
     *
     * Checking dimension of given image.
     */
    public function dimension($data, $width = null, $height = null) {
        $dimension = $file = getimagesize($data['profile_pic_url']['tmp_name']);
        if($dimension['0'] == $width && $dimension['1'] == $height)
        {
            return true;
        }
        return false;
    }

    /**
     * @param $data
     * @return bool
     *
     * Checking the given document is image or not.
     */
    public function isImage($data) {
        if(!empty($data['profile_pic_url']['name']))
        {
            return true;
        }
        return false;
    }

    /**
     * @param $data
     * @param $min
     * @param $max
     * @return bool
     *
     * Checking the size of given document.
     */
    public function size($data, $min, $max) {
        $size =$data['profile_pic_url']['size'] ;
        if($size > $min && $size < $max)
        {
            return true;
        }
        return false;
    }

    /**
     * @return bool
     */
    public function profileValidationForUpdate()
    {
        $validations = array(
            'first_name' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => 'First name must be required!'
                ),
                'rule1' => array(
                    'rule'    => array('between', 2, 20),
                    'message' => 'First Name must be between 2 to 20 characters!'
                ),
                'custom' => array(
                    'rule' => array('custom','/^[A-Z. a-z]{1,21}$/'),
                    'message' => 'Number/Special character and space are not allowed, only allow(A-Z, a-z, .)!',
                )
            ),

            'last_name' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => 'Last name must be required!'
                ),
                'rule1' => array(
                    'rule'    => array('between', 2, 20),
                    'message' => 'Last name must be between 2 to 20 characters!'
                ),
                'custom' => array(
                    'rule' => array('custom','/^[A-Z. a-z]{1,21}$/'),
                    'message' => 'Number/Special character and space are not allowed, only allow(A-Z, a-z, .)!',
                )
            ),

            'phone' => array(
                'rule1' => array(
                    'rule' => 'notEmpty',
                    'message' => 'Phone number must be required!',
                    'allowEmpty' => true
                ),
                'rule2' => array(
                    'rule' => array('phone', '/^[(]{0,1}[0-9]{3}[)]{0,1}[-\s.]{0,1}[0-9]{3}[-\s.]{0,1}[0-9]{4}$/'),
                    'message' => 'Phone number in us standard!',
                ),
            ),

            'street_1' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => 'Street 1 must be required!',
                    'allowEmpty' => true
                ),
            ),

            'street_2' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => 'Street 2 must be required!',
                    'allowEmpty' => true
                ),
            ),

            'City' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => 'City must be required!',
                    'allowEmpty' => true
                ),
            ),

            'postal_code' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => 'postal code must be required!',
                    'allowEmpty' => true
                ),
                'zipcode' => array(
                    'rule' => array('postal', null, 'us'),
                    'message' => 'Invalid postal code! postal code must be in us standard'
                )
            ),

            'state' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => 'State must be required!',
                    'allowEmpty' => true
                ),
            ),

            'biography' => array(
                'rule1' => array(
                    'rule' => 'notEmpty',
                    'message' => 'Biography must be required!',
                    'allowEmpty' => true
                ),
            ),

            'source' => array(
                'rule1' => array(
                    'rule' => 'notEmpty',
                    'message' => 'Source must be required!',
                ),
            ),

            'billing_street_1' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => 'Billing street 1 must be required!',
                    'allowEmpty' => true
                ),
            ),

            'billing_street_2' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => 'Billing street 2 must be required!',
                    'allowEmpty' => true
                ),
            ),

            'billing_city' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => 'Billing city must be required!',
                    'allowEmpty' => true
                ),
            ),

            'billing_postal_doe' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => 'Billing postal code must be required!',
                    'allowEmpty' => true
                ),
                'zipcode' => array(
                    'rule' => array('postal', null, 'us'),
                    'message' => 'Invalid Zip Code! zip code must be in us standard'
                )
            ),

            'billing_state' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => 'Billing state must be required!',
                    'allowEmpty' => true
                ),
            ),
        );

        $this->validate = $validations;
        return $this->validates();
    }

    /**
     * @param string $needed
     * @param $instructorID
     * @return array
     *
     */
    public function getStudentForInstructor($needed = 'all', $instructorID)
    {
        $fields = null;
        if($needed == 'list'){
            $fields = array('Profile.user_id', 'Profile.name');
        }

        $result = $this->find($needed, array('conditions' => array('Profile.instructor_id' => $instructorID), 'fields' => $fields));
        return $result;
    }


    /**
     * @param string $needed
     * @param array $options
     * @return array
     *
     * Getting the profile information based on some conditions.
     */
    public function getProfile($needed = 'all', $options = array())
    {
        $limit = 10;
        $fields = array();
        $orderBy = 'DESC';
        $recursive = 1;
        $queryConditions = array();

        if($needed == 'list'){
            $fields = array('Profile.id', 'Profile.name');
        }

        if(isset($options['user_id'])){
            $queryConditions = array_merge($queryConditions, array('Profile.user_id' => $options['user_id']));
        }

        if(isset($options['limit'])){
            $limit = $options['limit'];
        }

        if(isset($options['recursive'])){
            $recursive = $options['recursive'];
        }

        if(isset($options['order'])){
            $orderBy = $options['order'];
        }

        if(isset($options['conditions'])){
            if(is_array($options['conditions'])){
                $queryConditions = array_merge($queryConditions, $options['conditions']);
            }
        }

        $result = $this->find(
            $needed,
            array(
                'conditions' => $queryConditions,
                'limit' => $limit,
                'fields' => $fields,
                'recursive' => $recursive,
                'order' => 'Profile.id '. $orderBy,
            )
        );

        if($result){
            return $result;
        }
        return $result;
    }


    public function getUserListTypeHead($userID, $query)
    {

        $conditions = array(
            'Profile.instructor_id' => $userID,
            'OR' => array(
                'Profile.first_name LIKE' => '%'.$query.'%',
                'Profile.last_name LIKE' => '%'.$query.'%',
                'Profile.phone LIKE' => '%'.$query.'%',
            )
        );

        $fields = array(
            'CONCAT(Profile.first_name, " ", Profile.last_name, " - ", User.username) as contactName',
            'Profile.user_id'
        );

        $options = array(
            'conditions' => $conditions,
            'fields' => $fields,
            'recursive' => -1,
            'joins' => array(
                array(
                    'table' => 'users',
                    'type' => 'LEFT',
                    'alias' => 'User',
                    'conditions' => array('User.id = Profile.user_id')
                )
            )
        );

        $result = $this->find('all', $options);
        return $result;
    }


    public function getUserListTypeHeadForTC($userID, $query)
    {


        $conditions = array(
            'Contact.user_id' => $userID,
            'OR' => array(
                'Contact.first_name LIKE' => '%'.$query.'%',
                'Contact.last_name LIKE' => '%'.$query.'%',
                'Contact.phone LIKE' => '%'.$query.'%',
            )
        );

        $fields = array(
            'CONCAT(Contact.first_name, " ", Contact.last_name, " - ", Contact.email) as contactName',
            'Contact.user_id'
        );

        $options = array(
            'conditions' => $conditions,
            'fields' => $fields,
            'recursive' => -1,
            'joins' => array(
                array(
                    'table' => 'contacts',
                    'type' => 'LEFT',
                    'alias' => 'Contact',
                    'conditions' => array('Contact.user_id = Profile.user_id')
                )
            )
        );

        $result = $this->find('all', $options);
        return $result;
    }

    public function getStudentListTypeHeadForTC($userID, $query)
    {


        $conditions = array(
            'Profile.instructor_id' => $userID,
            'OR' => array(
                'Profile.first_name LIKE' => '%'.$query.'%',
                'Profile.last_name LIKE' => '%'.$query.'%',
                'Profile.phone LIKE' => '%'.$query.'%',
            )
        );

        $fields = array(
            'CONCAT(Profile.first_name, " ", Profile.last_name, " - ", User.username) as contactName',
            'Profile.user_id'
        );

        $options = array(
            'conditions' => $conditions,
            'fields' => $fields,
            'recursive' => -1,
            'joins' => array(
                array(
                    'table' => 'users',
                    'type' => 'LEFT',
                    'alias' => 'User',
                    'conditions' => array('User.id = Profile.user_id')
                )
            )
        );

        $result = $this->find('all', $options);
        return $result;
    }


    public function getStudentIdsByInstructorID($instructorID)
    {

        $options = array(
            'conditions' => array('Profile.instructor_id' => $instructorID),
            'fields' => array('User.id'),
            'recursive' => -1,
            'joins' => array(
                array(
                    'table' => 'users',
                    'type' => 'LEFT',
                    'alias' => 'User',
                    'conditions' => array('User.id = Profile.user_id')
                )
            )
        );

        $ids = $this->find('list', $options);
        if($ids){
            return array_values($ids);
        }

        return null;
    }

    public function getStudentIdsByTcID($tcID)
    {
        $instructorID = $this->TcsUser->getIntructorIdsForTC($tcID);

        $options = array(
            'conditions' => array('Profile.instructor_id' => $instructorID),
            'fields' => array('User.id'),
            'recursive' => -1,
            'joins' => array(
                array(
                    'table' => 'users',
                    'type' => 'LEFT',
                    'alias' => 'User',
                    'conditions' => array('User.id = Profile.user_id')
                ),
            )
        );

        $ids = $this->find('list', $options);
        if($ids){
            return array_values($ids);
        }

        return null;
    }

    /**
     * @param $instructorID
     * @param $studentIds
     * @return null
     */
    public function getStudentWithoutThisIds($instructorID, $studentIds)
    {
        $options = array(
            'conditions' => array(
                'User.id !=' => $studentIds,
                'Profile.instructor_id' => $instructorID
            ),
            'fields' => array(
                'CONCAT(Profile.first_name, " ", Profile.last_name) as name',
                'User.id'
            ),
            'recursive' => -1,
            'joins' => array(
                array(
                    'table' => 'users',
                    'type' => 'LEFT',
                    'alias' => 'User',
                    'conditions' => array('User.id = Profile.user_id')
                )
            )
        );

        $results = $this->find('all', $options);

        if($results){
            $users = array();
            foreach($results as $user){
                $users[] = array(
                    'id' => $user['User']['id'],
                    'name' => $user['0']['name']
                );
            }
            return array_values($users);
        }

        return array();
    }
}