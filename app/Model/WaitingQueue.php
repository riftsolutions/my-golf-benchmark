<?php
App::uses('AppModel', 'Model');
/**
 * WaitingQueue Model
 *
 * @property Appointment $Appointment
 * @property Waiting $Waiting
 * @property User $User
 */
class WaitingQueue extends AppModel {


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Appointment' => array(
			'className' => 'Appointment',
			'foreignKey' => 'appointment_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Waiting' => array(
			'className' => 'Waiting',
			'foreignKey' => 'waiting_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

	public function checkRunningQueue()
	{
		$current =  date('Y-m-d H:i:s');
		$afterFiveminute = date('Y-m-d H:i:s', strtotime('+5 min', strtotime($current)));

		return $this->query("
                  SELECT
                  	WaitingQueue.id,
                  	WaitingQueue.queue_time,
                  	WaitingQueue.response_code,
                  	WaitingQueue.validity_period,
                  	`User`.`id`,
                  	`User`.username,
                  	Profile.first_name,
                  	Profile.last_name,
                  	Appointment.id,

                  	Appointment.start,
                  	Appointment.end
                  from  waiting_queues as WaitingQueue
                  join users AS `User` on `User`.id = WaitingQueue.user_id
                  join profiles AS Profile on `User`.id = Profile.user_id
                  join appointments AS Appointment on WaitingQueue.appointment_id = Appointment.id

                  where WaitingQueue.status = 0 and ('".$current."' <= '".$afterFiveminute."' and WaitingQueue.queue_time between '".$current."' and '".$afterFiveminute."') or
                ('".$afterFiveminute."' < '".$current."' and queue_time not between '".$afterFiveminute."' and '".$current."')
                ");
	}

}
