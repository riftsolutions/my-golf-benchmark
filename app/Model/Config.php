<?php
App::uses('AppModel', 'Model');
class Config extends AppModel
{
    /**
     * @var string
     *
     * Name of this Billing Model
     */
    public $name = 'Config';

    /**
     * @param $key
     * @param $userID
     * @return bool
     *
     * Getting configuration data based on configuration key and user id.
     */
    public function getConfig($key, $userID){
        $result = $this->find('first', array(
                'conditions' => array(
                    'Config.user_id' => $userID,
                    'Config.key' => $key,
                )
            )
        );

        if($result){
            return $result['Config'];
        }
        return false;
    }

    /**
     * @param $key
     * @param $userID
     * @return bool
     *
     * This function wil return the configuration value of given key.
     */
    public function getConfigByKey($key, $userID){
        $result = $this->find('first', array(
                'conditions' => array(
                    'Config.user_id' => $userID,
                    'Config.key' => $key,
                )
            )
        );

        if($result){
            return $result['Config']['value'];
        }
        return false;
    }

    /**
     * @param $userID
     * @return bool
     *
     * This function will return all the configuration of given user id.
     */
    public function getAllConfiguration($userID){
        $result = $this->find('all', array(
                'conditions' => array(
                    'Config.user_id' => $userID,
                ),
                'fields' => array(
                    'Config.key',
                    'Config.value'
                )
            )
        );

        if($result){
            foreach($result as $config){
                $configuration[$config['Config']['key']] = $config['Config']['value'];
            }
            return $configuration;
        }
        return false;
    }
}
