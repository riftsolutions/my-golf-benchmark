<?php
App::uses('AppModel', 'Model');
class EmailsEvent extends AppModel
{
    /**
     * @var string
     *
     * Name of this Billing Model
     */
    public $name = 'EmailsEvent';

    public function getAllEvent()
    {
        return $this->find('all', array('conditions' => array('EmailsEvent.status' => 1)));
    }
}
