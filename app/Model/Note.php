<?php
App::uses('AppModel', 'Model');
class Note extends AppModel
{
    /**
     * @var string
     *
     * Name of this Billing Model
     */
    public $name = 'Note';

    public $validate = array(

        'contactPerson' => array(
            'rule1' => array(
                'rule' => array('notEmpty'),
                'message' => 'Contact must be required',
            )
        ),

        'note_date' => array(
            'rule1' => array(
                'rule' => array('notEmpty'),
                'message' => 'Note date must be required',
            )
        ),

        'note' => array(
            'rule1' => array(
                'rule' => array('notEmpty'),
                'message' => 'Note must be required',
            )
        ),
    );

    public $belongsTo = array(
        'Contact' => array(
            'className' => 'Contact',
            'foreignKey' => 'contact_id'
        )
    );

    public function getContactNotes($contactID)
    {
        $result = $this->find(
            'all', array(
                'conditions' => array(
                    'Note.contact_id' => $contactID
                ),
                'recursive' => -1,
                'order' => 'Note.created DESC'
            )
        );
        return $result;
    }

    public function getUserNotes($userID)
    {
        $result = $this->find(
            'all', array(
                'conditions' => array(
                    'Note.user_id' => $userID
                ),
                'recursive' => -1,
                'order' => 'Note.created DESC'
            )
        );
        return $result;
    }

    public function getFollowUp($options = array())
    {
        $limit = 5;
        $conditions = array();

        if(isset($options['conditions'])){
            $conditions = array_merge($conditions, $options['conditions']);
        }

        if(isset($options['limit'])){
            $limit = $options['limit'];
        }

        $fields = array(
            'Contact.id',
            'Contact.first_name',
            'Contact.last_name',
            'Contact.email',
            'Contact.phone',
            'Note.id',
            'Note.note_date',
            'Note.note',
            'Note.status',
            'User.username',
            'User.uuid',
            'User.id',
            'Profile.first_name',
            'Profile.last_name',
            'Profile.phone',
            'CONCAT(Instructor.first_name, " ", Instructor.last_name) as instructorName',
            'InstructorUser.uuid'

        );

        $options = array(
            'conditions' => $conditions,
            'fields' => $fields,
            'recursive' => -1,
            'limit' => $limit,
            'order' => 'Note.id DESC',
            'joins' => array(
                array(
                    'table' => 'contacts',
                    'type' => 'LEFT',
                    'alias' => 'Contact',
                    'conditions' => array('Contact.id = Note.contact_id')
                ),
                array(
                    'table' => 'profiles',
                    'type' => 'LEFT',
                    'alias' => 'Profile',
                    'conditions' => array('Profile.user_id = Note.user_id')
                ),
                array(
                    'table' => 'users',
                    'type' => 'LEFT',
                    'alias' => 'User',
                    'conditions' => array('User.id = Note.user_id'),
                ),
                array(
                    'table' => 'profiles',
                    'type' => 'LEFT',
                    'alias' => 'Instructor',
                    'conditions' => array('Instructor.user_id = Profile.instructor_id')
                ),
                array(
                    'table' => 'users',
                    'type' => 'LEFT',
                    'alias' => 'InstructorUser',
                    'conditions' => array('InstructorUser.id = Profile.instructor_id')
                ),

            )
        );

        $result = $this->find('all', $options);
        return $result;
    }


    public function getNoteOverview($options = array())
    {
        $conditions = array();
        if(isset($options['conditions'])){
            $conditions = array_merge($conditions, $options['conditions']);
        }
        $now = date('Y-m-d H:i:s');
        $fields = array(
            'COUNT(Note.id) AS total',
            'SUM(CASE WHEN Note.contact_id IS NOT NULL THEN 1 ELSE 0 END) AS leadsFollowup',
            'SUM(CASE WHEN Note.user_id IS NOT NULL THEN 1 ELSE 0 END) AS usersFollowup',
            'SUM(CASE WHEN Note.rating = 0 OR Note.rating IS NULL THEN 1 ELSE 0 END) AS noRatingFollowup',
            'SUM(CASE WHEN Note.rating = 1 THEN 1 ELSE 0 END) AS oneStarRatingFollowup',
            'SUM(CASE WHEN Note.rating = 2 THEN 1 ELSE 0 END) AS twoStarRatingFollowup',
            'SUM(CASE WHEN Note.rating = 3 THEN 1 ELSE 0 END) AS threeStarRatingFollowup',
            'SUM(CASE WHEN Note.rating = 4 THEN 1 ELSE 0 END) AS fourStarRatingFollowup',
            'SUM(CASE WHEN Note.rating = 5 THEN 1 ELSE 0 END) AS fiveStarRatingFollowup',
            'SUM(CASE WHEN Note.note_date > "'.$now.'" AND Note.status = 1 THEN 1 ELSE 0 END) AS upComingFollowup',
            'SUM(CASE WHEN Note.note_date < "'.$now.'" AND Note.status = 1 THEN 1 ELSE 0 END) AS expiredFollowup',
            'SUM(CASE WHEN Note.status = 2 THEN 1 ELSE 0 END) AS followedFollowup',
            'SUM(CASE WHEN Note.status = 3 THEN 1 ELSE 0 END) AS unfollowedFollowup',
        );

        $options = array(
            'conditions' => $conditions,
            'fields' => $fields,
            'recursive' => -1,
            'joins' => array(

            ),
        );
        $result = $this->find('first', $options);
        if($result)
        {
            return $result[0];
        }
        return null;
    }
}
