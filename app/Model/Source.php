<?php
App::uses('AppModel', 'Model');
class Source extends AppModel
{
    /**
     * @var string
     *
     * Name of this Billing Model
     */
    public $name = 'Source';

    public function getSourceList($slug = false){

        if($slug){
            return $this->find('all', array('fields' => array('Source.id', 'Source.name', 'Source.slug', 'Source.id'), array('conditions' => array('Source.status' => 1))));
        }
        else{
            return $this->find('list');
        }
    }


    public function getSourceByID($id){
        $result =  $this->find('first', array('conditions' => array('Source.id' => $id)));
        if($result){
            return $result['Source']['name'];
        }
        return false;
    }

    public function getSourceBySlug($slug){
        $result =  $this->find('first', array('conditions' => array('Source.slug' => $slug)));
        if($result){
            return $result['Source']['name'];
        }
        return false;
    }
}
